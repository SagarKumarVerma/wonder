﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogUpload
{
    public partial class Form1 : Form
    {
        OleDbConnection MyConn = new OleDbConnection();
        OleDbCommand MyCmd = new OleDbCommand();
        string sSql = "";
        DataSet DsCmd = new DataSet();
        DateTime CommandTime = System.DateTime.MinValue;
        DateTime CurrentDate = System.DateTime.Today;
        bool IsSent = false;
        string SqlServer = string.Empty;
        string DB = string.Empty;
        string SqlUser = string.Empty;
        string SqlPass = string.Empty;
        string FTPAddress = string.Empty;
        string FTPUser = string.Empty;
        string FTPPass = string.Empty;
        string TimeToCall = string.Empty;
        string LocalPath = string.Empty;
        string Interval = "5";
        int Days = 1;
        DateTime OFFICEPUNCH;
        //
                string x;
               string CARDNO;
               double Ctemp = 0;
               string MaskStatus, IsAbnormal;
               double FTemp=0;
               
        DateTime DTInterval = DateTime.Now;
        public Form1()
        {
            InitializeComponent();
            SqlServer = ConfigurationSettings.AppSettings["SqlServer"];
            DB = ConfigurationSettings.AppSettings["Database"];
            SqlUser = ConfigurationSettings.AppSettings["SqlUserName"];
            SqlPass = ConfigurationSettings.AppSettings["SqlPassword"];
            GetDevice();

            // DeletePhotoData();
            GetDataFromRawData();
        }
        public void GetDevice()
        {
            try
            {
                string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
                SqlConnection con = new SqlConnection(ConnectionString);
                

                sSql = "Select SerialNumber from tblMachine";
                SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                DataSet DsEmp = new DataSet();
                SDA.Fill(DsEmp);
                if( DsEmp.Tables[0].Rows.Count>0)
                {
                    cboDevice.ValueMember = "SerialNumber";
                    cboDevice.DisplayMember = "SerialNumber";
                    cboDevice.DataSource = DsEmp.Tables[0];
                }
            }
            catch
            {

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please Select File", "TimeWatch", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                textBox1.Focus();
                return;
            }
            string Ext = "";

           string ConnectionString = "Data Source="+SqlServer.Trim()+";Initial Catalog="+DB.Trim()+";User Id="+SqlUser.Trim()+";Password="+SqlPass.Trim()+";";
           SqlConnection con = new SqlConnection(ConnectionString);
           try
           {
               StreamReader sr = new StreamReader(textBox1.Text);
               this.Cursor = Cursors.WaitCursor;


                int Rec = 0;
               while ((x = sr.ReadLine()) != null)
               {

                    Rec = Rec + 1;

                    if(Rec==3008)
                    {
                        Rec = Rec;
                    }
                   // x = sr.ReadLine();
                   lblstatus.Text = "Reading Data From Text File Please Wait!!!!";
                   Application.DoEvents();
                   string[] split = x.Split(',');
                   try
                   {
                       if (split[6].Trim() == "MINOR_FACE_VERIFY_FAIL")
                       {
                           continue;
                       }
                       CARDNO = split[0].ToString().Trim();
                       if (CARDNO.Trim() == string.Empty || CARDNO.Trim()=="'")
                       {
                           CARDNO = "Visitor";
                       }
                       OFFICEPUNCH = Convert.ToDateTime(split[4].ToString().Trim());
                       MaskStatus = split[10].ToString().Trim();
                       IsAbnormal = split[9].ToString().Trim();
                       try
                       {
                           FTemp = Convert.ToDouble(split[8].ToString().Trim().Substring(0, 5));
                       }
                       catch
                       {
                           FTemp = 0;
                       }
                      
                       Ctemp = (FTemp - 32) * 5 / 9;
                       Ctemp = Math.Round(Ctemp, 2);
                        //if (con.State != ConnectionState.Open)
                        //{
                        //    con.Open();
                        //}
                        //string Insert = " Insert Into UserAttendance (DeviceID,UserID,AttState,VerifyMode,AttDateTime,UpdateedOn,CTemp,FTemp,MaskStatus,IsAbnomal) values "+
                        //                " ('"+cboDevice.SelectedValue.ToString().Trim()+"','" + CARDNO.Trim().Replace("'","") + "','0','','" + OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', "+
                        //                " '"+Ctemp.ToString().Trim()+"','"+FTemp.ToString().Trim()+"','"+MaskStatus.Trim()+"','"+IsAbnormal.Trim()+"' )";

                        //TraceService(Insert);
                        //SqlCommand Cmd = new SqlCommand(Insert, con);
                        //Cmd.CommandType = CommandType.Text;
                        //Cmd.ExecuteNonQuery();
                        //if (con.State != ConnectionState.Closed)
                        //{
                        //    con.Close();
                        //}
                        lblstatus.Text = "Inserting Data For User: "+ CARDNO +" For Date" + OFFICEPUNCH.ToShortTimeString();
                        Application.DoEvents();
                        try
                       {
                           if (con.State != ConnectionState.Open)
                           {
                               con.Open();
                           }
                           string Insert = " Insert Into UserAttendanceM (DeviceID,UserID,AttState,VerifyMode,AttDateTime,UpdateedOn,CTemp,FTemp,MaskStatus,IsAbnomal) values " +
                                           " ('" + cboDevice.SelectedValue.ToString().Trim() + "','" + CARDNO.Trim().Replace("'", "") + "','0','','" + OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:00") + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', " +
                                           " '" + Ctemp.ToString().Trim() + "','" + FTemp.ToString().Trim() + "','" + MaskStatus.Trim() + "','" + IsAbnormal.Trim() + "' )";

                           SqlCommand  Cmd = new SqlCommand(Insert, con);
                           Cmd.CommandType = CommandType.Text;
                           Cmd.ExecuteNonQuery();
                           if (con.State != ConnectionState.Closed)
                           {
                               con.Close();
                           }
                       }
                       catch
                       {

                       }
                   
                  
                   if (CARDNO.Trim().ToUpper() != "VISITOR")
                   {
                       //CARDNO = string.Format("{0:0000000000}", Convert.ToInt32(CARDNO.Trim().Replace("'", "")));
                       //sSql = "Select Paycode,companycode,ssn,DepartmentCode from tblemployee where presentcardno='" + CARDNO + "'";
                       //SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                       //DataSet DsEmp = new DataSet();
                       //SDA.Fill(DsEmp);

                       //lblstatus.Text = "Inserting Data For Code" + CARDNO + "  For Date- " + OFFICEPUNCH.ToString("dd/MM/yyyy HH:mm:ss");
                       //Application.DoEvents();
                       //if (DsEmp.Tables[0].Rows.Count > 0)
                       //{
                       //    try
                       //    {
                       //        if (con.State != ConnectionState.Open)
                       //        {
                       //            con.Open();
                       //        }

                       //        sSql = "insert into MachineRawPunch (CardNo, OfficePunch,P_Day, IsManual,PayCode,SSN,CompanyCode) values" +
                       //               "('" + CARDNO.Trim() + "','" + OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm") + "','N','N','" + DsEmp.Tables[0].Rows[0]["Paycode"].ToString().Trim() + "'," +
                       //               "'" + DsEmp.Tables[0].Rows[0]["SSN"].ToString().Trim() + "','" + DsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim() + "')";
                       //        Cmd = new SqlCommand(sSql, con);
                       //        Cmd.CommandType = CommandType.Text;
                       //        Cmd.ExecuteNonQuery();
                       //        if (con.State != ConnectionState.Closed)
                       //        {
                       //            con.Close();
                       //        }
                       //        try
                       //        {
                       //            using (MyConn = new OleDbConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
                       //            {
                       //                int result = 0;

                       //                MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                       //                MyCmd.CommandTimeout = 1800;
                       //                MyCmd.CommandType = CommandType.StoredProcedure;
                       //                MyCmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd"));
                       //                MyCmd.Parameters.AddWithValue("@ToDate", System.DateTime.Now);
                       //                MyCmd.Parameters.AddWithValue("@CompanyCode", DsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim());
                       //                MyCmd.Parameters.AddWithValue("@DepartmentCode", DsEmp.Tables[0].Rows[0]["DepartmentCode"].ToString().Trim());
                       //                MyCmd.Parameters.AddWithValue("@PayCode", DsEmp.Tables[0].Rows[0]["PayCode"].ToString().Trim());
                       //                MyCmd.Parameters.AddWithValue("@SetupId", 3);
                       //                MyConn.Open();
                       //                result = MyCmd.ExecuteNonQuery();
                       //                MyConn.Close();

                       //            }
                       //        }
                       //        catch (Exception Ex)
                       //        {

                       //            TraceService("Error In SP-->>" + Ex.Message);
                       //        }
                       //    }
                       //    catch
                       //    {


                       //    }
                       //}

                   }
                     }
               
                   catch (Exception Exr)
                   {
                       TraceService("Line85-->>" + Exr.Message);
                   }
               }
               lblstatus.Text = "Reading Data From Text File Please Wait!!!!";
               Application.DoEvents();
               MessageBox.Show("Data Inserted Successfully", "TimeWatch", MessageBoxButtons.OK, MessageBoxIcon.Information); 
           }
           catch (Exception ex)
           {

               TraceService(ex.Message.ToString());
           }
           this.Cursor = Cursors.Default;
           textBox1.Text = "";
           lblstatus.Text = "";

           Application.DoEvents();
       

            
        }
        public void TraceService(string errmessage)
        {
            FileStream fs = new FileStream(@"E:\ErrorLog.txt", FileMode.OpenOrCreate, FileAccess.Write);
           // FileStream fs = new FileStream(@""+Application.ExecutablePath+"\\ErrorLog.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine("\n" + errmessage);
            sw.Flush();
            sw.Close();
        }
        public void TraceServiceDelete (string errmessage)
        {
            FileStream fs = new FileStream(@"E:\DeletePhoto.txt", FileMode.OpenOrCreate, FileAccess.Write);
            // FileStream fs = new FileStream(@""+Application.ExecutablePath+"\\ErrorLog.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine("\n" + errmessage);
            sw.Flush();
            sw.Close();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = ".csv",
                //Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }else
            {
                return;
            }
          
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        protected void RTCProcess()
        {
            try
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                DateTime LastDate = System.DateTime.Today;
                DataSet DSEMP = new DataSet();
                string SetupID = "1";
                string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";

                SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + DB + ";Data source=" + SqlServer + ";User ID=" + SqlUser + ";Password=" + SqlPass + "");

                while (startDate<=LastDate)
                {
                    sSql = " select tblEmployeeShiftMaster.paycode,departmentcode,tblEmployeeShiftMaster.ssn,CompanyCode,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,tbltimeregister.dateoffice " +
                                          " from tblemployee inner join tblEmployeeShiftMaster on tblEmployeeShiftMaster.SSN = tblemployee.SSN inner  join tbltimeregister on tbltimeregister.PAYCODE = tblemployee.PAYCODE " +
                                          " where Active = 'Y'  and DateOFFICE >= '" + startDate.ToString("yyyy-MM-dd") + "' and HOURSWORKED> 0 and IN1 is null";

                    TraceServiceErr(sSql, "Query");
                    SqlDataAdapter SDE = new SqlDataAdapter(sSql, con);
                    SDE.Fill(DSEMP);
                    TraceServiceErr(DSEMP.Tables[0].Rows.Count.ToString(), "--> Count");
                    if (DSEMP.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in DSEMP.Tables[0].Rows)
                        {
                            try
                            {
                                if (con.State != ConnectionState.Open)
                                {
                                    con.Open();
                                }
                                sSql = "update MachineRawPunch Set P_Day='N' where SSN='" + dr["SSN"].ToString().Trim() + "' and officepunch>='" + startDate.ToString("yyyy-MM-dd") + "'";

                                SqlCommand Cmd = new SqlCommand(sSql, con);
                                Cmd.CommandType = CommandType.Text;
                                Cmd.ExecuteNonQuery();
                                if (con.State != ConnectionState.Closed)
                                {
                                    con.Close();
                                }
                                sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup  where companycode='" + dr["CompanyCode"].ToString().Trim() + "') ";
                                SqlDataAdapter SDS = new SqlDataAdapter(sSql, con);
                                DataSet DsS = new DataSet();
                                SDS.Fill(DsS);
                                if (DsS.Tables[0].Rows.Count > 0)
                                {
                                    SetupID = DsS.Tables[0].Rows[0]["setupid"].ToString().Trim();
                                }
                                try
                                {

                                    if (con.State != ConnectionState.Open)
                                    {
                                        con.Open();
                                    }

                                    SqlCommand sqlCmd = new SqlCommand("ProcessBackDate", con);
                                    sqlCmd.CommandType = CommandType.StoredProcedure;
                                    sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = startDate.AddDays(-1).ToString("yyyy-MM-dd");
                                    sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DateTime.Now;
                                    sqlCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = dr["CompanyCode"].ToString().Trim();
                                    sqlCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = dr["departmentcode"];
                                    sqlCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = dr["paycode"];
                                    sqlCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;

                                    sqlCmd.ExecuteNonQuery();
                                    if (con.State != ConnectionState.Closed)
                                    {
                                        con.Close();
                                    }
                                }
                                catch (Exception Ex)
                                {
                                    if (con.State != ConnectionState.Closed)
                                    {
                                        con.Close();
                                    }
                                    TraceServiceErr("SP-Loop PayCode " + dr["paycode"].ToString().Trim() + " -->>  ", Ex.Message.ToString());
                                    continue;
                                }


                            }
                            catch (Exception Ex)
                            {

                                TraceServiceErr("RTCProcess-Loop", Ex.Message.ToString());
                                continue;
                            }

                        }

                    }
                    TraceServiceErr("RTCProcess-Completed", System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss").Trim());
                    startDate = startDate.AddDays(1);
                }
               


            }
            catch (Exception Exc)
            {
                TraceServiceErr("RTCProcess", Exc.Message);
            }
        }

        private void TraceServiceErr(string errmessage, string source)
        {
            //FileNameLog = "ZKPUSHSERVICE_LOG-" + DateTime.Now + ".txt";
            //FileStream fs = new FileStream(@"D:\ZKLOGS\" + FileNameLog, FileMode.OpenOrCreate, FileAccess.Write);
            FileStream fs = new FileStream(@"D:\AttendanceService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(errmessage + source + "::" + DateTime.Now);
            sw.Flush();
            sw.Close();



        }
        private void timer1_Tick(object sender, EventArgs e)
        {
             timer1.Enabled = false;
           
            if (DTInterval <= DateTime.Now)
            {
                DTInterval = DTInterval.AddMinutes(Convert.ToDouble(Interval));

                // UpdateRTC();
                // UpdateRTC();
                //RTCProcess();
                GetDataFromRawData();
              //  timer1.Start();
            }
            

            timer1.Enabled = true;
        }
        private void UpdateRTC()
        {
            try
            {

                DateTime now = DateTime.Now.AddMonths(-1);
                var startDate = new DateTime(now.Year, now.Month, 1);
                DateTime LastDate = System.DateTime.Today;
                DataSet DSEMP = new DataSet();
                string ConnectionString = "Data Source="+SqlServer.Trim()+";Initial Catalog="+DB.Trim()+";User Id="+SqlUser.Trim()+";Password="+SqlPass.Trim()+";";
                SqlConnection con = new SqlConnection(ConnectionString);
                sSql = "select tblEmployeeShiftMaster.paycode,departmentcode,tblEmployeeShiftMaster.ssn,CompanyCode,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from tblemployee inner join tblEmployeeShiftMaster on tblEmployeeShiftMaster.paycode=tblemployee.paycode where Active='Y' and tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK='Y'";
                SqlDataAdapter SDE = new SqlDataAdapter(sSql, con);
                SDE.Fill(DSEMP);
                if (DSEMP.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in DSEMP.Tables[0].Rows)
                    {
                        try
                        {
                            lblstatus.Text = "Updating Attendance Data For Code-->>" + dr["paycode"].ToString().Trim() ;
                            Application.DoEvents();

                            if (con.State != ConnectionState.Open)
                            {
                                con.Open();
                            }
                            sSql = "update MachineRawPunch Set P_Day='N' where paycode='" + dr["paycode"].ToString().Trim() + "' and officepunch>='"+startDate.ToString("yyyy-MM-dd")+"'";

                            SqlCommand Cmd = new SqlCommand(sSql, con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.ExecuteNonQuery();
                            if (con.State != ConnectionState.Closed)
                            {
                                con.Close();
                            }




                            using (MyConn = new OleDbConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
                            {
                                MyCmd = new OleDbCommand("ProcessAllRecords", MyConn);
                                MyCmd.CommandTimeout = 1800;
                                MyCmd.CommandType = CommandType.StoredProcedure;
                                MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = startDate.AddDays(-1).ToString("yyyy-MM-dd");
                                MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = DateTime.Now;
                                MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = dr["CompanyCode"].ToString().Trim();
                                //  MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = dr["departmentcode"];
                                MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = dr["paycode"];
                                MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = 3;
                                MyConn.Open();
                                int result = MyCmd.ExecuteNonQuery();
                                MyConn.Close();

                            }
                        }
                        catch (Exception Ex)
                        {

                            TraceService(Ex.Message.ToString());
                             continue;
                        }

                    }

                }
                lblstatus.Text = "Update Attendance Data Finished At-->"+System.DateTime.Now;
                Application.DoEvents();
               

            }
            catch (Exception ex)
            {

                TraceService(ex.Message.ToString());
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
                SqlConnection con = new SqlConnection(ConnectionString);
                if (con.State==ConnectionState.Open)
                {
                    MessageBox.Show("Connection Success", "TimeWatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }


                DateTime ProcessFrom = System.DateTime.MinValue;
                ProcessFrom = Convert.ToDateTime(dateTimePicker1.Value);
                DateTime OFFICEPUNCH;
                if(checkBox1.Checked)
                {
                    sSql = "select * from UserAttendance where AttDateTime>='" + ProcessFrom.ToString("yyyy-MM-dd") + "' and UserID!='VISITOR'  order by AttDateTime ";
                }
                
                else
                {
                    sSql = "select * from UserAttendance where AttDateTime>='" + ProcessFrom.ToString("yyyy-MM-dd") + "' and UserID!='VISITOR'  order by AttDateTime";
                }
                SqlDataAdapter SDP = new SqlDataAdapter(sSql, con);
                DataSet DsP = new DataSet();
                SDP.Fill(DsP);
                if(DsP.Tables[0].Rows.Count>0)
                {
                    for (int P=0;P<DsP.Tables[0].Rows.Count;P++)
                    {
                        string CARDNO = string.Format("{0:00000000}", Convert.ToInt32(DsP.Tables[0].Rows[P]["UserID"].ToString().Trim()));
                        OFFICEPUNCH = Convert.ToDateTime(DsP.Tables[0].Rows[P]["AttDateTime"].ToString().Trim());
                        sSql = "Select Paycode,companycode,ssn,DepartmentCode from tblemployee where presentcardno='" + CARDNO + "'";
                        SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                        DataSet DsEmp = new DataSet();
                        SDA.Fill(DsEmp);

                        lblstatus.Text = "Inserting Data For Code" + CARDNO + "  For Date- " + OFFICEPUNCH.ToString("dd/MM/yyyy HH:mm:ss");
                        Application.DoEvents();
                        if (DsEmp.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                try
                                {
                                    if (con.State != ConnectionState.Open)
                                    {
                                        con.Open();
                                    }

                                    sSql = "insert into MachineRawPunch (CardNo, OfficePunch,P_Day, IsManual,PayCode,SSN,CompanyCode) values" +
                                           "('" + CARDNO.Trim() + "','" + OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm") + "','N','N','" + DsEmp.Tables[0].Rows[0]["Paycode"].ToString().Trim() + "'," +
                                           "'" + DsEmp.Tables[0].Rows[0]["SSN"].ToString().Trim() + "','" + DsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim() + "')";
                                    SqlCommand Cmd = new SqlCommand(sSql, con);
                                    Cmd.CommandType = CommandType.Text;
                                    Cmd.ExecuteNonQuery();
                                    if (con.State != ConnectionState.Closed)
                                    {
                                        con.Close();
                                    }
                                }
                                catch 
                                {
                                    
                                    
                                }
                                try
                                {
                                    using (MyConn = new OleDbConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
                                    {
                                        int result = 0;

                                        MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                        MyCmd.CommandTimeout = 1800;
                                        MyCmd.CommandType = CommandType.StoredProcedure;
                                        MyCmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd"));
                                        MyCmd.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd"));
                                        MyCmd.Parameters.AddWithValue("@CompanyCode", DsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim());
                                        MyCmd.Parameters.AddWithValue("@DepartmentCode", DsEmp.Tables[0].Rows[0]["DepartmentCode"].ToString().Trim());
                                        MyCmd.Parameters.AddWithValue("@PayCode", DsEmp.Tables[0].Rows[0]["PayCode"].ToString().Trim());
                                        MyCmd.Parameters.AddWithValue("@SetupId", 3);
                                        MyConn.Open();
                                        result = MyCmd.ExecuteNonQuery();
                                        MyConn.Close();

                                    }
                                }
                                catch (Exception Ex)
                                {

                                    TraceService("Error In SP-->>" + Ex.Message);
                                }
                            }
                            catch
                            {


                            }
                        }

                    }
                    lblstatus.Text = "Update Attendance Data Finished At-->" + System.DateTime.Now;
                    Application.DoEvents();
                }
                else
                {
                    MessageBox.Show("No Record Found!!!", "TimeWatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                MessageBox.Show("Data Download Completed", "TimeWatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            catch
            {

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                DateTime now = DateTime.Now.AddMonths(-1);
                var startDate = new DateTime(now.Year, now.Month, 1);
                DateTime LastDate = System.DateTime.Today;
                DataSet DSEMP = new DataSet();
                DateTime PDate = System.DateTime.Today;
                PDate = Convert.ToDateTime(dateTimePicker1.Value);
                string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
                SqlConnection con = new SqlConnection(ConnectionString);

                sSql = " select tblEmployeeShiftMaster.paycode,departmentcode,tblEmployeeShiftMaster.ssn,CompanyCode,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK"+
                       " from tblemployee inner join tblEmployeeShiftMaster on tblEmployeeShiftMaster.paycode=tblemployee.paycode where Active='Y' and tblemployee.companycode='HOWC' and tblemployee.paycode in " +
                       " (select distinct paycode from  machinerawpunch where convert(varchar(10),officepunch,120)='" + PDate.ToString("yyyy-MM-dd") + "')";
                SqlDataAdapter SDE = new SqlDataAdapter(sSql, con);
                SDE.Fill(DSEMP);
                if (DSEMP.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in DSEMP.Tables[0].Rows)
                    {
                        try
                        {
                            RemoveDuplicatePunches(PDate, dr["paycode"].ToString().Trim(), 15);
                            lblstatus.Text = "Updating Attendance Data For Code-->>" + dr["paycode"].ToString().Trim();
                            Application.DoEvents();
                            using (MyConn = new OleDbConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
                            {
                                MyCmd = new OleDbCommand("ProcessAllRecords", MyConn);
                                MyCmd.CommandTimeout = 1800;
                                MyCmd.CommandType = CommandType.StoredProcedure;
                                MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PDate.ToString("yyyy-MM-dd");
                                MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Today.ToString("yyyy-MM-dd");
                                MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = dr["CompanyCode"].ToString().Trim();
                                MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = dr["paycode"];
                                MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = 6;
                                MyConn.Open();
                                int result = MyCmd.ExecuteNonQuery();
                                MyConn.Close();

                            }
                        }
                        catch (Exception Ex)
                        {

                            TraceService(Ex.Message.ToString());
                            continue;
                        }

                    }

                }
                lblstatus.Text = "Update Attendance Data Finished At-->" + System.DateTime.Now;
                Application.DoEvents();


            }
            catch (Exception ex)
            {

                TraceService(ex.Message.ToString());
            }

        }

        public void RemoveDuplicatePunches(DateTime PunchDate, string Empcode, int DupMin)
        {
            string sSql = "";
            string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
            SqlConnection con = new SqlConnection(ConnectionString);
            string mEmpcd;
            DateTime mDate;
            try
            {
                sSql = "Select * from Machinerawpunch where officepunch between '" + PunchDate.ToString("yyyy-MM-dd 00:00:00") + "' and '" + PunchDate.ToString("yyyy-MM-dd 23:59:59") + "' and Paycode='" + Empcode + "'  Order By Cardno,officepunch";
                SqlDataAdapter SDMRP = new SqlDataAdapter(sSql, con);
                DataSet DsPunch = new DataSet();
                SDMRP.Fill(DsPunch);
                if (DsPunch.Tables[0].Rows.Count > 0)
                {
                    for (int Temp = 0; Temp < DsPunch.Tables[0].Rows.Count; Temp++)
                    {
                        mEmpcd = DsPunch.Tables[0].Rows[Temp]["Cardno"].ToString().Trim();
                        mDate = Convert.ToDateTime(DsPunch.Tables[0].Rows[Temp]["officepunch"].ToString().Trim());
                       int Temp1 = Temp + 1;
                        if (Temp1 > DsPunch.Tables[0].Rows.Count -1)
                        {
                            break;
                        }
                        if ((Convert.ToDateTime(DsPunch.Tables[0].Rows[Temp1]["OfficePunch"].ToString().Trim()).Subtract(mDate).TotalMinutes <= DupMin))
                        {
                           
                            if (con.State != ConnectionState.Open)
                            {
                                con.Open();
                            }

                            
                            sSql = "Delete from MachineRawPunch Where Cardno='" + mEmpcd + "' And OfficePunch = '" + Convert.ToDateTime(DsPunch.Tables[0].Rows[Temp1]["OfficePunch"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm") + "'";
                            SqlCommand Cmd = new SqlCommand(sSql, con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.ExecuteNonQuery();
                            if (con.State != ConnectionState.Closed)
                            {
                                con.Close();
                            }

                        }
                        else
                        {
                            mDate = Convert.ToDateTime(DsPunch.Tables[0].Rows[Temp1]["officepunch"].ToString().Trim());
                        }

                    }


                }

            }

            catch (Exception ex)
            {
                TraceService("RemoveDuplicatePunches" + ex.Message);

            }


        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                string Paycode = "9004";
                DateTime PunchTime = System.DateTime.Now;
                RemoveDuplicatePunches(PunchTime, Paycode, 10);

            }
            catch
            {

            }
        }


        private void GetDataFromRawData()
        {
            try
            {
                int Res=0;
                int DupMin = 5;
                string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
                SqlConnection con = new SqlConnection(ConnectionString);
                if (con.State == ConnectionState.Open)
                {
                    lblstatus.Text = "Connection Esteblished With Database";
                    Application.DoEvents();
                }

                DateTime ProcessFrom = System.DateTime.MinValue;
                var Now=System.DateTime.Today;
               
                DateTime OFFICEPUNCH;
                //if (checkBox1.Checked)
                //{
                //    sSql = "select * from DumpUserAttendance where Upload is null and UserID!='VISITOR' and  UserID not LIKE '%[^0-9]%' and CTemp!='' and MaskStatus='yes' and IsAbnomal='false'  and DeviceID in (select Serialnumber from tblmachine where MachineType='A') " +
                //        " order by AttDateTime ";
                //}

                //else
                //{
                //    sSql = "select * from DumpUserAttendance where Upload is null and UserID!='VISITOR' and  UserID not LIKE '%[^0-9]%' and CTemp!='' and MaskStatus='yes' and IsAbnomal='false'  and DeviceID in (select Serialnumber from tblmachine where MachineType='A') " +
                //        " order by AttDateTime ";
                //}
                if (checkBox1.Checked)
                {
                    sSql = "select * from DumpUserAttendance (nolock) where Upload is null and UserID!='VISITOR' and  UserID not LIKE '%[^0-9]%'   and DeviceID in (select Serialnumber from tblmachine where MachineType='A') " +
                        " AND AttDateTime>='2020-12-29' order by AttDateTime ";
                }

                else
                {
                    sSql = "select * from DumpUserAttendance (nolock) where Upload is null and UserID!='VISITOR' and  UserID not LIKE '%[^0-9]%'  and DeviceID in (select Serialnumber from tblmachine where MachineType='A') " +
                        " AND AttDateTime>='2020-12-29' order by AttDateTime ";
                }
                SqlDataAdapter SDP = new SqlDataAdapter(sSql, con);
                DataSet DsP = new DataSet();
                SDP.Fill(DsP);
                if (DsP.Tables[0].Rows.Count > 0)
                {
                    for (int P = 0; P < DsP.Tables[0].Rows.Count; P++)
                    {
                        string CARDNO = string.Format("{0:00000000}", Convert.ToInt32(DsP.Tables[0].Rows[P]["UserID"].ToString().Trim()));
                        OFFICEPUNCH = Convert.ToDateTime(DsP.Tables[0].Rows[P]["AttDateTime"].ToString().Trim());
                        sSql = " Select tblemployee.Paycode,tblemployee.companycode,tblemployee.ssn,DepartmentCode,tblEmployeeShiftMaster.DUPLICATECHECKMIN ,max(setupid) 'SetupID' from tblemployee "+
                               " inner join tblEmployeeShiftMaster on tblEmployeeShiftMaster.PAYCODE = tblemployee.PAYCODE "+
                               " inner join tblsetup on tblsetup.CompanyCode = tblemployee.COMPANYCODE "+
                               " where presentcardno = '"+ CARDNO + "' group by  SETUPID,tblemployee.Paycode,tblemployee.companycode,"+
                               " tblemployee.ssn,DepartmentCode,tblEmployeeShiftMaster.DUPLICATECHECKMIN";
                        SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                        DataSet DsEmp = new DataSet();
                        SDA.Fill(DsEmp);

                        lblstatus.Text = "Inserting Data For Code" + CARDNO + "  For Date- " + OFFICEPUNCH.ToString("dd/MM/yyyy HH:mm:ss");
                        Application.DoEvents();
                        if (DsEmp.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                try
                                {

                                    try
                                    {
                                        DupMin = Convert.ToInt32(DsEmp.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim());
                                    }
                                    catch
                                    {
                                        DupMin = 5;
                                    }
                                    if (con.State != ConnectionState.Open)
                                    {
                                        con.Open();
                                    }
                                    sSql = "insert into MachineRawPunch (CardNo, OfficePunch,P_Day, IsManual,PayCode,SSN,CompanyCode) values" +
                                           "('" + CARDNO.Trim() + "','" + OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm") + "','N','N','" + DsEmp.Tables[0].Rows[0]["Paycode"].ToString().Trim() + "'," +
                                           "'" + DsEmp.Tables[0].Rows[0]["SSN"].ToString().Trim() + "','" + DsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim() + "')";
                                    SqlCommand Cmd1 = new SqlCommand(sSql, con);
                                    Cmd1.CommandType = CommandType.Text;
                                    Res= Cmd1.ExecuteNonQuery();
                                    if (con.State != ConnectionState.Closed)
                                    {
                                        con.Close();
                                    }
                                }
                                catch
                                {


                                }
                                try
                                {
                                    RemoveDuplicatePunch(DsEmp.Tables[0].Rows[0]["Paycode"].ToString().Trim(), OFFICEPUNCH, DupMin);

                                }
                                catch
                                {

                                }


                                    try
                                    {
                                            int result = 0;
                                            Res = 0;
                                            if (con.State != ConnectionState.Open)
                                            {
                                                con.Open();
                                            }
                                            SqlCommand  CmdSP = new SqlCommand("ProcessBackDate", con);
                                            CmdSP.CommandTimeout = 1800;
                                            CmdSP.CommandType = CommandType.StoredProcedure;
                                            CmdSP.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd"));
                                            CmdSP.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd"));
                                            CmdSP.Parameters.AddWithValue("@CompanyCode", DsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim());
                                            CmdSP.Parameters.AddWithValue("@DepartmentCode", DsEmp.Tables[0].Rows[0]["DepartmentCode"].ToString().Trim());
                                            CmdSP.Parameters.AddWithValue("@PayCode", DsEmp.Tables[0].Rows[0]["PayCode"].ToString().Trim());
                                            CmdSP.Parameters.AddWithValue("@SetupId", DsEmp.Tables[0].Rows[0]["SetupID"].ToString().Trim());
                                            
                                            result = CmdSP.ExecuteNonQuery();
                                            if (con.State != ConnectionState.Closed)
                                            {
                                                con.Close();
                                            }


                                    }
                                    catch (Exception Ex)
                                    {
                                       
                                        TraceService("Error In SP-->> " + DsEmp.Tables[0].Rows[0]["PayCode"].ToString().Trim() + "" + Ex.Message);
                                        continue;
                                    }
                               // }
                               
                            }
                            catch (Exception Ex)
                            {

                                TraceService("Error In Record-->> " + DsEmp.Tables[0].Rows[0]["PayCode"].ToString().Trim() + "" + Ex.Message);
                            }
                        }
                        
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }
                        sSql = "update DumpUserAttendance set Upload=1 where RowID='"+ DsP.Tables[0].Rows[P]["RowID"].ToString().Trim() + "'";
                        SqlCommand CmdUp = new SqlCommand(sSql, con);
                        CmdUp.CommandType = CommandType.Text;
                        CmdUp.ExecuteNonQuery();
                        if (con.State != ConnectionState.Closed)
                        {
                            con.Close();
                        }
                    }
                    
                    //if (con.State != ConnectionState.Open)
                    //{
                    //    con.Open();
                    //}
                   
                    lblstatus.Text = "Update Attendance Data Finished At-->" + System.DateTime.Now;
                    Application.DoEvents();
                }
                else
                {
                    lblstatus.Text = "No Record Found To Process";
                    Application.DoEvents();
                    return;
                }

                lblstatus.Text = "Update Attendance Data Finished At-->" + System.DateTime.Now;
                Application.DoEvents();
                return;
            }
            catch
            {

            }
        }
        private void DeletePhotoData()
        {
            string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
            SqlConnection con = new SqlConnection(ConnectionString);
            try
            {
                int Photo = Convert.ToInt32(ConfigurationSettings.AppSettings["PhotoDays"]);
                if (con.State == ConnectionState.Open)
                {
                    lblstatus.Text = "Connection Esteblished With Database";
                    Application.DoEvents();
                }
                DateTime LastDate = System.DateTime.Now.AddDays(-Photo);
                sSql = "update UserAttendance set Limage=NULL where Limage is not null and Attdatetime<='"+LastDate.ToString("yyyy-MM-dd")+"'";
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                SqlCommand CmdUp = new SqlCommand(sSql, con);
                CmdUp.CommandType = CommandType.Text;
                CmdUp.CommandTimeout = 0;
                int Result= CmdUp.ExecuteNonQuery();
                if (Result > 0)
                {
                    TraceServiceDelete(sSql);
                    TraceServiceDelete("Delete Photo Success At :" + System.DateTime.Now.ToString("dd/MM/yyyy H:mm:ss"));
                }

                if (con.State != ConnectionState.Closed)
                {
                    con.Close();
                }

                sSql = "update UserAttendanceRaw set Limage=NULL where Limage is not null and Attdatetime<='" + LastDate.ToString("yyyy-MM-dd") + "'";
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                CmdUp = new SqlCommand(sSql, con);
                CmdUp.CommandType = CommandType.Text;
                CmdUp.CommandTimeout = 0;
                Result = CmdUp.ExecuteNonQuery();
                if (Result > 0)
                {
                    TraceServiceDelete(sSql);
                    TraceServiceDelete("Delete Photo Success At :" + System.DateTime.Now.ToString("dd/MM/yyyy H:mm:ss"));
                }
                if (con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
            catch(Exception Ex)
            {
                TraceServiceDelete(sSql);
                TraceServiceDelete("Error In Delete: "+ Ex.Message);
                if (con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;

            //if (DTInterval <= DateTime.Now)
            //{
            //    DTInterval = DTInterval.AddMinutes(Convert.ToDouble(Interval));
            //    GetDataFromRawData();
               
            //}

            if(System.DateTime.Now.ToString("HH:mm").Trim()=="00:01"|| System.DateTime.Now.ToString("HH:mm").Trim() == "00:05" || System.DateTime.Now.ToString("HH:mm").Trim() == "00:10")
            {
                DeletePhotoData();
            }


            timer2.Enabled = true;
        }

        private void RemoveDuplicatePunch(string PayCode, DateTime DupDate, int DuplicateMin)
        {
            try
            {
                string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
                var con = new SqlConnection(ConnectionString);
                SqlCommand cmd;
                DateTime DupDt = DupDate;
                DataSet Rs = new DataSet();
                string mEmpcd = PayCode;
                DateTime mDate = DateTime.MinValue;
                int S_Dupli = 10;
                S_Dupli = Convert.ToInt32(DuplicateMin);
                DupDt = Convert.ToDateTime(DupDate);
                sSql = "Select * from Machinerawpunch where officepunch between '" + DupDt.ToString("yyyy-MM-dd") + " 00:00:00' and '" + DupDt.ToString("yyyy-MM-dd") + " 23:59:59' and paycode='" + mEmpcd.ToString() + "' Order By Cardno,officepunch";
                SqlDataAdapter SD = new SqlDataAdapter(sSql, con);
                Rs = new DataSet();
                SD.Fill(Rs);
                if (Rs.Tables[0].Rows.Count > 0)
                {
                    for (int id = 0; id < Rs.Tables[0].Rows.Count; id++)
                    {
                        mEmpcd = Rs.Tables[0].Rows[id]["Cardno"].ToString().Trim();
                        mDate = Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]);
                        id = id + 1;
                        if (id == Rs.Tables[0].Rows.Count)
                        {
                            break;
                        }
                        while (mEmpcd == Rs.Tables[0].Rows[id]["Cardno"].ToString().Trim())
                        {
                            if (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, mDate, Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"])) <= S_Dupli)
                            {
                                if (con.State != ConnectionState.Open)
                                {
                                    con.Open();
                                }
                                sSql = "Delete MachineRawPunch Where Cardno='" + mEmpcd + "'And OfficePunch = '" + Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]).ToString("yyyy-MM-dd HH:mm") + "'";
                                SqlCommand Cmd = new SqlCommand(sSql, con);
                                Cmd.CommandType = CommandType.Text;
                                Cmd.ExecuteNonQuery();
                                if (con.State != ConnectionState.Closed)
                                {
                                    con.Close();
                                }
                            }
                            else
                            {
                                mDate = Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]);
                            }
                            id = id + 1;
                            if (id == Rs.Tables[0].Rows.Count)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {

                }
                return;
            }
            catch (Exception Ex)
            {

                TraceService("Error In DuplicateRemove: " + Ex.Message);
            }
        }

    }
   

    internal static class SimulateIsDate
    {
        internal static bool IsDate(object expression)
        {
            if (expression == null)
                return false;

            System.DateTime testDate;
            return System.DateTime.TryParse(expression.ToString(), out testDate);
        }
    }
    internal static class SimulateVal
    {

        internal static double Val(string expression)
        {
            if (expression == null)
                return 0;

            //try the entire string, then progressively smaller
            //substrings to simulate the behavior of VB's 'Val',
            //which ignores trailing characters after a recognizable value:
            for (int size = expression.Length; size > 0; size--)
            {
                double testDouble;
                if (double.TryParse(expression.Substring(0, size), out testDouble))
                    return testDouble;
            }

            //no value is recognized, so return 0:
            return 0;
        }
        internal static double Val(object expression)
        {
            if (expression == null)
                return 0;

            double testDouble;
            if (double.TryParse(expression.ToString(), out testDouble))
                return testDouble;

            //VB's 'Val' function returns -1 for 'true':
            bool testBool;
            if (bool.TryParse(expression.ToString(), out testBool))
                return testBool ? -1 : 0;

            //VB's 'Val' function returns the day of the month for dates:
            System.DateTime testDate;
            if (System.DateTime.TryParse(expression.ToString(), out testDate))
                return testDate.Day;

            //no value is recognized, so return 0:
            return 0;
        }
        internal static int Val(char expression)
        {
            int testInt;
            if (int.TryParse(expression.ToString(), out testInt))
                return testInt;
            else
                return 0;
        }
    }
    //----------------------------------------------------------------------------------------
    //	Copyright © 2009 - 2010 Tangible Software Solutions Inc.
    //	This class can be used by anyone provided that the copyright notice remains intact.
    //
    //	This class simulates the behavior of the classic VB 'DateDiff' function.
    //----------------------------------------------------------------------------------------
    internal static class SimulateDateDiff
    {
        internal enum DateInterval
        {
            Day,
            DayOfYear,
            Hour,
            Minute,
            Month,
            Quarter,
            Second,
            Weekday,
            WeekOfYear,
            Year
        }


        internal static long DateDiff(DateInterval intervalType, System.DateTime dateOne, System.DateTime dateTwo)
        {
            switch (intervalType)
            {
                case DateInterval.Day:
                case DateInterval.DayOfYear:
                    System.TimeSpan spanForDays = dateTwo - dateOne;
                    return (long)spanForDays.TotalDays;
                case DateInterval.Hour:
                    System.TimeSpan spanForHours = dateTwo - dateOne;
                    return (long)spanForHours.TotalHours;
                case DateInterval.Minute:
                    System.TimeSpan spanForMinutes = dateTwo - dateOne;
                    return (long)spanForMinutes.TotalMinutes;
                case DateInterval.Month:
                    return ((dateTwo.Year - dateOne.Year) * 12) + (dateTwo.Month - dateOne.Month);
                case DateInterval.Quarter:
                    long dateOneQuarter = (long)System.Math.Ceiling(dateOne.Month / 3.0);
                    long dateTwoQuarter = (long)System.Math.Ceiling(dateTwo.Month / 3.0);
                    return (4 * (dateTwo.Year - dateOne.Year)) + dateTwoQuarter - dateOneQuarter;
                case DateInterval.Second:
                    System.TimeSpan spanForSeconds = dateTwo - dateOne;
                    return (long)spanForSeconds.TotalSeconds;
                case DateInterval.Weekday:
                    System.TimeSpan spanForWeekdays = dateTwo - dateOne;
                    return (long)(spanForWeekdays.TotalDays / 7.0);
                case DateInterval.WeekOfYear:
                    System.DateTime dateOneModified = dateOne;
                    System.DateTime dateTwoModified = dateTwo;
                    while (dateTwoModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    {
                        dateTwoModified = dateTwoModified.AddDays(-1);
                    }
                    while (dateOneModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    {
                        dateOneModified = dateOneModified.AddDays(-1);
                    }
                    System.TimeSpan spanForWeekOfYear = dateTwoModified - dateOneModified;
                    return (long)(spanForWeekOfYear.TotalDays / 7.0);
                case DateInterval.Year:
                    return dateTwo.Year - dateOne.Year;
                default:
                    return 0;
            }
        }
    }
}
