﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.Mail;
using System.Globalization;
using System.IO;
using System.Collections.Generic;


public partial class frmLeaveApproval : System.Web.UI.Page
{
    string strsql = "";
    string txttodate, txtfromdate;
    string headEmail = "";
    string empEmail = "";
    DateTime date1, date2;
    OleDbDataReader LTDr;
    Class_Connection cn = new Class_Connection();
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    int days;
    bool finalLevel;
    string Pcode, Duration, vno, remarks, Approval_Status, Stage, Reason, RejectedRemarks;

    static DateTime dt_FinancialYear;

    OleDbConnection conn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
    OleDbCommand comm;
    string ErrorFileName = ConfigurationManager.AppSettings["ErrorFileName"].ToString();
    ErrorClass ec = new ErrorClass();

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<a href='Default.aspx' class='link'> Back to Login Page </a> <br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            //ec.Write_Log(Session["ErrorFileName"].ToString(), DateTime.Now.Date, PageName, FunctionName, ErrorMsg);
        }
        catch (Exception errr)
        {
        }
    }        
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Put user code to initialize the page here

            if (Session["UserName"] == null && Session["PAYCODE"] == null)
            {
                Session.Abandon();
                Response.Redirect("Default.aspx");
            }
            try
            {
                MasterPage myMaster = (MasterPage)this.Master;
                Label lbl = myMaster.FindControl("lbl_WelcomeUser") as Label;
                lbl.Text = "Welcome To Leave Management System, " + Session["EmpName"].ToString();

                Label LblUserType = myMaster.FindControl("LblUserType") as Label;
                LblUserType.Text = Session["LblUserType"].ToString();

                //   lblMsg.Text = "Leave Approval";
            }
            catch
            {

            }
            if (conn.State == 0)
                conn.Open();
            if (!IsPostBack)
            {
                if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
                {
                    Request.Browser.Adapters.Clear();
                }
                fill_grid();
            }
        }
        catch (Exception er)
        {
            Error_Occured("Page Load", er.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        fill_grid();
        DataGrid1.DataBind();
    }
    public void fill_grid()
    {
        //try
        //{
        string ReportView = "";
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        string Tmp = Session["PAYCODE"].ToString();
        if (Session["usertype"].ToString().ToUpper() == "A")
        {
            
            strsql = "select slno=count(*),convert(char(10),a.request_date,103) 'RequestDate', a.application_no,a.paycode,c.EmpName,a.LeaveCode, convert(char(10),a.leave_from,103) leave_from,convert(char(10),a.leave_to,103) leave_to,a.userremarks, Duration=case when a.halfday='N' then 'Full Day' when a.halfday='F' then 'First Half' when a.halfday='S' then 'Second Half' end,leavedays=a.leavedays,' ' remarks,'False' Approval,a.halfday 'D' from leave_request a,leave_request b,tblemployee c where a.paycode=c.paycode and a.application_no>=b.application_no and (a.application_no in (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved is null ) or a.application_no in  (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved='Y' and stage2_approved is null )) ";
            if ((Session["usertype"].ToString() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            {
                strsql += " and c.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if ((Session["usertype"].ToString() == "A") && (Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            {
                strsql += " and c.Departmentcode in (" + DataFilter.AuthDept.Trim() + ") ";
            }
            strsql += "group by a.application_no,a.paycode,c.empname,a.leave_from,a.leave_to,a.userremarks,a.leavedays,a.halfday,a.LeaveCode,a.request_date order by 1";
        }
        else if (Session["usertype"].ToString().ToUpper() == "H")
        {
            
            if (ReportView.ToString().Trim() == "Y")
            {
                strsql = "select slno=count(*),convert(char(10),a.request_date,103) 'RequestDate', a.application_no,a.paycode,c.EmpName,a.LeaveCode, convert(char(10),a.leave_from,103) leave_from,convert(char(10),a.leave_to,103) leave_to,a.userremarks, Duration=case when a.halfday='N' then 'Full Day' when a.halfday='F' then 'First Half' when a.halfday='S' then 'Second Half' end,leavedays=a.leavedays,' ' remarks,'False' Approval,a.halfday 'D' from leave_request a,leave_request b,tblemployee c where a.paycode=c.paycode and a.application_no>=b.application_no and (a.application_no in (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved is null and  (headid='" + Session["PAYCODE"].ToString() + "' or headid_2='" + Session["PAYCODE"].ToString() + "' )) or a.application_no in  (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved='Y' and stage2_approved is null and  headid in (select paycode from tblemployee where (headid='" + Session["PAYCODE"].ToString() + "' or headid_2='" + Session["PAYCODE"].ToString() + "')))) group by a.application_no,a.paycode,c.empname,a.leave_from,a.leave_to,a.userremarks,a.leavedays,a.halfday,a.LeaveCode,a.request_date order by 1";
            }
            else
            {
                strsql = "select slno=count(*),convert(char(10),a.request_date,103) 'RequestDate', a.application_no,a.paycode,c.EmpName,a.LeaveCode, convert(char(10),a.leave_from,103) leave_from,convert(char(10),a.leave_to,103) leave_to,a.userremarks, Duration=case when a.halfday='N' then 'Full Day' when a.halfday='F' then 'First Half' when a.halfday='S' then 'Second Half' end,leavedays=a.leavedays,' ' remarks,'False' Approval,a.halfday 'D' from leave_request a,leave_request b,tblemployee c where a.paycode=c.paycode and a.application_no>=b.application_no and (a.application_no in (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved is null ) or a.application_no in  (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved='Y' and stage2_approved is null)) ";
                if (Session["Auth_Comp"] != null)
                {
                    strsql += " and c.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    strsql += " and c.Departmentcode in (" + DataFilter.AuthDept.Trim() + ") ";
                }
                strsql += " group by a.application_no,a.paycode,c.empname,a.leave_from,a.leave_to,a.userremarks,a.leavedays,a.halfday,a.LeaveCode,a.request_date order by 1";
            }
        }
        else if (Session["usertype"].ToString().ToUpper() == "C")
            
            strsql = "select slno=count(*),convert(char(10),a.request_date,103) 'RequestDate', a.application_no,a.paycode,c.EmpName,a.LeaveCode, convert(char(10),a.leave_from,103) leave_from,convert(char(10),a.leave_to,103) leave_to,a.userremarks, Duration=case when a.halfday='N' then 'Full Day' when a.halfday='F' then 'First Half' when a.halfday='S' then 'Second Half' end,leavedays=a.leavedays,' ' remarks,'False' Approval,a.halfday 'D' from leave_request a,leave_request b,tblemployee c where a.paycode=c.paycode and a.application_no>=b.application_no and (a.application_no in (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved is null and  headid='" + Session["PAYCODE"].ToString() + "' ) or a.application_no in  (select leave_request.application_no from tblemployee ,leave_request  where tblemployee.paycode=leave_request.paycode and stage1_approved='Y' and stage2_approved is null and  headid in (select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "'))) group by a.application_no,a.paycode,c.empname,a.leave_from,a.leave_to,a.userremarks,a.leavedays,a.halfday,a.LeaveCode,a.request_date order by 1";
        else
            return;

        OleDbDataAdapter adapter = new OleDbDataAdapter(strsql, conn);
        DataSet ds = new DataSet();
        adapter.Fill(ds);
        
        DataGrid1.DataSource = ds;
        DataGrid1.DataBind();
        
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            string code = "";
            string pdate = "";
            DateTime ApplyDate;
            List<object> keys = DataGrid1.GetSelectedFieldValues(new string[] { DataGrid1.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Any Application To Approve/Reject')</script>");
                return;
            }


            //if error occurs while approval, Please increase the length of REASON field of TBLTIMEREGISTER  
            ViewState["Approval_Status"] = "Approved";
            DataSet ds = new DataSet();
            OleDbDataAdapter adpter = new OleDbDataAdapter("", conn);
            DateTime datefrom;
            DateTime dateto;
            string mvoucher;
            int voucher;
            string valid = "Y";
            string isoff = "Y";
            string ishld = "Y";
            string lvtype = "";
            for (int DR = 0; DR < DataGrid1.VisibleRowCount; DR++)
            {
                if (DataGrid1.Selection.IsRowSelected(DR) == true)
                {
                    valid = "Y";
                    isoff = "Y";
                    ishld = "Y";
                    lvtype = "";

                   
                        string headEmail = "";
                        string empEmail = "";


                        vno = DataGrid1.GetRowValues(DR, "application_no").ToString().Trim();
                        ViewState["app"] = vno.ToString();
                        strsql = "select LeaveCode from Leave_Request where Application_No= '" + vno + "' ";
                        LTDr = cn.Execute_Reader(strsql);
                        if (LTDr.Read())
                        {
                            ViewState["LeaveCode"] = LTDr[0].ToString().Trim();
                        }
                        LTDr.Close();
                        remarks = "NA";
                        if (remarks.Contains("'"))
                        {
                            remarks = remarks.ToString().Replace("'", "''");
                        }
                        RejectedRemarks = "NR";
                        Pcode = DataGrid1.GetRowValues(DR, "paycode").ToString().Trim();
                        txtfromdate = DataGrid1.GetRowValues(DR, "leave_from").ToString().Trim();
                        txttodate = DataGrid1.GetRowValues(DR, "leave_to").ToString().Trim();
                        Duration = DataGrid1.GetRowValues(DR, "Duration").ToString().Trim();
                        Reason = DataGrid1.GetRowValues(DR, "remarks").ToString().Trim();
                        pdate = DataGrid1.GetRowValues(DR, "RequestDate").ToString().Trim(); 
                        try
                        {
                            ApplyDate = DateTime.ParseExact(pdate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        }
                        catch
                        {
                            string change = "<script language='javascript'>alert('Invalid Date')</script>";
                            Page.RegisterStartupScript("msg", change);
                            return;
                        }
                        if (Reason.ToString().Contains("'"))
                        {
                            Reason = Reason.ToString().Replace("'", "''");
                        }


                        string todate, fromdate;
                        double leaveamt;
                        OleDbDataReader reader2;
                        OleDbDataReader reader1;
                        leaveamt = 0.0;
                        bool finalLevel = true;
                        string headid = "";
                        string Empname = "";

                        strsql = "select headid,e_mail1,empname from tblemployee where paycode='" + Pcode.ToString().Trim() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                        OleDbCommand comm1 = new OleDbCommand(strsql, conn);
                        OleDbDataReader dr1 = comm1.ExecuteReader();
                        if (dr1.Read())
                        {
                            headid = dr1[0].ToString().Trim();
                            empEmail = dr1[1].ToString().Trim();
                            Empname = dr1[2].ToString().Trim();
                        }
                        dr1.Close();


                        //Head Email
                        if (headid.ToString().Trim() != "")
                        {
                            strsql = "select e_mail1 from tblemployee where paycode='" + headid.ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                            dr1 = cn.Execute_Reader(strsql);
                            if (dr1.Read())
                            {
                                headEmail = dr1[0].ToString().Trim();
                            }
                            if (headid.ToString().Trim().ToUpper() == Session["PAYCODE"].ToString().Trim().ToUpper())
                            {
                                finalLevel = false;
                            }
                            else
                            {
                                finalLevel = true;
                            }
                            dr1.Close();
                        }


                        strsql = "Select LeaveApprovalStages from TblEmployee where PayCode = '" + Pcode.ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                        comm1 = new OleDbCommand(strsql, conn);
                        dr1 = comm1.ExecuteReader();



                        if (!dr1.Read())
                        {
                            string change = "<script language='JavaScript'>alert('Leave Approval Stages not defined for the employee(s)..')</script>";
                            Page.RegisterStartupScript("frmchange", change);
                            dr1.Close();
                            return;
                        }

                        //oxford leave approval customization start here

                        if (dr1[0].ToString().Trim() == "1")
                        {
                            dr1.Close();
                            finalLevel = true;

                            strsql = "Update leave_request set Stage1_approved='Y',Stage1_approval_Remarks='" + remarks.ToString().Trim() + "', " +
                                " stage1_approval_id='" + Session["PAYCODE"].ToString() + "',stage1_approval_date=getdate() , " +
                                " Stage2_approved='Y',Stage2_approval_remarks='" + remarks.ToString().Trim() + "', " +
                                " stage2_approval_id='" + Session["PAYCODE"].ToString() + "', " +
                                " stage2_approval_date=getdate(), chklbl1='Y' where application_no='" + vno + "'";
                            cn.execute_NonQuery(strsql);
                            ViewState["FinalLevel"] = "True";
                            if (empEmail.ToString().Trim() != "")
                            {
                                try
                                {
                                    Send_MailToApplicant(Pcode, empEmail, Empname);
                                }
                                catch (Exception er)
                                {
                                    Error_Occured("Send mail part 1", er.Message);
                                }
                            }


                            Stage = "1";
                            ViewState["Stage"] = Stage.ToString();


                        }
                        else
                        {
                            dr1.Close();
                            // ** My coding end 
                            Stage = "2";
                            ViewState["Stage"] = Stage.ToString();

                            Trap_Error("butapproved_Click", "6");

                            if (!finalLevel)
                            {
                                strsql = "update leave_request set Stage1_approved='Y',Stage1_approval_Remarks='" + remarks.ToString().Trim() + "', " +
                                    " stage1_approval_id='" + Session["PAYCODE"].ToString() + "',stage1_approval_date=getdate(), chklbl1='Y' " +
                                    " where application_no='" + vno.ToString() + "'";
                                cn.execute_NonQuery(strsql);
                                ViewState["FinalLevel"] = "False";
                                if (headEmail != string.Empty)
                                    Send_MailToHead();

                                if (empEmail.ToString().Trim() != "")
                                {
                                    try
                                    {
                                        Send_MailToApplicant(Pcode, empEmail, Empname);
                                    }
                                    catch (Exception er)
                                    {
                                        Error_Occured("Send mail part 1", er.Message);
                                    }
                                }
                            }
                            else
                            {
                                if (Session["PAYCODE"].ToString().ToUpper() == "ADMIN")
                                {
                                    Trap_Error("butapproved_Click", "7");
                                    strsql = "Update leave_request set Stage1_approved='Y',Stage1_approval_Remarks='" + remarks.ToString().Trim() + "', " +
                                    " stage1_approval_id='" + Session["PAYCODE"].ToString() + "',stage1_approval_date=getdate() , " +
                                    " Stage2_approved='Y',Stage2_approval_remarks='" + remarks.ToString().Trim() + "', " +
                                    " stage2_approval_id='" + Session["PAYCODE"].ToString() + "', " +
                                    " stage2_approval_date=getdate(), chklbl1='Y' where application_no='" + vno + "'";
                                    cn.execute_NonQuery(strsql);
                                    ViewState["FinalLevel"] = "False";
                                    if (headEmail != string.Empty)
                                        Send_MailToHead();

                                    if (empEmail.ToString().Trim() != "")
                                    {
                                        try
                                        {
                                            Send_MailToApplicant(Pcode, empEmail, Empname);
                                        }
                                        catch (Exception er)
                                        {
                                            Error_Occured("Send mail part 1", er.Message);
                                        }
                                    }


                                }
                                else
                                {


                                    strsql = "update leave_request set Stage2_approved='Y',Stage2_approval_remarks='" + remarks + "',stage2_approval_id='" + Session["PAYCODE"].ToString() + "',stage2_approval_date= getdate() , chklbl1='Y' where application_no='" + vno + "'";
                                    cn.execute_NonQuery(strsql);
                                    ViewState["FinalLevel"] = "True";
                                    if (empEmail.ToString().Trim() != "")
                                    {
                                        try
                                        {
                                            Send_MailToApplicant(Pcode, empEmail, Empname);
                                        }
                                        catch (Exception er)
                                        {
                                            Error_Occured("Send mail part 1", er.Message);
                                        }
                                    }
                                }
                            }
                        }
                        //OleDbCommand comm = new OleDbCommand(strsql, conn);
                        //comm.ExecuteNonQuery();

                        if (finalLevel)
                        {
                            string status = "";
                            double bal = 0;
                            string str = "";
                            string StrSql = "";
                            string Upaycode = "";
                            string SSN = "";
                            DateTime dtFrom = System.DateTime.MinValue;
                            DateTime dtTo = System.DateTime.MinValue;
                            DataSet dslv = new DataSet();
                            double avalue = 0;
                            string st = "";
                            string lvcode = "";
                            string lvcodeTm = "";
                            string lvtype_new = "";
                            double lvamount = 0;
                            DateTime lvapr = DateTime.MinValue;
                            string reason = "";
                            string hday = "";
                            double lvamount1 = 0;
                            double lvamount2 = 0;
                            OleDbDataReader dr;

                            vno = DataGrid1.GetRowValues(DR, "application_no").ToString().Trim();
                            StrSql = "select voucherno,Paycode,leavecode,leavedays,userremarks,halfday,convert(varchar(10),leave_from,103),convert(varchar(10),leave_to,103),isnull(LWPbal,0) 'LWPbal', SSN from leave_request where application_no='" + vno.ToString().Trim() + "'";
                            ds = cn.FillDataSet(StrSql);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {
                                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                                    Upaycode = ds.Tables[0].Rows[i][1].ToString().Trim();
                                    lvcode = ds.Tables[0].Rows[i][2].ToString().Trim();
                                    lvcodeTm = ds.Tables[0].Rows[i][2].ToString().Trim();
                                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][3].ToString());
                                    reason = ds.Tables[0].Rows[i][4].ToString().Trim().Replace("'", "");
                                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                                    datefrom = DateTime.ParseExact(ds.Tables[0].Rows[i][6].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dateto = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    bal = Convert.ToDouble(ds.Tables[0].Rows[i][8].ToString().Trim());
                                    SSN = ds.Tables[0].Rows[i]["SSN"].ToString().Trim();

                                    string weekname = "";
                                    DataSet dsResult = new DataSet();

                                    DataSet dswo = new DataSet();
                                    DateTime LWPfrom = new DateTime();
                                    DateTime LWPTo = new DateTime();
                                    DateTime futDate = new DateTime();
                                    DateTime PastDate = new DateTime();
                                    DataSet dsWeekname = new DataSet();
                                    string wkname = "";

                                    if (bal > 0)
                                    {
                                        strsql = "select datename(dw,'" + datefrom.ToString("yyyy-MM-dd") + "')";
                                        dsWeekname = cn.FillDataSet(strsql);
                                        if (dsWeekname.Tables[0].Rows.Count > 0)
                                        {
                                            wkname = dsWeekname.Tables[0].Rows[0][0].ToString().Trim();
                                            if (wkname.ToString().ToUpper() == "FRIDAY")
                                            {
                                                if (bal == 2)
                                                {
                                                    PastDate = datefrom.AddDays(1);
                                                    futDate = datefrom.AddDays(2);
                                                }
                                                else if (bal == 1)
                                                {
                                                    futDate = datefrom.AddDays(2);
                                                }
                                            }
                                            else if (wkname.ToString().ToUpper() == "MONDAY")
                                            {
                                                if (bal == 2)
                                                {
                                                    PastDate = datefrom.AddDays(-2);
                                                    futDate = datefrom.AddDays(-1);
                                                }
                                                else if (bal == 1)
                                                {
                                                    futDate = datefrom.AddDays(-2);
                                                }
                                            }
                                            else
                                            {
                                                PastDate = dateto.AddDays(-1);
                                                futDate = dateto.AddDays(-2);
                                            }
                                        }


                                        if (bal == 2)
                                        {
                                            strsql = "select Convert(varchar(10),dateoffice,103) 'dt' from tbltimeregister where dateoffice between '" + PastDate.ToString("yyyy-MM-dd") + "' and '" + futDate.ToString("yyyy-MM-dd") + "' and status='WO' and shiftAttended='OFF' and SSN ='" + SSN.ToString().Trim() + "' ";
                                            dswo = cn.FillDataSet(strsql);
                                            if (dswo.Tables[0].Rows.Count > 0)
                                            {
                                                LWPfrom = DateTime.ParseExact(dswo.Tables[0].Rows[0]["dt"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                LWPTo = DateTime.ParseExact(dswo.Tables[0].Rows[1]["dt"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                strsql = "update tbltimeregister set status='LWP',LeaveCode='LWP',absentvalue=1,wo_value=0,leaveamount=1,leavetype='A' where dateoffice between '" + LWPfrom.ToString("yyyy-MM-dd") + "' and '" + LWPTo.ToString("yyyy-MM-dd") + "' and SSN ='" + SSN.ToString().Trim() + "' ";
                                                cn.execute_NonQuery(strsql);
                                            }
                                        }
                                        if (bal == 1)
                                        {
                                            //strsql = "select Convert(varchar(10),dateoffice,103) 'dt' from tbltimeregister where dateoffice between '" + futDate.ToString("yyyy-MM-dd") + "' and '" + dateto.ToString("yyyy-MM-dd") + "' and status='WO' and shiftAttended='OFF' and PAYCODE ='" + pRow["Paycode"].ToString() + "' ";
                                            strsql = "select Convert(varchar(10),dateoffice,103) 'dt' from tbltimeregister where dateoffice = '" + futDate.ToString("yyyy-MM-dd") + "'  and status='WO' and shiftAttended='OFF' and SSN ='" + SSN.ToString().Trim() + "' ";
                                            dswo = cn.FillDataSet(strsql);
                                            if (dswo.Tables[0].Rows.Count > 0)
                                            {
                                                LWPfrom = DateTime.ParseExact(dswo.Tables[0].Rows[0]["dt"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                strsql = "update tbltimeregister set status='LWP',LeaveCode='LWP',absentvalue=1,wo_value=0,leaveamount=1,leavetype='A' where dateoffice = '" + LWPfrom.ToString("yyyy-MM-dd") + "'  and SSN ='" + SSN.ToString().Trim() + "' ";
                                                cn.execute_NonQuery(strsql);
                                            }
                                        }
                                    }

                                    while (dateto >= datefrom)
                                    {
                                        valid = "Y";
                                        strsql = "select isnull(max(voucherno),0) voucherno from tblvoucherno where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                                        //comm.CommandText = strsql;
                                        dr = cn.Execute_Reader(strsql);
                                        if (dr.Read())
                                        {
                                            voucher = int.Parse(dr["voucherno"].ToString()) + 1;
                                            mvoucher = voucher.ToString("0000000000");
                                        }
                                        else
                                        {
                                            mvoucher = "0000000001";
                                        }
                                        dr.Close();
                                        strsql = "update tblvoucherno set voucherno='" + mvoucher + "' where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                                        cn.execute_NonQuery(strsql);
                                        Trap_Error("butapproved_Click", "12");

                                        strsql = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                                        reader1 = cn.Execute_Reader(strsql);
                                        Trap_Error("butapproved_Click", "13");

                                        if (reader1.Read())
                                        {
                                            lvtype_new = reader1[6].ToString().Trim();
                                            isoff = reader1["ISOFFINCLUDE"].ToString().Trim();
                                            ishld = reader1["ISHOLIDAYINCLUDE"].ToString().Trim();
                                            lvtype = reader1["LeaveType"].ToString().Trim();
                                        }
                                        reader1.Close();

                                        strsql = "select * from holiday where hdate='" + datefrom.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + Upaycode.ToString().Trim() + "') " +
                                            "and departmentcode=(select departmentcode from tblemployee where paycode='" + Upaycode.ToString().Trim() + "')";
                                        reader2 = cn.Execute_Reader(strsql);
                                        if (reader2.Read())
                                        {
                                            if (ishld.ToString().Trim() == "Y")
                                                valid = "Y";
                                            else
                                                valid = "N";
                                        }
                                        reader2.Close();

                                        strsql = "Select * From tblTimeRegister  Where SSN = '" + SSN.ToString().Trim() + "'" + " And DateOffice='" + datefrom.ToString("yyyy-MM-dd") + "'  ";
                                        reader1 = cn.Execute_Reader(strsql);
                                        if (reader1.Read())
                                        {
                                            if (reader1["shift"].ToString().Trim() == "OFF")
                                            {
                                                if (isoff == "Y")
                                                {
                                                    valid = "Y";
                                                }
                                                else
                                                {
                                                    valid = "N";
                                                }
                                            }
                                        }
                                        reader1.Close();

                                        Trap_Error("butapproved_Click", "14");
                                        string firsthalflvcode = "";
                                        string Secondhalflvcode = "";

                                        if (hday.ToString().Trim() == "N")
                                        {
                                            lvamount = 1;
                                            lvamount1 = 0;
                                            lvamount2 = 0;
                                            lvcode = lvcode.ToString().Trim();
                                            Secondhalflvcode = "";
                                            firsthalflvcode = "";
                                        }
                                        else if (hday.ToString().Trim() == "F")
                                        {
                                            lvamount = 0.5;
                                            lvamount1 = 0.5;
                                            lvamount2 = 0;
                                            firsthalflvcode = lvcode.ToString();
                                            lvcode = "H_" + lvcode.ToString().Trim();
                                            Secondhalflvcode = "";

                                        }
                                        else
                                        {
                                            lvamount = 0.5;
                                            lvamount1 = 0;
                                            lvamount2 = 0.5;
                                            Secondhalflvcode = lvcode.ToString();
                                            lvcode = "H_" + lvcode.ToString().Trim();
                                            firsthalflvcode = "";
                                        }
                                        if (valid == "Y")
                                        {
                                            if (lvtype.ToString().Trim().ToUpper() == "L")
                                            {
                                                str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavevalue=1,absentvalue=0,wo_value=0,leavetype='L' " +
                                                    ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                                      " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate=getdate()," +
                                                      " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "',firsthalfleavecode='" + firsthalflvcode.ToString() + "',Secondhalfleavecode='" + Secondhalflvcode.ToString() + "' where SSN='" + SSN.ToString().Trim() + "' and dateoffice='" + datefrom.ToString("yyyy-MM-dd") + "' ";
                                            }
                                            else if (lvtype.ToString().Trim().ToUpper() == "P")
                                            {
                                                str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=1,absentvalue=0,wo_value=0,leavetype='P' " +
                                                  ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                                      " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate=getdate()," +
                                                      " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "',firsthalfleavecode='" + firsthalflvcode.ToString() + "',Secondhalfleavecode='" + Secondhalflvcode.ToString() + "' where SSN='" + SSN.ToString().Trim() + "' and dateoffice='" + datefrom.ToString("yyyy-MM-dd") + "' ";
                                            }
                                            else if (lvtype.ToString().Trim().ToUpper() == "A")
                                            {
                                                str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=0,absentvalue=1,wo_value=0,leavetype='A' " +
                                                    ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                                      " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate=getdate()," +
                                                      " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "',firsthalfleavecode='" + firsthalflvcode.ToString() + "',Secondhalfleavecode='" + Secondhalflvcode.ToString() + "' where SSN='" + SSN.ToString().Trim() + "' and dateoffice='" + datefrom.ToString("yyyy-MM-dd") + "' ";
                                            }
                                            else
                                            {
                                                datefrom = datefrom.AddDays(1);
                                                continue;
                                            }
                                            //Response.Write(str.ToString());
                                            cn.execute_NonQuery(str);
                                        }
                                        datefrom = datefrom.AddDays(1);
                                    }
                                }
                            }
                        }
                   
                }
        }
            Trap_Error("butapproved_Click", "***End 1 ***");
            fill_grid();
        }
        catch (Exception er)
        {
            Error_Occured("leaveapproval", er.Message);
        }
        finally
        {
            fill_grid();
        }
    }
    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            string headid = "";
            string Empname = "";
            string code = "", Dur = "", LEAVEDESCRIPTION = "", LeaveField = "";
            string HeadId = "", headName = "", FinalDur = "", lvName = "";

            List<object> keys = DataGrid1.GetSelectedFieldValues(new string[] { DataGrid1.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Any Application To Approve/Reject')</script>");
                return;
            }
            bool res;

            ViewState["Approval_Status"] = "Rejected";
             for (int DR = 0; DR < DataGrid1.VisibleRowCount; DR++)
            {
                if (DataGrid1.Selection.IsRowSelected(DR) == true)
                {
                   
                    finalLevel = false;

                    headEmail = "";
                    empEmail = "";
                    Pcode = "";


                    vno = DataGrid1.GetRowValues(DR, "application_no").ToString().Trim();
                    ViewState["app"] = vno.ToString();
                    strsql = "select LeaveCode from Leave_Request where Application_No= '" + vno + "' ";
                    LTDr = cn.Execute_Reader(strsql);
                    if (LTDr.Read())
                    {
                        ViewState["LeaveCode"] = LTDr[0].ToString().Trim();
                    }
                    LTDr.Close();
                    remarks = "NA";
                    if (remarks.Contains("'"))
                    {
                        remarks = remarks.ToString().Replace("'", "''");
                    }
                    RejectedRemarks = "NR";
                    Pcode = DataGrid1.GetRowValues(DR, "paycode").ToString().Trim();
                    txtfromdate = DataGrid1.GetRowValues(DR, "leave_from").ToString().Trim();
                    txttodate = DataGrid1.GetRowValues(DR, "leave_to").ToString().Trim();
                    Duration = DataGrid1.GetRowValues(DR, "Duration").ToString().Trim();
                    Reason = DataGrid1.GetRowValues(DR, "remarks").ToString().Trim();
                   

                    res = cn.chkvalid(RejectedRemarks.ToString().Trim());
                    if (!res)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('You have enter some invalid inputs.')", true);
                        return;
                    }

                    OleDbDataReader dr;

                    strsql = "select headid,e_mail1,empname from tblemployee where paycode='" + Pcode.ToString().Trim() + "'";
                    OleDbCommand comm1 = new OleDbCommand(strsql, conn);
                    OleDbDataReader dr1 = comm1.ExecuteReader();
                    if (dr1.Read())
                    {
                        headid = dr1[0].ToString().Trim();
                        empEmail = dr1[1].ToString().Trim();
                        Empname = dr1[2].ToString().Trim();
                    }
                    dr1.Close();


                    //Head Email
                    if (headid.ToString().Trim() != "")
                    {
                        strsql = "select e_mail1 from tblemployee where paycode='" + headid.ToString().Trim() + "'";
                        dr1 = cn.Execute_Reader(strsql);
                        if (dr1.Read())
                        {
                            headEmail = dr1[0].ToString().Trim();
                        }
                        if (headid.ToString().Trim().ToUpper() == Session["PAYCODE"].ToString().Trim().ToUpper())
                        {
                            finalLevel = false;
                        }
                        else
                        {
                            finalLevel = true;
                        }
                        dr1.Close();
                    }


                    //FIND LEAVE APPROVAL STAGES 
                    strsql = "Select LeaveApprovalStages from TblEmployee where PayCode = '" + Pcode.ToString() + "'";
                    comm1 = new OleDbCommand(strsql, conn);
                    dr1 = comm1.ExecuteReader();
                    dr1.Read();
                    if (dr1[0].ToString() == "1")
                        finalLevel = true;
                    dr1.Close();

                    if (finalLevel)
                    {
                        //check leave approval on Level 1
                        strsql = "select Stage1_Approved from leave_request where PayCode = '" + Pcode.ToString() + "' And application_no='" + vno + "'";
                        comm1 = new OleDbCommand(strsql, conn);
                        dr1 = comm1.ExecuteReader();
                        dr1.Read();
                        if (dr1.IsDBNull(0))
                            // reject on both levels 
                            strsql = "Update leave_request set Stage1_approved='N',Stage1_approval_Remarks='" + remarks + "',stage1_approval_id='" + Session["PAYCODE"].ToString() + "',stage1_approval_date=getdate() , Stage2_approved='N',Stage2_approval_remarks='" + remarks + "',stage2_approval_id='" + Session["PAYCODE"].ToString() + "',stage2_approval_date=getdate(), chklbl1='R' where application_no='" + vno + "'";
                        else
                            strsql = "Update leave_request set Stage2_approved='N', Stage2_approval_remarks='" + remarks + "', stage2_approval_id='" + Session["PAYCODE"].ToString() + "',stage2_approval_date=getdate(), chklbl1='R' where application_no='" + vno + "'";

                        dr1.Close();
                        cn.execute_NonQuery(strsql);

                        ViewState["FinalLevel"] = "True";
                        if (empEmail.ToString().Trim() != "")
                        {
                            try
                            {
                                Send_MailToApplicant(Pcode, empEmail, Empname);
                            }
                            catch (Exception er)
                            {
                                Error_Occured("Send mail part 1", er.Message);
                            }
                        }
                        //SendMail.SendEMail(empEmail, "Stage1 Leave approved ", "Stage1 Leave approved");
                        // *** END COMMENT
                    }
                    else
                    {
                        // if (headEmail != string.Empty)
                        //     Send_MailToHead();
                        //SendMail.SendEMail(headEmail, "Leave application for approval ", "Leave application for approval ");
                        ViewState["FinalLevel"] = "False";
                        if (empEmail.ToString().Trim() != "")
                        {
                            try
                            {
                                Send_MailToApplicant(Pcode, empEmail, Empname);
                            }
                            catch (Exception er)
                            {
                                Error_Occured("Send mail part 1", er.Message);
                            }
                        }
                        //SendMail.SendEMail(empEmail, "Stage1 Leave approved ", "Stage1 Leave approved");

                        strsql = "Update leave_request set Stage1_approved='N',Stage1_approval_Remarks='" + remarks + "',stage1_approval_id='" + Session["PAYCODE"].ToString() + "',stage1_approval_date=getdate() , Stage2_approved='N',Stage2_approval_remarks='" + remarks + "',stage2_approval_id='" + Session["PAYCODE"].ToString() + "',stage2_approval_date=getdate(), chklbl1='R' where application_no='" + vno + "'";
                        //strsql = "update leave_request set chklbl1='R',Stage1_approved='N',Stage1_approval_remarks='" + remarks + "' where application_no='" + vno + "'";
                        //Set_MatterStage1(); 
                        cn.execute_NonQuery(strsql);
                    }
                    

                    strsql = "select LeaveField from tblleavemaster where leavecode = ( select LeaveCode from Leave_Request where paycode ='" + Pcode.ToString() + "' and rtrim(ltrim(Application_No))='" + vno.ToString().Trim() + "' )";
                    comm = new OleDbCommand(strsql, conn);
                    dr = comm.ExecuteReader();
                    dr.Read();
                    LeaveField = dr[0].ToString();
                    dr.Close();


                    dt_FinancialYear = date1; //Convert.ToDateTime(txtfromdate.ToString().Trim());
                    //Trap_Error("Reject_Click", "6");
                    if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
                    {
                        if (dt_FinancialYear.Month == 1 || dt_FinancialYear.Month == 2 || dt_FinancialYear.Month == 3)
                        {
                            dt_FinancialYear = dt_FinancialYear.AddYears(-1);
                        }
                    }
                    //Trap_Error("Reject_Click", "7");
                    string year = dt_FinancialYear.Year.ToString().Trim();
                    //Trap_Error("Reject_Click", "8");
                    strsql = " update TBLLEAVELEDGER SET " + LeaveField.ToString() + " =  " + LeaveField.ToString() + "  -  " + Duration.ToString() + "   ";
                    strsql += " where  paycode='" + Pcode.ToString() + "' And Lyear='" + year + "'";
                    //Trap_Error("Reject_Click", strsql );
                    comm = new OleDbCommand(strsql, conn);
                    comm.ExecuteNonQuery();


                    //TblMailContent
                    if (ConfigurationManager.AppSettings["MailByDownloader"].ToString() == "Y")
                    {
                        HeadId = ""; headName = ""; FinalDur = "";
                        strsql = "select empname,E_mail1,headid from tblemployee where paycode ='" + Pcode.ToString() + "' ";
                        dr = cn.Execute_Reader(strsql);
                        string Stage1EmpName = "", FromMail = "";
                        if (dr.Read())
                        {
                            HeadId = dr[2].ToString().Trim();
                            Stage1EmpName = dr[0].ToString().Trim();
                            FromMail = dr["E_Mail1"].ToString().Trim();
                        }
                        dr.Close();
                        if (Session["usertype"].ToString().ToUpper() == "A")
                        {
                            headName = "by ADMIN";
                        }
                        else
                        {
                            strsql = "select empname from tblemployee where paycode='" + HeadId.ToString().ToUpper() + "'";
                            dr = cn.Execute_Reader(strsql);
                            if (dr.Read())
                            {
                                headName = "by your reporting manager " + dr[0].ToString().Trim();
                            }
                        }
                        if (Dur.ToString().Trim() == "N")
                        {
                            FinalDur = "Full Day";
                        }
                        else if (Dur.ToString().Trim() == "F")
                        {
                            FinalDur = "First Half";
                        }
                        else
                        {
                            FinalDur = "Second Half";
                        }
                        if (remarks.ToString().Trim() == "")
                        {
                            remarks = "Not Available";
                        }
                        if (FromMail.ToString().Trim() != "")
                        {
                            strsql = "insert into tblMailContent(AppNo,paycode,empname,FromMail,FromText,FromDate,ToDate,LeaveCode,LvStatus,Remarks,LvDuration,IsCalled,code) values ('" + vno.ToString() + "','" + Pcode.ToString() + "','" + Stage1EmpName.ToString() + "','" + FromMail.ToString() + "','" + headName.ToString() + "','" + date1.ToString("yyyy-MM-dd") + "','" + date2.ToString("yyyy-MM-dd") + "','" + LEAVEDESCRIPTION.ToString() + " ( " + lvName.ToString() + " - " + FinalDur.ToString() + "  )','R','" + remarks.ToString() + "','" + Dur.ToString().Trim() + "','N','" + lvName.ToString() + "')";
                            cn.execute_NonQuery(strsql);
                        }
                    }
                    //end


                    fill_grid();


                    // Trap_Error("Reject_Click", "9");
                }                                 
            }
        }
        catch (Exception er)
        {
            Error_Occured("Button2_Click", er.Message);
        }
        //Response.Redirect("frmLeaveApproval.aspx");  
    }
    protected void btncan_Click(object sender, EventArgs e)
    {
        if (Session["usertype"].ToString() == "A")
        {
            Response.Redirect("dashboard.aspx");
        }
        else
        {
            Response.Redirect("HomePage.aspx");
        }
    }
    private void Send_MailToApplicant(string pcode, string e_mail, string name)
    {
        Trap_Error("Send_MailToApplicant", "***Start***");
        try
        {

            string qry;
            OleDbDataReader dr;
            string EmpName = name.ToString();
            string message, txt = "";
            string headName = "";
            string hid = "";

            // Getting name of Stage1 employee
            qry = "select empname,E_mail1,headid from tblemployee where paycode ='" + Pcode.ToString() + "' ";
            dr = cn.Execute_Reader(qry);
            string Stage1EmpName = "", FromMail = "";

            Trap_Error("Send_MailToApplicant", "2");

            if (dr.Read())
            {
                hid = dr[2].ToString();
                Stage1EmpName = dr[0].ToString();
                FromMail = dr["E_Mail1"].ToString();
            }
            dr.Close();

            if (Session["usertype"].ToString().ToUpper() == "A")
            {    //FromMail = "info@admantechnologies.com";
                FromMail = ConfigurationManager.AppSettings["FromMail"].ToString();
                headName = "by ADMIN";
            }
            else
            {
                strsql = "select empname from tblemployee where paycode='" + hid.ToString().ToUpper() + "'";
                dr = cn.Execute_Reader(strsql);
                if (dr.Read())
                {
                    headName = "by your reporting supervisor " + dr[0].ToString();
                }
            }

            StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<table class='taglist' width='100%' cellpadding='0' cellspacing='0'>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            if (ViewState["FinalLevel"].ToString() == "True" && ViewState["Approval_Status"].ToString() == "Rejected")
            {
                sb.Append("<tr >");
                sb.Append("<td style='height:8px'></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
                sb.Append("Dear " + name.ToString() + " ,");
                sb.Append("</b></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='height:8px'></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
                sb.Append("Your Leave Request for " + ViewState["LeaveCode"].ToString() + " on " + txtfromdate.ToString() + " to " + txttodate.ToString() + " for " + Duration.ToString() + " Days has been rejected " + headName.ToString().Trim() + " reason  " + RejectedRemarks.ToString() + " .");
                sb.Append("</b></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='height:8px'></td>");
                sb.Append("</tr>");
            }
            if (ViewState["FinalLevel"].ToString() == "True" && ViewState["Approval_Status"].ToString() == "Approved")
            {

                sb.Append("<tr >");
                sb.Append("<td style='height:8px'></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
                sb.Append("Dear " + EmpName.ToString() + " ,");
                sb.Append("</b></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='height:8px'></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
                sb.Append("Your Leave Request for " + ViewState["LeaveCode"].ToString() + " on " + txtfromdate.ToString() + " to " + txttodate.ToString() + " for " + Duration.ToString() + " Days has been approved " + headName.ToString().Trim() + ".");
                sb.Append("</b></td>");
                sb.Append("</tr>");

                sb.Append("<tr >");
                sb.Append("<td style='height:8px'></td>");
                sb.Append("</tr>");
            }
            if (ViewState["FinalLevel"].ToString() == "False" && ViewState["Approval_Status"].ToString() == "Rejected")
            {
                sb.Append("<tr >");
                sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
                sb.Append("Your leave request has been " + ViewState["Approval_Status"].ToString() + " on Stage 1 by " + Stage1EmpName.ToString().Trim() + "(Paycode:" + Session["PAYCODE"].ToString().Trim() + ") ");
                sb.Append("and has been forwarded for Final Approval to the concerned person.");
                sb.Append("You will be intimated soon by mail...");
                sb.Append("</b></td>");
                sb.Append("</tr>");
            }

            sb.Append("<tr >");
            sb.Append("<td style='height:15px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr  >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            sb.Append("Best Regards,");
            sb.Append("</b></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");
            sb.Append("</table>");

            message = sb.ToString();
            SendMail.SendEMail(e_mail.ToString(), "Leave application Response From " + Session["PAYCODE"].ToString(), message.ToString());

        }
        catch (Exception er)
        {
            Error_Occured("Send_MailToApplicant", er.Message);
        }
    }

    private void Send_MailToHead()
    {
        try
        {
            Trap_Error("Send_MailToHead", "***Start***");

            string qry = "select empname from TblEmployee where paycode = '" + Pcode.ToString().Trim() + "'";
            if (conn.State == 0)
                conn.Open();

            OleDbCommand cmd = new OleDbCommand(qry, conn);
            OleDbDataReader dr;
            string EmpName = "";
            string message, txt;


            dr = cmd.ExecuteReader();
            if (dr.Read())
                EmpName = dr[0].ToString();
            dr.Close();

            // Getting name of Stage1 employee
            qry = "select empname,E_Mail1 from tblemployee where paycode ='" + Session["PAYCODE"].ToString() + "' ";
            dr = cn.Execute_Reader(qry);
            string Stage1EmpName = "", FromMail = "";
            if (dr.Read())
            {
                Stage1EmpName = dr[0].ToString();
                FromMail = dr["E_Mail1"].ToString();
            }
            dr.Close();

            string matter;


            StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<table class='taglist' width='100%' cellpadding='0' cellspacing='0'>");

            sb.Append("<tr>");
            sb.Append("<td colspan='2' align='center'><span style='font-family:Verdana;font-size:11px;color:Blue;'><b>Hi,</b></span></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            sb.Append("This is to intimate that Mr./Ms. " + EmpName.ToString().Trim() + "(Paycode:" + Pcode.ToString().Trim() + "), from your team has applied for Leave.");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");//dd
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            sb.Append("The leave(s) has/have been sanctioned on stage 1 by " + Stage1EmpName.ToString().Trim() + "(PayCode: " + Session["PAYCODE"].ToString().Trim() + " ) and now it has been forwarded to you for Final Approval/Rejection..");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");//dd
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            sb.Append("The Leave summary is as follows:- ");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;'><b>");
            sb.Append("Leave Type :");
            sb.Append("</b></td>");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;'><b>");
            sb.Append(ViewState["LeaveCode"].ToString());
            sb.Append("</b></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            sb.Append("Duration: " + txtfromdate.ToString() + " To " + txttodate.ToString());
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Sea Green;'><b>");
            sb.Append("VoucherNo :");
            sb.Append("</b></td>");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Sea Green;'><b>");
            sb.Append(" " + vno.ToString() + " ");
            sb.Append("</b></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;'><b>");
            sb.Append("Remarks :");
            sb.Append("</b></td>");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;'><b>");
            sb.Append(Reason.ToString());
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            string link = ConfigurationManager.AppSettings["EmailLink"].ToString();
            sb.Append("To approve/Reject the same please click on the link " + link.ToString() + ".");
            sb.Append("</b></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr  >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;' colspan='2'><b>");
            sb.Append("This is an auto generated mail and need not to be replied.");
            sb.Append("</b></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");


            sb.Append("</table>");

            message = sb.ToString();
            headEmail = Session["Headmail"].ToString();
            Trap_Error("Send_MailToHead", "3");
            SendMail.SendEMail(headEmail.ToString(), "Leave application for Final Approval/Rejection From " + Session["PAYCODE"].ToString(), message);

            Trap_Error("Send_MailToHead", "**End**");
        }
        catch (Exception er)
        {
            Error_Occured("Send_MailToHead", er.Message);
        }
    }
    protected void UpdateTimeRegister_Half()
    {
        string msg = "";
        DataSet dsDate = new DataSet();
        DateTime dateFrom = System.DateTime.MinValue;
        DateTime dateTo = System.DateTime.MinValue;
        string strsql = "";
        strsql = "select convert(varchar(10),dateadd(yy,datediff(yy,0,getdate()),0),103),convert(varchar(10),getdate(),103)";
        dsDate = cn.FillDataSet(strsql);
        if (dsDate.Tables[0].Rows.Count > 0)
        {
            try
            {
                dateFrom = DateTime.ParseExact(dsDate.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dateTo = DateTime.ParseExact(dsDate.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        else
            return;

        string str = "";
        string Upaycode = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        double avalue = 0;
        string st = "";
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string Pcode = "";
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        string StrSql = "";
        string StrCount = "";
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        double TimeLVAmount1 = 0;
        double TimeLVAmount2 = 0;
        double presntvalue = 0;
        double absentvalue = 0;
        double LeaveValue = 0;

        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103),l.leavecode, " +
                       " l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.presentvalue,t.absentvalue,T.LEAVEVALUE from tbltimeregister t join leave_request l 	on t.paycode=l.paycode and t.dateoffice between l.leave_from and l.leave_to " +
                        " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+l.leavecode+'%' and l.stage1_approved='Y'  " +
                        " and dateoffice between '" + dateFrom.ToString("yyyy-MM-dd") + "' and '" + dateTo.ToString("yyyy-MM-dd") + "'  " +
                        " and l.halfday!='N' order by t.paycode";

            ds = cn.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Upaycode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[i][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    TimeLVAmount1 = Convert.ToDouble(ds.Tables[0].Rows[i][8].ToString());
                    TimeLVAmount2 = Convert.ToDouble(ds.Tables[0].Rows[i][9].ToString());
                    presntvalue = Convert.ToDouble(ds.Tables[0].Rows[i][10].ToString());
                    absentvalue = Convert.ToDouble(ds.Tables[0].Rows[i][11].ToString());
                    LeaveValue = Convert.ToDouble(ds.Tables[0].Rows[i][12].ToString());

                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = cn.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    str = "";
                    //StrCount = "select * from leave_request where leave_from='" + doffice.ToString("yyyy-MM-dd") + "' and paycode='" + Upaycode.ToString().Trim() + "' and halfday!='N' ";
                    //dsCount = cn.FillDataSet(StrCount);
                    //if (dsCount.Tables[0].Rows.Count > 1)
                    //{
                    if (hday.ToString().Trim() == "F")
                    {
                        lvamount = 0.5;
                        lvamount1 = 0.5;
                        lvamount2 = 0;
                        lvcode = "H_" + lvcode.ToString().Trim();
                    }
                    else
                    {
                        lvamount = 0.5;
                        lvamount1 = 0;
                        lvamount2 = 0.5;
                        lvcode = "H_" + lvcode.ToString().Trim();
                    }
                    if (lvtype.ToString().Trim().ToUpper() == "L")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavevalue=leavevalue+0.5,absentvalue=absentvalue-0.5,leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavevalue=leavevalue+0.5,absentvalue=absentvalue-0.5,leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "P")
                    {

                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue+0.5,absentvalue=absentvalue-0.5,leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue+0.5,absentvalue=absentvalue-0.5,leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "A")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue-0.5,absentvalue=absentvalue+0.5,leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue-0.5,absentvalue=absentvalue+0.5,leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(str))
                    {
                        cn.execute_NonQuery(str);
                    }
                }
            }
            // }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    protected void UpdateTimeRegister()
    {
        string strsql = "";
        string msg = "";
        DataSet dsDate = new DataSet();
        DateTime dateFrom = System.DateTime.MinValue;
        DateTime dateTo = System.DateTime.MinValue;

        strsql = "select convert(varchar(10),dateadd(yy,datediff(yy,0,getdate()),0),103),convert(varchar(10),getdate(),103)";
        dsDate = cn.FillDataSet(strsql);
        if (dsDate.Tables[0].Rows.Count > 0)
        {
            try
            {
                dateFrom = DateTime.ParseExact(dsDate.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //dateFrom = DateTime.ParseExact("01/01/2013", "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dateTo = DateTime.ParseExact(dsDate.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        else
            return;

        string str = "";
        string Upaycode = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        double avalue = 0;
        string st = "";
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string Pcode = "";
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        string StrSql = "";
        string StrCount = "";
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();

        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103),l.leavecode, " +
                       " l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(l.stage1_approval_date,getdate()),103) from tbltimeregister t join leave_request l 	on t.paycode=l.paycode and t.dateoffice between l.leave_from and l.leave_to " +
                        " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+l.leavecode+'%' and l.stage1_approved='Y'  " +
                        " and dateoffice between '" + dateFrom.ToString("yyyy-MM-dd") + "' and '" + dateTo.ToString("yyyy-MM-dd") + "' 	 " +
                        " and l.halfday='N' order by l.leavecode";

            ds = cn.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Response.Write(ds.Tables[0].Rows.Count.ToString());
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Upaycode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    dtFrom = doffice;

                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = cn.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }

                    StrCount = "select * from leave_request where leave_from='" + doffice.ToString("yyyy-MM-dd") + "' and paycode='" + Upaycode.ToString().Trim() + "' and (stage1_approved!='N' or stage2_approved!='N') ";
                    dsCount = cn.FillDataSet(StrCount);
                    if (dsCount.Tables[0].Rows.Count > 1)
                    {
                        continue;
                    }

                    if (hday.ToString().Trim() == "N")
                    {
                        lvamount = 1;
                        lvamount1 = 0;
                        lvamount2 = 0;
                        lvcode = lvcode.ToString().Trim();
                    }
                    else if (hday.ToString().Trim() == "F")
                    {
                        lvamount = 0.5;
                        lvamount1 = 0.5;
                        lvamount2 = 0;
                        lvcode = "H_" + lvcode.ToString().Trim();
                    }
                    else
                    {
                        lvamount = 0.5;
                        lvamount1 = 0;
                        lvamount2 = 0.5;
                        lvcode = "H_" + lvcode.ToString().Trim();
                    }
                    if (lvtype.ToString().Trim().ToUpper() == "L")
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavevalue=1,absentvalue=0,leavetype='L' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavevalue=leavevalue+0.5,absentvalue=absentvalue-0.5,leavetype='L',leavetype1='L',leavetype2='',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "',secondhalfleavecode='' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',leavevalue=leavevalue+0.5,absentvalue=absentvalue-0.5,leavetype='L',leavetype2='L',leavetype1='',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "',firsthalfleavecode='' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "P")
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=1,absentvalue=0,leavetype='P' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue+0.5,absentvalue=absentvalue-0.5,leavetype='P',leavetype1='P',leavetype2='',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "',secondhalfleavecode='' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue+0.5,absentvalue=absentvalue-0.5,leavetype='P',leavetype2='P',leavetype1='',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "',firsthalfleavecode='' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "A")
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=0,absentvalue=1,leavetype='A' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue-0.5,absentvalue=absentvalue+0.5,leavetype='A',leavetype1='A',leavetype2='',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "',secondhalfleavecode='' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            str = "update tbltimeregister set Status='" + lvcode.ToString().Trim() + "',presentvalue=presentvalue-0.5,absentvalue=absentvalue+0.5,leavetype='A',leavetype2='A',leavetype1='',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "',firsthalfleavecode='' " +
                                ", leaveamount1=" + lvamount1 + ",leaveamount2=" + lvamount2 + " " +
                                  " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                  " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                    }
                    if (!string.IsNullOrEmpty(str))
                    {
                        cn.execute_NonQuery(str);
                    }
                }
            }
            else
            {
                Response.Write("No Record Found");
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void Trap_Error(string FunctionName, string Passed)
    {
        //        // FUNCTION TO TRAP ERRORS

        //        string pc=Session["PAYCODE"].ToString();
        //        int ErNo;
        //        string Emsg = "";
        //        DateTime Dt = System.DateTime.Now.Date;
        //        string PageName = "FrmLeaveApproval.aspx";
        //        string FilePath = Session["TrapFileName"].ToString();
        //        StringBuilder sBuilder = new StringBuilder();

        //        //if (!File.Exists(FilePath)) //If Not File Exists
        //        if (!File.Exists(FilePath)) //If Not File Exists
        //        {
        //            TextWriter tw = new StreamWriter(Session["TrapFileName"].ToString());
        //            //TextWriter tw = new StreamWriter(FilePath);
        //            tw.WriteLine("==================================================================================");
        //            tw.WriteLine("Error No.   Date\t\tPaycode\t\tSource\t\t\tFunction Name\tPassed   ");
        //            tw.WriteLine("==================================================================================");
        //            tw.Close();
        //        }

        //        ErNo = GenerateSrNumber(Session["TrapFileName"].ToString());

        //        //Open the File and Write the Error 

        //        FileStream Fs = File.Open(Session["TrapFileName"].ToString(), FileMode.Append);
        //        //FileStream Fs = File.Open(FilePath, FileMode.Append);
        //        StreamWriter Sw = new StreamWriter(Fs);


        //        //Emsg = " " + ErNo.ToString() + "\t" + System.DateTime.Today.ToShortDateString() + "\tFrmDefaultPageRegister1.aspx\tDivide Function";
        //        Emsg = " " + ErNo.ToString() + "\t" + Dt.ToShortDateString()+" "+ DateTime.Now.ToShortTimeString() + "\t" + pc.ToString() + "\t" + PageName.ToString() + " \t" + FunctionName.ToString() + "\t" + Passed.ToString();
        //        Sw.WriteLine(Emsg.ToString());
        //        Sw.WriteLine("-------------------------------------------------------------------------------------------");
        //        Sw.Close();     
    }
    private int GenerateSrNumber(string FilePath)
    {
        // Counting Records of the File 
        //StreamReader Sr = File.OpenText(Server.MapPath("ELMSLOG.TXT")); 

        StreamReader Sr = File.OpenText(FilePath);
        string TxtLine;
        int RecNo = -2;
        do
        {
            TxtLine = Sr.ReadLine();
            if (TxtLine != null && (TxtLine.ToString().Substring(0, 5)) != "-----")
                RecNo++;
        } while (TxtLine != null);

        Sr.Close();
        return RecNo;
    }
    protected void btnLeaveReport_Click(object sender, EventArgs e)
    {
        Response.Redirect("FrmLeaveSummary.aspx?id=" + null);
    }
}