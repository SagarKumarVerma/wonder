﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmEmployeeGroup.aspx.cs" Inherits="frmEmployeeGroup" %>


<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="HiddenFieldGrpId" runat="server" />
    <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }

    </script>  
    <script language="javascript" type="text/javascript" src="JS/validation_Employee.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <dx:ASPxCallbackPanel ID="ASPxCallbackPanel2" runat="server" ClientInstanceName="ASPxCallbackPanel2" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
            <dx:ASPxGridView ID="GrdGrp" runat="server" AutoGenerateColumns="False"
        Width="100%" KeyFieldName="GroupId" ClientInstanceName="GrdGrp" >
                <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                <ClientSideEvents RowDblClick="function(s, e) {
	document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value = GrdGrp.GetRowKey(e.visibleIndex);
//alert(document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value);          
pcLogin.Show();
}" />
                <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                <SettingsPager>
                    <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                </SettingsPager>
                <SettingsEditing Mode="PopupEditForm">
                </SettingsEditing>
                <Settings ShowFilterRow="True" ShowTitlePanel="True" HorizontalScrollBarMode="Visible" VerticalScrollBarStyle="VirtualSmooth" />
                <SettingsBehavior ConfirmDelete="True" />
                <SettingsCommandButton>
                    <ClearFilterButton Text=" ">
                        <Image IconID="filter_clearfilter_16x16">
                        </Image>
                    </ClearFilterButton>
                    <NewButton Text=" ">
                        <Image IconID="actions_add_16x16">
                        </Image>
                    </NewButton>
                    <UpdateButton ButtonType="Image" RenderMode="Image">
                        <Image IconID="save_save_16x16office2013">
                        </Image>
                    </UpdateButton>
                    <CancelButton ButtonType="Image" RenderMode="Image">
                        <Image IconID="actions_cancel_16x16office2013">
                        </Image>
                    </CancelButton>
                    <EditButton Text=" ">
                        <Image IconID="actions_edit_16x16devav">
                        </Image>
                    </EditButton>
                    <DeleteButton Text=" ">
                        <Image IconID="actions_trash_16x16">
                        </Image>
                    </DeleteButton>
                </SettingsCommandButton>
                <SettingsPopup>
                    <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter"
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                </SettingsPopup>
                  <SettingsSearchPanel Visible="True" />
                <SettingsText PopupEditFormCaption="Employee Group" Title="Employee Group" />
                <EditFormLayoutProperties ColCount="2">
                    <Items>
                        <dx:GridViewColumnLayoutItem ColumnName="GroupId">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="GroupName">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="Branch">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SHIFT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SHIFTTYPE">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SHIFTPATTERN">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SHIFTREMAINDAYS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="LASTSHIFTPERFORMED">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISAUTOSHIFT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="AUTH_SHIFTS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="FIRSTOFFDAY">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SECONDOFFTYPE">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="HALFDAYSHIFT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SECONDOFFDAY">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ALTERNATE_OFF_DAYS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="INONLY">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISPUNCHALL">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOUTWORK">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="TWO">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISTIMELOSSALLOWED">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="CDAYS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISROUNDTHECLOCKWORK">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OTRATE">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="PERMISLATEARRIVAL">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="PERMISEARLYDEPRT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="MAXDAYMIN">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="TIME">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SHORT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="HALF">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISHALFDAY">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISSHORT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="LateVerification">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="EveryInterval">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="DeductFrom">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="FromLeave">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="LateDays">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="DeductDay">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="MaxLateDur">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="PunchRequiredInDay">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="SinglePunchOnly">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="LastModifiedBy">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="LastModifiedDate">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="MARKMISSASHALFDAY">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="Id">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="PERMISLATEARR">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="PERMISEARLYDEP">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="DUPLICATECHECKMIN">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOVERSTAY">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="S_END">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="S_OUT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOTOUTMINUSSHIFTENDTIME">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOTWRKGHRSMINUSSHIFTHRS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOTEARLYCOMEPLUSLATEDEP">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOTALL">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISOTEARLYCOMING">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OTEARLYDUR">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OTLATECOMINGDUR">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OTRESTRICTENDDUR">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OTEARLYDEPARTUREDUR">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="DEDUCTWOOT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="DEDUCTHOLIDAYOT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISPRESENTONWOPRESENT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISPRESENTONHLDPRESENT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISAUTOABSENT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="AUTOSHIFT_LOW">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="AUTOSHIFT_UP">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="WOINCLUDE">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="NightShiftFourPunch">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OTROUND">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="PREWO">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISAWA">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISPREWO">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="LeaveFinancialYear">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="MIS">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="OwMinus">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="MSHIFT">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="MISCOMPOFF">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISCOMPOFF">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISAW">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISWA">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISAWP">
                        </dx:GridViewColumnLayoutItem>
                        <dx:GridViewColumnLayoutItem ColumnName="ISPWA">
                        </dx:GridViewColumnLayoutItem>
                        <dx:EditModeCommandLayoutItem ColSpan="2" HorizontalAlign="Right">
                        </dx:EditModeCommandLayoutItem>
                    </Items>
                </EditFormLayoutProperties>
                <Columns>
                    <dx:GridViewCommandColumn ShowNewButtonInHeader="True" VisibleIndex="0">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="GroupId" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="GroupName" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Branch" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SHIFT" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SHIFTTYPE" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SHIFTPATTERN" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SHIFTREMAINDAYS" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LASTSHIFTPERFORMED" VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISAUTOSHIFT" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="AUTH_SHIFTS" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FIRSTOFFDAY" VisibleIndex="11">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SECONDOFFTYPE" VisibleIndex="12">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="HALFDAYSHIFT" VisibleIndex="13">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SECONDOFFDAY" VisibleIndex="14">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ALTERNATE_OFF_DAYS" VisibleIndex="15">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="INONLY" VisibleIndex="16">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISPUNCHALL" VisibleIndex="17">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOUTWORK" VisibleIndex="18">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TWO" VisibleIndex="19">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISTIMELOSSALLOWED" VisibleIndex="20">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="CDAYS" VisibleIndex="21">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISROUNDTHECLOCKWORK" VisibleIndex="22">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOT" VisibleIndex="23">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OTRATE" VisibleIndex="24">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PERMISLATEARRIVAL" VisibleIndex="25">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PERMISEARLYDEPRT" VisibleIndex="26">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MAXDAYMIN" VisibleIndex="27">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOS" VisibleIndex="28">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TIME" VisibleIndex="29">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SHORT" VisibleIndex="30">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="HALF" VisibleIndex="31">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISHALFDAY" VisibleIndex="32">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISSHORT" VisibleIndex="33">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LateVerification" VisibleIndex="34">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EveryInterval" VisibleIndex="35">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DeductFrom" VisibleIndex="36">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FromLeave" VisibleIndex="37">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LateDays" VisibleIndex="38">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DeductDay" VisibleIndex="39">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MaxLateDur" VisibleIndex="40">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PunchRequiredInDay" VisibleIndex="41">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SinglePunchOnly" VisibleIndex="42">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastModifiedBy" VisibleIndex="43">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn FieldName="LastModifiedDate" VisibleIndex="44">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="MARKMISSASHALFDAY" VisibleIndex="45">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="47">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PERMISLATEARR" VisibleIndex="48">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PERMISEARLYDEP" VisibleIndex="49">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DUPLICATECHECKMIN" VisibleIndex="50">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOVERSTAY" VisibleIndex="51">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn FieldName="S_END" VisibleIndex="52">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataDateColumn FieldName="S_OUT" VisibleIndex="53">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOTOUTMINUSSHIFTENDTIME" VisibleIndex="54">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOTWRKGHRSMINUSSHIFTHRS" VisibleIndex="55">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOTEARLYCOMEPLUSLATEDEP" VisibleIndex="56">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOTALL" VisibleIndex="57">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISOTEARLYCOMING" VisibleIndex="58">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OTEARLYDUR" VisibleIndex="59">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OTLATECOMINGDUR" VisibleIndex="60">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OTRESTRICTENDDUR" VisibleIndex="61">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OTEARLYDEPARTUREDUR" VisibleIndex="62">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DEDUCTWOOT" VisibleIndex="63">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DEDUCTHOLIDAYOT" VisibleIndex="64">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISPRESENTONWOPRESENT" VisibleIndex="65">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISPRESENTONHLDPRESENT" VisibleIndex="66">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISAUTOABSENT" VisibleIndex="67">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="AUTOSHIFT_LOW" VisibleIndex="68">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="AUTOSHIFT_UP" VisibleIndex="69">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="WOINCLUDE" VisibleIndex="70">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="NightShiftFourPunch" VisibleIndex="71">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OTROUND" VisibleIndex="72">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PREWO" VisibleIndex="73">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISAWA" VisibleIndex="74">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISPREWO" VisibleIndex="75">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LeaveFinancialYear" VisibleIndex="76">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MIS" VisibleIndex="77">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="OwMinus" VisibleIndex="78">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MSHIFT" VisibleIndex="79">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MISCOMPOFF" VisibleIndex="80">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISCOMPOFF" VisibleIndex="81">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISAW" VisibleIndex="82">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISWA" VisibleIndex="83">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISAWP" VisibleIndex="84">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ISPWA" VisibleIndex="85">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Paddings Padding="0px" />
                <Border BorderWidth="0px" />
                <BorderBottom BorderWidth="1px" />
            </dx:ASPxGridView>
                </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Add">
                <ClientSideEvents Click="function(s, e) {
	document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value = '';
alert(document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value);          
pcLogin.Show();
}" />
            </dx:ASPxButton>
        </ContentTemplate>
    </asp:UpdatePanel>
    <dx:ASPxPopupControl ID="pcLogin" runat="server" AllowDragging="True" AllowResize="True" ClientInstanceName="pcLogin" CloseAction="CloseButton" CloseAnimationType="Fade" CloseOnEscape="True" ContentUrl="~/GroupEdit.aspx" EnableViewState="False" HeaderText="Employee Group" Height="700px" Modal="True" PopupAnimationType="Fade" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Middle" ScrollBars="Auto" Target="_parent" Theme="SoftOrange" Width="850px">
        <ClientSideEvents Closing="function(s, e) {
                            document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value='';
	window.location = &quot;frmEmployeeGroup.aspx&quot;;
}" />
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT * FROM [EmployeeGroup] ORDER BY [GroupId]"></asp:SqlDataSource>


</asp:Content>

