﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Selection.ascx.cs" Inherits="UserControl_Selection" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../Stylesheets/style.css" type="text/css" rel="stylesheet" />
<table cellpadding="0" cellspacing="0" width="720px">
<tr>
<td colspan="2" style="width:720" align="left">
<asp:UpdatePanel ID="update" runat="server"  >
<ContentTemplate>
<cc1:TabContainer runat="server" ID="Tabs"  Height="158px"  ActiveTabIndex="0" Width="720px" >
            <cc1:TabPanel runat="server" ID="Panel1" HeaderText="Company">
                <ContentTemplate>   
                <table cellpadding="0" cellspacing="0" style="width:700px">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Unselected</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Selected</td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px; height: 10px;">
                            &nbsp;</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px">
                                       </td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                <tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="listComleft" runat="server"  SelectionMode="Multiple"  Width="250px" Height="90px" CssClass="text_ListBox" AppendDataBoundItems="True" ></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">                                               
                <asp:button ID="Button3" text=">" OnClick="AddBtn_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/><br />
                <asp:button ID="Button4" text=">>" OnClick="AddAllBtn_Click" runat="server" Width="80px" CssClass="buttoncss"/>                               
                <asp:button ID="Button2" text="<" OnClick="RemoveBtn_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/> <br />
                <asp:button ID="Button1" text="<<" OnClick="RemoveAllBtn_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/><br />
                </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="listComRight" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>                            
                </ContentTemplate>
            </cc1:TabPanel>
            
                 <cc1:TabPanel runat="server" ID="TabPanel3" HeaderText="Employee">
                <ContentTemplate>   
                <table cellpadding="0" cellspacing="0" style="width:700px;">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td align="left" colspan="5">
                            <asp:RadioButtonList ID="radSelectType" runat="server" CssClass="Radiocss" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">Name</asp:ListItem>
                                <asp:ListItem>Paycode</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px; height: 10px;">
                            <asp:TextBox ID="txtSearch" runat="server" Width="160px"></asp:TextBox>
                            &nbsp;&nbsp;<asp:Button ID="cmdSearch" runat="server" CssClass="buttoncss" OnClick="cmdSearch_Click"
                                Text="Search" />
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px">
                                       </td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                <tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="listEmployeeLeft" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox"></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">                                               
                <asp:button ID="Button11" text=">" OnClick="AddEmp_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/><br />
                <asp:button ID="Button12" text=">>" OnClick="AddAllEmp_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/>                               
                <asp:button ID="Button10" text="<" OnClick="RemoveEmp_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/> <br />
                <asp:button ID="Button9" text="<<" OnClick="RemoveAllEmp_Click" runat="server" Width="80px" CausesValidation="False" CssClass="buttoncss"/><br />
                </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="listEmployeeRight" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>                            
                </ContentTemplate>
            </cc1:TabPanel>
            
                     <cc1:TabPanel runat="server" ID="TabPanel2" HeaderText="Department" >
                <ContentTemplate>
                           <table cellpadding="0" cellspacing="0" style="width:700px">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Unselected</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Selected</td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                               <tr>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td style="width: 120px" align="center">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px; height: 10px;">
                                       </td>
                                   <td style="width: 20px">
                                   </td>
                               </tr>
                <tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="listDeptleft" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox" CausesValidation="false"></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">               
                <asp:button ID="Button7" text=">" OnClick="AddBtn_Click1" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/><br />
                <asp:button ID="Button8" text=">>" OnClick="AddAllBtn_Click1" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/>                
                <asp:button ID="Button6" text="<" OnClick="RemoveBtn_Click1" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/> <br />
                <asp:button ID="Button5" text="<<" OnClick="RemoveAllBtn_Click1" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false" /><br />
                 </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="listDeptRight" runat="server" SelectionMode="Multiple" Width="250px" CausesValidation="false" Height="90px" CssClass="text_ListBox"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>
                </ContentTemplate>
            </cc1:TabPanel>
            
                   <cc1:TabPanel runat="server" ID="TabPanel4" HeaderText="Catagory" >
                <ContentTemplate>
                           <table cellpadding="0" cellspacing="0" style="width:700px">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Unselected</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Selected</td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                               <tr>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td style="width: 120px" align="center">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px; height: 10px;">
                                       </td>
                                   <td style="width: 20px">
                                   </td>
                               </tr>
                <tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="ListCatagoryLeft" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox" CausesValidation="false"></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">               
                <asp:button ID="Button15" text=">" OnClick="AddCat_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/><br />
                <asp:button ID="Button16" text=">>" OnClick="AddAllCat_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/>                
                <asp:button ID="Button14" text="<" OnClick="RemoveCat_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/> <br />
                <asp:button ID="Button13" text="<<" OnClick="RemoveAllCat_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/><br />
                 </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="ListCatagoryRight" runat="server" SelectionMode="Multiple" Width="250px" CausesValidation="false" Height="90px" CssClass="text_ListBox"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>
                </ContentTemplate>
            </cc1:TabPanel>
            
                   <cc1:TabPanel runat="server" ID="TabPanel5" HeaderText="Shift" >
                <ContentTemplate>
                           <table cellpadding="0" cellspacing="0" style="width:700px">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Unselected</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Selected</td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                               <tr>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td style="width: 120px" align="center">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px; height: 10px;">
                                       </td>
                                   <td style="width: 20px">
                                   </td>
                               </tr>
                <tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="listShiftLeft" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox" CausesValidation="false"></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">               
                <asp:button ID="Button19" text=">" OnClick="AddShift_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/><br />
                <asp:button ID="Button20" text=">>" OnClick="AddAllShift_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/>                
                <asp:button ID="Button18" text="<" OnClick="RemoveShift_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/> <br />
                <asp:button ID="Button17" text="<<" OnClick="RemoveAllShift_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/><br />
                 </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="listShiftRight" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CausesValidation="false" CssClass="text_ListBox"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>
                </ContentTemplate>
            </cc1:TabPanel>
            
                  <cc1:TabPanel runat="server" ID="TabPanel6" HeaderText="Section" >
                <ContentTemplate>
                           <table cellpadding="0" cellspacing="0" style="width:700px">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Unselected</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px" align="center">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Selected</td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                               <tr>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td style="width: 120px" align="center">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px; height: 10px;">
                                       </td>
                                   <td style="width: 20px">
                                   </td>
                               </tr>
                <tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="listDivisionLeft" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox" CausesValidation="false"></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">               
                <asp:button ID="Button23" text=">" OnClick="AddDivision_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false" /><br />
                <asp:button ID="Button24" text=">>" OnClick="AddAllDivision_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/>                
                <asp:button ID="Button22" text="<" OnClick="RemoveDivision_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/> <br />
                <asp:button ID="Button21" text="<<" OnClick="RemoveAllDivision_Click" runat="server" Width="80px" CssClass="buttoncss" CausesValidation="false"/><br />
                 </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="listDivisionRight" runat="server" SelectionMode="Multiple" Width="250px" CausesValidation="false" Height="90px" CssClass="text_ListBox"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>
                </ContentTemplate>
            </cc1:TabPanel>
            
              <cc1:TabPanel runat="server" ID="TabPanel7" >
                <ContentTemplate>
                           <table cellpadding="0" cellspacing="0" style="width:700px">
                    <tr>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Unselected</td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 120px">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td style="width: 250px" align="center">
                            Selected</td>
                        <td style="width: 20px">
                        </td>
                    </tr>
                               <tr>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td style="width: 120px">
                                   </td>
                                   <td style="width: 20px">
                                   </td>
                                   <td align="center" style="width: 250px; height: 10px;"></td>
                                   <td style="width: 20px">
                                   </td>
                               </tr>	
				<tr>
                <td style="width: 20px"></td>
                <td style="width: 250px"  valign="top"  ><asp:ListBox ID="listGradeLeft" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox" CausesValidation="false"></asp:ListBox> </td>
                <td style="width: 20px"></td>                
                <td style="width: 120px" valign="top" align="center">               
                <asp:button ID="Button27" text=">" OnClick="AddGrade_Click" runat="server" Width="80px" CausesValidation="false" CssClass="buttoncss"/><br />
                <asp:button ID="Button28" text=">>" OnClick="AddAllGrade_Click" runat="server" Width="80px" CausesValidation="false" CssClass="buttoncss"/>                
                <asp:button ID="Button26" text="<" OnClick="RemoveGrade_Click" runat="server" Width="80px" CausesValidation="false" CssClass="buttoncss"/> <br />
                <asp:button ID="Button25" text="<<" OnClick="RemoveAllGrade_Click" runat="server" Width="80px" CausesValidation="false" CssClass="buttoncss"/><br />
                 </td>
                <td style="width: 20px"></td>
                <td style="width: 250px" valign="top"> <asp:ListBox ID="listGradeRight" runat="server" SelectionMode="Multiple" Width="250px" Height="90px" CssClass="text_ListBox" CausesValidation="false"></asp:ListBox></td>
                <td style="width: 20px"></td>
                </tr>
                </table>
                </ContentTemplate>
            </cc1:TabPanel>            
        </cc1:TabContainer>
</ContentTemplate>
</asp:UpdatePanel>
</td>
</tr>
<tr>
<td style="width: 300px">    
</td>
<td style="width: 420px">
    <asp:Button ID="cmdOk" runat="server" Width="80px" Text="OK" OnClick="cmdOk_Click" CssClass="buttoncss" CausesValidation="false" />&nbsp;
    <asp:Button ID="cmdCancel" runat="server" OnClick="cmdCancel_Click" Text="Cancel" Width="80px" CssClass="buttoncss" CausesValidation="false" /></td>    
</tr>
</table>