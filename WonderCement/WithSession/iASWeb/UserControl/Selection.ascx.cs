﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControl_Selection : System.Web.UI.UserControl
{
    Class_Connection con = new Class_Connection();
    string datefrom = null;
    string dateTo = null;
    string strsql = null;
    string Strsql = null;
    DataSet ds = null;

    string Company = null;
    string Employee = null;
    string Dept = null;
    string category = null;
    string Shift = null;
    string Division = null;
    string Grade = null;

    ArrayList lasset1 = new ArrayList();
    ArrayList lsubordinate1 = new ArrayList();
    static ArrayList UpdateList1 = new ArrayList();

    ArrayList lasset = new ArrayList();
    ArrayList lsubordinate = new ArrayList();
    static ArrayList UpdateList = new ArrayList();
    DataSet dsEmpCount = new DataSet();
    string ReportView = "", EmpCodes = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindCompany();
            bindDept();
            bindEmployee();
            bindCatagory();
            bindShift();
            bindDivision();
            bindGrade();
            Session["SelectionQuery"] = null;
            bindFields();
            if ((Session["PAYCODE"].ToString().ToString().ToUpper().Trim() == "ADMIN") && (Session["usertype"].ToString().ToString().ToUpper() == "A"))
            {
                Button4.Enabled = false;
            }
        }
    }
    protected void bindFields()
    {
        Strsql = "select * from tblMapping";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            Panel1.HeaderText = ds.Tables[0].Rows[4]["Showas"].ToString().Trim();
            TabPanel2.HeaderText = ds.Tables[0].Rows[5]["Showas"].ToString().Trim();
            TabPanel4.HeaderText = ds.Tables[0].Rows[6]["Showas"].ToString().Trim();
            TabPanel6.HeaderText = ds.Tables[0].Rows[7]["Showas"].ToString().Trim();
            TabPanel7.HeaderText = ds.Tables[0].Rows[8]["Showas"].ToString().Trim();
            TabPanel3.HeaderText = ds.Tables[0].Rows[11]["Showas"].ToString().Trim();
        }
    }
    protected void bindCompany()
    {
        Strsql = "Select companycode,companycode+'-'+companyname as 'compDetails' from tblCompany";
        if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
        {
            Strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ")";
        }
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listComleft.DataSource = ds.Tables[0];
            listComleft.DataValueField = "companycode";
            listComleft.DataTextField = "compDetails";
            listComleft.DataBind();
        }
    }
    protected void bindDept()
    {
        Strsql = "select departmentcode,departmentcode+' - '+departmentname as 'deptDetails' from tbldepartment";
        if ((Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
        {
            Strsql += " where Departmentcode in (" + Session["Auth_Dept"].ToString() + ")";
        }
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listDeptleft.DataSource = ds.Tables[0];
            listDeptleft.DataValueField = "DepartmentCode";
            listDeptleft.DataTextField = "deptDetails";
            listDeptleft.DataBind();
        }
    }
    protected void bindEmployee()
    {

        Strsql = "";
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        if (Session["usertype"].ToString().Trim().ToUpper() == "H")
        {

            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null and headid='" + Session["PAYCODE"].ToString().Trim() + "' ";
                dsEmpCount = con.FillDataSet(Strsql);
                if (dsEmpCount.Tables[0].Rows.Count > 0)
                {
                    for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                    {
                        EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                    }
                    if (!string.IsNullOrEmpty(EmpCodes))
                    {
                        EmpCodes = EmpCodes.Substring(1);
                    }
                    Strsql += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                }
            }
            else
            {
                Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null ";
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                }
            }
        }
        else
        {
            Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null";

            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString() + ")";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString() + ")";
            }
        }

        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listEmployeeLeft.DataSource = ds.Tables[0];
            listEmployeeLeft.DataValueField = "paycode";
            listEmployeeLeft.DataTextField = "EmpDetails";
            listEmployeeLeft.DataBind();
        }
    }
    protected void bindCatagory()
    {
        Strsql = "select cat,cat+' - '+Catagoryname as 'CatDetails' from tblcatagory";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ListCatagoryLeft.DataSource = ds.Tables[0];
            ListCatagoryLeft.DataValueField = "cat";
            ListCatagoryLeft.DataTextField = "CatDetails";
            ListCatagoryLeft.DataBind();
        }
    }
    protected void bindShift()
    {
        Strsql = "select Shift,Shift +'-'+ convert(char(5),starttime, 108) +'-'+ convert(char(5),endtime, 108) 'ShiftTime' from  TblShiftMaster";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listShiftLeft.DataSource = ds.Tables[0];
            listShiftLeft.DataValueField = "Shift";
            listShiftLeft.DataTextField = "ShiftTime";
            listShiftLeft.DataBind();
        }
    }
    protected void bindDivision()
    {
        Strsql = "select divisioncode,divisioncode+'-'+divisionname 'DivisionDetails' from tbldivision";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listDivisionLeft.DataSource = ds.Tables[0];
            listDivisionLeft.DataValueField = "divisioncode";
            listDivisionLeft.DataTextField = "DivisionDetails";
            listDivisionLeft.DataBind();
        }
    }
    protected void bindGrade()
    {
        Strsql = "select GradeCode,gradecode +'-'+ gradename 'GradeDetails'  from tblGrade";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listGradeLeft.DataSource = ds.Tables[0];
            listGradeLeft.DataValueField = "GradeCode";
            listGradeLeft.DataTextField = "GradeDetails";
            listGradeLeft.DataBind();
        }
    }

    //Company Area

    public void AddBtn_Click(Object Src, EventArgs E)
    {

        /*if ((Session["PAYCODE"].ToString().ToString().ToUpper().Trim() == "ADMIN") && (Session["usertype"].ToString().ToString().ToUpper() == "A"))
        {
            if (listComRight.Items.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('You cannot select more than one company.');", true);
                return;
            }                
        }*/


        if (listComleft.SelectedIndex >= 0)
        {
            for (int i = 0; i < listComleft.Items.Count; i++)
            {
                if (listComleft.Items[i].Selected)
                {
                    if (!lasset.Contains(listComleft.Items[i]))
                    {
                        lasset.Add(listComleft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listComRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listComRight.Items.Add(((ListItem)lasset[i]));
                } listComleft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }
    public void AddAllBtn_Click(Object Src, EventArgs E)
    {
        /*if ((Session["PAYCODE"].ToString().ToString().ToUpper().Trim() == "ADMIN") && (Session["usertype"].ToString().ToString().ToUpper() == "A"))
        {
            if (listComRight.Items.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('You cannot select more than one company.');", true);
                return;
            }
        }*/

        while (listComleft.Items.Count != 0)
        {
            for (int i = 0; i < listComleft.Items.Count; i++)
            {
                if (!lasset.Contains(listComleft.Items[i]))
                {
                    lasset.Add(listComleft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listComRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listComRight.Items.Add(((ListItem)lasset[i]));
                }
                listComleft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }
    public void RemoveBtn_Click(Object Src, EventArgs E)
    {
        if (listComRight.SelectedItem != null)
        {
            for (int i = 0; i < listComRight.Items.Count; i++)
            {
                if (listComRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(listComRight.Items[i]))
                    {
                        lsubordinate.Add(listComRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listComleft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listComleft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listComRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }
    public void RemoveAllBtn_Click(Object Src, EventArgs E)
    {
        while (listComRight.Items.Count != 0)
        {
            for (int i = 0; i < listComRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(listComRight.Items[i]))
                {
                    lsubordinate.Add(listComRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listComleft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listComleft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listComRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }


    //Employee Area

    public void AddEmp_Click(Object Src, EventArgs E)
    {
        if (listEmployeeLeft.SelectedIndex >= 0)
        {
            for (int i = 0; i < listEmployeeLeft.Items.Count; i++)
            {
                if (listEmployeeLeft.Items[i].Selected)
                {
                    if (!lasset1.Contains(listEmployeeLeft.Items[i]))
                    {
                        lasset.Add(listEmployeeLeft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listEmployeeRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listEmployeeRight.Items.Add(((ListItem)lasset[i]));
                } listEmployeeLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void AddAllEmp_Click(Object Src, EventArgs E)
    {
        while (listEmployeeLeft.Items.Count != 0)
        {
            for (int i = 0; i < listEmployeeLeft.Items.Count; i++)
            {
                if (!lasset.Contains(listEmployeeLeft.Items[i]))
                {
                    lasset.Add(listEmployeeLeft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listEmployeeRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listEmployeeRight.Items.Add(((ListItem)lasset[i]));
                }
                listEmployeeLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }


    }

    public void RemoveEmp_Click(Object Src, EventArgs E)
    {
        if (listEmployeeRight.SelectedItem != null)
        {
            for (int i = 0; i < listEmployeeRight.Items.Count; i++)
            {
                if (listEmployeeRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(listEmployeeRight.Items[i]))
                    {
                        lsubordinate.Add(listEmployeeRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listEmployeeLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listEmployeeLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listEmployeeRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    public void RemoveAllEmp_Click(Object Src, EventArgs E)
    {
        while (listEmployeeRight.Items.Count != 0)
        {
            for (int i = 0; i < listEmployeeRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(listEmployeeRight.Items[i]))
                {
                    lsubordinate.Add(listEmployeeRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listEmployeeLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listEmployeeLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listEmployeeRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    //Department Area

    public void AddBtn_Click1(Object Src, EventArgs E)
    {
        if (listDeptleft.SelectedIndex >= 0)
        {
            for (int i = 0; i < listDeptleft.Items.Count; i++)
            {
                if (listDeptleft.Items[i].Selected)
                {
                    if (!lasset1.Contains(listDeptleft.Items[i]))
                    {
                        lasset.Add(listDeptleft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listDeptRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listDeptRight.Items.Add(((ListItem)lasset[i]));
                } listDeptleft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void AddAllBtn_Click1(Object Src, EventArgs E)
    {
        while (listDeptleft.Items.Count != 0)
        {
            for (int i = 0; i < listDeptleft.Items.Count; i++)
            {
                if (!lasset.Contains(listDeptleft.Items[i]))
                {
                    lasset.Add(listDeptleft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listDeptRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listDeptRight.Items.Add(((ListItem)lasset[i]));
                }
                listDeptleft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void RemoveBtn_Click1(Object Src, EventArgs E)
    {
        if (listDeptRight.SelectedItem != null)
        {
            for (int i = 0; i < listDeptRight.Items.Count; i++)
            {
                if (listDeptRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(listDeptRight.Items[i]))
                    {
                        lsubordinate.Add(listDeptRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listDeptleft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listDeptleft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listDeptRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    public void RemoveAllBtn_Click1(Object Src, EventArgs E)
    {
        while (listDeptRight.Items.Count != 0)
        {
            for (int i = 0; i < listDeptRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(listDeptRight.Items[i]))
                {
                    lsubordinate.Add(listDeptRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listDeptleft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listDeptleft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listDeptRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    //Catagory  listEmployeeLeft  listEmployeeRight ListCatagoryLeft  ListCatagoryRight
    public void AddCat_Click(Object Src, EventArgs E)
    {
        if (ListCatagoryLeft.SelectedIndex >= 0)
        {
            for (int i = 0; i < ListCatagoryLeft.Items.Count; i++)
            {
                if (ListCatagoryLeft.Items[i].Selected)
                {
                    if (!lasset1.Contains(ListCatagoryLeft.Items[i]))
                    {
                        lasset.Add(ListCatagoryLeft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!ListCatagoryRight.Items.Contains(((ListItem)lasset[i])))
                {
                    ListCatagoryRight.Items.Add(((ListItem)lasset[i]));
                } ListCatagoryLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void AddAllCat_Click(Object Src, EventArgs E)
    {
        while (ListCatagoryLeft.Items.Count != 0)
        {
            for (int i = 0; i < ListCatagoryLeft.Items.Count; i++)
            {
                if (!lasset.Contains(ListCatagoryLeft.Items[i]))
                {
                    lasset.Add(ListCatagoryLeft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!ListCatagoryRight.Items.Contains(((ListItem)lasset[i])))
                {
                    ListCatagoryRight.Items.Add(((ListItem)lasset[i]));
                }
                ListCatagoryLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void RemoveCat_Click(Object Src, EventArgs E)
    {
        if (ListCatagoryRight.SelectedItem != null)
        {
            for (int i = 0; i < ListCatagoryRight.Items.Count; i++)
            {
                if (ListCatagoryRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(ListCatagoryRight.Items[i]))
                    {
                        lsubordinate.Add(ListCatagoryRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!ListCatagoryLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    ListCatagoryLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                ListCatagoryRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    public void RemoveAllCat_Click(Object Src, EventArgs E)
    {
        while (ListCatagoryRight.Items.Count != 0)
        {
            for (int i = 0; i < ListCatagoryRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(ListCatagoryRight.Items[i]))
                {
                    lsubordinate.Add(ListCatagoryRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!ListCatagoryLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    ListCatagoryLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                ListCatagoryRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }


    //Shift
    public void AddShift_Click(Object Src, EventArgs E)
    {
        if (listShiftLeft.SelectedIndex >= 0)
        {
            for (int i = 0; i < listShiftLeft.Items.Count; i++)
            {
                if (listShiftLeft.Items[i].Selected)
                {
                    if (!lasset1.Contains(listShiftLeft.Items[i]))
                    {
                        lasset.Add(listShiftLeft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listShiftRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listShiftRight.Items.Add(((ListItem)lasset[i]));
                } listShiftLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }
    public void AddAllShift_Click(Object Src, EventArgs E)
    {
        while (listShiftLeft.Items.Count != 0)
        {
            for (int i = 0; i < listShiftLeft.Items.Count; i++)
            {
                if (!lasset.Contains(listShiftLeft.Items[i]))
                {
                    lasset.Add(listShiftLeft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listShiftRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listShiftRight.Items.Add(((ListItem)lasset[i]));
                }
                listShiftLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void RemoveShift_Click(Object Src, EventArgs E)
    {
        if (listShiftRight.SelectedItem != null)
        {
            for (int i = 0; i < listShiftRight.Items.Count; i++)
            {
                if (listShiftRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(listShiftRight.Items[i]))
                    {
                        lsubordinate.Add(listShiftRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listShiftLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listShiftLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listShiftRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    public void RemoveAllShift_Click(Object Src, EventArgs E)
    {
        while (listShiftRight.Items.Count != 0)
        {
            for (int i = 0; i < listShiftRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(listShiftRight.Items[i]))
                {
                    lsubordinate.Add(listShiftRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listShiftLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listShiftLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listShiftRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }


    //Section
    public void AddDivision_Click(Object Src, EventArgs E)
    {
        if (listDivisionLeft.SelectedIndex >= 0)
        {
            for (int i = 0; i < listDivisionLeft.Items.Count; i++)
            {
                if (listDivisionLeft.Items[i].Selected)
                {
                    if (!lasset1.Contains(listDivisionLeft.Items[i]))
                    {
                        lasset.Add(listDivisionLeft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listDivisionRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listDivisionRight.Items.Add(((ListItem)lasset[i]));
                } listDivisionLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void AddAllDivision_Click(Object Src, EventArgs E)
    {
        while (listDivisionLeft.Items.Count != 0)
        {
            for (int i = 0; i < listDivisionLeft.Items.Count; i++)
            {
                if (!lasset.Contains(listDivisionLeft.Items[i]))
                {
                    lasset.Add(listDivisionLeft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listDivisionRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listDivisionRight.Items.Add(((ListItem)lasset[i]));
                }
                listDivisionLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }
    public void RemoveDivision_Click(Object Src, EventArgs E)
    {
        if (listDivisionRight.SelectedItem != null)
        {
            for (int i = 0; i < listDivisionRight.Items.Count; i++)
            {
                if (listDivisionRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(listDivisionRight.Items[i]))
                    {
                        lsubordinate.Add(listDivisionRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listDivisionLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listDivisionLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listDivisionRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }
    public void RemoveAllDivision_Click(Object Src, EventArgs E)
    {
        while (listDivisionRight.Items.Count != 0)
        {
            for (int i = 0; i < listDivisionRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(listDivisionRight.Items[i]))
                {
                    lsubordinate.Add(listDivisionRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listDivisionLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listDivisionLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listDivisionRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }

    //Grade
    public void AddGrade_Click(Object Src, EventArgs E)
    {
        if (listGradeLeft.SelectedIndex >= 0)
        {
            for (int i = 0; i < listGradeLeft.Items.Count; i++)
            {
                if (listGradeLeft.Items[i].Selected)
                {
                    if (!lasset1.Contains(listGradeLeft.Items[i]))
                    {
                        lasset.Add(listGradeLeft.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listGradeRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listGradeRight.Items.Add(((ListItem)lasset[i]));
                } listGradeLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }

    public void AddAllGrade_Click(Object Src, EventArgs E)
    {
        while (listGradeLeft.Items.Count != 0)
        {
            for (int i = 0; i < listGradeLeft.Items.Count; i++)
            {
                if (!lasset.Contains(listGradeLeft.Items[i]))
                {
                    lasset.Add(listGradeLeft.Items[i]);
                }
            }
            for (int i = 0; i < lasset.Count; i++)
            {
                if (!listGradeRight.Items.Contains(((ListItem)lasset[i])))
                {
                    listGradeRight.Items.Add(((ListItem)lasset[i]));
                }
                listGradeLeft.Items.Remove(((ListItem)lasset[i]));
            }
        }
    }
    public void RemoveGrade_Click(Object Src, EventArgs E)
    {
        if (listGradeRight.SelectedItem != null)
        {
            for (int i = 0; i < listGradeRight.Items.Count; i++)
            {
                if (listGradeRight.Items[i].Selected)
                {
                    if (!lsubordinate.Contains(listGradeRight.Items[i]))
                    {
                        lsubordinate.Add(listGradeRight.Items[i]);
                    }
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listGradeLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listGradeLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listGradeRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }
    public void RemoveAllGrade_Click(Object Src, EventArgs E)
    {
        while (listGradeRight.Items.Count != 0)
        {
            for (int i = 0; i < listGradeRight.Items.Count; i++)
            {
                if (!lsubordinate.Contains(listGradeRight.Items[i]))
                {
                    lsubordinate.Add(listGradeRight.Items[i]);
                }
            }
            for (int i = 0; i < lsubordinate.Count; i++)
            {
                if (!listGradeLeft.Items.Contains(((ListItem)lsubordinate[i])))
                {
                    listGradeLeft.Items.Add(((ListItem)lsubordinate[i]));
                }
                listGradeRight.Items.Remove(((ListItem)lsubordinate[i]));
                UpdateList.Add(lsubordinate[i]);
            }
        }
    }
    protected void cmdOk_Click(object sender, EventArgs e)
    {
        if (listComRight.Items.Count > 0)
        {
            for (int i = 0; i < listComRight.Items.Count; i++)
            {
                Company += ",'" + listComRight.Items[i].Value.ToString() + "'";
            }
            Company = Company.Substring(1).Trim();
            Session["Com_Selection"] = Company.ToString();
        }
        else
        {
            Company = "";
        }
        if (listEmployeeRight.Items.Count > 0)
        {
            for (int i1 = 0; i1 < listEmployeeRight.Items.Count; i1++)
            {
                Employee += ",'" + listEmployeeRight.Items[i1].Value.ToString().Trim() + "'";
            }
            Employee = Employee.Substring(1).Trim();
            Session["Emp_Codes"] = Employee.ToString();
        }
        else
        {
            Employee = "";
        }
        if (listDeptRight.Items.Count > 0)
        {
            for (int i2 = 0; i2 < listDeptRight.Items.Count; i2++)
            {
                Dept += ",'" + listDeptRight.Items[i2].Value.ToString() + "'";
            }
            Dept = Dept.Substring(1).Trim();
            Session["Dept_Selection"] = Dept.ToString();
        }
        else
        {
            Dept = "";
        }

        if (ListCatagoryRight.Items.Count > 0)
        {
            for (int i3 = 0; i3 < ListCatagoryRight.Items.Count; i3++)
            {
                category += ",'" + ListCatagoryRight.Items[i3].Value.ToString() + "'";
            }
            category = category.Substring(1).Trim();
        }
        else
        {
            category = "";
        }
        if (listShiftRight.Items.Count > 0)
        {
            for (int i4 = 0; i4 < listShiftRight.Items.Count; i4++)
            {
                Shift += ",'" + listShiftRight.Items[i4].Value.ToString() + "'";
            }
            Shift = Shift.Substring(1).Trim();
        }
        else
        {
            Shift = "";
        }
        if (listDivisionRight.Items.Count > 0)
        {
            for (int i5 = 0; i5 < listDivisionRight.Items.Count; i5++)
            {
                Division += ",'" + listDivisionRight.Items[i5].Value.ToString() + "'";
            }
            Division = Division.Substring(1).Trim();
        }
        else
        {
            Division = "";
        }
        if (listGradeRight.Items.Count > 0)
        {
            for (int i6 = 0; i6 < listGradeRight.Items.Count; i6++)
            {
                Grade += ",'" + listGradeRight.Items[i6].Value.ToString() + "'";
            }
            Grade = Grade.Substring(1).Trim();
        }
        else
        {
            Grade = "";
        }

        strsql = " and 1=1 ";

        if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN") && Company.ToString().Trim() == "")
        {
            Company = Session["Auth_Comp"].ToString().Trim();
        }
        if ((Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN") && Dept.ToString().Trim() == "")
        {
            Dept = Session["Auth_Dept"].ToString().Trim();
        }

        if (Company != "")
        {
            strsql += "and tblemployee.Companycode in (" + Company.ToString().Trim() + ") ";
        }
        if (Employee != "")
        {
            strsql += "and tblemployee.paycode in (" + Employee.ToString().Trim() + ")  ";
        }
        if (Dept != "")
        {
            strsql += "and tblemployee.DepartmentCode in (" + Dept.ToString().Trim() + ")  ";
        }
        if (category != "")
        {
            strsql += "and tblemployee.Cat in (" + category.ToString().Trim() + ")  ";
        }
        if (Shift != "")
        {
            strsql += "and tblemployeeshiftmaster.shift in (" + Shift.ToString().Trim() + ")  ";
        }
        if (Division != "")
        {
            strsql += "and tblemployee.Divisioncode in (" + Division.ToString().Trim() + ")  ";
        }
        if (Grade != "")
        {
            strsql += "and tblemployee.Gradecode in (" + Grade.ToString().Trim() + ")";
        }

        //Strsql = "select tblemployee.paycode from tblemployee join tblemployeeshiftmaster  on tblemployeeshiftmaster.paycode=tblemployee.paycode " +
        //"where " + strsql.ToString() + "";

        Session["SelectionQuery"] = strsql;
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Session["SelectionQuery"] = "";
        Session["SelectionQuery"] = null;
        Session["Emp_Codes"] = "";
        Session["Emp_Codes"] = null;
        Session["Com_Selection"] = "";
        Session["Com_Selection"] = null;
        Session["Dept_Selection"] = "";
        Session["Dept_Selection"] = null;
        listComRight.Items.Clear();
        listDeptRight.Items.Clear();
        listDivisionRight.Items.Clear();
        listEmployeeRight.Items.Clear();
        listGradeRight.Items.Clear();
        listShiftRight.Items.Clear();
        ListCatagoryRight.Items.Clear();

        listComleft.Items.Clear();
        listDeptleft.Items.Clear();
        listDivisionLeft.Items.Clear();
        listEmployeeLeft.Items.Clear();
        listGradeLeft.Items.Clear();
        listShiftLeft.Items.Clear();
        ListCatagoryLeft.Items.Clear();

        bindCompany();
        bindDept();
        bindEmployee();
        bindCatagory();
        bindShift();
        bindDivision();
        bindGrade();
        lasset1.Clear();
        lsubordinate1.Clear();
        UpdateList1.Clear();
        lasset.Clear();
        lsubordinate.Clear();
        UpdateList.Clear();



        //Response.CacheControl = "No-Cache";

    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        if (Session["usertype"].ToString().Trim().ToUpper() == "H")
        {
            //Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null and headid='" + Session["PAYCODE"].ToString().Trim() + "' ";
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null and headid='" + Session["PAYCODE"].ToString().Trim() + "' ";
                dsEmpCount = con.FillDataSet(Strsql);
                if (dsEmpCount.Tables[0].Rows.Count > 0)
                {
                    for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                    {
                        EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                    }
                    if (!string.IsNullOrEmpty(EmpCodes))
                    {
                        EmpCodes = EmpCodes.Substring(1);
                    }
                    Strsql += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                }
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql = " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                }
            }
        }
        else if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN") && (Session["usertype"].ToString().Trim().ToUpper() == "A"))
        {
            Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ")";
        }
        else
        {
            Strsql = "select paycode,Empname+' - '+paycode as 'EmpDetails' from tblEmployee where active='Y' and LeavingDate is null";
        }
        if (!string.IsNullOrEmpty(txtSearch.Text.ToString().Trim()))
        {
            if (radSelectType.SelectedIndex == 0)
            {
                Strsql += " and empname like '" + txtSearch.Text.ToString().Trim() + "%' ";
            }
            else
            {
                Strsql += " and Paycode like '" + txtSearch.Text.ToString().Trim() + "%' ";
            }
        }
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            listEmployeeLeft.DataSource = ds.Tables[0];
            listEmployeeLeft.DataValueField = "paycode";
            listEmployeeLeft.DataTextField = "EmpDetails";
            listEmployeeLeft.DataBind();
        }
    }
}