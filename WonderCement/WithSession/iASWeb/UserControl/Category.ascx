﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Category.ascx.cs" Inherits="UserControl_Category" %>


          <dx:ASPxGridView ID="GridView" ClientInstanceName="lineitemgrid"
              DataSourceID="SqlCat" KeyFieldName="CAT" 
              runat="server" Width="100%" AutoGenerateColumns="False">
           <Columns>
            <dx:GridViewDataTextColumn FieldName="CATAGORYNAME" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CAT" VisibleIndex="1" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="LateVerification" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="EveryInterval" VisibleIndex="3">
               </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="DeductFrom" VisibleIndex="4">
               </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="FromLeave" VisibleIndex="5">
               </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="FromLeave1" VisibleIndex="6">
               </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="LateDays" VisibleIndex="7">
               </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="DeductDay" VisibleIndex="8">
               </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn FieldName="MaxLateDur" VisibleIndex="9">
               </dx:GridViewDataTextColumn>
           </Columns> 
              <%--<ClientSideEvents CustomButtonClick="OnCustomButtonClick" />   --%>       
          </dx:ASPxGridView>

        
<asp:SqlDataSource ID="SqlCat" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT [CATAGORYNAME], [CAT], [LateVerification], [EveryInterval], [DeductFrom], [FromLeave], [FromLeave1], [LateDays], [DeductDay], [MaxLateDur] FROM [tblCatagory] ORDER BY [CAT]"></asp:SqlDataSource>


        
