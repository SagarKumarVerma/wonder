﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="FrmReportLeave.aspx.cs" Inherits="FrmReportLeave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
       <div align="center" style="vertical-align:top" > 
               <dx:ASPxPanel ID="ASPxPanel2" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
 
<table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td ></td>
</tr>
<tr>
<td colspan="3" class="tableHeaderCss" align="center" style="height:25px; width: 100%;">
 <asp:Label ID="lblMsg" runat="server" Text="Leave Report" CssClass="lblCss" ></asp:Label>  </td>
</tr>
    <tr>
<td >

    <table cellspacing="1" style="border-style: solid; border-width: thin; padding: 5px; width: 100%;">
        <tr style="padding: 5px; border-style: solid; border-width: thin; padding: 5px; width: 100%; height: 19px; text-align: left;">
            <td style="padding: 5px">
                <dx:ASPxLabel ID="lblfrom" runat="server" Text="From">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxDateEdit ID="txtDateFrom" runat="server" Width="120px" EditFormatString="dd/MM/yyyy" OnCalendarDayCellPrepared="txtToDate_CalendarDayCellPrepared" AutoPostBack="True" OnDateChanged="txtDateFrom_DateChanged">
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="lblto" runat="server" Text="To Date" Visible="false" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxDateEdit ID="txtToDate" runat="server" Width="120px" EditFormatString="dd/MM/yyyy" Visible="false" OnCalendarDayCellPrepared="txtToDate_CalendarDayCellPrepared" >
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 5px">
                <dx:ASPxButton ID="btnGenerate" runat="server" Text="Generate" OnClick="btnGenerate_Click" >
                </dx:ASPxButton>
            </td>
            <td style="padding: 5px">
                <dx:ASPxRadioButton ID="radExcel" runat="server" GroupName="A" Checked="true" Text="Excel">
                </dx:ASPxRadioButton>
            </td>
            <td style="padding: 5px">
                <dx:ASPxRadioButton ID="radPDF" runat="server" GroupName="A" Text="PDF">
                </dx:ASPxRadioButton>
            </td>
            
        </tr>
        <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Select Company">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                   <dx:ASPxGridLookup ID="LookupCompany" runat="server" SelectionMode="Multiple"  ClientInstanceName="gridLookup"
                                                                            KeyFieldName="CompanyCode"  TextFormatString="{0}" MultiTextSeparator=",">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px" SelectAllCheckboxMode="AllPages" />
                                                                                <dx:GridViewDataColumn FieldName="CompanyCode" Caption="Code" Width="50px" />
                                                                                <dx:GridViewDataColumn FieldName="CompanyCode" Settings-AllowAutoFilter="False" Caption="Name">
<Settings AllowAutoFilter="False"></Settings>
                                                                                </dx:GridViewDataColumn>
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                                                                                <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                                            </GridViewProperties>
                                                                            <ClearButton DisplayMode="Always">
                                                                            </ClearButton>
                                                                        </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="selDep" runat="server" Text="Select Department" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookUpDept" runat="server" ClientInstanceName="GDep" KeyFieldName="DepartmentCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="DepartmentCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="departmentname" Settings-AllowAutoFilter="False" >
<Settings AllowAutoFilter="False"></Settings>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Select Section">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                 <dx:ASPxGridLookup ID="lookupSec" runat="server" ClientInstanceName="lookupSec" KeyFieldName="DivisionCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages"/>
                        <dx:GridViewDataColumn Caption="Code" FieldName="DivisionCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="DivisionName" Settings-AllowAutoFilter="False" >
<Settings AllowAutoFilter="False"></Settings>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                     <ClearButton DisplayMode="Always">
                     </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Select Grade" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                  <dx:ASPxGridLookup ID="lookupGrade" runat="server" ClientInstanceName="lookupGrade" KeyFieldName="GradeCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="GradeCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="GradeName" Settings-AllowAutoFilter="False" >
<Settings AllowAutoFilter="False"></Settings>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                      <ClearButton DisplayMode="Always">
                      </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Select Category">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="lookupCat" runat="server" ClientInstanceName="lookupCat" KeyFieldName="CAT" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages"/>
                        <dx:GridViewDataColumn Caption="Code" FieldName="CAT" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="CATAGORYNAME" Settings-AllowAutoFilter="False" >
<Settings AllowAutoFilter="False"></Settings>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Select Leave" Visible="False">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookUpShift" runat="server" ClientInstanceName="LookUpShift" KeyFieldName="Shift" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}" Visible="False">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages"/>
                        <dx:GridViewDataColumn Caption="Code" FieldName="Shift" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="ShiftTime" Settings-AllowAutoFilter="False" >
<Settings AllowAutoFilter="False"></Settings>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
        </tr>
          <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Select Employee">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookEmp" runat="server" ClientInstanceName="LookEmp" KeyFieldName="PAYCODE" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="PAYCODE" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="EMPNAME" Settings-AllowAutoFilter="False" >
<Settings AllowAutoFilter="False"></Settings>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="lblYear" runat="server" Text="Leave Year">
                </dx:ASPxLabel>
              </td>
            <td style="padding: 5px">
                
                <dx:ASPxComboBox ID="ddlYear" runat="server">
                </dx:ASPxComboBox>
                
            </td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 5px" colspan="8">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
   <table style="padding: 5px; width: 100%;">
                    <tr>
                         <td style="padding: 5px; width:15% ;">
                             <dx:ASPxRadioButton ID="radSencLeave" AutoPostBack="true" Text="Senctioned Leave" Checked="True" GroupName="a" OnCheckedChanged="fire"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:15% ;">
                             <dx:ASPxRadioButton ID="radLeaveCard" AutoPostBack="true"  Text="Leave Card"  GroupName="a" OnCheckedChanged="fire"    runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:15% ;">
                             <dx:ASPxRadioButton ID="radAccuredLeave" AutoPostBack="true" Text="Accrued Leave"   GroupName="a" OnCheckedChanged="fire"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                        <td style="padding: 5px; width:32% ;">
                           
                         </td>
                         <td style="padding: 5px; width:33% ;"></td>
                    </tr>
                             <tr>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radConsumedLeave" AutoPostBack="true"  Text="Consumed Leave"   GroupName="a" OnCheckedChanged="fire"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radBalanceLeave" AutoPostBack="true" Text="Balance Leave"   GroupName="a"  OnCheckedChanged="fire"  runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radLeaveRegister" AutoPostBack="true"  Text="Leave Register"  GroupName="a" OnCheckedChanged="fire"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                    </tr>
                             <tr>
                         <td style="padding: 5px">
                             &nbsp;</td>
                         <td style="padding: 5px">
                             &nbsp;</td>
                         <td style="padding: 5px">
                             &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                    </tr>
                      <tr>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                    </tr>
                      <tr>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                    </tr>
                      <tr>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                    </tr>
        <tr>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                    </tr>
    
         <tr id="Tday" runat="server" visible="false">
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                            &nbsp;</td>
                        <td style="padding: 5px">
                           
                            &nbsp;</td>
                    </tr>
                  
                </table>
                        </dx:PanelContent>
</PanelCollection>
                   
                    <Border BorderStyle="Solid" BorderWidth="1px" />
                   
 </dx:ASPxPanel>
                
                   
                </td>
        </tr>
    </table>

</td>
</tr>
    </table>
    </dx:PanelContent>
    </PanelCollection>
                   </dx:ASPxPanel>
        </div>
</asp:Content>

