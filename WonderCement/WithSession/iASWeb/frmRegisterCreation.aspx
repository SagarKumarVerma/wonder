﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmRegisterCreation.aspx.cs" Inherits="frmRegisterCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td style="text-align:center">Attendance Register Creation</td>
</tr>
<tr>
<td align="center" colspan="6" class="tableHeaderCss" style="height:25px;">
    &nbsp;</td>
</tr>
     
<tr>
    <td align="center">
        <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />
            <Items>

                <dx:EmptyLayoutItem />
                <dx:LayoutGroup Caption="Company & Date Selection" ColCount="1" Width="200px">
                    <Items>
                        <dx:LayoutItem Caption="Select Company">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxComboBox ID="ddlCompany" runat="server" ValueType="System.String" Width="200px" Theme="SoftOrange"></dx:ASPxComboBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>

                        <dx:LayoutItem Caption="From Date">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxDateEdit ID="TxtFromDate" runat="server" Width="200px" Theme="SoftOrange" EditFormatString="dd/MM/yyyy" OnCalendarDayCellPrepared="TxtFromDate_CalendarDayCellPrepared" />
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    </Items>
                </dx:LayoutGroup>
                <dx:LayoutGroup Caption="Record Selection" ColCount="1" Width="200px">
                    <Items>
                        <dx:LayoutItem Caption="Selection Mode">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxRadioButtonList ID="RadSal" runat="server"
                                        ValueField="ID" TextField="Name" RepeatColumns="2" RepeatLayout="Flow" AutoPostBack="True" Width="200px" OnSelectedIndexChanged="RadSal_SelectedIndexChanged">
                                        <Items>
                                            <dx:ListEditItem Text="All" Value="All" Selected="true" />
                                            <dx:ListEditItem Text="Selective" Value="Selective" />
                                        </Items>
                                        <CaptionSettings Position="Top" />
                                    </dx:ASPxRadioButtonList>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Select Employee">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxComboBox ID="ddlPaycode" runat="server" ValueType="System.String" Enabled="false" Width="200px"  SelectedIndex="-1"   Theme="SoftOrange" NullText="NONE">

                                    </dx:ASPxComboBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>

                    </Items>
                </dx:LayoutGroup>
                <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Right" Width="100%" HorizontalAlign="Center">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                            <dx:ASPxButton ID="btnCreate" runat="server" Text="Create" Width="100" OnClick="btnCreate_Click" />
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>

                    <CaptionSettings HorizontalAlign="Right"></CaptionSettings>
                </dx:LayoutItem>
            </Items>
        </dx:ASPxFormLayout>
    </td>
    </tr>
        </table>
</asp:Content>

