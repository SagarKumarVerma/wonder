﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmDepartment.aspx.cs" Inherits="frmLocation" %>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }
        function DeleteCall(s) {
            PageMethods.DetelGrid(s,returenValue);
        }
        function returenValue(returnText){
            lblStatus.SetText(returnText); 
        }
    </script>  
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <%-- DXCOMMENT: Configure ASPxGridView control --%>
    <dx:ASPxLabel ID="lblerror" runat="server" Text="" ForeColor="Red" Visible="false">
    </dx:ASPxLabel>
    <dx:ASPxGridView ID="GrdLocation" runat="server" AutoGenerateColumns="False" 
    Width="100%" KeyFieldName="DEPARTMENTCODE" OnCellEditorInitialize="GrdLocation_CellEditorInitialize" OnRowDeleting="GrdLocation_RowDeleting" OnRowInserting="GrdLocation_RowInserting" OnRowUpdating="GrdLocation_RowUpdating" OnRowValidating="GrdLocation_RowValidating" OnPageIndexChanged="GrdLocation_PageIndexChanged" OnHtmlDataCellPrepared="GrdLocation_HtmlDataCellPrepared" OnRowCommand="GrdLocation_RowCommand">
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <ClientSideEvents EndCallback="function(s, e) {
 if (s.cpMessage) {
 //check 
alert(s.cpMessage); //alert delete 
s.cpMessage; //delete 
} 
	
}" ContextMenu="function(s, e) {
	alert(&quot;contexmenu&quot;);
}" ContextMenuItemClick="function(s, e) {
	alert(&quot;menuItemClick&quot;);
}" CustomButtonClick="function(s, e) {
	alert(&quot;Cust&quot;);
}" ToolbarItemClick="function(s, e) {
	alert(&quot;toolbar&quot;);
}" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
        <SettingsPager>
            <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
        </SettingsPager>
        <SettingsEditing Mode="PopupEditForm">
        </SettingsEditing>
        <SettingsSearchPanel Visible="True" />
        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsCommandButton>
            <ClearFilterButton Text=" ">
                <Image IconID="filter_clearfilter_16x16">
                </Image>
            </ClearFilterButton>
            <NewButton Text=" ">
                <Image IconID="actions_add_16x16">
                </Image>
            </NewButton>
            <UpdateButton ButtonType="Image" RenderMode="Image">
                <Image IconID="save_save_16x16office2013">
                </Image>
            </UpdateButton>
            <CancelButton ButtonType="Image" RenderMode="Image">
                <Image IconID="actions_cancel_16x16office2013">
                </Image>
            </CancelButton>
            <EditButton Text=" ">
                <Image IconID="actions_edit_16x16devav">
                </Image>
            </EditButton>
            <DeleteButton Text=" ">
                <Image IconID="actions_trash_16x16">
                </Image>
            </DeleteButton>
        </SettingsCommandButton>
        <SettingsPopup>
            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
        </SettingsPopup>
        <SettingsText PopupEditFormCaption="Department" Title="Department" />
        <Columns>
            <dx:GridViewCommandColumn  Width="40px" ShowClearFilterButton="True" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="DEPARTMENTCODE" VisibleIndex="1" Caption="Code">
                <PropertiesTextEdit MaxLength="3">
                    <%--<ValidationSettings Display="Dynamic">
                        <RequiredField IsRequired="false" />
                    </ValidationSettings>--%>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DEPARTMENTNAME" VisibleIndex="2" Caption="Name">
                <PropertiesTextEdit MaxLength="50">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Head" FieldName="DEPARTMENTHEAD" ShowInCustomizationForm="True" VisibleIndex="3">
                <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="EMPNAME" ValueField="PAYCODE">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="EMAIL_ID" VisibleIndex="4" Caption="Email Id">
                <PropertiesTextEdit MaxLength="30">
                    <ClientSideEvents keyup="State_OnKeyUp" />
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Paddings Padding="0px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>
    <dx:ASPxLabel ID="lblStatus" runat="server" Text="" ForeColor="Red" ClientInstanceName="lblStatus">
    </dx:ASPxLabel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT [PAYCODE], [EMPNAME] FROM [TBLEMPLOYEE] where ACTIVE='Y' "></asp:SqlDataSource>
   
<%-- DXCOMMENT: Configure your datasource for ASPxGridView --%>



</asp:Content>

