﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GroupEdit : System.Web.UI.Page
{
    public class GrpData
    {
        public string GroupId; public string GroupName; public string Branch; public string SHIFT; public string SHIFTTYPE; public string SHIFTPATTERN;
        public string SHIFTREMAINDAYS; public string LASTSHIFTPERFORMED; public string ISAUTOSHIFT; public string AUTH_SHIFTS; public string FIRSTOFFDAY;
        public string SECONDOFFTYPE; public string HALFDAYSHIFT; public string SECONDOFFDAY; public string ALTERNATE_OFF_DAYS; public string INONLY;
        public string ISPUNCHALL; public string ISOUTWORK; public string TWO; public string ISTIMELOSSALLOWED; public string CDAYS; public string ISROUNDTHECLOCKWORK;
        public string ISOT; public string OTRATE; public string PERMISLATEARRIVAL; public string PERMISEARLYDEPRT; public string MAXDAYMIN; public string ISOS;
        public string TIME; public string SHORT; public string HALF; public string ISHALFDAY; public string ISSHORT; public string LateVerification;
        public string EveryInterval; public string DeductFrom; public string FromLeave; public string LateDays; public string DeductDay; public string MaxLaeDur;
        public string PunchRequiredInDay; public string SinglePunchOnly; public string LastModifiedBy; public string LastModifiedDate; public string MARKMISSASHALFDAY;
        public string Id; public string PERMISLATEARR; public string PERMISEARLYDEP; public string DUPLICATECHECKMIN; public string ISOVERSTAY;
        public string S_END; public string S_OUT; public string ISOTOUTMINUSSHIFTENDTIME; public string ISOTWRKGHRSMINUSSHIFTHRS;
        public string ISOTEARLYCOMEPLUSLATEDEP; public string ISOTALL; public string ISOTEARLYCOMING; public string OTEARLYDUR; public string OTLATECOMINGDUR;
        public string OTRESTRICTENDDUR; public string OTEARLYDEPARTUREDUR; public string DEDUCTWOOT; public string DEDUCTHOLIDAYOT; public string ISPRESENTONWOPRESENT;
        public string ISPRESENTONHLDPRESENT; public string ISAUTOABSENT; public string AUTOSHIFT_LOW; public string AUTOSHIFT_UP; public string WOINCLUDE;
        public string NightShiftFourPunch; public string OTROUND; public string PREWO; public string ISAWA; public string ISPREWO; public string LeaveFinancialYear;
        public string MIS; public string OwMinus; public string MSHIFT; public string MISCOMPOFF; public string ISCOMPOFF; public string ISAW; public string ISWA;
        public string ISAWP; public string ISPWA; public string IsRunAutoShift;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod()]
    public static string LoadGrp(string GrpId)
    {
        string GrpDataStr;
        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
        SqlDataAdapter adap = new SqlDataAdapter();
        DataSet ds = new DataSet();
        string sSql = "select * from EmployeeGroup where GroupId='" + GrpId + "' ";
        adap = new SqlDataAdapter(sSql, Conn);
        adap.Fill(ds);
        GrpData LS = new GrpData();

        LS.GroupId = GrpId;
        LS.GroupName = ds.Tables[0].Rows[0]["GroupName"].ToString().Trim();
        LS.Branch = ds.Tables[0].Rows[0]["Branch"].ToString().Trim();
        LS.SHIFT = ds.Tables[0].Rows[0]["SHIFT"].ToString().Trim();
        LS.SHIFTTYPE = ds.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim();
        LS.SHIFTPATTERN = ds.Tables[0].Rows[0]["SHIFTPATTERN"].ToString().Trim();
        LS.SHIFTREMAINDAYS = ds.Tables[0].Rows[0]["SHIFTREMAINDAYS"].ToString().Trim();
        LS.LASTSHIFTPERFORMED = ds.Tables[0].Rows[0]["LASTSHIFTPERFORMED"].ToString().Trim();
        LS.ISAUTOSHIFT = ds.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();
        LS.AUTH_SHIFTS = ds.Tables[0].Rows[0]["AUTH_SHIFTS"].ToString().Trim();
        LS.INONLY = ds.Tables[0].Rows[0]["INONLY"].ToString().Trim();
        LS.ISPUNCHALL = ds.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
        LS.ISOUTWORK = ds.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();
        LS.TWO = ds.Tables[0].Rows[0]["TWO"].ToString().Trim();
        LS.ISTIMELOSSALLOWED = ds.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
        LS.CDAYS = ds.Tables[0].Rows[0]["CDAYS"].ToString().Trim();
        LS.ISROUNDTHECLOCKWORK = ds.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim();
        LS.ISOT = ds.Tables[0].Rows[0]["ISOT"].ToString().Trim();
        LS.ISOS = ds.Tables[0].Rows[0]["ISOS"].ToString().Trim();
        LS.OTRATE = ds.Tables[0].Rows[0]["OTRATE"].ToString().Trim();
        LS.PERMISLATEARRIVAL = ds.Tables[0].Rows[0]["PERMISLATEARRIVAL"].ToString().Trim();
        LS.ISHALFDAY = ds.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
        LS.ISSHORT = ds.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
        LS.LateVerification = ds.Tables[0].Rows[0]["LateVerification"].ToString().Trim();
        LS.EveryInterval = ds.Tables[0].Rows[0]["EveryInterval"].ToString().Trim();
        LS.DeductFrom = ds.Tables[0].Rows[0]["DeductFrom"].ToString().Trim();
        LS.SinglePunchOnly = ds.Tables[0].Rows[0]["SinglePunchOnly"].ToString().Trim();
        LS.MARKMISSASHALFDAY = ds.Tables[0].Rows[0]["MARKMISSASHALFDAY"].ToString().Trim();
        LS.Id = ds.Tables[0].Rows[0]["Id"].ToString().Trim();
        LS.PERMISLATEARR = ds.Tables[0].Rows[0]["PERMISLATEARR"].ToString().Trim();
        LS.PERMISEARLYDEP = ds.Tables[0].Rows[0]["PERMISEARLYDEP"].ToString().Trim();
        LS.DUPLICATECHECKMIN = ds.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim();
        LS.ISOVERSTAY = ds.Tables[0].Rows[0]["ISOVERSTAY"].ToString().Trim();
        LS.S_END = ds.Tables[0].Rows[0]["S_END"].ToString().Trim();
        LS.S_OUT = ds.Tables[0].Rows[0]["S_OUT"].ToString().Trim();
        LS.ISOTOUTMINUSSHIFTENDTIME = ds.Tables[0].Rows[0]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
        LS.ISOTWRKGHRSMINUSSHIFTHRS = ds.Tables[0].Rows[0]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
        LS.ISOTEARLYCOMEPLUSLATEDEP = ds.Tables[0].Rows[0]["ISOTEARLYCOMEPLUSLATEDEP"].ToString().Trim();
        LS.ISOTALL = ds.Tables[0].Rows[0]["ISOTALL"].ToString().Trim();
        LS.ISOTEARLYCOMING = ds.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
        LS.OTEARLYDUR = ds.Tables[0].Rows[0]["OTEARLYDUR"].ToString().Trim();
        LS.OTLATECOMINGDUR = ds.Tables[0].Rows[0]["OTLATECOMINGDUR"].ToString().Trim();
        LS.OTRESTRICTENDDUR = ds.Tables[0].Rows[0]["OTRESTRICTENDDUR"].ToString().Trim();
        LS.OTEARLYDEPARTUREDUR = ds.Tables[0].Rows[0]["OTEARLYDEPARTUREDUR"].ToString().Trim();
        LS.DEDUCTWOOT = ds.Tables[0].Rows[0]["DEDUCTWOOT"].ToString().Trim();
        LS.DEDUCTHOLIDAYOT = ds.Tables[0].Rows[0]["DEDUCTHOLIDAYOT"].ToString().Trim();
        LS.ISPRESENTONWOPRESENT = ds.Tables[0].Rows[0]["ISPRESENTONWOPRESENT"].ToString().Trim();
        LS.ISPRESENTONHLDPRESENT = ds.Tables[0].Rows[0]["ISPRESENTONHLDPRESENT"].ToString().Trim();
        LS.ISAUTOABSENT = ds.Tables[0].Rows[0]["ISAUTOABSENT"].ToString().Trim();
        LS.AUTOSHIFT_LOW = ds.Tables[0].Rows[0]["AUTOSHIFT_LOW"].ToString().Trim();
        LS.AUTOSHIFT_UP = ds.Tables[0].Rows[0]["AUTOSHIFT_UP"].ToString().Trim();
        LS.WOINCLUDE = ds.Tables[0].Rows[0]["WOINCLUDE"].ToString().Trim();
        LS.NightShiftFourPunch = ds.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
        LS.OTROUND = ds.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
        LS.PREWO = ds.Tables[0].Rows[0]["PREWO"].ToString().Trim();
        LS.ISAWA = ds.Tables[0].Rows[0]["ISAWA"].ToString().Trim();
        LS.ISPREWO = ds.Tables[0].Rows[0]["ISPREWO"].ToString().Trim();
        LS.LeaveFinancialYear = ds.Tables[0].Rows[0]["LeaveFinancialYear"].ToString().Trim();
        LS.MIS = ds.Tables[0].Rows[0]["MIS"].ToString().Trim();
        LS.OwMinus = ds.Tables[0].Rows[0]["OwMinus"].ToString().Trim();
        LS.MSHIFT = ds.Tables[0].Rows[0]["MSHIFT"].ToString().Trim();
        LS.MISCOMPOFF = ds.Tables[0].Rows[0]["MISCOMPOFF"].ToString().Trim();
        LS.ISCOMPOFF = ds.Tables[0].Rows[0]["ISCOMPOFF"].ToString().Trim();
        LS.ISAW = ds.Tables[0].Rows[0]["ISAW"].ToString().Trim();
        LS.ISWA = ds.Tables[0].Rows[0]["ISWA"].ToString().Trim();
        LS.ISAWP = ds.Tables[0].Rows[0]["ISAWP"].ToString().Trim();
        LS.ISPWA = ds.Tables[0].Rows[0]["ISPWA"].ToString().Trim();
        LS.ALTERNATE_OFF_DAYS=ds.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();
        LS.IsRunAutoShift = ds.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();
        GrpDataStr = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(LS);
        return GrpDataStr;
    }

    [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod()]
    public static string SaveGrp(Boolean IsUpdate, String GroupId, String PERMISLATEARRIVAL, String PERMISEARLYDEPRT, String MAXDAYMIN,
        String ISROUNDTHECLOCKWORK, String ISTIMELOSSALLOWE, String ISHALFDAY, String ISSHORT, String TIME, String HALF, String SHORT, 
        String PunchRequiredInDay, String ISOT, String OTRATE, String IsOS, String SHIFTTYPE, String FIRSTOFFDAY, String SECONDOFFDAY, 
        String DUPLICATECHECKMIN, String S_END, String S_OUT, String AUTOSHIFT_LOW, String AUTOSHIFT_UP, String IsRunAutoShift, 
        String NightShiftFourPunch, String ISPRESENTONWOPRESENT, String ISPRESENTONHLDPRESENT, String ISAWA, String ISAW, String ISWA, 
        String ISAUTOABSENT, String WOINCLUDE, String IsAbsOnNoDay, String MIS, String IsMISAsH, String OwMinus, String ISCOMPOFF, 
        String PREWO, String SHIFTPATTERN, String SHIFTREMAINDAYS, String SECONDOFFTYPE, String HALFDAYSHIFT, String ALTERNATE_OFF_DAYS)
    { 
        string sSql;
        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
        SqlCommand Cmd;

        if (IsUpdate)
        {
            //update
            try
            {
                sSql = "Update EmployeeGroup set PERMISLATEARRIVAL ='" + PERMISLATEARRIVAL + "', " + 
"PERMISEARLYDEPRT = '" + PERMISEARLYDEPRT + "', " +
"MAXDAYMIN = '" + MAXDAYMIN + "', " + 
"ISROUNDTHECLOCKWORK = '" + ISROUNDTHECLOCKWORK + "', " + 
"ISTIMELOSSALLOWED = '" + ISTIMELOSSALLOWE + "', " + 
"ISHALFDAY = '" + ISHALFDAY + "', " + 
"ISHALFDAY = '" + ISHALFDAY + "', " + 
"ISSHORT = '" + ISSHORT + "', " + 
"TIME = '" + TIME + "', " + 
"HALF = '" + HALF + "', " + 
"SHORT = '" + SHORT + "', " + 
"PunchRequiredInDay = '" + PunchRequiredInDay + "', " + 
"ISOT = '" + ISOT + "', " + 
"OTRATE = '" + OTRATE + "', " + 
"IsOS = '" + IsOS + "', " + 
"SHIFTTYPE = '" + SHIFTTYPE + "', " + 
"FIRSTOFFDAY = '" + FIRSTOFFDAY + "', " + 
"SECONDOFFDAY = '" + SECONDOFFDAY + "', " + 
"DUPLICATECHECKMIN = '" + DUPLICATECHECKMIN + "', " + 
"S_END = '" + S_END + "', " + 
"S_OUT = '" + S_OUT + "', " + 
"AUTOSHIFT_LOW = '" + AUTOSHIFT_LOW + "', " + 
"AUTOSHIFT_UP = '" + AUTOSHIFT_UP + "', " + 
"IsRunAutoShift = '" + IsRunAutoShift + "', " + 
"NightShiftFourPunch = '" + NightShiftFourPunch + "', " + 
"ISPRESENTONWOPRESENT = '" + ISPRESENTONWOPRESENT + "', " + 
"ISPRESENTONHLDPRESENT = '" + ISPRESENTONHLDPRESENT + "', " + 
"ISAWA = '" + ISAWA + "', " + 
"ISAW = '" + ISAW + "', " + 
"ISWA = '" + ISWA + "', " + 
"ISAUTOABSENT = '" + ISAUTOABSENT + "', " + 
"WOINCLUDE = '" + WOINCLUDE + "', " + 
"IsAbsOnNoDay = '" + IsAbsOnNoDay + "', " + 
"MIS = '" + MIS + "', " + 
"IsMISAsH = '" + IsMISAsH + "', " + 
"OwMinus = '" + OwMinus + "', " + 
"ISCOMPOFF = '" + ISCOMPOFF + "', " + 
"PREWO = '" + PREWO + "', " + 
"SHIFTPATTERN = '" + SHIFTPATTERN + "', " + 
"SHIFTREMAINDAYS = '" + SHIFTREMAINDAYS + "', " + 
"SECONDOFFTYPE = '" + SECONDOFFTYPE + "', " + 
"HALFDAYSHIFT = '" + HALFDAYSHIFT + "', " + 
"ALTERNATE_OFF_DAYS = '" + ALTERNATE_OFF_DAYS + "' where GroupId = '" + GroupId + "'";
                if (Conn.State != ConnectionState.Open)
                    Conn.Open();
                Cmd = new SqlCommand(sSql, Conn);
                int result = Cmd.ExecuteNonQuery();
                if (Conn.State != ConnectionState.Closed)
                    Conn.Close();
                if (result > 0)
                {
                    return "true";
                }
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            //insert
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter();
                DataSet ds = new DataSet();
                sSql = "select Shift from tblShiftMaster where GroupId='" + GroupId + "' ";
                adap = new SqlDataAdapter(sSql, Conn);
                adap.Fill(ds);
                int result = ds.Tables[0].Rows.Count;
                if (result > 0)
                {
                    return "GroupId Already Exists";
                }
                sSql = "insert into EmployeeGroup (GroupId,PERMISLATEARRIVAL,PERMISEARLYDEPRT,MAXDAYMIN,ISROUNDTHECLOCKWORK,ISTIMELOSSALLOWED,ISHALFDAY,ISHALFDAY,ISSHORT,TIME,HALF,SHORT,PunchRequiredInDay,ISOT,OTRATE,IsOS,SHIFTTYPE,FIRSTOFFDAY,SECONDOFFDAY,DUPLICATECHECKMIN,S_END,S_OUT,AUTOSHIFT_LOW,AUTOSHIFT_UP,IsRunAutoShift,NightShiftFourPunch,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,ISAWA,ISAW,ISWA,ISAUTOABSENT,WOINCLUDE,IsAbsOnNoDay,MIS,IsMISAsH,OwMinus,ISCOMPOFF,PREWO,SHIFTPATTERN,SHIFTREMAINDAYS,SECONDOFFTYPE,HALFDAYSHIFT,ALTERNATE_OFF_DAYS) values " +
                        "('" + GroupId + "','" + PERMISLATEARRIVAL + "','" + PERMISEARLYDEPRT + "','" + MAXDAYMIN + "','" + ISROUNDTHECLOCKWORK + "','" + ISTIMELOSSALLOWE + "','" + ISHALFDAY + "','" + ISHALFDAY + "','" + ISSHORT + "','" + TIME + "','" + HALF + "','" + SHORT + "','" + PunchRequiredInDay + "','" + ISOT + "','" + OTRATE + "','" + IsOS + "','" + SHIFTTYPE + "','" + FIRSTOFFDAY + "','" + SECONDOFFDAY + "','" + DUPLICATECHECKMIN + "','" + S_END + "','" + S_OUT + "','" + AUTOSHIFT_LOW + "','" + AUTOSHIFT_UP + "','" + IsRunAutoShift + "','" + NightShiftFourPunch + "','" + ISPRESENTONWOPRESENT + "','" + ISPRESENTONHLDPRESENT + "','" + ISAWA + "','" + ISAW + "','" + ISWA + "','" + ISAUTOABSENT + "','" + WOINCLUDE + "','" + IsAbsOnNoDay + "','" + MIS + "','" + IsMISAsH + "','" + OwMinus + "','" + ISCOMPOFF + "','" + PREWO + "','" + SHIFTPATTERN + "','" + SHIFTREMAINDAYS + "','" + SECONDOFFTYPE + "','" + HALFDAYSHIFT + "','" + ALTERNATE_OFF_DAYS + "')";
                if (Conn.State != ConnectionState.Open)
                    Conn.Open();
                Cmd = new SqlCommand(sSql, Conn);
                result = Cmd.ExecuteNonQuery();
                if (Conn.State != ConnectionState.Closed)
                    Conn.Close();
                if (result > 0)
                {
                    return "true";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        return "true";
    }
}