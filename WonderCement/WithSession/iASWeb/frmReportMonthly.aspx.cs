﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.Text;
using System.IO;
using System.Diagnostics;
using GlobalSettings;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;


public partial class frmReportMonthly : System.Web.UI.Page
{
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    hourToMinute hm = new hourToMinute();
    Class_Connection con = new Class_Connection();
    string datefrom = null;
    string dateTo = null;
    string strsql = null;
    string ShiftSelected = null;
    string strsql1 = null;
    DataSet ds = null;
    DataSet ds1 = null;
    string OpenPopupPage = "";
    string Selection = "";
    string status = null;
    string leave = null;
    DateTime toDate;
    DateTime FromDate;
    DateTime toDateNepal;
    DateTime FromDateNepal;
    DataSet dsEmpCount = new DataSet();
    string EmpCodes = "";
    DataTable dTbl;
    string btnGoBack = "";
    string resMsg = "";
    string Strsql = "";
    string Shift = "";
    hourToMinute HM = new hourToMinute();
    ErrorClass ec = new ErrorClass();
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
        if (ex is HttpRequestValidationException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (!IsPostBack)
        {
            txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
            //LookupCompany.DataSource = DataFilter.CompanyNonAdmin;
            //LookupCompany.DataBind();
            //LookUpDept.DataSource = DataFilter.LocationNonAdmin;
            //LookUpDept.DataBind();
            //lookupSec.DataSource = DataFilter.DivNonAdmin;
            //lookupSec.DataBind();
            //lookupGrade.DataSource = DataFilter.GradeNonAdmin;
            //lookupGrade.DataBind();
            //lookupCat.DataSource = DataFilter.CatNonAdmin;
            //lookupCat.DataBind();
            //LookEmp.DataSource = DataFilter.EmpNonAdmin;
            //LookEmp.DataBind();
            bindShift();
            bindCompany();
            bindPaycode();
            bindDepartment();
            bindSection();
            bindGrade();
            bindCategory();
           
            //LookUpShift.DataSource = DataFilter.ShiftNonAdmin;
            //LookUpShift.DataBind();
            Session["IsNepali"] = "N";

        }
    }
    protected void bindCompany()
    {
        try
        {
            Strsql = "select CompanyCode,Companyname from tblCompany";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            else
            {
                Strsql += " where companycode ='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }

            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookupCompany.DataSource = ds.Tables[0];
                LookupCompany.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindCompany", Err.Message);
        }
    }
    protected void bindPaycode()
    {
        try
        {
            Strsql = "select PAYCODE,EMPNAME from tblemployee where active='Y' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookEmp.DataSource = ds.Tables[0];
                LookEmp.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindPaycode", Err.Message);
        }
    }

    protected void bindDepartment()
    {
        try
        {
            Strsql = "select DepartmentCode,departmentname from tbldepartment";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql += " where DepartmentCode in (" + Session["Auth_Dept"].ToString().Trim() + ") and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            else
            {
                Strsql += " where  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookUpDept.DataSource = ds.Tables[0];
                LookUpDept.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindDepartment", Err.Message);
        }
    }
    protected void bindCategory()
    {
        try
        {
            Strsql = "select CAT,CATAGORYNAME from tblCatagory where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupCat.DataSource = ds.Tables[0];
                lookupCat.DataBind();

            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindCategory", Err.Message);
        }
    }
    protected void bindSection()
    {
        try
        {
            Strsql = "select DivisionCode,DivisionName from tblDivision where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupSec.DataSource = ds.Tables[0];
                lookupSec.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindSection", Err.Message);
        }
    }
    protected void bindGrade()
    {
        try
        {
            Strsql = "select GradeCode,GradeName from tblGrade where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupGrade.DataSource = ds.Tables[0];
                lookupGrade.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindGrade", Err.Message);
        }
    }

    protected void bindShift()
    {
        Strsql = "select Shift,Shift +'-'+ convert(char(5),starttime, 108) +'-'+ convert(char(5),endtime, 108) 'ShiftTime' from  TblShiftMaster where companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") ";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            LookUpShift.DataSource = ds.Tables[0];
            LookUpShift.Value = "Shift";
            LookUpShift.Text = "ShiftTime";
            LookUpShift.DataBind();
        }
    }
   

    protected void GetSelection()
    {
        try
        {
            strsql = "";
            string Company = "";
            string Employee = "";
            string Dept = "";
            string category = "";
            string Shift = "";
            string Division = "";
            string Grade = "";

            if (LookupCompany.Text.ToString().Trim() != string.Empty || LookupCompany.Text.ToString().Trim() != "")
            {


                string[] SplitComp = LookupCompany.Text.ToString().Trim().Split(',');
                Company = "";
                foreach (string sc in SplitComp)
                    Company += "'" + sc.Trim() + "',";

                Company = Company.TrimEnd(',');


                //Company = LookupCompany.Text.ToString().Trim();
            }
            if (LookUpDept.Text.ToString().Trim() != string.Empty || LookUpDept.Text.ToString().Trim() != "")
            {
                string[] SplitDep = LookUpDept.Text.ToString().Trim().Split(',');
                Dept = "";
                foreach (string sc in SplitDep)
                    Dept += "'" + sc.Trim() + "',";

                Dept = Dept.TrimEnd(',');
            }
            if (lookupSec.Text.ToString().Trim() != string.Empty || lookupSec.Text.ToString().Trim() != "")
            {

                string[] SplitSec = lookupSec.Text.ToString().Trim().Split(',');
                Division = "";
                foreach (string sc in SplitSec)
                    Division += "'" + sc.Trim() + "',";

                Division = Division.TrimEnd(',');



                //Division = lookupSec.Text.ToString().Trim();
            }
            if (lookupGrade.Text.ToString().Trim() != string.Empty || lookupGrade.Text.ToString().Trim() != "")
            {

                string[] SplitGr = lookupGrade.Text.ToString().Trim().Split(',');
                Grade = "";
                foreach (string sc in SplitGr)
                    Grade += "'" + sc.Trim() + "',";

                Grade = Grade.TrimEnd(',');



                // Grade = lookupGrade.Text.ToString().Trim();
            }
            if (lookupCat.Text.ToString().Trim() != string.Empty || lookupCat.Text.ToString().Trim() != "")
            {


                string[] SplitCAt = lookupCat.Text.ToString().Trim().Split(',');
                category = "";
                foreach (string sc in SplitCAt)
                    category += "'" + sc.Trim() + "',";

                category = category.TrimEnd(',');

                //category = lookupCat.Text.ToString().Trim();
            }
            if (LookUpShift.Text.ToString().Trim() != string.Empty || LookUpShift.Text.ToString().Trim() != "")
            {

                string[] SplitShift = LookUpShift.Text.ToString().Trim().Split(',');
                Shift = "";
                foreach (string sc in SplitShift)
                    Shift += "'" + sc.Trim() + "',";

                Shift = Shift.TrimEnd(',');

                //Shift = LookUpShift.Text.ToString().Trim();
            }
            if (LookEmp.Text.ToString().Trim() != string.Empty || LookEmp.Text.ToString().Trim() != "")
            {
                string[] SplitEmp = LookEmp.Text.ToString().Trim().Split(',');
                Employee = "";
                foreach (string sc in SplitEmp)
                    Employee += "'" + sc.Trim() + "',";

                Employee = Employee.TrimEnd(',');


                //Employee = LookEmp.Text.ToString().Trim();
            }
            strsql = " and 1=1 ";
            if (Company != "")
            {
                strsql += "and tblemployee.Companycode in (" + Company.ToString().Trim() + ") ";
            }
            if (Employee != "")
            {
                strsql += "and tblemployee.paycode in (" + Employee.ToString().Trim() + ")  ";
            }
            if (Dept != "")
            {
                strsql += "and tblemployee.DepartmentCode in (" + Dept.ToString().Trim() + ")  ";
            }
            else
            {
                strsql += "and tblemployee.DepartmentCode in (" + Session["Auth_Dept"].ToString().Trim() + ")  ";
            }
            if (category != "")
            {
                strsql += "and tblemployee.Cat in (" + category.ToString().Trim() + ")  ";
            }
            //if (Shift != "")
            //{
            //    strsql += "and tbltimeregister.shiftattended in (" + Shift.ToString().Trim() + ")  ";
            //}
            if (Division != "")
            {
                strsql += "and tblemployee.Divisioncode in (" + Division.ToString().Trim() + ")  ";
            }
            if (Grade != "")
            {
                strsql += "and tblemployee.Gradecode in (" + Grade.ToString().Trim() + ")";
            }
            Session["SelectionQuery"] = strsql;
        }
        catch
        {
            Session["SelectionQuery"] = "";
            Session["SelectionQuery"] = null;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //LookupCompany.DataSource = DataFilter.CompanyNonAdmin;
        //LookupCompany.DataBind();
        //LookUpDept.DataSource = DataFilter.LocationNonAdmin;
        //LookUpDept.DataBind();
        //lookupSec.DataSource = DataFilter.DivNonAdmin;
        //lookupSec.DataBind();
        //lookupGrade.DataSource = DataFilter.GradeNonAdmin;
        //lookupGrade.DataBind();
        //lookupCat.DataSource = DataFilter.CatNonAdmin;
        //lookupCat.DataBind();
        //LookEmp.DataSource = DataFilter.EmpNonAdmin;
        //LookEmp.DataBind();
        bindShift();
        bindCompany();
        bindPaycode();
        bindDepartment();
        bindSection();
        bindGrade();
        bindCategory();
        bindShift();
    }
    protected string GetAttendanceLocation(string CardNo, string PunchTime)
    {
        string Location = "NA";
        DataSet DsLoc = new DataSet();
        DateTime AttTime = System.DateTime.MinValue;
        string sSql = "";
        try
        {
            if (PunchTime.Trim() == "")
            {
                return Location;
            }
            AttTime = Convert.ToDateTime(PunchTime);
            //if (Devices.ToString().Trim() == "")
            //{
                sSql = "select UA.DeviceID,TM.branch from UserAttendance UA inner join tblMachine TM on tm.SerialNumber=UA.DeviceID where ua.UserID= " +
                       "(select cast(presentcardno as int) from tblemployee where SSN='" + CardNo + "' ) and ua.AttDateTime='" + AttTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            //}
            //else
            //{
            //    sSql = "select UA.DeviceID,TM.branch from UserAttendance UA inner join tblMachine TM on tm.SerialNumber=UA.DeviceID where ua.UserID= " +
            //           "(select cast(presentcardno as int) from tblemployee where PAYCODE='" + CardNo + "' ) and ua.AttDateTime='" + AttTime.ToString("yyyy-MM-dd HH:mm:ss") + "' and " +
            //           "UA.DeviceID in (" + Devices.ToString().Trim() + ")";
            //}





            DsLoc = con.FillDataSet(sSql);
            if (DsLoc.Tables[0].Rows.Count > 0)
            {
                Location = DsLoc.Tables[0].Rows[0]["branch"].ToString();
            }
            else
            {
                Location = "NA";
            }

        }
        catch
        {
            Location = "Not Found";

        }
        return Location;
    }
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string ReportView = "", CompanySelection = "";
        string msg = "";
        Session["SelectionQuery"] = "";
        Session["SelectionQuery"] = null;
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        try
        {
           
            GetSelection();
            Session["ExportInto"] = 0;
            try
            {

                if (Session["IsNepali"].ToString().Trim() == "Y")
                {
                    //string FromDate1 = "", ToDate = "";
                    //DateTime dtFrom = System.DateTime.MinValue;
                    //DateTime dtTo = System.DateTime.MinValue;
                    //int MonthTo = NC.GetNepaliMonthIndex(ddlMonthTo.SelectedItem.Text.ToString());
                    //int MonthFrom = NC.GetNepaliMonthIndex(ddlMonthFrom.SelectedItem.Text.ToString());
                    //FromDate1 = ddlYearFrom.SelectedValue + "/" + MonthFrom + "/" + ddlDateFrom.SelectedValue;
                    //ToDate = ddlYearTo.SelectedValue + "/" + MonthTo + "/" + ddlDateTo.SelectedValue;
                    //dtFrom = Convert.ToDateTime(FromDate1);
                    //dtTo = Convert.ToDateTime(ToDate);
                    //FromDateNepal = dtFrom;
                    //toDateNepal = dtTo;
                    //dtFrom = NC.ToAD(dtFrom);
                    //dtTo = NC.ToAD(dtTo);
                    //FromDate = dtFrom;
                    //toDate = dtTo;

                }

                else
                {
                    //FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    FromDate = Convert.ToDateTime(txtDateFrom.Value);
                    toDate = Convert.ToDateTime(txtToDate.Value);

                }


            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Invalid Date Format');", true);
                txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
                return;
            }
            //FromDate = Convert.ToDateTime(txtDateFrom.Text);
            //toDate = Convert.ToDateTime(txtToDate.Text);
            try
            {
                ShiftSelected = "";
                if (LookUpShift.Text.ToString().Trim() != string.Empty || LookUpShift.Text.ToString().Trim() != "")
                {

                    string[] SplitShift = LookUpShift.Text.ToString().Trim().Split(',');
                    Shift = "";
                    foreach (string sc in SplitShift)
                        Shift += "'" + sc.Trim() + "',";

                    Shift = Shift.TrimEnd(',');

                  
                }
                if (Shift != "")
                {
                    ShiftSelected += "and tbltimeregister.shiftattended in (" + Shift.ToString().Trim() + ")  ";
                }
                else
                {
                    ShiftSelected = "";
                }
            }
            catch
            {

            }




            if (toDate < FromDate)
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('To Date must be greater than From Date');", true);
                return;
            }
            TimeSpan ts = toDate.Subtract(FromDate);
            int day = ts.Days;
            if (day > 30)
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Difference is more than one Month');", true);
                return;
            }
            UpdateTimeRegister();           
            OpenPopupPage = "";
            datefrom = txtDateFrom.Text.ToString().Trim();
            dateTo = txtToDate.Text.ToString().Trim();
            if (Session["SelectionQuery"] != null)
            {
                Selection = Session["SelectionQuery"].ToString();
            }
            else
            {
                if ((Session["usertype"].ToString().Trim() == "A") && (Session["PAYCODE"].ToString().ToString().ToUpper() != "ADMIN"))
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                    }
                }
                else if (Session["usertype"].ToString().Trim() == "H")
                {
                    if (ReportView.ToString().Trim() == "Y")
                    {
                        strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                       DataSet dsEmpCount = con.FillDataSet(strsql);
                        if (dsEmpCount.Tables[0].Rows.Count > 0)
                        {
                            for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                            {
                                EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                            }
                            if (!string.IsNullOrEmpty(EmpCodes))
                            {
                                EmpCodes = EmpCodes.Substring(1);
                            }
                            Selection += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                        }
                    }
                    else
                    {

                        if (Session["Auth_Comp"] != null)
                        {
                            Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                        }
                        if (Session["Auth_Dept"] != null)
                        {
                            Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                        }
                    }
                }
                else if ((Session["usertype"].ToString().Trim() == "A") && (Session["PAYCODE"].ToString().ToString().ToUpper() == "ADMIN"))
                {
                    if (Session["Com_Selection"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Com_Selection"].ToString() + ") ";
                    }
                    if (Session["Dept_Selection"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Dept_Selection"].ToString() + ") ";
                    }
                }
            }
            CompanySelection = "and tblcompany.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tbldepartment.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tblcatagory.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tbldivision.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tblGrade.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ")";



            if (radEmployeeWisePerformance.Checked)
            {

                try
                {
                    string field1 = "", field2 = "", tablename = "", SSN = "";
                    string Tmp = "";
                    int x, i;
                    string InLocatuion = "NA", OutLocation = "NA";
                    double present = 0, absent = 0, leave = 0, wo = 0, hld = 0, totalhr = 0;
                    //// create table 
                    System.Data.DataTable dTbl = new System.Data.DataTable();
                    dTbl.Columns.Add("Paycode");
                    DataRow dRow;
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    dTbl.Columns.Add("Date");
                    dTbl.Columns.Add("Day");
                    dTbl.Columns.Add("Shift");
                    dTbl.Columns.Add("In");
                    dTbl.Columns.Add("Out");
                    dTbl.Columns.Add("Late Arrival");
                    dTbl.Columns.Add("Early Departure");
                    dTbl.Columns.Add("Hours Worked");
                    dTbl.Columns.Add("Over Time");
                    dTbl.Columns.Add("Status");
                    //dTbl.Columns.Add("In Location");
                    //dTbl.Columns.Add("Out Location");
                    dTbl.Rows[0][0] = ("Paycode");

                    int ct = 0;
                    strsql = "";
                    strsql = "select tblemployee.paycode,empname,tblemployee.SSN,tbldepartment.departmentname,designation, tblCatagory.CATAGORYNAME  from tblemployee join tblcompany  on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on tblemployee.ssn=tblemployeeshiftmaster.ssn  " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE   join tblcatagory on tblemployee.CAT=tblcatagory.CAT   join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode  join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        "where  tblemployee.Active='Y' " +
                        "And (tblemployee.LeavingDate >= '" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and dateofjoin <= '" + toDate.ToString("yyyy-MM-dd") + "' ";
                    if (Selection.ToString().Trim() != "")
                    {
                        strsql = strsql + Selection.ToString();
                    }
                    strsql += " " + CompanySelection.ToString() + " ";
                    strsql += "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);
                    DataSet dsResult = new DataSet();
                    DataSet dsResult1 = new DataSet();
                    dsResult = con.FillDataSet(strsql);
                    if (dsResult.Tables[0].Rows.Count > 0)
                    {
                        for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                        {
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            present = 0; absent = 0; leave = 0; wo = 0; hld = 0; totalhr = 0;
                            dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1][0].ToString().Trim() + " ( " + dsResult.Tables[0].Rows[i1]["empname"].ToString().Trim() + " )" + "  Department:- (" + dsResult.Tables[0].Rows[i1]["departmentname"].ToString().Trim() + ") " + "  Designation:- (" + dsResult.Tables[0].Rows[i1]["designation"].ToString().Trim() + ")" + "  Category:- (" + dsResult.Tables[0].Rows[i1]["CATAGORYNAME"].ToString().Trim() + ")";
                            SSN = dsResult.Tables[0].Rows[i1][2].ToString().Trim();
                            dTbl.Rows[1][0] = ("Date");
                            dTbl.Rows[1][1] = ("Day");
                            dTbl.Rows[1][2] = ("Shift");
                            dTbl.Rows[1][3] = ("In");
                            dTbl.Rows[1][4] = ("Out");
                            dTbl.Rows[1][5] = ("Late Arrival");
                            dTbl.Rows[1][6] = ("Early Departure");
                            dTbl.Rows[1][7] = ("Hours Worked");
                            dTbl.Rows[1][8] = ("Over Time");
                            dTbl.Rows[1][9] = ("Status");
                            //dTbl.Rows[1][10] = ("In Location");
                            //dTbl.Rows[1][11] = ("Out Location");

                            ct = ct + 1;
                            strsql1 = "select convert(char(10),dateoffice,103) 'date',Substring(datename(dw,dateoffice),0,4) 'DayName',convert(char(5),in1,108) 'in1',convert(char(5),out2,108) 'out2',shiftattended 'shift',status, " +
                                    "case when latearrival=0 then null else Cast(latearrival / 60 as Varchar) +':' +Cast(latearrival % 60 as Varchar) end as 'latearrival', " +
                                    "case when earlydeparture=0 then null else Cast(earlydeparture/ 60 as Varchar) +':' +Cast(earlydeparture % 60 as Varchar) end as 'earlyarrival'," +
                                    "case when hoursworked=0 then null else substring(convert(varchar(20),dateadd(minute,hoursworked,0),108),0,6) end as 'hoursworked', " +
                                    "case when otduration=0 then null else Cast(otduration / 60 as Varchar) +':' +Cast(otduration  % 60 as Varchar) end as 'otduration' " +
                                    ",hoursworked,presentvalue,absentvalue,leavevalue,holiday_value,wo_value " +
                                    "from tbltimeregister where SSN='" + SSN.ToString() + "' and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and " + "'" + toDate.ToString("yyyy-MM-dd") + "'  ";
                            if (ShiftSelected.Trim() != string.Empty || ShiftSelected.Trim()!="")
                            {
                                strsql1 = strsql1 + ShiftSelected.ToString().Trim();
                            }
                            strsql1 = strsql1 + "Order by dateoffice";


                            dsResult1 = con.FillDataSet(strsql1);
                            if (dsResult1.Tables[0].Rows.Count > 0)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[ct][0] = ("Date");
                                dTbl.Rows[ct][1] = ("Day");
                                dTbl.Rows[ct][2] = ("Shift");
                                dTbl.Rows[ct][3] = ("In");
                                dTbl.Rows[ct][4] = ("Out");
                                dTbl.Rows[ct][5] = ("Late Arrival");
                                dTbl.Rows[ct][6] = ("Early Departure");
                                dTbl.Rows[ct][7] = ("Hours Worked");
                                dTbl.Rows[ct][8] = ("Over Time");
                                dTbl.Rows[ct][9] = ("Status");
                                //dTbl.Rows[ct][10] = ("In Location");
                                //dTbl.Rows[ct][11] = ("Out Location");
                                ct = ct + 1;

                                for (int j1 = 0; j1 < dsResult1.Tables[0].Rows.Count; j1++)
                                {

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    if (Session["IsNepali"].ToString().Trim() == "Y")
                                    {
                                      //  dTbl.Rows[ct][0] = NC.ToBS(Convert.ToDateTime(dsResult1.Tables[0].Rows[j1][0].ToString()));
                                    }
                                    else
                                    {
                                        dTbl.Rows[ct][0] = dsResult1.Tables[0].Rows[j1][0].ToString();
                                    }
                                    dTbl.Rows[ct][1] = dsResult1.Tables[0].Rows[j1][1].ToString();
                                    dTbl.Rows[ct][2] = dsResult1.Tables[0].Rows[j1][4].ToString();
                                    dTbl.Rows[ct][3] = dsResult1.Tables[0].Rows[j1][2].ToString();
                                    dTbl.Rows[ct][4] = dsResult1.Tables[0].Rows[j1][3].ToString(); ;
                                    dTbl.Rows[ct][5] = dsResult1.Tables[0].Rows[j1][6].ToString();
                                    dTbl.Rows[ct][6] = dsResult1.Tables[0].Rows[j1][7].ToString();
                                    dTbl.Rows[ct][7] = dsResult1.Tables[0].Rows[j1][8].ToString();
                                    dTbl.Rows[ct][8] = dsResult1.Tables[0].Rows[j1][9].ToString(); ;
                                    dTbl.Rows[ct][9] = dsResult1.Tables[0].Rows[j1][5].ToString();

                                    //InLocatuion = GetAttendanceLocation(SSN, dsResult1.Tables[0].Rows[j1]["in1"].ToString());
                                    //dTbl.Rows[ct][10] = InLocatuion.Trim();
                                    //OutLocation = GetAttendanceLocation(SSN, dsResult1.Tables[0].Rows[j1]["Out2"].ToString());
                                    //dTbl.Rows[ct][11] = OutLocation.Trim();

                                    totalhr = totalhr + ((string.IsNullOrEmpty(dsResult1.Tables[0].Rows[j1][10].ToString())) ? 0 : Convert.ToDouble(dsResult1.Tables[0].Rows[j1][10].ToString()));
                                    //Convert.ToDouble(dsResult1.Tables[0].Rows[j1][10].ToString());
                                    present += ((string.IsNullOrEmpty(dsResult1.Tables[0].Rows[j1][11].ToString())) ? 0 : Convert.ToDouble(dsResult1.Tables[0].Rows[j1][11].ToString()));
                                    //Convert.ToDouble(dsResult1.Tables[0].Rows[j1][11].ToString());
                                    absent += ((string.IsNullOrEmpty(dsResult1.Tables[0].Rows[j1][12].ToString())) ? 0 : Convert.ToDouble(dsResult1.Tables[0].Rows[j1][12].ToString()));
                                    //Convert.ToDouble(dsResult1.Tables[0].Rows[j1][12].ToString());
                                    leave += ((string.IsNullOrEmpty(dsResult1.Tables[0].Rows[j1][13].ToString())) ? 0 : Convert.ToDouble(dsResult1.Tables[0].Rows[j1][13].ToString()));
                                    //Convert.ToDouble(dsResult1.Tables[0].Rows[j1][13].ToString());
                                    hld += ((string.IsNullOrEmpty(dsResult1.Tables[0].Rows[j1][14].ToString())) ? 0 : Convert.ToDouble(dsResult1.Tables[0].Rows[j1][14].ToString()));
                                    //Convert.ToDouble(dsResult1.Tables[0].Rows[j1][14].ToString());
                                    wo += ((string.IsNullOrEmpty(dsResult1.Tables[0].Rows[j1][15].ToString())) ? 0 : Convert.ToDouble(dsResult1.Tables[0].Rows[j1][15].ToString()));
                                    //Convert.ToDouble(dsResult1.Tables[0].Rows[j1][15].ToString());

                                    ct = ct + 1;
                                }
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                //ct = ct + 1;
                                dTbl.Rows[ct][0] = "Present : " + present + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Absent : " + absent + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;leave : " + leave + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Holiday : " + hld + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Week Offs : " + wo + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Working : " + hm.minutetohour(totalhr.ToString());
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                            }
                            ct = ct + 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                        }
                      // dTbl.Columns.Remove("Status");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                        return;
                    }
                 //   ExportToPdf(dTbl);
                    bool isEmpty;
                    for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                    {

                        isEmpty = true;
                        for (int j = 0; j < dTbl.Columns.Count; j++)
                        {
                            if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                            {
                                isEmpty = false;
                                break;
                            }
                        }

                        if (isEmpty == true)
                        {
                            dTbl.Rows.RemoveAt(i2);
                            i2--;
                        }
                    }





                    string companyname = "";

                    strsql = "select companyname from tblcompany";
                    if (Session["Com_Selection"] != null)
                    {
                        strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                    }
                    else if (Session["Auth_Comp"] != null)
                    {
                        strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                    }
                    DataSet dsCom = new DataSet();
                    dsCom = con.FillDataSet(strsql);
                    if (dsCom.Tables[0].Rows.Count > 0)
                    {
                        for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                        {
                            companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                        }
                    }
                    if (!string.IsNullOrEmpty(companyname.ToString()))
                    {
                        companyname = companyname.Substring(1);
                    }
                    if (Session["IsNepali"].ToString().Trim() == "Y")
                    {
                        msg = "Employee Performance from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                    }
                    else
                    {
                        msg = msg = "Employee Performance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    }

                    // msg = "Employee Performance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                    sb.Append("<tr><td colspan='13' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: left'></td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: left'></td></tr> ");

                    x = dTbl.Columns.Count;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                                if (Tmp.Substring(0, 1) == "0")
                                {
                                    if (Tmp.Contains(":"))
                                    {
                                        Tmp = Tmp;
                                    }
                                    else
                                    {
                                        Tmp = Tmp;
                                        //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                    }
                                }
                            }
                            if (Tmp.Contains(field1.ToString()) && !string.IsNullOrEmpty(field1.ToString().Trim()))
                            {
                                sb.Append("<td colspan='13'  style='background-color:#DCF7FA;text-align: left;font-weight:bold;font-size:13px;padding-left:15px'>" + Tmp + "</td>");
                                break;
                            }
                            else if (Tmp.Contains("("))
                            {
                                sb.Append("<td colspan='13'  style='text-align: center;font-weight:bold;font-size:13px'>" + Tmp + "</td>");
                                break;
                            }
                            else if (Tmp.Contains("Present"))
                            {
                                sb.Append("<td colspan='13'  style='text-align: left;font-weight:bold;font-size:14px;padding-left:15px'>" + Tmp + "</td>");
                                break;
                            }
                            else
                            {
                                if (i >= 4)
                                    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    if(radExcel.Checked)
                    {
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeePerformance.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/EmployeePerformance.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    else
                    {
                        //Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        //string pdffilename = "EmployeePerformance.pdf";
                        //PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        //pdfDocument.Open();
                        //String htmlText = sb.ToString();
                        //StringReader str = new StringReader(htmlText);
                        //HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        //htmlworker.Parse(str);
                        //pdfWriter.CloseStream = false;
                        //pdfDocument.Close();
                        ////Download Pdf  
                        //Response.Buffer = true;
                        //Response.ContentType = "application/pdf";
                        //Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        //Response.Write(pdfDocument);
                        //Response.Flush();
                        //Response.End();


                        StringReader sr = new StringReader(sb.ToString());
                        Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        htmlparser.Parse(sr);
                        pdfDoc.Close();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeePerformance.pdf");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                        Response.End();
                       
                    }

                   
                }
                catch (Exception ex)
                {
                    Error_Occured("EmployeePerformance ", ex.Message);
                }
            }
            /* else
             {
                 if (Session["usertype"].ToString().ToString().ToUpper() != "A")
                 {
                     strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                     dsEmpCount = con.FillDataSet(strsql);
                     if (dsEmpCount.Tables[0].Rows.Count == 0)
                     {
                         return;
                     }
                     else
                     {
                         for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                         {
                             EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                         }
                         if (!string.IsNullOrEmpty(EmpCodes))
                         {
                             EmpCodes = EmpCodes.Substring(1);
                         }
                         strsql = "select tblemployee.empname 'Name',tblemployee.paycode 'Paycode',tblcompany.companyname 'CompanyName', " +
                                 "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                                 "sum(tbltimeregister.absentvalue) 'Absent',sum(tbltimeregister.presentvalue)'Present',sum(tbltimeregister.leavevalue) 'Leavevalue', " +
                                 "sum(tbltimeregister.holiday_value) 'holiday',sum(tbltimeregister.Wo_value) 'WO', " +
                                 "Cast(sum(tbltimeregister.hoursworked)/ 60 as Varchar) +':' +Cast(sum(tbltimeregister.hoursworked) % 60 as Varchar)'TotalHours', " +
                                 "Cast(sum(tbltimeregister.otduration)/ 60 as Varchar) +':' +Cast(sum(tbltimeregister.otduration) % 60 as Varchar) 'OT' " +
                                 "from tblemployee join tbltimeregister " +
                                 "on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                                 "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                                 "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                                 "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                 "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                                 "where tbltimeregister.dateoffice between  " +
                                 "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                                 "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                                 "And (tblemployee.LeavingDate >= '" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 " +
                                 "  and tblemployee.paycode  in (" + EmpCodes.ToString().Trim() + ")    " +
                                 "group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                                 "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";
                     }
                 }
                 else
                 {
                     if (Selection.ToString() != "")                    
                     {
                         strsql = "select tblemployee.empname 'Name',tblemployee.paycode 'Paycode',tblcompany.companyname 'CompanyName', " +
                                 "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                                 "sum(tbltimeregister.absentvalue) 'Absent',sum(tbltimeregister.presentvalue)'Present',sum(tbltimeregister.leavevalue) 'Leavevalue', " +
                                 "sum(tbltimeregister.holiday_value) 'holiday',sum(tbltimeregister.Wo_value) 'WO', " +
                                 "Cast(sum(tbltimeregister.hoursworked)/ 60 as Varchar) +':' +Cast(sum(tbltimeregister.hoursworked) % 60 as Varchar)'TotalHours', " +
                                 "Cast(sum(tbltimeregister.otduration)/ 60 as Varchar) +':' +Cast(sum(tbltimeregister.otduration) % 60 as Varchar) 'OT' " +
                                 "from tblemployee join tbltimeregister " +
                                 "on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                                 "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                                 "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                                 "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                 "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                                 "where tbltimeregister.dateoffice between  " +
                                 "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                                 "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                                 "And (tblemployee.LeavingDate >= '" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 " +
                                 //" and tblemployee.paycode in (" + Session["Emp_Codes"].ToString() + ")   " +
                                 " and " + Session["Emp_Codes"].ToString() + "   " +
                                 "group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                                 "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";
                     }
                     else
                     {
                         ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Please Select Employees for this Report');", true);
                         return;
                     }                    
                 }                
                 ds = con.FillDataSet(strsql);
                 if (ds.Tables[0].Rows.Count > 0)
                 {
                     Session["DateFrom"] = datefrom.ToString();
                     Session["DateTo"] = dateTo.ToString();
                     Session["NewDataSet"] = ds;
                     OpenChilePage("Reports/MonthlyReports/Rpt_MontlyPerformance.aspx");
                 }
                 else
                 {
                     ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                     return;
                 }
             }*/

            else if (radEmployeewiseAttendance.Checked)
            {

                strsql = "select tblemployee.presentcardno 'Card',tblemployee.empname 'Name',tbltimeregister.paycode 'PayCode',tblcompany.companyname 'Company', " +
                        "tblemployee.paycode 'Pay',tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "convert(decimal(10,2),sum(tbltimeregister.absentvalue)) 'Absent' ,convert(decimal(10,2),sum(tbltimeregister.presentvalue))'Present',convert(decimal(10,2),sum(tbltimeregister.leavevalue)) 'Leavevalue', " +
                        "convert(decimal(10,2),sum(tbltimeregister.holiday_value)) 'holiday',convert(decimal(10,2),sum(tbltimeregister.Wo_value)) 'WO', " +
                        "substring(convert(varchar(20),dateadd(minute,sum(tbltimeregister.otduration),0),108),0,6) 'OT'," +
                        "sum(tbltimeregister.otamount) 'OTAmount' " +
                        "from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN  " +
                        "join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                        "where tbltimeregister.dateoffice between " +
                        "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                        "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tbltimeregister.paycode,tblemployee.presentcardno,tblemployee.empname,tblcompany.companyname, " +
                "tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.paycode " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "Absent";
                            dTbl.Rows[0][6] = "Weekly Off";
                            dTbl.Rows[0][7] = "Holiday";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "OT";
                            dTbl.Rows[0][10] = "OT Amount";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Employee Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to "+toDate.ToString("dd-MM-yyyy")+" ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Employee Wise Attendance from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = msg = "Employee Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EmployeeWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EmployeeWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "Absent";
                            dTbl.Rows[0][6] = "Weekly Off";
                            dTbl.Rows[0][7] = "Holiday";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "OT";
                            dTbl.Rows[0][10] = "OT Amount";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Employee Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to "+toDate.ToString("dd-MM-yyyy")+" ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Employee Wise Attendance from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = msg = "Employee Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EmployeeWiseAttendance.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        } 
                    }
                    //Response.Redirect("Rpt_MonthlyEmployeeAttendance.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radDepartmentWise.Checked)
            {
                string DeptCode = "";
                string ComCode = "";
                DataSet dsAtt = new DataSet();
                /*strsql = "select tblemployee.departmentcode 'DeptCode',tbldepartment.departmentname 'DeptName', " +
                        "count(tblemployee.paycode) 'PayCode',tblcompany.companyname 'Company',tblcompany.companyCode 'CompanyID' from tblemployee  " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode join " +
                        "tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tblemployee.dateofjoin < '" + toDate.ToString("yyyy-MM-dd") + "' and " +
                        "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and " +
                        "tblemployee.active ='Y' ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += "group by tblemployee.departmentcode,tbldepartment.departmentname,tblcompany.companyname,tblcompany.companyCode ";*/

                strsql = "select tblemployee.departmentcode 'DeptCode',tbldepartment.departmentname 'DeptName', " +
                        "count(tblemployee.paycode) 'PayCode' from tblemployee  " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode join " +
                        " tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        "where tblemployee.dateofjoin < '" + toDate.ToString("yyyy-MM-dd") + "' and " +
                        "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and " +
                        "tblemployee.active ='Y' and tbldepartment.companycode=" + Session["Auth_Comp"].ToString().Trim() + " ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += "group by tblemployee.departmentcode,tbldepartment.departmentname";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("DeptCode");
                            dTbl.Columns.Add("DeptName");
                            dTbl.Columns.Add("TotalEmp");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Holiday");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Dept + " Code";
                            dTbl.Rows[0][2] = Global.getEmpInfo._Dept + " Name";
                            dTbl.Rows[0][3] = "Total " + Global.getEmpInfo._Msg;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "Absent";
                            dTbl.Rows[0][6] = "Leave";
                            dTbl.Rows[0][7] = "Weekly Off";
                            dTbl.Rows[0][8] = "Holiday";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();

                                    DeptCode = ds.Tables[0].Rows[i1][0].ToString();
                                    strsql = "select sum(presentValue),Sum(AbsentValue),Sum(leavevalue),Sum(Wo_value),sum(holiday_value) from tbltimeregister where " +
                                        " paycode in (select paycode from tblemployee where  departmentcode='" + DeptCode.ToString() + "' and companycode=" + Session["Auth_Comp"].ToString().Trim() + ") " +
                                        " and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' ";
                                    dsAtt = con.FillDataSet(strsql);
                                    if (dsAtt.Tables[0].Rows.Count > 0)
                                    {
                                        dTbl.Rows[ct][4] = dsAtt.Tables[0].Rows[0][0].ToString();
                                        dTbl.Rows[ct][5] = dsAtt.Tables[0].Rows[0][1].ToString();
                                        dTbl.Rows[ct][6] = dsAtt.Tables[0].Rows[0][2].ToString();
                                        dTbl.Rows[ct][7] = dsAtt.Tables[0].Rows[0][3].ToString();
                                        dTbl.Rows[ct][8] = dsAtt.Tables[0].Rows[0][4].ToString();
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=DepartmentWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/DepartmentWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("DeptCode");
                            dTbl.Columns.Add("DeptName");
                            dTbl.Columns.Add("TotalEmp");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Holiday");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Dept + " Code";
                            dTbl.Rows[0][2] = Global.getEmpInfo._Dept + " Name";
                            dTbl.Rows[0][3] = "Total " + Global.getEmpInfo._Msg;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "Absent";
                            dTbl.Rows[0][6] = "Leave";
                            dTbl.Rows[0][7] = "Weekly Off";
                            dTbl.Rows[0][8] = "Holiday";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();

                                    DeptCode = ds.Tables[0].Rows[i1][0].ToString();
                                    strsql = "select sum(presentValue),Sum(AbsentValue),Sum(leavevalue),Sum(Wo_value),sum(holiday_value) from tbltimeregister where " +
                                        " paycode in (select paycode from tblemployee where  departmentcode='" + DeptCode.ToString() + "' and companycode=" + Session["Auth_Comp"].ToString().Trim() + ") " +
                                        " and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' ";
                                    dsAtt = con.FillDataSet(strsql);
                                    if (dsAtt.Tables[0].Rows.Count > 0)
                                    {
                                        dTbl.Rows[ct][4] = dsAtt.Tables[0].Rows[0][0].ToString();
                                        dTbl.Rows[ct][5] = dsAtt.Tables[0].Rows[0][1].ToString();
                                        dTbl.Rows[ct][6] = dsAtt.Tables[0].Rows[0][2].ToString();
                                        dTbl.Rows[ct][7] = dsAtt.Tables[0].Rows[0][3].ToString();
                                        dTbl.Rows[ct][8] = dsAtt.Tables[0].Rows[0][4].ToString();
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "DeptSummary.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                }
            }
            else if (radMonthlyLate.Checked)
            {
                strsql = "select sno='', case  when tbltimeregister.latearrival != 0 then substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6) else null end as 'Late', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "tbltimeregister.paycode 'paycode',datename(dd,tbltimeregister.dateoffice)'Date',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "Total=(SELECT Case When sum(latearrival)/60 = 1 Then Convert(nvarchar(10),sum(latearrival)/60) Else Convert(nvarchar(10),sum(latearrival)/60)  End + ':' + " +
                        "Case When sum(latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(latearrival)%60) Else '0'+Convert(nvarchar(2),sum(latearrival)%60) End " +
                        "from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "from tbltimeregister join tblemployee on tblemployee.SSN=tbltimeregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                        "where tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.SSN in " +
                        "(select SSN from tbltimeregister where latearrival > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += "group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.latearrival,tbltimeregister.PAYCODE,tbltimeregister.DateOFFICE,tblemployee.SSN " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "  ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (radExcel.Checked)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN  " +
                                         " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                         " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            string SSN = "";
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.latearrival),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and latearrival > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.latearrival,0),108) 'latearrival'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["latearrival"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            //  msg = "Late Arrival Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Late Arrival Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late Arrival Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }

                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LateArrivalRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LateArrivalRegister.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN  " +
                                         " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                         " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            string SSN = "";
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.latearrival),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and latearrival > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.latearrival,0),108) 'latearrival'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["latearrival"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            //  msg = "Late Arrival Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Late Arrival Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late Arrival Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }

                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "LateArrivalRegister.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radEarlyDeparture.Checked)
            {
                strsql = "select sno='', case  when tbltimeregister.earlydeparture != 0 then substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlydeparture,0),108),0,6) else null end as 'Late', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "tbltimeregister.paycode 'paycode',datename(dd,tbltimeregister.dateoffice)'Date',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "Total=(SELECT Case When sum(earlydeparture)/60 = 1 Then Convert(nvarchar(10),sum(earlydeparture)/60) Else Convert(nvarchar(10),sum(earlydeparture)/60)  End + ':' + " +
                        "Case When sum(earlydeparture)%60 >= 10 Then Convert(nvarchar(2),sum(earlydeparture)%60) Else '0'+Convert(nvarchar(2),sum(earlydeparture)%60) End " +
                        "from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "from tbltimeregister join tblemployee on tblemployee.SSN=tbltimeregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        "join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                        "where tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.SSN in " +
                        "(select SSN from tbltimeregister where earlydeparture > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.earlydeparture,tbltimeregister.PAYCODE,tbltimeregister.DateOFFICE " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            string SSN = "";
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.earlydeparture),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and earlydeparture > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.earlydeparture,0),108) 'earlydeparture'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["earlydeparture"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Early Departure Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Departure Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            //msg = "Early Departure Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EarlyDepartureRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EarlyDeparture.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            string SSN = "";
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.earlydeparture),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and earlydeparture > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.earlydeparture,0),108) 'earlydeparture'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["earlydeparture"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Early Departure Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Departure Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            //msg = "Early Departure Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EarlyDepartureRegister.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    //Response.Redirect("Rpt_MontlyEarlyDeparture.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radAbsent.Checked)
            {
                strsql = "select sno='', case  when tbltimeregister.absentvalue != 0 then tbltimeregister.absentvalue else null end as 'Late', " +
                          "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                          "tbltimeregister.paycode 'paycode',datename(dd,tbltimeregister.dateoffice)'Date',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                          "Total=(select convert(decimal(10,2),sum(tbltimeregister.absentvalue)) " +
                          "from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between  " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                          " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                          "from tbltimeregister join tblemployee on tblemployee.SSN=tbltimeregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                          "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                          "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                          "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                          "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                          "join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                          "where tbltimeregister.dateoffice between  " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                          " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.paycode in " +
                          "(select paycode from tbltimeregister where absentvalue > 0 and dateoffice between " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                          " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                          "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.absentvalue,tbltimeregister.PAYCODE,tbltimeregister.DateOFFICE,tblemployee.SSN " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                /*strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/

                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                                /*strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',TblTimeregister.absentvalue 'absentvalue'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["absentvalue"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim();
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Absenteeism Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Absenteeism Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }
                            // msg = "Absenteeism Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=AbsenteeismRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/AbsenteeismDeparture.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                /*strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/

                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                                /*strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',TblTimeregister.absentvalue 'absentvalue'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["absentvalue"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim();
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Absenteeism Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Absenteeism Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }
                            // msg = "Absenteeism Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "AbsentRegister.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            //Shift Wise presence
            else if (radshiftwise.Checked)
            {
                DataSet dsTime = new DataSet();
                double Present = 0;
                double Absent = 0;
                double WO = 0;
                double Leave = 0;
                double OT = 0;
                double OTAmt = 0;

                strsql = " select tblemployee.paycode'paycode',tblemployee.empname 'empname', " +
                         " tblemployee.presentcardno 'presentcardno',  " +
                         " tblEmployeeShiftMaster.paycode 'code', " +
                         " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                         " tblcompany.companyname 'company',tblemployee.SSN from tblemployee join tblcompany on " +
                         " tblemployee.companycode=tblcompany.companycode " +
                         " join tblEmployeeShiftMaster on tblemployee.SSN=tblEmployeeShiftMaster.SSN " +
                         " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                         " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                         " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                         " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                         " where tblemployee.active='Y' ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 " +
                          " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblEmployeeShiftMaster.PAYCODE,tblemployee.SSN " +
                          " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "", SSN = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Present";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "W.Off";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "OT";
                            dTbl.Rows[0][10] = "OT Amt";

                            int rowCount = 1;
                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    Present = 0;
                                    Absent = 0;
                                    WO = 0;
                                    Leave = 0;
                                    OT = 0;
                                    OTAmt = 0;
                                    dTbl.Rows[ct][0] = rowCount.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    strsql1 = "select  Paycode,Shiftattended,convert(decimal(10,2),sum(Presentvalue))'Present',convert(decimal(10,2),sum(absentvalue))'Absent',convert(decimal(10,2),sum(WO_VALUE))'WeeklyOff', " +
                                            "convert(decimal(10,2),sum(leavevalue))'LeaveValue',sum(otduration) 'OT',convert(decimal(10,2),sum(otamount))'OTAmount' " +
                                            " from tbltimeregister where dateoffice between convert(datetime,convert(char(10),'" + FromDate.ToString() + "',103),103) and " +
                                            "convert(datetime,convert(char(10),'" + toDate.ToString() + "',103),103) and SSN = '" + ds.Tables[0].Rows[i1]["SSN"].ToString().Trim() + "' " +
                                            "group by shiftattended,paycode order by paycode";
                                    dsTime = con.FillDataSet(strsql1);
                                    if (dsTime.Tables[0].Rows.Count > 0)
                                    {
                                        for (int kp = 0; kp < dsTime.Tables[0].Rows.Count; kp++)
                                        {
                                            dTbl.Rows[ct][4] = dsTime.Tables[0].Rows[kp][1].ToString();
                                            dTbl.Rows[ct][5] = dsTime.Tables[0].Rows[kp][2].ToString();
                                            dTbl.Rows[ct][6] = dsTime.Tables[0].Rows[kp][3].ToString();
                                            dTbl.Rows[ct][7] = dsTime.Tables[0].Rows[kp][4].ToString();
                                            dTbl.Rows[ct][8] = dsTime.Tables[0].Rows[kp][5].ToString();
                                            dTbl.Rows[ct][9] = hm.minutetohour(dsTime.Tables[0].Rows[kp][6].ToString());
                                            dTbl.Rows[ct][10] = dsTime.Tables[0].Rows[kp][7].ToString();

                                            Present += Convert.ToDouble(dsTime.Tables[0].Rows[kp][2].ToString());
                                            Absent += Convert.ToDouble(dsTime.Tables[0].Rows[kp][3].ToString());
                                            WO += Convert.ToDouble(dsTime.Tables[0].Rows[kp][4].ToString());
                                            leave += Convert.ToDouble(dsTime.Tables[0].Rows[kp][5].ToString());
                                            OT += Convert.ToDouble(dsTime.Tables[0].Rows[kp][6].ToString());
                                            OTAmt += Convert.ToDouble(dsTime.Tables[0].Rows[kp][7].ToString());

                                            dRow = dTbl.NewRow();
                                            dTbl.Rows.Add(dRow);
                                            ct = ct + 1;
                                        }
                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);
                                        ct = ct + 1;
                                        dTbl.Rows[ct][4] = "Sub Total";
                                        dTbl.Rows[ct][5] = Present.ToString();
                                        dTbl.Rows[ct][6] = Absent.ToString();
                                        dTbl.Rows[ct][7] = WO.ToString();
                                        dTbl.Rows[ct][8] = Leave.ToString();
                                        dTbl.Rows[ct][9] = hm.minutetohour(OT.ToString());
                                        dTbl.Rows[ct][10] = OTAmt.ToString();

                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                    rowCount = rowCount + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDateNepal.ToString("dd-MM-yyyy") + " TO " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                        }
                                    }
                                    else if ((i == 3))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                        }
                                    }
                                    else if ((i == 4))
                                    {
                                        if (Tmp.ToString().Trim() == "Sub Total")
                                        {
                                            sb.Append("<td style='width:60px;text-align: left;font-weight:bold;font-size:10px;color:red'>" + Tmp + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                    else
                                    {
                                        sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ShiftWiseAtt.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ShiftWiseAtt.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "", SSN = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Present";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "W.Off";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "OT";
                            dTbl.Rows[0][10] = "OT Amt";

                            int rowCount = 1;
                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    Present = 0;
                                    Absent = 0;
                                    WO = 0;
                                    Leave = 0;
                                    OT = 0;
                                    OTAmt = 0;
                                    dTbl.Rows[ct][0] = rowCount.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    strsql1 = "select  Paycode,Shiftattended,convert(decimal(10,2),sum(Presentvalue))'Present',convert(decimal(10,2),sum(absentvalue))'Absent',convert(decimal(10,2),sum(WO_VALUE))'WeeklyOff', " +
                                            "convert(decimal(10,2),sum(leavevalue))'LeaveValue',sum(otduration) 'OT',convert(decimal(10,2),sum(otamount))'OTAmount' " +
                                            " from tbltimeregister where dateoffice between convert(datetime,convert(char(10),'" + FromDate.ToString() + "',103),103) and " +
                                            "convert(datetime,convert(char(10),'" + toDate.ToString() + "',103),103) and SSN = '" + ds.Tables[0].Rows[i1]["SSN"].ToString().Trim() + "' " +
                                            "group by shiftattended,paycode order by paycode";
                                    dsTime = con.FillDataSet(strsql1);
                                    if (dsTime.Tables[0].Rows.Count > 0)
                                    {
                                        for (int kp = 0; kp < dsTime.Tables[0].Rows.Count; kp++)
                                        {
                                            dTbl.Rows[ct][4] = dsTime.Tables[0].Rows[kp][1].ToString();
                                            dTbl.Rows[ct][5] = dsTime.Tables[0].Rows[kp][2].ToString();
                                            dTbl.Rows[ct][6] = dsTime.Tables[0].Rows[kp][3].ToString();
                                            dTbl.Rows[ct][7] = dsTime.Tables[0].Rows[kp][4].ToString();
                                            dTbl.Rows[ct][8] = dsTime.Tables[0].Rows[kp][5].ToString();
                                            dTbl.Rows[ct][9] = hm.minutetohour(dsTime.Tables[0].Rows[kp][6].ToString());
                                            dTbl.Rows[ct][10] = dsTime.Tables[0].Rows[kp][7].ToString();

                                            Present += Convert.ToDouble(dsTime.Tables[0].Rows[kp][2].ToString());
                                            Absent += Convert.ToDouble(dsTime.Tables[0].Rows[kp][3].ToString());
                                            WO += Convert.ToDouble(dsTime.Tables[0].Rows[kp][4].ToString());
                                            leave += Convert.ToDouble(dsTime.Tables[0].Rows[kp][5].ToString());
                                            OT += Convert.ToDouble(dsTime.Tables[0].Rows[kp][6].ToString());
                                            OTAmt += Convert.ToDouble(dsTime.Tables[0].Rows[kp][7].ToString());

                                            dRow = dTbl.NewRow();
                                            dTbl.Rows.Add(dRow);
                                            ct = ct + 1;
                                        }
                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);
                                        ct = ct + 1;
                                        dTbl.Rows[ct][4] = "Sub Total";
                                        dTbl.Rows[ct][5] = Present.ToString();
                                        dTbl.Rows[ct][6] = Absent.ToString();
                                        dTbl.Rows[ct][7] = WO.ToString();
                                        dTbl.Rows[ct][8] = Leave.ToString();
                                        dTbl.Rows[ct][9] = hm.minutetohour(OT.ToString());
                                        dTbl.Rows[ct][10] = OTAmt.ToString();

                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                    rowCount = rowCount + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDateNepal.ToString("dd-MM-yyyy") + " TO " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                        }
                                    }
                                    else if ((i == 3))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                        }
                                    }
                                    else if ((i == 4))
                                    {
                                        if (Tmp.ToString().Trim() == "Sub Total")
                                        {
                                            sb.Append("<td style='width:60px;text-align: left;font-weight:bold;font-size:10px;color:red'>" + Tmp + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                    else
                                    {
                                        sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "ShiftWiseAttendance.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Rpt_MonthlyShiftWise.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            //OT Register
            else if (radMontlyOT.Checked)
            {
                strsql = " select sno='', tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        " datename(dd,tbltimeregister.dateoffice) 'Date',case when tbltimeregister.otduration!=0 then " +
                    //"Cast(otduration/ 60 as Varchar) +'.' +Cast(otduration % 60 as Varchar) else null end as 'OT' , " +
                        " substring(convert(varchar(20),dateadd(minute,tbltimeregister.otduration,0),108),0,6) else null end as 'OT' ," +
                        " Total=(select Case When sum(tbltimeregister.otduration)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.otduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.otduration)/60)  End + ':' + " +
                        " Case When sum(tbltimeregister.otduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) End " +
                        " from tbltimeregister where tblemployee.SSN=tbltimeregister.SSN and tbltimeregister.dateoffice between  " + "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "'),tblemployee.SSN  " +
                        " from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN " +
                        " join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and  " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.SSN in " +
                        " (select SSN from tbltimeregister where otduration > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
               " tbltimeregister.DateOFFICE,tbltimeregister.OTDURATION,tblemployee.SSN " +
               " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalOTduration=(select isnull(sum(tbltimeregister.otduration),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and otduration > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.otduration,0),108) 'otduration'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                if (dsStatus.Tables[0].Rows[st]["otduration"].ToString() != "00:00")
                                                {
                                                    dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["otduration"].ToString().Trim();
                                                }
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalOTduration"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            // msg = "Over Time Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Over Time Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Over Time Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }




                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=OverTimeRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/OverTimeRegister.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalOTduration=(select isnull(sum(tbltimeregister.otduration),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and otduration > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.otduration,0),108) 'otduration'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                if (dsStatus.Tables[0].Rows[st]["otduration"].ToString() != "00:00")
                                                {
                                                    dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["otduration"].ToString().Trim();
                                                }
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalOTduration"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            // msg = "Over Time Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Over Time Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Over Time Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }




                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "MonthlyOT.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            //Monthly OT Summary
            else if (radOT.Checked)
            {
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                    "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                    "Case When sum(tbltimeregister.otduration)/60 < 10 Then '0'+Convert(nvarchar(10),sum(tbltimeregister.otduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.otduration)/60)  End + ':' + " +
                    "Case When sum(tbltimeregister.otduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) End  as 'OT', " +
                    "convert(decimal(10,2),Sum(otamount)) 'Amount',tblemployee.SSN " +
                    "from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN join tblcompany on  " +
                    "tblemployee.companycode=tblcompany.companycode " +
                    "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                    "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                    "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                    "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                    " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                    "where tbltimeregister.SSN in " +
                    "(select SSN from tbltimeregister where otduration > 0 and dateoffice between " +
                    " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                    " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                    "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.paycode, " +
                "tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.SSN " +
                "order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Session["DateFrom"] = datefrom.ToString();
                    Session["DateTo"] = dateTo.ToString();
                    Session["NewDataSet"] = ds;
                    OpenChilePage("Reports/MonthlyReports/Rpt_MontlyOTSummary.aspx");
                    //Response.Redirect("Rpt_MontlyOTSummary.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radOverStay.Checked)
            {
                strsql = " select sno='', tblemployee.paycode 'Paycode',tblemployee.empname 'Name', " +
                        " tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        " datename(dd,tbltimeregister.dateoffice) 'Date'," +
                        " case when tbltimeregister.osduration!=0 then substring(convert(varchar(20),dateadd(minute,tbltimeregister.osduration,0),108),0,6) else null end as 'OverStay' , " +
                        " Total=(select Case When sum(tbltimeregister.osduration)/60 < 10 Then '0'+Convert(nvarchar(10),sum(tbltimeregister.osduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.osduration)/60)  End + ':' + " +
                        " Case When sum(tbltimeregister.osduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) End " +
                        " from tbltimeregister where tblemployee.SSN=tbltimeregister.SSN and tbltimeregister.dateoffice between " +
                        "  '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        "  '" + toDate.ToString("yyyy-MM-dd") + "'),tblemployee.SSN " +
                        " from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " where dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' " +
                        " and  tbltimeregister.SSN in (select SSN from tbltimeregister where osduration > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        " and tblemployee.active='Y' and " +
                        " (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                " tbltimeregister.DateOFFICE,tbltimeregister.OSDURATION,tblemployee.SSN " +
                " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalOsduration=(select isnull(sum(tbltimeregister.osduration),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and osduration > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.osduration,0),108) 'osduration'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                if (dsStatus.Tables[0].Rows[st]["osduration"].ToString() != "00:00")
                                                {
                                                    dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["osduration"].ToString().Trim();
                                                }
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalOsduration"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            //msg = "OverStay Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "OverStay Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "OverStay Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=OverStayRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/OverStayRegister.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = "PayCode";
                            dTbl.Rows[i]["CardNo"] = "Card No";
                            dTbl.Rows[i]["Name"] = "Employee Name";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null)  ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = " Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,tblemployee.SSN " +
                                             " from tblemployee join tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode  " +
                                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblcompany on tblemployee.Companycode=tblCompany.Companycode join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                            " where tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " ";
                                strsql += " " + CompanySelection.ToString() + " ";
                                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalOsduration=(select isnull(sum(tbltimeregister.osduration),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and osduration > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.osduration,0),108) 'osduration'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                if (dsStatus.Tables[0].Rows[st]["osduration"].ToString() != "00:00")
                                                {
                                                    dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["osduration"].ToString().Trim();
                                                }
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalOsduration"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            //msg = "OverStay Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "OverStay Register for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "OverStay Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "OverStayRegister.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    //Response.Redirect("Rpt_MontlyOverStay.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radShiftSchedule.Checked)
            {
                strsql = " select sno='', tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'CardNo', " +
                        " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        " tblemployee.designation 'Designation',tblemployeeshiftmaster.shifttype 'ShiftType', " +
                        " tblemployeeshiftmaster.shift 'ShiftPattern',tblemployeeshiftmaster.firstoffday 'FirstOff', " +
                        " tblemployeeshiftmaster.secondoffday '2ndOff',tblemployeeshiftmaster.secondofftype '2ndOffType', " +
                        " tblemployeeshiftmaster.alternate_off_days '2ndOffDays', " +
                        " datename(dd,tbltimeregister.dateoffice) 'Date',tbltimeregister.shift 'Shift', " +
                        " tblcompany.companyname 'Company',datename(mm,'" + FromDate.ToString("yyyy-MM-dd") + "') 'Month',tblemployee.SSN " +
                        " from tblemployee join tblemployeeshiftmaster on  " +
                        " tblemployee.SSN=tblemployeeshiftmaster.SSN join tbltimeregister on " +
                        " tblemployee.SSN=tbltimeregister.SSN join tblcompany on " +
                        " tblemployee.companycode=tblcompany.companycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " where tbltimeregister.dateoffice between Dateadd(m, Datediff(m, 30, '" + FromDate.ToString("yyyy-MM-dd") + "' ), 0) " +
                        " and Dateadd(s,-1,Dateadd(mm, Datediff(m,0,'" + FromDate.ToString("yyyy-MM-dd") + "')+1,0)) " +
                        " and tblemployee.active='Y' " +
                        " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Card");
                            dTbl.Columns.Add("Paycode");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Designation");
                            dTbl.Columns.Add("Shifttype");
                            dTbl.Columns.Add("Shiftpattern");

                            dTbl.Columns.Add("WO1");
                            dTbl.Columns.Add("WO2");
                            dTbl.Columns.Add("WO2Type");
                            dTbl.Columns.Add("WO2Days");

                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }

                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i][0] = "SNo";
                            dTbl.Rows[i][1] = "Card No";
                            dTbl.Rows[i][2] = "PayCode";
                            dTbl.Rows[i][3] = "Employee Name";
                            dTbl.Rows[i][4] = Global.getEmpInfo._Designation;
                            dTbl.Rows[i][5] = "Shift Type";
                            dTbl.Rows[i][6] = "Shift Pattern";
                            dTbl.Rows[i][7] = "1st WO";
                            dTbl.Rows[i][8] = "2nd WO";
                            dTbl.Rows[i][9] = "2nd WO Type";
                            dTbl.Rows[i][10] = "2nd WO Days";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }

                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            /*if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }*/

                            strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name'," +
                               " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                               " tblemployee.designation 'Designation',tblemployeeshiftmaster.shifttype 'ShiftType', " +
                               " tblemployeeshiftmaster.shift 'ShiftPattern',tblemployeeshiftmaster.firstoffday 'FirstOff', " +
                               " tblemployeeshiftmaster.secondoffday '2ndOff',tblemployeeshiftmaster.secondofftype '2ndOffType', " +
                               " tblemployeeshiftmaster.alternate_off_days '2ndOffDays', " +
                               " tblcompany.companyname 'Company',datename(mm,'" + FromDate.ToString("yyyy-MM-dd") + "') 'Month',tblemployee.SSN " +
                               " from tblemployee join tblemployeeshiftmaster on  " +
                               " tblemployee.SSN=tblemployeeshiftmaster.SSN join tblcompany on " +
                               " tblemployee.companycode=tblcompany.companycode " +
                               " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                               " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                               " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                               " join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                               " where tblemployee.active='Y' " +
                               " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                            if (Selection.ToString().Trim() != "")
                            {
                                strsql += Selection.ToString();
                            }
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";


                            //and tblemployee.paycode='1389' 
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i][0] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i][2] = Tmp;  //dr["Paycode"].ToString().Trim();
                                    dTbl.Rows[i][1] = dsMuster.Tables[0].Rows[ms]["presentcardno"].ToString().Trim();
                                    dTbl.Rows[i][3] = dsMuster.Tables[0].Rows[ms]["Name"].ToString().Trim();
                                    dTbl.Rows[i][4] = dsMuster.Tables[0].Rows[ms]["Designation"].ToString().Trim();
                                    dTbl.Rows[i][5] = dsMuster.Tables[0].Rows[ms]["shifttype"].ToString().Trim();
                                    dTbl.Rows[i][6] = dsMuster.Tables[0].Rows[ms]["shiftpattern"].ToString().Trim();
                                    dTbl.Rows[i][7] = dsMuster.Tables[0].Rows[ms]["FirstOff"].ToString().Trim();
                                    dTbl.Rows[i][8] = dsMuster.Tables[0].Rows[ms]["2ndOff"].ToString().Trim();
                                    dTbl.Rows[i][9] = dsMuster.Tables[0].Rows[ms]["2ndOfftype"].ToString().Trim();
                                    dTbl.Rows[i][10] = dsMuster.Tables[0].Rows[ms]["2ndOffdays"].ToString().Trim();
                                    x = 11;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift  " +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["Shift"].ToString().Trim();
                                                x++;
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            // msg = "Shift Schedule for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Shift Schedule for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Shift Schedule for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 3) || (i == 4))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:150px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else
                                    {
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:50px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //   sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ShiftSchedule.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ShiftSchedule.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Card");
                            dTbl.Columns.Add("Paycode");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Designation");
                            dTbl.Columns.Add("Shifttype");
                            dTbl.Columns.Add("Shiftpattern");

                            dTbl.Columns.Add("WO1");
                            dTbl.Columns.Add("WO2");
                            dTbl.Columns.Add("WO2Type");
                            dTbl.Columns.Add("WO2Days");

                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }

                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i][0] = "SNo";
                            dTbl.Rows[i][1] = "Card No";
                            dTbl.Rows[i][2] = "PayCode";
                            dTbl.Rows[i][3] = "Employee Name";
                            dTbl.Rows[i][4] = Global.getEmpInfo._Designation;
                            dTbl.Rows[i][5] = "Shift Type";
                            dTbl.Rows[i][6] = "Shift Pattern";
                            dTbl.Rows[i][7] = "1st WO";
                            dTbl.Rows[i][8] = "2nd WO";
                            dTbl.Rows[i][9] = "2nd WO Type";
                            dTbl.Rows[i][10] = "2nd WO Days";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }

                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            /*if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }*/

                            strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name'," +
                               " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                               " tblemployee.designation 'Designation',tblemployeeshiftmaster.shifttype 'ShiftType', " +
                               " tblemployeeshiftmaster.shift 'ShiftPattern',tblemployeeshiftmaster.firstoffday 'FirstOff', " +
                               " tblemployeeshiftmaster.secondoffday '2ndOff',tblemployeeshiftmaster.secondofftype '2ndOffType', " +
                               " tblemployeeshiftmaster.alternate_off_days '2ndOffDays', " +
                               " tblcompany.companyname 'Company',datename(mm,'" + FromDate.ToString("yyyy-MM-dd") + "') 'Month',tblemployee.SSN " +
                               " from tblemployee join tblemployeeshiftmaster on  " +
                               " tblemployee.SSN=tblemployeeshiftmaster.SSN join tblcompany on " +
                               " tblemployee.companycode=tblcompany.companycode " +
                               " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                               " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                               " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                               " join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                               " where tblemployee.active='Y' " +
                               " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                            if (Selection.ToString().Trim() != "")
                            {
                                strsql += Selection.ToString();
                            }
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";


                            //and tblemployee.paycode='1389' 
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string SSN = "";
                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i][0] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                    dTbl.Rows[i][2] = Tmp;  //dr["Paycode"].ToString().Trim();
                                    dTbl.Rows[i][1] = dsMuster.Tables[0].Rows[ms]["presentcardno"].ToString().Trim();
                                    dTbl.Rows[i][3] = dsMuster.Tables[0].Rows[ms]["Name"].ToString().Trim();
                                    dTbl.Rows[i][4] = dsMuster.Tables[0].Rows[ms]["Designation"].ToString().Trim();
                                    dTbl.Rows[i][5] = dsMuster.Tables[0].Rows[ms]["shifttype"].ToString().Trim();
                                    dTbl.Rows[i][6] = dsMuster.Tables[0].Rows[ms]["shiftpattern"].ToString().Trim();
                                    dTbl.Rows[i][7] = dsMuster.Tables[0].Rows[ms]["FirstOff"].ToString().Trim();
                                    dTbl.Rows[i][8] = dsMuster.Tables[0].Rows[ms]["2ndOff"].ToString().Trim();
                                    dTbl.Rows[i][9] = dsMuster.Tables[0].Rows[ms]["2ndOfftype"].ToString().Trim();
                                    dTbl.Rows[i][10] = dsMuster.Tables[0].Rows[ms]["2ndOffdays"].ToString().Trim();
                                    x = 11;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift  " +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["Shift"].ToString().Trim();
                                                x++;
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            // msg = "Shift Schedule for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Shift Schedule for the month of " + FromDateNepal.ToString("MMMM") + " " + FromDateNepal.Year.ToString("0000") + " from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Shift Schedule for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 3) || (i == 4))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:150px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else
                                    {
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:50px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //   sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "ShiftSchedule.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    //Response.Redirect("Rpt_MontlyShiftSchedule.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radEarlyDeptSummary.Checked)
            {
                double TotalDays = 0;
                /*strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "total=(select convert(decimal(10,2),count(tbltimeregister.earlydeparture)) from tbltimeregister where tbltimeregister.earlydeparture > 0 and " +
                            "tbltimeregister.dateoffice between " +
                            "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                            "and tblemployee.paycode=tbltimeregister.paycode), " +
                            "Case When sum(tbltimeregister.earlydeparture)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60) Else Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.earlydeparture)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) End 'EarlyDeparture' , " +
                            "Cast(sum(tbltimeregister.earlydeparture) / 60 as Varchar) +'.' +Cast(sum(tbltimeregister.earlydeparture) % 60 as Varchar) 'EarlyDeparture'  " +
                            "from tblemployee  join tbltimeregister  on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            "where tbltimeregister.paycode in  " +
                            "(select tbltimeregister.paycode from tbltimeregister where tbltimeregister.earlydeparture > 0 and tbltimeregister.dateoffice between  " +
                            "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            "'" + toDate.ToString("yyyy-MM-dd") + "') and " +
                            "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " ;
                            if (Selection.ToString().Trim() != "")
                            {
                                strsql += Selection.ToString();
                            }
                            strsql +=" group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                            ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                            "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";*/



                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', count(earlydeparture), " +
                        " Cast(sum(tbltimeregister.earlydeparture) / 60 as Varchar) +'.' +Cast(sum(tbltimeregister.earlydeparture) % 60 as Varchar) 'EarlyDeparture' from " +
                        " tblemployee  join tbltimeregister  on tblemployee.SSN=tbltimeregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                         " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " where DateOFFICE between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and EARLYDEPARTURE > 0 and (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += "group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname ,tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno  order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "  ";



                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("ED");
                            dTbl.Columns.Add("Days");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Early Departure";
                            dTbl.Rows[0][5] = "No. of Days";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][8].ToString();
                                    TotalDays = TotalDays + Convert.ToDouble(ds.Tables[0].Rows[i1][8].ToString());

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;

                                dTbl.Rows[ct][0] = "";
                                dTbl.Rows[ct][1] = "";
                                dTbl.Rows[ct][2] = "Total";
                                dTbl.Rows[ct][3] = "";
                                dTbl.Rows[ct][4] = "";
                                dTbl.Rows[ct][5] = TotalDays.ToString();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //   msg = "Early Departure Summary from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Early Departure Summary from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Departure Summary from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }




                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:65;text-align: left;font-weight:normal;font-size:10px'>  </td>");
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EarlyDepartureSummary.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EarlyDepartureSummary.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("ED");
                            dTbl.Columns.Add("Days");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Early Departure";
                            dTbl.Rows[0][5] = "No. of Days";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][8].ToString();
                                    TotalDays = TotalDays + Convert.ToDouble(ds.Tables[0].Rows[i1][8].ToString());

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;

                                dTbl.Rows[ct][0] = "";
                                dTbl.Rows[ct][1] = "";
                                dTbl.Rows[ct][2] = "Total";
                                dTbl.Rows[ct][3] = "";
                                dTbl.Rows[ct][4] = "";
                                dTbl.Rows[ct][5] = TotalDays.ToString();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //   msg = "Early Departure Summary from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Early Departure Summary from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Departure Summary from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }




                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:65;text-align: left;font-weight:normal;font-size:10px'>  </td>");
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EarlyDepartureSummary.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radLateArrivalSummary.Checked)
            {
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company'," +
                        " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        " total=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN), " +
                        " first=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN and latearrival between 0 and 10), " +
                        " second=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN and latearrival between 11 and 30), " +
                        " third=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN and latearrival between 31 and 60), " +
                        " forth=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN and latearrival > 60),tblemployee.SSN " +
                        " from tblemployee tblemployee join tbltimeregister tbltimeregister on tblemployee.SSN=tbltimeregister.SSN join tblcompany tblcompany on " +
                        " tblemployee.companycode=tblcompany.companycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " where tbltimeregister.paycode in " +
                        " (select paycode from tbltimeregister where latearrival > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        " and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.paycode, " +
                "tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.SSN " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("TotalLate");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Total Late";
                            dTbl.Rows[0][5] = ">(0.01)";
                            dTbl.Rows[0][6] = ">(0.10)";
                            dTbl.Rows[0][7] = ">(0.30)";
                            dTbl.Rows[0][8] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Late Arrival Summery from " + FromDate.ToString("dd-MM-yyyy") + " to "+toDate.ToString("dd-MM-yyyy")+" ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Late Arrival Summery from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late Arrival Summery from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LateArrival.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LateArrival.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("TotalLate");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Total Late";
                            dTbl.Rows[0][5] = ">(0.01)";
                            dTbl.Rows[0][6] = ">(0.10)";
                            dTbl.Rows[0][7] = ">(0.30)";
                            dTbl.Rows[0][8] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Late Arrival Summery from " + FromDate.ToString("dd-MM-yyyy") + " to "+toDate.ToString("dd-MM-yyyy")+" ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Late Arrival Summery from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late Arrival Summery from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "LateSummary.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Rpt_Montly_LateSummary.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radLate_OverStay.Checked)
            {
                double Latehrs = 0;
                double daysCount = 0;
                double overstay = 0;
                string code = "";
                int lateDays = 0;
                /*strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "Late=(select Case When sum(tbltimeregister.latearrival)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60) Else Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) End  " +                        
                        "from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode), " +
                        "NoOfDays=(select convert(decimal(10,2), count(tbltimeregister.latearrival)) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode)," +
                        "OverStay=(select Cast(sum(tbltimeregister.osduration) / 60 as Varchar) +'.' +Cast(sum(tbltimeregister.osduration) % 60 as Varchar) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode) " +
                        "from tblemployee tblemployee join tbltimeregister tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.paycode in " +
                        "(select paycode from tbltimeregister where latearrival > 0 and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        " and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " ;
                        if (Selection.ToString().Trim() != "")
                        {
                           strsql += Selection.ToString();
                        }
                        strsql +=" group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                        ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                        "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";*/


                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.DateOffice,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   " +
                        " tblTimeRegister.PayCode,tblTimeRegister.LateArrival,tblTimeRegister.OsDuration,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblemployee.SSN  " +
                        " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment,tblgrade " +
                        " Where (tblTimeRegister.LateArrival>0 or tblTimeRegister.OsDuration>0) and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblgrade.GradeCode = tblEmployee.GradeCode And tblTimeRegister.SSN = tblEmployee.SSN And " +
                        " tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' and  " +
                        " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And " +
                        " (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Late");
                            dTbl.Columns.Add("LateDays");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Late";
                            dTbl.Rows[0][5] = "No. of Days";
                            dTbl.Rows[0][6] = "Over Stay";

                            int i1 = 0;
                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                // for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                while (i1 <= ds.Tables[0].Rows.Count)
                                {
                                    Latehrs = 0;
                                    daysCount = 0;
                                    overstay = 0;
                                    lateDays = 0;
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["PayCode"].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Presentcardno"].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["EmpName"].ToString();

                                    code = ds.Tables[0].Rows[i1]["PayCode"].ToString();
                                    while (code == ds.Tables[0].Rows[i1]["PayCode"].ToString())
                                    {
                                        if ((Convert.ToDouble(ds.Tables[0].Rows[i1]["LateArrival"].ToString()) > 0) || (Convert.ToDouble(ds.Tables[0].Rows[i1]["OsDuration"].ToString()) > 0))
                                        {
                                            lateDays = lateDays + 1;
                                            Latehrs = Latehrs + Convert.ToDouble(ds.Tables[0].Rows[i1]["LateArrival"].ToString());
                                            overstay = overstay + Convert.ToDouble(ds.Tables[0].Rows[i1]["OsDuration"].ToString());
                                        }
                                        i1 = i1 + 1;
                                        if (i1 == Convert.ToInt32(ds.Tables[0].Rows.Count))
                                        {
                                            break;
                                        }
                                    }
                                    dTbl.Rows[ct][4] = hm.minutetohour(Latehrs.ToString());
                                    dTbl.Rows[ct][5] = lateDays.ToString();
                                    dTbl.Rows[ct][6] = hm.minutetohour(overstay.ToString());
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    if (i1 == Convert.ToInt32(ds.Tables[0].Rows.Count))
                                    {
                                        break;
                                    }
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Late And Over Stay " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Late And Over Stay " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late And Over Stay " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LossOverstay.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LossOverstay.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Late");
                            dTbl.Columns.Add("LateDays");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Late";
                            dTbl.Rows[0][5] = "No. of Days";
                            dTbl.Rows[0][6] = "Over Stay";

                            int i1 = 0;
                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                // for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                while (i1 <= ds.Tables[0].Rows.Count)
                                {
                                    Latehrs = 0;
                                    daysCount = 0;
                                    overstay = 0;
                                    lateDays = 0;
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["PayCode"].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Presentcardno"].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["EmpName"].ToString();

                                    code = ds.Tables[0].Rows[i1]["PayCode"].ToString();
                                    while (code == ds.Tables[0].Rows[i1]["PayCode"].ToString())
                                    {
                                        if ((Convert.ToDouble(ds.Tables[0].Rows[i1]["LateArrival"].ToString()) > 0) || (Convert.ToDouble(ds.Tables[0].Rows[i1]["OsDuration"].ToString()) > 0))
                                        {
                                            lateDays = lateDays + 1;
                                            Latehrs = Latehrs + Convert.ToDouble(ds.Tables[0].Rows[i1]["LateArrival"].ToString());
                                            overstay = overstay + Convert.ToDouble(ds.Tables[0].Rows[i1]["OsDuration"].ToString());
                                        }
                                        i1 = i1 + 1;
                                        if (i1 == Convert.ToInt32(ds.Tables[0].Rows.Count))
                                        {
                                            break;
                                        }
                                    }
                                    dTbl.Rows[ct][4] = hm.minutetohour(Latehrs.ToString());
                                    dTbl.Rows[ct][5] = lateDays.ToString();
                                    dTbl.Rows[ct][6] = hm.minutetohour(overstay.ToString());
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    if (i1 == Convert.ToInt32(ds.Tables[0].Rows.Count))
                                    {
                                        break;
                                    }
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Late And Over Stay " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Late And Over Stay " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late And Over Stay " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "LateOverStay.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Rpt_MontlyLate_Overstay.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radTotalTimeLoss.Checked)
            {
                double Losshrs = 0;
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "Late=(select isnull(Case When sum(tbltimeregister.latearrival)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60) Else Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) End, 0 ) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN and tbltimeregister.latearrival > 0), " +
                            "LateMinute=(select isnull(Case When sum(tbltimeregister.latearrival)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60) Else Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN ), " +
                            "NoOfDays=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.latearrival >0 and tblemployee.SSN=tbltimeregister.SSN), " +
                            "EarlyDeptDays=(select count(tbltimeregister.earlydeparture) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.earlydeparture > 0 ), " +
                            "EarlyDept=(select isnull(Case When sum(tbltimeregister.earlydeparture)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60) Else Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.earlydeparture)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.SSN=tbltimeregister.SSN), " +
                            "ExcesslunchDays=(select count(tbltimeregister.EXCLUNCHHOURS) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.EXCLUNCHHOURS > 0 )," +
                            "Excesslunch=(select isnull(Case When sum(tbltimeregister.EXCLUNCHHOURS)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.EXCLUNCHHOURS)/60) Else Convert(nvarchar(10),sum(tbltimeregister.EXCLUNCHHOURS)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.EXCLUNCHHOURS)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.EXCLUNCHHOURS)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.EXCLUNCHHOURS)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode), " +
                            "OverStay=(select isnull(Case When sum(tbltimeregister.osduration)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.osduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.osduration)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.osduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between  " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and (earlydeparture > 0 or latearrival > 0 or EXCLUNCHHOURS > 0) ),tblemployee.SSN " +
                            "from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                            "where tbltimeregister.SSN in " +
                            "( select SSN from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' and (earlydeparture > 0 or latearrival > 0 or EXCLUNCHHOURS > 0) ) " +
                            "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname  " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.SSN " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("LateHours");
                            dTbl.Columns.Add("LateDays");
                            dTbl.Columns.Add("EarlyHours");
                            dTbl.Columns.Add("EarlyDays");
                            dTbl.Columns.Add("ExcessLunchHours");
                            dTbl.Columns.Add("ExcessLunchDays");
                            dTbl.Columns.Add("LossHrs");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Late Hrs";
                            dTbl.Rows[0][5] = "Late Days";
                            dTbl.Rows[0][6] = "Early Hrs";
                            dTbl.Rows[0][7] = "Early Days";
                            dTbl.Rows[0][8] = "Excess Lunch Hours";
                            dTbl.Rows[0][9] = "Excess Lunch Days";
                            dTbl.Rows[0][10] = "Loss Hrs";
                            dTbl.Rows[0][11] = "Over Stay";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    Losshrs = Convert.ToDouble(hm.hour(ds.Tables[0].Rows[i1][8].ToString())) + Convert.ToDouble(hm.hour(ds.Tables[0].Rows[i1][12].ToString()));

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][10] = hm.minutetohour(Losshrs.ToString());
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = " TOTAL LOSS AND OVER STAY FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " +toDate.ToString("dd-MM-yyyy")+" ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = " TOTAL LOSS AND OVER STAY FROM " + FromDateNepal.ToString("dd-MM-yyyy") + " TO " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = " TOTAL LOSS AND OVER STAY FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LossOverstay.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LossOverstay.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("LateHours");
                            dTbl.Columns.Add("LateDays");
                            dTbl.Columns.Add("EarlyHours");
                            dTbl.Columns.Add("EarlyDays");
                            dTbl.Columns.Add("ExcessLunchHours");
                            dTbl.Columns.Add("ExcessLunchDays");
                            dTbl.Columns.Add("LossHrs");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Late Hrs";
                            dTbl.Rows[0][5] = "Late Days";
                            dTbl.Rows[0][6] = "Early Hrs";
                            dTbl.Rows[0][7] = "Early Days";
                            dTbl.Rows[0][8] = "Excess Lunch Hours";
                            dTbl.Rows[0][9] = "Excess Lunch Days";
                            dTbl.Rows[0][10] = "Loss Hrs";
                            dTbl.Rows[0][11] = "Over Stay";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    Losshrs = Convert.ToDouble(hm.hour(ds.Tables[0].Rows[i1][8].ToString())) + Convert.ToDouble(hm.hour(ds.Tables[0].Rows[i1][12].ToString()));

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][10] = hm.minutetohour(Losshrs.ToString());
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = " TOTAL LOSS AND OVER STAY FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " +toDate.ToString("dd-MM-yyyy")+" ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = " TOTAL LOSS AND OVER STAY FROM " + FromDateNepal.ToString("dd-MM-yyyy") + " TO " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = " TOTAL LOSS AND OVER STAY FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "TimeLoss.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Rpt_MontlyTimeLoss.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radEmpAttendance_Percentage.Checked)
            {
                double monthDays = 0;
                monthDays = day + 1;
                strsql = "select tblemployee.presentcardno 'Card',tblemployee.empname 'Name',tbltimeregister.paycode 'PayCode',tblcompany.companyname 'Company', " +
                            " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            " convert(decimal(10,2),sum(tbltimeregister.presentvalue))'Present', " +
                            " convert(decimal(10,2),sum(tbltimeregister.presentvalue)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'PersentPer' , " +
                            " convert(decimal(10,2),sum(tbltimeregister.absentvalue)) 'Absent', " +
                            " convert(decimal(10,2),sum(tbltimeregister.absentvalue)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'AbsentPer' ," +
                            " convert(decimal(10,2),sum(tbltimeregister.leavevalue)) 'Leavevalue'," +
                            " convert(decimal(10,2),sum(tbltimeregister.leavevalue)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'leavePer' ," +
                            " convert(decimal(10,2),sum(tbltimeregister.holiday_value)) 'holiday'," +
                            " convert(decimal(10,2),sum(tbltimeregister.holiday_value)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'holidayPer' ," +
                            " convert(decimal(10,2),sum(tbltimeregister.Wo_value)) 'WO'," +
                            " convert(decimal(10,2),sum(tbltimeregister.Wo_value)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'WoPer',tblemployee.SSN " +
                            " from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode " +
                            " join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                            " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                            " where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' " +
                            " and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tbltimeregister.paycode,tblemployee.presentcardno,tblemployee.empname,tblcompany.companyname " +
                " ,tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.SSN " +
                " order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("PreP");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("PreA");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("PreWO");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("PreH");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("PreL");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "%";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "%";
                            dTbl.Rows[0][8] = "Weekly Off";
                            dTbl.Rows[0][9] = "%";
                            dTbl.Rows[0][10] = "Holiday";
                            dTbl.Rows[0][11] = "%";
                            dTbl.Rows[0][12] = "Leave";
                            dTbl.Rows[0][13] = "%";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][13].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Percentage Analysis - Employee Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Percentage Analysis - Employee Wise from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Percentage Analysis - Employee Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EmployeeWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EmployeeWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("PreP");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("PreA");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("PreWO");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("PreH");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("PreL");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "%";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "%";
                            dTbl.Rows[0][8] = "Weekly Off";
                            dTbl.Rows[0][9] = "%";
                            dTbl.Rows[0][10] = "Holiday";
                            dTbl.Rows[0][11] = "%";
                            dTbl.Rows[0][12] = "Leave";
                            dTbl.Rows[0][13] = "%";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][13].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Percentage Analysis - Employee Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Percentage Analysis - Employee Wise from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Percentage Analysis - Employee Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EmpPer.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Rpt_MonthlyEmployeeAttendance_Percentage.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radDeptWiseAttendance_Percentagte.Checked)
            {
                double TotalEmp = 0;
                string DeptCode = "";
                DataSet dsAtt = new DataSet();
                int monthDays = 0;
                strsql = " select tblemployee.departmentcode 'DeptCode',tbldepartment.departmentname 'DeptName',count(tblemployee.paycode) 'PayCode',tblemployee.companycode " +
                        " from tblemployee " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode  " +
                        " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " where tblemployee.dateofjoin < convert(datetime,'" + dateTo.ToString() + "',103) and tblemployee.active ='Y' " +
                        " and (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1   ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by tblemployee.departmentcode,tbldepartment.departmentname,tblemployee.companycode";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            monthDays = day + 1;
                            string Tmp = "", ComCode = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("DeptCode");
                            dTbl.Columns.Add("DeptName");
                            dTbl.Columns.Add("TotalEmp");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("PreP");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("PreA");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("PreL");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("PreWO");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("PreH");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Dept + " Code";
                            dTbl.Rows[0][2] = Global.getEmpInfo._Dept + " Name";
                            dTbl.Rows[0][3] = "Total " + Global.getEmpInfo._Msg;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "%";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "%";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "%";
                            dTbl.Rows[0][10] = "Weekly Off";
                            dTbl.Rows[0][11] = "%";
                            dTbl.Rows[0][12] = "Holiday";
                            dTbl.Rows[0][13] = "%";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    TotalEmp = Convert.ToDouble(ds.Tables[0].Rows[i1][2].ToString());

                                    DeptCode = ds.Tables[0].Rows[i1][0].ToString();
                                    ComCode = ds.Tables[0].Rows[i1]["CompanyCode"].ToString().Trim();
                                    strsql = "select sum(presentValue),Sum(AbsentValue),Sum(leavevalue),Sum(Wo_value),sum(holiday_value) from tbltimeregister where SSN in (select SSN from tblemployee where companycode='" + ComCode.ToString() + "' and departmentcode='" + DeptCode.ToString() + "') and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' ";
                                    dsAtt = con.FillDataSet(strsql);
                                    if (dsAtt.Tables[0].Rows.Count > 0)
                                    {
                                        dTbl.Rows[ct][4] = dsAtt.Tables[0].Rows[0][0].ToString();
                                        dTbl.Rows[ct][5] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][0].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][6] = dsAtt.Tables[0].Rows[0][1].ToString();
                                        dTbl.Rows[ct][7] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][1].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][8] = dsAtt.Tables[0].Rows[0][2].ToString();
                                        dTbl.Rows[ct][9] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][2].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][10] = dsAtt.Tables[0].Rows[0][3].ToString();
                                        dTbl.Rows[ct][11] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][3].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][12] = dsAtt.Tables[0].Rows[0][4].ToString();
                                        dTbl.Rows[ct][13] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][4].ToString()) * 100) / (TotalEmp * monthDays), 2);
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Percentage Analysis - Department Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Percentage Analysis - Department Wise from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Percentage Analysis - Department Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=DepartmentWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/DepartmentWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            monthDays = day + 1;
                            string Tmp = "", ComCode = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("DeptCode");
                            dTbl.Columns.Add("DeptName");
                            dTbl.Columns.Add("TotalEmp");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("PreP");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("PreA");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("PreL");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("PreWO");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("PreH");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Dept + " Code";
                            dTbl.Rows[0][2] = Global.getEmpInfo._Dept + " Name";
                            dTbl.Rows[0][3] = "Total " + Global.getEmpInfo._Msg;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "%";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "%";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "%";
                            dTbl.Rows[0][10] = "Weekly Off";
                            dTbl.Rows[0][11] = "%";
                            dTbl.Rows[0][12] = "Holiday";
                            dTbl.Rows[0][13] = "%";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    TotalEmp = Convert.ToDouble(ds.Tables[0].Rows[i1][2].ToString());

                                    DeptCode = ds.Tables[0].Rows[i1][0].ToString();
                                    ComCode = ds.Tables[0].Rows[i1]["CompanyCode"].ToString().Trim();
                                    strsql = "select sum(presentValue),Sum(AbsentValue),Sum(leavevalue),Sum(Wo_value),sum(holiday_value) from tbltimeregister where SSN in (select SSN from tblemployee where companycode='" + ComCode.ToString() + "' and departmentcode='" + DeptCode.ToString() + "') and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' ";
                                    dsAtt = con.FillDataSet(strsql);
                                    if (dsAtt.Tables[0].Rows.Count > 0)
                                    {
                                        dTbl.Rows[ct][4] = dsAtt.Tables[0].Rows[0][0].ToString();
                                        dTbl.Rows[ct][5] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][0].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][6] = dsAtt.Tables[0].Rows[0][1].ToString();
                                        dTbl.Rows[ct][7] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][1].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][8] = dsAtt.Tables[0].Rows[0][2].ToString();
                                        dTbl.Rows[ct][9] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][2].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][10] = dsAtt.Tables[0].Rows[0][3].ToString();
                                        dTbl.Rows[ct][11] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][3].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][12] = dsAtt.Tables[0].Rows[0][4].ToString();
                                        dTbl.Rows[ct][13] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][4].ToString()) * 100) / (TotalEmp * monthDays), 2);
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Percentage Analysis - Department Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {
                                msg = "Percentage Analysis - Department Wise from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Percentage Analysis - Department Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "DepartmentWiseAtt.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radPerformanceRegister.Checked)
            {
                string Tmp = "";
                int x, i;
                DataSet drdetails = new DataSet();
                int p = 1;
                string inout = "";
                double totalLeaves = 0.0;
                double Leave = 0.0;
                double totalHD = 0.0;
                double HD = 0.0;
                double totalCoff = 0.0;
                double Coff = 0.0;
                double Coff1 = 0.0;
                double totalLWP = 0.0;
                double LWP = 0.0;
                double TotalDays = 0.0;
                double totalWO_Hld = 0.0;
                double totalWD = 0.0;
                double halfdayleave = 0.0;
                double fulldayleave = 0.0;
                double bdayleave = 0.0;
                double totalbdayleave = 0.0;
                double Wdays = 0.0;
                double TotalWdays = 0.0;
                int dd = 0;
                int kp = 0;
                int pp = 0;
                int f = 0;
                bool isEmpty = true;
                DataSet dsday = new DataSet();
                DataSet ds = new DataSet();
                System.Data.DataTable dTbl = new System.Data.DataTable();
                DataSet dsEmp = new DataSet();
                DataRow dRow;
                string field1 = "", field2 = "", tablename = "", SSN = "";
                DateTime dt = System.DateTime.MinValue;
                DateTime doj = System.DateTime.MinValue;
                DateTime startdt = System.DateTime.MinValue;
                DataSet dsResult = new DataSet();
                if (radExcel.Checked)
                {
                    try
                    {
                        dt = FromDate;
                        startdt = FromDate;
                        dTbl.Columns.Add("PayCode");
                        while (dt <= toDate)
                        {
                            dTbl.Columns.Add(dt.Day.ToString("00"));
                            dt = dt.AddDays(1);
                        }
                        dt = FromDate;
                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            //strsql = "Select paycode, empname,departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN  from tblemployee, tblDepartment where Active='Y' And e.departmentcode = d.departmentcode  AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by departmentname, paycode ";
                            strsql = "Select tblemployee.paycode, empname,departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN  " +
                                    "from tblemployee  join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode   join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblgrade on tblemployee.gradecode=tblgrade.gradecode  join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                    " where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += "order by departmentname, tblemployee.paycode ";
                        }
                        else
                        {
                            //strsql = "Select tblemployee.paycode, tblemployee.empname,tblemployee.PRESENTCARDNO,d.departmentname,convert(varchar(10),dateofjoin,103) 'doj'  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin<='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)  And tblemployee.departmentcode = d.departmentcode   " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                            strsql = "Select tblemployee.paycode, empname,departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN  " +
                                    "from tblemployee  join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode   join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblgrade on tblemployee.gradecode=tblgrade.gradecode  join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                    " where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                            strsql += " " + Selection.ToString() + " ";
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += "order by departmentname, tblemployee.paycode ";
                        }

                        //Response.Write(strsql.ToString());
                        //dr = con.Execute_Reader(strsql);
                        dsEmp = con.FillDataSet(strsql);
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[0][0] = ("PayCode");
                        x = 1;
                        i = 0;
                        x = 1;
                        //while (dr.Read())
                        for (int co = 0; co < dsEmp.Tables[0].Rows.Count; co++)
                        {
                            FromDate = startdt;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            Tmp = dsEmp.Tables[0].Rows[co]["Paycode"].ToString().Trim();
                            SSN = dsEmp.Tables[0].Rows[co]["SSN"].ToString().Trim();
                            doj = DateTime.ParseExact(dsEmp.Tables[0].Rows[co]["doj"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            //Open this comment
                            if (doj >= FromDate)
                            {
                                FromDate = doj;
                            }
                            else
                            {
                                FromDate = FromDate;
                            }

                            strsql = "select Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.SSN='" + SSN.ToString().Trim() + "' and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0 ),  Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.SSN='" + SSN.ToString().Trim() + "' and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue>0 ),  WO_holiday=(select isnull(sum(tbltimeregister.wo_value),0) from tbltimeregister " +
                                    "where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and SSN in ('" + SSN.ToString().Trim() + "') )";
                            drdetails = con.FillDataSet(strsql);
                            for (int dd1 = 0; dd1 < drdetails.Tables[0].Rows.Count; dd1++)
                            {
                                totalWD = Convert.ToDouble(drdetails.Tables[0].Rows[dd1]["Presentvalue"]);
                                totalLWP = Convert.ToDouble(drdetails.Tables[0].Rows[dd1]["Absentvalue"]);
                                totalWO_Hld = Convert.ToDouble(drdetails.Tables[0].Rows[dd1]["WO_holiday"]);
                            }

                            //dTbl.Rows[i]["PayCode"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paycode : " + Tmp + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name : " + dsEmp.Tables[0].Rows[co]["Empname"].ToString().Trim() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CardNo : " + dsEmp.Tables[0].Rows[co]["PRESENTCARDNO"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Present : " + totalWD.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Absent : " + totalLWP.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WO : " + totalWO_Hld.ToString();
                            dTbl.Rows[i]["PayCode"] = "Paycode : " + Tmp + "&nbsp;Name : " + dsEmp.Tables[0].Rows[co]["Empname"].ToString().Trim() + "&nbsp;CardNo : " + dsEmp.Tables[0].Rows[co]["PRESENTCARDNO"].ToString() + "Present : " + totalWD.ToString() + "&nbsp;Absent : " + totalLWP.ToString() + "&nbsp;WO : " + totalWO_Hld.ToString();

                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            p = x;
                            x = 1;
                            i++;
                            {
                                strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                             " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.shiftattended 'ShiftAtt',tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                            " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                            " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',convert(varchar(5),dateadd(minute,tblTimeRegister.Latearrival,0),108) 'Late',convert(varchar(5),dateadd(minute,tblTimeRegister.otduration,0),108) 'OT',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment,tblgrade Where tblgrade.gradecode = tblEmployee.gradecode And tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.SSN = tblEmployee.SSN And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') and dateoffice>=dateofjoin";
                                strsql += " " + CompanySelection.ToString() + " ";

                                strsql1 = "";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 1][x - 1] = "In";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 2][x - 1] = "Out";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 3][x - 1] = "Late";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 4][x - 1] = "Hrs Worked";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 5][x - 1] = "Over Time";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 6][x - 1] = "Status";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 7][x - 1] = "Shift Att";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                //dr1 = con.Execute_Reader(strsql);
                                dsResult = con.FillDataSet(strsql);
                                f = x;
                                while (dt <= toDate)
                                {
                                    dTbl.Rows[i][x] = dt.Day.ToString("00");
                                    dt = dt.AddDays(1);
                                    x++;
                                }
                                x = f;
                                kp = i;

                                strsql1 = "select distinct(datediff(dd,'" + startdt.ToString("yyyy-MM-dd") + "',e.dateofjoin)) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "' and  dateofjoin<'" + toDate.ToString("yyyy-MM-dd") + "'";
                                ds = new DataSet();
                                ds = con.FillDataSet(strsql1);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                    if (dd > 0)
                                    {
                                        x = 1 + dd;
                                    }
                                }
                                //while (dr1.Read())
                                for (int cm = 0; cm < dsResult.Tables[0].Rows.Count; cm++)
                                {
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["in1"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["out2"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["Late"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["Thours"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["OT"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["status"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["ShiftAtt"].ToString().Trim();
                                    i++;
                                    x++;
                                    pp = i;
                                    i = kp;
                                }
                            }
                            i = pp + 1;
                            x = p;
                        }
                        //dr.Close();
                        //for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        //{ 
                        //    isEmpty = true;
                        //    for (int j = 0; j < dTbl.Columns.Count; j++)
                        //    {
                        //        if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                        //        {
                        //            isEmpty = false;
                        //            break;
                        //        }
                        //    }
                        //    if (isEmpty == true)
                        //    {
                        //        dTbl.Rows.RemoveAt(i2);
                        //        i2--;
                        //    }                                
                        //}

                        /*for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (dTbl.Rows[i2][j].ToString().ToUpper().Contains("PAYCODE"))
                                {
                                    DataRow dr = dTbl.NewRow();
                                    dr["Paycode"] = dTbl.Rows[i2][j].ToString();
                                    dTbl.Rows.Add(dr);
                                }
                            }
                        }*/


                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {
                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {

                                    isEmpty = false;
                                    break;
                                }
                            }
                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }




                        /*if (dTbl.Rows.Count > 0)
                        {        
                            GridView1.DataSource = dTbl;
                            GridView1.DataBind();
                            WriteXL();

                        }
                        return;*/


                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int totalcol = Convert.ToInt32(dTbl.Columns.Count);
                        // msg = "Performance Register from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {
                            msg = "Performance Register from " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "Performance Register from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        }



                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1'>");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: right;padding-right:25px;'>Run Date and Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: center'> " + msg.ToString() + " </td></tr> ");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: left'></td></tr> ");
                        string[] fs;
                        string first = "";
                        string second = "";
                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Tmp.Contains("Name"))
                                {
                                    sb.Append("<td colspan='" + totalcol + "'  style='text-align: left;font-size:13px;font-weight:bold;padding-left:15px'>" + Tmp + "</td>");
                                    break;
                                }
                                else
                                {
                                    if (i >= 4)
                                        sb.Append("<td style='text-align: left;font-weight:normal;font-size:12px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='text-align: left;font-weight:normal;font-size:12px'>" + Tmp + "</td>");
                                }
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        //Response.Write(tbl.ToString());

                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=PerformanceRegister.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/PerformanceRegister.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        //HttpContext.Current.ApplicationInstance.CompleteRequest();
                        HttpContext.Current.Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    strsql = "select tblemployee.paycode 'paycode',tblemployee.empname 'empname',tblemployee.presentcardno 'presentcardno', " +
                        "tblcompany.companyname 'Company', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "sum(tbltimeregister.presentvalue) 'Present', " +
                        "Case When sum(tbltimeregister.hoursworked)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.hoursworked)/60) Else Convert(nvarchar(10),sum(tbltimeregister.hoursworked)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.hoursworked)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.hoursworked)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.hoursworked)%60) End 'TotalHours' , " +
                        //"Cast(sum(tbltimeregister.hoursworked) / 60 as Varchar) +':' +Cast(sum(tbltimeregister.hoursworked % 60) as Varchar)'TotalHours', " +
                        "Case When sum(tbltimeregister.otduration)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.otduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.otduration)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.otduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) End 'OT' , " +
                        //"Cast(sum(tbltimeregister.otduration) / 60 as Varchar) +':' +Cast(sum(tbltimeregister.otduration % 60) as Varchar) 'OT', "+
                        "sum(tbltimeregister.absentvalue) 'absent', " +
                        "sum(tbltimeregister.holiday_value)'holiday'," +
                        "sum(tbltimeregister.wo_value)'wo',sum(tbltimeregister.otamount)'otamount' " +
                        "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join " +
                        "tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tblemployee.active='Y' and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ";
                    if (Selection.ToString().Trim() != "")
                    {
                        strsql += Selection.ToString();
                    }
                    strsql += " and (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " +
                    "group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                    ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                    "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                    ds = con.FillDataSet(strsql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyPerformanceRegister.aspx");
                        //Response.Redirect("Rpt_MonthlyPerformanceRegister.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                        return;
                    }
                }
            }
            else if (radMusterRoll.Checked)
            {
                if (radExcel.Checked)
                {
                    string Tmp = "";
                    int colCount = 0;
                    DateTime dtnew = System.DateTime.MinValue;
                    DateTime dtnewNepal = System.DateTime.MinValue;
                    dtnew = FromDate;

                    DateTime dtPrint = System.DateTime.MinValue;
                    DateTime dtPrintNepal = System.DateTime.MinValue;
                    dtPrint = FromDate;
                    dtPrintNepal = FromDateNepal;
                    try
                    {
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("Code");
                        // dTbl.Columns.Add("SBU");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Area");
                        dTbl.Columns.Add("Location");

                        //DataSet ds = new DataSet();
                        //strsql = "select datepart(dd,dateoffice) 'dy' from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode=(select top 1 paycode from tbltimeregister where dateoffice < '" + FromDate.ToString("yyyy-MM-dd") + "') ";
                        //ds = con.FillDataSet(strsql);
                        //for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        //{
                        //    dTbl.Columns.Add(ds.Tables[0].Rows[i1]["dy"].ToString());
                        //}
                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {
                            while (dtnewNepal <= toDateNepal)
                            {
                                dTbl.Columns.Add(dtnewNepal.ToString("dd"));
                                dtnewNepal = dtnewNepal.AddDays(1);
                            }
                        }
                        else
                        {
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                        }
                        //while (dtnew <= toDate)
                        //{
                        //    dTbl.Columns.Add(dtnew.ToString("dd"));
                        //    dtnew = dtnew.AddDays(1);
                        //}


                        /*dTbl.Columns.Add("HOD");                        
                        dTbl.Columns.Add("HLD");
                        dTbl.Columns.Add("CL");
                        dTbl.Columns.Add("EL");
                        dTbl.Columns.Add("ML");*/
                        dTbl.Columns.Add("Leave");
                        dTbl.Columns.Add("DW");
                        dTbl.Columns.Add("ABS");
                        dTbl.Columns.Add("POW");
                        dTbl.Columns.Add("WO");
                        dTbl.Columns.Add("Total");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["Code"] = "PayCode";
                        //dTbl.Rows[i]["SBU"] = "Catagory";
                        dTbl.Rows[i]["Name"] = "Employee Name";
                        dTbl.Rows[i]["Area"] = "Department";
                        dTbl.Rows[i]["Location"] = "Company";

                        dtnew = FromDate;
                        string colname = "";
                        while (dtnew <= toDate)
                        {
                            colname = dtnew.ToString("dd");
                            dTbl.Rows[i][colname] = dtnew.ToString("dd");
                            dtnew = dtnew.AddDays(1);
                        }


                        /*for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        {
                            dTbl.Rows[i][ds.Tables[0].Rows[i1]["dy"].ToString()] = ds.Tables[0].Rows[i1]["dy"].ToString();
                        }*/
                        /*dTbl.Rows[i]["HOD"] = "HOD";                        
                        dTbl.Rows[i]["HLD"] = "HLD";
                        dTbl.Rows[i]["CL"] = "CL";
                        dTbl.Rows[i]["EL"] = "EL";
                        dTbl.Rows[i]["ML"] = "ML";
                        dTbl.Rows[i]["PL"] = "PL";*/
                        dTbl.Rows[i]["Leave"] = "Leave";
                        dTbl.Rows[i]["DW"] = "PRE";
                        dTbl.Rows[i]["ABS"] = "ABS";
                        dTbl.Rows[i]["POW"] = "POW";
                        dTbl.Rows[i]["WO"] = "WO";
                        dTbl.Rows[i]["Total"] = "Total";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            /*strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                     " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                    " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/
                            strsql = "Select tblemployee.paycode, tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN  " +
                                        "from tblemployee  join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode   join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblgrade on tblemployee.gradecode=tblgrade.gradecode  join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += "order by departmentname, tblemployee.paycode ";
                        }
                        else
                        {
                            /*strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                     " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                    " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/

                            strsql = "select tblemployee.paycode, tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN " +
                                        "from tblemployee  join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode   join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblgrade on tblemployee.gradecode=tblgrade.gradecode  join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += " " + Selection.ToString() + " ";
                            strsql += "order by departmentname, tblemployee.paycode ";
                        }


                        //and tblemployee.paycode='1389' 
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);
                        if (dsMuster.Tables[0].Rows.Count == 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        i = 1;
                        x = 1;
                        int p = 1;
                        string inout = "", SSN = "";
                        double totalLeaves = 0.0;
                        double Leave = 0.0;
                        double totalHD = 0.0;
                        double HD = 0.0;
                        double totalCoff = 0.0;
                        double Coff = 0.0;
                        double Coff1 = 0.0;
                        double totalLWP = 0.0;
                        double LWP = 0.0;
                        double TotalDays = 0.0;
                        double totalWO_Hld = 0.0;
                        double totalWD = 0.0;
                        double halfdayleave = 0.0;
                        double fulldayleave = 0.0;
                        double bdayleave = 0.0;
                        double totalbdayleave = 0.0;
                        double Wdays = 0.0;
                        double TotalWdays = 0.0;
                        DataSet dsStatus;
                        DateTime dRoster = System.DateTime.MinValue;
                        DateTime djoin = System.DateTime.MinValue;
                        int dd = 0;

                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                dTbl.Rows[i]["Code"] = Tmp;  //dr["Paycode"].ToString().Trim();
                                //dTbl.Rows[i]["SBU"] = dsMuster.Tables[0].Rows[ms]["CatagoryName"].ToString().Trim();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                dTbl.Rows[i]["Area"] = dsMuster.Tables[0].Rows[ms]["departmentname"].ToString().Trim();
                                dTbl.Rows[i]["Location"] = dsMuster.Tables[0].Rows[ms]["CompanyCode"].ToString().Trim();
                                x = 5;
                                {
                                    /* strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                 " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                                 " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ) , " +
                                                 " CL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='CL' ),  " +
                                                 " EL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='EL' ),  " +
                                                 " ML=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='ML' ),  " +
                                                 " PL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='PL' ),  " +
                                                 " Rest=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode not in ('CL','EL','ML','PL') and leavetype='L') ,  " +
                                                 " LWP=(select isnull(Sum(tbltimeregister.AbsentValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ),  " +
                                                 " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                                 " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                                 " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";*/

                                    dRoster = System.DateTime.MinValue;
                                    djoin = System.DateTime.MinValue;
                                    FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    DateTime newFromDate = System.DateTime.MinValue;
                                    newFromDate = FromDate;

                                    strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                    ds = new DataSet();
                                    dsStatus = new DataSet();
                                    ds = con.FillDataSet(strsql1);
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        try
                                        {
                                            dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        catch
                                        {
                                            continue;
                                        }

                                        if (newFromDate <= djoin)
                                        {
                                            if (newFromDate > djoin)
                                            {
                                                FromDate = newFromDate;
                                                strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                            else if (newFromDate < djoin)
                                            {
                                                FromDate = djoin;
                                                if (djoin < newFromDate)
                                                {
                                                    FromDate = newFromDate;
                                                }
                                                strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                            else if (newFromDate == djoin)
                                            {
                                                //FromDate = dRoster;
                                                strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                        }
                                    }





                                    strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                               " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                               " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ) , " +
                                               " Leave =(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and leavetype = 'L' ),  " +
                                        /*" EL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='EL' ),  " +
                                        " ML=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='ML' ),  " +
                                        " PL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='PL' ),  " +
                                        " Rest=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode not in ('CL','EL','ML','PL') and leavetype='L') ,  " +*/
                                               " LWP=(select isnull(Sum(tbltimeregister.AbsentValue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ),  " +
                                               " POW=(select isnull(Sum(tbltimeregister.Presentvalue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Status = 'POW' ),  " +
                                               " datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Status  " +
                                               " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";

                                    // Response.Write(strsql.ToString());

                                    dd = 0;
                                    strsql1 = "";


                                    dsStatus = con.FillDataSet(strsql);
                                    if (dsStatus.Tables[0].Rows.Count > 0)
                                    {
                                        for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                        {
                                            dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["status"].ToString().Trim();
                                            x++;
                                            //if (x > colCount)
                                            //{
                                            //    continue;
                                            //}
                                        }

                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            totalWD = Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Presentvalue"].ToString().Trim());
                                            dTbl.Rows[i]["DW"] = totalWD.ToString();
                                            /*dTbl.Rows[i]["HLD"] = dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim().ToString();
                                            dTbl.Rows[i]["CL"] = dsStatus.Tables[0].Rows[0]["CL"].ToString().Trim();
                                            dTbl.Rows[i]["EL"] = dsStatus.Tables[0].Rows[0]["EL"].ToString().Trim();
                                            dTbl.Rows[i]["ML"] = dsStatus.Tables[0].Rows[0]["ML"].ToString().Trim();
                                            dTbl.Rows[i]["PL"] = dsStatus.Tables[0].Rows[0]["PL"].ToString().Trim();*/
                                            dTbl.Rows[i]["Leave"] = dsStatus.Tables[0].Rows[0]["Leave"].ToString().Trim();
                                            dTbl.Rows[i]["WO"] = dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim().ToString();
                                            dTbl.Rows[i]["ABS"] = dsStatus.Tables[0].Rows[0]["LWP"].ToString().Trim();
                                            dTbl.Rows[i]["POW"] = dsStatus.Tables[0].Rows[0]["POW"].ToString().Trim();
                                            TotalDays = Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Presentvalue"].ToString().Trim()) + Convert.ToDouble(dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim()) + Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Leave"].ToString().Trim());
                                            dTbl.Rows[i]["Total"] = TotalDays.ToString().Trim();
                                        }
                                    }
                                    //dTbl.Rows[i]["HOD"] = dsMuster.Tables[0].Rows[ms]["HOD"].ToString().Trim();
                                }
                                i++;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int widthset = 0;
                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        // msg = "Muster Roll for the month of " + dtPrint.ToString("MMMM") + " " + dtPrint.Year.ToString("0000") + " from " + dtPrint.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {
                            msg = "Muster Roll for the month of " + dtPrintNepal.ToString("MMMM") + " " + dtPrintNepal.Year.ToString("0000") + " from " + dtPrintNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "Muster Roll for the month of " + dtPrint.ToString("MMMM") + " " + dtPrint.Year.ToString("0000") + " from " + dtPrint.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        }



                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if ((i == 1) || (i == 2) || (i == 3) || ((i == 4)))
                                {
                                    if (i == 1)
                                        widthset = 75;
                                    else if (i == 2)
                                        widthset = 160;
                                    else if (i == 3)
                                        widthset = 140;
                                    else
                                        widthset = 50;

                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 8)
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:40px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=MusterRoll.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/MusterRoll.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    string Tmp = "";
                    int colCount = 0;
                    DateTime dtnew = System.DateTime.MinValue;
                    DateTime dtnewNepal = System.DateTime.MinValue;
                    dtnew = FromDate;

                    DateTime dtPrint = System.DateTime.MinValue;
                    DateTime dtPrintNepal = System.DateTime.MinValue;
                    dtPrint = FromDate;
                    dtPrintNepal = FromDateNepal;
                    try
                    {
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("Code");
                        // dTbl.Columns.Add("SBU");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Area");
                        dTbl.Columns.Add("Location");

                        //DataSet ds = new DataSet();
                        //strsql = "select datepart(dd,dateoffice) 'dy' from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode=(select top 1 paycode from tbltimeregister where dateoffice < '" + FromDate.ToString("yyyy-MM-dd") + "') ";
                        //ds = con.FillDataSet(strsql);
                        //for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        //{
                        //    dTbl.Columns.Add(ds.Tables[0].Rows[i1]["dy"].ToString());
                        //}
                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {
                            while (dtnewNepal <= toDateNepal)
                            {
                                dTbl.Columns.Add(dtnewNepal.ToString("dd"));
                                dtnewNepal = dtnewNepal.AddDays(1);
                            }
                        }
                        else
                        {
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                        }
                        //while (dtnew <= toDate)
                        //{
                        //    dTbl.Columns.Add(dtnew.ToString("dd"));
                        //    dtnew = dtnew.AddDays(1);
                        //}


                        /*dTbl.Columns.Add("HOD");                        
                        dTbl.Columns.Add("HLD");
                        dTbl.Columns.Add("CL");
                        dTbl.Columns.Add("EL");
                        dTbl.Columns.Add("ML");*/
                        dTbl.Columns.Add("Leave");
                        dTbl.Columns.Add("DW");
                        dTbl.Columns.Add("ABS");
                        dTbl.Columns.Add("POW");
                        dTbl.Columns.Add("WO");
                        dTbl.Columns.Add("Total");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["Code"] = "PayCode";
                        //dTbl.Rows[i]["SBU"] = "Catagory";
                        dTbl.Rows[i]["Name"] = "Employee Name";
                        dTbl.Rows[i]["Area"] = "Department";
                        dTbl.Rows[i]["Location"] = "Company";

                        dtnew = FromDate;
                        string colname = "";
                        while (dtnew <= toDate)
                        {
                            colname = dtnew.ToString("dd");
                            dTbl.Rows[i][colname] = dtnew.ToString("dd");
                            dtnew = dtnew.AddDays(1);
                        }


                        /*for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        {
                            dTbl.Rows[i][ds.Tables[0].Rows[i1]["dy"].ToString()] = ds.Tables[0].Rows[i1]["dy"].ToString();
                        }*/
                        /*dTbl.Rows[i]["HOD"] = "HOD";                        
                        dTbl.Rows[i]["HLD"] = "HLD";
                        dTbl.Rows[i]["CL"] = "CL";
                        dTbl.Rows[i]["EL"] = "EL";
                        dTbl.Rows[i]["ML"] = "ML";
                        dTbl.Rows[i]["PL"] = "PL";*/
                        dTbl.Rows[i]["Leave"] = "Leave";
                        dTbl.Rows[i]["DW"] = "PRE";
                        dTbl.Rows[i]["ABS"] = "ABS";
                        dTbl.Rows[i]["POW"] = "POW";
                        dTbl.Rows[i]["WO"] = "WO";
                        dTbl.Rows[i]["Total"] = "Total";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            /*strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                     " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                    " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/
                            strsql = "Select tblemployee.paycode, tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN  " +
                                        "from tblemployee  join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode   join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblgrade on tblemployee.gradecode=tblgrade.gradecode  join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += "order by departmentname, tblemployee.paycode ";
                        }
                        else
                        {
                            /*strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                     " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                    " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";*/

                            strsql = "select tblemployee.paycode, tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj',tblemployee.SSN " +
                                        "from tblemployee  join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode   join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblgrade on tblemployee.gradecode=tblgrade.gradecode  join tblcompany on tblemployee.companycode=tblcompany.companycode join tblcatagory on tblemployee.CAT=tblcatagory.CAT  join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                                        " where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                            strsql += " " + CompanySelection.ToString() + " ";
                            strsql += " " + Selection.ToString() + " ";
                            strsql += "order by departmentname, tblemployee.paycode ";
                        }


                        //and tblemployee.paycode='1389' 
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);
                        if (dsMuster.Tables[0].Rows.Count == 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        i = 1;
                        x = 1;
                        int p = 1;
                        string inout = "", SSN = "";
                        double totalLeaves = 0.0;
                        double Leave = 0.0;
                        double totalHD = 0.0;
                        double HD = 0.0;
                        double totalCoff = 0.0;
                        double Coff = 0.0;
                        double Coff1 = 0.0;
                        double totalLWP = 0.0;
                        double LWP = 0.0;
                        double TotalDays = 0.0;
                        double totalWO_Hld = 0.0;
                        double totalWD = 0.0;
                        double halfdayleave = 0.0;
                        double fulldayleave = 0.0;
                        double bdayleave = 0.0;
                        double totalbdayleave = 0.0;
                        double Wdays = 0.0;
                        double TotalWdays = 0.0;
                        DataSet dsStatus;
                        DateTime dRoster = System.DateTime.MinValue;
                        DateTime djoin = System.DateTime.MinValue;
                        int dd = 0;

                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                SSN = dsMuster.Tables[0].Rows[ms]["SSN"].ToString().Trim();
                                dTbl.Rows[i]["Code"] = Tmp;  //dr["Paycode"].ToString().Trim();
                                //dTbl.Rows[i]["SBU"] = dsMuster.Tables[0].Rows[ms]["CatagoryName"].ToString().Trim();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                dTbl.Rows[i]["Area"] = dsMuster.Tables[0].Rows[ms]["departmentname"].ToString().Trim();
                                dTbl.Rows[i]["Location"] = dsMuster.Tables[0].Rows[ms]["CompanyCode"].ToString().Trim();
                                x = 5;
                                {
                                    /* strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                 " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                                 " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ) , " +
                                                 " CL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='CL' ),  " +
                                                 " EL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='EL' ),  " +
                                                 " ML=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='ML' ),  " +
                                                 " PL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='PL' ),  " +
                                                 " Rest=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode not in ('CL','EL','ML','PL') and leavetype='L') ,  " +
                                                 " LWP=(select isnull(Sum(tbltimeregister.AbsentValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ),  " +
                                                 " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                                 " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                                 " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";*/

                                    dRoster = System.DateTime.MinValue;
                                    djoin = System.DateTime.MinValue;
                                    FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    DateTime newFromDate = System.DateTime.MinValue;
                                    newFromDate = FromDate;

                                    strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString().Trim() + "' group by dateofjoin";
                                    ds = new DataSet();
                                    dsStatus = new DataSet();
                                    ds = con.FillDataSet(strsql1);
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        try
                                        {
                                            dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        catch
                                        {
                                            continue;
                                        }

                                        if (newFromDate <= djoin)
                                        {
                                            if (newFromDate > djoin)
                                            {
                                                FromDate = newFromDate;
                                                strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                            else if (newFromDate < djoin)
                                            {
                                                FromDate = djoin;
                                                if (djoin < newFromDate)
                                                {
                                                    FromDate = newFromDate;
                                                }
                                                strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                            else if (newFromDate == djoin)
                                            {
                                                //FromDate = dRoster;
                                                strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.SSN=e.SSN where e.SSN='" + SSN.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                        }
                                    }





                                    strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                               " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                               " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ) , " +
                                               " Leave =(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and leavetype = 'L' ),  " +
                                        /*" EL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='EL' ),  " +
                                        " ML=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='ML' ),  " +
                                        " PL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='PL' ),  " +
                                        " Rest=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode not in ('CL','EL','ML','PL') and leavetype='L') ,  " +*/
                                               " LWP=(select isnull(Sum(tbltimeregister.AbsentValue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ),  " +
                                               " POW=(select isnull(Sum(tbltimeregister.Presentvalue),0) from tbltimeregister where tbltimeregister.SSN=tblemployee.SSN and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Status = 'POW' ),  " +
                                               " datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Status  " +
                                               " from tblTimeRegister join tblemployee on tbltimeregister.SSN=tblemployee.SSN where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.SSN in ('" + SSN.ToString() + "') ";

                                    // Response.Write(strsql.ToString());

                                    dd = 0;
                                    strsql1 = "";


                                    dsStatus = con.FillDataSet(strsql);
                                    if (dsStatus.Tables[0].Rows.Count > 0)
                                    {
                                        for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                        {
                                            dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["status"].ToString().Trim();
                                            x++;
                                            //if (x > colCount)
                                            //{
                                            //    continue;
                                            //}
                                        }

                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            totalWD = Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Presentvalue"].ToString().Trim());
                                            dTbl.Rows[i]["DW"] = totalWD.ToString();
                                            /*dTbl.Rows[i]["HLD"] = dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim().ToString();
                                            dTbl.Rows[i]["CL"] = dsStatus.Tables[0].Rows[0]["CL"].ToString().Trim();
                                            dTbl.Rows[i]["EL"] = dsStatus.Tables[0].Rows[0]["EL"].ToString().Trim();
                                            dTbl.Rows[i]["ML"] = dsStatus.Tables[0].Rows[0]["ML"].ToString().Trim();
                                            dTbl.Rows[i]["PL"] = dsStatus.Tables[0].Rows[0]["PL"].ToString().Trim();*/
                                            dTbl.Rows[i]["Leave"] = dsStatus.Tables[0].Rows[0]["Leave"].ToString().Trim();
                                            dTbl.Rows[i]["WO"] = dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim().ToString();
                                            dTbl.Rows[i]["ABS"] = dsStatus.Tables[0].Rows[0]["LWP"].ToString().Trim();
                                            dTbl.Rows[i]["POW"] = dsStatus.Tables[0].Rows[0]["POW"].ToString().Trim();
                                            TotalDays = Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Presentvalue"].ToString().Trim()) + Convert.ToDouble(dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim()) + Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Leave"].ToString().Trim());
                                            dTbl.Rows[i]["Total"] = TotalDays.ToString().Trim();
                                        }
                                    }
                                    //dTbl.Rows[i]["HOD"] = dsMuster.Tables[0].Rows[ms]["HOD"].ToString().Trim();
                                }
                                i++;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int widthset = 0;
                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        // msg = "Muster Roll for the month of " + dtPrint.ToString("MMMM") + " " + dtPrint.Year.ToString("0000") + " from " + dtPrint.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {
                            msg = "Muster Roll for the month of " + dtPrintNepal.ToString("MMMM") + " " + dtPrintNepal.Year.ToString("0000") + " from " + dtPrintNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "Muster Roll for the month of " + dtPrint.ToString("MMMM") + " " + dtPrint.Year.ToString("0000") + " from " + dtPrint.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        }



                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if ((i == 1) || (i == 2) || (i == 3) || ((i == 4)))
                                {
                                    if (i == 1)
                                        widthset = 75;
                                    else if (i == 2)
                                        widthset = 160;
                                    else if (i == 3)
                                        widthset = 140;
                                    else
                                        widthset = 50;

                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 8)
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:40px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "MusterRoll.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
            }
            else if (radLogReport.Checked)
            {
                try
                {
                    string field1 = "", field2 = "", tablename = "", SSN = "";
                    string ShiftHrs ="";
                    string Tmp = "";
                    int x, i;
                    double present = 0, absent = 0, leave = 0, wo = 0, hld = 0, totalhr = 0;
                    System.Data.DataTable dTbl = new System.Data.DataTable();
                    DataRow dRow;
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);
                    dTbl.Columns.Add("Personnel No");
                    dTbl.Columns.Add("First Name");
                    dTbl.Columns.Add("Department");
                    dTbl.Columns.Add("Date");
                    dTbl.Columns.Add("Weekday");
                    dTbl.Columns.Add("Check-In");
                    dTbl.Columns.Add("Check-Out");
                    dTbl.Columns.Add("Device Name");
                    dTbl.Columns.Add("Check-In Time");
                    dTbl.Columns.Add("Check-Out Time");
                    dTbl.Columns.Add("Total Time");
                    dTbl.Columns.Add("Absent");
                    dTbl.Columns.Add("Total Time Worked");
                   

                    int ct = 0;
                    strsql = "";
                    strsql = "select tblemployee.paycode,empname,tblemployee.SSN,tbldepartment.departmentname from tblemployee join tblcompany  on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on tblemployee.ssn=tblemployeeshiftmaster.ssn  " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE   join tblcatagory on tblemployee.CAT=tblcatagory.CAT   join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode  join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        "where  tblemployee.Active='Y' " +
                        "And (tblemployee.LeavingDate >= '" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and dateofjoin <= '" + toDate.ToString("yyyy-MM-dd") + "' ";
                    if (Selection.ToString().Trim() != "")
                    {
                        strsql = strsql + Selection.ToString();
                    }
                    strsql += " " + CompanySelection.ToString() + " ";
                    strsql += "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);
                    DataSet dsResult = new DataSet();
                    DataSet dsResult1 = new DataSet();
                    dsResult = con.FillDataSet(strsql);
                    if (dsResult.Tables[0].Rows.Count > 0)
                    {
                        for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                        {
                            present = 0; absent = 0; leave = 0; wo = 0; hld = 0; totalhr = 0;
                            SSN = dsResult.Tables[0].Rows[i1][2].ToString().Trim();
                            dTbl.Rows[1][0] = ("Personnel No");
                            dTbl.Rows[1][1] = ("First Name");
                            dTbl.Rows[1][2] = ("Department");
                            dTbl.Rows[1][3] = ("Date");
                            dTbl.Rows[1][4] = ("Weekday");
                            dTbl.Rows[1][5] = ("Check-In");
                            dTbl.Rows[1][6] = ("Check-Out");
                            dTbl.Rows[1][7] = ("Device Name");
                            dTbl.Rows[1][8] = ("Check-In Time");
                            dTbl.Rows[1][9] = ("Check-Out Time");
                            dTbl.Rows[1][10] = ("Total Time");
                            dTbl.Rows[1][11] = ("Absent");
                            dTbl.Rows[1][12] = ("Total Time Worked");

                            ct = ct + 1;
                            strsql1 = "select convert(char(10),dateoffice,103) 'date',Substring(datename(dw,dateoffice),0,4) 'DayName',Convert(varchar(5),ShiftStartTime,108) 'StartTime',Convert(varchar(5),SHIFTENDTIME,108) 'EndTime',isnull(DATEDIFF(n,ShiftStartTime,SHIFTENDTIME),0)'SD' ,convert(char(5),in1,108) 'in1',convert(char(5),out2,108) 'out2',shiftattended 'shift',status, " +
                                    "case when latearrival=0 then null else Cast(latearrival / 60 as Varchar) +':' +Cast(latearrival % 60 as Varchar) end as 'latearrival', " +
                                    "case when earlydeparture=0 then null else Cast(earlydeparture/ 60 as Varchar) +':' +Cast(earlydeparture % 60 as Varchar) end as 'earlyarrival'," +
                                    "case when hoursworked=0 then null else substring(convert(varchar(20),dateadd(minute,hoursworked,0),108),0,6) end as 'hoursworked', " +
                                    "case when otduration=0 then null else Cast(otduration / 60 as Varchar) +':' +Cast(otduration  % 60 as Varchar) end as 'otduration' " +
                                    ",hoursworked,presentvalue,absentvalue,leavevalue,holiday_value,wo_value " +
                                    "from tbltimeregister where SSN='" + SSN.ToString() + "' and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and " + "'" + toDate.ToString("yyyy-MM-dd") + "'  ";
                            if (ShiftSelected.Trim() != string.Empty || ShiftSelected.Trim() != "")
                            {
                                strsql1 = strsql1 + ShiftSelected.ToString().Trim();
                            }


                            dsResult1 = con.FillDataSet(strsql1);
                            if (dsResult1.Tables[0].Rows.Count > 0)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                                for (int j1 = 0; j1 < dsResult1.Tables[0].Rows.Count; j1++)
                                {

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    try
                                    {
                                        if (dsResult1.Tables[0].Rows[j1]["SD"].ToString().Trim() != "0" || dsResult1.Tables[0].Rows[j1]["SD"].ToString().Trim() != null)
                                      {

                                          ShiftHrs = hm.minutetohourNew(dsResult1.Tables[0].Rows[j1]["SD"].ToString().Trim());
                                      }

                                    }
                                    catch
                                    {
                                        ShiftHrs = "00:00";
                                    }




                                    dTbl.Rows[ct][0] = dsResult.Tables[0].Rows[i1]["paycode"].ToString().Trim();
                                    dTbl.Rows[ct][1] = dsResult.Tables[0].Rows[i1]["empname"].ToString().Trim();
                                    dTbl.Rows[ct][2] = dsResult.Tables[0].Rows[i1]["departmentname"].ToString().Trim();
                                    dTbl.Rows[ct][3] = dsResult1.Tables[0].Rows[j1]["date"].ToString();
                                    dTbl.Rows[ct][4] = dsResult1.Tables[0].Rows[j1]["DayName"].ToString().Trim();
                                    dTbl.Rows[ct][5] = dsResult1.Tables[0].Rows[j1]["StartTime"].ToString().Trim();
                                    dTbl.Rows[ct][6] = dsResult1.Tables[0].Rows[j1]["EndTime"].ToString().Trim();
                                    dTbl.Rows[ct][7] = "";
                                    dTbl.Rows[ct][8] = dsResult1.Tables[0].Rows[j1]["in1"].ToString().Trim();
                                    dTbl.Rows[ct][9] = dsResult1.Tables[0].Rows[j1]["out2"].ToString().Trim();
                                    dTbl.Rows[ct][10] = ShiftHrs.Trim();
                                    dTbl.Rows[ct][11] = dsResult1.Tables[0].Rows[j1]["absentvalue"].ToString().Trim();
                                    dTbl.Rows[ct][12] = dsResult1.Tables[0].Rows[j1]["hoursworked"].ToString().Trim();
                                    ct = ct + 1;
                                }
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                              
                            }
                            ct = ct + 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                        }
                      //  dTbl.Columns.Remove("Status");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                        return;
                    }

                    bool isEmpty;
                    for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                    {

                        isEmpty = true;
                        for (int j = 0; j < dTbl.Columns.Count; j++)
                        {
                            if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                            {
                                isEmpty = false;
                                break;
                            }
                        }

                        if (isEmpty == true)
                        {
                            dTbl.Rows.RemoveAt(i2);
                            i2--;
                        }
                    }

                    string companyname = "";
                    strsql = "select companyname from tblcompany";
                    if (Session["Com_Selection"] != null)
                    {
                        strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                    }
                    else if (Session["Auth_Comp"] != null)
                    {
                        strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                    }
                    DataSet dsCom = new DataSet();
                    dsCom = con.FillDataSet(strsql);
                    if (dsCom.Tables[0].Rows.Count > 0)
                    {
                        for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                        {
                            companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                        }
                    }
                    if (!string.IsNullOrEmpty(companyname.ToString()))
                    {
                        companyname = companyname.Substring(1);
                    }
                    if (Session["IsNepali"].ToString().Trim() == "Y")
                    {
                        msg = "Daily Total Time Report From " + FromDateNepal.ToString("dd-MM-yyyy") + " to " + toDateNepal.ToString("dd-MM-yyyy") + " ";
                    }
                    else
                    {
                        msg = msg = "Daily Total Time Report From " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    }

                    // msg = "Employee Performance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                    sb.Append("<tr><td colspan='13' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: left'></td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='13' style='text-align: left'></td></tr> ");

                    x = dTbl.Columns.Count;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                                if (Tmp.Substring(0, 1) == "0")
                                {
                                    if (Tmp.Contains(":"))
                                    {
                                        Tmp = Tmp;
                                    }
                                    else
                                    {
                                        Tmp = Tmp;
                                        //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                    }
                                }
                            }
                            if (Tmp.Contains(field1.ToString()) && !string.IsNullOrEmpty(field1.ToString().Trim()))
                            {
                                sb.Append("<td colspan='13'  style='background-color:#DCF7FA;text-align: left;font-weight:bold;font-size:13px;padding-left:15px'>" + Tmp + "</td>");
                                break;
                            }
                            else if (Tmp.Contains("("))
                            {
                                sb.Append("<td colspan='13'  style='text-align: center;font-weight:bold;font-size:13px'>" + Tmp + "</td>");
                                break;
                            }
                            else if (Tmp.Contains("Present"))
                            {
                                sb.Append("<td colspan='13'  style='text-align: left;font-weight:bold;font-size:14px;padding-left:15px'>" + Tmp + "</td>");
                                break;
                            }
                            else
                            {
                                if (i >= 4)
                                    sb.Append("<td style='width:105px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:105px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    if (radExcel.Checked)
                    {
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=DailyTotal.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/DailyTotal.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    else
                    {
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "DailyTotal.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }


                }
                catch (Exception ex)
                {
                    //Response.Write(ex.Message.ToString());
                }
            }
            else if (radManualPunch.Checked)
            {
                
                    string Tmp = "";
                    int colCount = 0;
                    DateTime dtnew = System.DateTime.MinValue;
                    DateTime dtnewNepal = System.DateTime.MinValue;
                    dtnew = FromDate;

                    DateTime dtPrint = System.DateTime.MinValue;
                    DateTime dtPrintNepal = System.DateTime.MinValue;
                    dtPrint = FromDate;
                    dtPrintNepal = FromDateNepal;
                    try
                    {

                       


                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Sr. No.");
                        dTbl.Columns.Add("Department");
                        
                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {
                            while (dtnewNepal <= toDateNepal)
                            {
                                dTbl.Columns.Add(dtnewNepal.ToString("dd"));
                                dtnewNepal = dtnewNepal.AddDays(1);
                            }
                        }
                        else
                        {
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                        }
                        dTbl.Columns.Add("Total");
                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["Sr. No."] = "SNo";
                        dTbl.Rows[i]["Department"] = "Department Name";
                       
                        dtnew = FromDate;
                        string colname = "";
                        while (dtnew <= toDate)
                        {
                            colname = dtnew.ToString("dd");
                            dTbl.Rows[i][colname] = dtnew.ToString("dd");
                            dtnew = dtnew.AddDays(1);
                        }
                        dTbl.Rows[i]["Total"] = "Total";
                        colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select * from tbldepartment Where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                          //  strsql += " " + CompanySelection.ToString() + " ";
                            strsql += "order by departmentname";
                        }
                        else
                        {
                            string Dep = "";
                            strsql = "Select * from tbldepartment Where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                            if(LookUpDept.Text!="")
                            {
                                string[] SplitDep = LookUpDept.Text.ToString().Trim().Split(',');
                               
                                foreach (string sc in SplitDep)
                                Dep += "'" + sc.Trim() + "',";
                                Dep = Dep.TrimEnd(',');
                                
                            }
                            if(Dep.Trim()!="")
                            {
                                strsql += "and tbldepartment.DepartmentCode in (" + Dep.ToString().Trim() + ")  ";
                            }

                            strsql += "order by departmentname";
                        }

                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);
                        if (dsMuster.Tables[0].Rows.Count == 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        i = 1;
                        x = 1;
                        int p = 1;
                        string inout = "", SSN = "";
                        double totalLeaves = 0.0;
                        double TotalPresent = 0.0;
                        double totalHD = 0.0;
                        double HD = 0.0;
                        double totalCoff = 0.0;
                        double Coff = 0.0;
                        double Coff1 = 0.0;
                        double totalLWP = 0.0;
                        double LWP = 0.0;
                        double TotalDays = 0.0;
                        double totalWO_Hld = 0.0;
                        double totalWD = 0.0;
                        double halfdayleave = 0.0;
                        double fulldayleave = 0.0;
                        double bdayleave = 0.0;
                        double totalbdayleave = 0.0;
                        double Wdays = 0.0;
                        double TotalWdays = 0.0;
                        DataSet dsStatus;
                        DateTime dRoster = System.DateTime.MinValue;
                        DateTime djoin = System.DateTime.MinValue;
                        int dd = 0;
                        int ct = 0;

                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                                dTbl.Rows[i]["Sr. No."] = Convert.ToString(i);
                                dTbl.Rows[i]["Department"] = dsMuster.Tables[0].Rows[ms]["departmentname"].ToString().Trim();
                                Tmp = dsMuster.Tables[0].Rows[ms]["departmentname"].ToString().Trim();
                                SSN = dsMuster.Tables[0].Rows[ms]["departmentcode"].ToString().Trim();
                                TotalPresent = 0;
                                x = 2;
                                {
                                    FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    while (FromDate<=toDate)
                                    {
                                        strsql =" Select TblDepartment.DepartmentCode,TblDepartment.DepartmentName, sum(tblTimeregister.PresentValue)'Present' " +
                                                " from tblemployeeshiftmaster, tblcatagory, tblDivision,tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY,tblgrade " +
                                                " Where tblGrade.GradeCode = tblEmployee.GradeCode  And tblEmployee.Companycode = tblCompany.Companycode AND tblemployee.paycode=tblemployeeshiftmaster.paycode and " +
                                                " tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.SSN = TblEmployee.SSN And  " +
                                                " TblEmployee.DepartmentCode = TblDepartment.DepartmentCode AND  tbltimeregister.DateOffice ='" + FromDate.ToString("yyyy-MM-dd") + "'  And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or  " +
                                                " tblemployee.LeavingDate is null) AND  1=1  and  1=1  and  TblDepartment.DepartmentCode='" + SSN.Trim() + "' and tblDepartment.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'  group by TblDepartment.DepartmentCode,TblDepartment.DepartmentName," +
                                                " tblcompany.companyname order by tbldepartment.departmentcode";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {

                                            dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[0]["Present"].ToString().Trim();
                                            TotalPresent = TotalPresent + ((string.IsNullOrEmpty(dsStatus.Tables[0].Rows[0]["Present"].ToString())) ? 0 : Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Present"].ToString()));
                                            x++;

                                        }
                                        else
                                        {
                                            dTbl.Rows[i][x] = "0";
                                            x++;
                                        }

                                        FromDate = FromDate.AddDays(1);
                                    }
                                    dTbl.Rows[i]["Total"] = TotalPresent.ToString().Trim();
                                    i++;
                                }
                            }
                            FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            x = 2;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            ct = ct + 1;
                            dTbl.Rows[ct][1] = "Total";
                            while (FromDate <= toDate)
                            {
                                TotalDays = 0;
                                
                                if (LookUpDept.Text.ToString().Trim()=="")
                                {
                                    strsql = " Select sum(tblTimeregister.PresentValue)'Present'  from tblemployeeshiftmaster, tblcatagory, " +
                                        " tblDivision,tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY,tblgrade  Where tblGrade.GradeCode = tblEmployee.GradeCode  And " +
                                        " tblEmployee.Companycode = tblCompany.Companycode AND tblemployee.paycode=tblemployeeshiftmaster.paycode and  tblEmployee.CAT = tblCatagory.cat And " +
                                        " tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.SSN = TblEmployee.SSN And   TblEmployee.DepartmentCode = TblDepartment.DepartmentCode AND " +
                                        " tbltimeregister.DateOffice=  '" + FromDate.ToString("yyyy-MM-dd") + "'  And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or   tblemployee.LeavingDate is null) AND  1=1  and  1=1  and " +
                                        " tblDepartment.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'  group by     tbltimeregister.DateOFFICE ";
                                }
                                else
                                {
                                    string Dep = "";
                                    string[] SplitDep = LookUpDept.Text.ToString().Trim().Split(',');

                                    foreach (string sc in SplitDep)
                                        Dep += "'" + sc.Trim() + "',";
                                    Dep = Dep.TrimEnd(',');

                                    if (Dep.Trim()!="")
                                    {
                                        strsql = " Select sum(tblTimeregister.PresentValue)'Present'  from tblemployeeshiftmaster, tblcatagory, " +
                                      " tblDivision,tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY,tblgrade  Where tblGrade.GradeCode = tblEmployee.GradeCode  And " +
                                      " tblEmployee.Companycode = tblCompany.Companycode AND tblemployee.paycode=tblemployeeshiftmaster.paycode and  tblEmployee.CAT = tblCatagory.cat And " +
                                      " tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.SSN = TblEmployee.SSN And   TblEmployee.DepartmentCode = TblDepartment.DepartmentCode AND " +
                                      " tbltimeregister.DateOffice=  '" + FromDate.ToString("yyyy-MM-dd") + "'  And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or   tblemployee.LeavingDate is null) AND  1=1  and  1=1  and " +
                                      " tblDepartment.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' and tblDepartment.departmentcode in ("+Dep.Trim()+")  group by     tbltimeregister.DateOFFICE ";
                                    }

                                }
                               
                                DataSet DsT = new DataSet();
                                DsT = con.FillDataSet(strsql);
                                if (DsT.Tables[0].Rows.Count>0)
                                {
                                    TotalDays = Convert.ToDouble(DsT.Tables[0].Rows[0]["Present"].ToString());
                                }
                                dTbl.Rows[ct][x] = TotalDays.ToString();
                                x++;
                                FromDate = FromDate.AddDays(1);
                            }

                            //dRow = dTbl.NewRow();
                            //dTbl.Rows.Add(dRow);
                            ////ct = ct + 1;
                            //dTbl.Rows[ct][0] = "Present : " + present + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Absent : " + absent + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;leave : " + leave + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Holiday : " + hld + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Week Offs : " + wo + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Working : " + hm.minutetohour(totalhr.ToString());
                              
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int widthset = 0;
                        int colcount = Convert.ToInt32(dTbl.Columns.Count);

                        msg = "Staff Count  " + dtPrint.ToString("MMMM") + " " + dtPrint.Year.ToString("0000") + " From " + dtPrint.ToString("dd-MM-yyyy") + " To " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if ((i == 1) || (i == 2) || (i == 3) || ((i == 4)))
                                {
                                    if (i == 1)
                                        widthset = 200;
                                    else if (i == 2)
                                        widthset = 50;
                                    else if (i == 3)
                                        widthset = 50;
                                    else
                                        widthset = 50;

                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 8)
                                    sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=StaffCount.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/StaffCount.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "StaffCount.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();

                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message.ToString());
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
    private byte[] exportpdf(DataTable dtEmployee)
    {

        // creating document object  
        System.IO.MemoryStream ms = new System.IO.MemoryStream();
        iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(PageSize.A4);
        rec.BackgroundColor = new BaseColor(System.Drawing.Color.Olive);
        Document doc = new Document(rec);
        doc.SetPageSize(iTextSharp.text.PageSize.A4);
        PdfWriter writer = PdfWriter.GetInstance(doc, ms);
        doc.Open();

        //Creating paragraph for header  
        BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        iTextSharp.text.Font fntHead = new iTextSharp.text.Font(bfntHead, 16, 1, iTextSharp.text.BaseColor.BLUE);
        Paragraph prgHeading = new Paragraph();
        prgHeading.Alignment = Element.ALIGN_LEFT;
        prgHeading.Add(new Chunk("Dynamic Report PDF".ToUpper(), fntHead));
        doc.Add(prgHeading);

        //Adding paragraph for report generated by  
        Paragraph prgGeneratedBY = new Paragraph();
        BaseFont btnAuthor = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        iTextSharp.text.Font fntAuthor = new iTextSharp.text.Font(btnAuthor, 8, 2, iTextSharp.text.BaseColor.BLUE);
        prgGeneratedBY.Alignment = Element.ALIGN_RIGHT;
        //prgGeneratedBY.Add(new Chunk("Report Generated by : ASPArticles", fntAuthor));  
        //prgGeneratedBY.Add(new Chunk("\nGenerated Date : " + DateTime.Now.ToShortDateString(), fntAuthor));  
        doc.Add(prgGeneratedBY);

        //Adding a line  
        Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
        doc.Add(p);

        //Adding line break  
        doc.Add(new Chunk("\n", fntHead));

        //Adding  PdfPTable  
        PdfPTable table = new PdfPTable(dtEmployee.Columns.Count);

        for (int i = 0; i < dtEmployee.Columns.Count; i++)
        {
            string cellText = Server.HtmlDecode(dtEmployee.Columns[i].ColumnName);
            PdfPCell cell = new PdfPCell();
            cell.Phrase = new Phrase(cellText, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, 1, new BaseColor(System.Drawing.ColorTranslator.FromHtml("#000000"))));
            cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C8C8C8"));
            //cell.Phrase = new Phrase(cellText, new Font(Font.FontFamily.TIMES_ROMAN, 10, 1, new BaseColor(grdStudent.HeaderStyle.ForeColor)));  
            //cell.BackgroundColor = new BaseColor(grdStudent.HeaderStyle.BackColor);  
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.PaddingBottom = 5;
            table.AddCell(cell);
        }

        //writing table Data  
        for (int i = 0; i < dtEmployee.Rows.Count; i++)
        {
            for (int j = 0; j < dtEmployee.Columns.Count; j++)
            {
                table.AddCell(dtEmployee.Rows[i][j].ToString());
            }
        }

        doc.Add(table);
        doc.Close();

        byte[] result = ms.ToArray();
        return result;

    }  
    private void ExportGridToword( DataTable DT)
    {

        string FileName = "MonthlyPerformance" + System.DateTime.Now.ToString() + ".pdf";
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());

        Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
        GridView1.AllowPaging = true;
        GridView1.DataBind();
    }
    private void OpenChilePage(string FormToOpen)
    {
        OpenPopupPage = "";
        //OpenPopupPage = "<script language='javascript'>window.open('" + FormToOpen.Trim() + "', 'PopupWindow','location=no,status=no,menubar=no,toolbar=no,width=1000,height=1000');window.moveTo(0,0);</script>";
        OpenPopupPage = "<script language='javascript'>time=window.open('" + FormToOpen.Trim() + "', 'PopupWindow','location=no,status=no,menubar=no,toolbar=no,width=1020,height=800,scrollbars=yes');time.moveTo(0,0);</script>";
        Page.RegisterStartupScript("OpenChild", OpenPopupPage);
    }
    protected void UpdateTimeRegister_Half()
    {
        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        string str = "";
        string Upaycode = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        string StrSql = "";
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        double TimeLVAmount1 = 0;
        double TimeLVAmount2 = 0;
        double presntvalue = 0;
        double absentvalue = 0;
        double LeaveValue = 0;
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103),l.leavecode, " +
                       " l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.presentvalue,t.absentvalue,T.LEAVEVALUE from tbltimeregister t join leave_request l 	on t.paycode=l.paycode and t.dateoffice between l.leave_from and l.leave_to " +
                        " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+ltrim(rtrim(l.leavecode))+'%' and l.stage1_approved='Y' and l.stage2_approved='Y'  " +
                        " and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  " +
                        " and l.halfday!='N' order by t.paycode";

            ds = con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Upaycode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[i][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    TimeLVAmount1 = Convert.ToDouble(ds.Tables[0].Rows[i][8].ToString());
                    TimeLVAmount2 = Convert.ToDouble(ds.Tables[0].Rows[i][9].ToString());
                    presntvalue = Convert.ToDouble(ds.Tables[0].Rows[i][10].ToString());
                    absentvalue = Convert.ToDouble(ds.Tables[0].Rows[i][11].ToString());
                    LeaveValue = Convert.ToDouble(ds.Tables[0].Rows[i][12].ToString());

                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = con.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    str = "";

                    if (hday.ToString().Trim() == "F")
                    {
                        lvamount = 0.5;
                        lvamount1 = 0.5;
                        lvamount2 = 0;
                    }
                    else
                    {
                        lvamount = 0.5;
                        lvamount1 = 0;
                        lvamount2 = 0.5;
                    }
                    if (lvtype.ToString().Trim().ToUpper() == "L")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "P")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "A")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(str))
                    {
                        con.execute_NonQuery(str);
                        if (Connection.State == 0)
                            Connection.Open();

                        OleDbCommand upd = new OleDbCommand("upd", Connection);
                        upd.CommandType = CommandType.StoredProcedure;
                        OleDbParameter myParm1 = upd.Parameters.Add("@paycode", OleDbType.Char, 10);
                        myParm1.Value = Upaycode.ToString();
                        OleDbParameter myParm2 = upd.Parameters.Add("@dateoffice", OleDbType.Date, 10);
                        myParm2.Value = dtFrom.ToString("yyyy-MM-dd");
                        upd.ExecuteNonQuery();
                        Connection.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    protected void UpdateTimeRegister()
    {
        //FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

        try
        {
            FromDate = Convert.ToDateTime(txtDateFrom.Value);
            toDate = Convert.ToDateTime(txtToDate.Value);
            Module_UPD UPD = new Module_UPD();
            string str = "";
            DateTime dtFrom = System.DateTime.MinValue;
            DateTime dtTo = System.DateTime.MinValue;
            DataSet dslv = new DataSet();
            string lvcode = "";
            string lvtype = "";
            double lvamount = 0;
            DateTime lvapr = DateTime.MinValue;
            string reason = "";
            string vno = "";
            string hday = "";
            double lvamount1 = 0;
            double lvamount2 = 0;
            string Pcode = "";
            string SSN = "";
            string lvcodeTm = "";
            DateTime doffice = System.DateTime.MinValue;
            DataSet ds = new DataSet();
            DataSet drlvType = new DataSet();
            DataSet dsCount = new DataSet();
            string LvType1 = "";
            string LvType2 = "";
            string FirstHalfLvCode = "";
            string SecondHalfLvCode = "";
            string ShiftAttended = "";
            string Holiday = "";
            string OldStatus = "";
            DataSet dsD = new DataSet();
            int result = 0;
            string StrSql = "";
            string isOffInclude = "";
            string isHldInclude = "";
            string Status = "";
            try
            {
                StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103), " +
                        " l.leavecode,  l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.status,l.leavecode,t.presentvalue,t.absentvalue,T.LEAVEVALUE,t.in1,t.shiftattended,lm.isoffinclude,lm.ISHOLIDAYINCLUDE,t.Status,e.ssn  " +
                        " from tbltimeregister t join leave_request l 	on t.paycode=l.paycode join tblleavemaster lm on lm.leavecode=l.leavecode and t.dateoffice between l.leave_from and l.leave_to  " +
                        " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+rtrim(l.leavecode)+'%' and l.stage1_approved='Y' and l.stage2_approved='Y' " +
                        " and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' order by t.paycode";


                ds = con.FillDataSet(StrSql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                        Pcode = ds.Tables[0].Rows[i][1].ToString().Trim();
                        SSN = ds.Tables[0].Rows[i]["SSN"].ToString().Trim();
                        doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                        lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                        lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][4].ToString());
                        hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                        ShiftAttended = ds.Tables[0].Rows[i]["ShiftAttended"].ToString().Trim();
                        OldStatus = ds.Tables[0].Rows[i]["in1"].ToString().Trim();
                        reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                        lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        isOffInclude = ds.Tables[0].Rows[i]["isoffinclude"].ToString().Trim();
                        isHldInclude = ds.Tables[0].Rows[i]["ISHOLIDAYINCLUDE"].ToString().Trim();
                        Status = ds.Tables[0].Rows[i]["Status"].ToString().Trim();
                        dtFrom = doffice;
                        str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' and companycode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                        drlvType = con.FillDataSet(str);
                        if (drlvType.Tables[0].Rows.Count > 0)
                        {
                            lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                        }
                        if (isOffInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && ShiftAttended.ToString().Trim().ToUpper() == "OFF")
                        {
                            continue;
                        }
                        if (isHldInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && Status.ToString().Trim().ToUpper() == "HLD")
                        {
                            continue;
                        }
                        try
                        {
                            if (hday.ToString().Trim() == "N")
                            {
                                lvamount = 1;
                                lvcodeTm = lvcode;
                            }
                            else if (hday.ToString().Trim() == "F")
                            {
                                lvamount = 0.5;
                                lvamount1 = 0.5;
                                lvcodeTm = lvcode;
                            }
                            else
                            {
                                lvamount = 0.5;
                                lvamount2 = 0.5;
                                lvcodeTm = lvcode;
                            }
                            StrSql = "";
                            StrSql = "select * from holiday where hdate='" + dtFrom.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + Pcode.ToString().Trim() + "') " +
                                    "and departmentcode=(select departmentcode from tblemployee where paycode='" + Pcode.ToString().Trim() + "')";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                Holiday = "Y";
                            }
                            StrSql = "";
                            /*StrSql = "Select * From tblTimeRegister  Where Paycode = '" + Pcode.ToString().Trim() + "'" + " And DateOffice='" + dtFrom.ToString("yyyy-MM-dd") + "'";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                ShiftAttended = dsD.Tables[0].Rows[0]["SHIFTATTENDED"].ToString().Trim();
                            }*/
                            StrSql = "";
                            if (hday.ToString().Trim() == "N")
                            {
                                FirstHalfLvCode = null;
                                LvType1 = null;
                                SecondHalfLvCode = null;
                                LvType2 = null;
                                lvamount = 1;
                            }
                            else if (hday.ToString().Trim() == "F")
                            {
                                StrSql = "select isnull(SECONDHALFLEAVECODE,''),LeaveType2 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                dsD = con.FillDataSet(StrSql);
                                if (dsD.Tables[0].Rows.Count > 0)
                                {
                                    if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                    {
                                        FirstHalfLvCode = lvcode.ToString().Trim();
                                        LvType1 = lvtype.ToString().Trim();
                                        SecondHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                        LvType2 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                        lvamount = 1;
                                    }
                                }
                            }
                            else if (hday.ToString().Trim() == "S")
                            {
                                StrSql = "select isnull(FIRSTHALFLEAVECODE,''),LeaveType1 from tbltimeregister where ssn='" + SSN.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                dsD = con.FillDataSet(StrSql);
                                if (dsD.Tables[0].Rows.Count > 0)
                                {
                                    if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                    {
                                        FirstHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                        LvType1 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                        SecondHalfLvCode = lvcode.ToString().Trim();
                                        LvType2 = lvtype.ToString().Trim();
                                        lvamount = 1;
                                    }
                                }
                            }
                            if (hday.ToString().Trim() == "N")
                            {
                                StrSql = "Update tblTimeRegister Set LeaveCode='" + lvcodeTm.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where SSN='" + SSN.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            }
                            else if (hday.ToString().Trim() == "F")
                            {
                                StrSql = "Update tblTimeRegister Set FIRSTHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType1='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount1=" + lvamount1 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where SSN='" + SSN.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            }
                            else if (hday.ToString().Trim() == "S")
                            {
                                StrSql = "Update tblTimeRegister Set SECONDHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType2='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount2=" + lvamount2 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where SSN='" + SSN.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            }
                            result = con.execute_NonQuery(StrSql);
                            if (result > 0)
                            {
                                UPD.Upd(Pcode, dtFrom, lvamount, lvamount1, lvamount2, lvcode, FirstHalfLvCode, SecondHalfLvCode, lvtype, LvType1, LvType2, ShiftAttended, Holiday, OldStatus);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch
            {

            }
        }
        catch 
        {
            
            
        }
    }
    protected void txtDateFrom_DateChanged(object sender, EventArgs e)
    {
        try
        {
            FromDate = Convert.ToDateTime(txtDateFrom.Value);  //DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Invalid Date Format');", true);
            txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
            txtDateFrom.Focus();
            return;
        }
        toDate = FromDate;
        txtToDate.Text = toDate.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
    }
    protected void txtDateFrom_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {
        //if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Red;
        //}
        //else
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Black;
        //}
    }
    protected void txtToDate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {
        //if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Red;
        //}
        //else
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Black;
        //}
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        // specified ASP.NET server control at run time.
    }
    public void ExportToPdf(DataTable dt)
    {
        Document document = new Document();
        PdfWriter writer = PdfWriter.GetInstance(document, new FileStream("D://sample.pdf", FileMode.Create));
        document.Open();
        iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, 5);

        PdfPTable table = new PdfPTable(dt.Columns.Count);
        PdfPRow row = null;
        float[] widths = new float[] { 4f, 4f, 4f, 4f };

        table.SetWidths(widths);

        table.WidthPercentage = 100;
        int iCol = 0;
        string colname = "";
        PdfPCell cell = new PdfPCell(new Phrase("Products"));

        cell.Colspan = dt.Columns.Count;

        foreach (DataColumn c in dt.Columns)
        {

            table.AddCell(new Phrase(c.ColumnName, font5));
        }

        foreach (DataRow r in dt.Rows)
        {
            if (dt.Rows.Count > 0)
            {
                table.AddCell(new Phrase(r[0].ToString(), font5));
                table.AddCell(new Phrase(r[1].ToString(), font5));
                table.AddCell(new Phrase(r[2].ToString(), font5));
                table.AddCell(new Phrase(r[3].ToString(), font5));
            }
        } document.Add(table);
        document.Close();
    }
}