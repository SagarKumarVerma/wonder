﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class frmAddLeave : System.Web.UI.Page
{
    string Strsql = null;
    DataSet ds = null;
    Class_Connection Con = new Class_Connection();
    int result = 0;
    string WeeklyOff = null;
    string holiday = null;
    string isAccrual = null;
    string ShowOnWeb = null;
    string Fixed = null;
    string IsCompOff = null;
    string LeaveType = null;

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        txtLeaveCode.Focus();
        //txtDescription.Attributes.Add("onkeypress", "return isAlphabet('" + txtDescription.ClientID + "');");                
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LeaveMasterVisible"] == null)
            {
                Response.Redirect("PageNotFound.aspx");
            }
           // lblMsg.Text = Session["FormName"].ToString();
            if (Request.QueryString["Value"] != null)
            {
                txtLeaveCode.Focus();
                string leavecode = Request.QueryString["Value"].ToString();

                txtLeaveField.Text = leavecode.ToString();
                ViewState["LeaveField"] = leavecode.ToString().Trim();
                txtLeaveField.Enabled = false;
                BindLeave(leavecode);
                Session["IsNewRecord"] = "N";
            }
            else
            {
                txtMin.Text = "00.00";
                txtLeave.Text = "00.00";
                txtMax.Text = "00.00";
                txtMaxAccuralLimit.Text = "000.00";
                txtPresent.Text = "00.00";
                bindLeaveField();
                Session["IsNewRecord"] = "Y";

            }
            }
    }
    protected void BindLeave(string leavecode)
    {
        Strsql = "select * from tblleavemaster where leavefield='" + leavecode.ToString().Trim() + "' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' ";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtLeaveCode.Text = ds.Tables[0].Rows[0]["LeaveCode"].ToString().Trim();

            txtDescription.Text = ds.Tables[0].Rows[0]["LEAVEDESCRIPTION"].ToString().Trim();
            WeeklyOff = ds.Tables[0].Rows[0]["ISOFFINCLUDE"].ToString().Trim();
            holiday = ds.Tables[0].Rows[0]["ISHOLIDAYINCLUDE"].ToString().Trim();
            isAccrual = ds.Tables[0].Rows[0]["ISLEAVEACCRUAL"].ToString().Trim();
            hdIsAccrual.Value = ds.Tables[0].Rows[0]["ISLEAVEACCRUAL"].ToString().Trim();
            Fixed = ds.Tables[0].Rows[0]["Fixed"].ToString().Trim();
            hdFixed.Value = ds.Tables[0].Rows[0]["Fixed"].ToString().Trim();
            txtMin.Text = ds.Tables[0].Rows[0]["SMIN"].ToString().Trim();
            txtMax.Text = ds.Tables[0].Rows[0]["SMAX"].ToString().Trim();
            txtPresent.Text = ds.Tables[0].Rows[0]["PRESENT"].ToString().Trim();
            txtLeave.Text = ds.Tables[0].Rows[0]["LEAVE"].ToString().Trim();
            txtMaxAccuralLimit.Text = ds.Tables[0].Rows[0]["LEAVELIMIT"].ToString().Trim();
            IsCompOff = ds.Tables[0].Rows[0]["isCompOffType"].ToString().Trim();
            ChkShowOnWeb.Checked = (ds.Tables[0].Rows[0]["ShowOnWeb"].ToString().Trim().ToUpper()) == "Y" ? true : false;
            
            if (WeeklyOff == "Y")
            {
                chkWeeklyOff.Checked = true;
            }
            if (holiday == "Y")
            {
                chkHolidays.Checked = true;
            }
            if (isAccrual == "Y")
            {
                chkIsAccrual.Checked = true;
            }
            if (Fixed == "Y")
            {
                radFixed.Checked = true;
                radCarried.Checked = false;
            }
            else
            {
                radFixed.Checked = false;
                radCarried.Checked = true;
            }
            LeaveType = ds.Tables[0].Rows[0]["LEAVETYPE"].ToString().ToUpper();
           if(LeaveType.Trim()=="L")
           {
               ddlLeaveType.SelectedIndex = 0;
           }
           else if(LeaveType.Trim()=="P")
            {
                ddlLeaveType.SelectedIndex = 1;
            }
           else if (LeaveType.Trim() == "A")
           {
               ddlLeaveType.SelectedIndex = 2;
           }
              else
           {
               ddlLeaveType.SelectedIndex = 0;
           }
        }
        else
        {
            txtLeaveField.Text = "L01";
        }
    }
    protected void bindLeaveField()
    {
        Strsql = "select isnull(max(convert(varchar(100),right(leavefield,2))+1),1) as 'MaxLeave' from tblleavemaster where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        int LvField;
        LvField = Convert.ToInt32(ds.Tables[0].Rows[0]["MaxLeave"].ToString().Trim());
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtLeaveField.Text = "L" + LvField.ToString("00");
        }
        else
        {
            txtLeaveField.Text = "L01";
        }
    }
    protected void chkIsAccrual_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIsAccrual.Checked)
        {
            LA.Visible = true;
        }
        else
        {
            LA.Visible = false;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool res;
        res = Con.chkvalid(txtDescription.Text.ToString().Trim());
        if (!res)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('You have enter some invalid inputs.')", true);
            return;
        }
        //Weekly Off 
        if (chkWeeklyOff.Checked)
        {
            WeeklyOff = "Y";
        }
        else
        {
            WeeklyOff = "N";
        }
        //Holidays
        if (chkHolidays.Checked)
        {
            holiday = "Y";
        }
        else
        {
            holiday = "N";
        }
        //Accrual
        if (chkIsAccrual.Checked)
        {
            isAccrual = "Y";
        }
        else
        {
            isAccrual = "N";
        }
        if (radCarried.Checked)
        {
            Fixed = "N";
        }
        else
        {
            {
                Fixed = "Y";
                txtPresent.Text = "0.0";
                txtLeave.Text = "0.0";
                txtMaxAccuralLimit.Text = "0.0";
            }
        }
      




        ShowOnWeb = ((ChkShowOnWeb.Checked) == true) ? "Y" : "N";
        if (Session["IsNewRecord"].ToString() == "Y")
        {
            try
            {
                Strsql = "select count(leavecode) from tblleavemaster where leavecode='" + txtLeaveCode.Text.ToString().Trim() + "' ";
                result = Con.execute_Scalar(Strsql);
                if (result > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Leave Code Already Exists');", true);
                    return;
                }
                else
                {
                    Strsql = "insert into tblLeaveMaster(LEAVEFIELD,LEAVECODE,LEAVEDESCRIPTION,ISOFFINCLUDE,ISHOLIDAYINCLUDE,ISLEAVEACCRUAL, " +
                        "LEAVETYPE,SMIN,SMAX,PRESENT,LEAVE,LEAVELIMIT,FIXED,ShowOnWeb,Companycode)values " +
                        "('" + txtLeaveField.Text.ToString().Trim().ToUpper() + "','" + txtLeaveCode.Text.ToString().Trim().ToUpper() + "', " +
                        " '" + txtDescription.Text.ToString().Trim().ToUpper() + "','" + WeeklyOff.ToString().Trim() + "','" + holiday.ToString().Trim() + "', " +
                        " '" + isAccrual.ToString().Trim().ToUpper() + "','" + ddlLeaveType.SelectedItem.Value.ToString().Trim().ToUpper() + "'," + txtMin.Text.ToString().Trim() + ", " +
                        " " + txtMax.Text.ToString().Trim() + "," + txtPresent.Text.ToString().Trim() + "," + txtLeave.Text.ToString().Trim() + ", " +
                        " '" + txtMaxAccuralLimit.Text.ToString().Trim() + "','" + Fixed.ToString().Trim() + "', '" + ShowOnWeb + "','"+Session["LoginCompany"].ToString().Trim()+"' )";

                    result = Con.execute_NonQuery(Strsql);
                    if (result > 0)
                    {
                        Response.Redirect("frmLeaveMaster.aspx");
                       
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        else
        {
            //update region
            try
            {
                Strsql = "select count(leavecode) from tblleavemaster where LeaveField!= '" + txtLeaveField.Text.ToString() + "' And  leavecode='" + txtLeaveCode.Text.ToString().Trim() + "' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
                result = Con.execute_Scalar(Strsql);
                if (result > 0)
                {
                    txtLeaveCode.Focus();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Leave Code Already Exists');", true);
                    return;
                }
                if (ViewState["LeaveField"].ToString() == txtLeaveField.Text.ToString().Trim())
                {
                    Strsql = "update tblLeaveMaster set LEAVECODE='" + txtLeaveCode.Text.ToString().Trim().ToUpper() + "',LEAVEDESCRIPTION='" + txtDescription.Text.ToString().Trim().ToUpper() + "', " +
                    "ISOFFINCLUDE='" + WeeklyOff.ToString().Trim().ToUpper() + "',ISHOLIDAYINCLUDE='" + holiday.ToString().Trim().ToUpper() + "',ISLEAVEACCRUAL='" + isAccrual.ToString().Trim().ToUpper() + "', " +
                    "LEAVETYPE='" + ddlLeaveType.SelectedItem.Value.ToString().Trim().ToUpper() + "',SMIN=" + txtMin.Text.ToString().Trim() + ",SMAX=" + txtMax.Text.ToString().Trim() + ", " +
                    "PRESENT=" + txtPresent.Text.ToString().Trim() + ",LEAVE=" + txtLeave.Text.ToString().Trim() + ",LEAVELIMIT=" + txtMaxAccuralLimit.Text.ToString().Trim() + ", " +
                    "FIXED='" + Fixed.ToString().Trim().ToUpper() + "', ShowOnWeb = '" + ShowOnWeb.ToString().Trim().ToUpper() + "' where leavefield='" + txtLeaveField.Text.ToString().Trim().ToUpper() + "' and Companycode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                }
                result = Con.execute_NonQuery(Strsql);
                if (result > 0)
                {
                    Response.Redirect("frmLeaveMaster.aspx");
                   
                }
            }
            catch (Exception ex)
            {
                txtLeaveCode.Focus();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Error');", true);
                return;
            }
        }
        Session["IsNewRecord"] = "";
        Session["IsNewRecord"] = null;
    }
}