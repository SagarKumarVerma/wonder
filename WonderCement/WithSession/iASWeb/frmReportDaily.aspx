﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmReportDaily.aspx.cs" Inherits="frmReportDaily" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div align="center" style="vertical-align:top" > 
               <dx:ASPxPanel ID="ASPxPanel2" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
 
<table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td ></td>
</tr>
<tr>
<td colspan="3" class="tableHeaderCss" align="center" style="height:25px; width: 100%;">
 <asp:Label ID="lblMsg" runat="server" Text="Daily Attendance Report" CssClass="lblCss" ></asp:Label>  </td>
</tr>
    <tr>
<td >

    <table cellspacing="1" style="border-style: solid; border-width: thin; padding: 5px; width: 100%;">
        <tr style="padding: 5px; border-style: solid; border-width: thin; padding: 5px; width: 100%; height: 19px; text-align: left;">
            <td style="padding: 5px">
                <dx:ASPxLabel ID="lblfrom" runat="server" Text="From">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxDateEdit ID="txtDateFrom" runat="server" Width="120px" EditFormatString="dd/MM/yyyy" OnCalendarDayCellPrepared="txtDateFrom_CalendarDayCellPrepared">
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="lblto" runat="server" Text="To Date" Visible="false" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxDateEdit ID="txtToDate" runat="server" Width="120px" EditFormatString="dd/MM/yyyy" Visible="false" OnCalendarDayCellPrepared="txtToDate_CalendarDayCellPrepared" >
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 5px">
                <dx:ASPxButton ID="btnGenerate" runat="server" Text="Generate" OnClick="btnGenerate_Click">
                </dx:ASPxButton>
            </td>
            <td style="padding: 5px">
                <dx:ASPxRadioButton ID="radExcel" runat="server" GroupName="A" Checked="true" Text="Excel">
                </dx:ASPxRadioButton>
            </td>
            <td style="padding: 5px">
                <dx:ASPxRadioButton ID="radPDF" runat="server" GroupName="A" Text="PDF">
                </dx:ASPxRadioButton>
            </td>
            
        </tr>
        <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Select Company">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                   <dx:ASPxGridLookup ID="LookupCompany" runat="server" SelectionMode="Multiple"  ClientInstanceName="gridLookup"
                                                                            KeyFieldName="CompanyCode"  TextFormatString="{0}" MultiTextSeparator=",">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px" SelectAllCheckboxMode="AllPages"/>
                                                                                <dx:GridViewDataColumn FieldName="CompanyCode" Caption="Code" Width="50px" />
                                                                                <dx:GridViewDataColumn FieldName="Companyname"  Caption="Name">
                                                                                </dx:GridViewDataColumn>
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                                                                                <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                                            </GridViewProperties>
                                                                            <ClearButton DisplayMode="Always">
                                                                            </ClearButton>
                                                                        </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="selDep" runat="server" Text="Select Department" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookUpDept" runat="server" ClientInstanceName="GDep" KeyFieldName="DepartmentCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="DepartmentCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="departmentname"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Select Section">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                 <dx:ASPxGridLookup ID="lookupSec" runat="server" ClientInstanceName="lookupSec" KeyFieldName="DivisionCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="DivisionCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="DivisionName"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                     <ClearButton DisplayMode="Always">
                     </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Select Contractor" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                  <dx:ASPxGridLookup ID="lookupGrade" runat="server" ClientInstanceName="lookupGrade" KeyFieldName="GradeCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="GradeCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="GradeName" >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                      <ClearButton DisplayMode="Always">
                      </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Select Category">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="lookupCat" runat="server" ClientInstanceName="lookupCat" KeyFieldName="CAT" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="CAT" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="CATAGORYNAME"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Select Shift">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookUpShift" runat="server" ClientInstanceName="LookUpShift" KeyFieldName="Shift" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="Shift" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="ShiftTime" >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
        </tr>
          <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Select Employee">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookEmp" runat="server" ClientInstanceName="LookEmp" KeyFieldName="PAYCODE" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="PAYCODE" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="EMPNAME"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Sort By">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxComboBox ID="ddlSorting" runat="server" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="PayCode" Value="tblemployee.paycode" />
                                      <dx:ListEditItem  Selected="True" Value="tblemployee.paycode" Text="Paycode" /> 
                 <dx:ListEditItem  Value="tblemployee.presentcardno" Text="Card Number" />
                 <dx:ListEditItem  Value="tblemployee.EmpName" Text="Employee Name" />
                 <dx:ListEditItem  Value="tbldepartment.Departmentcode,tblemployee.paycode" Text="Department + Paycode" />
                 <dx:ListEditItem  Value="tbldepartment.Departmentcode,tblemployee.presentcardno" Text="Departmentcode + Card Number" />
                 <dx:ListEditItem  Value="tbldepartment.Departmentcode,tblemployee.empname" Text="Department + Name" />
                 <dx:ListEditItem  Value="tbldivision.divisioncode,tblemployee.paycode" Text="Section+Paycode" />
                 <dx:ListEditItem  Value="tbldivision.divisioncode,tblemployee.presentcardno" Text="Section + Card Number" />
                 <dx:ListEditItem  Value="tbldivision.divisioncode,tblemployee.empname" Text="Section + Name" />
                 <dx:ListEditItem  Value="tblcatagory.cat,tblemployee.paycode" Text="Catagory + Paycode" />
                 <dx:ListEditItem  Value="tblcatagory.Cat,tblemployee.presentcardno" Text="Catagory + Card Number" />
                 <dx:ListEditItem  Value="tblcatagory.Cat,tblemployee.empname" Text="Catagory + Name" />
                                </Items>
                            </dx:ASPxComboBox>
            </td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 5px" colspan="8">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
   <table style="padding: 5px; width: 100%;">
                    <tr>
                         <td style="padding: 5px; width:15% ;">
                             <dx:ASPxRadioButton ID="radLateArrival" AutoPostBack="true" Text="Late Arrival" Checked="True" GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:15% ;">
                             <dx:ASPxRadioButton ID="radDailyPerformance" AutoPostBack="true"  Text="Daily Performance"  GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:15% ;">
                             <dx:ASPxRadioButton ID="radcontlatearrival" AutoPostBack="true" Text="Continious Late Arrival"   GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                        <td style="padding: 5px; width:32% ;">
                           
                         </td>
                         <td style="padding: 5px; width:33% ;"></td>
                    </tr>
                             <tr>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radAbsenteesim" AutoPostBack="true"  Text="Absenteesim"   GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radEarlyDeparture" AutoPostBack="true" Text="Early Departure"   GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radContEarlyDept1" AutoPostBack="true"  Text="Continious Early Departure"  GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                    </tr>
                             <tr>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radAttendance" AutoPostBack="true"  Text="Attendance"  GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radTimeLoss" AutoPostBack="true" Text="Time Loss"  GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px">
                             <dx:ASPxRadioButton ID="radContAbsent1" AutoPostBack="true" Text="Continious Absenteesim"  GroupName="a" OnCheckedChanged="fire" runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radDepartment" AutoPostBack="true" Text="Department Summary"  GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radOT" AutoPostBack="true" Text="OT Report" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radOverTimeSummary" AutoPostBack="true" Text="OT Summary" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
                      <tr>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radEarlyArrival" AutoPostBack="true" Text="Early Arrival"  GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radshiftwise" AutoPostBack="true" Text="Shift Wise" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radMisPunch" AutoPostBack="true" Text="MIS Punch Report" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
                      <tr>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radPresent" AutoPostBack="true" Text="Present"  GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radShiftChange" AutoPostBack="true" Text="Shift Change" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radoutwork" AutoPostBack="true" Text="Out Work" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
                      <tr>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radManualPunch" AutoPostBack="true" Text="Manual Punch"  GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radhris" AutoPostBack="true" Text="Temprature Report" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radmachinepunch" AutoPostBack="true" Text="Machine Raw Punch" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
        <tr>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radContAbsenteesim" AutoPostBack="true" Text="Cont Abs"  GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radEditLog" runat="server" AutoPostBack="True" GroupName="a" OnCheckedChanged="fire" Text="Edit Log">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radContEarlyDept" runat="server" AutoPostBack="True" GroupName="a" OnCheckedChanged="fire" Text="Cont Early" Visible="False">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
        <tr>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radcontlatearrival1" AutoPostBack="true" Text="Cont Late"  GroupName="a" OnCheckedChanged="fire" runat="server" Visible="False">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxRadioButton ID="radAbnormal" AutoPostBack="true" Text="Abnormal Punch" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>
                            &nbsp;</td>
                        <td style="padding: 5px">
                           <%-- <dx:ASPxRadioButton ID="ASPxRadioButton3" AutoPostBack="true" Text="Text Data" GroupName="a" OnCheckedChanged="fire" runat="server">
                            </dx:ASPxRadioButton>--%>
                            <dx:ASPxRadioButton ID="radTextPunch" runat="server" AutoPostBack="True" GroupName="a" OnCheckedChanged="fire" Text="Text File" Visible="False">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
         <tr id="Tday" runat="server" visible="false">
                        <td style="padding: 5px">
                            <dx:ASPxLabel ID="lblDays" runat="server" Text="Enter Days"></dx:ASPxLabel>
                        </td>
                        <td style="padding: 5px">
                            <dx:ASPxTextBox ID="txtDays" runat="server" Width="50px"></dx:ASPxTextBox>
                        </td>
                        <td style="padding: 5px">
                           
                            <dx:ASPxRadioButton ID="radMRP" runat="server" AutoPostBack="True" GroupName="a" OnCheckedChanged="fire" Text="Machine Raw Punch">
                            </dx:ASPxRadioButton>
                           
                        </td>
                    </tr>
                  
                </table>
                        </dx:PanelContent>
</PanelCollection>
                   
                    <Border BorderStyle="Solid" BorderWidth="1px" />
                   
 </dx:ASPxPanel>
                
                   
                </td>
        </tr>
    </table>

</td>
</tr>
    </table>
    </dx:PanelContent>
    </PanelCollection>
                   </dx:ASPxPanel>
        </div>
</asp:Content>

