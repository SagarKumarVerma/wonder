﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.Text;
using GlobalSettings;
public partial class frmMonthlyReport : System.Web.UI.Page
{
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    hourToMinute hm = new hourToMinute();
    Class_Connection con = new Class_Connection();
    string datefrom = null;
    string dateTo = null;
    string strsql = null;
    string strsql1 = null;
    DataSet ds = null;
    DataSet ds1 = null;
    string OpenPopupPage = "";
    string Selection = "";
    string status = null;
    string leave = null;
    DateTime toDate;
    DateTime FromDate;
    DataSet dsEmpCount = new DataSet();
    string EmpCodes = "";
    DataTable dTbl;
    string btnGoBack = "";
    string resMsg = "";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
        if (ex is HttpRequestValidationException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            txtDateFrom.Focus();
            txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
        }
        //if (Session["MonthlyReportVisible"] == null)
        //{
        //    Response.Redirect("PageNotFound.aspx");
        //}
    }

    protected void UpdateTimeRegister_Half()
    {
        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        string str = "";
        string Upaycode = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        string StrSql = "";
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        double TimeLVAmount1 = 0;
        double TimeLVAmount2 = 0;
        double presntvalue = 0;
        double absentvalue = 0;
        double LeaveValue = 0;
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103),l.leavecode, " +
                       " l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.presentvalue,t.absentvalue,T.LEAVEVALUE from tbltimeregister t join leave_request l 	on t.paycode=l.paycode and t.dateoffice between l.leave_from and l.leave_to " +
                        " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+ltrim(rtrim(l.leavecode))+'%' and l.stage1_approved='Y' and l.stage2_approved='Y'  " +
                        " and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  " +
                        " and l.halfday!='N' order by t.paycode";

            ds = con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Upaycode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[i][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    TimeLVAmount1 = Convert.ToDouble(ds.Tables[0].Rows[i][8].ToString());
                    TimeLVAmount2 = Convert.ToDouble(ds.Tables[0].Rows[i][9].ToString());
                    presntvalue = Convert.ToDouble(ds.Tables[0].Rows[i][10].ToString());
                    absentvalue = Convert.ToDouble(ds.Tables[0].Rows[i][11].ToString());
                    LeaveValue = Convert.ToDouble(ds.Tables[0].Rows[i][12].ToString());

                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = con.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    str = "";

                    if (hday.ToString().Trim() == "F")
                    {
                        lvamount = 0.5;
                        lvamount1 = 0.5;
                        lvamount2 = 0;
                    }
                    else
                    {
                        lvamount = 0.5;
                        lvamount1 = 0;
                        lvamount2 = 0.5;
                    }
                    if (lvtype.ToString().Trim().ToUpper() == "L")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "P")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "A")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(str))
                    {
                        con.execute_NonQuery(str);
                        if (Connection.State == 0)
                            Connection.Open();

                        OleDbCommand upd = new OleDbCommand("upd", Connection);
                        upd.CommandType = CommandType.StoredProcedure;
                        OleDbParameter myParm1 = upd.Parameters.Add("@paycode", OleDbType.Char, 10);
                        myParm1.Value = Upaycode.ToString();
                        OleDbParameter myParm2 = upd.Parameters.Add("@dateoffice", OleDbType.Date, 10);
                        myParm2.Value = dtFrom.ToString("yyyy-MM-dd");
                        upd.ExecuteNonQuery();
                        Connection.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    protected void UpdateTimeRegister()
    {
        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        Module_UPD UPD = new Module_UPD();
        string str = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string Pcode = "";
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        string LvType1 = "";
        string LvType2 = "";
        string FirstHalfLvCode = "";
        string SecondHalfLvCode = "";
        string ShiftAttended = "";
        string Holiday = "";
        string OldStatus = "";
        DataSet dsD = new DataSet();
        int result = 0;
        string StrSql = "";
        string isOffInclude = "";
        string isHldInclude = "";
        string Status = "";
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103), " +
                    " l.leavecode,  l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.status,l.leavecode,t.presentvalue,t.absentvalue,T.LEAVEVALUE,t.in1,t.shiftattended,lm.isoffinclude,lm.ISHOLIDAYINCLUDE,t.Status  " +
                    " from tbltimeregister t join leave_request l 	on t.paycode=l.paycode join tblleavemaster lm on lm.leavecode=l.leavecode and t.dateoffice between l.leave_from and l.leave_to  " +
                    " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+rtrim(l.leavecode)+'%' and l.stage1_approved='Y' and l.stage2_approved='Y' " +
                    " and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' order by t.paycode";

            ds = con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Pcode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    ShiftAttended = ds.Tables[0].Rows[i]["ShiftAttended"].ToString().Trim();
                    OldStatus = ds.Tables[0].Rows[i]["in1"].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    isOffInclude = ds.Tables[0].Rows[i]["isoffinclude"].ToString().Trim();
                    isHldInclude = ds.Tables[0].Rows[i]["ISHOLIDAYINCLUDE"].ToString().Trim();
                    Status = ds.Tables[0].Rows[i]["Status"].ToString().Trim();
                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = con.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    if (isOffInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && ShiftAttended.ToString().Trim().ToUpper() == "OFF")
                    {
                        continue;
                    }
                    if (isHldInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && Status.ToString().Trim().ToUpper() == "HLD")
                    {
                        continue;
                    }
                    try
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            lvamount = 1;
                            lvcodeTm = lvcode;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            lvamount = 0.5;
                            lvamount1 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        else
                        {
                            lvamount = 0.5;
                            lvamount2 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        StrSql = "";
                        StrSql = "select * from holiday where hdate='" + dtFrom.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + Pcode.ToString().Trim() + "') " +
                                "and departmentcode=(select departmentcode from tblemployee where paycode='" + Pcode.ToString().Trim() + "')";
                        dsD = con.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            Holiday = "Y";
                        }
                        StrSql = "";
                        /*StrSql = "Select * From tblTimeRegister  Where Paycode = '" + Pcode.ToString().Trim() + "'" + " And DateOffice='" + dtFrom.ToString("yyyy-MM-dd") + "'";
                        dsD = con.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            ShiftAttended = dsD.Tables[0].Rows[0]["SHIFTATTENDED"].ToString().Trim();
                        }*/
                        StrSql = "";
                        if (hday.ToString().Trim() == "N")
                        {
                            FirstHalfLvCode = null;
                            LvType1 = null;
                            SecondHalfLvCode = null;
                            LvType2 = null;
                            lvamount = 1;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "select isnull(SECONDHALFLEAVECODE,''),LeaveType2 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = lvcode.ToString().Trim();
                                    LvType1 = lvtype.ToString().Trim();
                                    SecondHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType2 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "select isnull(FIRSTHALFLEAVECODE,''),LeaveType1 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType1 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    SecondHalfLvCode = lvcode.ToString().Trim();
                                    LvType2 = lvtype.ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        if (hday.ToString().Trim() == "N")
                        {
                            StrSql = "Update tblTimeRegister Set LeaveCode='" + lvcodeTm.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "Update tblTimeRegister Set FIRSTHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType1='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount1=" + lvamount1 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "Update tblTimeRegister Set SECONDHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType2='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount2=" + lvamount2 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        result = con.execute_NonQuery(StrSql);
                        if (result > 0)
                        {
                            UPD.Upd(Pcode, dtFrom, lvamount, lvamount1, lvamount2, lvcode, FirstHalfLvCode, SecondHalfLvCode, lvtype, LvType1, LvType2, ShiftAttended, Holiday, OldStatus);
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
        }
        catch
        {

        }
    }
    protected void cmdGenerate_Click(object sender, EventArgs e)
    {
        string ReportView = "";
        string msg = "";
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        try
        {
            /*if ((Session["PAYCODE"].ToString().ToString().ToUpper().Trim() == "ADMIN") && (Session["usertype"].ToString().ToString().ToUpper() == "A"))
            {
                if (Session["SelectionQuery"] == null)
                {
                    if (Session["Com_Selection"] == null)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Please Select a company.');", true);
                        return;
                    }
                }
            }*/
            Session["ExportInto"] = 0;
            try
            {
                FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Invalid Date Format');", true);
                txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
                return;
            }
            //FromDate = Convert.ToDateTime(txtDateFrom.Text);
            //toDate = Convert.ToDateTime(txtToDate.Text);

            if (toDate < FromDate)
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('To Date must be greater than From Date');", true);
                return;
            }
            TimeSpan ts = toDate.Subtract(FromDate);
            int day = ts.Days;
            if (day > 30)
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Difference is more than one Month');", true);
                return;
            }
            UpdateTimeRegister();
            OpenPopupPage = "";
            datefrom = txtDateFrom.Text.ToString().Trim();
            dateTo = txtToDate.Text.ToString().Trim();
            if (Session["SelectionQuery"] != null)
            {
                Selection = Session["SelectionQuery"].ToString();
            }
            else
            {
                if ((Session["usertype"].ToString().Trim() == "A") && (Session["PAYCODE"].ToString().ToString().ToUpper() != "ADMIN"))
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                    }
                }
                else if (Session["usertype"].ToString().Trim() == "H")
                {
                    if (ReportView.ToString().Trim() == "Y")
                    {
                        strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                        dsEmpCount = con.FillDataSet(strsql);
                        if (dsEmpCount.Tables[0].Rows.Count > 0)
                        {
                            for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                            {
                                EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                            }
                            if (!string.IsNullOrEmpty(EmpCodes))
                            {
                                EmpCodes = EmpCodes.Substring(1);
                            }
                            Selection += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                        }
                    }
                    else
                    {

                        if (Session["Auth_Comp"] != null)
                        {
                            Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                        }
                        if (Session["Auth_Dept"] != null)
                        {
                            Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                        }
                    }
                }
                else if ((Session["usertype"].ToString().Trim() == "A") && (Session["PAYCODE"].ToString().ToString().ToUpper() == "ADMIN"))
                {
                    if (Session["Com_Selection"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Com_Selection"].ToString() + ") ";
                    }
                    if (Session["Dept_Selection"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Dept_Selection"].ToString() + ") ";
                    }
                }
            }
            if (radEmployeeWisePerformance.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        double present = 0, absent = 0, leave = 0, wo = 0, hld = 0, totalhr = 0;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Paycode");
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("Date");
                        dTbl.Columns.Add("Day");
                        dTbl.Columns.Add("Shift");
                        dTbl.Columns.Add("In");
                        dTbl.Columns.Add("Out");
                        dTbl.Columns.Add("Late Arrival");
                        dTbl.Columns.Add("Early Departure");
                        dTbl.Columns.Add("Hours Worked");
                        dTbl.Columns.Add("Over Time");
                        dTbl.Columns.Add("Status");

                        dTbl.Rows[0][0] = ("Paycode");

                        int ct = 0;
                        strsql = "";
                        strsql = "select paycode,empname from tblemployee where  (Active='Y' or Active='N') " +
                            "And (tblemployee.LeavingDate >= '" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and dateofjoin <= '" + toDate.ToString("yyyy-MM-dd") + "' ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                present = 0; absent = 0; leave = 0; wo = 0; hld = 0; totalhr = 0;
                                dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1][0].ToString() + " ( " + dsResult.Tables[0].Rows[i1][1].ToString() + " )";

                                //dRow = dTbl.NewRow();
                                //dTbl.Rows.Add(dRow);

                                dTbl.Rows[1][0] = ("Date");
                                dTbl.Rows[1][1] = ("Day");
                                dTbl.Rows[1][2] = ("Shift");
                                dTbl.Rows[1][3] = ("In");
                                dTbl.Rows[1][4] = ("Out");
                                dTbl.Rows[1][5] = ("Late Arrival");
                                dTbl.Rows[1][6] = ("Early Departure");
                                dTbl.Rows[1][7] = ("Hours Worked");
                                dTbl.Rows[1][8] = ("Over Time");
                                dTbl.Rows[1][9] = ("Status");

                                ct = ct + 1;
                                strsql1 = "select convert(char(10),dateoffice,103) 'date',Substring(datename(dw,dateoffice),0,4) 'DayName',convert(char(5),in1,108) 'in1',convert(char(5),out2,108) 'out2',shiftattended 'shift',status, " +
                                        "case when latearrival=0 then null else Cast(latearrival / 60 as Varchar) +':' +Cast(latearrival % 60 as Varchar) end as 'latearrival', " +
                                        "case when earlydeparture=0 then null else Cast(earlydeparture/ 60 as Varchar) +':' +Cast(earlydeparture % 60 as Varchar) end as 'earlyarrival'," +
                                        "case when hoursworked=0 then null else substring(convert(varchar(20),dateadd(minute,hoursworked,0),108),0,6) end as 'hoursworked', " +
                                        "case when otduration=0 then null else Cast(otduration / 60 as Varchar) +':' +Cast(otduration  % 60 as Varchar) end as 'otduration' " +
                                        ",hoursworked,presentvalue,absentvalue,leavevalue,holiday_value,wo_value " +
                                        "from tbltimeregister where paycode='" + dsResult.Tables[0].Rows[i1][0].ToString() + "' and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and " + "'" + toDate.ToString("yyyy-MM-dd") + "'  ";
                                // strsql1 = "select * from tbltimeregister where paycode='" + dsResult.Tables[0].Rows[i1][0].ToString() + "' and dateoffice between '2012-07-01' and '2012-07-31'";
                                dsResult1 = con.FillDataSet(strsql1);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    dTbl.Rows[ct][0] = ("Date");
                                    dTbl.Rows[ct][1] = ("Day");
                                    dTbl.Rows[ct][2] = ("Shift");
                                    dTbl.Rows[ct][3] = ("In");
                                    dTbl.Rows[ct][4] = ("Out");
                                    dTbl.Rows[ct][5] = ("Late Arrival");
                                    dTbl.Rows[ct][6] = ("Early Departure");
                                    dTbl.Rows[ct][7] = ("Hours Worked");
                                    dTbl.Rows[ct][8] = ("Over Time");
                                    dTbl.Rows[ct][9] = ("Status");

                                    ct = ct + 1;

                                    for (int j1 = 0; j1 < dsResult1.Tables[0].Rows.Count; j1++)
                                    {

                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);

                                        dTbl.Rows[ct][0] = dsResult1.Tables[0].Rows[j1][0].ToString();
                                        dTbl.Rows[ct][1] = dsResult1.Tables[0].Rows[j1][1].ToString();
                                        dTbl.Rows[ct][2] = dsResult1.Tables[0].Rows[j1][4].ToString();
                                        dTbl.Rows[ct][3] = dsResult1.Tables[0].Rows[j1][2].ToString();
                                        dTbl.Rows[ct][4] = dsResult1.Tables[0].Rows[j1][3].ToString(); ;
                                        dTbl.Rows[ct][5] = dsResult1.Tables[0].Rows[j1][6].ToString();
                                        dTbl.Rows[ct][6] = dsResult1.Tables[0].Rows[j1][7].ToString();
                                        dTbl.Rows[ct][7] = dsResult1.Tables[0].Rows[j1][8].ToString();
                                        dTbl.Rows[ct][8] = dsResult1.Tables[0].Rows[j1][9].ToString(); ;
                                        dTbl.Rows[ct][9] = dsResult1.Tables[0].Rows[j1][5].ToString();
                                        totalhr = totalhr + Convert.ToDouble(dsResult1.Tables[0].Rows[j1][10].ToString());
                                        present += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][11].ToString());
                                        absent += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][12].ToString());
                                        leave += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][13].ToString());
                                        hld += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][14].ToString());
                                        wo += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][15].ToString());

                                        ct = ct + 1;
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    //ct = ct + 1;
                                    dTbl.Rows[ct][0] = "Present : " + present + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Absent : " + absent + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;leave : " + leave + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Holiday : " + hld + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Week Offs : " + wo + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Working : " + hm.minutetohour(totalhr.ToString());
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                                ct = ct + 1;
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                            }
                            dTbl.Columns.Remove("Status");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }





                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }


                        msg = "Employee Performance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='10' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='10' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='10' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='10' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='10' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                            //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                        }
                                    }
                                }
                                if (Tmp.Contains(field1.ToString()) && !string.IsNullOrEmpty(field1.ToString().Trim()))
                                {
                                    sb.Append("<td colspan='10'  style='background-color:#DCF7FA;text-align: left;font-weight:bold;font-size:13px;padding-left:15px'>" + Tmp + "</td>");
                                    break;
                                }
                                else if (Tmp.Contains("("))
                                {
                                    sb.Append("<td colspan='10'  style='text-align: center;font-weight:bold;font-size:13px'>" + Tmp + "</td>");
                                    break;
                                }
                                else if (Tmp.Contains("Present"))
                                {
                                    sb.Append("<td colspan='10'  style='text-align: left;font-weight:bold;font-size:14px;padding-left:15px'>" + Tmp + "</td>");
                                    break;
                                }
                                else
                                {
                                    if (i >= 4)
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeePerformance.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/EmployeePerformance.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report available in Excel Format');", true);
                    return;
                }
            }

            else if (radAttendance1.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        string field1 = "";
                        string Tmp = "";
                        int x, i;
                        double present = 0, absent = 0, leave = 0, wo = 0, hld = 0, totalhr = 0;
                        double PAccom = 0;
                        double CAccom = 0;

                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Paycode");
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("Date");
                        dTbl.Columns.Add("Day");
                        dTbl.Columns.Add("Shift");
                        dTbl.Columns.Add("In");
                        dTbl.Columns.Add("Out");
                        dTbl.Columns.Add("Shift Late");
                        dTbl.Columns.Add("Shift Early");
                        dTbl.Columns.Add("Hours Worked");
                        dTbl.Columns.Add("Over Time");
                        dTbl.Columns.Add("Status");
                        dTbl.Columns.Add("Personal Accommodation");
                        dTbl.Columns.Add("Company Accommodation");

                        dTbl.Rows[0][0] = ("Paycode");

                        int ct = 0;
                        strsql = "";
                        strsql = "select paycode,empname from tblemployee where  (Active='Y' or Active='N') " +
                            "And (tblemployee.LeavingDate >= '" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and dateofjoin <= '" + toDate.ToString("yyyy-MM-dd") + "' ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                present = 0; absent = 0; leave = 0; wo = 0; hld = 0; totalhr = 0;
                                dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1][0].ToString() + " ( " + dsResult.Tables[0].Rows[i1][1].ToString() + " )";

                                dTbl.Rows[1][0] = ("Date");
                                dTbl.Rows[1][1] = ("Day");
                                dTbl.Rows[1][2] = ("Shift");
                                dTbl.Rows[1][3] = ("In");
                                dTbl.Rows[1][4] = ("Out");
                                dTbl.Rows[1][5] = ("Late Arrival");
                                dTbl.Rows[1][6] = ("Early Departure");
                                dTbl.Rows[1][7] = ("Hours Worked");
                                dTbl.Rows[1][8] = ("Over Time");
                                dTbl.Rows[1][9] = ("Status");
                                dTbl.Rows[1][10] = ("Personal Accommodation");
                                dTbl.Rows[1][11] = ("Company Accommodation");

                                ct = ct + 1;
                                strsql1 = "select convert(char(10),dateoffice,103) 'date',Substring(datename(dw,dateoffice),0,4) 'DayName',convert(char(5),in1,108) 'in1',convert(char(5),out2,108) 'out2',shiftattended 'shift',status, " +
                                        "case when latearrival=0 then null else Cast(latearrival / 60 as Varchar) +':' +Cast(latearrival % 60 as Varchar) end as 'latearrival', " +
                                        "case when earlydeparture=0 then null else Cast(earlydeparture/ 60 as Varchar) +':' +Cast(earlydeparture % 60 as Varchar) end as 'earlyarrival'," +
                                        "case when hoursworked=0 then null else substring(convert(varchar(20),dateadd(minute,hoursworked,0),108),0,6) end as 'hoursworked', " +
                                        "case when otduration=0 then null else Cast(otduration / 60 as Varchar) +':' +Cast(otduration  % 60 as Varchar) end as 'otduration' " +
                                        ",hoursworked,presentvalue,absentvalue,leavevalue,holiday_value,wo_value " +
                                        "from tbltimeregister where paycode='" + dsResult.Tables[0].Rows[i1][0].ToString() + "' and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and " + "'" + toDate.ToString("yyyy-MM-dd") + "'  ";
                                dsResult1 = con.FillDataSet(strsql1);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    dTbl.Rows[ct][0] = ("Date");
                                    dTbl.Rows[ct][1] = ("Day");
                                    dTbl.Rows[ct][2] = ("Shift");
                                    dTbl.Rows[ct][3] = ("In");
                                    dTbl.Rows[ct][4] = ("Out");
                                    dTbl.Rows[ct][5] = ("Late Arrival");
                                    dTbl.Rows[ct][6] = ("Early Departure");
                                    dTbl.Rows[ct][7] = ("Hours Worked");
                                    dTbl.Rows[ct][8] = ("Over Time");
                                    dTbl.Rows[ct][9] = ("Status");
                                    dTbl.Rows[ct][10] = ("Personal Accommodation");
                                    dTbl.Rows[ct][11] = ("Company Accommodation");

                                    ct = ct + 1;

                                    for (int j1 = 0; j1 < dsResult1.Tables[0].Rows.Count; j1++)
                                    {
                                        PAccom = 0;
                                        CAccom = 0;
                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);

                                        dTbl.Rows[ct][0] = dsResult1.Tables[0].Rows[j1][0].ToString();
                                        dTbl.Rows[ct][1] = dsResult1.Tables[0].Rows[j1][1].ToString();
                                        dTbl.Rows[ct][2] = dsResult1.Tables[0].Rows[j1][4].ToString();
                                        dTbl.Rows[ct][3] = dsResult1.Tables[0].Rows[j1][2].ToString();
                                        dTbl.Rows[ct][4] = dsResult1.Tables[0].Rows[j1][3].ToString(); ;
                                        dTbl.Rows[ct][5] = dsResult1.Tables[0].Rows[j1][6].ToString();
                                        dTbl.Rows[ct][6] = dsResult1.Tables[0].Rows[j1][7].ToString();
                                        dTbl.Rows[ct][7] = dsResult1.Tables[0].Rows[j1][8].ToString();
                                        dTbl.Rows[ct][8] = dsResult1.Tables[0].Rows[j1][9].ToString(); ;
                                        dTbl.Rows[ct][9] = dsResult1.Tables[0].Rows[j1][5].ToString();
                                        totalhr = totalhr + Convert.ToDouble(dsResult1.Tables[0].Rows[j1][10].ToString());
                                        if (dsResult1.Tables[0].Rows[j1][5].ToString().Trim() == "POW" || dsResult1.Tables[0].Rows[j1][5].ToString().Trim() == "POH")
                                        {
                                            PAccom = 3050;
                                            CAccom = 2400;
                                            dTbl.Rows[ct][10] = PAccom.ToString();
                                            dTbl.Rows[ct][11] = CAccom.ToString();

                                        }

                                        else if (dsResult1.Tables[0].Rows[j1][5].ToString().Trim() == "A" || dsResult1.Tables[0].Rows[j1][5].ToString().Trim() == "COF")
                                        {
                                            PAccom = 650;
                                            CAccom = 0;
                                            dTbl.Rows[ct][10] = PAccom.ToString();
                                            dTbl.Rows[ct][11] = CAccom.ToString();

                                        }
                                        else
                                        {
                                            PAccom = 1850;
                                            CAccom = 1200;
                                            dTbl.Rows[ct][10] = PAccom.ToString();
                                            dTbl.Rows[ct][11] = CAccom.ToString();

                                        }


                                        present += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][11].ToString());
                                        absent += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][12].ToString());
                                        leave += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][13].ToString());
                                        hld += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][14].ToString());
                                        wo += Convert.ToDouble(dsResult1.Tables[0].Rows[j1][15].ToString());

                                        ct = ct + 1;
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    //ct = ct + 1;
                                    dTbl.Rows[ct][0] = "Present : " + present + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Absent : " + absent + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;leave : " + leave + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Holiday : " + hld + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Week Offs : " + wo + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Working : " + hm.minutetohour(totalhr.ToString());
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                                ct = ct + 1;
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                            }
                            //dTbl.Columns.Remove("Status");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }





                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }


                        msg = "Employee Performance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='12' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='12' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='12' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='12' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='12' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                            //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                        }
                                    }
                                }
                                if (Tmp.Contains(field1.ToString()) && !string.IsNullOrEmpty(field1.ToString().Trim()))
                                {
                                    sb.Append("<td colspan='12'  style='background-color:#DCF7FA;text-align: left;font-weight:bold;font-size:13px;padding-left:15px'>" + Tmp + "</td>");
                                    break;
                                }
                                else if (Tmp.Contains("("))
                                {
                                    sb.Append("<td colspan='12'  style='text-align: center;font-weight:bold;font-size:13px'>" + Tmp + "</td>");
                                    break;
                                }
                                else if (Tmp.Contains("Present"))
                                {
                                    sb.Append("<td colspan='12'  style='text-align: left;font-weight:bold;font-size:14px;padding-left:15px'>" + Tmp + "</td>");
                                    break;
                                }
                                else
                                {
                                    if (i >= 4)
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeePerformance.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/EmployeePerformance.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report available in Excel Format');", true);
                    return;
                }
            }

            else if (radEmployeewiseAttendance.Checked)
            {

                strsql = "select tblemployee.presentcardno 'Card',tblemployee.empname 'Name',tbltimeregister.paycode 'PayCode',tblcompany.companyname 'Company', " +
                        "tblemployee.paycode 'Pay',tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "convert(decimal(10,2),sum(tbltimeregister.absentvalue)) 'Absent' ,convert(decimal(10,2),sum(tbltimeregister.presentvalue))'Present',convert(decimal(10,2),sum(tbltimeregister.leavevalue)) 'Leavevalue', " +
                        "convert(decimal(10,2),sum(tbltimeregister.holiday_value)) 'holiday',convert(decimal(10,2),sum(tbltimeregister.Wo_value)) 'WO', " +
                        "substring(convert(varchar(20),dateadd(minute,sum(tbltimeregister.otduration),0),108),0,6) 'OT'," +
                        "sum(tbltimeregister.otamount) 'OTAmount' " +
                        "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode  " +
                        "join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.dateoffice between " +
                        "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                        "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString() + " ";
                }
                strsql += " group by tbltimeregister.paycode,tblemployee.presentcardno,tblemployee.empname,tblcompany.companyname, " +
                "tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.paycode " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "Absent";
                            dTbl.Rows[0][6] = "Weekly Off";
                            dTbl.Rows[0][7] = "Holiday";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "OT";
                            dTbl.Rows[0][10] = "OT Amount";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = "Employee Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EmployeeWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EmployeeWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyEmployeeAttendance.aspx");
                    }
                    //Response.Redirect("Rpt_MonthlyEmployeeAttendance.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radDepartmentWise.Checked)
            {
                string DeptCode = "";
                string ComCode = "";
                DataSet dsAtt = new DataSet();
                /*strsql = "select tblemployee.departmentcode 'DeptCode',tbldepartment.departmentname 'DeptName', " +
                        "count(tblemployee.paycode) 'PayCode',tblcompany.companyname 'Company',tblcompany.companyCode 'CompanyID' from tblemployee  " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode join " +
                        "tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tblemployee.dateofjoin < '" + toDate.ToString("yyyy-MM-dd") + "' and " +
                        "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and " +
                        "tblemployee.active ='Y' ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += "group by tblemployee.departmentcode,tbldepartment.departmentname,tblcompany.companyname,tblcompany.companyCode ";*/

                strsql = "select tblemployee.departmentcode 'DeptCode',tbldepartment.departmentname 'DeptName', " +
                        "count(tblemployee.paycode) 'PayCode' from tblemployee  " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode join " +
                        " tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tblemployee.dateofjoin < '" + toDate.ToString("yyyy-MM-dd") + "' and " +
                        "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 and " +
                        "tblemployee.active ='Y' ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += "group by tblemployee.departmentcode,tbldepartment.departmentname";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("DeptCode");
                            dTbl.Columns.Add("DeptName");
                            dTbl.Columns.Add("TotalEmp");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Holiday");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Dept + " Code";
                            dTbl.Rows[0][2] = Global.getEmpInfo._Dept + " Name";
                            dTbl.Rows[0][3] = "Total " + Global.getEmpInfo._Msg;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "Absent";
                            dTbl.Rows[0][6] = "Leave";
                            dTbl.Rows[0][7] = "Weekly Off";
                            dTbl.Rows[0][8] = "Holiday";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();

                                    DeptCode = ds.Tables[0].Rows[i1][0].ToString();
                                    strsql = "select sum(presentValue),Sum(AbsentValue),Sum(leavevalue),Sum(Wo_value),sum(holiday_value) from tbltimeregister where paycode in (select paycode from tblemployee where  departmentcode='" + DeptCode.ToString() + "') and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' ";
                                    dsAtt = con.FillDataSet(strsql);
                                    if (dsAtt.Tables[0].Rows.Count > 0)
                                    {
                                        dTbl.Rows[ct][4] = dsAtt.Tables[0].Rows[0][0].ToString();
                                        dTbl.Rows[ct][5] = dsAtt.Tables[0].Rows[0][1].ToString();
                                        dTbl.Rows[ct][6] = dsAtt.Tables[0].Rows[0][2].ToString();
                                        dTbl.Rows[ct][7] = dsAtt.Tables[0].Rows[0][3].ToString();
                                        dTbl.Rows[ct][8] = dsAtt.Tables[0].Rows[0][4].ToString();
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = Global.getEmpInfo._Dept + " Wise Attendance from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=DepartmentWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/DepartmentWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyDepartment.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                }
            }
            else if (radMonthlyLate.Checked)
            {
                strsql = "select sno='', case  when tbltimeregister.latearrival != 0 then substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6) else null end as 'Late', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "tbltimeregister.paycode 'paycode',datename(dd,tbltimeregister.dateoffice)'Date',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "Total=(SELECT Case When sum(latearrival)/60 = 1 Then Convert(nvarchar(10),sum(latearrival)/60) Else Convert(nvarchar(10),sum(latearrival)/60)  End + ':' + " +
                        "Case When sum(latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(latearrival)%60) Else '0'+Convert(nvarchar(2),sum(latearrival)%60) End " +
                        "from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "from tbltimeregister join tblemployee on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.paycode in " +
                        "(select paycode from tbltimeregister where latearrival > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += "group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.latearrival,tbltimeregister.PAYCODE,tbltimeregister.DateOFFICE " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (RadExport.SelectedIndex == 0)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[i]["CardNo"] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.latearrival),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and latearrival > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.latearrival,0),108) 'latearrival'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["latearrival"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            msg = "Late Arrival Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LateArrivalRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LateArrivalRegister.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {

                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyLate.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radEarlyDeparture.Checked)
            {
                strsql = "select sno='', case  when tbltimeregister.earlydeparture != 0 then substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlydeparture,0),108),0,6) else null end as 'Late', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "tbltimeregister.paycode 'paycode',datename(dd,tbltimeregister.dateoffice)'Date',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "Total=(SELECT Case When sum(earlydeparture)/60 = 1 Then Convert(nvarchar(10),sum(earlydeparture)/60) Else Convert(nvarchar(10),sum(earlydeparture)/60)  End + ':' + " +
                        "Case When sum(earlydeparture)%60 >= 10 Then Convert(nvarchar(2),sum(earlydeparture)%60) Else '0'+Convert(nvarchar(2),sum(earlydeparture)%60) End " +
                        "from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "from tbltimeregister join tblemployee on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.paycode in " +
                        "(select paycode from tbltimeregister where earlydeparture > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }

                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.earlydeparture,tbltimeregister.PAYCODE,tbltimeregister.DateOFFICE " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[i]["CardNo"] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.earlydeparture),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and earlydeparture > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.earlydeparture,0),108) 'earlydeparture'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["earlydeparture"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            msg = "Early Departure Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EarlyDepartureRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EarlyDeparture.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyEarlyDeparture.aspx");
                    }
                    //Response.Redirect("Rpt_MontlyEarlyDeparture.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radAbsent.Checked)
            {
                strsql = "select sno='', case  when tbltimeregister.absentvalue != 0 then tbltimeregister.absentvalue else null end as 'Late', " +
                          "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                          "tbltimeregister.paycode 'paycode',datename(dd,tbltimeregister.dateoffice)'Date',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                          "Total=(select convert(decimal(10,2),sum(tbltimeregister.absentvalue)) " +
                          "from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                          " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                          "from tbltimeregister join tblemployee on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                          "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                          "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                          "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                          "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                          "where tbltimeregister.dateoffice between  " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                          " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.paycode in " +
                          "(select paycode from tbltimeregister where absentvalue > 0 and dateoffice between " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                          " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                          "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.absentvalue,tbltimeregister.PAYCODE,tbltimeregister.DateOFFICE " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[i]["CardNo"] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalLate=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',TblTimeregister.absentvalue 'absentvalue'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["absentvalue"].ToString().Trim();
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = dsStatus.Tables[0].Rows[0]["TotalLate"].ToString().Trim();
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            msg = "Absenteeism Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=AbsenteeismRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/AbsenteeismDeparture.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyAbsent.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            //Shift Wise presence
            else if (radshiftwise.Checked)
            {
                DataSet dsTime = new DataSet();
                double Present = 0;
                double Absent = 0;
                double WO = 0;
                double Leave = 0;
                double OT = 0;
                double OTAmt = 0;

                strsql = "select tblemployee.paycode'paycode',tblemployee.empname 'empname', " +
                         "tblemployee.presentcardno 'presentcardno',  " +
                         "tblEmployeeShiftMaster.paycode 'code', " +
                         "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                         "tblcompany.companyname 'company' from tblemployee join tblcompany on " +
                         "tblemployee.companycode=tblcompany.companycode " +
                         "join tblEmployeeShiftMaster on tblemployee.paycode=tblEmployeeShiftMaster.paycode " +
                         "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                         "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                         "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                         "where tblemployee.active='Y' ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 " +
                "group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblEmployeeShiftMaster.PAYCODE " +
                "order by " + ddlSorting.SelectedItem.Value.ToString() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Present";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "W.Off";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "OT";
                            dTbl.Rows[0][10] = "OT Amt";

                            int rowCount = 1;
                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    Present = 0;
                                    Absent = 0;
                                    WO = 0;
                                    Leave = 0;
                                    OT = 0;
                                    OTAmt = 0;
                                    dTbl.Rows[ct][0] = rowCount.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    strsql1 = "select  Paycode,Shiftattended,convert(decimal(10,2),sum(Presentvalue))'Present',convert(decimal(10,2),sum(absentvalue))'Absent',convert(decimal(10,2),sum(WO_VALUE))'WeeklyOff', " +
                                            "convert(decimal(10,2),sum(leavevalue))'LeaveValue',sum(otduration) 'OT',convert(decimal(10,2),sum(otamount))'OTAmount' " +
                                            " from tbltimeregister where dateoffice between convert(datetime,convert(char(10),'" + FromDate.ToString() + "',103),103) and " +
                                            "convert(datetime,convert(char(10),'" + toDate.ToString() + "',103),103) and paycode = '" + ds.Tables[0].Rows[i1][0].ToString().Trim() + "' " +
                                            "group by shiftattended,paycode order by paycode";
                                    dsTime = con.FillDataSet(strsql1);
                                    if (dsTime.Tables[0].Rows.Count > 0)
                                    {
                                        for (int kp = 0; kp < dsTime.Tables[0].Rows.Count; kp++)
                                        {
                                            dTbl.Rows[ct][4] = dsTime.Tables[0].Rows[kp][1].ToString();
                                            dTbl.Rows[ct][5] = dsTime.Tables[0].Rows[kp][2].ToString();
                                            dTbl.Rows[ct][6] = dsTime.Tables[0].Rows[kp][3].ToString();
                                            dTbl.Rows[ct][7] = dsTime.Tables[0].Rows[kp][4].ToString();
                                            dTbl.Rows[ct][8] = dsTime.Tables[0].Rows[kp][5].ToString();
                                            dTbl.Rows[ct][9] = hm.minutetohour(dsTime.Tables[0].Rows[kp][6].ToString());
                                            dTbl.Rows[ct][10] = dsTime.Tables[0].Rows[kp][7].ToString();

                                            Present += Convert.ToDouble(dsTime.Tables[0].Rows[kp][2].ToString());
                                            Absent += Convert.ToDouble(dsTime.Tables[0].Rows[kp][3].ToString());
                                            WO += Convert.ToDouble(dsTime.Tables[0].Rows[kp][4].ToString());
                                            leave += Convert.ToDouble(dsTime.Tables[0].Rows[kp][5].ToString());
                                            OT += Convert.ToDouble(dsTime.Tables[0].Rows[kp][6].ToString());
                                            OTAmt += Convert.ToDouble(dsTime.Tables[0].Rows[kp][7].ToString());

                                            dRow = dTbl.NewRow();
                                            dTbl.Rows.Add(dRow);
                                            ct = ct + 1;
                                        }
                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);
                                        ct = ct + 1;
                                        dTbl.Rows[ct][4] = "Sub Total";
                                        dTbl.Rows[ct][5] = Present.ToString();
                                        dTbl.Rows[ct][6] = Absent.ToString();
                                        dTbl.Rows[ct][7] = WO.ToString();
                                        dTbl.Rows[ct][8] = Leave.ToString();
                                        dTbl.Rows[ct][9] = hm.minutetohour(OT.ToString());
                                        dTbl.Rows[ct][10] = OTAmt.ToString();

                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                    rowCount = rowCount + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = " SHIFT WISE ATTENDANCE DETAILS FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                        }
                                    }
                                    else if ((i == 3))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                        }
                                    }
                                    else if ((i == 4))
                                    {
                                        if (Tmp.ToString().Trim() == "Sub Total")
                                        {
                                            sb.Append("<td style='width:60px;text-align: left;font-weight:bold;font-size:10px;color:red'>" + Tmp + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                    else
                                    {
                                        sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ShiftWiseAtt.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ShiftWiseAtt.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyShiftWise.aspx");
                    }
                    //Response.Redirect("Rpt_MonthlyShiftWise.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            //OT Register
            else if (radMontlyOT.Checked)
            {
                strsql = "select sno='', tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "datename(dd,tbltimeregister.dateoffice) 'Date',case when tbltimeregister.otduration!=0 then " +
                    //"Cast(otduration/ 60 as Varchar) +'.' +Cast(otduration % 60 as Varchar) else null end as 'OT' , " +
                        "substring(convert(varchar(20),dateadd(minute,tbltimeregister.otduration,0),108),0,6) else null end as 'OT' ," +
                        "Total=(select Case When sum(tbltimeregister.otduration)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.otduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.otduration)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.otduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) End " +
                        "from tbltimeregister where tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.dateoffice between  " + "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        "'" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode  " +
                        "join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and  " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.paycode in " +
                        "(select paycode from tbltimeregister where otduration > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
               "tbltimeregister.DateOFFICE,tbltimeregister.OTDURATION " +
               "order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[i]["CardNo"] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalOTduration=(select isnull(sum(tbltimeregister.otduration),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and otduration > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.otduration,0),108) 'otduration'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                if (dsStatus.Tables[0].Rows[st]["otduration"].ToString() != "00:00")
                                                {
                                                    dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["otduration"].ToString().Trim();
                                                }
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalOTduration"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            msg = "Over Time Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=OverTimeRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/OverTimeRegister.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {

                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyOT.aspx");
                        //Response.Redirect("Rpt_MontlyOT.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            //Monthly OT Summary
            else if (radOT.Checked)
            {
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                    "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                    "Case When sum(tbltimeregister.otduration)/60 < 10 Then '0'+Convert(nvarchar(10),sum(tbltimeregister.otduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.otduration)/60)  End + ':' + " +
                    "Case When sum(tbltimeregister.otduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) End  as 'OT', " +
                    "convert(decimal(10,2),Sum(otamount)) 'Amount' " +
                    "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany on  " +
                    "tblemployee.companycode=tblcompany.companycode " +
                    "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                    "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                    "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                    "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                    "where tbltimeregister.paycode in " +
                    "(select paycode from tbltimeregister where otduration > 0 and dateoffice between " +
                    " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                    " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                    "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.paycode, " +
                "tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                "order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Session["DateFrom"] = datefrom.ToString();
                    Session["DateTo"] = dateTo.ToString();
                    Session["NewDataSet"] = ds;
                    OpenChilePage("Reports/MonthlyReports/Rpt_MontlyOTSummary.aspx");
                    //Response.Redirect("Rpt_MontlyOTSummary.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radOverStay.Checked)
            {
                strsql = "select sno='', tblemployee.paycode 'Paycode',tblemployee.empname 'Name', " +
                        "tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "datename(dd,tbltimeregister.dateoffice) 'Date'," +
                        "case when tbltimeregister.osduration!=0 then substring(convert(varchar(20),dateadd(minute,tbltimeregister.osduration,0),108),0,6) else null end as 'OverStay' , " +
                        "Total=(select Case When sum(tbltimeregister.osduration)/60 < 10 Then '0'+Convert(nvarchar(10),sum(tbltimeregister.osduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.osduration)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.osduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) End " +
                        "from tbltimeregister where tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' " +
                        "and  tbltimeregister.paycode in (select paycode from tbltimeregister where osduration > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "and active='Y' and " +
                        "(tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno, " +
                "tbltimeregister.DateOFFICE,tbltimeregister.OSDURATION " +
                "order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Code");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Columns.Add("TotalLate");
                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = "SNo";
                            dTbl.Rows[i]["Code"] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[i]["CardNo"] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }
                            dTbl.Rows[i]["TotalLate"] = "Total";
                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.Presentcardno,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    dTbl.Rows[i]["Code"] = Tmp;
                                    dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                    dTbl.Rows[i]["CardNo"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();

                                    x = 4;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " TotalOsduration=(select isnull(sum(tbltimeregister.osduration),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and osduration > 0   )  " +
                                                   " ,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',convert(varchar(5),dateadd(minute,TblTimeregister.osduration,0),108) 'osduration'" +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                if (dsStatus.Tables[0].Rows[st]["osduration"].ToString() != "00:00")
                                                {
                                                    dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["osduration"].ToString().Trim();
                                                }
                                                x++;
                                            }
                                            if (dsStatus.Tables[0].Rows.Count > 0)
                                            {
                                                dTbl.Rows[i]["TotalLate"] = hm.minutetohour(dsStatus.Tables[0].Rows[0]["TotalOsduration"].ToString().Trim());
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            msg = "OverStay Register for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (i == 3)
                                        sb.Append("<td style='width:150px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=OverStayRegister.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/OverStayRegister.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {

                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyOverStay.aspx");
                    }
                    //Response.Redirect("Rpt_MontlyOverStay.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radShiftSchedule.Checked)
            {
                strsql = "select sno='', tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'CardNo', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "tblemployee.designation 'Designation',tblemployeeshiftmaster.shifttype 'ShiftType', " +
                        "tblemployeeshiftmaster.shift 'ShiftPattern',tblemployeeshiftmaster.firstoffday 'FirstOff', " +
                        "tblemployeeshiftmaster.secondoffday '2ndOff',tblemployeeshiftmaster.secondofftype '2ndOffType', " +
                        "tblemployeeshiftmaster.alternate_off_days '2ndOffDays', " +
                        "datename(dd,tbltimeregister.dateoffice) 'Date',tbltimeregister.shift 'Shift', " +
                        "tblcompany.companyname 'Company',datename(mm,'" + FromDate.ToString("yyyy-MM-dd") + "') 'Month' " +
                        "from tblemployee join tblemployeeshiftmaster on  " +
                        "tblemployee.paycode=tblemployeeshiftmaster.paycode join tbltimeregister on " +
                        "tblemployee.paycode=tbltimeregister.paycode join tblcompany on " +
                        "tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "where tbltimeregister.dateoffice between Dateadd(m, Datediff(m, 30, '" + FromDate.ToString("yyyy-MM-dd") + "' ), 0) " +
                        "and Dateadd(s,-1,Dateadd(mm, Datediff(m,0,'" + FromDate.ToString("yyyy-MM-dd") + "')+1,0)) " +
                        "and tblemployee.active='Y' " +
                        "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        string Tmp = "";
                        int colCount = 0;
                        DateTime dtnew = System.DateTime.MinValue;
                        dtnew = FromDate;
                        try
                        {
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            dTbl.Columns.Add("Sno");
                            dTbl.Columns.Add("Card");
                            dTbl.Columns.Add("Paycode");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Designation");
                            dTbl.Columns.Add("Shifttype");
                            dTbl.Columns.Add("Shiftpattern");

                            dTbl.Columns.Add("WO1");
                            dTbl.Columns.Add("WO2");
                            dTbl.Columns.Add("WO2Type");
                            dTbl.Columns.Add("WO2Days");

                            while (dtnew <= toDate)
                            {
                                dTbl.Columns.Add(dtnew.ToString("dd"));
                                dtnew = dtnew.AddDays(1);
                            }

                            DataRow dRow;
                            i = 0;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i][0] = "SNo";
                            dTbl.Rows[i][1] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[i][2] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[i][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[i][4] = Global.getEmpInfo._Designation;
                            dTbl.Rows[i][5] = "Shift Type";
                            dTbl.Rows[i][6] = "Shift Pattern";
                            dTbl.Rows[i][7] = "1st WO";
                            dTbl.Rows[i][8] = "2nd WO";
                            dTbl.Rows[i][9] = "2nd WO Type";
                            dTbl.Rows[i][10] = "2nd WO Days";
                            dtnew = FromDate;
                            string colname = "";
                            while (dtnew <= toDate)
                            {
                                colname = dtnew.ToString("dd");
                                dTbl.Rows[i][colname] = dtnew.ToString("dd");
                                dtnew = dtnew.AddDays(1);
                            }

                            colCount = Convert.ToInt32(dTbl.Columns.Count);
                            /*if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                         " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                        " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                            }*/

                            strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name'," +
                               "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                               "tblemployee.designation 'Designation',tblemployeeshiftmaster.shifttype 'ShiftType', " +
                               "tblemployeeshiftmaster.shift 'ShiftPattern',tblemployeeshiftmaster.firstoffday 'FirstOff', " +
                               "tblemployeeshiftmaster.secondoffday '2ndOff',tblemployeeshiftmaster.secondofftype '2ndOffType', " +
                               "tblemployeeshiftmaster.alternate_off_days '2ndOffDays', " +
                               "tblcompany.companyname 'Company',datename(mm,'" + FromDate.ToString("yyyy-MM-dd") + "') 'Month' " +
                               "from tblemployee join tblemployeeshiftmaster on  " +
                               "tblemployee.paycode=tblemployeeshiftmaster.paycode join tblcompany on " +
                               "tblemployee.companycode=tblcompany.companycode " +
                               "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                               "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                               "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                               "where tblemployee.active='Y' " +
                               "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                            if (Selection.ToString().Trim() != "")
                            {
                                strsql += Selection.ToString();
                            }
                            strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";


                            //and tblemployee.paycode='1389' 
                            DataSet dsMuster = new DataSet();
                            dsMuster = con.FillDataSet(strsql);
                            if (dsMuster.Tables[0].Rows.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            i = 1;
                            x = 1;
                            int p = 1;
                            DataSet dsStatus;
                            DateTime dRoster = System.DateTime.MinValue;
                            DateTime djoin = System.DateTime.MinValue;
                            int dd = 0;

                            if (dsMuster.Tables[0].Rows.Count > 0)
                            {
                                for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                                {
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    dTbl.Rows[i][0] = Convert.ToString(i);
                                    Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                    dTbl.Rows[i][2] = Tmp;  //dr["Paycode"].ToString().Trim();
                                    dTbl.Rows[i][1] = dsMuster.Tables[0].Rows[ms]["presentcardno"].ToString().Trim();
                                    dTbl.Rows[i][3] = dsMuster.Tables[0].Rows[ms]["Name"].ToString().Trim();
                                    dTbl.Rows[i][4] = dsMuster.Tables[0].Rows[ms]["Designation"].ToString().Trim();
                                    dTbl.Rows[i][5] = dsMuster.Tables[0].Rows[ms]["shifttype"].ToString().Trim();
                                    dTbl.Rows[i][6] = dsMuster.Tables[0].Rows[ms]["shiftpattern"].ToString().Trim();
                                    dTbl.Rows[i][7] = dsMuster.Tables[0].Rows[ms]["FirstOff"].ToString().Trim();
                                    dTbl.Rows[i][8] = dsMuster.Tables[0].Rows[ms]["2ndOff"].ToString().Trim();
                                    dTbl.Rows[i][9] = dsMuster.Tables[0].Rows[ms]["2ndOfftype"].ToString().Trim();
                                    dTbl.Rows[i][10] = dsMuster.Tables[0].Rows[ms]["2ndOffdays"].ToString().Trim();
                                    x = 11;
                                    {
                                        dRoster = System.DateTime.MinValue;
                                        djoin = System.DateTime.MinValue;
                                        FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        DateTime newFromDate = System.DateTime.MinValue;
                                        newFromDate = FromDate;

                                        strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                        ds = new DataSet();
                                        dsStatus = new DataSet();
                                        ds = con.FillDataSet(strsql1);
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            }
                                            catch
                                            {
                                                continue;
                                            }

                                            if (newFromDate <= djoin)
                                            {
                                                if (newFromDate > djoin)
                                                {
                                                    FromDate = newFromDate;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate < djoin)
                                                {
                                                    FromDate = djoin;
                                                    if (djoin < newFromDate)
                                                    {
                                                        FromDate = newFromDate;
                                                    }
                                                    strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                                else if (newFromDate == djoin)
                                                {
                                                    //FromDate = dRoster;
                                                    strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                    ds = con.FillDataSet(strsql1);
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                        if (dd > 0)
                                                        {
                                                            x = x + dd;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                   " datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift  " +
                                                   " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";
                                        dd = 0;
                                        strsql1 = "";
                                        dsStatus = con.FillDataSet(strsql);
                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                            {
                                                dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["Shift"].ToString().Trim();
                                                x++;
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }
                            int widthset = 0;
                            int colcount = Convert.ToInt32(dTbl.Columns.Count);
                            msg = "Shift Schedule for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                            x = dTbl.Columns.Count;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 3) || (i == 4))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:150px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else
                                    {
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:50px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //   sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ShiftSchedule.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ShiftSchedule.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {

                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }

                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyShiftSchedule.aspx");
                    }
                    //Response.Redirect("Rpt_MontlyShiftSchedule.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radEarlyDeptSummary.Checked)
            {
                double TotalDays = 0;
                /*strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "total=(select convert(decimal(10,2),count(tbltimeregister.earlydeparture)) from tbltimeregister where tbltimeregister.earlydeparture > 0 and " +
                            "tbltimeregister.dateoffice between " +
                            "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                            "and tblemployee.paycode=tbltimeregister.paycode), " +
                            "Case When sum(tbltimeregister.earlydeparture)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60) Else Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.earlydeparture)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) End 'EarlyDeparture' , " +
                            "Cast(sum(tbltimeregister.earlydeparture) / 60 as Varchar) +'.' +Cast(sum(tbltimeregister.earlydeparture) % 60 as Varchar) 'EarlyDeparture'  " +
                            "from tblemployee  join tbltimeregister  on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            "where tbltimeregister.paycode in  " +
                            "(select tbltimeregister.paycode from tbltimeregister where tbltimeregister.earlydeparture > 0 and tbltimeregister.dateoffice between  " +
                            "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            "'" + toDate.ToString("yyyy-MM-dd") + "') and " +
                            "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " ;
                            if (Selection.ToString().Trim() != "")
                            {
                                strsql += Selection.ToString();
                            }
                            strsql +=" group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                            ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                            "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";*/



                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', count(earlydeparture), " +
                        " Cast(sum(tbltimeregister.earlydeparture) / 60 as Varchar) +'.' +Cast(sum(tbltimeregister.earlydeparture) % 60 as Varchar) 'EarlyDeparture' from " +
                        " tblemployee  join tbltimeregister  on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        " where DateOFFICE between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and EARLYDEPARTURE > 0 and (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += "group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname ,tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno  order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "  ";



                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("ED");
                            dTbl.Columns.Add("Days");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Early Departure";
                            dTbl.Rows[0][5] = "No. of Days";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][8].ToString();
                                    TotalDays = TotalDays + Convert.ToDouble(ds.Tables[0].Rows[i1][8].ToString());

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;

                                dTbl.Rows[ct][0] = "";
                                dTbl.Rows[ct][1] = "";
                                dTbl.Rows[ct][2] = "Total";
                                dTbl.Rows[ct][3] = "";
                                dTbl.Rows[ct][4] = "";
                                dTbl.Rows[ct][5] = TotalDays.ToString();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = "Early Departure Summary from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:65;text-align: left;font-weight:normal;font-size:10px'>  </td>");
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EarlyDepartureSummary.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EarlyDepartureSummary.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyEarlyDeparture_Summary.aspx");
                        //Response.Redirect("Rpt_MontlyEarlyDeparture_Summary.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radLateArrivalSummary.Checked)
            {
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company'," +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "total=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode), " +
                        "first=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and latearrival between 0 and 10), " +
                        "second=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and latearrival between 11 and 30), " +
                        "third=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and latearrival between 31 and 60), " +
                        "forth=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and latearrival > 60) " +
                        "from tblemployee tblemployee join tbltimeregister tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany tblcompany on " +
                        "tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.paycode in " +
                        "(select paycode from tbltimeregister where latearrival > 0 and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.paycode, " +
                "tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("TotalLate");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Total Late";
                            dTbl.Rows[0][5] = ">(0.01)";
                            dTbl.Rows[0][6] = ">(0.10)";
                            dTbl.Rows[0][7] = ">(0.30)";
                            dTbl.Rows[0][8] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = "Late Arrival Summery from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LateArrival.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LateArrival.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_Montly_LateSummary.aspx");
                    }
                    //Response.Redirect("Rpt_Montly_LateSummary.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radLate_OverStay.Checked)
            {
                double Latehrs = 0;
                double daysCount = 0;
                double overstay = 0;
                string code = "";
                int lateDays = 0;
                /*strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "Late=(select Case When sum(tbltimeregister.latearrival)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60) Else Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) End  " +                        
                        "from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode), " +
                        "NoOfDays=(select convert(decimal(10,2), count(tbltimeregister.latearrival)) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode)," +
                        "OverStay=(select Cast(sum(tbltimeregister.osduration) / 60 as Varchar) +'.' +Cast(sum(tbltimeregister.osduration) % 60 as Varchar) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between  " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode) " +
                        "from tblemployee tblemployee join tbltimeregister tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.paycode in " +
                        "(select paycode from tbltimeregister where latearrival > 0 and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        " '" + toDate.ToString("yyyy-MM-dd") + "') " +
                        " and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " ;
                        if (Selection.ToString().Trim() != "")
                        {
                           strsql += Selection.ToString();
                        }
                        strsql +=" group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                        ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                        "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";*/


                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.DateOffice,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   " +
                        " tblTimeRegister.PayCode,tblTimeRegister.LateArrival,tblTimeRegister.OsDuration,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode,tblDepartment.DepartmentName  " +
                        " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment " +
                        " Where (tblTimeRegister.LateArrival>0 or tblTimeRegister.OsDuration>0) and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And " +
                        " tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' and  " +
                        " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And " +
                        " (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Late");
                            dTbl.Columns.Add("LateDays");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Late";
                            dTbl.Rows[0][5] = "No. of Days";
                            dTbl.Rows[0][6] = "Over Stay";

                            int i1 = 0;
                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                // for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                while (i1 <= ds.Tables[0].Rows.Count)
                                {
                                    Latehrs = 0;
                                    daysCount = 0;
                                    overstay = 0;
                                    lateDays = 0;
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["PayCode"].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Presentcardno"].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["EmpName"].ToString();

                                    code = ds.Tables[0].Rows[i1]["PayCode"].ToString();
                                    while (code == ds.Tables[0].Rows[i1]["PayCode"].ToString())
                                    {
                                        if ((Convert.ToDouble(ds.Tables[0].Rows[i1]["LateArrival"].ToString()) > 0) || (Convert.ToDouble(ds.Tables[0].Rows[i1]["OsDuration"].ToString()) > 0))
                                        {
                                            lateDays = lateDays + 1;
                                            Latehrs = Latehrs + Convert.ToDouble(ds.Tables[0].Rows[i1]["LateArrival"].ToString());
                                            overstay = overstay + Convert.ToDouble(ds.Tables[0].Rows[i1]["OsDuration"].ToString());
                                        }
                                        i1 = i1 + 1;
                                        if (i1 == Convert.ToInt32(ds.Tables[0].Rows.Count))
                                        {
                                            break;
                                        }
                                    }
                                    dTbl.Rows[ct][4] = hm.minutetohour(Latehrs.ToString());
                                    dTbl.Rows[ct][5] = lateDays.ToString();
                                    dTbl.Rows[ct][6] = hm.minutetohour(overstay.ToString());
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    if (i1 == Convert.ToInt32(ds.Tables[0].Rows.Count))
                                    {
                                        break;
                                    }
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = "Late And Over Stay " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LossOverstay.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LossOverstay.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyLate_Overstay.aspx");
                    }
                    //Response.Redirect("Rpt_MontlyLate_Overstay.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radTotalTimeLoss.Checked)
            {
                double Losshrs = 0;
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card',tblcompany.companyname 'Company', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "Late=(select isnull(Case When sum(tbltimeregister.latearrival)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60) Else Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) End, 0 ) from tbltimeregister tbltimeregister where tbltimeregister.latearrival > 0 and tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.latearrival > 0), " +
                            "LateMinute=(select isnull(Case When sum(tbltimeregister.latearrival)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60) Else Convert(nvarchar(10),sum(tbltimeregister.latearrival)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.latearrival)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.latearrival)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode ), " +
                            "NoOfDays=(select count(tbltimeregister.latearrival) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.latearrival >0 and tblemployee.paycode=tbltimeregister.paycode), " +
                            "EarlyDeptDays=(select count(tbltimeregister.earlydeparture) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.earlydeparture > 0 ), " +
                            "EarlyDept=(select isnull(Case When sum(tbltimeregister.earlydeparture)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60) Else Convert(nvarchar(10),sum(tbltimeregister.earlydeparture)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.earlydeparture)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.earlydeparture)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode), " +
                            "ExcesslunchDays=(select count(tbltimeregister.EXCLUNCHHOURS) from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and tbltimeregister.EXCLUNCHHOURS > 0 )," +
                            "Excesslunch=(select isnull(Case When sum(tbltimeregister.EXCLUNCHHOURS)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.EXCLUNCHHOURS)/60) Else Convert(nvarchar(10),sum(tbltimeregister.EXCLUNCHHOURS)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.EXCLUNCHHOURS)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.EXCLUNCHHOURS)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.EXCLUNCHHOURS)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode), " +
                            "OverStay=(select isnull(Case When sum(tbltimeregister.osduration)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.osduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.osduration)/60)  End + ':' + " +
                            "Case When sum(tbltimeregister.osduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.osduration)%60) End,0 ) " +
                            "from tbltimeregister tbltimeregister where tbltimeregister.dateoffice between  " +
                            " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblemployee.paycode=tbltimeregister.paycode and (earlydeparture > 0 or latearrival > 0 or EXCLUNCHHOURS > 0) ) " +
                            "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            "where tbltimeregister.paycode in " +
                            "( select paycode from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            " '" + toDate.ToString("yyyy-MM-dd") + "' and (earlydeparture > 0 or latearrival > 0 or EXCLUNCHHOURS > 0) ) " +
                            "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname  " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("LateHours");
                            dTbl.Columns.Add("LateDays");
                            dTbl.Columns.Add("EarlyHours");
                            dTbl.Columns.Add("EarlyDays");
                            dTbl.Columns.Add("ExcessLunchHours");
                            dTbl.Columns.Add("ExcessLunchDays");
                            dTbl.Columns.Add("LossHrs");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Late Hrs";
                            dTbl.Rows[0][5] = "Late Days";
                            dTbl.Rows[0][6] = "Early Hrs";
                            dTbl.Rows[0][7] = "Early Days";
                            dTbl.Rows[0][8] = "Excess Lunch Hours";
                            dTbl.Rows[0][9] = "Excess Lunch Days";
                            dTbl.Rows[0][10] = "Loss Hrs";
                            dTbl.Rows[0][11] = "Over Stay";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    Losshrs = Convert.ToDouble(hm.hour(ds.Tables[0].Rows[i1][8].ToString())) + Convert.ToDouble(hm.hour(ds.Tables[0].Rows[i1][12].ToString()));

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][10] = hm.minutetohour(Losshrs.ToString());
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = " TOTAL LOSS AND OVER STAY FROM " + FromDate.ToString("dd-MM-yyyy") + " TO " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LossOverstay.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LossOverstay.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MontlyTimeLoss.aspx");
                    }
                    //Response.Redirect("Rpt_MontlyTimeLoss.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radEmpAttendance_Percentage.Checked)
            {
                double monthDays = 0;
                monthDays = day + 1;
                strsql = "select tblemployee.presentcardno 'Card',tblemployee.empname 'Name',tbltimeregister.paycode 'PayCode',tblcompany.companyname 'Company', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "convert(decimal(10,2),sum(tbltimeregister.presentvalue))'Present', " +
                            "convert(decimal(10,2),sum(tbltimeregister.presentvalue)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'PersentPer' , " +
                            "convert(decimal(10,2),sum(tbltimeregister.absentvalue)) 'Absent', " +
                            "convert(decimal(10,2),sum(tbltimeregister.absentvalue)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'AbsentPer' ," +
                            "convert(decimal(10,2),sum(tbltimeregister.leavevalue)) 'Leavevalue'," +
                            "convert(decimal(10,2),sum(tbltimeregister.leavevalue)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'leavePer' ," +
                            "convert(decimal(10,2),sum(tbltimeregister.holiday_value)) 'holiday'," +
                            "convert(decimal(10,2),sum(tbltimeregister.holiday_value)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'holidayPer' ," +
                            "convert(decimal(10,2),sum(tbltimeregister.Wo_value)) 'WO'," +
                            "convert(decimal(10,2),sum(tbltimeregister.Wo_value)/datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',dateadd(dd,1,'" + toDate.ToString("yyyy-MM-dd") + "'))*100 ) 'WoPer' " +
                            "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode " +
                            "join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            "where tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                            "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                            "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tbltimeregister.paycode,tblemployee.presentcardno,tblemployee.empname,tblcompany.companyname " +
                ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("PreP");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("PreA");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("PreWO");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("PreH");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("PreL");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Paycode;
                            dTbl.Rows[0][2] = Global.getEmpInfo._CardNo;
                            dTbl.Rows[0][3] = Global.getEmpInfo._EmpName;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "%";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "%";
                            dTbl.Rows[0][8] = "Weekly Off";
                            dTbl.Rows[0][9] = "%";
                            dTbl.Rows[0][10] = "Holiday";
                            dTbl.Rows[0][11] = "%";
                            dTbl.Rows[0][12] = "Leave";
                            dTbl.Rows[0][13] = "%";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][13].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = "Percentage Analysis - Employee Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EmployeeWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EmployeeWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyEmployeeAttendance_Percentage.aspx");
                    }
                    //Response.Redirect("Rpt_MonthlyEmployeeAttendance_Percentage.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radDeptWiseAttendance_Percentagte.Checked)
            {
                double TotalEmp = 0;
                string DeptCode = "";
                DataSet dsAtt = new DataSet();
                int monthDays = 0;
                strsql = "select tblemployee.departmentcode 'DeptCode',tbldepartment.departmentname 'DeptName',count(tblemployee.paycode) 'PayCode' " +
                        "from tblemployee tblemployee " +
                        "join tbldepartment tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode  " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tblemployee.dateofjoin < convert(datetime,'" + dateTo.ToString() + "',103) and tblemployee.active ='Y' " +
                        " and (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1   ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " group by tblemployee.departmentcode,tbldepartment.departmentname ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (RadExport.SelectedIndex == 0)
                    {
                        try
                        {


                            monthDays = day + 1;
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("DeptCode");
                            dTbl.Columns.Add("DeptName");
                            dTbl.Columns.Add("TotalEmp");
                            dTbl.Columns.Add("Present");
                            dTbl.Columns.Add("PreP");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("PreA");
                            dTbl.Columns.Add("Leave");
                            dTbl.Columns.Add("PreL");
                            dTbl.Columns.Add("WeeklyOff");
                            dTbl.Columns.Add("PreWO");
                            dTbl.Columns.Add("Holiday");
                            dTbl.Columns.Add("PreH");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = Global.getEmpInfo._Dept + " Code";
                            dTbl.Rows[0][2] = Global.getEmpInfo._Dept + " Name";
                            dTbl.Rows[0][3] = "Total " + Global.getEmpInfo._Msg;
                            dTbl.Rows[0][4] = "Present";
                            dTbl.Rows[0][5] = "%";
                            dTbl.Rows[0][6] = "Absent";
                            dTbl.Rows[0][7] = "%";
                            dTbl.Rows[0][8] = "Leave";
                            dTbl.Rows[0][9] = "%";
                            dTbl.Rows[0][10] = "Weekly Off";
                            dTbl.Rows[0][11] = "%";
                            dTbl.Rows[0][12] = "Holiday";
                            dTbl.Rows[0][13] = "%";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    TotalEmp = Convert.ToDouble(ds.Tables[0].Rows[i1][2].ToString());

                                    DeptCode = ds.Tables[0].Rows[i1][0].ToString();
                                    strsql = "select sum(presentValue),Sum(AbsentValue),Sum(leavevalue),Sum(Wo_value),sum(holiday_value) from tbltimeregister where paycode in (select paycode from tblemployee where  departmentcode='" + DeptCode.ToString() + "') and dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and  '" + toDate.ToString("yyyy-MM-dd") + "' ";
                                    dsAtt = con.FillDataSet(strsql);
                                    if (dsAtt.Tables[0].Rows.Count > 0)
                                    {
                                        dTbl.Rows[ct][4] = dsAtt.Tables[0].Rows[0][0].ToString();
                                        dTbl.Rows[ct][5] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][0].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][6] = dsAtt.Tables[0].Rows[0][1].ToString();
                                        dTbl.Rows[ct][7] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][1].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][8] = dsAtt.Tables[0].Rows[0][2].ToString();
                                        dTbl.Rows[ct][9] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][2].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][10] = dsAtt.Tables[0].Rows[0][3].ToString();
                                        dTbl.Rows[ct][11] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][3].ToString()) * 100) / (TotalEmp * monthDays), 2);

                                        dTbl.Rows[ct][12] = dsAtt.Tables[0].Rows[0][4].ToString();
                                        dTbl.Rows[ct][13] = Math.Round((Convert.ToDouble(dsAtt.Tables[0].Rows[0][4].ToString()) * 100) / (TotalEmp * monthDays), 2);
                                    }
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            msg = "Percentage Analysis - Department Wise from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=DepartmentWiseAttendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/DepartmentWiseAttendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyDept_Percentage.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radPerformanceRegister.Checked)
            {
                string Tmp = "";
                int x, i;
                DataSet drdetails = new DataSet();
                int p = 1;
                string inout = "";
                double totalLeaves = 0.0;
                double Leave = 0.0;
                double totalHD = 0.0;
                double HD = 0.0;
                double totalCoff = 0.0;
                double Coff = 0.0;
                double Coff1 = 0.0;
                double totalLWP = 0.0;
                double LWP = 0.0;
                double TotalDays = 0.0;
                double totalWO_Hld = 0.0;
                double totalWD = 0.0;
                double halfdayleave = 0.0;
                double fulldayleave = 0.0;
                double bdayleave = 0.0;
                double totalbdayleave = 0.0;
                double Wdays = 0.0;
                double TotalWdays = 0.0;
                int dd = 0;
                int kp = 0;
                int pp = 0;
                int f = 0;
                bool isEmpty = true;
                DataSet dsday = new DataSet();
                DataSet ds = new DataSet();
                System.Data.DataTable dTbl = new System.Data.DataTable();
                DataSet dsEmp = new DataSet();
                DataRow dRow;
                string field1 = "", field2 = "", tablename = "";
                DateTime dt = System.DateTime.MinValue;
                DateTime doj = System.DateTime.MinValue;
                DateTime startdt = System.DateTime.MinValue;
                DataSet dsResult = new DataSet();
                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        dt = FromDate;
                        startdt = FromDate;
                        dTbl.Columns.Add("PayCode");
                        while (dt <= toDate)
                        {
                            dTbl.Columns.Add(dt.Day.ToString("00"));
                            dt = dt.AddDays(1);
                        }
                        dt = FromDate;
                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select paycode, empname,departmentname,PRESENTCARDNO,convert(varchar(10),dateofjoin,103) 'doj'  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  AND dateofjoin <='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' and (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by departmentname, paycode ";
                        }
                        else
                        {
                            strsql = "Select tblemployee.paycode, tblemployee.empname,tblemployee.PRESENTCARDNO,d.departmentname,convert(varchar(10),dateofjoin,103) 'doj'  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin<='" + FromDate.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") + "' And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)  And tblemployee.departmentcode = d.departmentcode   " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                        }

                        //Response.Write(strsql.ToString());
                        //dr = con.Execute_Reader(strsql);
                        dsEmp = con.FillDataSet(strsql);
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[0][0] = ("PayCode");
                        x = 1;
                        i = 0;
                        x = 1;
                        //while (dr.Read())
                        for (int co = 0; co < dsEmp.Tables[0].Rows.Count; co++)
                        {
                            FromDate = startdt;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            Tmp = dsEmp.Tables[0].Rows[co]["Paycode"].ToString().Trim();
                            doj = DateTime.ParseExact(dsEmp.Tables[0].Rows[co]["doj"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            //Open this comment
                            if (doj >= FromDate)
                            {
                                FromDate = doj;
                            }
                            else
                            {
                                FromDate = FromDate;
                            }

                            strsql = "select Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode='" + Tmp.ToString().Trim() + "' and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0 ),  Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode='" + Tmp.ToString().Trim() + "' and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue>0 ),  WO_holiday=(select isnull(sum(tbltimeregister.wo_value),0) from tbltimeregister " +
                                    "where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode in ('" + Tmp.ToString().Trim() + "') )";
                            drdetails = con.FillDataSet(strsql);
                            for (int dd1 = 0; dd1 < drdetails.Tables[0].Rows.Count; dd1++)
                            {
                                totalWD = Convert.ToDouble(drdetails.Tables[0].Rows[dd1]["Presentvalue"]);
                                totalLWP = Convert.ToDouble(drdetails.Tables[0].Rows[dd1]["Absentvalue"]);
                                totalWO_Hld = Convert.ToDouble(drdetails.Tables[0].Rows[dd1]["WO_holiday"]);
                            }

                            //dTbl.Rows[i]["PayCode"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paycode : " + Tmp + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name : " + dsEmp.Tables[0].Rows[co]["Empname"].ToString().Trim() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CardNo : " + dsEmp.Tables[0].Rows[co]["PRESENTCARDNO"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Present : " + totalWD.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Absent : " + totalLWP.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WO : " + totalWO_Hld.ToString();
                            dTbl.Rows[i]["PayCode"] = "Paycode : " + Tmp + "&nbsp;Name : " + dsEmp.Tables[0].Rows[co]["Empname"].ToString().Trim() + "&nbsp;CardNo : " + dsEmp.Tables[0].Rows[co]["PRESENTCARDNO"].ToString() + "Present : " + totalWD.ToString() + "&nbsp;Absent : " + totalLWP.ToString() + "&nbsp;WO : " + totalWO_Hld.ToString();

                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            p = x;
                            x = 1;
                            i++;
                            {
                                strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                             " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.shiftattended 'ShiftAtt',tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                            " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                            " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',convert(varchar(5),dateadd(minute,tblTimeRegister.Latearrival,0),108) 'Late',convert(varchar(5),dateadd(minute,tblTimeRegister.otduration,0),108) 'OT',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') and dateoffice>=dateofjoin";

                                strsql1 = "";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 1][x - 1] = "In";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 2][x - 1] = "Out";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 3][x - 1] = "Late";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 4][x - 1] = "Hrs Worked";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 5][x - 1] = "Over Time";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 6][x - 1] = "Status";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i + 7][x - 1] = "Shift Att";
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                //dr1 = con.Execute_Reader(strsql);
                                dsResult = con.FillDataSet(strsql);
                                f = x;
                                while (dt <= toDate)
                                {
                                    dTbl.Rows[i][x] = dt.Day.ToString("00");
                                    dt = dt.AddDays(1);
                                    x++;
                                }
                                x = f;
                                kp = i;

                                strsql1 = "select distinct(datediff(dd,'" + startdt.ToString("yyyy-MM-dd") + "',e.dateofjoin)) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "' and  dateofjoin<'" + toDate.ToString("yyyy-MM-dd") + "'";
                                ds = new DataSet();
                                ds = con.FillDataSet(strsql1);
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                    if (dd > 0)
                                    {
                                        x = 1 + dd;
                                    }
                                }
                                //while (dr1.Read())
                                for (int cm = 0; cm < dsResult.Tables[0].Rows.Count; cm++)
                                {
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["in1"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["out2"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["Late"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["Thours"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["OT"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["status"].ToString().Trim();
                                    i++;
                                    dTbl.Rows[i][x] = dsResult.Tables[0].Rows[cm]["ShiftAtt"].ToString().Trim();
                                    i++;
                                    x++;
                                    pp = i;
                                    i = kp;
                                }
                            }
                            i = pp + 1;
                            x = p;
                        }
                        //dr.Close();
                        //for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        //{ 
                        //    isEmpty = true;
                        //    for (int j = 0; j < dTbl.Columns.Count; j++)
                        //    {
                        //        if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                        //        {
                        //            isEmpty = false;
                        //            break;
                        //        }
                        //    }
                        //    if (isEmpty == true)
                        //    {
                        //        dTbl.Rows.RemoveAt(i2);
                        //        i2--;
                        //    }                                
                        //}

                        /*for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (dTbl.Rows[i2][j].ToString().ToUpper().Contains("PAYCODE"))
                                {
                                    DataRow dr = dTbl.NewRow();
                                    dr["Paycode"] = dTbl.Rows[i2][j].ToString();
                                    dTbl.Rows.Add(dr);
                                }
                            }
                        }*/


                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {
                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {

                                    isEmpty = false;
                                    break;
                                }
                            }
                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }




                        /*if (dTbl.Rows.Count > 0)
                        {        
                            GridView1.DataSource = dTbl;
                            GridView1.DataBind();
                            WriteXL();

                        }
                        return;*/


                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int totalcol = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Performance Register from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1'>");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: right;padding-right:25px;'>Run Date and Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: center'> " + msg.ToString() + " </td></tr> ");
                        sb.Append("<tr><td colspan='" + totalcol + "' style='text-align: left'></td></tr> ");
                        string[] fs;
                        string first = "";
                        string second = "";
                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Tmp.Contains("Name"))
                                {
                                    sb.Append("<td colspan='" + totalcol + "'  style='text-align: left;font-size:13px;font-weight:bold;padding-left:15px'>" + Tmp + "</td>");
                                    break;
                                }
                                else
                                {
                                    if (i >= 4)
                                        sb.Append("<td style='text-align: left;font-weight:normal;font-size:12px'>" + Tmp + "</td>");
                                    else
                                        sb.Append("<td style='text-align: left;font-weight:normal;font-size:12px'>" + Tmp + "</td>");
                                }
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        //Response.Write(tbl.ToString());

                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=PerformanceRegister.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/PerformanceRegister.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        //HttpContext.Current.ApplicationInstance.CompleteRequest();
                        HttpContext.Current.Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    strsql = "select tblemployee.paycode 'paycode',tblemployee.empname 'empname',tblemployee.presentcardno 'presentcardno', " +
                        "tblcompany.companyname 'Company', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "sum(tbltimeregister.presentvalue) 'Present', " +
                        "Case When sum(tbltimeregister.hoursworked)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.hoursworked)/60) Else Convert(nvarchar(10),sum(tbltimeregister.hoursworked)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.hoursworked)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.hoursworked)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.hoursworked)%60) End 'TotalHours' , " +
                        //"Cast(sum(tbltimeregister.hoursworked) / 60 as Varchar) +':' +Cast(sum(tbltimeregister.hoursworked % 60) as Varchar)'TotalHours', " +
                        "Case When sum(tbltimeregister.otduration)/60 = 1 Then Convert(nvarchar(10),sum(tbltimeregister.otduration)/60) Else Convert(nvarchar(10),sum(tbltimeregister.otduration)/60)  End + ':' + " +
                        "Case When sum(tbltimeregister.otduration)%60 >= 10 Then Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) Else '0'+Convert(nvarchar(2),sum(tbltimeregister.otduration)%60) End 'OT' , " +
                        //"Cast(sum(tbltimeregister.otduration) / 60 as Varchar) +':' +Cast(sum(tbltimeregister.otduration % 60) as Varchar) 'OT', "+
                        "sum(tbltimeregister.absentvalue) 'absent', " +
                        "sum(tbltimeregister.holiday_value)'holiday'," +
                        "sum(tbltimeregister.wo_value)'wo',sum(tbltimeregister.otamount)'otamount' " +
                        "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join " +
                        "tblcompany on tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tblemployee.active='Y' and dateoffice between " +
                        " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ";
                    if (Selection.ToString().Trim() != "")
                    {
                        strsql += Selection.ToString();
                    }
                    strsql += " and (tblemployee.LeavingDate>='" + toDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " +
                    "group by tblemployee.paycode,tblemployee.empname,tblemployee.presentcardno,tblcompany.companyname " +
                    ",tblemployee.empname,tblemployee.paycode,tblcompany.companyname,tbldepartment.DEPARTMENTCODE,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno " +
                    "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";

                    ds = con.FillDataSet(strsql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyPerformanceRegister.aspx");
                        //Response.Redirect("Rpt_MonthlyPerformanceRegister.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                        return;
                    }
                }
            }
            else if (radMusterRoll.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    DateTime dtnew = System.DateTime.MinValue;
                    dtnew = FromDate;
                    DateTime dtPrint = System.DateTime.MinValue;
                    dtPrint = FromDate;
                    try
                    {
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("Code");
                        // dTbl.Columns.Add("SBU");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Area");
                        dTbl.Columns.Add("Location");

                        //DataSet ds = new DataSet();
                        //strsql = "select datepart(dd,dateoffice) 'dy' from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode=(select top 1 paycode from tbltimeregister where dateoffice < '" + FromDate.ToString("yyyy-MM-dd") + "') ";
                        //ds = con.FillDataSet(strsql);
                        //for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        //{
                        //    dTbl.Columns.Add(ds.Tables[0].Rows[i1]["dy"].ToString());
                        //}

                        while (dtnew <= toDate)
                        {
                            dTbl.Columns.Add(dtnew.ToString("dd"));
                            dtnew = dtnew.AddDays(1);
                        }


                        /*dTbl.Columns.Add("HOD");                        
                        dTbl.Columns.Add("HLD");
                        dTbl.Columns.Add("CL");
                        dTbl.Columns.Add("EL");
                        dTbl.Columns.Add("ML");*/
                        dTbl.Columns.Add("Leave");
                        dTbl.Columns.Add("DW");
                        dTbl.Columns.Add("ABS");
                        dTbl.Columns.Add("POW");
                        dTbl.Columns.Add("WO");
                        dTbl.Columns.Add("Total");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["Code"] ="PayCode";
                        //dTbl.Rows[i]["SBU"] = "Catagory";
                        dTbl.Rows[i]["Name"] = "Name";
                        dTbl.Rows[i]["Area"] = "Department";
                        dTbl.Rows[i]["Location"] = "Company";

                        dtnew = FromDate;
                        string colname = "";
                        while (dtnew <= toDate)
                        {
                            colname = dtnew.ToString("dd");
                            dTbl.Rows[i][colname] = dtnew.ToString("dd");
                            dtnew = dtnew.AddDays(1);
                        }


                        /*for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        {
                            dTbl.Rows[i][ds.Tables[0].Rows[i1]["dy"].ToString()] = ds.Tables[0].Rows[i1]["dy"].ToString();
                        }*/
                        /*dTbl.Rows[i]["HOD"] = "HOD";                        
                        dTbl.Rows[i]["HLD"] = "HLD";
                        dTbl.Rows[i]["CL"] = "CL";
                        dTbl.Rows[i]["EL"] = "EL";
                        dTbl.Rows[i]["ML"] = "ML";
                        dTbl.Rows[i]["PL"] = "PL";*/
                        dTbl.Rows[i]["Leave"] = "Leave";
                        dTbl.Rows[i]["DW"] = "PRE";
                        dTbl.Rows[i]["ABS"] = "ABS";
                        dTbl.Rows[i]["POW"] = "POW";
                        dTbl.Rows[i]["WO"] = "WO";
                        dTbl.Rows[i]["Total"] = "Total";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                     " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                    " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        else
                        {
                            strsql = "Select tblemployee.paycode,tblemployee.CompanyCode,tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                     " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                    " where Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }


                        //and tblemployee.paycode='1389' 
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);
                        if (dsMuster.Tables[0].Rows.Count == 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        i = 1;
                        x = 1;
                        int p = 1;
                        string inout = "";
                        double totalLeaves = 0.0;
                        double Leave = 0.0;
                        double totalHD = 0.0;
                        double HD = 0.0;
                        double totalCoff = 0.0;
                        double Coff = 0.0;
                        double Coff1 = 0.0;
                        double totalLWP = 0.0;
                        double LWP = 0.0;
                        double TotalDays = 0.0;
                        double totalWO_Hld = 0.0;
                        double totalWD = 0.0;
                        double halfdayleave = 0.0;
                        double fulldayleave = 0.0;
                        double bdayleave = 0.0;
                        double totalbdayleave = 0.0;
                        double Wdays = 0.0;
                        double TotalWdays = 0.0;
                        DataSet dsStatus;
                        DateTime dRoster = System.DateTime.MinValue;
                        DateTime djoin = System.DateTime.MinValue;
                        int dd = 0;

                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                dTbl.Rows[i]["Code"] = Tmp;  //dr["Paycode"].ToString().Trim();
                                //dTbl.Rows[i]["SBU"] = dsMuster.Tables[0].Rows[ms]["CatagoryName"].ToString().Trim();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();
                                dTbl.Rows[i]["Area"] = dsMuster.Tables[0].Rows[ms]["departmentname"].ToString().Trim();
                                dTbl.Rows[i]["Location"] = dsMuster.Tables[0].Rows[ms]["CompanyCode"].ToString().Trim();
                                x = 5;
                                {
                                    /* strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                 " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                                 " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ) , " +
                                                 " CL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='CL' ),  " +
                                                 " EL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='EL' ),  " +
                                                 " ML=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='ML' ),  " +
                                                 " PL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='PL' ),  " +
                                                 " Rest=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode not in ('CL','EL','ML','PL') and leavetype='L') ,  " +
                                                 " LWP=(select isnull(Sum(tbltimeregister.AbsentValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ),  " +
                                                 " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                                 " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                                 " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";*/

                                    dRoster = System.DateTime.MinValue;
                                    djoin = System.DateTime.MinValue;
                                    FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    DateTime newFromDate = System.DateTime.MinValue;
                                    newFromDate = FromDate;

                                    strsql1 = "select convert(varchar(10),min(dateoffice),103),convert(varchar(10),e.dateofjoin,103) from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString().Trim() + "' group by dateofjoin";
                                    ds = new DataSet();
                                    dsStatus = new DataSet();
                                    ds = con.FillDataSet(strsql1);
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        try
                                        {
                                            dRoster = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            djoin = DateTime.ParseExact(ds.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        catch
                                        {
                                            continue;
                                        }

                                        if (newFromDate <= djoin)
                                        {
                                            if (newFromDate > djoin)
                                            {
                                                FromDate = newFromDate;
                                                strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + dRoster.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                            else if (newFromDate < djoin)
                                            {
                                                FromDate = djoin;
                                                if (djoin < newFromDate)
                                                {
                                                    FromDate = newFromDate;
                                                }
                                                strsql1 = "select distinct(datediff(dd,'" + newFromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                            else if (newFromDate == djoin)
                                            {
                                                //FromDate = dRoster;
                                                strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "','" + djoin.ToString("yyyy-MM-dd") + "')) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                                ds = con.FillDataSet(strsql1);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                    if (dd > 0)
                                                    {
                                                        x = x + dd;
                                                    }
                                                }
                                            }
                                        }
                                    }





                                    strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                               " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                               " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ) , " +
                                               " Leave =(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and leavetype = 'L' ),  " +
                                        /*" EL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='EL' ),  " +
                                        " ML=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='ML' ),  " +
                                        " PL=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='PL' ),  " +
                                        " Rest=(select isnull(Sum(tbltimeregister.LeaveValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode not in ('CL','EL','ML','PL') and leavetype='L') ,  " +*/
                                               " LWP=(select isnull(Sum(tbltimeregister.AbsentValue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ),  " +
                                               " POW=(select isnull(Sum(tbltimeregister.Presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Status = 'POW' ),  " +
                                               " datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Status  " +
                                               " from tblTimeRegister join tblemployee on tbltimeregister.paycode=tblemployee.paycode where tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";

                                    // Response.Write(strsql.ToString());

                                    dd = 0;
                                    strsql1 = "";


                                    dsStatus = con.FillDataSet(strsql);
                                    if (dsStatus.Tables[0].Rows.Count > 0)
                                    {
                                        for (int st = 0; st < dsStatus.Tables[0].Rows.Count; st++)
                                        {
                                            dTbl.Rows[i][x] = dsStatus.Tables[0].Rows[st]["status"].ToString().Trim();
                                            x++;
                                            //if (x > colCount)
                                            //{
                                            //    continue;
                                            //}
                                        }

                                        if (dsStatus.Tables[0].Rows.Count > 0)
                                        {
                                            totalWD = Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Presentvalue"].ToString().Trim());
                                            dTbl.Rows[i]["DW"] = totalWD.ToString();
                                            /*dTbl.Rows[i]["HLD"] = dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim().ToString();
                                            dTbl.Rows[i]["CL"] = dsStatus.Tables[0].Rows[0]["CL"].ToString().Trim();
                                            dTbl.Rows[i]["EL"] = dsStatus.Tables[0].Rows[0]["EL"].ToString().Trim();
                                            dTbl.Rows[i]["ML"] = dsStatus.Tables[0].Rows[0]["ML"].ToString().Trim();
                                            dTbl.Rows[i]["PL"] = dsStatus.Tables[0].Rows[0]["PL"].ToString().Trim();*/
                                            dTbl.Rows[i]["Leave"] = dsStatus.Tables[0].Rows[0]["Leave"].ToString().Trim();
                                            dTbl.Rows[i]["WO"] = dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim().ToString();
                                            dTbl.Rows[i]["ABS"] = dsStatus.Tables[0].Rows[0]["LWP"].ToString().Trim();
                                            dTbl.Rows[i]["POW"] = dsStatus.Tables[0].Rows[0]["POW"].ToString().Trim();
                                            TotalDays = Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Presentvalue"].ToString().Trim()) + Convert.ToDouble(dsStatus.Tables[0].Rows[0]["WO_full"].ToString().Trim()) + Convert.ToDouble(dsStatus.Tables[0].Rows[0]["Leave"].ToString().Trim());
                                            dTbl.Rows[i]["Total"] = TotalDays.ToString().Trim();
                                        }
                                    }
                                    //dTbl.Rows[i]["HOD"] = dsMuster.Tables[0].Rows[ms]["HOD"].ToString().Trim();
                                }
                                i++;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int widthset = 0;
                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Muster Roll for the month of " + dtPrint.ToString("MMMM") + " " + dtPrint.Year.ToString("0000") + " from " + dtPrint.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if ((i == 1) || (i == 2) || (i == 3) || ((i == 4)))
                                {
                                    if (i == 1)
                                        widthset = 75;
                                    else if (i == 2)
                                        widthset = 160;
                                    else if (i == 3)
                                        widthset = 140;
                                    else
                                        widthset = 50;

                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:" + widthset + "px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 8)
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:40px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=MusterRoll.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/MusterRoll.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    datefrom = txtDateFrom.Text.ToString().Trim();
                    dateTo = txtToDate.Text.ToString().Trim();
                    strsql = " select sno='',absentvalue=(select sum(tbltimeregister.absentvalue)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                          " Presentvalue=(select sum(tbltimeregister.presentvalue)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                          " wo_value=(select sum(tbltimeregister.wo_value) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                          " leavevalue=(select sum(tbltimeregister.leavevalue)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode), " +
                          " total=(select sum(tbltimeregister.absentvalue)+sum(tbltimeregister.leavevalue)+sum(tbltimeregister.presentvalue)+sum(tbltimeregister.wo_value)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                          " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, " +
                          " tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,  " +
                          " tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, " +
                          " tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, " +
                          " tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status," +
                          " tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, " +
                          " tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode, " +
                          " tblTimeRegister.in1,tblTimeRegister.out2,tblTimeRegister.lunchstarttime,tblTimeRegister.LeaveAmount " +
                          ",tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision,  " +
                          " tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And  " +
                          " tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode " +
                          " And tblTimeRegister.DateOffice Between  " +
                          " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and " +
                          " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode " +
                          " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                    if (Selection.ToString().Trim() != "")
                    {
                        strsql += Selection.ToString();
                    }
                    strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                    ds = con.FillDataSet(strsql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                        for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                        {

                            if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                            {
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                            else
                            {
                                n++;
                                Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                                ds.Tables[0].Rows[i]["sno"] = n.ToString();
                            }
                        }
                        Session["DateFrom"] = datefrom.ToString();
                        Session["DateTo"] = dateTo.ToString();
                        Session["NewDataSet"] = ds;
                        OpenChilePage("Reports/MonthlyReports/Rpt_MusterRollForm.aspx");


                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                        return;
                    }
                }
            }
            else if (radLogReport.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    try
                    {
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Presentcardno");
                        dTbl.Columns.Add("Field");
                        dTbl.Columns.Add("Before");
                        dTbl.Columns.Add("New");
                        dTbl.Columns.Add("UserID");
                        dTbl.Columns.Add("LogDate");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["PayCode"] = Global.getEmpInfo._Paycode;
                        dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                        dTbl.Rows[i]["Presentcardno"] = Global.getEmpInfo._CardNo;
                        dTbl.Rows[i]["Field"] = "Field";
                        dTbl.Rows[i]["Before"] = "Before";
                        dTbl.Rows[i]["New"] = "New";
                        dTbl.Rows[i]["UserID"] = "Logged User";
                        dTbl.Rows[i]["LogDate"] = "Log Date";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "select tblemployee.paycode,tblemployee.EmpName,tblemployee.Presentcardno,tbllog1.mtable,tbllog1.before,tbllog1.new,tbllog1.userid, " +
                                " convert(varchar(10),tbllog1.logdate,103) 'LogDate' from tblemployee tblemployee join tbllog1 tbllog1 on tblemployee.paycode=tbllog1.paycode " +
                                " where tblemployee.active='Y' And tbllog1.logdate Between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                        }
                        else
                        {
                            strsql = "select tblemployee.paycode,tblemployee.EmpName,tblemployee.Presentcardno,tbllog1.mtable,tbllog1.before,tbllog1.new,tbllog1.userid, " +
                                " convert(varchar(10),tbllog1.logdate,103) 'LogDate' from tblemployee tblemployee join tbllog1 tbllog1 on tblemployee.paycode=tbllog1.paycode " +
                                " where tblemployee.active='Y' And tbllog1.logdate Between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        //and tblemployee.paycode='1389' 
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);

                        i = 1;
                        x = 1;
                        int p = 1;

                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                dTbl.Rows[i]["PayCode"] = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["EmpName"].ToString().Trim();
                                dTbl.Rows[i]["Presentcardno"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();
                                dTbl.Rows[i]["Field"] = dsMuster.Tables[0].Rows[ms]["mtable"].ToString().Trim();
                                dTbl.Rows[i]["Before"] = dsMuster.Tables[0].Rows[ms]["Before"].ToString().Trim();
                                dTbl.Rows[i]["New"] = dsMuster.Tables[0].Rows[ms]["New"].ToString().Trim();
                                dTbl.Rows[i]["UserID"] = dsMuster.Tables[0].Rows[ms]["UserID"].ToString().Trim();
                                dTbl.Rows[i]["LogDate"] = dsMuster.Tables[0].Rows[ms]["LogDate"].ToString().Trim();
                                i = i + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[0][0].ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "LogReport from " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (i == 2)
                                {
                                    sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else if ((i == 3) || (i == 1))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            //Tmp = "'" + Tmp.ToString();
                                            //sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:100px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 4)
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=LogReport.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/LogReport.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report in available in Excel format.');", true);
                    return;
                }
            }
            else if (radManualPunch.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    try
                    {
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Presentcardno");
                        dTbl.Columns.Add("Date");
                        dTbl.Columns.Add("LoggedUser");
                        dTbl.Columns.Add("PunchDate");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["PayCode"] = Global.getEmpInfo._Paycode;
                        dTbl.Rows[i]["Name"] = Global.getEmpInfo._EmpName;
                        dTbl.Rows[i]["Presentcardno"] = Global.getEmpInfo._CardNo;
                        dTbl.Rows[i]["Date"] = "Date";
                        dTbl.Rows[i]["LoggedUser"] = "LoggedUser";
                        dTbl.Rows[i]["PunchDate"] = "PunchDate";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "select tblemployee.paycode,tblemployee.EmpName,tblemployee.Presentcardno,convert(varchar(10),tblLog_ManualPunch.PunchDate,103) 'Dt',convert(varchar(5),tblLog_ManualPunch.PunchDate,108) 'tme',tblLog_ManualPunch.LoggedUser,convert(varchar(10),tblLog_ManualPunch.LoggedDate,103) 'LogDate' from tblemployee tblemployee join tblLog_ManualPunch tblLog_ManualPunch on tblemployee.paycode=tblLog_ManualPunch.paycode  where tblemployee.active='Y' And tblLog_ManualPunch.loggeddate Between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                        }
                        else
                        {
                            strsql = "select tblemployee.paycode,tblemployee.EmpName,tblemployee.Presentcardno,convert(varchar(10),tblLog_ManualPunch.PunchDate,103) 'Dt',convert(varchar(5),tblLog_ManualPunch.PunchDate,108) 'tme',tblLog_ManualPunch.LoggedUser,convert(varchar(10),tblLog_ManualPunch.LoggedDate,103) 'LogDate' from tblemployee tblemployee join tblLog_ManualPunch tblLog_ManualPunch on tblemployee.paycode=tblLog_ManualPunch.paycode  where tblemployee.active='Y' And tblLog_ManualPunch.loggeddate Between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        //and tblemployee.paycode='1389' 
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);

                        i = 1;
                        x = 1;
                        int p = 1;

                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                dTbl.Rows[i]["Sno"] = Convert.ToString(i);
                                dTbl.Rows[i]["PayCode"] = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms]["EmpName"].ToString().Trim();
                                dTbl.Rows[i]["Presentcardno"] = dsMuster.Tables[0].Rows[ms]["Presentcardno"].ToString().Trim();
                                dTbl.Rows[i]["Date"] = dsMuster.Tables[0].Rows[ms]["Dt"].ToString().Trim() + " " + dsMuster.Tables[0].Rows[ms]["tme"].ToString().Trim();
                                dTbl.Rows[i]["LoggedUser"] = dsMuster.Tables[0].Rows[ms]["LoggedUser"].ToString().Trim();
                                dTbl.Rows[i]["PunchDate"] = dsMuster.Tables[0].Rows[ms]["LogDate"].ToString().Trim();
                                i = i + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[0][0].ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Manual Punch Report from " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (i == 2)
                                {
                                    sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else if ((i == 3) || (i == 1))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            //Tmp = "'" + Tmp.ToString();
                                            //sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:100px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 4)
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=ManualPunch.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/ManualPunch.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(Tmp.ToString()+"--"+ex.Message.ToString());
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report in available in Excel format.');", true);
                    return;
                }
            }
            else if (radMonthlyReport.Checked)
            {

                datefrom = txtDateFrom.Text.ToString().Trim();
                dateTo = txtToDate.Text.ToString().Trim();
                strsql = " select sno='',absentvalue=(select sum(tbltimeregister.absentvalue)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                      " Presentvalue=(select sum(tbltimeregister.presentvalue)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                      " wo_value=(select sum(tbltimeregister.wo_value) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                      " leavevalue=(select sum(tbltimeregister.leavevalue)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode), " +
                      " total=(select sum(tbltimeregister.absentvalue)+sum(tbltimeregister.leavevalue)+sum(tbltimeregister.presentvalue)+sum(tbltimeregister.wo_value)  from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode) , " +
                      " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, " +
                      " tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,  " +
                      " tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, " +
                      " tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, " +
                      " tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status," +
                      " tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, " +
                      " tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode, " +
                      " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2,tblTimeRegister.lunchstarttime,tblTimeRegister.LeaveAmount " +
                      ",tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision,  " +
                      " tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And  " +
                      " tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode " +
                      " And tblTimeRegister.DateOffice Between  " +
                      " '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and " +
                      " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode " +
                      " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Pcode = ds.Tables[0].Rows[0]["paycode"].ToString().Trim().ToUpper();
                    for (int i = 0, n = 1; i < ds.Tables[0].Rows.Count; i++)
                    {

                        if (Pcode == ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper())
                        {
                            ds.Tables[0].Rows[i]["sno"] = n.ToString();
                        }
                        else
                        {
                            n++;
                            Pcode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim().ToUpper();
                            ds.Tables[0].Rows[i]["sno"] = n.ToString();
                        }
                    }
                    Session["DateFrom"] = datefrom.ToString();
                    Session["DateTo"] = dateTo.ToString();
                    Session["NewDataSet"] = ds;
                    //OpenChilePage("Reports/MonthlyReports/Rpt_MusterRollForm.aspx");
                    OpenChilePage("Reports/MonthlyReports/Rpt_MusterRollForm_New.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radAttendanceDetails.Checked)
            {

                strsql = "select tblemployee.presentcardno 'Card',tblemployee.empname 'Name',tbltimeregister.paycode 'PayCode',tblcompany.companyname 'Company', " +
                        "tblemployee.paycode 'Pay',tblemployee.presentcardno 'presentcardno',tblemployee.Designation 'Designation',tbldepartment.DEPARTMENTName 'DepartmentName',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                        "Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  and presentvalue>0 ), " +
                        "Halfvalue=(select isnull(count(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  and presentvalue=0.5 ), " +
                        "Leavevalue=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  and Leavetype='L' and leaveamount>0 ), " +
                        "Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  and absentvalue>0 ), " +
                        "coff=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  and upper(Status)='CO' ), " +
                        "convert(decimal(10,2),sum(tbltimeregister.holiday_value))+convert(decimal(10,2),sum(tbltimeregister.Wo_value)) 'WO_holiday' " +
                        "from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode  " +
                        "join tblcompany on tblcompany.companycode=tblemployee.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        "where tbltimeregister.dateoffice between " +
                        "'" + FromDate.ToString("yyyy-MM-dd") + "' and " +
                        "'" + toDate.ToString("yyyy-MM-dd") + "' " +
                        "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " group by tbltimeregister.paycode,tblemployee.presentcardno,tblemployee.empname,tblcompany.companyname, " +
                "tbldepartment.DEPARTMENTName,tblcatagory.CAT,tblDivision.DivisionCode,tblemployee.presentcardno,tblemployee.paycode,tblemployee.Designation " +
                "order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";



                // Response.Write(strsql.ToString());
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Session["DateFrom"] = FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000");
                    Session["DateTo"] = dateTo.ToString();
                    Session["NewDataSet"] = ds;
                    OpenChilePage("Reports/MonthlyReports/Rpt_MonthlyEmployeeAttendance_New.aspx");
                    //Response.Redirect("Rpt_MonthlyEmployeeAttendance.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No record found');", true);
                    return;
                }
            }
            else if (radlvarrear.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    try
                    {
                        int x, i;
                        dTbl = new DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("Code");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("LeaveCode");
                        dTbl.Columns.Add("RequestDate");
                        dTbl.Columns.Add("From");
                        dTbl.Columns.Add("To");
                        dTbl.Columns.Add("Duration");
                        dTbl.Columns.Add("Remarks");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["Code"] = "Code";
                        dTbl.Rows[i]["Name"] = "Name";
                        dTbl.Rows[i]["LeaveCode"] = "LeaveCode";
                        dTbl.Rows[i]["RequestDate"] = "Request Date";
                        dTbl.Rows[i]["From"] = "From";
                        dTbl.Rows[i]["To"] = "To";
                        dTbl.Rows[i]["Duration"] = "Duration";
                        dTbl.Rows[i]["Remarks"] = "Remarks";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);


                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select leave_request.paycode,tblemployee.empname,convert(varchar(10),leave_request.request_date,103),convert(varchar(10),leave_request.leave_from,103), " +
                                    " convert(varchar(10),leave_request.leave_to,103),leave_request.leavecode,halfday=case leave_request.halfday when 'N' then 'Full Day' when 'F' then 'First Half' else 'Second Half' end, " +
                                    " leave_request.userremarks,convert(varchar(10),leave_request.stage3_approval_date,103) " +
                                    " from leave_request leave_request join tblemployee tblemployee on leave_request.paycode=tblemployee.paycode where IsArrear='Y' and Stage1_approved='Y' and Stage2_approved='Y' and " +
                                    " leave_request.leave_from between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                    " and tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        else
                        {
                            strsql = "Select leave_request.paycode,tblemployee.empname,convert(varchar(10),leave_request.request_date,103),convert(varchar(10),leave_request.leave_from,103), " +
                                    " convert(varchar(10),leave_request.leave_to,103),leave_request.leavecode,halfday=case leave_request.halfday when 'N' then 'Full Day' when 'F' then 'First Half' else 'Second Half' end, " +
                                    " leave_request.userremarks,convert(varchar(10),leave_request.stage3_approval_date,103) " +
                                    " from leave_request leave_request join tblemployee tblemployee on leave_request.paycode=tblemployee.paycode where IsArrear='Y' and Stage1_approved='Y' and Stage2_approved='Y' and " +
                                    " leave_request.leave_from between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                    " and tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                        }
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);

                        i = 1;
                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(ms + 1);
                                dTbl.Rows[i]["Code"] = dsMuster.Tables[0].Rows[ms][0].ToString();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms][1].ToString();
                                dTbl.Rows[i]["LeaveCode"] = dsMuster.Tables[0].Rows[ms][5].ToString();
                                dTbl.Rows[i]["RequestDate"] = dsMuster.Tables[0].Rows[ms][2].ToString();
                                dTbl.Rows[i]["From"] = dsMuster.Tables[0].Rows[ms][3].ToString();
                                dTbl.Rows[i]["To"] = dsMuster.Tables[0].Rows[ms][4].ToString();
                                dTbl.Rows[i]["Duration"] = dsMuster.Tables[0].Rows[ms][6].ToString();
                                dTbl.Rows[i]["Remarks"] = dsMuster.Tables[0].Rows[ms][7].ToString();
                                i = i + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Leave Arrears Report from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }

                                if (i == 1)
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:80px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:80px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if ((i == 8) || (i == 2))
                                {
                                    sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:80px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=LeaveArrears.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/LeaveArrears.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }
            else if (radReg_arrear.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    try
                    {
                        int x, i;
                        dTbl = new DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("Code");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("RequestDate");
                        dTbl.Columns.Add("RequestFor");
                        dTbl.Columns.Add("InTime");
                        dTbl.Columns.Add("OutTime");
                        dTbl.Columns.Add("Remarks");
                        dTbl.Columns.Add("ApprovalID");
                        dTbl.Columns.Add("ApprovalDate");


                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["Code"] = "Code";
                        dTbl.Rows[i]["Name"] = "Name";
                        dTbl.Rows[i]["RequestDate"] = "Request Date";
                        dTbl.Rows[i]["RequestFor"] = "Request For";
                        dTbl.Rows[i]["InTime"] = "In Time";
                        dTbl.Rows[i]["OutTime"] = "Out Time";
                        dTbl.Rows[i]["Remarks"] = "Remarks";
                        dTbl.Rows[i]["ApprovalID"] = "Approval ID";
                        dTbl.Rows[i]["ApprovalDate"] = "Approval Date";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);


                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select att_request.paycode,tblemployee.empname,convert(varchar(10),att_request.request_date,103),convert(varchar(10),att_request.request_for,103), " +
                                        " convert(varchar(5),att_request.intime,108),convert(varchar(5),att_request.outtime,108), " +
                                        " att_request.userremarks,stage1_approvalid,convert(varchar(10),att_request.stage1_approvaldate,103)  " +
                                        " from att_request att_request join tblemployee tblemployee on att_request.paycode=tblemployee.paycode " +
                                        " where IsArrear='Y' and Stage1_approval='Y' and Stage2_approval='Y' and " +
                                        " att_request.request_date between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                        " and tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) ";
                        }
                        else
                        {
                            strsql = "Select att_request.paycode,tblemployee.empname,convert(varchar(10),att_request.request_date,103),convert(varchar(10),att_request.request_for,103), " +
                                        " convert(varchar(5),att_request.intime,108),convert(varchar(5),att_request.outtime,108), " +
                                        " att_request.userremarks,stage1_approvalid,convert(varchar(10),att_request.stage1_approvaldate,103)  " +
                                        " from att_request att_request join tblemployee tblemployee on att_request.paycode=tblemployee.paycode " +
                                        " where IsArrear='Y' and Stage1_approval='Y' and Stage2_approval='Y' and " +
                                        " att_request.request_date between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                        " and tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                        }
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);

                        i = 1;
                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(ms + 1);
                                dTbl.Rows[i]["Code"] = dsMuster.Tables[0].Rows[ms][0].ToString();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms][1].ToString();
                                dTbl.Rows[i]["RequestDate"] = dsMuster.Tables[0].Rows[ms][2].ToString();
                                dTbl.Rows[i]["RequestFor"] = dsMuster.Tables[0].Rows[ms][3].ToString();
                                dTbl.Rows[i]["InTime"] = dsMuster.Tables[0].Rows[ms][4].ToString();
                                dTbl.Rows[i]["OutTime"] = dsMuster.Tables[0].Rows[ms][5].ToString();
                                dTbl.Rows[i]["Remarks"] = dsMuster.Tables[0].Rows[ms][6].ToString();
                                dTbl.Rows[i]["ApprovalID"] = dsMuster.Tables[0].Rows[ms][7].ToString();
                                dTbl.Rows[i]["ApprovalDate"] = dsMuster.Tables[0].Rows[ms][8].ToString();
                                i = i + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Regularization Arrears Report from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }

                                if (i == 1)
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:70px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (i == 2)
                                {
                                    sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:80px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=RegularizationArrears.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/RegularizationArrears.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }
            else if (radCoff.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    try
                    {
                        int x, i;
                        dTbl = new DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("Code");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("LeaveCode");
                        dTbl.Columns.Add("RequestDate");
                        dTbl.Columns.Add("From");
                        dTbl.Columns.Add("To");
                        dTbl.Columns.Add("AgainstDate");
                        dTbl.Columns.Add("Duration");
                        dTbl.Columns.Add("Remarks");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["Code"] = "Code";
                        dTbl.Rows[i]["Name"] = "Name";
                        dTbl.Rows[i]["LeaveCode"] = "LeaveCode";
                        dTbl.Rows[i]["RequestDate"] = "Request Date";
                        dTbl.Rows[i]["From"] = "From";
                        dTbl.Rows[i]["To"] = "To";
                        dTbl.Rows[i]["AgainstDate"] = "Against Date";
                        dTbl.Rows[i]["Duration"] = "Duration";
                        dTbl.Rows[i]["Remarks"] = "Remarks";

                        colCount = Convert.ToInt32(dTbl.Columns.Count);


                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select leave_request.paycode,tblemployee.empname,convert(varchar(10),leave_request.request_date,103),convert(varchar(10),leave_request.leave_from,103), " +
                                    " convert(varchar(10),leave_request.leave_to,103),leave_request.leavecode,halfday=case leave_request.halfday when 'N' then 'Full Day' when 'F' then 'First Half' else 'Second Half' end, " +
                                    " leave_request.userremarks,convert(varchar(10),leave_request.stage3_approval_date,103) " +
                                    " from leave_request leave_request join tblemployee tblemployee on leave_request.paycode=tblemployee.paycode where leavecode='CO' and Stage1_approved='Y' and Stage2_approved='Y' and " +
                                    " leave_request.leave_from between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                    " and tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        else
                        {
                            strsql = "Select leave_request.paycode,tblemployee.empname,convert(varchar(10),leave_request.request_date,103),convert(varchar(10),leave_request.leave_from,103), " +
                                    " convert(varchar(10),leave_request.leave_to,103),leave_request.leavecode,halfday=case leave_request.halfday when 'N' then 'Full Day' when 'F' then 'First Half' else 'Second Half' end, " +
                                    " leave_request.userremarks,convert(varchar(10),leave_request.stage3_approval_date,103) " +
                                    " from leave_request leave_request join tblemployee tblemployee on leave_request.paycode=tblemployee.paycode where leavecode='CO' and Stage1_approved='Y' and Stage2_approved='Y' and " +
                                    " leave_request.leave_from between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                    " and tblemployee.Active='Y'  AND dateofjoin <='" + toDate.ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";

                        }
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);

                        i = 1;
                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(ms + 1);
                                dTbl.Rows[i]["Code"] = dsMuster.Tables[0].Rows[ms][0].ToString();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms][1].ToString();
                                dTbl.Rows[i]["LeaveCode"] = dsMuster.Tables[0].Rows[ms][5].ToString();
                                dTbl.Rows[i]["RequestDate"] = dsMuster.Tables[0].Rows[ms][2].ToString();
                                dTbl.Rows[i]["From"] = dsMuster.Tables[0].Rows[ms][3].ToString();
                                dTbl.Rows[i]["To"] = dsMuster.Tables[0].Rows[ms][4].ToString();
                                dTbl.Rows[i]["AgainstDate"] = dsMuster.Tables[0].Rows[ms][8].ToString();
                                dTbl.Rows[i]["Duration"] = dsMuster.Tables[0].Rows[ms][6].ToString();
                                dTbl.Rows[i]["Remarks"] = dsMuster.Tables[0].Rows[ms][7].ToString();
                                i = i + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Comp Off Report from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }

                                if (i == 1)
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:100px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (i == 9)
                                {
                                    sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:80px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=CompOff.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/CompOff.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }
            else if (radStatusChange.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int colCount = 0;
                    try
                    {
                        int x, i;
                        dTbl = new DataTable();
                        dTbl.Columns.Add("Sno");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("First");
                        dTbl.Columns.Add("Second");
                        dTbl.Columns.Add("Modifiedby");
                        dTbl.Columns.Add("Modifiedon");

                        DataRow dRow;
                        i = 0;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[i]["SNo"] = "SNo";
                        dTbl.Rows[i]["PayCode"] = "Code";
                        dTbl.Rows[i]["Name"] = "Name";
                        dTbl.Rows[i]["First"] = "Initial Status";
                        dTbl.Rows[i]["Second"] = "Changed Status";
                        dTbl.Rows[i]["Modifiedby"] = "Modified By";
                        dTbl.Rows[i]["Modifiedon"] = "Modified On";


                        colCount = Convert.ToInt32(dTbl.Columns.Count);


                        if (string.IsNullOrEmpty(Selection.ToString()))
                        {
                            strsql = "Select tblEmployee_backup.paycode,tblemployee.empname,convert(varchar(10),tblEmployee_backup.modifiedon,103), " +
                                    " tblEmployee_backup.Modifiedby,tblEmployee_backup.FirstStatus,tblEmployee_backup.SecondStatus " +
                                    " from tblEmployee_backup tblEmployee_backup join tblemployee tblemployee on tblEmployee_backup.paycode=tblemployee.paycode where  " +
                                    " tblEmployee_backup.Modifiendon between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                    " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        else
                        {
                            strsql = "Select tblEmployee_backup.paycode,tblemployee.empname,convert(varchar(10),tblEmployee_backup.modifiedon,103), " +
                                    " tblEmployee_backup.Modifiedby,First=case tblEmployee_backup.FirstStatus when 'Y' then 'Active' else 'InActive' End ,Second=case tblEmployee_backup.SecondStatus when 'Y' then 'Active' else 'InActive' end " +
                                    " from tblEmployee_backup tblEmployee_backup join tblemployee tblemployee on tblEmployee_backup.paycode=tblemployee.paycode where  " +
                                    " tblEmployee_backup.Modifiedon between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' " +
                                    "  " + Selection.ToString() + " order by " + ddlSorting.SelectedItem.Value.ToString() + " ";
                        }
                        DataSet dsMuster = new DataSet();
                        dsMuster = con.FillDataSet(strsql);

                        i = 1;
                        if (dsMuster.Tables[0].Rows.Count > 0)
                        {
                            for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                            {
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);

                                dTbl.Rows[i]["Sno"] = Convert.ToString(ms + 1);
                                dTbl.Rows[i]["PayCode"] = dsMuster.Tables[0].Rows[ms][0].ToString();
                                dTbl.Rows[i]["Name"] = dsMuster.Tables[0].Rows[ms][1].ToString();
                                dTbl.Rows[i]["First"] = dsMuster.Tables[0].Rows[ms][4].ToString();
                                dTbl.Rows[i]["Second"] = dsMuster.Tables[0].Rows[ms][5].ToString();
                                dTbl.Rows[i]["Modifiedby"] = dsMuster.Tables[0].Rows[ms][3].ToString();
                                dTbl.Rows[i]["Modifiedon"] = dsMuster.Tables[0].Rows[ms][2].ToString();
                                i = i + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int colcount = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Status Change Report from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }

                                if (i == 1)
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:100px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (i == 2)
                                {
                                    sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:80px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=StatusChange.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/StatusChange.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }


           /* else if (radAttendance1.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    //try
                    //{
                    string Tmp = "";
                    int x, i;
                    DataRow dRow;
                    string str = "";
                    //// create table 
                    DateTime curdate = System.DateTime.MinValue;
                    DataSet dr4 = new DataSet();

                    System.Data.DataTable dTbl = new System.Data.DataTable();
                    dTbl.Columns.Add("Catagory");
                    dTbl.Columns.Add("Sno");
                    dTbl.Columns.Add("PayCode");
                    dTbl.Columns.Add("CardNo");
                    dTbl.Columns.Add("Name");


                    DataSet ds = new DataSet();
                    strsql = "select datename(dw,dateoffice)+' '+ CONVERT(VARCHAR(11), dateoffice, 100) 'dy1',datepart(dd,dateoffice) 'dy' from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode=(select top 1 paycode from tbltimeregister where hoursworked>0) ";
                    ds = con.FillDataSet(strsql);
                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Columns.Add(ds.Tables[0].Rows[i1]["dy1"].ToString());
                    }

                    dTbl.Columns.Add("WorkHr.");
                    dTbl.Columns.Add("AvgHr.");
                    dTbl.Columns.Add("Present");
                    dTbl.Columns.Add("Absent");
                    dTbl.Columns.Add("Holidays");
                    dTbl.Columns.Add("Leave");

                    x = 5;
                    //for (i = 1; i <= 31; i++)
                    //    dTbl.Rows[0][x++] = (i.ToString());
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Rows[0][x++] = ds.Tables[0].Rows[i1]["dy1"].ToString();
                    }
                    for (int i2 = 0; i2 < 6; i2++)
                    {
                        dTbl.Rows[0][x++] = "Performance";
                    }
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    dTbl.Rows[1][0] = ("Catagory");
                    dTbl.Rows[1][1] = ("Sno");
                    dTbl.Rows[1][2] = ("PayCode");
                    dTbl.Rows[1][3] = ("CardNo");
                    dTbl.Rows[1][4] = ("Name");

                    x = 5;
                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Rows[1][x] = "In1-Out1-In2-Out2-Work-Status";
                        x = x + 1;
                    }

                    dTbl.Rows[1][x] = ("WorkHr.");
                    dTbl.Rows[1][x + 1] = ("AvgHr.");
                    dTbl.Rows[1][x + 2] = ("Present");
                    dTbl.Rows[1][x + 3] = ("Absent");
                    dTbl.Rows[1][x + 4] = ("Holidays");
                    dTbl.Rows[1][x + 5] = ("Leave");


                    DataSet dscat = new DataSet();
                    string strcat = "";
                    string catid = "";
                    string catname = "";
                    DataSet dsfill = new DataSet();

                    i = 2;
                    x = 1;
                    int p = 1;
                    strcat = "select cat,catagoryname from tblcatagory";
                    dscat = con.FillDataSet(strcat);
                    if (dscat.Tables[0].Rows.Count > 0)
                    {
                        for (int ic = 0; ic < dscat.Tables[0].Rows.Count; ic++)
                        {
                            catid = dscat.Tables[0].Rows[ic][0].ToString().Trim();
                            catname = dscat.Tables[0].Rows[ic][0].ToString().Trim() + "--" + dscat.Tables[0].Rows[ic][1].ToString().Trim();



                            if (string.IsNullOrEmpty(Selection.ToString()))
                            {
                                // strsql = "Select paycode,PRESENTCARDNO,empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode and paycode='10029' AND dateofjoin <='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "' And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by departmentname, paycode ";                 
                                strsql = "Select paycode,PRESENTCARDNO,empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode AND dateofjoin <='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "' And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) and cat='" + catid.ToString() + "' order by departmentname, paycode ";
                            }
                            else
                            {
                                strsql = "Select tblemployee.paycode, tblemployee.empname,d.departmentname  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin<='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "' And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)  And tblemployee.departmentcode = d.departmentcode   " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                            }

                            //Response.Write(strsql.ToString());


                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);


                            OleDbDataReader dr;
                            OleDbDataReader dr1;
                            string pdf = "";

                            string inout = "";
                            double totalLeaves = 0.0;
                            double Leave = 0.0;
                            double totalHD = 0.0;
                            double HD = 0.0;
                            double totalCoff = 0.0;
                            double Coff = 0.0;
                            double Coff1 = 0.0;
                            double totalLWP = 0.0;
                            double LWP = 0.0;
                            double TotalDays = 0.0;
                            double totalWO_Hld = 0.0;
                            double totalWD = 0.0;
                            double halfdayleave = 0.0;
                            double fulldayleave = 0.0;
                            double AveHr = 0.0;
                            double totalleaves = 0.0;
                            double Wdays = 0.0;
                            double TotalWdays = 0.0;


                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            dTbl.Rows[i]["Catagory"] = catname.ToString();
                            i++;

                            //dr = con.Execute_Reader(strsql);
                            //while (dr.Read())
                            dsfill = con.FillDataSet(strsql);
                            if (dsfill.Tables[0].Rows.Count > 0)
                            {
                                for (int p2 = 0; p2 < dsfill.Tables[0].Rows.Count; p2++)
                                {


                                    {

                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);


                                        dTbl.Rows[i]["Sno"] = Convert.ToString(x++);
                                        Tmp = dsfill.Tables[0].Rows[p2]["Paycode"].ToString().Trim();
                                        dTbl.Rows[i]["PayCode"] = Tmp;
                                        dTbl.Rows[i]["Name"] = dsfill.Tables[0].Rows[p2]["Empname"].ToString().Trim();
                                        dTbl.Rows[i]["CardNo"] = dsfill.Tables[0].Rows[p2]["PRESENTCARDNO"].ToString().Trim();
                                        p = x;
                                        x = 5;
                                        {
                                            strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                                        " HrsWorkedMin=(select isnull(sum(tbltimeregister.hoursworked),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ),  " +
                                                        " HrsWorked=(select cast(isnull(sum(tbltimeregister.hoursworked),0)/60 as varchar)+'.'+cast(isnull(sum(tbltimeregister.hoursworked),0)%60 as varchar) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' ),  " +
                                                        " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                                        " Halfvalue=(select isnull(count(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue=0.5 ),  " +
                                                        " FullLeavevalue=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount = 1 ),  " +
                                                        " HalfLeavevalue=(select isnull(count(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount = 0.5 ),  " +
                                                        " Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ), " +
                                                        " coff=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and ( upper(Status)='CO' or upper(Status)='H_CO' ) ) , " +
                                                        " B_day=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and (upper(Status)='BDL' or upper(Status)='H_BDL' ) ) , " +
                                                        " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value =1 ) , " +
                                                        " hld=(select isnull(convert(decimal(10,2),sum(tbltimeregister.holiday_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  ) , " +
                                                        " WO_half=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value = 0.5 ) , " +
                                                        " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.DateOffice 'DateOffice1',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                                        " isnull(convert(varchar(5),tblTimeRegister.in1,108),'NA') in1,isnull(convert(varchar(5),tblTimeRegister.out1,108),'NA') out1,  isnull(convert(varchar(5),tblTimeRegister.in2,108),'NA') in2,isnull(convert(varchar(5),tblTimeRegister.out2,108),'NA') out2, " +
                                                        " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.Status 'TStatus',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";

                                            // Response.Write(strsql);
                                            int dd = 0;
                                            strsql1 = "";
                                            strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',e.dateofjoin)) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                                            ds = new DataSet();
                                            ds = con.FillDataSet(strsql1);
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                                if (dd > 0)
                                                {
                                                    x = x + dd;
                                                }
                                            }
                                            dr1 = con.Execute_Reader(strsql);
                                            while (dr1.Read())
                                            {
                                                string dp = dr1["Dateoffice"].ToString();
                                                pdf = dr1["Dateoffice1"].ToString().Substring(0, 10);
                                                curdate = DateTime.ParseExact(pdf.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                str = "select  officepunch from machinerawpunch where paycode='" + Tmp.ToString() + "' and convert(varchar(10),officepunch,105)='" + curdate.ToString("dd-MM-yyyy") + "'";
                                                dr4 = con.FillDataSet(str);
                                                if (dr4.Tables[0].Rows.Count > 0)
                                                {

                                                    if (dr4.Tables[0].Rows.Count == 1)
                                                    {
                                                        inout = dr4.Tables[0].Rows[0][0].ToString().Trim();
                                                    }
                                                    else if (dr4.Tables[0].Rows.Count == 2)
                                                    {
                                                        inout = dr4.Tables[0].Rows[0][0].ToString().Trim() + "-" + dr4.Tables[0].Rows[1][0].ToString().Trim();
                                                    }
                                                    else if (dr4.Tables[0].Rows.Count == 3)
                                                    {
                                                        inout = dr4.Tables[0].Rows[0][0].ToString().Trim() + "-" + dr4.Tables[0].Rows[1][0].ToString().Trim() + "-" + dr4.Tables[0].Rows[2][0].ToString().Trim();
                                                    }
                                                    else if (dr4.Tables[0].Rows.Count == 4)
                                                    {
                                                        inout = dr4.Tables[0].Rows[0][0].ToString().Trim() + "-" + dr4.Tables[0].Rows[1][0].ToString().Trim() + "-" + dr4.Tables[0].Rows[2][0].ToString().Trim() + "-" + dr4.Tables[0].Rows[3][0].ToString().Trim();
                                                    }

                                                    //if (!string.IsNullOrEmpty(dr4.Tables[0].Rows[m1][0].ToString()))
                                                    //{
                                                    //    inout = dr4.Tables[0].Rows[m1][0].ToString().Trim();
                                                    //}
                                                    //if (!string.IsNullOrEmpty(dr4.Tables[0].Rows[m1][1].ToString()))
                                                    //{
                                                    //    inout = dr4.Tables[0].Rows[m1][0].ToString().Trim() + "" + dr4.Tables[0].Rows[m1][1].ToString().Trim();                                                            
                                                    //}
                                                    //if (!string.IsNullOrEmpty(dr4.Tables[0].Rows[m1][2].ToString()))
                                                    //{
                                                    //    inout = dr4.Tables[0].Rows[m1][2].ToString();
                                                    //}
                                                    //if (!string.IsNullOrEmpty(dr4.Tables[0].Rows[m1][3].ToString()))
                                                    //{
                                                    //    inout = dr4.Tables[0].Rows[m1][3].ToString();
                                                    //}
                                                }
                                                //inout = dr4[0].ToString().Trim() + "-" + dr4[1].ToString().Trim() + "-" + dr4[2].ToString().Trim() + "-" + dr4[3].ToString().Trim() + "-" + dr1["Thours"].ToString().Trim() + "-" + dr1["TStatus"].ToString().Trim();

                                                // inout = dr1["in1"].ToString().Trim() + "-" + dr1["out1"].ToString().Trim() + "-" + dr1["in2"].ToString().Trim() + "-" + dr1["out2"].ToString().Trim() + "-" + dr1["Thours"].ToString().Trim() + "-" + dr1["TStatus"].ToString().Trim();
                                                if (inout.Contains(":"))
                                                {
                                                    dTbl.Rows[i][x] = dr1["in1"].ToString().Trim() + "-" + dr1["out1"].ToString().Trim() + "-" + dr1["in2"].ToString().Trim() + "-" + dr1["out2"].ToString().Trim() + "-" + dr1["Thours"].ToString().Trim() + "-" + dr1["TStatus"].ToString().Trim();
                                                }
                                                else
                                                {
                                                    dTbl.Rows[i][x] = dr1["status"].ToString().Trim();
                                                }
                                                dTbl.Rows[i]["WorkHr."] = dr1["HrsWorked"].ToString().Trim();
                                                AveHr = Math.Round(Convert.ToDouble(dr1["HrsWorked"]) / Convert.ToDouble(dr1["Presentvalue"]), 2);
                                                dTbl.Rows[i]["AvgHr."] = AveHr.ToString().Trim();
                                                dTbl.Rows[i]["Present"] = dr1["Presentvalue"].ToString().Trim();
                                                dTbl.Rows[i]["Absent"] = dr1["Absentvalue"].ToString().Trim();
                                                dTbl.Rows[i]["Holidays"] = dr1["hld"].ToString().Trim();

                                                totalleaves = Convert.ToDouble(dr1["fullleavevalue"]) + Convert.ToDouble(dr1["HalfLeavevalue"]);
                                                dTbl.Rows[i]["Leave"] = totalleaves.ToString().Trim();
                                                x++;
                                            }
                                            dr1.Close();
                                        }
                                        i++;
                                        x = p;

                                    }
                                }
                            }

                        }
                    }

                    int colc = dTbl.Columns.Count;

                    string msg = "Attendance details for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1'>");
                    sb.Append("<tr><td colspan='" + colc + "' style='text-align: center'> " + msg.ToString() + " </td></tr> ");
                    sb.Append("<tr><td colspan='" + colc + "' style='text-align: center'></td></tr> ");
                    string[] fs;
                    string first = "";
                    string second = "";
                    string third = "";
                    string forth = "";
                    string fifth = "";
                    string sixth = "";
                    int pcount = 0;

                    x = dTbl.Columns.Count;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                                if (Tmp.Substring(0, 1) == "0")
                                {
                                    if (Tmp.Contains(":"))
                                    {
                                        Tmp = Tmp;
                                    }
                                    else
                                    {
                                        Tmp = Tmp;
                                        //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                    }
                                }
                            }
                            if (i >= 4)
                            {
                                if (Tmp.Contains(":"))
                                {
                                    if (Tmp.Contains("-"))
                                    {
                                        fs = Tmp.Split('-');
                                        first = fs[0].ToString();
                                        second = fs[1].ToString();
                                        third = fs[2].ToString();
                                        forth = fs[3].ToString();
                                        fifth = fs[4].ToString();
                                        sixth = fs[5].ToString();

                                        sb.Append("<td>");
                                        sb.Append("<table border='0' >");
                                        sb.Append("<tr><td style='text-align: center;border-right:solid 1px black'>" + first.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + second.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + third.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + forth.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + fifth.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + sixth.ToString() + "</td></tr> ");
                                        sb.Append("</table>");
                                        sb.Append("</td>");
                                    }
                                }
                                else if (Tmp == "In1-Out1-In2-Out2-Work-Status")
                                {
                                    fs = Tmp.Split('-');
                                    first = fs[0].ToString();
                                    second = fs[1].ToString();
                                    third = fs[2].ToString();
                                    forth = fs[3].ToString();
                                    fifth = fs[4].ToString();
                                    sixth = fs[5].ToString();

                                    sb.Append("<td>");
                                    sb.Append("<table border='0' >");
                                    sb.Append("<tr><td style='text-align: center;border-right:solid 1px black'>" + first.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + second.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + third.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + forth.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + fifth.ToString() + "</td><td style='text-align: left;border-right:solid 1px black'>" + sixth.ToString() + "</td></tr> ");
                                    sb.Append("</table>");
                                    sb.Append("</td>");
                                }
                                else if (Tmp == "Performance")
                                {
                                    if (pcount == 3)
                                    {
                                        sb.Append("<td style='text-align: center;border-right:solid 1px white'></td><td style='text-align: center;border-right:solid 1px white'></td><td style='width:100px;text-align: center;border-right:solid 1px white'>Performance</td>");
                                        pcount = pcount + 1;
                                    }
                                    else
                                        pcount = pcount + 1;
                                }
                                else
                                {
                                    sb.Append("<td style='width:100px;text-align: center;' >" + Tmp + "</td>");
                                }
                            }
                            else
                                sb.Append("<td style='text-align: center;' >" + Tmp + "</td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    //Response.Write(tbl.ToString());

                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;filename=AttendanceDetails.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/AttendanceDetails.xls";
                    System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                    Response.Write(sb.ToString());
                    Response.End();
                    //}
                    //catch
                    //{
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }
            else if (radAttend_Leave.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int x, i;
                    //// create table 
                    System.Data.DataTable dTbl = new System.Data.DataTable();
                    dTbl.Columns.Add("Sno");
                    dTbl.Columns.Add("PayCode");
                    dTbl.Columns.Add("EmpName");
                    dTbl.Columns.Add("Department");

                    DataSet ds = new DataSet();
                    strsql = "select datepart(dd,dateoffice) 'dy' from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode=(select top 1 paycode from tbltimeregister where hoursworked>0) ";
                    ds = con.FillDataSet(strsql);
                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Columns.Add(ds.Tables[0].Rows[i1]["dy"].ToString());
                    }

                    //for (i = 1; i <= 31; i++)
                    //  dTbl.Columns.Add(i.ToString());

                    //dTbl.Columns.Add("WDays");
                    //dTbl.Columns.Add("Leave");
                    //dTbl.Columns.Add("Half Day");
                    //dTbl.Columns.Add("C Off");
                    //dTbl.Columns.Add("LWP");
                    //dTbl.Columns.Add("WO and Hld");
                    //dTbl.Columns.Add("Total Days");

                    dTbl.Columns.Add("WDays");
                    dTbl.Columns.Add("Full Day Leave");
                    dTbl.Columns.Add("Half Day Leave");
                    dTbl.Columns.Add("Total Leave");
                    dTbl.Columns.Add("Half Day In Month");
                    dTbl.Columns.Add("C Off");
                    dTbl.Columns.Add("BDay");
                    dTbl.Columns.Add("LWP");
                    dTbl.Columns.Add("WO and Hld");
                    dTbl.Columns.Add("SAT");
                    dTbl.Columns.Add("Total Days");

                    //if (string.IsNullOrEmpty(Selection.ToString()))
                    //{
                    //    strsql = "Select paycode, empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' AND dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' And e.departmentcode = d.departmentcode  order by departmentname, paycode ";
                    //}
                    //else
                    //{
                    //    strsql = "Select tblemployee.paycode, tblemployee.empname,d.departmentname  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' And  tblemployee.departmentcode = d.departmentcode   " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                    //}         
                    if (string.IsNullOrEmpty(Selection.ToString()))
                    {
                        strsql = "Select paycode, empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  AND dateofjoin <='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "'  And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by departmentname, paycode ";
                        //strsql = "Select paycode, empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  and dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  order by departmentname, paycode ";
                        // strsql = "Select paycode, empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  and dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode='10264'  order by departmentname, paycode ";
                    }
                    else
                    {
                        strsql = "Select tblemployee.paycode, tblemployee.empname,d.departmentname  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "' And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)  And tblemployee.departmentcode = d.departmentcode   " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                    }
                    OleDbDataReader dr;
                    OleDbDataReader dr1;
                    dr = con.Execute_Reader(strsql);
                    DataRow dRow;

                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    dTbl.Rows[0][0] = ("Sno");
                    dTbl.Rows[0][1] = ("PayCode");
                    dTbl.Rows[0][2] = ("EmpName");
                    dTbl.Rows[0][3] = ("Department");

                    x = 4;
                    //for (i = 1; i <= 31; i++)
                    //    dTbl.Rows[0][x++] = (i.ToString());

                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Rows[0][x++] = ds.Tables[0].Rows[i1]["dy"].ToString();
                    }

                    //for (i = 1; i <= 31; i++) 
                    //    dTbl.Rows[0][x++]=ds.Tables[0].Rows[i1]["dy"].ToString();
                    //dTbl.Rows[0][x] = ("WDays");
                    //dTbl.Rows[0][x + 1] = ("Leave");
                    //dTbl.Rows[0][x + 2] = ("Half Day");
                    //dTbl.Rows[0][x + 3] = ("C Off");
                    //dTbl.Rows[0][x + 4] = ("LWP");
                    //dTbl.Rows[0][x + 5] = ("WO and Hld");
                    //dTbl.Rows[0][x + 6] = ("Total Days");

                    dTbl.Rows[0][x] = ("WDays");
                    dTbl.Rows[0][x + 1] = ("Full Day Leave");
                    dTbl.Rows[0][x + 2] = ("Half Day Leave");
                    dTbl.Rows[0][x + 3] = ("Total Leave");
                    dTbl.Rows[0][x + 4] = ("Half Day In Month");
                    dTbl.Rows[0][x + 5] = ("C Off");
                    dTbl.Rows[0][x + 6] = ("BDay");
                    dTbl.Rows[0][x + 7] = ("LWP");
                    dTbl.Rows[0][x + 8] = ("WO and Hld");
                    dTbl.Rows[0][x + 9] = ("SAT");
                    dTbl.Rows[0][x + 10] = ("Total Days");

                    i = 1;
                    x = 1;
                    int p = 1;
                    //string inout = "";
                    //double totalLeaves = 0.0;
                    //double Leave = 0.0;
                    //double totalHD = 0.0;
                    //double HD = 0.0;
                    //double totalCoff = 0.0;
                    //double Coff = 0.0;
                    //double totalLWP = 0.0;
                    //double totalWD = 0.0;
                    //double totalWO_Hld = 0.0;
                    //double LWP = 0.0;
                    //double TotalDays = 0.0;

                    string inout = "";
                    double totalLeaves = 0.0;
                    double Leave = 0.0;
                    double totalHD = 0.0;
                    double HD = 0.0;
                    double totalCoff = 0.0;
                    double Coff = 0.0;
                    double Coff1 = 0.0;
                    double totalLWP = 0.0;
                    double LWP = 0.0;
                    double TotalDays = 0.0;
                    double totalWO_Hld = 0.0;
                    double totalWD = 0.0;
                    double halfdayleave = 0.0;
                    double fulldayleave = 0.0;
                    double bdayleave = 0.0;
                    double totalbdayleave = 0.0;
                    double Wdays = 0.0;
                    double TotalWdays = 0.0;

                    while (dr.Read())
                    {
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[i]["Sno"] = Convert.ToString(x++);
                        Tmp = dr["Paycode"].ToString().Trim();
                        dTbl.Rows[i]["PayCode"] = Tmp;  //dr["Paycode"].ToString().Trim();
                        dTbl.Rows[i]["Empname"] = dr["Empname"].ToString().Trim();
                        dTbl.Rows[i]["Department"] = dr["DepartmentName"].ToString().Trim();
                        p = x;
                        x = 4;
                        {
                            //strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                            //             "Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                            //            "Halfvalue=(select isnull(count(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue=0.5 ),  " +
                            //            "Leavevalue=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount > 0 ),  " +
                            //            "Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ), " +
                            //            " coff=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and upper(Status)='COF' ) , " +
                            //            " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value =1 ) , " +
                            //            " hld=(select isnull(convert(decimal(10,2),sum(tbltimeregister.holiday_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  ) , " +
                            //            " WO_half=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value = 0.5 ) , " +
                            //            "  tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                            //            " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                            //            " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";


                            strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                         " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                        " Halfvalue=(select isnull(count(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue=0.5 ),  " +
                                        " FullLeavevalue=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount = 1 ),  " +
                                        " HalfLeavevalue=(select isnull(count(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount = 0.5 ),  " +
                                        " Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ), " +
                                        " coff=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and ( upper(Status)='CO' or upper(Status)='H_CO' ) ) , " +
                                        " B_day=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and (upper(Status)='BDL' or upper(Status)='H_BDL' ) ) , " +
                                        " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value =1 ) , " +
                                        " hld=(select isnull(convert(decimal(10,2),sum(tbltimeregister.holiday_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  ) , " +
                                        " WO_half=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value = 0.5 ) , " +
                                        " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                        " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                        " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";




                            int dd = 0;
                            strsql1 = "";
                            strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',e.dateofjoin)) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                            ds = new DataSet();
                            ds = con.FillDataSet(strsql1);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                if (dd > 0)
                                {
                                    x = x + dd;
                                }
                            }

                            dr1 = con.Execute_Reader(strsql);
                            while (dr1.Read())
                            {
                                inout = dr1["Thours"].ToString().Trim();
                                if (inout.ToString().Trim() != "00:00")
                                {
                                    if (dr1["Status"].ToString().Trim().ToUpper() == "H_CL")
                                    {
                                        dTbl.Rows[i][x] = dr1["Thours"].ToString().Trim() + "- H_CL";
                                    }
                                    else
                                    {
                                        dTbl.Rows[i][x] = dr1["Thours"].ToString().Trim();
                                    }
                                }
                                else
                                {
                                    dTbl.Rows[i][x] = dr1["status"].ToString().Trim();
                                }

                                Coff1 = Convert.ToDouble(dr1["coff"].ToString().Trim());
                                bdayleave = Convert.ToDouble(dr1["B_day"].ToString().Trim());
                                dTbl.Rows[i]["C Off"] = dr1["coff"].ToString().Trim();
                                dTbl.Rows[i]["BDay"] = dr1["B_day"].ToString().Trim();

                                totalWD = Convert.ToDouble(dr1["Presentvalue"].ToString().Trim()) - Coff1 - bdayleave;
                                dTbl.Rows[i]["WDays"] = totalWD.ToString();
                                // dTbl.Rows[i]["Total Leave"] = dr1["Leavevalue"].ToString().Trim();
                                dTbl.Rows[i]["Half Day In Month"] = dr1["Halfvalue"].ToString().Trim();

                                dTbl.Rows[i]["LWP"] = dr1["Absentvalue"].ToString().Trim();

                                dTbl.Rows[i]["Full Day Leave"] = dr1["FullLeavevalue"].ToString().Trim();
                                dTbl.Rows[i]["Half Day Leave"] = dr1["HalfLeavevalue"].ToString().Trim();


                                fulldayleave = Convert.ToDouble(dr1["FullLeavevalue"].ToString().Trim());
                                halfdayleave = Convert.ToDouble(dr1["HalfLeavevalue"].ToString().Trim()) / 2;

                                dTbl.Rows[i]["Total Leave"] = fulldayleave + halfdayleave;

                                totalWO_Hld = Convert.ToDouble(dr1["WO_full"].ToString().Trim()) + Convert.ToDouble(dr1["hld"].ToString().Trim());
                                dTbl.Rows[i]["WO and Hld"] = totalWO_Hld.ToString();
                                dTbl.Rows[i]["SAT"] = Convert.ToDouble(dr1["WO_half"].ToString().Trim());
                                TotalDays = totalWD + fulldayleave + halfdayleave + Coff1 + bdayleave + totalWO_Hld + Convert.ToDouble(dr1["WO_half"].ToString().Trim());
                                dTbl.Rows[i]["Total Days"] = TotalDays.ToString().Trim();
                                Wdays = totalWD;
                                LWP = Convert.ToDouble(dr1["Absentvalue"]);
                                HD = Convert.ToDouble(dr1["Halfvalue"]);
                                Coff = Convert.ToDouble(dr1["coff"]);
                                Leave = fulldayleave + halfdayleave;



                                x++;
                            }
                            TotalWdays += Wdays;
                            totalCoff += Coff;
                            totalLeaves += Leave;
                            totalLWP += LWP;
                            totalHD += HD;
                            totalbdayleave += bdayleave;
                        }
                        i++;
                        x = p;
                    }
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);
                    dTbl.Rows[i][0] = "Total";
                    dTbl.Rows[i]["WDays"] = TotalWdays.ToString().Trim();
                    dTbl.Rows[i]["Total Leave"] = totalLeaves.ToString().Trim();
                    dTbl.Rows[i]["Half Day In Month"] = totalHD.ToString().Trim();
                    dTbl.Rows[i]["C Off"] = totalCoff.ToString().Trim();
                    dTbl.Rows[i]["BDay"] = totalbdayleave.ToString().Trim();
                    dTbl.Rows[i]["LWP"] = totalLWP.ToString().Trim();
                    dr.Close();

                    string msg = "Attendance details for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1'>");

                    sb.Append("<tr><td colspan='45' style='text-align: center'> " + msg.ToString() + " </td></tr> ");
                    sb.Append("<tr><td colspan='45' style='text-align: left'></td></tr> ");

                    x = dTbl.Columns.Count;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                                if (Tmp.Substring(0, 1) == "0")
                                {
                                    if (Tmp.Contains(":"))
                                    {
                                        Tmp = Tmp;
                                    }
                                    else
                                    {
                                        Tmp = Tmp;
                                        //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                    }
                                }
                            }
                            if (i >= 4)
                                sb.Append("<td style='width:75px;text-align: left''>" + Tmp + "</td>");
                            else
                                sb.Append("<td style='text-align: left'>" + Tmp + "</td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;filename=Attendnce_PTT.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/Attendnce_PTT.xls";
                    System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }
            else if (RadAtt_Leave.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    string Tmp = "";
                    int x, i;
                    //// create table 
                    System.Data.DataTable dTbl = new System.Data.DataTable();
                    dTbl.Columns.Add("Sno");
                    dTbl.Columns.Add("PayCode");
                    dTbl.Columns.Add("EmpName");
                    dTbl.Columns.Add("Department");
                    dTbl.Columns.Add("Designation");
                    //
                    //              DataSet ds = new DataSet();
                    //            strsql = "select datepart(dd,dateoffice) 'dy' from tbltimeregister where dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode=(select top 1 paycode from tbltimeregister where hoursworked>0) ";
                    //          ds = con.FillDataSet(strsql);
                    // for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    // {
                    //    dTbl.Columns.Add(ds.Tables[0].Rows[i1]["dy"].ToString());
                    //                }

                    //for (i = 1; i <= 31; i++)
                    //  dTbl.Columns.Add(i.ToString());

                    dTbl.Columns.Add("WDays");
                    dTbl.Columns.Add("Full Day Leave");
                    dTbl.Columns.Add("Half Day Leave");
                    dTbl.Columns.Add("Total Leave");
                    dTbl.Columns.Add("Half Day In Month");
                    dTbl.Columns.Add("C Off");
                    dTbl.Columns.Add("BDay");
                    dTbl.Columns.Add("LWP");
                    dTbl.Columns.Add("WO and Hld");
                    dTbl.Columns.Add("SAT");
                    dTbl.Columns.Add("Total Days");
                    //if (string.IsNullOrEmpty(Selection.ToString()))
                    //{
                    //    strsql = "Select paycode, empname,departmentname,designation  from tblemployee e, tblDepartment d where Active='Y' AND dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' And e.departmentcode = d.departmentcode  order by departmentname, paycode ";
                    //}
                    //else
                    //{
                    //    strsql = "Select tblemployee.paycode, tblemployee.empname,d.departmentname,tblemployee.designation  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' And tblemployee.departmentcode = d.departmentcode   " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                    //}         

                    if (string.IsNullOrEmpty(Selection.ToString()))
                    {
                        strsql = "Select paycode, empname,departmentname,Designation  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  AND dateofjoin <='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "' And (LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or LeavingDate is null) order by departmentname, paycode ";
                        //strsql = "Select paycode, empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  and dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  order by departmentname, paycode ";
                        // strsql = "Select paycode, empname,departmentname  from tblemployee e, tblDepartment d where Active='Y' And e.departmentcode = d.departmentcode  and dateofjoin between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and paycode='10264'  order by departmentname, paycode ";
                    }
                    else
                    {
                        strsql = "Select tblemployee.paycode, tblemployee.empname,d.departmentname,tblemployee.Designation  from tblemployee tblemployee, tblDepartment d where tblemployee.Active='Y' AND dateofjoin <='" + FromDate.AddMonths(1).ToString("yyyy-MM-dd") + "'   And tblemployee.departmentcode = d.departmentcode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)  " + Selection.ToString() + " order by d.departmentname, tblemployee.paycode  ";
                    }

                    OleDbDataReader dr;
                    OleDbDataReader dr1;
                    dr = con.Execute_Reader(strsql);
                    DataRow dRow;

                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    dTbl.Rows[0][0] = ("Sno");
                    dTbl.Rows[0][1] = ("PayCode");
                    dTbl.Rows[0][2] = ("EmpName");
                    dTbl.Rows[0][3] = ("Department");
                    dTbl.Rows[0][4] = ("Designation");

                    x = 5;
                    //for (i = 1; i <= 31; i++)
                    //    dTbl.Rows[0][x++] = (i.ToString());

                    //for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    // {
                    //dTbl.Rows[0][x++] = ds.Tables[0].Rows[i1]["dy"].ToString();
                    // }

                    //for (i = 1; i <= 31; i++) 
                    //    dTbl.Rows[0][x++]=ds.Tables[0].Rows[i1]["dy"].ToString();
                    dTbl.Rows[0][x] = ("WDays");
                    dTbl.Rows[0][x + 1] = ("Full Day Leave");
                    dTbl.Rows[0][x + 2] = ("Half Day Leave");
                    dTbl.Rows[0][x + 3] = ("Total Leave");
                    dTbl.Rows[0][x + 4] = ("Half Day In Month");
                    dTbl.Rows[0][x + 5] = ("C Off");
                    dTbl.Rows[0][x + 6] = ("BDay");
                    dTbl.Rows[0][x + 7] = ("LWP");
                    dTbl.Rows[0][x + 8] = ("WO and Hld");
                    dTbl.Rows[0][x + 9] = ("SAT");
                    dTbl.Rows[0][x + 10] = ("Total Days");


                    i = 1;
                    x = 1;
                    int p = 1;
                    //string inout = "";
                    //double totalLeaves = 0.0;
                    //double Leave = 0.0;
                    //double totalHD = 0.0;
                    //double HD = 0.0;
                    //double totalCoff = 0.0;
                    //double Coff = 0.0;
                    //double totalLWP = 0.0;
                    //double totalWD = 0.0;
                    //double totalWO_Hld = 0.0;
                    //double LWP = 0.0;
                    //double TotalDays = 0.0;

                    string inout = "";
                    double totalLeaves = 0.0;
                    double Leave = 0.0;
                    double totalHD = 0.0;
                    double HD = 0.0;
                    double totalCoff = 0.0;
                    double Coff = 0.0;
                    double Coff1 = 0.0;
                    double totalLWP = 0.0;
                    double LWP = 0.0;
                    double TotalDays = 0.0;
                    double totalWO_Hld = 0.0;
                    double totalWD = 0.0;
                    double halfdayleave = 0.0;
                    double fulldayleave = 0.0;
                    double bdayleave = 0.0;
                    double totalbdayleave = 0.0;
                    double Wdays = 0.0;
                    double TotalWdays = 0.0;

                    while (dr.Read())
                    {
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[i]["Sno"] = Convert.ToString(x++);
                        Tmp = dr["Paycode"].ToString().Trim();
                        dTbl.Rows[i]["PayCode"] = Tmp;  //dr["Paycode"].ToString().Trim();
                        dTbl.Rows[i]["Empname"] = dr["Empname"].ToString().Trim();
                        dTbl.Rows[i]["Department"] = dr["DepartmentName"].ToString().Trim();
                        dTbl.Rows[i]["Designation"] = dr["Designation"].ToString().Trim();
                        p = x;
                        x = 4;
                        {
                            //strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                            //             "Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                            //            "Halfvalue=(select isnull(count(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue=0.5 ),  " +
                            //            "Leavevalue=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount > 0 ),  " +
                            //            "Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ), " +
                            //            " coff=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and upper(Status)='COF' ) , " +
                            //            " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value =1 ) , " +
                            //            " hld=(select isnull(convert(decimal(10,2),sum(tbltimeregister.holiday_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  ) , " +
                            //            " WO_half=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value = 0.5 ) , " +
                            //            "  tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                            //            " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                            //            " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";

                            strsql = " select datepart(dd,dateadd(dd,-1,dateadd(mm,1,cast(cast(year('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-'+cast(month('" + FromDate.ToString("yyyy-MM-dd") + "') as varchar)+'-01' as datetime)))) 'Days', " +
                                        " Presentvalue=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue > 0   ),  " +
                                       " Halfvalue=(select isnull(count(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and presentvalue=0.5 ),  " +
                                       " FullLeavevalue=(select isnull(sum(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount = 1 ),  " +
                                       " HalfLeavevalue=(select isnull(count(tbltimeregister.leavevalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavetype='L' and leaveamount = 0.5 ),  " +
                                       " Absentvalue=(select isnull(sum(tbltimeregister.absentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and absentvalue > 0 ), " +
                                       " coff=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and ( upper(Status)='CO' or upper(Status)='H_CO' ) ) , " +
                                       " B_day=(select isnull(sum(tbltimeregister.presentvalue),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and (upper(Status)='BDL' or upper(Status)='H_BDL' ) ) , " +
                                       " WO_full=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value =1 ) , " +
                                       " hld=(select isnull(convert(decimal(10,2),sum(tbltimeregister.holiday_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'  ) , " +
                                       " WO_half=(select isnull(convert(decimal(10,2),sum(tbltimeregister.Wo_value)),0) from tbltimeregister where tbltimeregister.paycode=tblemployee.paycode and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and wo_value = 0.5 ) , " +
                                       " tblCatagory.CatagoryName, tblDivision.DivisionName,datepart(dd,tblTimeRegister.DateOffice) 'DateOffice',tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblTimeRegister.PayCode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.DepartmentCode, tblDepartment.DepartmentName,tblTimeRegister.Wo_Value,tblTimeRegister.PresentValue, tblTimeRegister.AbsentValue,tblTimeRegister.LeaveValue,tblTimeRegister.Status, tblTimeRegister.HoliDay_value,tblTimeRegister.leavetype1,tblTimeRegister.leavetype2, tblTimeRegister.firsthalfleavecode,tblTimeRegister.secondhalfleavecode,tblTimeRegister.LeaveCode,  " +
                                       " convert(varchar(5),tblTimeRegister.in1,108) in1,convert(varchar(5),tblTimeRegister.out2,108) out2, " +
                                       " convert(varchar(5),dateadd(minute,tblTimeRegister.hoursworked,0),108) 'Thours',tblTimeRegister.LeaveAmount ,tblTimeRegister.LeaveAmount1,tblTimeRegister.LeaveAmount2 from tblCatagory,tblDivision, tblTimeRegister,tblEmployee,tblCompany,tblDepartment Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND 1=1 and 1=1 and 1=1 and tblemployee.paycode in ('" + Tmp.ToString() + "') ";


                            int dd = 0;
                            strsql1 = "";
                            strsql1 = "select distinct(datediff(dd,'" + FromDate.ToString("yyyy-MM-dd") + "',e.dateofjoin)) 'dd' from tbltimeregister t join tblemployee e on t.paycode=e.paycode where e.paycode='" + Tmp.ToString() + "'";
                            ds = new DataSet();
                            ds = con.FillDataSet(strsql1);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                dd = Convert.ToInt32(ds.Tables[0].Rows[0]["dd"].ToString());
                                if (dd > 0)
                                {
                                    x = x + dd;
                                }
                            }

                            dr1 = con.Execute_Reader(strsql);
                            while (dr1.Read())
                            {
                                //totalWD =Convert.ToDouble(dr1["Presentvalue"].ToString().Trim()) ;
                                //dTbl.Rows[i]["WDays"] = totalWD.ToString();
                                //dTbl.Rows[i]["Leave"] = dr1["Leavevalue"].ToString().Trim();
                                //dTbl.Rows[i]["Half Day"] = dr1["Halfvalue"].ToString().Trim();
                                //dTbl.Rows[i]["C Off"] = dr1["coff"].ToString().Trim();
                                //dTbl.Rows[i]["LWP"] = dr1["Absentvalue"].ToString().Trim();
                                //totalWO_Hld = Convert.ToDouble(dr1["WO_full"].ToString().Trim()) + Convert.ToDouble(dr1["hld"].ToString().Trim()) + Convert.ToDouble(dr1["Wo_half"].ToString().Trim());
                                //dTbl.Rows[i]["WO and Hld"] = totalWO_Hld.ToString();
                                //TotalDays = Convert.ToDouble(dr1["Presentvalue"]) + Convert.ToDouble(dr1["Leavevalue"]) + Convert.ToDouble(dr1["WO_full"]) + Convert.ToDouble(dr1["hld"]) + Convert.ToDouble(dr1["WO_half"]) ;
                                //dTbl.Rows[i]["Total Days"] = TotalDays.ToString().Trim();
                                //LWP = Convert.ToDouble(dr1["Absentvalue"]);
                                //HD = Convert.ToDouble(dr1["Halfvalue"]);
                                //Coff = Convert.ToDouble(dr1["coff"]);
                                //Leave = Convert.ToDouble(dr1["Leavevalue"]);


                                Coff1 = Convert.ToDouble(dr1["coff"].ToString().Trim());
                                bdayleave = Convert.ToDouble(dr1["B_day"].ToString().Trim());
                                dTbl.Rows[i]["C Off"] = dr1["coff"].ToString().Trim();
                                dTbl.Rows[i]["BDay"] = dr1["B_day"].ToString().Trim();

                                totalWD = Convert.ToDouble(dr1["Presentvalue"].ToString().Trim()) - Coff1 - bdayleave;
                                dTbl.Rows[i]["WDays"] = totalWD.ToString();
                                // dTbl.Rows[i]["Total Leave"] = dr1["Leavevalue"].ToString().Trim();
                                dTbl.Rows[i]["Half Day In Month"] = dr1["Halfvalue"].ToString().Trim();

                                dTbl.Rows[i]["LWP"] = dr1["Absentvalue"].ToString().Trim();

                                dTbl.Rows[i]["Full Day Leave"] = dr1["FullLeavevalue"].ToString().Trim();
                                dTbl.Rows[i]["Half Day Leave"] = dr1["HalfLeavevalue"].ToString().Trim();


                                fulldayleave = Convert.ToDouble(dr1["FullLeavevalue"].ToString().Trim());
                                halfdayleave = Convert.ToDouble(dr1["HalfLeavevalue"].ToString().Trim()) / 2;

                                dTbl.Rows[i]["Total Leave"] = fulldayleave + halfdayleave;

                                totalWO_Hld = Convert.ToDouble(dr1["WO_full"].ToString().Trim()) + Convert.ToDouble(dr1["hld"].ToString().Trim());
                                dTbl.Rows[i]["WO and Hld"] = totalWO_Hld.ToString();
                                dTbl.Rows[i]["SAT"] = Convert.ToDouble(dr1["WO_half"].ToString().Trim());
                                TotalDays = totalWD + fulldayleave + halfdayleave + Coff1 + bdayleave + totalWO_Hld + Convert.ToDouble(dr1["WO_half"].ToString().Trim());
                                dTbl.Rows[i]["Total Days"] = TotalDays.ToString().Trim();
                                Wdays = totalWD;
                                LWP = Convert.ToDouble(dr1["Absentvalue"]);
                                HD = Convert.ToDouble(dr1["Halfvalue"]);
                                Coff = Convert.ToDouble(dr1["coff"]);
                                Leave = fulldayleave + halfdayleave;
                                x++;
                            }
                            TotalWdays += Wdays;
                            totalCoff += Coff;
                            totalLeaves += Leave;
                            totalLWP += LWP;
                            totalHD += HD;
                            totalbdayleave += bdayleave;
                        }
                        i++;
                        x = p;
                    }
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);
                    dTbl.Rows[i][0] = "Total";
                    dTbl.Rows[i]["WDays"] = TotalWdays.ToString().Trim();
                    dTbl.Rows[i]["Total Leave"] = totalLeaves.ToString().Trim();
                    dTbl.Rows[i]["Half Day In Month"] = totalHD.ToString().Trim();
                    dTbl.Rows[i]["C Off"] = totalCoff.ToString().Trim();
                    dTbl.Rows[i]["BDay"] = totalbdayleave.ToString().Trim();
                    dTbl.Rows[i]["LWP"] = totalLWP.ToString().Trim();
                    dr.Close();

                    string msg = "Attendance details for the month of " + FromDate.ToString("MMMM") + " " + FromDate.Year.ToString("0000") + " from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1'>");

                    sb.Append("<tr><td colspan='16' style='text-align: center'> " + msg.ToString() + " </td></tr> ");
                    sb.Append("<tr><td colspan='16' style='text-align: left'></td></tr> ");

                    x = dTbl.Columns.Count;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                                if (Tmp.Substring(0, 1) == "0")
                                {
                                    if (Tmp.Contains(":"))
                                    {
                                        Tmp = Tmp;
                                    }
                                    else
                                    {
                                        Tmp = Tmp;
                                        //Tmp = "=text( " + Tmp + ",\"" + mm + "\" ) ";
                                    }
                                }
                            }
                            if (i >= 4)
                                sb.Append("<td style='width:75px;text-align: left''>" + Tmp + "</td>");
                            else
                                sb.Append("<td style='text-align: left'>" + Tmp + "</td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;filename=Attendnce_PTT.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/Attendnce_PTT.xls";
                    System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                    Response.Write(sb.ToString());
                    Response.End();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format');", true);
                    return;
                }
            }*/
            else if (radNightShift.Checked)
            {

                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("Catagory");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("Designation");
                        dTbl.Columns.Add("NightPresent");
                        dTbl.Columns.Add("HOD");
                        dTbl.Columns.Add("Location");



                        int ct = 0;
                        if (Session["PAYCODE"].ToString().ToString().ToUpper() != "ADMIN" && Session["usertype"].ToString().ToString().ToUpper() != "A")
                        {
                            strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                            dsEmpCount = con.FillDataSet(strsql);
                            if (dsEmpCount.Tables[0].Rows.Count == 0)
                            {
                                return;
                            }
                            else
                            {
                                for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                                {
                                    EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                                }
                                if (!string.IsNullOrEmpty(EmpCodes))
                                {
                                    EmpCodes = EmpCodes.Substring(1);
                                }
                            }
                            strsql = " select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tbldepartment.DEPARTMENTName 'Department',tblcatagory.CATagoryname 'Catagory', " +
                                " Headid=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid),Designation, " +
                                " convert(decimal(10,2),sum(tbltimeregister.presentvalue))'Present',tblemployee.companycode from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode " +
                                " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT   " +
                                " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                                " where tbltimeregister.presentvalue > 0 and tblemployee.paycode in (" + EmpCodes.ToString() + ") and tblemployee.Active='Y'  and tbltimeregister.dateoffice " +
                                //" between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'   and datediff(dd,in1,out2) = 1 " +
                                " between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'   and Shiftattended = 'N' and status='P' " +
                                " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1   " +
                                " group by tbltimeregister.paycode,tblemployee.empname, tbldepartment.DEPARTMENTName,tblcatagory.CATagoryname,tblemployee.paycode,Headid,Designation,tblemployee.companycode order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "  ";
                        }
                        else
                        {
                            strsql = "";
                            strsql = " select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tbldepartment.DEPARTMENTName 'Department',tblcatagory.CATagoryname 'Catagory', " +
                                " Headid=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid),Designation, " +
                               " convert(decimal(10,2),sum(tbltimeregister.presentvalue))'Present',tblemployee.companycode from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode " +
                               " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT   " +
                               " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                               " where tbltimeregister.presentvalue > 0  and tblemployee.Active='Y'  and tbltimeregister.dateoffice " +
                                // " between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'   and datediff(dd,in1,out2) = 1 " +
                               " between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'   and Shiftattended = 'N' and status='P' " +
                               " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1   ";

                            if (Selection.ToString().Trim() != "")
                            {
                                strsql = strsql + Selection.ToString();
                            }
                            strsql += " group by tbltimeregister.paycode,tblemployee.empname, tbldepartment.DEPARTMENTName,tblcatagory.CATagoryname,tblemployee.paycode,Headid,Designation,tblemployee.companycode order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";
                        }
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["Paycode"] = "Code";
                        dTbl.Rows[ct]["Catagory"] = "Catagory";
                        dTbl.Rows[ct]["Name"] = "Name";
                        dTbl.Rows[ct]["Department"] = "Dept.";
                        dTbl.Rows[ct]["DESIGNATION"] = "Designation";
                        dTbl.Rows[ct]["NightPresent"] = "Days";
                        dTbl.Rows[ct]["HOD"] = "HOD";
                        dTbl.Rows[ct]["Location"] = "CompanyCode";
                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1][0].ToString();
                                dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1][1].ToString();
                                dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1][2].ToString();
                                dTbl.Rows[ct]["Catagory"] = dsResult.Tables[0].Rows[i1][3].ToString();
                                dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1][4].ToString();
                                dTbl.Rows[ct]["NightPresent"] = dsResult.Tables[0].Rows[i1][6].ToString();
                                dTbl.Rows[ct]["Designation"] = dsResult.Tables[0].Rows[i1][5].ToString();
                                dTbl.Rows[ct]["Location"] = dsResult.Tables[0].Rows[i1][7].ToString();
                                ct = ct + 1;
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Employee Night Attendance Count From " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if ((i == 0) || (i == 7))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:50px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 10)
                                    sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=EmpNightAttendance.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/EmpNightAttendance.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report available in Excel format.');", true);
                    return;
                }
            }
            else if (radShiftChangeSBU.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        double lngTotalEmpCount = 0;
                        string strDepartmentName = "";
                        string strDepartmentCode = "";
                        double NightDbl = 0;
                        double Mornigdbl = 0;
                        double EveDbl = 0;
                        double GenDbl = 0;
                        double OffDbl = 0;
                        double NightchangeDbl = 0;
                        double Mornigchangedbl = 0;
                        double EvechangeDbl = 0;
                        double GenchangeDbl = 0;
                        double OffchangeDbl = 0;
                        double ANightDbl = 0;
                        double AMornigdbl = 0;
                        double AEveDbl = 0;
                        double AGenDbl = 0;
                        double AOffDbl = 0;
                        int ct = 0;
                        int mcount = 0;
                        string Tmp = "";
                        int x, i;

                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("SBU");
                        dTbl.Columns.Add("N");
                        dTbl.Columns.Add("M");
                        dTbl.Columns.Add("E");
                        dTbl.Columns.Add("G");
                        dTbl.Columns.Add("O");

                        dTbl.Columns.Add("N1");
                        dTbl.Columns.Add("M1");
                        dTbl.Columns.Add("E1");
                        dTbl.Columns.Add("G1");
                        dTbl.Columns.Add("O1");


                        strsql = " Select TBLCATAGORY.CAT,tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,TBLTIMEREGISTER.SHIFTATTENDED,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " +
                                    " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " +
                                    " Where (TBLTIMEREGISTER.PRESENTVALUE>0 OR SHIFT='OFF') AND tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" +
                                    " AND tbltimeregister.DateOffice  BETWEEN '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " order by  TBLCATAGORY.CAT";
                        DataSet dsResult = new DataSet();
                        dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count <= 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        ct = 0;

                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["SBU"] = "SBU";
                        dTbl.Rows[ct]["N"] = "N";
                        dTbl.Rows[ct]["M"] = "M";
                        dTbl.Rows[ct]["E"] = "E";
                        dTbl.Rows[ct]["G"] = "G";
                        dTbl.Rows[ct]["O"] = "O";

                        dTbl.Rows[ct]["N1"] = "N";
                        dTbl.Rows[ct]["M1"] = "M";
                        dTbl.Rows[ct]["E1"] = "E";
                        dTbl.Rows[ct]["G1"] = "G";
                        dTbl.Rows[ct]["O1"] = "O";


                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        //dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {



                                lngTotalEmpCount = 0;

                                strDepartmentName = dsResult.Tables[0].Rows[i1]["CatagoryName"].ToString().Trim();
                                strDepartmentCode = dsResult.Tables[0].Rows[i1]["cat"].ToString().Trim();
                                NightDbl = 0;
                                Mornigdbl = 0;
                                EveDbl = 0;
                                GenDbl = 0;
                                OffDbl = 0;
                                NightchangeDbl = 0;
                                Mornigchangedbl = 0;
                                EvechangeDbl = 0;
                                GenchangeDbl = 0;
                                OffchangeDbl = 0;
                                ANightDbl = 0;
                                AMornigdbl = 0;
                                AEveDbl = 0;
                                AGenDbl = 0;
                                AOffDbl = 0;

                                while (strDepartmentCode.ToString().Trim() == dsResult.Tables[0].Rows[i1]["CAT"].ToString().Trim())
                                {
                                    if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "OFF")
                                    {
                                        OffDbl = OffDbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "N")
                                    {
                                        NightDbl = NightDbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "M")
                                    {
                                        Mornigdbl = Mornigdbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "E")
                                    {
                                        EveDbl = EveDbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "G")
                                    {
                                        GenDbl = GenDbl + 1;
                                    }
                                    if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() != dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim(' '))
                                    {
                                        if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "OFF")
                                        {
                                            OffchangeDbl = OffchangeDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "N")
                                        {
                                            NightchangeDbl = NightchangeDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "M")
                                        {
                                            Mornigchangedbl = Mornigchangedbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "E")
                                        {
                                            EvechangeDbl = EvechangeDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "G")
                                        {
                                            GenchangeDbl = GenchangeDbl + 1;
                                        }
                                    }

                                    if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() != dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim())
                                    {
                                        if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "OFF")
                                        {
                                            AOffDbl = AOffDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "N")
                                        {
                                            ANightDbl = ANightDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "M")
                                        {
                                            AMornigdbl = AMornigdbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "E")
                                        {
                                            AEveDbl = AEveDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "G")
                                        {
                                            AGenDbl = AGenDbl + 1;
                                        }
                                    }

                                    lngTotalEmpCount = lngTotalEmpCount + 1;
                                    i1 = i1 + 1;
                                    if (Convert.ToInt32(dsResult.Tables[0].Rows.Count) == i1)
                                    {
                                        break;
                                    }
                                }

                                mcount = mcount + 1;
                                dTbl.Rows[ct]["SNo"] = mcount;
                                dTbl.Rows[ct]["SBU"] = strDepartmentName;
                                dTbl.Rows[ct]["N"] = NightDbl;
                                dTbl.Rows[ct]["M"] = Mornigdbl;
                                dTbl.Rows[ct]["E"] = EveDbl;
                                dTbl.Rows[ct]["G"] = GenDbl;
                                dTbl.Rows[ct]["O"] = OffDbl;

                                dTbl.Rows[ct]["N1"] = ANightDbl;
                                dTbl.Rows[ct]["M1"] = AMornigdbl;
                                dTbl.Rows[ct]["E1"] = AEveDbl;
                                dTbl.Rows[ct]["G1"] = AGenDbl;
                                dTbl.Rows[ct]["O1"] = AOffDbl;

                                ct = ct + 1;
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                i1 = i1 - 1;
                            }
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "SBU WISE SHIFT CHANGE SUMMARY FROM " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right;padding-right:15px'>Run Date & Time : " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='7' style='text-align: center'>ROSTER</td><td colspan='5' style='text-align: center'>CHANGE</td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 10)
                                    sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=SBUShiftChange.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/SBUShiftChange.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report available in Excel format.');", true);
                    return;
                }
            }
            else if (radShiftChangeDepartment.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        double lngTotalEmpCount = 0;
                        string strDepartmentName = "";
                        string strDepartmentCode = "";
                        double NightDbl = 0;
                        double Mornigdbl = 0;
                        double EveDbl = 0;
                        double GenDbl = 0;
                        double OffDbl = 0;
                        double NightchangeDbl = 0;
                        double Mornigchangedbl = 0;
                        double EvechangeDbl = 0;
                        double GenchangeDbl = 0;
                        double OffchangeDbl = 0;
                        double ANightDbl = 0;
                        double AMornigdbl = 0;
                        double AEveDbl = 0;
                        double AGenDbl = 0;
                        double AOffDbl = 0;
                        int ct = 0;
                        int mcount = 0;
                        string Tmp = "";
                        int x, i;

                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("DEPT");
                        dTbl.Columns.Add("N");
                        dTbl.Columns.Add("M");
                        dTbl.Columns.Add("E");
                        dTbl.Columns.Add("G");
                        dTbl.Columns.Add("O");

                        dTbl.Columns.Add("N1");
                        dTbl.Columns.Add("M1");
                        dTbl.Columns.Add("E1");
                        dTbl.Columns.Add("G1");
                        dTbl.Columns.Add("O1");


                        strsql = " Select TBLCATAGORY.CAT,tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,TBLTIMEREGISTER.SHIFTATTENDED,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " +
                                    " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " +
                                    " Where (TBLTIMEREGISTER.PRESENTVALUE>0 OR SHIFT='OFF') AND tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" +
                                    " AND tbltimeregister.DateOffice  BETWEEN '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null)";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " order by  TblDepartment.DepartmentCode";
                        DataSet dsResult = new DataSet();
                        dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count <= 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        ct = 0;

                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["DEPT"] = "DEPT";
                        dTbl.Rows[ct]["N"] = "N";
                        dTbl.Rows[ct]["M"] = "M";
                        dTbl.Rows[ct]["E"] = "E";
                        dTbl.Rows[ct]["G"] = "G";
                        dTbl.Rows[ct]["O"] = "O";

                        dTbl.Rows[ct]["N1"] = "N";
                        dTbl.Rows[ct]["M1"] = "M";
                        dTbl.Rows[ct]["E1"] = "E";
                        dTbl.Rows[ct]["G1"] = "G";
                        dTbl.Rows[ct]["O1"] = "O";


                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        //dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngTotalEmpCount = 0;
                                strDepartmentName = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString().Trim();
                                strDepartmentCode = dsResult.Tables[0].Rows[i1]["DepartmentCode"].ToString().Trim();
                                NightDbl = 0;
                                Mornigdbl = 0;
                                EveDbl = 0;
                                GenDbl = 0;
                                OffDbl = 0;
                                NightchangeDbl = 0;
                                Mornigchangedbl = 0;
                                EvechangeDbl = 0;
                                GenchangeDbl = 0;
                                OffchangeDbl = 0;
                                ANightDbl = 0;
                                AMornigdbl = 0;
                                AEveDbl = 0;
                                AGenDbl = 0;
                                AOffDbl = 0;

                                while (strDepartmentCode.ToString().Trim() == dsResult.Tables[0].Rows[i1]["DepartmentCode"].ToString().Trim())
                                {
                                    if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "OFF")
                                    {
                                        OffDbl = OffDbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "N")
                                    {
                                        NightDbl = NightDbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "M")
                                    {
                                        Mornigdbl = Mornigdbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "E")
                                    {
                                        EveDbl = EveDbl + 1;
                                    }
                                    else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "G")
                                    {
                                        GenDbl = GenDbl + 1;
                                    }
                                    if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() != dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim(' '))
                                    {
                                        if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "OFF")
                                        {
                                            OffchangeDbl = OffchangeDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "N")
                                        {
                                            NightchangeDbl = NightchangeDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "M")
                                        {
                                            Mornigchangedbl = Mornigchangedbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "E")
                                        {
                                            EvechangeDbl = EvechangeDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() == "G")
                                        {
                                            GenchangeDbl = GenchangeDbl + 1;
                                        }
                                    }

                                    if (dsResult.Tables[0].Rows[i1]["Shift"].ToString().Trim() != dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim())
                                    {
                                        if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "OFF")
                                        {
                                            AOffDbl = AOffDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "N")
                                        {
                                            ANightDbl = ANightDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "M")
                                        {
                                            AMornigdbl = AMornigdbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "E")
                                        {
                                            AEveDbl = AEveDbl + 1;
                                        }
                                        else if (dsResult.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim() == "G")
                                        {
                                            AGenDbl = AGenDbl + 1;
                                        }
                                    }

                                    lngTotalEmpCount = lngTotalEmpCount + 1;
                                    i1 = i1 + 1;
                                    if (Convert.ToInt32(dsResult.Tables[0].Rows.Count) == i1)
                                    {
                                        break;
                                    }
                                }

                                mcount = mcount + 1;
                                dTbl.Rows[ct]["SNo"] = mcount;
                                dTbl.Rows[ct]["DEPT"] = strDepartmentName;
                                dTbl.Rows[ct]["N"] = NightDbl;
                                dTbl.Rows[ct]["M"] = Mornigdbl;
                                dTbl.Rows[ct]["E"] = EveDbl;
                                dTbl.Rows[ct]["G"] = GenDbl;
                                dTbl.Rows[ct]["O"] = OffDbl;

                                dTbl.Rows[ct]["N1"] = ANightDbl;
                                dTbl.Rows[ct]["M1"] = AMornigdbl;
                                dTbl.Rows[ct]["E1"] = AEveDbl;
                                dTbl.Rows[ct]["G1"] = AGenDbl;
                                dTbl.Rows[ct]["O1"] = AOffDbl;

                                ct = ct + 1;
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                i1 = i1 - 1;
                            }
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        strsql = "Select companyname from tblcompany ";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "DEPARTMENT WISE SHIFT CHANGE SUMMARY FROM " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right;padding-right:15px'>Run Date & Time : " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='7' style='text-align: center'>ROSTER</td><td colspan='5' style='text-align: center'>CHANGE</td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 10)
                                    sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=DeptShiftChange.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/SBUShiftChange.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report available in Excel format.');", true);
                    return;
                }
            }
            else if (radTotalAbsent.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {
                    try
                    {
                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("Catagory");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("Designation");
                        dTbl.Columns.Add("NightPresent");
                        dTbl.Columns.Add("HOD");
                        dTbl.Columns.Add("Location");



                        int ct = 0;
                        if (Session["PAYCODE"].ToString().ToString().ToUpper() != "ADMIN" && Session["usertype"].ToString().ToString().ToUpper() != "A")
                        {
                            strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                            dsEmpCount = con.FillDataSet(strsql);
                            if (dsEmpCount.Tables[0].Rows.Count == 0)
                            {
                                return;
                            }
                            else
                            {
                                for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                                {
                                    EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                                }
                                if (!string.IsNullOrEmpty(EmpCodes))
                                {
                                    EmpCodes = EmpCodes.Substring(1);
                                }
                            }
                            strsql = " select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tbldepartment.DEPARTMENTName 'Department',tblcatagory.CATagoryname 'Catagory', " +
                                " Headid=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid),Designation, " +
                                " convert(decimal(10,2),sum(tbltimeregister.absentvalue))'Absent',tblemployee.companycode from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode " +
                                " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT   " +
                                " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                                " where tblemployee.paycode in (" + EmpCodes.ToString() + ") and tblemployee.Active='Y'  and tbltimeregister.dateoffice " +
                                " between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'   and tbltimeregister.absentvalue > 0 " +
                                " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1   " +
                                " group by tbltimeregister.paycode,tblemployee.empname, tbldepartment.DEPARTMENTName,tblcatagory.CATagoryname,tblemployee.paycode,Headid,Designation,tblemployee.companycode order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "  ";
                        }
                        else
                        {
                            strsql = "";
                            strsql = " select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tbldepartment.DEPARTMENTName 'Department',tblcatagory.CATagoryname 'Catagory', " +
                                " Headid=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid),Designation, " +
                               " convert(decimal(10,2),sum(tbltimeregister.absentvalue))'Absent',tblemployee.companycode from tblemployee join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode " +
                               " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT   " +
                               " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                               " where tblemployee.Active='Y'  and tbltimeregister.dateoffice " +
                               " between  '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "'   and absentvalue > 0 " +
                               " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1   ";

                            if (Selection.ToString().Trim() != "")
                            {
                                strsql = strsql + Selection.ToString();
                            }
                            strsql += " group by tbltimeregister.paycode,tblemployee.empname, tbldepartment.DEPARTMENTName,tblcatagory.CATagoryname,tblemployee.paycode,Headid,Designation,tblemployee.companycode order by  " + ddlSorting.SelectedItem.Value.ToString().Trim() + "   ";
                        }
                        ct = 0;
                        //Response.Write(strsql.ToString());
                        DataSet dsResult = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["Paycode"] = "Code";
                        dTbl.Rows[ct]["Catagory"] = "SBU";
                        dTbl.Rows[ct]["Name"] = "Name";
                        dTbl.Rows[ct]["Department"] = "Area";
                        dTbl.Rows[ct]["DESIGNATION"] = "Designation";
                        dTbl.Rows[ct]["NightPresent"] = "Days";
                        dTbl.Rows[ct]["HOD"] = "HOD";
                        dTbl.Rows[ct]["Location"] = "Location";
                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1][0].ToString();
                                dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1][1].ToString();
                                dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1][2].ToString();
                                dTbl.Rows[ct]["Catagory"] = dsResult.Tables[0].Rows[i1][3].ToString();
                                dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1][4].ToString();
                                dTbl.Rows[ct]["NightPresent"] = dsResult.Tables[0].Rows[i1][6].ToString();
                                dTbl.Rows[ct]["Designation"] = dsResult.Tables[0].Rows[i1][5].ToString();
                                dTbl.Rows[ct]["Location"] = dsResult.Tables[0].Rows[i1][7].ToString();
                                ct = ct + 1;
                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j = 0; j < dTbl.Columns.Count; j++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        msg = "Total Absent From " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if ((i == 0) || (i == 7))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        if (Tmp.ToString().Substring(0, 1) == "0")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:50px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                        else
                                        {
                                            sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        }
                                    }
                                }
                                else if (Convert.ToInt32(Tmp.ToString().Length) >= 10)
                                    sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                else
                                    sb.Append("<td style='width:60px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=TotalAbsent.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/TotalAbsent.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch
                    {

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report available in Excel format.');", true);
                    return;
                }
            }
            if (radMonthlyAttendance_New.Checked)
            {
                if (RadExport.SelectedIndex == 0)
                {

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Report Can be Generated only in Excel Format.');", true);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message.ToString());
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
    private void OpenChilePage(string FormToOpen)
    {
        OpenPopupPage = "";
        //OpenPopupPage = "<script language='javascript'>window.open('" + FormToOpen.Trim() + "', 'PopupWindow','location=no,status=no,menubar=no,toolbar=no,width=1000,height=1000');window.moveTo(0,0);</script>";
        OpenPopupPage = "<script language='javascript'>time=window.open('" + FormToOpen.Trim() + "', 'PopupWindow','location=no,status=no,menubar=no,toolbar=no,width=1020,height=800,scrollbars=yes');time.moveTo(0,0);</script>";
        Page.RegisterStartupScript("OpenChild", OpenPopupPage);
    }
    protected void cmdClose_Click1(object sender, EventArgs e)
    {
        Session["SelectionQuery"] = "";
        Session["SelectionQuery"] = null;
        Session["Emp_Codes"] = "";
        Session["Emp_Codes"] = null;
        Session["Com_Selection"] = "";
        Session["Com_Selection"] = null;
        Session["Dept_Selection"] = "";
        Session["Dept_Selection"] = null;
        if (Session["usertype"].ToString() == "A")
        {
            Response.Redirect("dashboard.aspx");
        }
        else
        {
            Response.Redirect("HomePage.aspx");
        }

    }
    protected void txtDateFrom_TextChanged(object sender, EventArgs e)
    {
        try
        {
            FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Invalid Date Format');", true);
            txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
            txtDateFrom.Focus();
            return;
        }
        toDate = FromDate;
        txtToDate.Text = toDate.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
    }
    protected void WriteXL()
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=MusterRoll.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite =
        new HtmlTextWriter(stringWrite);
        GridView1.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        // specified ASP.NET server control at run time.
    }
}