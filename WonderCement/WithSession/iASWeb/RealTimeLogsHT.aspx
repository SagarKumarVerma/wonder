﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="RealTimeLogsHT.aspx.cs" Inherits="RealTimeLogsHT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <script type="text/javascript">
    window.setInterval(function () {
        ASPxCallbackPanelGrd.PerformCallback();
        //ASPxGridView1.reload();
    }, 5000);
 </script>
        <style type="text/css">
            .auto-style1 {
                width: 623px;
            }
        </style>
     <div style="border-style: solid; border-width: thin; border-color: inherit;">
    <table class="dxeBinImgCPnlSys">
  
     
        <tr>
            <td colspan="7">
          
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanelGrd" runat="server" ClientInstanceName="ASPxCallbackPanelGrd" Width="100%">
                    <PanelCollection>
<dx:PanelContent runat="server">
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView1" EnableTheming="True" Font-Size="Small" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared" OnPageIndexChanged="ASPxGridView1_PageIndexChanged" Theme="SoftOrange" Width="100%">
        <SettingsPager NumericButtonCount="100" PageSize="100">
        </SettingsPager>
        <Settings ShowFilterRow="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="User ID" FieldName="UserID" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="0">
                <CellStyle ForeColor="Blue">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Punch Time" FieldName="AttDateTime" ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Device" FieldName="DeviceID" ShowInCustomizationForm="True" VisibleIndex="3">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Verify Mode" FieldName="VerifyMode" ShowInCustomizationForm="True" VisibleIndex="4">
                <Settings AllowEllipsisInText="True" />
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Temprature In Celsius" FieldName="CTemp" ReadOnly="True" ShowInCustomizationForm="True" UnboundType="String" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Temprature In Fahrenheit " FieldName="FTemp" ReadOnly="True" ShowInCustomizationForm="True" UnboundType="String" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Mask Status" FieldName="MaskStatus" ShowInCustomizationForm="True" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Is Abnormal" FieldName="IsAbnomal" ShowInCustomizationForm="True" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataBinaryImageColumn Caption="Thermal Image" FieldName="TImage" Visible="false" ShowInCustomizationForm="True" VisibleIndex="10">
                <PropertiesBinaryImage ImageHeight="100px" ImageWidth="100px">
                    <EditingSettings Enabled="True">
                    </EditingSettings>
                </PropertiesBinaryImage>
            </dx:GridViewDataBinaryImageColumn>
            <dx:GridViewDataBinaryImageColumn Caption="Image" FieldName="LImage" ShowInCustomizationForm="True" VisibleIndex="9">
                <PropertiesBinaryImage ImageHeight="100px" ImageWidth="100px">
                    <EditingSettings Enabled="True">
                    </EditingSettings>
                </PropertiesBinaryImage>
            </dx:GridViewDataBinaryImageColumn>
        </Columns>
    </dx:ASPxGridView>
                        </dx:PanelContent>
</PanelCollection>
                </dx:ASPxCallbackPanel>
          
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                <table>
                    <tr>
                       <td style="width:auto;padding:5px">
                            
                <dx:ASPxComboBox ID="ASPxComboBox2" runat="server" AutoPostBack="True" Theme="SoftOrange" SelectedIndex="0">
                    <Items>
                        <dx:ListEditItem Selected="True" Text="Excel" Value="0" ImageUrl="~/Image/Excel.png" />
                        <dx:ListEditItem Text="PDF" Value="1" ImageUrl="~/Image/pdf.png" />
                        <dx:ListEditItem Text="CSV" Value="2" ImageUrl="~/Image/csv.png" />
                    </Items>
                
                </dx:ASPxComboBox>
                        </td>
                       <td style="width:auto;padding:5px">
                             <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Export"
                    Theme="SoftOrange" OnClick="ASPxButton2_Click">
                    <Image IconID="export_export_16x16">
                    </Image>
                </dx:ASPxButton>
                        </td>
                    </tr>
                </table>




            </td>
            <td>

               

                <br />
                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"
                    FileName="Logs" GridViewID="ASPxGridView1">
                </dx:ASPxGridViewExporter>
                <br />


            </td>
        </tr>
    </table>
    </div>
</asp:Content>

