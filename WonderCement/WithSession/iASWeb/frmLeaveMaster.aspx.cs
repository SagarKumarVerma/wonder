﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Text.RegularExpressions;

public partial class frmLeaveMaster : System.Web.UI.Page

{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string DName, DHOD, DEmail;
    bool isNewRecord = false;

    string LeaveField = "";


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            
            BindData();
        }

    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select Leavefield,leavecode,leavedescription,case ShowOnWeb when 'Y' then 'Yes' else 'No' End as 'Show on Web', case IsLeaveAccrual when " +
                         " 'Y' then 'Yes' else 'No' End as 'Is Accrual', case ISOFFINCLUDE when 'Y' then 'Yes' else 'No' end 'Off Include', " +
                         " case ISHOLIDAYINCLUDE when 'Y' then 'Yes' else 'No' end 'Holiday Include',LeaveType  from tblLeaveMaster";
            }
            else
            {
                Strsql = " select Leavefield,leavecode,leavedescription,case ShowOnWeb when 'Y' then 'Yes' else 'No' End as 'Show on Web', case IsLeaveAccrual when " +
                        " 'Y' then 'Yes' else 'No' End as 'Is Accrual', case ISOFFINCLUDE when 'Y' then 'Yes' else 'No' end 'Off Include', " +
                        " case ISHOLIDAYINCLUDE when 'Y' then 'Yes' else 'No' end 'Holiday Include',LeaveType  from tblLeaveMaster Where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdLeave.DataSource = ds.Tables[0];
                grdLeave.DataBind();
            }
        }
        catch (Exception E)
        {
            Error_Occured("BindData", E.Message);
        }


    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        grdLeave.DataBind();
    }
 
    protected void grdLeave_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        grdLeave.PageIndex = pageIndex;
        this.BindData();  
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAddLeave.aspx");
    }
}