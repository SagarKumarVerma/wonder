﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using DevExpress.Data.Filtering;
using BioCloud;

public partial class TemplateManage : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;

    SqlConnection msqlConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            GetDevice();
            GetUser();
            msqlConn = FKWebTools.GetDBPool();


        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetUser();
        ASPxGridView1.DataBind();
        GetDevice();
        ASPxGridView2.DataBind();

    }

    public void GetDevice()
    {
        try
        {
            DataSet DSDev = new DataSet();
            string str = "";
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                str = "select device_id,device_name,GroupID from tbl_fkdevice_status where connected=1 and IsAuthorized='Y' order by device_name ";
            }
            else
            {
                str = "select device_id,device_name,GroupID from tbl_fkdevice_status where  CompanyCode='"+ Session["LoginCompany"].ToString().Trim() +"' and connected=1 and IsAuthorized='Y' order by device_name ";
            }

            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                ASPxGridView2.DataSource = DSDev;
                ASPxGridView2.DataBind();
            }

        }

        catch
        {

        }

    }

    public void GetUser()
    {
        try
        {
            string str = "";
            DataSet DSDev = new DataSet();

            str = " select user_id,tblemployee.empname,case when RIGHT(password,1)='?' then '' else 'Password' end +''+case when card IS null then '' else 'Card' end+''+case when face is null then ''" +
                         " else 'Face' end+''+case when fp_0 is null then '' else 'Finger' end as TemplateType,Device_ID from tbl_realtime_userinfo left join tblemployee on CAST(tblemployee.presentcardno as int)=tbl_realtime_userinfo.user_id    where Device_ID in (select device_id from tbl_fkdevice_status " +
                         " where CompanyCode='"+ Session["LoginCompany"].ToString().Trim()  + "') ";
            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DSDev;
                ASPxGridView1.DataBind();
            }

        }

        catch
        {

        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            string sSql = string.Empty;

            #region Validation


            List<object> KeyUser = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            List<object> KeyDevice = ASPxGridView2.GetSelectedFieldValues(new string[] { ASPxGridView2.KeyFieldName });
            if (KeyUser.Count == 0 || KeyDevice.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device/User')</script>");
                return;
            }


            #endregion

            string sDevId = "";
            string sUserId = "", sUserName = "", sPreviledge = "",FromDevice="";


            if (chkAdmin.Checked)
            {
                sPreviledge = "MANAGER";
            }
            else
            {
                sPreviledge = "USER";
            }

            for (int iU = 0; iU < ASPxGridView1.VisibleRowCount; iU++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(iU) == true)
                {
                    try
                    {
                        sUserId = Convert.ToString(ASPxGridView1.GetRowValues(iU, "user_id").ToString().Trim());
                        FromDevice = Convert.ToString(ASPxGridView1.GetRowValues(iU, "Device_ID").ToString().Trim());
                        msqlConn = FKWebTools.GetDBPool();
                        if (string.IsNullOrEmpty(sUserId))
                        {
                            continue;
                        }
                        for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
                        {
                            if (ASPxGridView2.Selection.IsRowSelected(i) == true)
                            {
                                try
                                {
                                    sDevId = Convert.ToString(ASPxGridView2.GetRowValues(i, "device_id").ToString().Trim());
                                    Userinfo userinfo = getUserInfo(sUserId, FromDevice);
                                    sUserName = userinfo.user_name.ToString().Trim();
                                    if (sUserName.ToString().Trim().Length > 14)
                                    {
                                        sUserName = sUserName.Substring(0, 14);
                                    }
                                    JObject vResultJson = new JObject();
                                    JArray vEnrollDataArrayJson = new JArray();
                                    FKWebCmdTrans cmdTrans = new FKWebCmdTrans();

                                    int index = 1;
                                    vResultJson.Add("user_id", userinfo.user_id);
                                    vResultJson.Add("user_name", sUserName);
                                    vResultJson.Add("user_privilege", sPreviledge);
                                    if (userinfo.photoData != null && userinfo.photoData.Length > 0)
                                        vResultJson.Add("user_photo", FKWebTools.GetBinIndexString(index++));
                                    for (int nIndex = 0; nIndex <= FKWebTools.BACKUP_FP_9; nIndex++)
                                    {
                                        if (userinfo.fpData[nIndex] != null && userinfo.fpData[nIndex].Length > 0)
                                        {
                                            JObject vEnrollDataJson = new JObject();
                                            vEnrollDataJson.Add("backup_number", nIndex);
                                            vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                                            vEnrollDataArrayJson.Add(vEnrollDataJson);
                                        }
                                    }
                                    if (userinfo.pwdData.Length > 0)
                                    {
                                        JObject vEnrollDataJson = new JObject();
                                        vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_PSW);
                                        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                                        vEnrollDataArrayJson.Add(vEnrollDataJson);
                                    }

                                    if (userinfo.cardData.Length > 0)
                                    {
                                        JObject vEnrollDataJson = new JObject();
                                        vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_CARD);
                                        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                                        vEnrollDataArrayJson.Add(vEnrollDataJson);
                                    }
                                    if (userinfo.faceData != null && userinfo.faceData.Length > 0)
                                    {
                                        JObject vEnrollDataJson = new JObject();
                                        vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_FACE);
                                        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                                        vEnrollDataArrayJson.Add(vEnrollDataJson);
                                    }
                                    vResultJson.Add("enroll_data_array", vEnrollDataArrayJson);
                                    string sFinal = vResultJson.ToString();
                                    byte[] binData = new byte[0];
                                    byte[] strParam = new byte[0];
                                    if (userinfo.photoData != null && userinfo.photoData.Length > 0)
                                    {
                                        FKWebTools.AppendBinaryData(ref binData, userinfo.photoData);
                                    }
                                    for (int nIndex = 0; nIndex <= FKWebTools.BACKUP_FP_9; nIndex++)
                                    {

                                        if (userinfo.fpData[nIndex] != null && userinfo.fpData[nIndex].Length > 0)
                                        {
                                            FKWebTools.AppendBinaryData(ref binData, userinfo.fpData[nIndex]);
                                        }

                                    }
                                    if (userinfo.pwdData.Length > 0)
                                    {
                                        byte[] mPwdBin = new byte[0];
                                        cmdTrans.CreateBSCommBufferFromString(userinfo.pwdData, out mPwdBin);
                                        FKWebTools.ConcateByteArray(ref binData, mPwdBin);
                                    }
                                    if (userinfo.cardData.Length > 0)
                                    {
                                        byte[] mCardBin = new byte[0];
                                        cmdTrans.CreateBSCommBufferFromString(userinfo.cardData, out mCardBin);
                                        FKWebTools.ConcateByteArray(ref binData, mCardBin);
                                    }
                                    if (userinfo.faceData != null && userinfo.faceData.Length > 0)
                                    {
                                        FKWebTools.AppendBinaryData(ref binData, userinfo.faceData);
                                    }
                                    cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                                    FKWebTools.ConcateByteArray(ref strParam, binData);
                                    mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "SET_USER_INFO", sDevId, strParam);

                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }



                    }
                    catch
                    {
                        continue;
                    }
                }
            }









            //for (int i = 0; i < KeyUser.Count; i++)
            //{

            //    sUserId = Convert.ToString(KeyUser[i]);

            //    // sUserName = "Ajitesh";
            //    msqlConn = FKWebTools.GetDBPool();
            //    if (string.IsNullOrEmpty(sUserId)) return;
                //for (int DevRec = 0; DevRec < KeyDevice.Count; DevRec++)
                //{
                //    sDevId = Convert.ToString(KeyDevice[DevRec]);
                //    Userinfo userinfo = getUserInfo(sUserId);
                //    sUserName = userinfo.user_name.ToString().Trim();
                //    JObject vResultJson = new JObject();
                //    JArray vEnrollDataArrayJson = new JArray();
                //    FKWebCmdTrans cmdTrans = new FKWebCmdTrans();

                //    int index = 1;
                //    vResultJson.Add("user_id", userinfo.user_id);
                //    vResultJson.Add("user_name", sUserName);
                //    vResultJson.Add("user_privilege", sPreviledge);
                //    if (userinfo.photoData != null && userinfo.photoData.Length > 0)
                //        vResultJson.Add("user_photo", FKWebTools.GetBinIndexString(index++));
                //    for (int nIndex = 0; nIndex <= FKWebTools.BACKUP_FP_9; nIndex++)
                //    {
                //        if (userinfo.fpData[nIndex] != null && userinfo.fpData[nIndex].Length > 0)
                //        {
                //            JObject vEnrollDataJson = new JObject();
                //            vEnrollDataJson.Add("backup_number", nIndex);
                //            vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                //            vEnrollDataArrayJson.Add(vEnrollDataJson);
                //        }
                //    }
                //    if (userinfo.pwdData.Length > 0)
                //    {
                //        JObject vEnrollDataJson = new JObject();
                //        vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_PSW);
                //        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                //        vEnrollDataArrayJson.Add(vEnrollDataJson);
                //    }

                //    if (userinfo.cardData.Length > 0)
                //    {
                //        JObject vEnrollDataJson = new JObject();
                //        vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_CARD);
                //        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                //        vEnrollDataArrayJson.Add(vEnrollDataJson);
                //    }
                //    if (userinfo.faceData != null && userinfo.faceData.Length > 0)
                //    {
                //        JObject vEnrollDataJson = new JObject();
                //        vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_FACE);
                //        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                //        vEnrollDataArrayJson.Add(vEnrollDataJson);
                //    }
                //    vResultJson.Add("enroll_data_array", vEnrollDataArrayJson);
                //    string sFinal = vResultJson.ToString();
                //    byte[] binData = new byte[0];
                //    byte[] strParam = new byte[0];
                //    if (userinfo.photoData != null && userinfo.photoData.Length > 0)
                //    {
                //        FKWebTools.AppendBinaryData(ref binData, userinfo.photoData);
                //    }
                //    for (int nIndex = 0; nIndex <= FKWebTools.BACKUP_FP_9; nIndex++)
                //    {

                //        if (userinfo.fpData[nIndex] != null && userinfo.fpData[nIndex].Length > 0)
                //        {
                //            FKWebTools.AppendBinaryData(ref binData, userinfo.fpData[nIndex]);
                //        }

                //    }
                //    if (userinfo.pwdData.Length > 0)
                //    {
                //        byte[] mPwdBin = new byte[0];
                //        cmdTrans.CreateBSCommBufferFromString(userinfo.pwdData, out mPwdBin);
                //        FKWebTools.ConcateByteArray(ref binData, mPwdBin);
                //    }
                //    if (userinfo.cardData.Length > 0)
                //    {
                //        byte[] mCardBin = new byte[0];
                //        cmdTrans.CreateBSCommBufferFromString(userinfo.cardData, out mCardBin);
                //        FKWebTools.ConcateByteArray(ref binData, mCardBin);
                //    }
                //    if (userinfo.faceData != null && userinfo.faceData.Length > 0)
                //    {
                //        FKWebTools.AppendBinaryData(ref binData, userinfo.faceData);
                //    }
                //    cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                //    FKWebTools.ConcateByteArray(ref strParam, binData);
                //    mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "SET_USER_INFO", sDevId, strParam);
                //}


           // }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Uploaded Successfully')", true);
            return;

        }
        catch
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Upload Failed')", true);
            return;
        }
    }
    private Userinfo getUserInfo(string user_id,string FromDevice)
    {
        Userinfo userinfo = new Userinfo();
        string sSql = "SELECT @user_name=[user_name],@privilige=[privilige],@card=[card],@Password=[Password],@Face=[Face],@photo=[photo],@Fp_0=[Fp_0],@Fp_1=[Fp_1],"
            + "@Fp_2=[Fp_2],@Fp_3=[Fp_3],@Fp_4=[Fp_4],@Fp_5=[Fp_5],@Fp_6=[Fp_6],@Fp_7=[Fp_7],@Fp_8=[Fp_8],@Fp_9=[Fp_9] FROM tbl_realtime_userinfo where user_id = '" + user_id + "' and Device_ID='" + FromDevice + "'";

        sSql = "";
        sSql = "SELECT @user_name=[empname],@privilige=[privilige],@card=[card],@Password=[Password],@Face=[Face],@photo=[photo],@Fp_0=[Fp_0],@Fp_1=[Fp_1],"
            + "@Fp_2=[Fp_2],@Fp_3=[Fp_3],@Fp_4=[Fp_4],@Fp_5=[Fp_5],@Fp_6=[Fp_6],@Fp_7=[Fp_7],@Fp_8=[Fp_8],@Fp_9=[Fp_9] FROM tbl_realtime_userinfo RT left join tblemployee on  CAST(tblemployee.presentcardno as int)=RT.user_id  where " +
               " rt.device_id ='" + FromDevice.Trim() + "' and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'";



        ////sSql = "Select TE.EmpName empname,TRT.privilige,TRT.card,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige " +
        ////       " FROM tbl_realtime_userinfo,TblEmployee  where CAST(tblemployee.presentcardno as int)=tbl_realtime_userinfo.user_id and user_id = '" + user_id + "'  ";

        //sSql = " select RT.*,tblemployee.EMPNAME from tbl_realtime_userinfo RT left join tblemployee on  CAST(tblemployee.presentcardno as int)=RT.user_id  where " +
        //       " rt.device_id ='"+FromDevice.Trim()+"' and tblemployee.COMPANYCODE='"+Session["LoginCompany"].ToString().Trim()+"'";




        SqlCommand sqlCmd = new SqlCommand(sSql, con);

        SqlParameter sql_user_name = new SqlParameter("@user_name", SqlDbType.VarChar);
        sql_user_name.Direction = ParameterDirection.Output;
        sql_user_name.Size = -1;
        sqlCmd.Parameters.Add(sql_user_name);

        SqlParameter sql_privilige = new SqlParameter("@privilige", SqlDbType.VarChar);
        sql_privilige.Direction = ParameterDirection.Output;
        sql_privilige.Size = -1;
        sqlCmd.Parameters.Add(sql_privilige);

        SqlParameter sql_card = new SqlParameter("@card", SqlDbType.VarChar);
        sql_card.Direction = ParameterDirection.Output;
        sql_card.Size = -1;
        sqlCmd.Parameters.Add(sql_card);

        SqlParameter sql_Password = new SqlParameter("@Password", SqlDbType.VarChar);
        sql_Password.Direction = ParameterDirection.Output;
        sql_Password.Size = -1;
        sqlCmd.Parameters.Add(sql_Password);

        SqlParameter sql_Face = new SqlParameter("@Face", SqlDbType.VarBinary);
        sql_Face.Direction = ParameterDirection.Output;
        sql_Face.Size = -1;
        sqlCmd.Parameters.Add(sql_Face);

        SqlParameter sql_photo = new SqlParameter("@photo", SqlDbType.VarBinary);
        sql_photo.Direction = ParameterDirection.Output;
        sql_photo.Size = -1;
        sqlCmd.Parameters.Add(sql_photo);

        SqlParameter sql_Fp_0 = new SqlParameter("@Fp_0", SqlDbType.VarBinary);
        sql_Fp_0.Direction = ParameterDirection.Output;
        sql_Fp_0.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_0);

        SqlParameter sql_Fp_1 = new SqlParameter("@Fp_1", SqlDbType.VarBinary);
        sql_Fp_1.Direction = ParameterDirection.Output;
        sql_Fp_1.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_1);

        SqlParameter sql_Fp_2 = new SqlParameter("@Fp_2", SqlDbType.VarBinary);
        sql_Fp_2.Direction = ParameterDirection.Output;
        sql_Fp_2.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_2);

        SqlParameter sql_Fp_3 = new SqlParameter("@Fp_3", SqlDbType.VarBinary);
        sql_Fp_3.Direction = ParameterDirection.Output;
        sql_Fp_3.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_3);

        SqlParameter sql_Fp_4 = new SqlParameter("@Fp_4", SqlDbType.VarBinary);
        sql_Fp_4.Direction = ParameterDirection.Output;
        sql_Fp_4.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_4);

        SqlParameter sql_Fp_5 = new SqlParameter("@Fp_5", SqlDbType.VarBinary);
        sql_Fp_5.Direction = ParameterDirection.Output;
        sql_Fp_5.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_5);

        SqlParameter sql_Fp_6 = new SqlParameter("@Fp_6", SqlDbType.VarBinary);
        sql_Fp_6.Direction = ParameterDirection.Output;
        sql_Fp_6.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_6);

        SqlParameter sql_Fp_7 = new SqlParameter("@Fp_7", SqlDbType.VarBinary);
        sql_Fp_7.Direction = ParameterDirection.Output;
        sql_Fp_7.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_7);

        SqlParameter sql_Fp_8 = new SqlParameter("@Fp_8", SqlDbType.VarBinary);
        sql_Fp_8.Direction = ParameterDirection.Output;
        sql_Fp_8.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_8);

        SqlParameter sql_Fp_9 = new SqlParameter("@Fp_9", SqlDbType.VarBinary);
        sql_Fp_9.Direction = ParameterDirection.Output;
        sql_Fp_9.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_9);

        FKWebTools.GetDBPool();
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        sqlCmd.ExecuteNonQuery();

        if (con.State != ConnectionState.Closed)
        {
            con.Close();
        }
        userinfo.user_id = user_id;
        if (sql_user_name.Value.ToString().Trim().Length > 14)
        {
            sql_user_name.Value = sql_user_name.Value.ToString().Trim().Substring(0, 13);
        }

        userinfo.user_name = FormatSqlStringData(sql_user_name.Value.ToString().Trim());
        userinfo.user_privilege = FormatSqlStringData(sql_privilige.Value);
        userinfo.pwdData = FormatSqlStringData(sql_Password.Value);
        userinfo.cardData = FormatSqlStringData(sql_card.Value);
        userinfo.photoData = FormatSqlbyteData(sql_photo.Value);
        userinfo.faceData = FormatSqlbyteData(sql_Face.Value);
        userinfo.fpData[0] = FormatSqlbyteData(sql_Fp_0.Value);
        userinfo.fpData[1] = FormatSqlbyteData(sql_Fp_1.Value);
        userinfo.fpData[2] = FormatSqlbyteData(sql_Fp_2.Value);
        userinfo.fpData[3] = FormatSqlbyteData(sql_Fp_3.Value);
        userinfo.fpData[4] = FormatSqlbyteData(sql_Fp_4.Value);
        userinfo.fpData[5] = FormatSqlbyteData(sql_Fp_5.Value);
        userinfo.fpData[6] = FormatSqlbyteData(sql_Fp_6.Value);
        userinfo.fpData[7] = FormatSqlbyteData(sql_Fp_7.Value);
        userinfo.fpData[8] = FormatSqlbyteData(sql_Fp_8.Value);
        userinfo.fpData[9] = FormatSqlbyteData(sql_Fp_9.Value);
        return userinfo;
    }
    private string FormatSqlStringData(object value)
    {
        if (value == null) return string.Empty;
        return value == DBNull.Value ? string.Empty : value.ToString();
    }

    private byte[] FormatSqlbyteData(object value)
    {
        if (value == null) return null;
        return value == DBNull.Value ? null : (byte[])value;
    }

    protected void ASPxGridView1_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView1.PageIndex = pageIndex;
        this.GetUser();
    }

    protected void ASPxGridView2_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView2.PageIndex = pageIndex;
        this.GetDevice();
    }

    protected void ASPxGridView1_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {

        //if (e.DataColumn.Caption == "User Name")
        //{

        //    string UserId = Convert.ToString(e.GetValue("user_id"));

        //    string sSql = "Select Empname from tblemployee where presentcardno='" + UserId + "'";
        //    DataSet Dstemp = new DataSet();
        //    Dstemp = cn.FillDataSet(sSql);
        //    if(Dstemp.Tables[0].Rows.Count>0)
        //    {
        //        e.Cell.Text = Dstemp.Tables[0].Rows[0][0].ToString().Trim();
        //    }
        //    else
        //    {
        //        e.Cell.Text = "NA";
        //    }


        //}

    }

    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        try
        {
            #region Validation


            List<object> KeyUser = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            List<object> KeyDevice = ASPxGridView2.GetSelectedFieldValues(new string[] { ASPxGridView2.KeyFieldName });
            if (KeyUser.Count == 0 || KeyDevice.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device/User')</script>");
                return;
            }


            #endregion


            string sDevId = "";
            string sUserId = "", sUserName = "", sPreviledge = "";




            for (int i = 0; i < KeyUser.Count; i++)
            {

                sUserId = Convert.ToString(KeyUser[i]);


                Session["operation"] = DEL_USER_INFO;
                for (int DevRec = 0; DevRec < KeyDevice.Count; DevRec++)
                {
                    sDevId = Convert.ToString(KeyDevice[DevRec]);
                    FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                    JObject vResultJson = new JObject();
                    vResultJson.Add("user_id", sUserId);
                    string sFinal = vResultJson.ToString();
                    byte[] strParam = new byte[0];
                    cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                    mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "DELETE_USER", sDevId, strParam);

                }
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Deleted Successfully')", true);
            return;
        }
        catch
        {

        }
    }

    protected void ASPxGridView1_Load(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName == "user_id")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                string[] strContainerNumbers = e.Value.ToString().Split(',');
                Session["filter"] = e.Value;
                CriteriaOperator cop = new InOperator("user_id", strContainerNumbers);
                e.Criteria = cop;
            }
            else
            {
                if (Session["filter"] != null)
                    e.Value = Session["filter"].ToString();
            }
        }
    }

    protected void ASPxGridView2_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName == "device_id")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                string[] strContainerNumbers = e.Value.ToString().Split(',');
                Session["filter"] = e.Value;
                CriteriaOperator cop = new InOperator("device_id", strContainerNumbers);
                e.Criteria = cop;
            }
            else
            {
                if (Session["filter"] != null)
                    e.Value = Session["filter"].ToString();
            }
        }
    }
   
}