﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmHoliday.aspx.cs" Inherits="frmHoliday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
       <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }  
    </script>  
       <div style="width:100%">
           <table width="100%">
               <tr>
                   <td>
                       <dx:ASPxGridView ID="GrdHoliday" runat="server" AutoGenerateColumns="False" 
    Width="100%" KeyFieldName="Date" OnRowDeleting="GrdHoliday_RowDeleting" >
                           <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                           <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                           <SettingsPager>
                               <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                           </SettingsPager>
                           <SettingsEditing Mode="PopupEditForm">
                           </SettingsEditing>
                           <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                           <SettingsBehavior ConfirmDelete="True" />
                           <SettingsCommandButton>
                               <ClearFilterButton Text=" ">
                                   <Image IconID="filter_clearfilter_16x16">
                                   </Image>
                               </ClearFilterButton>
                               <NewButton Text=" ">
                                   <Image IconID="actions_add_16x16">
                                   </Image>
                               </NewButton>
                               <UpdateButton ButtonType="Image" RenderMode="Image">
                                   <Image IconID="save_save_16x16office2013">
                                   </Image>
                               </UpdateButton>
                               <CancelButton ButtonType="Image" RenderMode="Image">
                                   <Image IconID="actions_cancel_16x16office2013">
                                   </Image>
                               </CancelButton>
                               <EditButton Text=" ">
                                   <Image IconID="actions_edit_16x16devav">
                                   </Image>
                               </EditButton>
                               <DeleteButton Text=" ">
                                   <Image IconID="actions_trash_16x16">
                                   </Image>
                               </DeleteButton>
                           </SettingsCommandButton>
                           <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                           <SettingsPopup>
                               <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                           </SettingsPopup>
                           <SettingsSearchPanel Visible="True" />
                           <SettingsText PopupEditFormCaption="Holiday" Title="Holiday" />
                           <Columns>
                               <dx:GridViewCommandColumn  Width="40px" ShowClearFilterButton="True" ShowDeleteButton="True" VisibleIndex="0">
                               </dx:GridViewCommandColumn>
                               <dx:GridViewDataTextColumn FieldName="Date" VisibleIndex="1" Caption="Date">
                                   <PropertiesTextEdit MaxLength="3">
                                       <%--<ValidationSettings Display="Dynamic">
                        <RequiredField IsRequired="false" />
                    </ValidationSettings>--%>
                                   </PropertiesTextEdit>
                               </dx:GridViewDataTextColumn>
                               <dx:GridViewDataTextColumn FieldName="Holiday" VisibleIndex="2" Caption="Holiday">
                               </dx:GridViewDataTextColumn>
                               <dx:GridViewDataTextColumn FieldName="COMPANYCODE" VisibleIndex="3" Caption="Company Code">
                               </dx:GridViewDataTextColumn>
                               <dx:GridViewDataTextColumn FieldName="DEPARTMENTCODE" VisibleIndex="4" Caption="Department Code">
                               </dx:GridViewDataTextColumn>
                           </Columns>
                           <Paddings Padding="0px" />
                           <Border BorderWidth="0px" />
                           <BorderBottom BorderWidth="1px" />
                       </dx:ASPxGridView>
                   </td>
               </tr>
               <tr>
                   <td>
                       <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click">
                       </dx:ASPxButton>
                   </td>
               </tr>
           </table>
       </div>

</asp:Content>

