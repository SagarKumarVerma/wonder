﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Data.SqlClient;
public partial class frmEmployeeGroup : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string DName, DHOD, DEmail;
    bool isNewRecord = false;

    //public class GrpData
    //{
    //    public string GroupId; public string GroupName; public string Branch; public string SHIFT; public string SHIFTTYPE; public string SHIFTPATTERN;
    //    public string SHIFTREMAINDAYS; public string LASTSHIFTPERFORMED; public string ISAUTOSHIFT; public string AUTH_SHIFTS; public string FIRSTOFFDAY; 
    //    public string SECONDOFFTYPE; public string HALFDAYSHIFT; public string SECONDOFFDAY; public string ALTERNATE_OFF_DAYS; public string INONLY; 
    //    public string ISPUNCHALL; public string ISOUTWORK; public string TWO; public string ISTIMELOSSALLOWED; public string CDAYS; public string ISROUNDTHECLOCKWORK; 
    //    public string ISOT; public string OTRATE; public string PERMISLATEARRIVAL; public string PERMISEARLYDEPRT; public string MAXDAYMIN; public string ISOS; 
    //    public string TIME; public string SHORT; public string HALF; public string ISHALFDAY; public string ISSHORT; public string LateVerification; 
    //    public string EveryInterval; public string DeductFrom; public string FromLeave; public string LateDays; public string DeductDay; public string MaxLaeDur; 
    //    public string PunchRequiredInDay; public string SinglePunchOnly; public string LastModifiedBy; public string LastModifiedDate; public string MARKMISSASHALFDAY; 
    //    public string Id; public string PERMISLATEARR; public string PERMISEARLYDEP; public string DUPLICATECHECKMIN; public string ISOVERSTAY;
    //    public string S_END; public string S_OUT; public string ISOTOUTMINUSSHIFTENDTIME; public string ISOTWRKGHRSMINUSSHIFTHRS;
    //    public string ISOTEARLYCOMEPLUSLATEDEP; public string ISOTALL; public string ISOTEARLYCOMING; public string OTEARLYDUR; public string OTLATECOMINGDUR; 
    //    public string OTRESTRICTENDDUR; public string OTEARLYDEPARTUREDUR; public string DEDUCTWOOT; public string DEDUCTHOLIDAYOT; public string ISPRESENTONWOPRESENT;
    //    public string ISPRESENTONHLDPRESENT; public string ISAUTOABSENT; public string AUTOSHIFT_LOW; public string AUTOSHIFT_UP; public string WOINCLUDE;
    //    public string NightShiftFourPunch; public string OTROUND; public string PREWO; public string ISAWA; public string ISPREWO; public string LeaveFinancialYear; 
    //    public string MIS; public string OwMinus; public string MSHIFT; public string MISCOMPOFF; public string ISCOMPOFF; public string ISAW; public string ISWA; 
    //    public string ISAWP; public string ISPWA;
    //}

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["ComVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
           // BindData();
        }

    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        Strsql = "SELECT [GroupId],[GroupName],[Branch],[SHIFT],[SHIFTTYPE],[SHIFTPATTERN],[SHIFTREMAINDAYS],[LASTSHIFTPERFORMED],[ISAUTOSHIFT],[AUTH_SHIFTS],[FIRSTOFFDAY],[SECONDOFFTYPE],[HALFDAYSHIFT] " +
      ",[SECONDOFFDAY],[ALTERNATE_OFF_DAYS],[INONLY],[ISPUNCHALL],[ISOUTWORK],[TWO],[ISTIMELOSSALLOWED],[CDAYS],[ISROUNDTHECLOCKWORK],[ISOT],[OTRATE],[PERMISLATEARRIVAL],[PERMISEARLYDEPRT],[MAXDAYMIN] " +
      ",[ISOS],[TIME],[SHORT],[HALF],[ISHALFDAY],[ISSHORT],[LateVerification],[EveryInterval],[DeductFrom],[FromLeave],[LateDays],[DeductDay],[MaxLateDur],[PunchRequiredInDay],[SinglePunchOnly] " +
      ",[LastModifiedBy],[LastModifiedDate],[MARKMISSASHALFDAY],[Id],[PERMISLATEARR],[PERMISEARLYDEP],[DUPLICATECHECKMIN],[ISOVERSTAY],[S_END],[S_OUT],[ISOTOUTMINUSSHIFTENDTIME],[ISOTWRKGHRSMINUSSHIFTHRS] " +
      " ,[ISOTEARLYCOMEPLUSLATEDEP],[ISOTALL],[ISOTEARLYCOMING],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],[OTEARLYDEPARTUREDUR],[DEDUCTWOOT],[DEDUCTHOLIDAYOT],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT] " +
      ",[ISAUTOABSENT],[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[WOINCLUDE],[NightShiftFourPunch],[OTROUND],[PREWO],[ISAWA],[ISPREWO],[LeaveFinancialYear],[MIS],[OwMinus],[MSHIFT],[MISCOMPOFF],[ISCOMPOFF] " +
      ",[ISAW],[ISWA],[ISAWP],[ISPWA] FROM [dbo].[EmployeeGroup] order by 1";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GrdGrp.DataSource = ds.Tables[0];
            GrdGrp.DataBind();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdGrp.DataBind();
    }

    //[System.Web.Services.WebMethod()]
    //[System.Web.Script.Services.ScriptMethod()]
    //public static string LoadGrp(string GrpId)
    //{
    //    string GrpDataStr;
    //    SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
    //    SqlDataAdapter adap = new SqlDataAdapter();
    //    DataSet ds = new DataSet();
    //    string sSql = "select * from EmployeeGroup where shift='" + GrpId + "' ";
    //    adap = new SqlDataAdapter(sSql, Conn);
    //    adap.Fill(ds);
    //    GrpData LS = new GrpData();

    //    LS.GroupId = GrpId;
    //    LS.GroupName = ds.Tables[0].Rows[0]["GroupName"].ToString().Trim();
    //    LS.Branch = ds.Tables[0].Rows[0]["Branch"].ToString().Trim();
    //    LS.SHIFT = ds.Tables[0].Rows[0]["SHIFT"].ToString().Trim();
    //    LS.SHIFTTYPE = ds.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim();
    //    LS.SHIFTPATTERN = ds.Tables[0].Rows[0]["SHIFTPATTERN"].ToString().Trim();
    //    LS.SHIFTREMAINDAYS = ds.Tables[0].Rows[0]["SHIFTREMAINDAYS"].ToString().Trim();
    //    LS.LASTSHIFTPERFORMED = ds.Tables[0].Rows[0]["LASTSHIFTPERFORMED"].ToString().Trim();
    //    LS.ISAUTOSHIFT = ds.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();
    //    LS.AUTH_SHIFTS = ds.Tables[0].Rows[0]["AUTH_SHIFTS"].ToString().Trim();
    //    LS.INONLY = ds.Tables[0].Rows[0]["INONLY"].ToString().Trim();
    //    LS.ISPUNCHALL = ds.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
    //    LS.ISOUTWORK = ds.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();
    //    LS.TWO = ds.Tables[0].Rows[0]["TWO"].ToString().Trim();
    //    LS.ISTIMELOSSALLOWED = ds.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
    //    LS.CDAYS = ds.Tables[0].Rows[0]["CDAYS"].ToString().Trim();
    //    LS.ISROUNDTHECLOCKWORK = ds.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim(); 
    //    LS.ISOT = ds.Tables[0].Rows[0]["ISOT"].ToString().Trim();
    //    LS.OTRATE = ds.Tables[0].Rows[0]["OTRATE"].ToString().Trim();
    //    LS.PERMISLATEARRIVAL = ds.Tables[0].Rows[0]["PERMISLATEARRIVAL"].ToString().Trim();
    //    LS.ISHALFDAY = ds.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
    //    LS.ISSHORT = ds.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
    //    LS.LateVerification = ds.Tables[0].Rows[0]["LateVerification"].ToString().Trim();
    //    LS.EveryInterval = ds.Tables[0].Rows[0]["EveryInterval"].ToString().Trim(); 
    //    LS.DeductFrom = ds.Tables[0].Rows[0]["DeductFrom"].ToString().Trim();
    //    LS.SinglePunchOnly = ds.Tables[0].Rows[0]["SinglePunchOnly"].ToString().Trim();
    //    LS.MARKMISSASHALFDAY = ds.Tables[0].Rows[0]["MARKMISSASHALFDAY"].ToString().Trim();
    //    LS.Id = ds.Tables[0].Rows[0]["Id"].ToString().Trim();
    //    LS.PERMISLATEARR = ds.Tables[0].Rows[0]["PERMISLATEARR"].ToString().Trim();
    //    LS.PERMISEARLYDEP = ds.Tables[0].Rows[0]["PERMISEARLYDEP"].ToString().Trim();
    //    LS.DUPLICATECHECKMIN = ds.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim();
    //    LS.ISOVERSTAY = ds.Tables[0].Rows[0]["ISOVERSTAY"].ToString().Trim();
    //    LS.S_END = ds.Tables[0].Rows[0]["S_END"].ToString().Trim();
    //    LS.S_OUT = ds.Tables[0].Rows[0]["S_OUT"].ToString().Trim(); 
    //    LS.ISOTOUTMINUSSHIFTENDTIME = ds.Tables[0].Rows[0]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
    //    LS.ISOTWRKGHRSMINUSSHIFTHRS = ds.Tables[0].Rows[0]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
    //    LS.ISOTEARLYCOMEPLUSLATEDEP = ds.Tables[0].Rows[0]["ISOTEARLYCOMEPLUSLATEDEP"].ToString().Trim();
    //    LS.ISOTALL = ds.Tables[0].Rows[0]["ISOTALL"].ToString().Trim();
    //    LS.ISOTEARLYCOMING = ds.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
    //    LS.OTEARLYDUR = ds.Tables[0].Rows[0]["OTEARLYDUR"].ToString().Trim();
    //    LS.OTLATECOMINGDUR = ds.Tables[0].Rows[0]["OTLATECOMINGDUR"].ToString().Trim();
    //    LS.OTRESTRICTENDDUR = ds.Tables[0].Rows[0]["OTRESTRICTENDDUR"].ToString().Trim();
    //    LS.OTEARLYDEPARTUREDUR = ds.Tables[0].Rows[0]["OTEARLYDEPARTUREDUR"].ToString().Trim();
    //    LS.DEDUCTWOOT = ds.Tables[0].Rows[0]["DEDUCTWOOT"].ToString().Trim(); 
    //    LS.DEDUCTHOLIDAYOT = ds.Tables[0].Rows[0]["DEDUCTHOLIDAYOT"].ToString().Trim();
    //    LS.ISPRESENTONWOPRESENT = ds.Tables[0].Rows[0]["ISPRESENTONWOPRESENT"].ToString().Trim();
    //    LS.ISPRESENTONHLDPRESENT = ds.Tables[0].Rows[0]["ISPRESENTONHLDPRESENT"].ToString().Trim();
    //    LS.ISAUTOABSENT = ds.Tables[0].Rows[0]["ISAUTOABSENT"].ToString().Trim();
    //    LS.AUTOSHIFT_LOW = ds.Tables[0].Rows[0]["AUTOSHIFT_LOW"].ToString().Trim();
    //    LS.AUTOSHIFT_UP = ds.Tables[0].Rows[0]["AUTOSHIFT_UP"].ToString().Trim();
    //    LS.WOINCLUDE = ds.Tables[0].Rows[0]["WOINCLUDE"].ToString().Trim();
    //    LS.NightShiftFourPunch = ds.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
    //    LS.OTROUND = ds.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
    //    LS.PREWO = ds.Tables[0].Rows[0]["PREWO"].ToString().Trim(); 
    //    LS.ISAWA = ds.Tables[0].Rows[0]["ISAWA"].ToString().Trim();
    //    LS.ISPREWO = ds.Tables[0].Rows[0]["ISPREWO"].ToString().Trim();
    //    LS.LeaveFinancialYear = ds.Tables[0].Rows[0]["LeaveFinancialYear"].ToString().Trim();
    //    LS.MIS = ds.Tables[0].Rows[0]["MIS"].ToString().Trim();
    //    LS.OwMinus = ds.Tables[0].Rows[0]["OwMinus"].ToString().Trim();
    //    LS.MSHIFT = ds.Tables[0].Rows[0]["MSHIFT"].ToString().Trim();
    //    LS.MISCOMPOFF = ds.Tables[0].Rows[0]["MISCOMPOFF"].ToString().Trim();
    //    LS.ISCOMPOFF = ds.Tables[0].Rows[0]["ISCOMPOFF"].ToString().Trim();
    //    LS.ISAW = ds.Tables[0].Rows[0]["ISAW"].ToString().Trim();
    //    LS.ISWA = ds.Tables[0].Rows[0]["ISWA"].ToString().Trim(); 
    //    LS.ISAWP = ds.Tables[0].Rows[0]["ISAWP"].ToString().Trim();
    //    LS.ISPWA = ds.Tables[0].Rows[0]["ISPWA"].ToString().Trim();
        
    //    GrpDataStr = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(LS);
    //    return GrpDataStr; 
    //}
}