using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GlobalSettings;

    public partial class MainMaster : System.Web.UI.MasterPage
    {
        Class_Connection ConM = new Class_Connection();
        bool MasterCheck = false;
//ASPxNavBar1
        protected void Page_Load(object sender, EventArgs e)
        {
            //HIDLoginCompany.Value = Session["LoginCompany"].ToString().Trim();
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("Login.aspx");
                }

                BindMenu();
            }
            catch
            {
                
               
            }
        }
  
      private void BindMenu()
        {
            Class_Connection Con = new Class_Connection();
            string Strsql;
            DataSet ds;

            DataSet UserDs = new DataSet();
            UserDs = (DataSet)Session["UserDs"];
            if (UserDs == null)
                return;
            if (Session["usertype"].ToString().Trim() == "U")
            {
                ASPxNavBar1.Groups[1].Visible = false;
                ASPxNavBar1.Groups[2].Visible = false;
                ASPxNavBar1.Groups[3].Visible = false;
                ASPxNavBar1.Groups[4].Visible = false;
                ASPxNavBar1.Groups[5].Visible = false;
                ASPxNavBar1.Groups[6].Visible = false;
                ASPxNavBar1.Groups[7].Visible = false;
                ASPxNavBar1.Groups[0].Items[0].Visible = true;
                ASPxNavBar1.Groups[0].Items[1].Visible = true;
                ASPxNavBar1.Groups[0].Items[2].Visible = false;
                ASPxNavBar1.Groups[0].Items[3].Visible = false;
                ASPxNavBar1.Groups[0].Items[4].Visible = true;
            }
            else if (Session["usertype"].ToString().Trim() == "H")
            {
                ASPxNavBar1.Groups[0].Items[0].Visible = true;
                ASPxNavBar1.Groups[0].Items[1].Visible = true;
                ASPxNavBar1.Groups[0].Items[2].Visible = true;
                ASPxNavBar1.Groups[0].Items[3].Visible = true;
                ASPxNavBar1.Groups[0].Items[4].Visible = true;
            }
            else
            {
                ASPxNavBar1.Groups[0].Items[0].Visible = true;
                ASPxNavBar1.Groups[0].Items[1].Visible = true;
                ASPxNavBar1.Groups[0].Items[2].Visible = true;
                ASPxNavBar1.Groups[0].Items[3].Visible = true;
                ASPxNavBar1.Groups[0].Items[4].Visible = false;
            }
          //For Main
            if (UserDs.Tables[0].Rows[0]["Main"].ToString() == "Y")
            {
                ASPxNavBar1.Groups[1].Visible = true;
                if (UserDs.Tables[0].Rows[0]["Company"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[0].Visible = true;
                    Session["ComVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[0].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["Department"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[1].Visible = true;
                    Session["DeptVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[1].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["Section"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[2].Visible = true;
                    Session["SectionVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[2].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["Grade"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[3].Visible = true;
                    Session["GradeVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[3].Visible = false;
                }

                if (UserDs.Tables[0].Rows[0]["Category"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[4].Visible = true;
                    Session["CategoryVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[4].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["Shift"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[5].Visible = true;
                    Session["ShiftVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[5].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["Visitor"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[6].Visible = true;
                    Session["EmployeeGrpVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[6].Visible = false;
                }

                if (UserDs.Tables[0].Rows[0]["Employee"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[7].Visible = true;
                    Session["EmployeeVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[7].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["ManualUpload"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[1].Items[8].Visible = true;
                    Session["EmployeeUploadVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[1].Items[8].Visible = false;
                }
            }
            else
            {
                ASPxNavBar1.Groups[1].Visible = false;
            }
            if (UserDs.Tables[0].Rows[0]["v_Transaction"].ToString() == "Y")
            {
                ASPxNavBar1.Groups[2].Visible = true;
                if (UserDs.Tables[0].Rows[0]["Manual_Attendance"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[2].Items[0].Visible = true;
                    Session["Manual_AttendanceVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[2].Items[0].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["Holiday"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[2].Items[2].Visible = true;
                    Session["HolidayVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[2].Items[2].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["ShiftChange"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[2].Items[1].Visible = false;
                    Session["ShiftChangeVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[2].Items[1].Visible = false;
                }
            }
            else
            {
                ASPxNavBar1.Groups[2].Visible = false;
            }

            if (UserDs.Tables[0].Rows[0]["DataProcess"].ToString() == "Y")
            {
                ASPxNavBar1.Groups[3].Visible = true;
                ASPxNavBar1.Groups[3].Items[3].Visible = false;
                if (UserDs.Tables[0].Rows[0]["RegisterCreation"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[3].Items[0].Visible = true;
                    Session["RegisterCreationVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[3].Items[0].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["RegisterUpdation"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[3].Items[1].Visible = true;
                    Session["RegisterUpdationVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[3].Items[1].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["BackDateProcess"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[3].Items[2].Visible = true;
                    Session["BackDateProcessVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[3].Items[2].Visible = false;
                }
            }
            else
            {
                ASPxNavBar1.Groups[3].Visible = false;
            }
            if (UserDs.Tables[0].Rows[0]["Leave"].ToString() == "Y")
            {
                ASPxNavBar1.Groups[4].Visible = true;
                if (UserDs.Tables[0].Rows[0]["LeaveMaster"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[4].Items[0].Visible = true;
                    Session["LeaveMasterVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[4].Items[0].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["LeaveAccural"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[4].Items[1].Visible = true;
                    Session["LeaveAccuralVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[4].Items[1].Visible = false;
                }
               


                //if (UserDs.Tables[0].Rows[0]["LeaveApplication"].ToString() == "Y")
                //{
                //    ASPxNavBar1.Groups[4].Items[2].Visible = true;
                //    Session["LeaveApplicationVisible"] = "Y";
                //}
                //else
                //{
                //    ASPxNavBar1.Groups[4].Items[2].Visible = false;
                //}
                //if (UserDs.Tables[0].Rows[0]["usertype"].ToString() == "A" || UserDs.Tables[0].Rows[0]["usertype"].ToString() == "H" || UserDs.Tables[0].Rows[0]["usertype"].ToString() == "T")
                //{
                //    ASPxNavBar1.Groups[4].Items[3].Visible = true;
                //}
                //else
                //{
                //    ASPxNavBar1.Groups[4].Items[3].Visible = false;
                //}
                if (UserDs.Tables[0].Rows[0]["LeaveAccuralAuto"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[4].Items[2].Visible = true;
                    Session["LeaveAccuralAutoVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[4].Items[2].Visible = false;
                }
                if (UserDs.Tables[0].Rows[0]["usertype"].ToString() == "A")
                {
                    ASPxNavBar1.Groups[4].Items[3].Visible = true;
                }
                else
                {
                    ASPxNavBar1.Groups[4].Items[3].Visible = false;
                }
                //if (UserDs.Tables[0].Rows[0]["usertype"].ToString() == "A")
                //{
                //    ASPxNavBar1.Groups[4].Items[5].Visible = false;
                //}
                //{
                //    ASPxNavBar1.Groups[4].Items[5].Visible = false;
                //}

            }
            else
            {
                ASPxNavBar1.Groups[4].Visible = false;
            }
            if (UserDs.Tables[0].Rows[0]["usertype"].ToString() == "A")
            {
                ASPxNavBar1.Groups[5].Visible = true;
                if (UserDs.Tables[0].Rows[0]["UserPrevilege"].ToString() == "Y")
                {
                    ASPxNavBar1.Groups[5].Items[4].Visible = true;
                    Session["UserPrevilegeVisible"] = "Y";
                }
                else
                {
                    ASPxNavBar1.Groups[5].Items[4].Visible = false;
                }
                ASPxNavBar1.Groups[5].Items[3].Visible = true;
                ASPxNavBar1.Groups[5].Items[6].Visible = true;

                ASPxNavBar1.Groups[5].Items[0].Visible = false;
                ASPxNavBar1.Groups[5].Items[1].Visible = false;
                ASPxNavBar1.Groups[5].Items[2].Visible = false;
                ASPxNavBar1.Groups[5].Items[5].Visible = false;

            }
            else

            {
                ASPxNavBar1.Groups[5].Visible = false;
            }
            if (UserDs.Tables[0].Rows[0]["usertype"].ToString() == "A")
            {
                ASPxNavBar1.Groups[6].Visible = true;
            }
            else
            {
                ASPxNavBar1.Groups[6].Visible = false;
            }
            if (UserDs.Tables[0].Rows[0]["Reports"].ToString() == "Y")
            {
                ASPxNavBar1.Groups[7].Visible = true;
                //if (UserDs.Tables[0].Rows[0]["TimeOfficeReport"].ToString() == "Y")
                //{

                //}
                //else
                //{

                //}
            }
            else
            {
                ASPxNavBar1.Groups[7].Visible = false;
            }


        }

      protected void ASPxNavBar1_HeaderClick(object source, DevExpress.Web.NavBarGroupCancelEventArgs e)
      {
          if (e.Group.Text == "LogOut")
          {
              Session.Clear();
              Session.Abandon();
              Response.Redirect("Login.aspx");
          }
      }
      protected void ASPxNavBar1_ItemClick(object source, DevExpress.Web.NavBarItemEventArgs e)
      {

      }
}