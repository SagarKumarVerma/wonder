﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v17.1.Web, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v17.1.Web, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web.Designer" TagPrefix="dxchartdesigner" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.XtraCharts.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <script type="text/javascript">
    window.setInterval(function () {
        ASPxCallbackPanelGrd.PerformCallback();
        //ASPxGridView1.reload();
    }, 10000);
 </script>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content=""><title>SB Admin 2 - Cards</title>

  <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
        <link href="Content/sb-admin-2.min.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <div id="wrapper">
            <div id="content-wrapper" class="d-flex flex-column">
                <div id="content">

        <dx:ASPxCallbackPanel ID="ASPxCallbackPanelGrd" runat="server" ClientInstanceName="ASPxCallbackPanelGrd" Width="100%">
                    <PanelCollection>
<dx:PanelContent runat="server">

        <!-- Begin Page Content -->
                    <div class="container-fluid">

          <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">Dashboard </h1>
                            <%-- <dx:ASPxComboBox ID="ASPxComboBox1" AutoPostBack="true" runat="server">
                <ClientSideEvents ValueChanged="function(s, e) { ASPxClientUtils.SetCookie('theme', s.GetText()); }" />
            </dx:ASPxComboBox>--%>
                        </div>
                        <div class="row">
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Total Employee</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblTotal" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Present</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblPresent" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <%--<div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>--%>
                                              <div class="col">
                                                  <div class="progress progress-sm mr-2">
                                                      <div id="DivPer" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                      </div>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                    Absent</div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                            <dx:ASPxLabel ID="lblAbsent" runat="server" Text="0" Theme="Aqua" Font-Bold="true"   Font-Size="X-Large" Cursor="pointer">
                                                            </dx:ASPxLabel>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div id="DivAbs" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                   Late</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblLate" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>

                                            </div>
                                             <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div id="divLate" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Week Off</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblOff" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                             <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div id="DivOff" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Leave</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblLeave" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                             <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div id="DivLeave" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                    Holiday</div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                            <dx:ASPxLabel ID="lblHoliday" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                            </dx:ASPxLabel>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div id="DivHoliday" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                    Pending Leave Requests</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblPendingReq" runat="server" Text="0" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Marked Punched</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblPunch" runat="server"  Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    With Mask</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblMask" runat="server"  Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <%--<div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>--%>
                                              <div class="col">
                                                  <div class="progress progress-sm mr-2">
                                                      <div id="Div1" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                      </div>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                    Without Mask</div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                                            <dx:ASPxLabel ID="lblWMask" runat="server"   Theme="Aqua" Font-Bold="true"   Font-Size="X-Large" Cursor="pointer">
                                                            </dx:ASPxLabel>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div id="Div2" runat="server" class="progress-bar bg-info" role="progressbar" style="width: 9%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                  <%--          <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-warning shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                                   Visitor</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <dx:ASPxLabel ID="lblVisitor" runat="server" Theme="Aqua" Font-Bold="true" Font-Size="X-Large">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">

                                <!-- Default Card Example -->
                                <div class="card mb-4">
                                    <div class="card-header">
                                        Shift Summary
                                    </div>
                                    <div class="card-body">
                                         <dx:ASPxGridView ID="GrdHC"  Width="100%" Theme="SoftOrange" Visible="false"  runat="server"></dx:ASPxGridView>
                                       
                                    </div>
                                </div>

                                <!-- Basic Card Example -->
                                <div class="card-header">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"> </h6>
                                    </div>
                                    <div class="card-body">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                         
                                    </div>
                                    <div class="card-body">
                                         
                                    </div>
                                </div>

                                <!-- Collapsable Card Example -->
                                <%--<div class="card shadow mb-4">
                                    <!-- Card Header - Accordion -->
                                    <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                                        <h6 class="m-0 font-weight-bold text-primary"></h6>
                                    </a>
                                    <!-- Card Content - Collapse -->
                                    <div class="collapse show" id="collapseCardExample">
                                        <div class="card-body">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="card-body">
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
        <!-- /.container-fluid -->
       </dx:PanelContent>
</PanelCollection>
                </dx:ASPxCallbackPanel>
                </div>
            </div>
        </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>

  <!-- Logout Modal-->
      <%--  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">
                            Cancel
                        </button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>--%>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

    </body>

</asp:Content>

