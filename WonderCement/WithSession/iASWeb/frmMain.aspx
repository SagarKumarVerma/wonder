﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmMain.aspx.cs" Inherits="frmMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div style="padding:25px">
        <center>
            <asp:Calendar ID="Calendar1" runat="server" 
            BackColor="White" 
            BorderColor="Black" 
            Font-Names="Segoe UI"     
            Font-Size="10pt"   
            ForeColor="Black"
            NextMonthText="Next»"   
            PrevMonthText="«Prev"  
            SelectMonthText="»"   
            SelectWeekText="›"  
            OnDayRender="Calendar1_DayRender" OnSelectionChanged="Calendar1_SelectionChanged"
            OnVisibleMonthChanged="Calendar1_VisibleMonthChanged" Height="400px" 
            Width="800px" NextPrevFormat="FullMonth" DayNameFormat="Shortest" TitleFormat="Month">
                <SelectedDayStyle BackColor="#CC3333" ForeColor="White" />
                <TodayDayStyle BackColor="#CCCC99" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <DayStyle Width="14%" />
                <NextPrevStyle Font-Size="8pt" ForeColor="White" />
                <DayHeaderStyle Font-Bold="True" Height="10pt" Font-Size="7pt" ForeColor="#333333" BackColor="#CCCCCC" />
                <SelectorStyle BackColor="#CCCCCC" Font-Bold="True" Font-Names="Verdana" Font-Size="8pt" ForeColor="#333333" Width="1%" />
                <TitleStyle BackColor="Black" Font-Bold="True" Font-Size="13pt" ForeColor="White" 
                                 Height="14pt" />
            </asp:Calendar>
        </center>
    </div>
</asp:Content>

