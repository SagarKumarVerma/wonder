﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Data.OleDb;

public partial class Holiday : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    DataSet ds = null;
    string Strsql = null;
    Class_Connection Con = new Class_Connection();
    string C_Code = null;
    string D_Code = null;
    string Msg;
    ArrayList lasset = new ArrayList();
    ArrayList lsubordinate = new ArrayList();
    static ArrayList UpdateList = new ArrayList();
    int result = 0;
    int result1 = 0;
    string OtFactor = "";
    string CompanyCode = null;
    string DeptCode = null;
    DateTime Hdate, AdjustedDate;
    ArrayList lasset1 = new ArrayList();
    ArrayList lsubordinate1 = new ArrayList();
    static ArrayList UpdateList1 = new ArrayList();



    string btnGoBack = "";
    string resMsg = "";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
        if (ex is HttpRequestValidationException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if ( Session["UserName"] == null)
        {
            Session.Abandon();
            Response.Redirect("Login.aspx");
        }
        else
        {
           
        }
        if (Session["HolidayVisible"] == null)
        {
            Response.Redirect("PageNotFound.aspx");
        }
        txtDate.Focus();
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            bindCompany();
            bindDept();
           
        }
    }
    protected void bindCompany()
    {
        Strsql = "Select companycode,companycode+'-'+companyname as 'compDetails' from tblCompany where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GridLookup.DataSource = ds.Tables[0];

            GridLookup.DataBind();
        }
    }

    protected void bindDept()
    {
        Strsql = "select departmentcode,departmentcode+' - '+departmentname as 'deptDetails' from tbldepartment where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GDep.DataSource = ds.Tables[0];

            GDep.DataBind();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
       
        bindCompany();
        bindDept();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        lblStatus.Text = "";
        try
        {
            Hdate = DateTime.ParseExact(txtDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch
        {
            //Msg = "<script language='javascript'>alert('Invalid Date')</script>";
            ////Page.RegisterStartupScript("Msg", Msg);
            lblStatus.Text = "Invalid Date";
            return;
        }
     
        try
        {
            if (txtDateAdjusted.Text.ToString().Trim() != "")
            {
                AdjustedDate = DateTime.ParseExact(txtDateAdjusted.Text.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }
        catch
        {
            //Msg = "<script language='javascript'>alert('Invalid Adjust Date')</script>";
            //Page.RegisterStartupScript("Msg", Msg);
            lblStatus.Text = "Invalid Adjust Date";
            return;
            return;
        }
        string Hdate1 = txtDate.Text.ToString().Trim();
        string[] hdate1 = Hdate1.Split('/');
        Hdate1 = hdate1[2].ToString();
        DataSet dsRe = new DataSet();
        Strsql = "select  distinct datename(yyyy,dateoffice) 'HDate' from tbltimeregister";

        dsRe = Con.FillDataSet(Strsql);
        bool found = false;
        if (dsRe.Tables[0].Rows.Count > 0)
        {
            for (int K = 0; K < dsRe.Tables[0].Rows.Count; K++)
            {
                if (dsRe.Tables[0].Rows[K]["HDate"].ToString() == Hdate1)
                {
                    found = true;
                }
            }
        }
        if (!found)
        {
            //Msg = "<script language='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
            //Page.RegisterStartupScript("Msg", Msg);
            lblStatus.Text = "Invalid Year. Roster Not Created For The Specified Year";
            return;
        }
        if (txtOT.Text.ToString().Trim() == "" )
            OtFactor = "0";
        else
            OtFactor = txtOT.Text.ToString().Trim();

        //if no company selected
        if (GridLookup.Text.Length == 0 || GridLookup.Text.ToString().Trim() == "")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Select Company');", true);
            lblStatus.Text = "Select Company";
            GridLookup.Focus();
            return;
        }
        if (GDep.Text.Length == 0 || GDep.Text.ToString().Trim()=="")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Select Department');", true);
            lblStatus.Text = "Select Department";
            GDep.Focus();
            return;
        }

        try
        {
            DataSet DsHoliday = new DataSet();
            string Reason = "";
            int result = 0;
            string [] SelectedComp = GridLookup.Value.ToString().Trim().Split(',');

            string[] SelectedDep = GDep.Text.ToString().Trim().Split(',');
            
            foreach(string ComCode in SelectedComp)
            {
                C_Code = ComCode.ToString().Trim();
                //int count = Convert.ToInt32(listDeptRight.Items.Count);

                foreach (string DepCode in SelectedDep)
                {
                    D_Code = DepCode.Trim();
                    Strsql = "Select * from Holiday WHERE Hdate = '" + Hdate.ToString("yyyy-MM-dd") + "' and COMPANYCODE='" + C_Code + "' AND DEPARTMENTCODE='" + D_Code + "' ";
                    DsHoliday = Con.FillDataSet(Strsql);
                    if (DsHoliday.Tables[0].Rows.Count == 0)
                    {
                        Reason = txtReason.Text.ToString().Trim();
                        if (Reason.Contains("'"))
                        {
                            Reason = Reason.ToString().Replace("'", "''");
                        }
                        Strsql = "insert into holiday (Hdate,Holiday,OT_Factor,CompanyCode,departmentcode,Adjustmentholiday) " +
                                 "values ('" + Hdate.ToString("yyyy-MM-dd") + "','" + Reason.ToString().Trim() + "','" + OtFactor + "', '" + C_Code.ToString() + "','" + D_Code.ToString() + "' ";
                        if (txtDateAdjusted.Text.Trim() == "")
                            Strsql += " ,null ";
                        else
                            Strsql += " ,'" + AdjustedDate.ToString("yyyy-MM-dd") + "' ";

                        Strsql += "  )";
                        result = Con.execute_NonQuery(Strsql);
                        if (result > 0)
                        {
                            //Strsql = "insert into tblFunctionCall (FromDate, ToDate, Companycode, DepartmentCode, FunctionName )  " +
                            //         " Values ('" + Hdate.ToString("yyyy-MM-dd") + "', '" + Hdate.ToString("yyyy-MM-dd") + "','" + C_Code + "','" + D_Code + "', 'AssignHolidays' ) ";
                            //result = Con.execute_NonQuery(Strsql);
                            string SetupID = "";

                            DataSet Rs = new DataSet();
                            string sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
                            Rs = Con.FillDataSet(sSql);
                            if (Rs.Tables[0].Rows.Count > 0)
                            {
                                SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
                            }

                            try
                            {
                                using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                {
                                    MyCmd = new OleDbCommand("ProcessAssignedHolidays", MyConn);
                                    MyCmd.CommandTimeout = 1800;
                                    MyCmd.CommandType = CommandType.StoredProcedure;
                                    MyCmd.Parameters.Add("@MinDate", SqlDbType.VarChar).Value = Hdate.ToString("yyyy-MM-dd");
                                    MyCmd.Parameters.Add("@MaxDate", SqlDbType.VarChar).Value = Hdate.ToString("yyyy-MM-dd");
                                    MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = C_Code;
                                    MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = D_Code;
                                    MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                                    MyConn.Open();
                                    result = MyCmd.ExecuteNonQuery();
                                    MyConn.Close();

                                }
                            }
                            catch 
                            {
                                
                                
                            }

                        }
                        DeptCode += ",'" + D_Code.ToString() + "'";
                    }
                }
                CompanyCode += ",'" + C_Code.ToString() + "'";
            }
            Msg = "<script language='javascript'>alert('Holiday created...')</script>";
            Page.RegisterStartupScript("Msg", Msg);
            lblStatus.Text = "Saved Successfully";
            lblStatus.ForeColor = System.Drawing.Color.Green;
            Response.Redirect("frmHoliday.aspx");
        }
        catch (Exception ex)
        {
            lblStatus.Text =ex.Message;
        }   
    }
    protected void txtDate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
}