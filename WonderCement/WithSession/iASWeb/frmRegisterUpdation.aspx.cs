﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using GlobalSettings;

public partial class frmRegisterUpdation : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class1 cCount = new Class1();
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;
    string ReportView = "";
    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;
    ErrorClass ec = new ErrorClass();
    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["BackDateProcessVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}

            TxtFromDate.Text = "01/" + System.DateTime.Now.Month.ToString("00") + "/" + System.DateTime.Now.Year.ToString("0000");
            if (Request.QueryString["ID"] != null)
            {
                string Grade = Request.QueryString["ID"].ToString();

            }
            bindPaycode();

            bindCompany();

        }
    }
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void bindCompany()
    {
        Strsql = "select CompanyCode,convert(varchar(100),CompanyName)+' - '+convert(varchar(100),CompanyCode) as 'Comid' from tblCompany where CompanyCode!=''";
        //if ((Session["usertype"].ToString().Trim() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() !="ADMIN"))
        if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
        {
            Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            Strsql += " and companycode in (select companycode from tblemployee where paycode='" + Session["PAYCODE"].ToString().Trim() + "') ";
        }
        //Response.Write(Strsql.ToString());
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlCompany.DataSource = ds;
            ddlCompany.TextField = "Comid";
            ddlCompany.ValueField = "CompanyCode";
            ddlCompany.DataBind();
            ddlCompany.SelectedIndex = 0;
        }
        else
        {
            ddlCompany.Items.Add("NONE");
            ddlCompany.SelectedIndex = 0;

        }
    }

    protected void bindPaycode()
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where Active='Y' ";
        if (Session["usertype"].ToString() == "A")
        {
            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
            }
            Strsql += "  Order by EmpName ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql += " and headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
            }
            Strsql += "  Order by EmpName ";
        }
        else
            return;


        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPaycode.DataSource = ds;
            ddlPaycode.TextField = "EmpName1";
            ddlPaycode.ValueField = "paycode";
            ddlPaycode.DataBind();
            ddlPaycode.Items.Add("None");
            ddlPaycode.SelectedIndex = -1;
        }
        else
        {
            ddlPaycode.Items.Add("NONE");
            ddlPaycode.SelectedIndex = 0;
        }
    }


    protected void RadSal_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() == "ALL")
        {
            ddlPaycode.Enabled = false;
        }
        else
        {
            ddlPaycode.Enabled = true;
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        { }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        try
        {
            int rCount = 0;
            string Msg = "";
            string SSN = "";
            if (TxtFromDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Input Date');", true);
                TxtFromDate.Focus();
                return;
            }
            if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() != "ALL" && (ddlPaycode.SelectedItem.Text.Trim().ToUpper() == "NONE" || ddlPaycode.SelectedItem.Text.Trim() == ""))
            {

                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please select Any Record');", true);
                ddlPaycode.Focus();
                return;
            }
            DateTime dateFrom = Convert.ToDateTime(TxtFromDate.Text.ToString());
            if (RadSal.SelectedIndex == 0)
            {
                for (int k = 0; k < ddlCompany.Items.Count; k++)
                {
                    Strsql = "insert into tblFunctionCall(fromDate,FunctionName,CompanyCode) values('" + dateFrom.ToString("yyyy-MM-dd") + "','CreateDutyRoster','" + ddlCompany.Items[k].Value.ToString().Trim() + "')";
                    result = Con.execute_NonQuery(Strsql);
                    //try
                    //{
                    //    using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                    //    {
                    //        MyCmd = new OleDbCommand("ProcessUpdateDutyRoster", MyConn);
                    //        MyCmd.CommandTimeout = 1800;
                    //        MyCmd.CommandType = CommandType.StoredProcedure;
                    //        MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = "";
                    //        MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = DateTime.ParseExact(TxtFromDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //        MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ddlCompany.SelectedItem.Value.ToString().Trim();
                    //        MyConn.Open();
                    //        result = MyCmd.ExecuteNonQuery();
                    //        MyConn.Close();

                    //    }
                    //}
                    //catch (Exception Ex)
                    //{

                    //    Error_Occured("UpdateAll", Ex.Message);
                    //}
                }
            }
            else
            {
                if (ddlPaycode.SelectedIndex == -1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Input PayCode');", true);
                    TxtFromDate.Focus();
                    return;
                }
                rCount = cCount.chkRecord(ddlPaycode.SelectedItem.Value.ToString(), "UpdateDutyRoster");
                if (rCount == 0)
                {
                    Strsql = "insert into tblFunctionCall(paycode,fromDate,FunctionName) values('" + ddlPaycode.SelectedItem.Value.ToString().Trim() + "','" + dateFrom.ToString("yyyy-MM-dd") + "','CreateDutyRoster')";
                    result = Con.execute_NonQuery(Strsql);
                    SSN = ddlCompany.SelectedItem.Value.ToString().Trim() + "_" + ddlPaycode.SelectedItem.Value.ToString().Trim();
                    //try
                    //{
                    //    using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                    //    {
                    //       // MyCmd = new OleDbCommand("ProcessUpdateDutyRoster", MyConn);
                    //       // MyCmd.CommandTimeout = 1800;
                    //       // MyCmd.CommandType = CommandType.StoredProcedure;
                    //       // MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ddlPaycode.SelectedItem.Value.ToString();
                    //       // MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = DateTime.ParseExact(TxtFromDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //       //// MyCmd.Parameters.Add("@WO_Include", SqlDbType.VarChar).Value = 'N';
                    //       // MyCmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = SSN;
                    //       // MyConn.Open();
                    //       // result = MyCmd.ExecuteNonQuery();
                    //       // MyConn.Close();
                    //        MyCmd = new OleDbCommand("ProcessUpdateDutyRoster", MyConn);
                    //        MyCmd.CommandTimeout = 1800;
                    //        MyCmd.CommandType = CommandType.StoredProcedure;
                    //        MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ddlPaycode.SelectedItem.Value.ToString();
                    //        MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = DateTime.ParseExact(TxtFromDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //        MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ddlCompany.SelectedItem.Value.ToString().Trim();
                    //        MyConn.Open();
                    //        result = MyCmd.ExecuteNonQuery();
                    //        MyConn.Close();

                    //    }
                    //}
                    //catch(Exception Ex)
                    //{

                    //    Error_Occured("Update", Ex.Message);
                    //}
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Attendance Register Updated Successfully');document.location='frmRegisterUpdation.aspx'", true);
        }
        catch
        {

        }
    }

    protected void TxtFromDate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
}