USE [master]
GO
/****** Object:  Database [iASWeb]    Script Date: 02/07/2020 11:09:43 ******/
CREATE DATABASE [iASWeb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TimeWatch', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\iAS.mdf' , SIZE = 7488KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TimeWatch_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\iAS_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [iASWeb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [iASWeb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [iASWeb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [iASWeb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [iASWeb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [iASWeb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [iASWeb] SET ARITHABORT OFF 
GO
ALTER DATABASE [iASWeb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [iASWeb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [iASWeb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [iASWeb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [iASWeb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [iASWeb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [iASWeb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [iASWeb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [iASWeb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [iASWeb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [iASWeb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [iASWeb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [iASWeb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [iASWeb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [iASWeb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [iASWeb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [iASWeb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [iASWeb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [iASWeb] SET  MULTI_USER 
GO
ALTER DATABASE [iASWeb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [iASWeb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [iASWeb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [iASWeb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [iASWeb]
GO
/****** Object:  Schema [IIS APPPOOL\TWZKTECO]    Script Date: 02/07/2020 11:09:44 ******/
CREATE SCHEMA [IIS APPPOOL\TWZKTECO]
GO
/****** Object:  Table [dbo].[AccessBack]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessBack](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[MC_NO] [char](2) NULL,
	[IN_OUT] [char](1) NULL,
	[ACCESS_ALLOWED] [char](1) NULL,
	[DOOR_TIME] [char](5) NULL,
	[FLAG] [char](1) NULL,
	[ERROR_CODE] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccessCur]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessCur](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[MC_NO] [char](2) NULL,
	[IN_OUT] [char](1) NULL,
	[ACCESS_ALLOWED] [char](1) NULL,
	[DOOR_TIME] [char](5) NULL,
	[FLAG] [char](1) NULL,
	[ERROR_CODE] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACCESSREPORT]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCESSREPORT](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[MC_NO] [char](2) NULL,
	[IN_OUT] [char](1) NULL,
	[ACCESS_ALLOWED] [char](1) NULL,
	[DOOR_TIME] [char](5) NULL,
	[FLAG] [char](1) NULL,
	[ERROR_CODE] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ARREAR]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ARREAR](
	[PAYCODE] [char](10) NULL,
	[MON_YEAR] [datetime] NULL,
	[FOR_MONTH] [datetime] NULL,
	[PAIDDAYS] [numeric](5, 2) NULL,
	[ARREARBASIC] [numeric](11, 2) NULL,
	[ARREARDA] [numeric](11, 2) NULL,
	[ARREARHRA] [numeric](11, 2) NULL,
	[ARREARCONV] [numeric](11, 2) NULL,
	[ARREARMED] [numeric](11, 2) NULL,
	[ARREARAMOUNT1] [numeric](11, 2) NULL,
	[ARREARAMOUNT2] [numeric](11, 2) NULL,
	[ARREARAMOUNT3] [numeric](11, 2) NULL,
	[ARREARAMOUNT4] [numeric](11, 2) NULL,
	[ARREARAMOUNT5] [numeric](11, 2) NULL,
	[ARREARAMOUNT6] [numeric](11, 2) NULL,
	[ARREARAMOUNT7] [numeric](11, 2) NULL,
	[ARREARAMOUNT8] [numeric](11, 2) NULL,
	[ARREARAMOUNT9] [numeric](11, 2) NULL,
	[ARREARAMOUNT10] [numeric](11, 2) NULL,
	[ARREARBASIC_AMT] [numeric](11, 2) NULL,
	[ARREARDA_AMT] [numeric](11, 2) NULL,
	[ARREARHRA_AMT] [numeric](11, 2) NULL,
	[ARREARCONV_AMT] [numeric](11, 2) NULL,
	[ARREARMED_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT1_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT2_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT3_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT4_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT5_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT6_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT7_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT8_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT9_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT10_AMT] [numeric](11, 2) NULL,
	[APPLICABLETO] [datetime] NULL,
	[ARREARPF] [numeric](11, 2) NULL,
	[ARREARVPF] [numeric](11, 2) NULL,
	[ARREAREPF_AMT] [numeric](11, 2) NULL,
	[ARREARFPF_AMT] [numeric](11, 2) NULL,
	[ARREARESI] [numeric](11, 2) NULL,
	[ARREARESIONAMT] [numeric](11, 2) NULL,
	[ARREARPFONAMT] [numeric](11, 2) NULL,
	[ARREARVPFONAMT] [numeric](11, 2) NULL,
	[TO_MONTH] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ARREARREG]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ARREARREG](
	[PAYCODE] [char](10) NULL,
	[MON_YEAR] [datetime] NULL,
	[FOR_MONTH] [datetime] NULL,
	[PAIDDAYS] [numeric](5, 2) NULL,
	[ARREARBASIC] [numeric](11, 2) NULL,
	[ARREARDA] [numeric](11, 2) NULL,
	[ARREARHRA] [numeric](11, 2) NULL,
	[ARREARCONV] [numeric](11, 2) NULL,
	[ARREARMED] [numeric](11, 2) NULL,
	[ARREARAMOUNT1] [numeric](11, 2) NULL,
	[ARREARAMOUNT2] [numeric](11, 2) NULL,
	[ARREARAMOUNT3] [numeric](11, 2) NULL,
	[ARREARAMOUNT4] [numeric](11, 2) NULL,
	[ARREARAMOUNT5] [numeric](11, 2) NULL,
	[ARREARAMOUNT6] [numeric](11, 2) NULL,
	[ARREARAMOUNT7] [numeric](11, 2) NULL,
	[ARREARAMOUNT8] [numeric](11, 2) NULL,
	[ARREARAMOUNT9] [numeric](11, 2) NULL,
	[ARREARAMOUNT10] [numeric](11, 2) NULL,
	[ARREARBASIC_AMT] [numeric](11, 2) NULL,
	[ARREARDA_AMT] [numeric](11, 2) NULL,
	[ARREARHRA_AMT] [numeric](11, 2) NULL,
	[ARREARCONV_AMT] [numeric](11, 2) NULL,
	[ARREARMED_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT1_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT2_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT3_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT4_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT5_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT6_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT7_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT8_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT9_AMT] [numeric](11, 2) NULL,
	[ARREARAMOUNT10_AMT] [numeric](11, 2) NULL,
	[APPLICABLETO] [datetime] NULL,
	[ARREARPF] [numeric](11, 2) NULL,
	[ARREARVPF] [numeric](11, 2) NULL,
	[ARREAREPF_AMT] [numeric](11, 2) NULL,
	[ARREARFPF_AMT] [numeric](11, 2) NULL,
	[ARREARESI] [numeric](11, 2) NULL,
	[ARREARESIONAMT] [numeric](11, 2) NULL,
	[ARREARPFONAMT] [numeric](11, 2) NULL,
	[ARREARVPFONAMT] [numeric](11, 2) NULL,
	[TO_MONTH] [datetime] NULL,
	[VIES_1] [char](12) NULL,
	[VIES_2] [char](12) NULL,
	[VIES_3] [char](12) NULL,
	[VIES_4] [char](12) NULL,
	[VIES_5] [char](12) NULL,
	[VIES_6] [char](12) NULL,
	[VIES_7] [char](12) NULL,
	[VIES_8] [char](12) NULL,
	[VIES_9] [char](12) NULL,
	[VIES_10] [char](12) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ARREARTEMP]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ARREARTEMP](
	[PAYCODE] [char](10) NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VSALARY] [numeric](11, 2) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VDA_AMT] [numeric](11, 2) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VHRA_AMT] [numeric](11, 2) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[PROF_TAX_AMT] [numeric](11, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VVPF_AMT] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL,
	[VESI_AMT] [numeric](11, 2) NULL,
	[VTDAYS] [numeric](11, 2) NULL,
	[MON_YEAR] [datetime] NULL,
	[FOR_MONTH] [datetime] NULL,
	[AMTONPF] [numeric](11, 2) NULL,
	[AMTONVPF] [numeric](11, 2) NULL,
	[AMTONESI] [numeric](11, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Att_request]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Att_request](
	[application_no] [bigint] IDENTITY(1,1) NOT NULL,
	[paycode] [varchar](10) NULL,
	[Request_date] [datetime] NULL,
	[InTime] [varchar](10) NULL,
	[Request_for] [datetime] NULL,
	[UserRemarks] [varchar](100) NULL,
	[stage1_approval] [char](1) NULL,
	[stage1_approvalID] [varchar](10) NULL,
	[stage1_ApprovalDate] [datetime] NULL,
	[stage1_Remarks1] [varchar](100) NULL,
	[stage2_approval] [char](1) NULL,
	[stage2_approvalID] [varchar](10) NULL,
	[stage2_ApprovalDate] [datetime] NULL,
	[stage2_Remarks1] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[application_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BioDeviceGroup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BioDeviceGroup](
	[GroupID] [char](3) NOT NULL,
	[GroupName] [varchar](50) NULL,
 CONSTRAINT [PK_BioDeviceGroup] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BioDeviceGroupMapping]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BioDeviceGroupMapping](
	[GroupID] [varchar](3) NOT NULL,
	[DeviceID] [varchar](50) NULL,
 CONSTRAINT [PK_BioDeviceGroupMapping] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CanteenData]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CanteenData](
	[MCNo] [nvarchar](25) NULL,
	[CardNo] [nvarchar](8) NULL,
	[PunchTime] [datetime] NULL,
	[ItemCode] [nvarchar](2) NULL,
	[ItemQty] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cardlist]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cardlist](
	[CardNO1] [nvarchar](50) NULL,
	[CardNo] [nvarchar](8) NULL,
	[Name] [nvarchar](16) NULL,
	[departmentcode] [nvarchar](3) NULL,
	[SNO] [float] NULL,
	[Pass] [nvarchar](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClientDeviceGroup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientDeviceGroup](
	[GroupID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClientID] [bigint] NULL,
	[GroupName] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClientDeviceGroupMapping]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientDeviceGroupMapping](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NULL,
	[DeviceID] [varchar](50) NULL,
	[ClientID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Config]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Config](
	[Image] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[crtemp]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crtemp](
	[TYPE] [char](15) NULL,
	[DAY] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DailyCount]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyCount](
	[Head] [varchar](50) NULL,
	[Count] [decimal](18, 0) NULL,
	[User] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DashboardMessage]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DashboardMessage](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[MessageText] [varchar](2000) NULL,
	[CreatedBy] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[ExpiresOn] [datetime] NULL,
	[IsPublished] [bit] NULL,
	[CompanyCode] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeviceCommands]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceCommands](
	[Dev_Cmd_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SerialNumber] [varchar](50) NULL,
	[CommandContent] [varchar](50) NULL,
	[TransferToDevice] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[Executed] [bit] NULL,
	[IsOnline] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[FID] [varchar](50) NULL,
	[StartDateATTLOG] [datetime] NULL,
	[EndDateATTLOG] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeviceMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceMaster](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NULL,
	[SerialNumber] [varchar](50) NULL,
	[DeviceName] [varchar](50) NULL,
	[TimeZone] [varchar](250) NULL,
	[TransactionCount] [varchar](50) NULL,
	[FPCount] [varchar](50) NULL,
	[UserCount] [varchar](50) NULL,
	[FaceCount] [varchar](50) NULL,
	[FvCount] [varchar](50) NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Email]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email](
	[EmailId] [nvarchar](100) NULL,
	[Subject] [nvarchar](100) NULL,
	[SendName] [nvarchar](50) NULL,
	[detail] [nvarchar](200) NULL,
	[ServerIP] [nvarchar](50) NULL,
	[MailOpt] [float] NULL,
	[Pwd] [nvarchar](10) NULL,
	[PortNo] [nvarchar](5) NULL,
	[CCMail] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeGroup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeGroup](
	[GroupId] [varchar](50) NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
	[Branch] [varchar](50) NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTTYPE] [char](1) NULL,
	[SHIFTPATTERN] [char](50) NULL,
	[SHIFTREMAINDAYS] [int] NULL,
	[LASTSHIFTPERFORMED] [char](3) NULL,
	[ISAUTOSHIFT] [char](1) NULL,
	[AUTH_SHIFTS] [varchar](50) NULL,
	[FIRSTOFFDAY] [char](3) NULL,
	[SECONDOFFTYPE] [char](1) NULL,
	[HALFDAYSHIFT] [char](3) NULL,
	[SECONDOFFDAY] [char](3) NULL,
	[ALTERNATE_OFF_DAYS] [char](10) NULL,
	[INONLY] [char](1) NULL,
	[ISPUNCHALL] [char](1) NULL,
	[ISOUTWORK] [char](1) NULL,
	[TWO] [char](1) NULL,
	[ISTIMELOSSALLOWED] [char](1) NULL,
	[CDAYS] [float] NULL,
	[ISROUNDTHECLOCKWORK] [char](1) NULL,
	[ISOT] [char](1) NULL,
	[OTRATE] [char](6) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[PERMISEARLYDEPRT] [int] NULL,
	[MAXDAYMIN] [float] NULL,
	[ISOS] [char](1) NULL,
	[TIME] [int] NULL,
	[SHORT] [int] NULL,
	[HALF] [int] NULL,
	[ISHALFDAY] [char](1) NULL,
	[ISSHORT] [char](1) NULL,
	[LateVerification] [char](1) NULL,
	[EveryInterval] [char](1) NULL,
	[DeductFrom] [char](1) NULL,
	[FromLeave] [char](3) NULL,
	[LateDays] [float] NULL,
	[DeductDay] [float] NULL,
	[MaxLateDur] [float] NULL,
	[PunchRequiredInDay] [int] NULL,
	[SinglePunchOnly] [int] NULL,
	[LastModifiedBy] [varchar](max) NULL,
	[LastModifiedDate] [datetime] NULL,
	[MARKMISSASHALFDAY] [char](1) NULL,
	[Id] [int] NULL,
	[PERMISLATEARR] [int] NULL,
	[PERMISEARLYDEP] [int] NULL,
	[DUPLICATECHECKMIN] [int] NULL,
	[ISOVERSTAY] [char](1) NULL,
	[S_END] [datetime] NULL,
	[S_OUT] [datetime] NULL,
	[ISOTOUTMINUSSHIFTENDTIME] [char](1) NULL,
	[ISOTWRKGHRSMINUSSHIFTHRS] [char](1) NULL,
	[ISOTEARLYCOMEPLUSLATEDEP] [char](1) NULL,
	[ISOTALL] [char](1) NULL,
	[ISOTEARLYCOMING] [char](1) NULL,
	[OTEARLYDUR] [int] NULL,
	[OTLATECOMINGDUR] [int] NULL,
	[OTRESTRICTENDDUR] [int] NULL,
	[OTEARLYDEPARTUREDUR] [int] NULL,
	[DEDUCTWOOT] [int] NULL,
	[DEDUCTHOLIDAYOT] [int] NULL,
	[ISPRESENTONWOPRESENT] [char](1) NULL,
	[ISPRESENTONHLDPRESENT] [char](1) NULL,
	[ISAUTOABSENT] [char](1) NULL,
	[AUTOSHIFT_LOW] [int] NULL,
	[AUTOSHIFT_UP] [int] NULL,
	[WOINCLUDE] [char](1) NULL,
	[NightShiftFourPunch] [char](1) NULL,
	[OTROUND] [char](1) NULL,
	[PREWO] [int] NULL,
	[ISAWA] [char](1) NULL,
	[ISPREWO] [char](1) NULL,
	[LeaveFinancialYear] [char](1) NULL,
	[MIS] [char](1) NULL,
	[OwMinus] [char](1) NULL,
	[MSHIFT] [char](3) NULL,
	[MISCOMPOFF] [char](1) NULL,
	[ISCOMPOFF] [char](1) NULL,
	[ISAW] [char](1) NULL,
	[ISWA] [char](1) NULL,
	[ISAWP] [char](1) NULL,
	[ISPWA] [char](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FingerSetup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FingerSetup](
	[CardNo] [nvarchar](10) NULL,
	[Fp_no] [int] NULL,
	[UserFp] [ntext] NULL,
	[name] [nvarchar](16) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FORM6]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FORM6](
	[PAYCODE] [char](10) NULL,
	[MON_YEAR] [datetime] NULL,
	[VPRE] [numeric](6, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VVPF_AMT] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL,
	[AMTONPF] [numeric](11, 2) NULL,
	[AMTONVPF] [numeric](11, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FORM6SUMM]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FORM6SUMM](
	[SRNO] [numeric](2, 0) NULL,
	[MONTHNAME] [char](7) NULL,
	[EPFCONTRIBUTION] [numeric](12, 2) NULL,
	[PFCONTRIBUTION] [numeric](12, 2) NULL,
	[ACTNO2] [numeric](11, 2) NULL,
	[ACTNO22] [numeric](11, 2) NULL,
	[TXTCOMMENT] [char](150) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FPDATA]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FPDATA](
	[User_id] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Fp1] [image] NULL,
	[Flag1] [nvarchar](1) NULL,
	[FP2] [image] NULL,
	[FLAG2] [nvarchar](1) NULL,
	[FP3] [image] NULL,
	[FLAG3] [nvarchar](1) NULL,
	[Option] [nvarchar](1) NULL,
	[image] [image] NULL,
	[password] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[fptable]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fptable](
	[EMachineNumber] [int] NULL,
	[EnrollNumber] [nvarchar](10) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[FingerNumber] [int] NULL,
	[Privilege] [int] NULL,
	[Password] [nvarchar](50) NULL,
	[Template] [ntext] NULL,
	[Cardnumber] [nvarchar](10) NULL,
	[calid] [char](10) NULL,
	[authority] [char](10) NULL,
	[check_type] [char](10) NULL,
	[opendoor_type] [char](10) NULL,
	[face_Data1] [ntext] NULL,
	[face_data2] [ntext] NULL,
	[face_data3] [ntext] NULL,
	[face_data4] [ntext] NULL,
	[face_data5] [ntext] NULL,
	[face_data6] [ntext] NULL,
	[face_data7] [ntext] NULL,
	[face_data8] [ntext] NULL,
	[face_data9] [ntext] NULL,
	[face_data10] [ntext] NULL,
	[face_data11] [ntext] NULL,
	[face_data12] [ntext] NULL,
	[face_data13] [ntext] NULL,
	[face_data14] [ntext] NULL,
	[face_data15] [ntext] NULL,
	[face_data16] [ntext] NULL,
	[face_data17] [ntext] NULL,
	[face_data18] [ntext] NULL,
	[Template_Tw] [image] NULL,
	[VerifyMode] [char](1) NULL,
	[Fingermf850] [varchar](8000) NULL,
	[Template_Face] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[History]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History](
	[Tid] [int] NULL,
	[TerminalName] [nvarchar](255) NULL,
	[UserID] [nvarchar](255) NULL,
	[UserName] [nvarchar](255) NULL,
	[LogDate] [nvarchar](255) NULL,
	[LogTime] [nvarchar](255) NULL,
	[Verify] [nvarchar](255) NULL,
	[FKey] [nvarchar](255) NULL,
	[DlData] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Holiday]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday](
	[HDate] [datetime] NOT NULL,
	[HOLIDAY] [char](20) NULL,
	[ADJUSTMENTHOLIDAY] [datetime] NULL,
	[OT_FACTOR] [float] NULL,
	[COMPANYCODE] [char](4) NOT NULL,
	[DEPARTMENTCODE] [char](3) NOT NULL,
	[BranchCode] [char](10) NOT NULL,
	[Divisioncode] [char](10) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[iDMSSetting]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[iDMSSetting](
	[NoOfCommandsAtTime] [varchar](50) NULL,
	[AutoSync] [varchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InstallSystemInfo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InstallSystemInfo](
	[MAC] [varchar](max) NULL,
	[ComName] [varchar](50) NULL,
	[ComAdd] [varchar](max) NULL,
	[ComContact] [varchar](50) NULL,
	[ComEmail] [varchar](50) NULL,
	[UserKey] [varchar](50) NULL,
	[License] [varchar](50) NULL,
	[InstalledDate] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[MaxDevice] [varchar](500) NULL,
	[MaxUser] [varchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LAST_XFER_STAT]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LAST_XFER_STAT](
	[MACHINENO] [nvarchar](25) NULL,
	[TDATE] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[leave_request]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[leave_request](
	[application_no] [numeric](18, 0) NOT NULL,
	[paycode] [char](10) NULL,
	[request_id] [varchar](100) NULL,
	[request_date] [datetime] NULL,
	[leave_from] [datetime] NULL,
	[leave_to] [datetime] NULL,
	[leavecode] [char](3) NULL,
	[halfday] [char](1) NULL,
	[leavedays] [float] NULL,
	[userremarks] [varchar](200) NULL,
	[Stage1_approved] [char](1) NULL,
	[Stage1_approval_id] [char](20) NULL,
	[Stage1_approval_date] [datetime] NULL,
	[Stage1_approval_Remarks] [varchar](2000) NULL,
	[Stage2_approved] [char](20) NULL,
	[Stage2_approval_id] [char](20) NULL,
	[Stage2_approval_date] [datetime] NULL,
	[Stage2_Approval_Remarks] [varchar](2000) NULL,
	[hod_remarks] [varchar](30) NULL,
	[chklbl1] [char](1) NULL,
	[voucherno] [char](10) NULL,
	[Stage3_approved] [char](1) NULL,
	[Stage3_approval_id] [char](10) NULL,
	[Stage3_approval_date] [datetime] NULL,
	[Stage3_approval_Remarks] [varchar](2000) NULL,
	[LWPbal] [float] NULL,
	[IsArrear] [char](10) NULL,
	[BVRLeave] [char](20) NULL,
	[OD_Purpose] [char](1) NULL,
	[OD_In] [datetime] NULL,
	[OD_Out] [datetime] NULL,
	[medicaldoc] [varchar](500) NULL,
	[SSN] [varchar](100) NULL,
	[CoffDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MachineRawPunch]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MachineRawPunch](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[P_DAY] [char](1) NULL,
	[ISMANUAL] [char](1) NULL,
	[ReasonCode] [char](3) NULL,
	[MC_NO] [char](3) NULL,
	[INOUT] [char](1) NULL,
	[PAYCODE] [char](10) NULL,
	[Lcode] [char](4) NULL,
	[SSN] [varchar](100) NOT NULL,
	[CompanyCode] [varchar](4) NULL,
	[DeviceID] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuMaster](
	[MailId] [bit] NOT NULL,
	[MultipleDataCap] [bit] NOT NULL,
	[OnlineDataCap] [bit] NOT NULL,
	[CardWriter] [bit] NOT NULL,
	[FingerPrint] [bit] NOT NULL,
	[ReasonCard] [bit] NOT NULL,
	[CardMaster] [bit] NOT NULL,
	[Masking] [bit] NOT NULL,
	[SingleMM] [bit] NOT NULL,
	[MultipleMM] [bit] NOT NULL,
	[OnlineMM] [bit] NOT NULL,
	[Proximity] [bit] NOT NULL,
	[ProxiLStoM] [bit] NOT NULL,
	[HeperTerminal] [bit] NOT NULL,
	[Proxyfinger] [bit] NOT NULL,
	[Mailopt] [float] NULL,
	[MachineConfig] [bit] NOT NULL,
	[BootLoader] [bit] NOT NULL,
	[DataCaptureConsole] [bit] NOT NULL,
	[DataDeletionConsole] [bit] NOT NULL,
	[FingerScannerDevice] [bit] NOT NULL,
	[AutoDataDeletionConsole] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MIN_MAX_DATE]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MIN_MAX_DATE](
	[PUNCH_DATE] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OperationLog]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OperationLog](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](50) NULL,
	[OperationType] [varchar](150) NULL,
	[AdminID] [varchar](50) NULL,
	[OpTime] [datetime] NULL,
	[Value1] [varchar](50) NULL,
	[Value2] [varchar](50) NULL,
	[Value3] [varchar](50) NULL,
	[Value4] [varchar](50) NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OTSETUP]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OTSETUP](
	[OTMONTHLY] [char](2) NULL,
	[OTPERDAY] [char](2) NULL,
	[OTPERWEEK] [char](2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ParallelDB]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParallelDB](
	[IsParallel] [varchar](50) NULL,
	[DBName] [varchar](max) NULL,
	[DBType] [varchar](50) NULL,
	[AuthMode] [varchar](50) NULL,
	[ServerName] [varchar](max) NULL,
	[UserName] [varchar](max) NULL,
	[Pwd] [varchar](max) NULL,
	[TblName] [varchar](max) NULL,
	[PreCardNo] [varchar](max) NULL,
	[PreIsPrifix] [varchar](50) NULL,
	[PreLength] [varchar](50) NULL,
	[PreText] [varchar](50) NULL,
	[Paycode] [varchar](max) NULL,
	[PayIsPrefix] [varchar](50) NULL,
	[PayLength] [varchar](50) NULL,
	[PayText] [varchar](50) NULL,
	[PunchDate] [varchar](max) NULL,
	[PunchDateFormat] [varchar](max) NULL,
	[PunchTime] [varchar](max) NULL,
	[PunchTimeFormat] [varchar](max) NULL,
	[PunchDateTime] [varchar](max) NULL,
	[PunchDateTimeFormat] [varchar](max) NULL,
	[DeviceId] [varchar](max) NULL,
	[PunchDirection] [varchar](max) NULL,
	[InValue] [varchar](max) NULL,
	[OutVaue] [varchar](max) NULL,
	[IsManual] [varchar](max) NULL,
	[YesValue] [varchar](max) NULL,
	[NoValue] [varchar](max) NULL,
	[Rfield1] [varchar](max) NULL,
	[Rfield1Value] [varchar](max) NULL,
	[Rfield2] [varchar](max) NULL,
	[Rfield2Value] [varchar](max) NULL,
	[Rfield3] [varchar](max) NULL,
	[Rfield3Value] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_BONUSPAYED]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_BONUSPAYED](
	[PAYCODE] [char](10) NULL,
	[ACC_YEAR] [char](8) NULL,
	[PAYED_DATE] [datetime] NULL,
	[LASTBONUS] [numeric](10, 2) NULL,
	[BONUSAMT] [numeric](10, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_f16standarddeduction]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_f16standarddeduction](
	[ID_NO] [int] NULL,
	[LOWERLIMIT] [numeric](12, 2) NULL,
	[UPPERLIMIT] [numeric](12, 2) NULL,
	[FACTOR1] [numeric](2, 0) NULL,
	[FACTOR2] [numeric](2, 0) NULL,
	[FIXED] [numeric](12, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_form16_setup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_form16_setup](
	[ID_NO] [int] NULL,
	[PERCENT16] [numeric](5, 2) NULL,
	[AMOUNT] [numeric](11, 2) NULL,
	[SURCHARGE] [numeric](5, 2) NULL,
	[QUALIFYRATE] [numeric](5, 2) NULL,
	[WOMENTAXDED] [numeric](10, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_form16data]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_form16data](
	[PAYCODE] [char](10) NOT NULL,
	[ACC_YEAR] [char](8) NOT NULL,
	[ACCESSMENTYEAR] [char](8) NULL,
	[GROSS] [numeric](12, 2) NULL,
	[BASICB] [numeric](12, 2) NULL,
	[HRAB] [numeric](12, 2) NULL,
	[CONVEYANCEB] [numeric](12, 2) NULL,
	[LEAVESALARYB] [numeric](12, 2) NULL,
	[BONUSB] [numeric](12, 2) NULL,
	[OTHERSB] [numeric](12, 2) NULL,
	[CONVEYANCE] [numeric](12, 2) NULL,
	[HRA] [numeric](12, 2) NULL,
	[STDDEDUCTION] [numeric](10, 2) NULL,
	[ENTERTAINMENT] [numeric](10, 2) NULL,
	[TAXONEMPLOYMENT] [numeric](10, 2) NULL,
	[OTHERINCOME] [numeric](10, 2) NULL,
	[MEDICLAIM] [numeric](10, 2) NULL,
	[MEDICLAIMQ] [numeric](10, 2) NULL,
	[MEDICLAIMD] [numeric](10, 2) NULL,
	[DONATION] [numeric](10, 2) NULL,
	[DONATIONQ] [numeric](10, 2) NULL,
	[DONATIOND] [numeric](10, 2) NULL,
	[INTONHOUSING] [numeric](10, 2) NULL,
	[INTONHOUSINGQ] [numeric](10, 2) NULL,
	[INTONHOUSINGD] [numeric](10, 2) NULL,
	[MATTER9D] [char](25) NULL,
	[VALUE9D] [numeric](10, 2) NULL,
	[VALUEQ9D] [numeric](10, 2) NULL,
	[VALUED9D] [numeric](10, 2) NULL,
	[TAXONTOTALINC] [numeric](10, 2) NULL,
	[PF] [numeric](10, 2) NULL,
	[PFQ] [numeric](10, 2) NULL,
	[PFD] [numeric](10, 2) NULL,
	[LIC] [numeric](10, 2) NULL,
	[LICQ] [numeric](10, 2) NULL,
	[LICD] [numeric](10, 2) NULL,
	[NSC] [numeric](10, 2) NULL,
	[NSCQ] [numeric](10, 2) NULL,
	[NSCD] [numeric](10, 2) NULL,
	[INTONNSC] [numeric](10, 2) NULL,
	[INTONNSCQ] [numeric](10, 2) NULL,
	[INTONNSCD] [numeric](10, 2) NULL,
	[INVINBONDS] [numeric](10, 2) NULL,
	[INVINBONDSQ] [numeric](10, 2) NULL,
	[INVINBONDSD] [numeric](10, 2) NULL,
	[PAMTOFHOUSING] [numeric](10, 2) NULL,
	[PAMTOFHOUSINGQ] [numeric](10, 2) NULL,
	[PAMTOFHOUSINGD] [numeric](10, 2) NULL,
	[EDUALLOWANCE] [numeric](10, 2) NULL,
	[EDUALLOWANCEQ] [numeric](10, 2) NULL,
	[MATTER13G] [char](25) NULL,
	[VALUE13G] [numeric](10, 2) NULL,
	[VALUEQ13G] [numeric](10, 2) NULL,
	[VALUED13G] [numeric](10, 2) NULL,
	[MATTERA88A] [char](25) NULL,
	[VALUEA88A] [numeric](10, 2) NULL,
	[VALUEAQ88A] [numeric](10, 2) NULL,
	[MATTERB88A] [char](25) NULL,
	[VALUEB88A] [numeric](10, 2) NULL,
	[VALUEBQ88A] [numeric](10, 2) NULL,
	[WOMENTAX] [numeric](10, 2) NULL,
	[WOMENTAXQ] [numeric](10, 2) NULL,
	[SECTION89] [numeric](10, 2) NULL,
	[SURCHARGE] [numeric](10, 2) NULL,
	[TOTALTAXPAYABLE] [numeric](10, 2) NULL,
	[TDS] [numeric](10, 2) NULL,
	[TAXPAYREFUND] [numeric](10, 2) NULL,
	[HRAACTUALLYPAYED] [numeric](10, 2) NULL,
	[EDUEXP] [numeric](18, 0) NULL,
	[ULIP] [numeric](18, 0) NULL,
	[PENSION] [numeric](18, 0) NULL,
	[C80] [numeric](18, 0) NULL,
	[EDUCATIONCESS] [numeric](18, 0) NULL,
	[total80c] [numeric](18, 0) NULL,
	[total80d] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_form16dataF]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_form16dataF](
	[PAYCODE] [char](10) NOT NULL,
	[ACC_YEAR] [char](8) NOT NULL,
	[ACCESSMENTYEAR] [char](8) NULL,
	[GROSS] [numeric](12, 2) NULL,
	[BASICB] [numeric](12, 2) NULL,
	[HRAB] [numeric](12, 2) NULL,
	[CONVEYANCEB] [numeric](12, 2) NULL,
	[LEAVESALARYB] [numeric](12, 2) NULL,
	[BONUSB] [numeric](12, 2) NULL,
	[OTHERSB] [numeric](12, 2) NULL,
	[CONVEYANCE] [numeric](12, 2) NULL,
	[HRA] [numeric](12, 2) NULL,
	[STDDEDUCTION] [numeric](10, 2) NULL,
	[ENTERTAINMENT] [numeric](10, 2) NULL,
	[TAXONEMPLOYMENT] [numeric](10, 2) NULL,
	[OTHERINCOME] [numeric](10, 2) NULL,
	[MEDICLAIM] [numeric](10, 2) NULL,
	[MEDICLAIMQ] [numeric](10, 2) NULL,
	[MEDICLAIMD] [numeric](10, 2) NULL,
	[DONATION] [numeric](10, 2) NULL,
	[DONATIONQ] [numeric](10, 2) NULL,
	[DONATIOND] [numeric](10, 2) NULL,
	[INTONHOUSING] [numeric](10, 2) NULL,
	[INTONHOUSINGQ] [numeric](10, 2) NULL,
	[INTONHOUSINGD] [numeric](10, 2) NULL,
	[MATTER9D] [char](25) NULL,
	[VALUE9D] [numeric](10, 2) NULL,
	[VALUEQ9D] [numeric](10, 2) NULL,
	[VALUED9D] [numeric](10, 2) NULL,
	[TAXONTOTALINC] [numeric](10, 2) NULL,
	[PF] [numeric](10, 2) NULL,
	[PFQ] [numeric](10, 2) NULL,
	[PFD] [numeric](10, 2) NULL,
	[LIC] [numeric](10, 2) NULL,
	[LICQ] [numeric](10, 2) NULL,
	[LICD] [numeric](10, 2) NULL,
	[NSC] [numeric](10, 2) NULL,
	[NSCQ] [numeric](10, 2) NULL,
	[NSCD] [numeric](10, 2) NULL,
	[INTONNSC] [numeric](10, 2) NULL,
	[INTONNSCQ] [numeric](10, 2) NULL,
	[INTONNSCD] [numeric](10, 2) NULL,
	[INVINBONDS] [numeric](10, 2) NULL,
	[INVINBONDSQ] [numeric](10, 2) NULL,
	[INVINBONDSD] [numeric](10, 2) NULL,
	[PAMTOFHOUSING] [numeric](10, 2) NULL,
	[PAMTOFHOUSINGQ] [numeric](10, 2) NULL,
	[PAMTOFHOUSINGD] [numeric](10, 2) NULL,
	[EDUALLOWANCE] [numeric](10, 2) NULL,
	[EDUALLOWANCEQ] [numeric](10, 2) NULL,
	[MATTER13G] [char](25) NULL,
	[VALUE13G] [numeric](10, 2) NULL,
	[VALUEQ13G] [numeric](10, 2) NULL,
	[VALUED13G] [numeric](10, 2) NULL,
	[MATTERA88A] [char](25) NULL,
	[VALUEA88A] [numeric](10, 2) NULL,
	[VALUEAQ88A] [numeric](10, 2) NULL,
	[MATTERB88A] [char](25) NULL,
	[VALUEB88A] [numeric](10, 2) NULL,
	[VALUEBQ88A] [numeric](10, 2) NULL,
	[WOMENTAX] [numeric](10, 2) NULL,
	[WOMENTAXQ] [numeric](10, 2) NULL,
	[SECTION89] [numeric](10, 2) NULL,
	[SURCHARGE] [numeric](10, 2) NULL,
	[TOTALTAXPAYABLE] [numeric](10, 2) NULL,
	[TDS] [numeric](10, 2) NULL,
	[TAXPAYREFUND] [numeric](10, 2) NULL,
	[HRAACTUALLYPAYED] [numeric](10, 2) NULL,
	[EDUEXP] [numeric](18, 0) NULL,
	[ULIP] [numeric](18, 0) NULL,
	[PENSION] [numeric](18, 0) NULL,
	[C80] [numeric](18, 0) NULL,
	[EDUCATIONCESS] [numeric](18, 0) NULL,
	[total80c] [numeric](18, 0) NULL,
	[total80d] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_form16pay]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_form16pay](
	[PAYCODE] [char](10) NULL,
	[PAYDATE] [datetime] NULL,
	[ACC_YEAR] [char](8) NULL,
	[AMOUNT] [numeric](11, 2) NULL,
	[BANK] [char](60) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_form16paytemp]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_form16paytemp](
	[paycode] [char](10) NULL,
	[acc_year] [char](8) NULL,
	[paydate1] [datetime] NULL,
	[paydate2] [datetime] NULL,
	[paydate3] [datetime] NULL,
	[paydate4] [datetime] NULL,
	[paydate5] [datetime] NULL,
	[paydate6] [datetime] NULL,
	[paydate7] [datetime] NULL,
	[paydate8] [datetime] NULL,
	[paydate9] [datetime] NULL,
	[paydate10] [datetime] NULL,
	[paydate11] [datetime] NULL,
	[paydate12] [datetime] NULL,
	[paydate13] [datetime] NULL,
	[paydate14] [datetime] NULL,
	[amount] [char](25) NULL,
	[amount1] [numeric](11, 2) NULL,
	[amount2] [numeric](11, 2) NULL,
	[amount3] [numeric](11, 2) NULL,
	[amount4] [numeric](11, 2) NULL,
	[amount5] [numeric](11, 2) NULL,
	[amount6] [numeric](11, 2) NULL,
	[amount7] [numeric](11, 2) NULL,
	[amount8] [numeric](11, 2) NULL,
	[amount9] [numeric](11, 2) NULL,
	[amount10] [numeric](11, 2) NULL,
	[amount11] [numeric](11, 2) NULL,
	[amount12] [numeric](11, 2) NULL,
	[amount13] [numeric](11, 2) NULL,
	[amount14] [numeric](11, 2) NULL,
	[bank1] [char](60) NULL,
	[bank2] [char](60) NULL,
	[bank3] [char](60) NULL,
	[bank4] [char](60) NULL,
	[bank5] [char](60) NULL,
	[bank6] [char](60) NULL,
	[bank7] [char](60) NULL,
	[bank8] [char](60) NULL,
	[bank9] [char](60) NULL,
	[bank10] [char](60) NULL,
	[bank11] [char](60) NULL,
	[bank12] [char](60) NULL,
	[bank13] [char](60) NULL,
	[bank14] [char](60) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_FORMULA]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_FORMULA](
	[CODE] [char](1) NULL,
	[FORM] [char](355) NULL,
	[CompanyCode] [char](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_FULLFINAL]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_FULLFINAL](
	[PAYCODE] [char](10) NOT NULL,
	[VNETSALARY] [numeric](11, 2) NULL,
	[VLEAVE] [numeric](11, 2) NULL,
	[VLEAVE_INCASH] [numeric](11, 2) NULL,
	[VGRATUITY_DUR] [numeric](11, 2) NULL,
	[VGRATUITY_AMT] [numeric](11, 2) NULL,
	[VNOTICE_MONTH] [numeric](11, 2) NULL,
	[VNOTICE_AMT] [numeric](11, 2) NULL,
	[LEAVING_DATE] [datetime] NULL,
	[MON_YEAR] [datetime] NULL,
	[Advance_AMT] [numeric](11, 2) NULL,
	[loan_AMT] [numeric](11, 2) NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[L01_CASH] [float] NULL,
	[L02_CASH] [float] NULL,
	[L03_CASH] [float] NULL,
	[L04_CASH] [float] NULL,
	[L05_CASH] [float] NULL,
	[L06_CASH] [float] NULL,
	[L07_CASH] [float] NULL,
	[L08_CASH] [float] NULL,
	[L09_CASH] [float] NULL,
	[L10_CASH] [float] NULL,
	[L11_CASH] [float] NULL,
	[L12_CASH] [float] NULL,
	[L13_CASH] [float] NULL,
	[L14_CASH] [float] NULL,
	[L15_CASH] [float] NULL,
	[L16_CASH] [float] NULL,
	[L17_CASH] [float] NULL,
	[L18_CASH] [float] NULL,
	[L19_CASH] [float] NULL,
	[L20_CASH] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_GRATUITY]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_GRATUITY](
	[PAYCODE] [char](10) NOT NULL,
	[ASONDATE] [datetime] NULL,
	[GRATUITY_AMT] [numeric](10, 2) NULL,
	[PERIOD] [numeric](10, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_LEAVEENCASHMENT]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_LEAVEENCASHMENT](
	[PAYCODE] [char](10) NULL,
	[ACC_YEAR] [char](8) NULL,
	[PAYED_Month] [char](7) NULL,
	[L01_CASH] [float] NULL,
	[L02_CASH] [float] NULL,
	[L03_CASH] [float] NULL,
	[L04_CASH] [float] NULL,
	[L05_CASH] [float] NULL,
	[L06_CASH] [float] NULL,
	[L07_CASH] [float] NULL,
	[L08_CASH] [float] NULL,
	[L09_CASH] [float] NULL,
	[L10_CASH] [float] NULL,
	[L11_CASH] [float] NULL,
	[L12_CASH] [float] NULL,
	[L13_CASH] [float] NULL,
	[L14_CASH] [float] NULL,
	[L15_CASH] [float] NULL,
	[L16_CASH] [float] NULL,
	[L17_CASH] [float] NULL,
	[L18_CASH] [float] NULL,
	[L19_CASH] [float] NULL,
	[L20_CASH] [float] NULL,
	[ENCASHMENT_AMT] [numeric](10, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pay_Master]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pay_Master](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VPaymentby] [char](1) NULL,
	[VBankAcc] [char](25) NULL,
	[VEmployeeType] [char](1) NULL,
	[VCPF_NO] [char](12) NULL,
	[VPF_NO] [numeric](5, 0) NULL,
	[VESI_NO] [char](12) NULL,
	[VPAN_NO] [char](12) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[BONUS_ALLOWED] [char](1) NULL,
	[GRADUTY_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[bankCODE] [char](3) NULL,
	[EFFECTFROM] [datetime] NULL,
	[VGross] [numeric](10, 2) NULL,
	[VIncrement] [numeric](10, 2) NULL,
	[pfUlimit] [char](1) NULL,
	[SSN] [nchar](50) NOT NULL,
	[Company_Code] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_master1]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_master1](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VPaymentby] [char](1) NULL,
	[VBankAcc] [char](25) NULL,
	[VEmployeeType] [char](1) NULL,
	[VCPF_NO] [char](12) NULL,
	[VPF_NO] [numeric](5, 0) NULL,
	[VESI_NO] [char](12) NULL,
	[VPAN_NO] [char](12) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[BONUS_ALLOWED] [char](1) NULL,
	[GRADUTY_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[bankCODE] [char](3) NULL,
	[EFFECTFROM] [datetime] NULL,
	[VGross] [numeric](10, 2) NULL,
	[VIncrement] [numeric](10, 2) NULL,
	[pfUlimit] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_MASTERD]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_MASTERD](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VPaymentby] [char](1) NULL,
	[VBankAcc] [char](25) NULL,
	[VEmployeeType] [char](1) NULL,
	[VCPF_NO] [char](12) NULL,
	[VPF_NO] [numeric](5, 0) NULL,
	[VESI_NO] [char](12) NULL,
	[VPAN_NO] [char](12) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[BONUS_ALLOWED] [char](1) NULL,
	[GRADUTY_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[bankCODE] [char](3) NULL,
	[EFFECTFROM] [datetime] NULL,
	[VGross] [numeric](10, 2) NULL,
	[VIncrement] [numeric](10, 2) NULL,
	[pfUlimit] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_MASTERT]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_MASTERT](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VPaymentby] [char](1) NULL,
	[VBankAcc] [char](25) NULL,
	[VEmployeeType] [char](1) NULL,
	[VCPF_NO] [char](12) NULL,
	[VPF_NO] [numeric](5, 0) NULL,
	[VESI_NO] [char](12) NULL,
	[VPAN_NO] [char](12) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[BONUS_ALLOWED] [char](1) NULL,
	[GRADUTY_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[bankCODE] [char](3) NULL,
	[EFFECTFROM] [datetime] NULL,
	[VGross] [numeric](10, 2) NULL,
	[VIncrement] [numeric](10, 2) NULL,
	[pfUlimit] [char](1) NULL,
	[SSN] [nchar](50) NOT NULL,
	[Company_Code] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_REIMURSH]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_REIMURSH](
	[PAYCODE] [char](10) NOT NULL,
	[MON_YEAR] [datetime] NOT NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[paid] [char](1) NULL,
	[COMPANYCODE] [nchar](10) NULL,
	[SSN] [nchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_RESULT]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_RESULT](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VSALARY] [numeric](11, 2) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VDA_AMT] [numeric](11, 2) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VHRA_AMT] [numeric](11, 2) NULL,
	[VOT] [numeric](6, 2) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VOT_AMT] [numeric](11, 2) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[PROF_TAX_AMT] [numeric](11, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VVPF_AMT] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL,
	[VESI_AMT] [numeric](11, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[VTDS_AMT] [numeric](11, 2) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VD_1_AMT] [numeric](11, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VD_2_AMT] [numeric](11, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VD_3_AMT] [numeric](11, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VD_4_AMT] [numeric](11, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VD_5_AMT] [numeric](11, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VD_6_AMT] [numeric](11, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VD_7_AMT] [numeric](11, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VD_8_AMT] [numeric](11, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VD_9_AMT] [numeric](11, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VD_10_AMT] [numeric](11, 2) NULL,
	[VFINE] [numeric](8, 2) NULL,
	[VADVANCE] [numeric](8, 2) NULL,
	[VPRE] [numeric](6, 2) NULL,
	[VABS] [numeric](6, 2) NULL,
	[VHLD] [numeric](6, 2) NULL,
	[VWO] [numeric](6, 2) NULL,
	[VCL] [numeric](6, 2) NULL,
	[VSL] [numeric](6, 2) NULL,
	[VPL_EL] [numeric](6, 2) NULL,
	[VOTHER_LV] [numeric](6, 2) NULL,
	[VLEAVE] [numeric](6, 2) NULL,
	[VTDAYS] [numeric](6, 2) NULL,
	[VTOTAL_DAY] [numeric](6, 2) NULL,
	[VLATE] [numeric](7, 2) NULL,
	[VT_LATE] [numeric](11, 2) NULL,
	[VEARLY] [numeric](7, 2) NULL,
	[VT_EARLY] [numeric](11, 2) NULL,
	[MON_YEAR] [datetime] NOT NULL,
	[AMTONPF] [numeric](11, 2) NULL,
	[AMTONVPF] [numeric](11, 2) NULL,
	[AMTONESI] [numeric](11, 2) NULL,
	[ESIONOT] [numeric](11, 2) NULL,
	[SSN] [nchar](50) NOT NULL,
	[COMPANYCODE] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_RESULTD]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_RESULTD](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VSALARY] [numeric](11, 2) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VDA_AMT] [numeric](11, 2) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VHRA_AMT] [numeric](11, 2) NULL,
	[VOT] [numeric](6, 2) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VOT_AMT] [numeric](11, 2) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[PROF_TAX_AMT] [numeric](11, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VVPF_AMT] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL,
	[VESI_AMT] [numeric](11, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[VTDS_AMT] [numeric](11, 2) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VD_1_AMT] [numeric](11, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VD_2_AMT] [numeric](11, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VD_3_AMT] [numeric](11, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VD_4_AMT] [numeric](11, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VD_5_AMT] [numeric](11, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VD_6_AMT] [numeric](11, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VD_7_AMT] [numeric](11, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VD_8_AMT] [numeric](11, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VD_9_AMT] [numeric](11, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VD_10_AMT] [numeric](11, 2) NULL,
	[VFINE] [numeric](8, 2) NULL,
	[VADVANCE] [numeric](8, 2) NULL,
	[VPRE] [numeric](6, 2) NULL,
	[VABS] [numeric](6, 2) NULL,
	[VHLD] [numeric](6, 2) NULL,
	[VWO] [numeric](6, 2) NULL,
	[VCL] [numeric](6, 2) NULL,
	[VSL] [numeric](6, 2) NULL,
	[VPL_EL] [numeric](6, 2) NULL,
	[VOTHER_LV] [numeric](6, 2) NULL,
	[VLEAVE] [numeric](6, 2) NULL,
	[VTDAYS] [numeric](6, 2) NULL,
	[VTOTAL_DAY] [numeric](6, 2) NULL,
	[VLATE] [numeric](7, 2) NULL,
	[VT_LATE] [numeric](11, 2) NULL,
	[VEARLY] [numeric](7, 2) NULL,
	[VT_EARLY] [numeric](11, 2) NULL,
	[MON_YEAR] [datetime] NOT NULL,
	[AMTONPF] [numeric](11, 2) NULL,
	[AMTONVPF] [numeric](11, 2) NULL,
	[AMTONESI] [numeric](11, 2) NULL,
	[ESIONOT] [numeric](11, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pay_setup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pay_setup](
	[PFLIMIT] [numeric](9, 2) NULL,
	[PF] [numeric](5, 2) NULL,
	[EPF] [numeric](5, 2) NULL,
	[FPF] [numeric](5, 2) NULL,
	[PFE] [numeric](5, 2) NULL,
	[VPF] [numeric](5, 2) NULL,
	[PFRND] [numeric](1, 0) NULL,
	[PFREL] [char](15) NULL,
	[ESILIMIT] [numeric](9, 2) NULL,
	[ESI] [numeric](5, 2) NULL,
	[ESIE] [numeric](5, 2) NULL,
	[ESIRND] [numeric](1, 0) NULL,
	[VDES_1] [char](12) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VDES_2] [char](12) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VDES_3] [char](12) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VDES_4] [char](12) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VDES_5] [char](12) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VDES_6] [char](12) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VDES_7] [char](12) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VDES_8] [char](12) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VDES_9] [char](12) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VDES_10] [char](12) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VIES_1] [char](12) NULL,
	[VIT_1] [char](1) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIES_2] [char](12) NULL,
	[VIT_2] [char](1) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIES_3] [char](12) NULL,
	[VIT_3] [char](1) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIES_4] [char](12) NULL,
	[VIT_4] [char](1) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIES_5] [char](12) NULL,
	[VIT_5] [char](1) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIES_6] [char](12) NULL,
	[VIT_6] [char](1) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIES_7] [char](12) NULL,
	[VIT_7] [char](1) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIES_8] [char](12) NULL,
	[VIT_8] [char](1) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIES_9] [char](12) NULL,
	[VIT_9] [char](1) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIES_10] [char](12) NULL,
	[VIT_10] [char](1) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[TDS_RATE] [numeric](8, 2) NULL,
	[TDS_F] [char](1) NULL,
	[BONUS_WORKDAYS] [numeric](5, 2) NULL,
	[BONUS_WAGELIMIT] [numeric](9, 2) NULL,
	[BONUS_LIMIT] [numeric](9, 2) NULL,
	[BONUS_ARRIER] [char](1) NULL,
	[BONUS_RATE] [numeric](5, 2) NULL,
	[PFAC02] [numeric](5, 2) NULL,
	[PFAC21] [numeric](5, 2) NULL,
	[PFAC22] [numeric](5, 2) NULL,
	[BASIC_RND] [char](1) NULL,
	[DA_RND] [char](1) NULL,
	[HRA_RND] [char](1) NULL,
	[CONV_RND] [char](1) NULL,
	[MEDICAL_RND] [char](1) NULL,
	[EARN1_RND] [char](1) NULL,
	[EARN2_RND] [char](1) NULL,
	[EARN3_RND] [char](1) NULL,
	[EARN4_RND] [char](1) NULL,
	[EARN5_RND] [char](1) NULL,
	[EARN6_RND] [char](1) NULL,
	[EARN7_RND] [char](1) NULL,
	[EARN8_RND] [char](1) NULL,
	[EARN9_RND] [char](1) NULL,
	[EARN10_RND] [char](1) NULL,
	[TDS_RND] [char](1) NULL,
	[PROF_TAX_RND] [char](1) NULL,
	[DED1_RND] [char](1) NULL,
	[DED2_RND] [char](1) NULL,
	[DED3_RND] [char](1) NULL,
	[DED4_RND] [char](1) NULL,
	[DED5_RND] [char](1) NULL,
	[DED6_RND] [char](1) NULL,
	[DED7_RND] [char](1) NULL,
	[DED8_RND] [char](1) NULL,
	[DED9_RND] [char](1) NULL,
	[DED10_RND] [char](1) NULL,
	[OTAMT_RND] [char](1) NULL,
	[GRADUTY_YRLIMIT] [numeric](5, 2) NULL,
	[GRADUTY_FORMULA] [char](50) NULL,
	[otLogic] [char](1) NULL,
	[otLogic1] [char](1) NULL,
	[otperDay] [char](2) NULL,
	[otperWeek] [char](2) NULL,
	[procDate] [datetime] NULL,
	[PF_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[GRADUTY_ALLOWED] [char](1) NULL,
	[BONUS_ALLOWED] [char](1) NULL,
	[REIMBREL] [char](15) NULL,
	[LEAVEREL] [char](15) NULL,
	[LEAVEPF] [char](1) NULL,
	[LVFIELD] [char](100) NULL,
	[gbasic] [numeric](3, 0) NULL,
	[gda] [numeric](3, 0) NULL,
	[ghra] [numeric](3, 0) NULL,
	[gconv] [numeric](3, 0) NULL,
	[gmed] [numeric](3, 0) NULL,
	[gEARN1] [numeric](3, 0) NULL,
	[gEARN2] [numeric](3, 0) NULL,
	[gEARN3] [numeric](3, 0) NULL,
	[gEARN4] [numeric](3, 0) NULL,
	[gEARN5] [numeric](3, 0) NULL,
	[gEARN6] [numeric](3, 0) NULL,
	[gEARN7] [numeric](3, 0) NULL,
	[gEARN8] [numeric](3, 0) NULL,
	[gEARN9] [numeric](3, 0) NULL,
	[gEARN10] [numeric](3, 0) NULL,
	[COM_CODE] [nchar](10) NULL,
	[ESIREL] [char](16) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_SUMMARY]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_SUMMARY](
	[PAYCODE] [char](10) NULL,
	[MON_YEAR] [datetime] NULL,
	[VSALARY] [numeric](11, 2) NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VHRA_AMT] [numeric](11, 2) NULL,
	[VDA_AMT] [numeric](11, 2) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[VOT_AMT] [numeric](11, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VVPF_AMT] [numeric](11, 2) NULL,
	[VESI_AMT] [numeric](11, 2) NULL,
	[VTDS_AMT] [numeric](11, 2) NULL,
	[VPROF_TAX_AMT] [numeric](11, 2) NULL,
	[VD_1_AMT] [numeric](11, 2) NULL,
	[VD_2_AMT] [numeric](11, 2) NULL,
	[VD_3_AMT] [numeric](11, 2) NULL,
	[VD_4_AMT] [numeric](11, 2) NULL,
	[VD_5_AMT] [numeric](11, 2) NULL,
	[VD_6_AMT] [numeric](11, 2) NULL,
	[VD_7_AMT] [numeric](11, 2) NULL,
	[VD_8_AMT] [numeric](11, 2) NULL,
	[VD_9_AMT] [numeric](11, 2) NULL,
	[VD_10_AMT] [numeric](11, 2) NULL,
	[VPRE] [numeric](11, 2) NULL,
	[VPF_RATE] [numeric](8, 2) NULL,
	[VESI_RATE] [numeric](8, 2) NULL,
	[VVPF_RATE] [numeric](8, 2) NULL,
	[EMPPF] [numeric](11, 2) NULL,
	[ARREAR_AMT] [numeric](11, 2) NULL,
	[ARREARPF_AMT] [numeric](11, 2) NULL,
	[ARREARVPF_AMT] [numeric](11, 2) NULL,
	[ARREARESI_AMT] [numeric](11, 2) NULL,
	[TADYS] [numeric](3, 0) NULL,
	[VTDAYS] [numeric](6, 2) NULL,
	[DEPARTMENTCODE] [char](3) NULL,
	[TOTEMPLOYEE] [numeric](4, 0) NULL,
	[ESIMEMBER] [numeric](4, 0) NULL,
	[PFMEMBER1] [numeric](4, 0) NULL,
	[PFMEMBER2] [numeric](4, 0) NULL,
	[VPFMEMBER1] [numeric](4, 0) NULL,
	[VPFMEMBER2] [numeric](4, 0) NULL,
	[VADVANCE] [numeric](11, 2) NULL,
	[VFINE] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL,
	[AMTONPF] [numeric](11, 2) NULL,
	[AMTONESI] [numeric](11, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAY_TEMP]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAY_TEMP](
	[PAYCODE] [char](10) NOT NULL,
	[VBASIC] [numeric](10, 2) NULL,
	[VSALARY] [numeric](11, 2) NULL,
	[VDA_RATE] [numeric](10, 2) NULL,
	[VDA_F] [char](1) NULL,
	[VDA_AMT] [numeric](11, 2) NULL,
	[VMED_RATE] [numeric](8, 2) NULL,
	[VMED_F] [char](1) NULL,
	[VMED_AMT] [numeric](11, 2) NULL,
	[VCONV_RATE] [numeric](8, 2) NULL,
	[VCONV_F] [char](1) NULL,
	[VCONV_AMT] [numeric](11, 2) NULL,
	[VHRA_RATE] [numeric](10, 2) NULL,
	[VHRA_F] [char](1) NULL,
	[VHRA_AMT] [numeric](11, 2) NULL,
	[VOT] [numeric](6, 2) NULL,
	[VOT_RATE] [numeric](6, 2) NULL,
	[VOT_FLAG] [char](1) NULL,
	[VOT_AMT] [numeric](11, 2) NULL,
	[VI_1] [numeric](8, 2) NULL,
	[VIT_1] [char](1) NULL,
	[VI_1_AMT] [numeric](11, 2) NULL,
	[VI_2] [numeric](8, 2) NULL,
	[VIT_2] [char](1) NULL,
	[VI_2_AMT] [numeric](11, 2) NULL,
	[VI_3] [numeric](8, 2) NULL,
	[VIT_3] [char](1) NULL,
	[VI_3_AMT] [numeric](11, 2) NULL,
	[VI_4] [numeric](8, 2) NULL,
	[VIT_4] [char](1) NULL,
	[VI_4_AMT] [numeric](11, 2) NULL,
	[VI_5] [numeric](8, 2) NULL,
	[VIT_5] [char](1) NULL,
	[VI_5_AMT] [numeric](11, 2) NULL,
	[VI_6] [numeric](8, 2) NULL,
	[VIT_6] [char](1) NULL,
	[VI_6_AMT] [numeric](11, 2) NULL,
	[VI_7] [numeric](8, 2) NULL,
	[VIT_7] [char](1) NULL,
	[VI_7_AMT] [numeric](11, 2) NULL,
	[VI_8] [numeric](8, 2) NULL,
	[VIT_8] [char](1) NULL,
	[VI_8_AMT] [numeric](11, 2) NULL,
	[VI_9] [numeric](8, 2) NULL,
	[VIT_9] [char](1) NULL,
	[VI_9_AMT] [numeric](11, 2) NULL,
	[VI_10] [numeric](8, 2) NULL,
	[VIT_10] [char](1) NULL,
	[VI_10_AMT] [numeric](11, 2) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL,
	[PROF_TAX_AMT] [numeric](11, 2) NULL,
	[VPF_AMT] [numeric](11, 2) NULL,
	[VVPF_AMT] [numeric](11, 2) NULL,
	[VEPF_AMT] [numeric](11, 2) NULL,
	[VFPF_AMT] [numeric](11, 2) NULL,
	[VESI_AMT] [numeric](11, 2) NULL,
	[VTDS_RATE] [numeric](8, 2) NULL,
	[VTDS_F] [char](1) NULL,
	[VTDS_AMT] [numeric](11, 2) NULL,
	[VDT_1] [char](1) NULL,
	[VD_1] [numeric](8, 2) NULL,
	[VD_1_AMT] [numeric](11, 2) NULL,
	[VDT_2] [char](1) NULL,
	[VD_2] [numeric](8, 2) NULL,
	[VD_2_AMT] [numeric](11, 2) NULL,
	[VDT_3] [char](1) NULL,
	[VD_3] [numeric](8, 2) NULL,
	[VD_3_AMT] [numeric](11, 2) NULL,
	[VDT_4] [char](1) NULL,
	[VD_4] [numeric](8, 2) NULL,
	[VD_4_AMT] [numeric](11, 2) NULL,
	[VDT_5] [char](1) NULL,
	[VD_5] [numeric](8, 2) NULL,
	[VD_5_AMT] [numeric](11, 2) NULL,
	[VDT_6] [char](1) NULL,
	[VD_6] [numeric](8, 2) NULL,
	[VD_6_AMT] [numeric](11, 2) NULL,
	[VDT_7] [char](1) NULL,
	[VD_7] [numeric](8, 2) NULL,
	[VD_7_AMT] [numeric](11, 2) NULL,
	[VDT_8] [char](1) NULL,
	[VD_8] [numeric](8, 2) NULL,
	[VD_8_AMT] [numeric](11, 2) NULL,
	[VDT_9] [char](1) NULL,
	[VD_9] [numeric](8, 2) NULL,
	[VD_9_AMT] [numeric](11, 2) NULL,
	[VDT_10] [char](1) NULL,
	[VD_10] [numeric](8, 2) NULL,
	[VD_10_AMT] [numeric](11, 2) NULL,
	[VFINE] [numeric](8, 2) NULL,
	[VADVANCE] [numeric](8, 2) NULL,
	[VPRE] [numeric](6, 2) NULL,
	[VABS] [numeric](6, 2) NULL,
	[VHLD] [numeric](6, 2) NULL,
	[VWO] [numeric](6, 2) NULL,
	[VCL] [numeric](6, 2) NULL,
	[VSL] [numeric](6, 2) NULL,
	[VPL_EL] [numeric](6, 2) NULL,
	[VOTHER_LV] [numeric](6, 2) NULL,
	[VLEAVE] [numeric](6, 2) NULL,
	[VTDAYS] [numeric](6, 2) NULL,
	[VTOTAL_DAY] [numeric](6, 2) NULL,
	[VLATE] [numeric](7, 2) NULL,
	[VT_LATE] [numeric](13, 2) NULL,
	[VEARLY] [numeric](7, 2) NULL,
	[VT_EARLY] [numeric](13, 2) NULL,
	[MON_YEAR] [datetime] NOT NULL,
	[AMTONPF] [numeric](11, 2) NULL,
	[AMTONVPF] [numeric](11, 2) NULL,
	[AMTONESI] [numeric](11, 2) NULL,
	[ESIONOT] [numeric](11, 2) NULL,
	[SSN] [nchar](50) NOT NULL,
	[COMPANYCODE] [nchar](10) NOT NULL,
	[VDES_1] [char](12) NULL,
	[VDES_2] [char](12) NULL,
	[VDES_3] [char](12) NULL,
	[VDES_4] [char](12) NULL,
	[VDES_5] [char](12) NULL,
	[VDES_6] [char](12) NULL,
	[VDES_7] [char](12) NULL,
	[VDES_8] [char](12) NULL,
	[VDES_9] [char](12) NULL,
	[VDES_10] [char](12) NULL,
	[VIES_1] [char](12) NULL,
	[VIES_2] [char](12) NULL,
	[VIES_3] [char](12) NULL,
	[VIES_4] [char](12) NULL,
	[VIES_5] [char](12) NULL,
	[VIES_6] [char](12) NULL,
	[VIES_7] [char](12) NULL,
	[VIES_8] [char](12) NULL,
	[VIES_9] [char](12) NULL,
	[VIES_10] [char](12) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhoneAPIServerConfig]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhoneAPIServerConfig](
	[ConfigId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyCode] [varchar](50) NULL,
	[StagingURL] [varchar](255) NULL,
	[ProductionURL] [varchar](255) NULL,
	[IsActive] [bit] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[TestingURL] [varchar](255) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UseTestMode] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhoneAttendence]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhoneAttendence](
	[AttendenceId] [int] IDENTITY(1,1) NOT NULL,
	[EmpCode] [varchar](50) NULL,
	[Latitude] [decimal](12, 9) NULL,
	[Longitude] [decimal](12, 9) NULL,
	[photo] [varbinary](max) NULL,
	[CompanyCode] [varchar](10) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[FileName] [varchar](255) NULL,
	[Process] [char](1) NULL,
	[AddressLine] [varchar](255) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[Country] [varchar](100) NULL,
	[ZipCode] [varchar](10) NULL,
	[KnownName] [varchar](255) NULL,
	[MacAddress] [varchar](30) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PROFESSIONALTAX]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PROFESSIONALTAX](
	[ID_NO] [int] NULL,
	[LOWERLIMIT] [numeric](11, 2) NULL,
	[UPPERLIMIT] [numeric](11, 2) NULL,
	[TAXAMOUNT] [numeric](11, 2) NULL,
	[BRANCHCODE] [char](3) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PwdUser]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PwdUser](
	[UserName] [nvarchar](10) NULL,
	[Pwd] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rawdata]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rawdata](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[P_DAY] [char](1) NULL,
	[ReasonCode] [char](3) NULL,
	[ERROR_CODE] [char](2) NOT NULL,
	[Id_No] [varchar](24) NOT NULL,
	[PROCESS] [char](1) NOT NULL,
	[DOOR_TIME] [char](5) NOT NULL,
	[Inout] [char](1) NULL,
	[sss] [char](25) NULL,
	[Created_Date] [datetime] NULL,
	[FName] [varchar](1) NULL,
	[Lcode] [char](4) NULL,
	[device_id] [varchar](24) NULL,
	[user_id] [varchar](24) NULL,
	[io_time] [datetime] NULL,
	[Audit_Action] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reason]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reason](
	[ReasonCode] [nvarchar](3) NULL,
	[CardNo] [nvarchar](8) NULL,
	[IsGroupReason] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegInfo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegInfo](
	[ServerID] [int] NULL,
	[UserID] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RunTimeMAdmin]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RunTimeMAdmin](
	[PAYCODE] [char](10) NULL,
	[CARDNO] [char](8) NULL,
	[EMPNAME] [char](25) NULL,
	[PUNCH1] [char](5) NULL,
	[PUNCH2] [char](5) NULL,
	[PUNCH3] [char](5) NULL,
	[PUNCH4] [char](5) NULL,
	[DATEOFFICE] [datetime] NULL,
	[PUNCH1M] [char](3) NULL,
	[PUNCH2M] [char](3) NULL,
	[PUNCH3M] [char](3) NULL,
	[PUNCH4M] [char](3) NULL,
	[COMPANYCODE] [char](3) NULL,
	[DEPARTMENTCODE] [char](3) NULL,
	[CAT] [char](3) NULL,
	[INOUT] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SerialKeyValidator]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SerialKeyValidator](
	[SerialNumber] [varchar](50) NOT NULL,
	[isActive] [bit] NULL,
	[ActivatedOn] [datetime] NULL,
	[ExpiredOn] [datetime] NULL,
	[CompanyCode] [char](10) NULL,
 CONSTRAINT [PK_SerialKeyValidator] PRIMARY KEY CLUSTERED 
(
	[SerialNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServiceUpdateTime]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceUpdateTime](
	[Updatetime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SystemInfo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemInfo](
	[Auto_Del_DL] [int] NULL,
	[Backup_Path] [nvarchar](255) NULL,
	[Disconnect] [int] NULL,
	[Auto_DownLoad] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fk_D_UserFP]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fk_D_UserFP](
	[device_id] [varchar](50) NOT NULL,
	[user_id] [varchar](50) NOT NULL,
	[backup_number] [int] NULL,
	[audit_action] [varchar](50) NULL,
	[update_time] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fkcmd_trans]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fkcmd_trans](
	[trans_id] [varchar](16) NOT NULL,
	[device_id] [varchar](24) NULL,
	[cmd_code] [varchar](32) NOT NULL,
	[return_code] [varchar](64) NULL,
	[status] [varchar](16) NOT NULL,
	[update_time] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fkcmd_trans_cmd_param]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fkcmd_trans_cmd_param](
	[trans_id] [varchar](16) NOT NULL,
	[device_id] [varchar](24) NOT NULL,
	[cmd_param] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fkcmd_trans_cmd_result]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fkcmd_trans_cmd_result](
	[trans_id] [varchar](16) NOT NULL,
	[device_id] [varchar](24) NOT NULL,
	[cmd_result] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fkcmd_trans_cmd_result_log_data]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fkcmd_trans_cmd_result_log_data](
	[trans_id] [nvarchar](16) NOT NULL,
	[device_id] [nvarchar](24) NOT NULL,
	[user_id] [nvarchar](16) NULL,
	[verify_mode] [nvarchar](64) NULL,
	[io_mode] [nvarchar](32) NULL,
	[io_time] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fkcmd_trans_cmd_result_user_id_list]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fkcmd_trans_cmd_result_user_id_list](
	[trans_id] [nvarchar](16) NOT NULL,
	[device_id] [nvarchar](24) NOT NULL,
	[user_id] [nvarchar](16) NULL,
	[backup_number] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_fkdevice_status]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_fkdevice_status](
	[device_id] [varchar](24) NOT NULL,
	[device_name] [varchar](24) NOT NULL,
	[connected] [int] NOT NULL,
	[last_update_time] [datetime] NOT NULL,
	[last_update_fk_time] [datetime] NULL,
	[device_info] [nvarchar](2048) NULL,
	[user_count] [int] NULL,
	[manager_count] [int] NULL,
	[fp_count] [int] NULL,
	[face_count] [int] NULL,
	[password_count] [int] NULL,
	[idcard_count] [int] NULL,
	[total_log_count] [int] NULL,
	[total_user_count] [int] NULL,
	[IsMasterDevice] [char](1) NULL,
	[GroupID] [varchar](500) NULL,
	[IPAddrss] [varchar](20) NULL,
	[IsAuthorized] [char](1) NULL,
	[TimeZone] [int] NULL,
	[CompanyCode] [varchar](5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_realtime_enroll_data]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_realtime_enroll_data](
	[update_time] [datetime] NOT NULL,
	[device_id] [varchar](24) NOT NULL,
	[user_id] [varchar](24) NOT NULL,
	[user_data] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_realtime_glog]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_realtime_glog](
	[update_time] [datetime] NOT NULL,
	[device_id] [varchar](24) NULL,
	[user_id] [varchar](24) NULL,
	[verify_mode] [varchar](64) NULL,
	[io_mode] [varchar](32) NULL,
	[io_time] [datetime] NULL,
	[log_image] [varbinary](max) NULL,
	[io_workcode] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_realtime_userinfo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_realtime_userinfo](
	[user_id] [varchar](60) NOT NULL,
	[user_name] [varchar](50) NULL,
	[privilige] [varchar](50) NULL,
	[card] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Face] [varbinary](max) NULL,
	[photo] [varbinary](max) NULL,
	[Fp_0] [varbinary](max) NULL,
	[Fp_1] [varbinary](max) NULL,
	[Fp_2] [varbinary](max) NULL,
	[Fp_3] [varbinary](max) NULL,
	[Fp_4] [varbinary](max) NULL,
	[Fp_5] [varbinary](max) NULL,
	[Fp_6] [varbinary](max) NULL,
	[Fp_7] [varbinary](max) NULL,
	[Fp_8] [varbinary](max) NULL,
	[Fp_9] [varbinary](max) NULL,
	[Device_ID] [varchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RunSetup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RunSetup](
	[IsRun] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAbsentSMS]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAbsentSMS](
	[DateOffice] [datetime] NULL,
	[AbsentSms] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLADVANCE]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLADVANCE](
	[PAYCODE] [char](10) NOT NULL,
	[A_L] [char](1) NULL,
	[IDNO] [numeric](7, 0) NOT NULL,
	[MON_YEAR] [datetime] NULL,
	[Entry_Date] [datetime] NULL,
	[Tran_Month] [datetime] NULL,
	[ADV_AMT] [numeric](10, 2) NULL,
	[INST_AMT] [numeric](7, 2) NULL,
	[INSTNO] [numeric](3, 0) NULL,
	[INTREST_RATE] [numeric](5, 2) NULL,
	[SSN] [nchar](50) NULL,
	[COMPANYCODE] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLADVANCEDATA]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLADVANCEDATA](
	[PAYCODE] [char](10) NOT NULL,
	[A_L] [char](1) NULL,
	[IDNO] [numeric](7, 0) NOT NULL,
	[MON_YEAR] [datetime] NOT NULL,
	[INST_AMT] [numeric](7, 2) NULL,
	[CASH_AMT] [numeric](7, 2) NULL,
	[BALANCE_AMT] [numeric](10, 2) NULL,
	[PAMT] [numeric](7, 2) NULL,
	[IAMT] [numeric](7, 2) NULL,
	[INSTNO] [numeric](3, 0) NULL,
	[SSN] [nchar](50) NULL,
	[COMPANYCODE] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAtt_Mail]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAtt_Mail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Maildate] [datetime] NULL,
	[Sent] [char](1) NULL,
	[CompanyCode] [char](5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBank]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBank](
	[BANKCODE] [char](3) NOT NULL,
	[BANKNAME] [char](50) NULL,
	[BANKADDRESS] [char](150) NULL,
	[SHORTNAME] [char](10) NULL,
	[CompanyCode] [nchar](10) NOT NULL,
 CONSTRAINT [PK_tblBank] PRIMARY KEY CLUSTERED 
(
	[BANKCODE] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblbranch]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblbranch](
	[BRANCHCODE] [char](3) NOT NULL,
	[BRANCHNAME] [char](35) NULL,
	[COMPANYCODE] [nchar](10) NOT NULL,
 CONSTRAINT [PK_tblbranch] PRIMARY KEY CLUSTERED 
(
	[BRANCHCODE] ASC,
	[COMPANYCODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCalander]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCalander](
	[MDate] [datetime] NOT NULL,
	[PROCess] [char](1) NULL,
	[NRTCPROC] [char](1) NULL,
	[AbsentMail] [char](1) NULL,
	[AutoEL] [char](1) NULL,
	[Autoleaveapproval] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLCANTEEN]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLCANTEEN](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[ISMANUAL] [char](1) NULL,
	[SHIFT] [char](3) NULL,
	[PURPOSE] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCatagory]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCatagory](
	[CAT] [char](3) NOT NULL,
	[CATAGORYNAME] [varchar](500) NULL,
	[Days180] [char](1) NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblCatagory] PRIMARY KEY CLUSTERED 
(
	[CAT] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblCommSetting]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblCommSetting](
	[Port] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCompany]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompany](
	[COMPANYCODE] [char](4) NOT NULL,
	[COMPANYNAME] [char](50) NULL,
	[COMPANYADDRESS] [varchar](150) NULL,
	[SHORTNAME] [char](10) NOT NULL,
	[PANNUM] [char](25) NULL,
	[TANNUMBER] [char](50) NULL,
	[TDSCIRCLE] [char](25) NULL,
	[LCNO] [char](25) NULL,
	[PFNO] [char](12) NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[active] [varchar](1) NULL,
	[DCode] [char](10) NULL,
 CONSTRAINT [PK_tblCompany] PRIMARY KEY CLUSTERED 
(
	[COMPANYCODE] ASC,
	[SHORTNAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCreditLeave]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCreditLeave](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[LYear] [bigint] NOT NULL,
	[LMonth] [bigint] NOT NULL,
	[Credit] [char](1) NULL,
	[LateAttendance] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblcustomer]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblcustomer](
	[PAYCODE] [char](10) NOT NULL,
	[CARDNO] [char](8) NULL,
	[EMPLOYEE_NAME] [char](25) NULL,
	[FATHER_NAME] [char](30) NULL,
	[DATEOFBIRTH] [datetime] NULL,
	[DATEOFJOIN] [datetime] NULL,
	[LEAVINGDATE] [datetime] NULL,
	[LEAVINGREASON] [char](35) NULL,
	[COMPANYNAME] [char](50) NULL,
	[DEPARTMENTNAME] [char](45) NULL,
	[CATAGORYNAME] [char](35) NULL,
	[DIVISIONNAME] [char](45) NULL,
	[GRADENAME] [char](45) NULL,
	[SEX] [char](1) NULL,
	[MARRIED] [char](1) NULL,
	[BUS] [char](10) NULL,
	[QUALIFICATION] [char](20) NULL,
	[EXPERIENCE] [char](20) NULL,
	[DESIGNATION] [char](25) NULL,
	[ADDRESS1] [char](100) NULL,
	[PINCODE1] [char](8) NULL,
	[TELEPHONE1] [char](20) NULL,
	[E_MAIL1] [char](50) NULL,
	[ADDRESS2] [char](100) NULL,
	[PINCODE2] [char](10) NULL,
	[TELEPHONE2] [char](20) NULL,
	[BLOODGROUP] [char](3) NULL,
	[GROSS] [numeric](10, 2) NULL,
	[BASIC] [numeric](10, 2) NULL,
	[PAYMENTBY] [char](1) NULL,
	[BANKACC] [char](25) NULL,
	[EMPLOYEETYPE] [char](1) NULL,
	[PF_CODE] [char](12) NULL,
	[PF_NO] [numeric](5, 0) NULL,
	[ESI_NO] [char](12) NULL,
	[PAN_NO] [char](12) NULL,
	[DA_RATE] [numeric](10, 2) NULL,
	[HRA_RATE] [numeric](10, 2) NULL,
	[CONV_RATE] [numeric](8, 2) NULL,
	[MED_RATE] [numeric](8, 2) NULL,
	[EARNING_1] [numeric](8, 2) NULL,
	[EARNING_2] [numeric](8, 2) NULL,
	[EARNING_3] [numeric](8, 2) NULL,
	[EARNING_4] [numeric](8, 2) NULL,
	[EARNING_5] [numeric](8, 2) NULL,
	[EARNING_6] [numeric](8, 2) NULL,
	[EARNING_7] [numeric](8, 2) NULL,
	[EARNING_8] [numeric](8, 2) NULL,
	[EARNING_9] [numeric](8, 2) NULL,
	[EARNING_10] [numeric](8, 2) NULL,
	[DEDUCTION_1] [numeric](8, 2) NULL,
	[DEDUCTION_2] [numeric](8, 2) NULL,
	[DEDUCTION_3] [numeric](8, 2) NULL,
	[DEDUCTION_4] [numeric](8, 2) NULL,
	[DEDUCTION_5] [numeric](8, 2) NULL,
	[DEDUCTION_6] [numeric](8, 2) NULL,
	[DEDUCTION_7] [numeric](8, 2) NULL,
	[DEDUCTION_8] [numeric](8, 2) NULL,
	[DEDUCTION_9] [numeric](8, 2) NULL,
	[DEDUCTION_10] [numeric](8, 2) NULL,
	[TDS_RATE] [numeric](8, 2) NULL,
	[PF_ALLOWED] [char](1) NULL,
	[ESI_ALLOWED] [char](1) NULL,
	[VPF_ALLOWED] [char](1) NULL,
	[BONUS_ALLOWED] [char](1) NULL,
	[GRADUTY_ALLOWED] [char](1) NULL,
	[PROF_TAX_ALLOWED] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDepartment]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDepartment](
	[DepartmentCode] [char](3) NOT NULL,
	[DepartmentName] [char](45) NULL,
	[DEPARTMENTHEAD] [char](35) NULL,
	[EMAIL_ID] [nchar](35) NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblDepartment] PRIMARY KEY CLUSTERED 
(
	[DepartmentCode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDependant]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDependant](
	[PayCode] [char](10) NULL,
	[CompanyCode] [char](3) NULL,
	[Name] [varchar](30) NULL,
	[DOB] [datetime] NULL,
	[Relation] [varchar](20) NULL,
	[IsResidingWith] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDESPANSARY]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDESPANSARY](
	[DESPANSARYCODE] [char](3) NOT NULL,
	[DESPANSARYNAME] [char](50) NULL,
	[DESPANSARYADDRESS] [char](150) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDeviceStatus]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDeviceStatus](
	[SerialNumber] [varchar](50) NULL,
	[ConnectionTime] [datetime] NULL,
	[Status] [char](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDivision]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDivision](
	[Divisioncode] [char](3) NOT NULL,
	[DivisionName] [char](45) NULL,
	[GraceDays] [float] NULL,
	[StartCoreTime] [datetime] NULL,
	[EndCoreTime] [datetime] NULL,
	[MaxFlexiHrs] [bigint] NULL,
	[MaxFlexiHrsWeek] [bigint] NULL,
	[ValidTimeEarly] [datetime] NULL,
	[ValidTimeDept] [datetime] NULL,
	[isFH] [char](1) NULL,
	[maxFH] [bigint] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblDivision] PRIMARY KEY CLUSTERED 
(
	[Divisioncode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblEMPHOLIDAY]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblEMPHOLIDAY](
	[paycode] [char](10) NULL,
	[hdate] [smalldatetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblemployee]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblemployee](
	[ACTIVE] [char](1) NULL,
	[PAYCODE] [char](10) NOT NULL,
	[EMPNAME] [char](25) NULL,
	[GUARDIANNAME] [char](30) NULL,
	[DateOFBIRTH] [datetime] NULL,
	[DateOFJOIN] [datetime] NULL,
	[PRESENTCARDNO] [char](8) NOT NULL,
	[COMPANYCODE] [char](4) NULL,
	[DivisionCode] [char](3) NULL,
	[CAT] [char](3) NULL,
	[SEX] [char](1) NULL,
	[ISMARRIED] [char](1) NULL,
	[BUS] [char](10) NULL,
	[QUALIFICATION] [char](20) NULL,
	[EXPERIENCE] [char](20) NULL,
	[DESIGNATION] [char](50) NULL,
	[ADDRESS1] [char](100) NULL,
	[PINCODE1] [char](8) NULL,
	[TELEPHONE1] [char](20) NULL,
	[E_MAIL1] [char](50) NULL,
	[ADDRESS2] [char](100) NULL,
	[PINCODE2] [char](10) NULL,
	[TELEPHONE2] [char](20) NULL,
	[BLOODGROUP] [char](3) NULL,
	[EMPPHOTO] [char](100) NULL,
	[EMPSIGNATURE] [char](100) NULL,
	[DepartmentCode] [char](3) NULL,
	[GradeCode] [char](3) NULL,
	[Leavingdate] [datetime] NULL,
	[LeavingReason] [char](35) NULL,
	[VehicleNo] [char](15) NULL,
	[PFNO] [char](12) NULL,
	[PF_NO] [numeric](5, 0) NULL,
	[ESINO] [char](12) NULL,
	[AUTHORISEDMACHINE] [char](150) NULL,
	[EMPTYPE] [char](1) NULL,
	[BankAcc] [char](25) NULL,
	[bankCODE] [char](3) NULL,
	[Reporting1] [varchar](10) NULL,
	[Reporting2] [varchar](10) NULL,
	[BRANCHCODE] [char](3) NULL,
	[DESPANSARYCODE] [char](3) NULL,
	[headid] [char](10) NULL,
	[Email_CC] [varchar](100) NULL,
	[LeaveApprovalStages] [char](2) NULL,
	[IsFixedShift] [char](1) NULL,
	[involuntary] [nvarchar](2) NULL,
	[EmpBioData] [varchar](200) NULL,
	[dateofexp] [datetime] NULL,
	[Disable] [char](1) NULL,
	[Group_Index] [char](1) NULL,
	[OnRoll] [char](1) NULL,
	[teamid] [char](5) NULL,
	[PSCODE] [char](5) NULL,
	[SUBAREACODE] [char](10) NULL,
	[SECTIONID] [char](10) NULL,
	[POSITIONCODE] [char](10) NULL,
	[trainer] [char](1) NULL,
	[trainee] [char](1) NULL,
	[TraineeLevel] [char](1) NULL,
	[RockWellid] [char](20) NULL,
	[Template_Send] [char](1) NULL,
	[Grade] [int] NULL,
	[headid_2] [varchar](10) NULL,
	[head_id] [char](10) NULL,
	[HOD_Codd] [varchar](10) NULL,
	[HOD_Code] [varchar](10) NULL,
	[logid] [bigint] NULL,
	[GroupCode] [char](3) NULL,
	[ClassCode] [char](3) NULL,
	[ReligionCode] [char](3) NULL,
	[ValidityStartDate] [datetime] NULL,
	[ValidityEndDate] [datetime] NULL,
	[SSN] [varchar](100) NOT NULL,
	[DeviceGroupID] [int] NULL,
	[ClientID] [int] NULL,
	[GroupID] [varchar](3) NULL,
 CONSTRAINT [PK_tblemployee] PRIMARY KEY CLUSTERED 
(
	[PRESENTCARDNO] ASC,
	[SSN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEmployeeGroupPolicy]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmployeeGroupPolicy](
	[GroupID] [varchar](50) NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTTYPE] [char](1) NULL,
	[SHIFTPATTERN] [char](11) NULL,
	[SHIFTREMAINDAYS] [int] NULL,
	[LASTSHIFTPERFORMED] [char](3) NULL,
	[INONLY] [char](1) NULL,
	[ISPUNCHALL] [char](1) NULL,
	[ISTIMELOSSALLOWED] [char](1) NULL,
	[ALTERNATE_OFF_DAYS] [char](10) NULL,
	[CDAYS] [float] NULL,
	[ISROUNDTHECLOCKWORK] [char](1) NULL,
	[ISOT] [char](1) NULL,
	[OTRATE] [char](6) NULL,
	[FIRSTOFFDAY] [char](3) NULL,
	[SECONDOFFTYPE] [char](1) NULL,
	[HALFDAYSHIFT] [char](3) NULL,
	[SECONDOFFDAY] [char](3) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[PERMISEARLYDEPRT] [int] NULL,
	[ISAUTOSHIFT] [char](1) NULL,
	[ISOUTWORK] [char](1) NULL,
	[MAXDAYMIN] [float] NULL,
	[ISOS] [char](1) NULL,
	[AUTH_SHIFTS] [char](50) NULL,
	[TIME] [int] NULL,
	[SHORT] [int] NULL,
	[HALF] [int] NULL,
	[ISHALFDAY] [char](1) NULL,
	[ISSHORT] [char](1) NULL,
	[TWO] [char](1) NULL,
	[isReleaver] [char](1) NULL,
	[isWorker] [char](1) NULL,
	[isFlexi] [char](1) NULL,
	[SSN] [varchar](100) NOT NULL,
	[MIS] [char](1) NULL,
	[IsCOF] [char](1) NULL,
	[HLFAfter] [int] NULL,
	[HLFBefore] [int] NULL,
	[ResignedAfter] [int] NULL,
	[EnableAutoResign] [char](1) NULL,
	[S_END] [datetime] NULL,
	[S_OUT] [datetime] NULL,
	[AUTOSHIFT_LOW] [int] NULL,
	[AUTOSHIFT_UP] [int] NULL,
	[ISPRESENTONWOPRESENT] [char](1) NULL,
	[ISPRESENTONHLDPRESENT] [char](1) NULL,
	[NightShiftFourPunch] [char](1) NULL,
	[ISAUTOABSENT] [char](1) NULL,
	[ISAWA] [char](1) NULL,
	[ISWA] [char](1) NULL,
	[ISAW] [char](1) NULL,
	[ISPREWO] [char](1) NULL,
	[ISOTOUTMINUSSHIFTENDTIME] [char](1) NULL,
	[ISOTWRKGHRSMINUSSHIFTHRS] [char](1) NULL,
	[ISOTEARLYCOMEPLUSLATEDEP] [char](1) NULL,
	[DEDUCTHOLIDAYOT] [int] NULL,
	[DEDUCTWOOT] [int] NULL,
	[ISOTEARLYCOMING] [char](1) NULL,
	[OTMinus] [char](1) NULL,
	[OTROUND] [char](1) NULL,
	[OTEARLYDUR] [int] NULL,
	[OTLATECOMINGDUR] [int] NULL,
	[OTRESTRICTENDDUR] [int] NULL,
	[DUPLICATECHECKMIN] [int] NULL,
	[PREWO] [int] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblEmployeeGroupPolicy] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEmployeeShiftMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmployeeShiftMaster](
	[PAYCODE] [char](10) NOT NULL,
	[CARDNO] [char](8) NOT NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTTYPE] [char](1) NULL,
	[SHIFTPATTERN] [char](11) NULL,
	[SHIFTREMAINDAYS] [int] NULL,
	[LASTSHIFTPERFORMED] [char](3) NULL,
	[INONLY] [char](1) NULL,
	[ISPUNCHALL] [char](1) NULL,
	[ISTIMELOSSALLOWED] [char](1) NULL,
	[ALTERNATE_OFF_DAYS] [char](10) NULL,
	[CDAYS] [float] NULL,
	[ISROUNDTHECLOCKWORK] [char](1) NULL,
	[ISOT] [char](1) NULL,
	[OTRATE] [char](6) NULL,
	[FIRSTOFFDAY] [char](3) NULL,
	[SECONDOFFTYPE] [char](1) NULL,
	[HALFDAYSHIFT] [char](3) NULL,
	[SECONDOFFDAY] [char](3) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[PERMISEARLYDEPRT] [int] NULL,
	[ISAUTOSHIFT] [char](1) NULL,
	[ISOUTWORK] [char](1) NULL,
	[MAXDAYMIN] [float] NULL,
	[ISOS] [char](1) NULL,
	[AUTH_SHIFTS] [char](50) NULL,
	[TIME] [int] NULL,
	[SHORT] [int] NULL,
	[HALF] [int] NULL,
	[ISHALFDAY] [char](1) NULL,
	[ISSHORT] [char](1) NULL,
	[TWO] [char](1) NULL,
	[isReleaver] [char](1) NULL,
	[isWorker] [char](1) NULL,
	[isFlexi] [char](1) NULL,
	[SSN] [varchar](100) NOT NULL,
	[MIS] [char](1) NULL,
	[IsCOF] [char](1) NULL,
	[HLFAfter] [int] NULL,
	[HLFBefore] [int] NULL,
	[ResignedAfter] [int] NULL,
	[EnableAutoResign] [char](1) NULL,
	[S_END] [datetime] NULL,
	[S_OUT] [datetime] NULL,
	[AUTOSHIFT_LOW] [int] NULL,
	[AUTOSHIFT_UP] [int] NULL,
	[ISPRESENTONWOPRESENT] [char](1) NULL,
	[ISPRESENTONHLDPRESENT] [char](1) NULL,
	[NightShiftFourPunch] [char](1) NULL,
	[ISAUTOABSENT] [char](1) NULL,
	[ISAWA] [char](1) NULL,
	[ISWA] [char](1) NULL,
	[ISAW] [char](1) NULL,
	[ISPREWO] [char](1) NULL,
	[ISOTOUTMINUSSHIFTENDTIME] [char](1) NULL,
	[ISOTWRKGHRSMINUSSHIFTHRS] [char](1) NULL,
	[ISOTEARLYCOMEPLUSLATEDEP] [char](1) NULL,
	[DEDUCTHOLIDAYOT] [int] NULL,
	[DEDUCTWOOT] [int] NULL,
	[ISOTEARLYCOMING] [char](1) NULL,
	[OTMinus] [char](1) NULL,
	[OTROUND] [char](1) NULL,
	[OTEARLYDUR] [int] NULL,
	[OTLATECOMINGDUR] [int] NULL,
	[OTRESTRICTENDDUR] [int] NULL,
	[DUPLICATECHECKMIN] [int] NULL,
	[PREWO] [int] NULL,
 CONSTRAINT [PK_tblEmployeeShiftMaster] PRIMARY KEY CLUSTERED 
(
	[CARDNO] ASC,
	[SSN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEmployeeTemp]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmployeeTemp](
	[ACTIVE] [char](1) NULL,
	[PAYCODE] [char](10) NOT NULL,
	[EMPNAME] [char](25) NULL,
	[GUARDIANNAME] [char](30) NULL,
	[DateOFBIRTH] [datetime] NULL,
	[DateOFJOIN] [datetime] NULL,
	[PRESENTCARDNO] [char](8) NOT NULL,
	[COMPANYCODE] [char](4) NULL,
	[DivisionCode] [char](3) NULL,
	[CAT] [char](3) NULL,
	[SEX] [char](1) NULL,
	[ISMARRIED] [char](1) NULL,
	[BUS] [char](10) NULL,
	[QUALIFICATION] [char](20) NULL,
	[EXPERIENCE] [char](20) NULL,
	[DESIGNATION] [char](50) NULL,
	[ADDRESS1] [char](100) NULL,
	[PINCODE1] [char](8) NULL,
	[TELEPHONE1] [char](20) NULL,
	[E_MAIL1] [char](50) NULL,
	[ADDRESS2] [char](100) NULL,
	[PINCODE2] [char](10) NULL,
	[TELEPHONE2] [char](20) NULL,
	[BLOODGROUP] [char](3) NULL,
	[EMPPHOTO] [char](100) NULL,
	[EMPSIGNATURE] [char](100) NULL,
	[DepartmentCode] [char](3) NULL,
	[GradeCode] [char](3) NULL,
	[Leavingdate] [datetime] NULL,
	[LeavingReason] [char](35) NULL,
	[VehicleNo] [char](15) NULL,
	[PFNO] [char](12) NULL,
	[PF_NO] [numeric](5, 0) NULL,
	[ESINO] [char](12) NULL,
	[AUTHORISEDMACHINE] [char](150) NULL,
	[EMPTYPE] [char](1) NULL,
	[BankAcc] [char](25) NULL,
	[bankCODE] [char](3) NULL,
	[Reporting1] [varchar](10) NULL,
	[Reporting2] [varchar](10) NULL,
	[BRANCHCODE] [char](3) NULL,
	[DESPANSARYCODE] [char](3) NULL,
	[headid] [varchar](20) NULL,
	[Email_CC] [varchar](100) NULL,
	[LeaveApprovalStages] [char](2) NULL,
	[IsFixedShift] [char](1) NULL,
	[involuntary] [nvarchar](2) NULL,
	[EmpBioData] [varchar](200) NULL,
	[dateofexp] [datetime] NULL,
	[Disable] [char](1) NULL,
	[Group_Index] [char](1) NULL,
	[OnRoll] [char](1) NULL,
	[teamid] [char](5) NULL,
	[PSCODE] [char](5) NULL,
	[SUBAREACODE] [char](10) NULL,
	[SECTIONID] [char](10) NULL,
	[POSITIONCODE] [char](10) NULL,
	[trainer] [char](1) NULL,
	[trainee] [char](1) NULL,
	[TraineeLevel] [char](1) NULL,
	[RockWellid] [char](20) NULL,
	[Template_Send] [char](1) NULL,
	[Grade] [int] NULL,
	[headid_2] [varchar](10) NULL,
	[head_id] [char](10) NULL,
	[HOD_Codd] [varchar](10) NULL,
	[HOD_Code] [varchar](10) NULL,
	[logid] [bigint] NULL,
	[GroupCode] [char](3) NULL,
	[ClassCode] [char](3) NULL,
	[ReligionCode] [char](3) NULL,
	[ValidityStartDate] [datetime] NULL,
	[ValidityEndDate] [datetime] NULL,
	[SSN] [varchar](100) NOT NULL,
	[DeviceGroupID] [int] NULL,
	[ClientID] [int] NULL,
	[EmployeeGroup] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLFORM10]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLFORM10](
	[PAYCODE] [char](10) NULL,
	[EMPNAME] [varchar](30) NULL,
	[GUARDIANNAME] [varchar](30) NULL,
	[DATEOFBIRTH] [datetime] NULL,
	[DATEOFJOIN] [datetime] NULL,
	[LEAVINGDATE] [datetime] NULL,
	[SEX] [char](1) NULL,
	[LeavingReason] [varchar](30) NULL,
	[VCPF_NO] [varchar](12) NULL,
	[VPF_NO] [int] NULL,
	[companyname] [varchar](50) NULL,
	[COMPANYADDRESS] [varchar](150) NULL,
	[pfno] [varchar](12) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblFTPSetup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFTPSetup](
	[EnableFTP] [char](1) NULL,
	[FTPServer] [varchar](100) NULL,
	[FTPUser] [varchar](50) NULL,
	[FTPPassword] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblFunctionCall]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblFunctionCall](
	[paycode] [char](10) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[FunctionName] [varchar](50) NULL,
	[CompanyCode] [varchar](4) NULL,
	[DepartmentCode] [char](3) NULL,
	[UsbFileName] [varchar](200) NULL,
	[CaptureData] [char](1) NULL,
	[Called] [char](1) NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Divisioncode] [char](3) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGrade]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGrade](
	[GradeCode] [char](3) NOT NULL,
	[GradeName] [char](45) NULL,
	[PERMISLATEARRIVAL] [int] NULL,
	[Grade] [int] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblGrade] PRIMARY KEY CLUSTERED 
(
	[GradeCode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblGroupMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGroupMaster](
	[GroupCode] [char](3) NULL,
	[GroupName] [char](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblHistory]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblHistory](
	[Terminalid] [char](10) NULL,
	[UserID] [char](10) NULL,
	[PunchTime] [datetime] NULL,
	[Door] [char](10) NULL,
	[Inout] [char](1) NULL,
	[STATUS] [char](100) NULL,
	[ISFP] [char](1) NULL,
	[Paycode] [char](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblHODremarks]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblHODremarks](
	[USER_R] [char](20) NOT NULL,
	[Date] [datetime] NULL,
	[Remarks] [char](250) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblimage]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblimage](
	[paycode] [char](10) NOT NULL,
	[photo] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblJOBMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblJOBMaster](
	[JOBCode] [char](3) NULL,
	[JOBName] [char](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblleaveledger]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblleaveledger](
	[PAYCODE] [char](10) NOT NULL,
	[L01] [float] NULL,
	[L02] [float] NULL,
	[L03] [float] NULL,
	[L04] [float] NULL,
	[L05] [float] NULL,
	[L06] [float] NULL,
	[L07] [float] NULL,
	[L08] [float] NULL,
	[L09] [float] NULL,
	[L10] [float] NULL,
	[L11] [float] NULL,
	[L12] [float] NULL,
	[L13] [float] NULL,
	[L14] [float] NULL,
	[L15] [float] NULL,
	[L16] [float] NULL,
	[L17] [float] NULL,
	[L18] [float] NULL,
	[L19] [float] NULL,
	[L20] [float] NULL,
	[L01_ADD] [float] NULL,
	[L02_ADD] [float] NULL,
	[L03_ADD] [float] NULL,
	[L04_ADD] [float] NULL,
	[L05_ADD] [float] NULL,
	[L06_ADD] [float] NULL,
	[L07_ADD] [float] NULL,
	[L08_ADD] [float] NULL,
	[L09_ADD] [float] NULL,
	[L10_ADD] [float] NULL,
	[L11_ADD] [float] NULL,
	[L12_ADD] [float] NULL,
	[L13_ADD] [float] NULL,
	[L14_ADD] [float] NULL,
	[L15_ADD] [float] NULL,
	[L16_ADD] [float] NULL,
	[L17_ADD] [float] NULL,
	[L18_ADD] [float] NULL,
	[L19_ADD] [float] NULL,
	[L20_ADD] [float] NULL,
	[LYEAR] [numeric](4, 0) NOT NULL,
	[ACC_FLAG] [char](1) NULL,
	[L01_CASH] [float] NULL,
	[L02_CASH] [float] NULL,
	[L03_CASH] [float] NULL,
	[L04_CASH] [float] NULL,
	[L05_CASH] [float] NULL,
	[L06_CASH] [float] NULL,
	[L07_CASH] [float] NULL,
	[L08_CASH] [float] NULL,
	[L09_CASH] [float] NULL,
	[L10_CASH] [float] NULL,
	[L11_CASH] [float] NULL,
	[L12_CASH] [float] NULL,
	[L13_CASH] [float] NULL,
	[L14_CASH] [float] NULL,
	[L15_CASH] [float] NULL,
	[L16_CASH] [float] NULL,
	[L17_CASH] [float] NULL,
	[L18_CASH] [float] NULL,
	[L19_CASH] [float] NULL,
	[L20_CASH] [float] NULL,
	[HOLIDAYS] [float] NULL,
	[ELBal] [float] NULL,
	[ELMonth] [int] NULL,
	[SSN] [varchar](100) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblleavemaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblleavemaster](
	[LEAVEFIELD] [char](3) NOT NULL,
	[LEAVECODE] [char](3) NULL,
	[LEAVEDESCRIPTION] [char](50) NULL,
	[ISOFFINCLUDE] [char](1) NULL,
	[ISHOLIDAYINCLUDE] [char](1) NULL,
	[ISLEAVEACCRUAL] [char](1) NULL,
	[LEAVETYPE] [char](1) NULL,
	[SMIN] [numeric](7, 2) NULL,
	[SMAX] [numeric](7, 2) NULL,
	[PRESENT] [numeric](7, 2) NULL,
	[LEAVE] [numeric](7, 2) NULL,
	[LEAVELIMIT] [numeric](7, 2) NULL,
	[FIXED] [char](1) NULL,
	[showonweb] [char](1) NULL,
	[isMonthly] [char](1) NULL,
	[isCompOffType] [char](1) NULL,
	[ExpiryDays] [numeric](7, 2) NULL,
	[CompanyCode] [varchar](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLLINK]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLLINK](
	[PAYOUT_FLD] [char](200) NULL,
	[USER_FLD] [char](20) NULL,
	[FILE_NAME] [char](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLog]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLog](
	[LogId] [int] NULL,
	[UserId] [char](10) NULL,
	[LogDate] [datetime] NULL,
	[MTable] [varchar](30) NULL,
	[Task] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblLog_ManualPunch]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLog_ManualPunch](
	[LogId] [bigint] IDENTITY(1,1) NOT NULL,
	[Paycode] [char](10) NULL,
	[PunchDate] [datetime] NULL,
	[LoggedUser] [varchar](10) NULL,
	[LoggedDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMachine]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMachine](
	[ID_NO] [numeric](18, 0) NOT NULL,
	[A_R] [char](1) NULL,
	[IN_OUT] [char](1) NULL,
	[LOCATION] [char](30) NULL,
	[commkey] [numeric](18, 0) NULL,
	[isfp] [char](1) NULL,
	[MACHINELOCATION] [char](50) NULL,
	[branch] [char](50) NULL,
	[MAC_ADDRESS] [char](50) NULL,
	[MachineType] [char](1) NULL,
	[CompanyCode] [varchar](4) NOT NULL,
	[Reader] [char](10) NULL,
	[ClientID] [int] NULL,
	[SerialNumber] [varchar](50) NULL,
	[DeviceName] [varchar](50) NULL,
	[TransactionCount] [varchar](50) NULL,
	[FPCount] [varchar](50) NULL,
	[UserCount] [varchar](50) NULL,
	[FaceCount] [varchar](50) NULL,
	[FvCount] [varchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[DeviceMode] [varchar](50) NULL,
	[FaceFunOn] [varchar](10) NULL,
	[FingerFunOn] [varchar](10) NULL,
	[PhotoFunOn] [varchar](10) NULL,
	[Timezone] [int] NULL,
	[Port] [char](10) NULL,
	[IsMasterDevice] [char](1) NULL,
	[GroupID] [char](3) NULL,
	[IsAuthorized] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMailContent]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMailContent](
	[AppNo] [char](10) NULL,
	[paycode] [varchar](10) NULL,
	[empname] [varchar](100) NULL,
	[FromMail] [varchar](100) NULL,
	[FromText] [varchar](200) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[LeaveCode] [varchar](100) NULL,
	[LvStatus] [char](1) NULL,
	[Remarks] [varchar](200) NULL,
	[LvDuration] [char](1) NULL,
	[IsCalled] [char](1) NULL,
	[code] [varchar](10) NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblmailcount]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblmailcount](
	[Paycode] [char](10) NOT NULL,
	[third] [char](1) NULL,
	[fifth] [char](1) NULL,
	[monthnum] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMailRegister]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMailRegister](
	[PAYCODE] [char](10) NOT NULL,
	[DateOFFICE] [datetime] NOT NULL,
	[Reason] [char](1) NULL,
	[LATEDURATION] [int] NULL,
	[MAILSEND] [char](1) NULL,
	[HODMAILSEND] [char](1) NULL,
	[DAYS] [int] NULL,
	[REPLIED] [char](1) NULL,
	[HODMAILSEND2] [char](1) NULL,
	[DAYS2] [int] NULL,
	[REPLIED2] [char](1) NULL,
	[remarks1] [varchar](50) NULL,
	[Remarks2] [varchar](50) NULL,
	[close] [varchar](1) NULL,
	[close_transaction] [char](1) NULL,
	[Approved_By] [varchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMailSent]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMailSent](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Dt] [datetime] NULL,
	[isCalled] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMailSetup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMailSetup](
	[Server] [varchar](40) NULL,
	[sender] [varchar](100) NULL,
	[type] [varchar](10) NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[pop3] [varchar](20) NULL,
	[Port] [int] NULL,
	[SendTo] [varchar](50) NULL,
	[CC1] [varchar](50) NULL,
	[CC2] [varchar](50) NULL,
	[RunTime] [datetime] NULL,
	[HRMail] [varchar](200) NULL,
	[CCMail] [char](800) NULL,
	[SenderName] [varchar](500) NULL,
	[IsEnable] [char](1) NULL,
	[enablessl] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblMailShoot]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblMailShoot](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[ForDate] [datetime] NULL,
	[Bday] [char](1) NULL,
	[Anniversary] [char](1) NULL,
	[called] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMailTo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMailTo](
	[ToMail] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMapping]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMapping](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[FieldName] [char](100) NULL,
	[ShowAs] [char](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMedical]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMedical](
	[paycode] [char](10) NULL,
	[Mdate] [datetime] NULL,
	[Application_No] [int] NULL,
	[FileName] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMsg]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMsg](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [char](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblNominee]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblNominee](
	[PayCode] [char](10) NULL,
	[CompanyCode] [char](3) NULL,
	[Name] [varchar](30) NULL,
	[DOB] [datetime] NULL,
	[Relation] [varchar](20) NULL,
	[GuardianName] [varchar](30) NULL,
	[Address] [varchar](60) NULL,
	[Share] [int] NULL,
	[IsResidingWith] [char](1) NULL,
	[Alternate_Nominee] [varchar](60) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOutWorkRegister]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOutWorkRegister](
	[PAYCODE] [char](10) NOT NULL,
	[DateOFFICE] [datetime] NOT NULL,
	[IN1] [datetime] NULL,
	[RIN1] [char](3) NULL,
	[OUT1] [datetime] NULL,
	[ROUT1] [char](3) NULL,
	[IN2] [datetime] NULL,
	[RIN2] [char](3) NULL,
	[OUT2] [datetime] NULL,
	[ROUT2] [char](3) NULL,
	[IN3] [datetime] NULL,
	[RIN3] [char](3) NULL,
	[OUT3] [datetime] NULL,
	[ROUT3] [char](3) NULL,
	[IN4] [datetime] NULL,
	[RIN4] [char](3) NULL,
	[OUT4] [datetime] NULL,
	[ROUT4] [char](3) NULL,
	[IN5] [datetime] NULL,
	[RIN5] [char](3) NULL,
	[OUT5] [datetime] NULL,
	[ROUT5] [char](3) NULL,
	[IN6] [datetime] NULL,
	[RIN6] [char](3) NULL,
	[OUT6] [datetime] NULL,
	[ROUT6] [char](3) NULL,
	[IN7] [datetime] NULL,
	[RIN7] [char](3) NULL,
	[OUT7] [datetime] NULL,
	[ROUT7] [char](3) NULL,
	[IN8] [datetime] NULL,
	[RIN8] [char](3) NULL,
	[OUT8] [datetime] NULL,
	[ROUT8] [char](3) NULL,
	[IN9] [datetime] NULL,
	[RIN9] [char](3) NULL,
	[OUT9] [datetime] NULL,
	[ROUT9] [char](3) NULL,
	[IN10] [datetime] NULL,
	[RIN10] [char](3) NULL,
	[OUT10] [datetime] NULL,
	[ROUT10] [char](3) NULL,
	[OUTWORKDURATION] [int] NULL,
	[Reason_OutWork] [char](1) NULL,
	[SSN] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblParallelDB]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblParallelDB](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[IsEnableParallel] [char](1) NULL,
	[EmpCode] [char](1) NULL,
	[BiometricID] [char](1) NULL,
	[PunchTime] [char](1) NULL,
	[InOut] [char](1) NULL,
	[DeviceID] [char](1) NULL,
	[DeviceSerial] [char](1) NULL,
	[UserName] [char](1) NULL,
	[ConnectionMode] [char](1) NULL,
	[Server] [varchar](50) NULL,
	[DBUser] [varchar](50) NULL,
	[DBPassword] [varchar](50) NULL,
	[DBName] [varchar](50) NULL,
	[TableName] [varchar](50) NULL,
 CONSTRAINT [PK_tblParallelDB] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPayout]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPayout](
	[PAYCODE] [char](10) NOT NULL,
	[EMPNAME] [char](25) NULL,
	[PRESENTCARDNO] [char](8) NULL,
	[COMPANYCODE] [char](3) NULL,
	[DEPARTMENTCODE] [char](3) NULL,
	[CAT] [char](3) NULL,
	[PRESENTVALUE] [numeric](6, 2) NULL,
	[ABSENTVALUE] [numeric](6, 2) NULL,
	[HOLIDAY_VALUE] [numeric](6, 2) NULL,
	[WO_VALUE] [numeric](6, 2) NULL,
	[TOTALLOSSHRS] [numeric](6, 2) NULL,
	[LEAVEVALUE] [numeric](6, 2) NULL,
	[HOURSWORKED] [numeric](6, 2) NULL,
	[OTDURATION] [numeric](6, 2) NULL,
	[OSDURATION] [numeric](6, 2) NULL,
	[EARLYDEPARTURE] [numeric](6, 2) NULL,
	[LATEARRIVAL] [numeric](6, 2) NULL,
	[MONTHDAYS] [numeric](3, 0) NULL,
	[PAYABLEDAYS] [numeric](3, 0) NULL,
	[LATEDAYS] [numeric](3, 0) NULL,
	[EARLYDAYS] [numeric](3, 0) NULL,
	[LEAVE1] [numeric](6, 2) NULL,
	[LEAVE2] [numeric](6, 2) NULL,
	[LEAVE3] [numeric](6, 2) NULL,
	[LEAVE4] [numeric](6, 2) NULL,
	[LEAVE5] [numeric](6, 2) NULL,
	[LEAVE6] [numeric](6, 2) NULL,
	[LEAVE7] [numeric](6, 2) NULL,
	[LEAVE8] [numeric](6, 2) NULL,
	[LEAVE9] [numeric](6, 2) NULL,
	[LEAVE10] [numeric](6, 2) NULL,
	[LEAVE11] [numeric](6, 2) NULL,
	[LEAVE12] [numeric](6, 2) NULL,
	[LEAVE13] [numeric](6, 2) NULL,
	[LEAVE14] [numeric](6, 2) NULL,
	[LEAVE15] [numeric](6, 2) NULL,
	[LEAVE16] [numeric](6, 2) NULL,
	[LEAVE17] [numeric](6, 2) NULL,
	[LEAVE18] [numeric](6, 2) NULL,
	[LEAVE19] [numeric](6, 2) NULL,
	[LEAVE20] [numeric](6, 2) NULL,
	[FROMDATE] [datetime] NULL,
	[TODATE] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPiece]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPiece](
	[Pcode] [char](3) NOT NULL,
	[Pname] [char](35) NULL,
	[Prate] [numeric](6, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPieceData]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPieceData](
	[PAYCODE] [char](10) NULL,
	[Pcode] [char](3) NULL,
	[Prate] [numeric](6, 2) NULL,
	[Pno] [numeric](6, 2) NULL,
	[Pamount] [numeric](8, 2) NULL,
	[Entry_Date] [datetime] NULL,
	[MON_YEAR] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPunchData]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPunchData](
	[CardNo] [nvarchar](8) NULL,
	[MacName] [nvarchar](25) NULL,
	[PunchTime] [datetime] NULL,
	[Status] [nvarchar](1) NULL,
	[InOut] [nvarchar](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRawData]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRawData](
	[update_time] [datetime] NOT NULL,
	[device_id] [varchar](24) NULL,
	[user_id] [varchar](24) NULL,
	[verify_mode] [varchar](64) NULL,
	[io_mode] [varchar](32) NULL,
	[io_time] [datetime] NULL,
	[log_image] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReasonMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReasonMaster](
	[ReasonCode] [char](3) NOT NULL,
	[Description] [char](45) NULL,
	[LeaveCode] [char](3) NULL,
	[LeaveValue] [float] NULL,
	[Late] [char](1) NULL,
	[Early] [char](1) NULL,
	[ExcLunchHours] [char](1) NULL,
	[HoursWorked] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReligionMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReligionMaster](
	[ReligionCode] [char](3) NULL,
	[ReligionName] [char](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblsetup]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblsetup](
	[SETUPID] [char](11) NOT NULL,
	[PERMISLATEARR] [int] NULL,
	[PERMISEARLYDEP] [int] NULL,
	[DUPLICATECHECKMIN] [int] NULL,
	[ISOVERSTAY] [char](1) NULL,
	[S_END] [datetime] NULL,
	[S_OUT] [datetime] NULL,
	[ISOTOUTMINUSSHIFTENDTIME] [char](1) NULL,
	[ISOTWRKGHRSMINUSSHIFTHRS] [char](1) NULL,
	[ISOTEARLYCOMEPLUSLATEDEP] [char](1) NULL,
	[ISOTALL] [char](1) NULL,
	[ISOTEARLYCOMING] [char](1) NULL,
	[OTEARLYDUR] [int] NULL,
	[OTLATECOMINGDUR] [int] NULL,
	[OTRESTRICTENDDUR] [int] NULL,
	[OTEARLYDEPARTUREDUR] [int] NULL,
	[DEDUCTWOOT] [int] NULL,
	[DEDUCTHOLIDAYOT] [int] NULL,
	[ISPRESENTONWOPRESENT] [char](1) NULL,
	[ISPRESENTONHLDPRESENT] [char](1) NULL,
	[MAXWRKDURATION] [float] NULL,
	[TIME1] [float] NULL,
	[CHECKLATE] [int] NULL,
	[CHECKEARLY] [int] NULL,
	[TIME] [int] NULL,
	[HALF] [int] NULL,
	[SHORT] [int] NULL,
	[ISAUTOABSENT] [char](1) NULL,
	[AUTOSHIFT_LOW] [int] NULL,
	[AUTOSHIFT_UP] [int] NULL,
	[ISAUTOSHIFT] [char](1) NULL,
	[ISHALFDAY] [char](1) NULL,
	[ISSHORT] [char](1) NULL,
	[TWO] [char](1) NULL,
	[WOINCLUDE] [char](1) NULL,
	[IsOutWork] [char](1) NULL,
	[NightShiftFourPunch] [char](1) NULL,
	[LinesPerPage] [int] NULL,
	[SkipAfterDepartment] [char](1) NULL,
	[meals_rate] [numeric](7, 2) NULL,
	[INOUT] [char](1) NULL,
	[SMART] [char](1) NULL,
	[ISHELP] [char](1) NULL,
	[ATT_ACC] [char](1) NULL,
	[OTROUND] [char](1) NULL,
	[PREWO] [int] NULL,
	[ISAWA] [char](1) NULL,
	[ISPREWO] [char](1) NULL,
	[LeaveFinancialYear] [char](1) NULL,
	[Online] [char](1) NULL,
	[AutoDownload] [char](1) NULL,
	[MIS] [char](1) NULL,
	[OwMinus] [char](1) NULL,
	[LDAP] [char](1) NULL,
	[Morning_Time] [datetime] NULL,
	[Evening_Time] [datetime] NULL,
	[Email_Late] [char](1) NULL,
	[Email_Early] [char](1) NULL,
	[Email_MIS] [char](1) NULL,
	[Email_Abs] [char](1) NULL,
	[StartCoreTime] [datetime] NULL,
	[EndCoreTime] [datetime] NULL,
	[MaxFlexiHrs] [bigint] NULL,
	[MaxFlexiHrsWeek] [bigint] NULL,
	[ValidTimeEarly] [datetime] NULL,
	[ValidTimeDept] [datetime] NULL,
	[SMSPassword] [char](25) NULL,
	[SMSUserID] [char](25) NULL,
	[SMSMessage] [char](255) NULL,
	[FromText] [char](255) NULL,
	[ReminderDays] [bigint] NULL,
	[LateVerification] [char](1) NULL,
	[TimerDur] [varchar](10) NULL,
	[BackDays] [varchar](10) NULL,
	[CompanyCode] [varchar](4) NOT NULL,
	[ISAW] [char](1) NULL,
	[ISWA] [char](1) NULL,
 CONSTRAINT [PK_tblsetup] PRIMARY KEY CLUSTERED 
(
	[SETUPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblShiftMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblShiftMaster](
	[SHIFT] [char](3) NOT NULL,
	[STARTTIME] [datetime] NULL,
	[ENDTIME] [datetime] NULL,
	[LUNCHTIME] [datetime] NULL,
	[LUNCHDURATION] [int] NULL,
	[LUNCHENDTIME] [datetime] NULL,
	[ORDERINF12] [int] NULL,
	[OTSTARTAFTER] [float] NULL,
	[OTDEDUCTHRS] [int] NULL,
	[LUNCHDEDUCTION] [int] NULL,
	[SHIFTPOSITION] [char](7) NULL,
	[SHIFTDURATION] [float] NULL,
	[OTDEDUCTAFTER] [float] NULL,
	[CompanyCode] [varchar](4) NOT NULL,
 CONSTRAINT [PK_tblShiftMaster] PRIMARY KEY CLUSTERED 
(
	[SHIFT] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSMS]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSMS](
	[SMSKey] [varchar](200) NULL,
	[SenderID] [varchar](100) NULL,
	[IsAbsent] [char](1) NULL,
	[AbsentSMSFor] [char](1) NULL,
	[AbsentSMSAfter] [bigint] NULL,
	[AbsentSMSTime] [datetime] NULL,
	[AbsentSMS1] [varchar](80) NULL,
	[AbsentSMS2] [varchar](80) NULL,
	[AbsentName] [char](1) NULL,
	[AbsentDate] [char](1) NULL,
	[IsLate] [char](1) NULL,
	[LateSMS1] [char](80) NULL,
	[LateSMS2] [char](80) NULL,
	[IsIn] [char](1) NULL,
	[InSMS1] [char](80) NULL,
	[InSMS2] [char](80) NULL,
	[IsOut] [char](1) NULL,
	[OutSMS1] [char](80) NULL,
	[OutSMS2] [char](80) NULL,
	[OutSMSAfter] [bigint] NULL,
	[isCalled] [char](1) NULL,
	[dbType] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblTeam]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblTeam](
	[TeamId] [char](3) NOT NULL,
	[DepartmentCode] [char](3) NULL,
	[TeamName] [varchar](30) NULL,
	[TeamLeader] [char](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbltimeregister]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbltimeregister](
	[PAYCODE] [char](10) NOT NULL,
	[DateOFFICE] [datetime] NOT NULL,
	[SHIFTSTARTTIME] [datetime] NULL,
	[SHIFTENDTIME] [datetime] NULL,
	[LUNCHSTARTTIME] [datetime] NULL,
	[LUNCHENDTIME] [datetime] NULL,
	[HOURSWORKED] [int] NULL,
	[EXCLUNCHHOURS] [int] NULL,
	[OTDURATION] [int] NULL,
	[OSDURATION] [int] NULL,
	[OTAMOUNT] [float] NULL,
	[EARLYARRIVAL] [int] NULL,
	[EARLYDEPARTURE] [int] NULL,
	[LATEARRIVAL] [int] NULL,
	[LUNCHEARLYDEPARTURE] [int] NULL,
	[LUNCHLATEARRIVAL] [int] NULL,
	[TOTALLOSSHRS] [int] NULL,
	[STATUS] [char](6) NULL,
	[LEAVETYPE1] [char](1) NULL,
	[LEAVETYPE2] [char](1) NULL,
	[FIRSTHALFLEAVECODE] [char](3) NULL,
	[SECONDHALFLEAVECODE] [char](3) NULL,
	[REASON] [varchar](2000) NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTATTENDED] [char](3) NULL,
	[IN1] [datetime] NULL,
	[IN2] [datetime] NULL,
	[OUT1] [datetime] NULL,
	[OUT2] [datetime] NULL,
	[IN1MANNUAL] [char](1) NULL,
	[IN2MANNUAL] [char](1) NULL,
	[OUT1MANNUAL] [char](1) NULL,
	[OUT2MANNUAL] [char](1) NULL,
	[LEAVEVALUE] [float] NULL,
	[PRESENTVALUE] [float] NULL,
	[ABSENTVALUE] [float] NULL,
	[HOLIDAY_VALUE] [float] NULL,
	[WO_VALUE] [float] NULL,
	[OUTWORKDURATION] [int] NULL,
	[LEAVETYPE] [char](1) NULL,
	[LEAVECODE] [char](3) NULL,
	[LEAVEAMOUNT] [float] NULL,
	[LEAVEAMOUNT1] [float] NULL,
	[LEAVEAMOUNT2] [float] NULL,
	[FLAG] [char](4) NULL,
	[LEAVEAPRDate] [datetime] NULL,
	[VOUCHER_NO] [char](10) NULL,
	[ReasonCode] [char](3) NULL,
	[ApprovedOT] [float] NULL,
	[WBR_Flag] [char](1) NULL,
	[Stage1OTApprovedBy] [char](10) NULL,
	[Stage2OTApprovedBy] [char](10) NULL,
	[Stage1ApprovalDate] [datetime] NULL,
	[Stage2ApprovalDate] [datetime] NULL,
	[AllowOT] [int] NULL,
	[AbsentSMS] [char](1) NULL,
	[LateSMS] [char](1) NULL,
	[InSMS] [char](1) NULL,
	[OutSMS] [char](1) NULL,
	[SSN] [varchar](100) NULL,
	[COFUsed] [char](1) NULL,
 CONSTRAINT [PK_tbltimeregister] PRIMARY KEY CLUSTERED 
(
	[PAYCODE] ASC,
	[DateOFFICE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTimeRegisterd]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTimeRegisterd](
	[PAYCODE] [char](10) NOT NULL,
	[DateOFFICE] [datetime] NOT NULL,
	[SHIFTSTARTTIME] [datetime] NULL,
	[SHIFTENDTIME] [datetime] NULL,
	[LUNCHSTARTTIME] [datetime] NULL,
	[LUNCHENDTIME] [datetime] NULL,
	[HOURSWORKED] [int] NULL,
	[EXCLUNCHHOURS] [int] NULL,
	[OTDURATION] [int] NULL,
	[OSDURATION] [int] NULL,
	[OTAMOUNT] [float] NULL,
	[EARLYARRIVAL] [int] NULL,
	[EARLYDEPARTURE] [int] NULL,
	[LATEARRIVAL] [int] NULL,
	[LUNCHEARLYDEPARTURE] [int] NULL,
	[LUNCHLATEARRIVAL] [int] NULL,
	[TOTALLOSSHRS] [int] NULL,
	[STATUS] [char](6) NULL,
	[LEAVETYPE1] [char](1) NULL,
	[LEAVETYPE2] [char](1) NULL,
	[FIRSTHALFLEAVECODE] [char](3) NULL,
	[SECONDHALFLEAVECODE] [char](3) NULL,
	[REASON] [char](30) NULL,
	[SHIFT] [char](3) NULL,
	[SHIFTATTENDED] [char](3) NULL,
	[IN1] [datetime] NULL,
	[IN2] [datetime] NULL,
	[OUT1] [datetime] NULL,
	[OUT2] [datetime] NULL,
	[IN1MANNUAL] [char](1) NULL,
	[IN2MANNUAL] [char](1) NULL,
	[OUT1MANNUAL] [char](1) NULL,
	[OUT2MANNUAL] [char](1) NULL,
	[LEAVEVALUE] [float] NULL,
	[PRESENTVALUE] [float] NULL,
	[ABSENTVALUE] [float] NULL,
	[HOLIDAY_VALUE] [float] NULL,
	[WO_VALUE] [float] NULL,
	[OUTWORKDURATION] [int] NULL,
	[LEAVETYPE] [char](1) NULL,
	[LEAVECODE] [char](3) NULL,
	[LEAVEAMOUNT] [float] NULL,
	[LEAVEAMOUNT1] [float] NULL,
	[LEAVEAMOUNT2] [float] NULL,
	[FLAG] [char](4) NULL,
	[LEAVEAPRDate] [datetime] NULL,
	[VOUCHER_NO] [char](10) NULL,
	[ReasonCode] [char](3) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLUSER]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLUSER](
	[USER_R] [char](20) NOT NULL,
	[USERDESCRIPRION] [char](50) NULL,
	[PASSWORD] [char](20) NULL,
	[AutoProcess] [char](1) NULL,
	[DataProcess] [char](1) NULL,
	[Main] [char](1) NULL,
	[V_Transaction] [char](1) NULL,
	[Admin] [char](1) NULL,
	[Payroll] [char](1) NULL,
	[Reports] [char](1) NULL,
	[Leave] [char](1) NULL,
	[Company] [char](1) NULL,
	[Department] [char](1) NULL,
	[Section] [char](1) NULL,
	[Grade] [char](1) NULL,
	[Category] [char](1) NULL,
	[Shift] [char](1) NULL,
	[Employee] [char](1) NULL,
	[Visitor] [char](1) NULL,
	[Reason_Card] [char](1) NULL,
	[Manual_Attendance] [char](1) NULL,
	[OstoOt] [char](1) NULL,
	[ShiftChange] [char](1) NULL,
	[HoliDay] [char](1) NULL,
	[LeaveMaster] [char](1) NULL,
	[LeaveApplication] [char](1) NULL,
	[LeaveAccural] [char](1) NULL,
	[LeaveAccuralAuto] [char](1) NULL,
	[TimeOfficeSetup] [char](1) NULL,
	[UserPrevilege] [char](1) NULL,
	[Verification] [char](1) NULL,
	[InstallationSetup] [char](1) NULL,
	[EmployeeSetup] [char](1) NULL,
	[ArearEntry] [char](1) NULL,
	[Advance_Loan] [char](1) NULL,
	[Form16] [char](1) NULL,
	[Form16Return] [char](1) NULL,
	[payrollFormula] [char](1) NULL,
	[PayrollSetup] [char](1) NULL,
	[LoanAdjustment] [char](1) NULL,
	[TimeOfficeReport] [char](1) NULL,
	[VisitorReport] [char](1) NULL,
	[PayrollReport] [char](1) NULL,
	[RegisterCreation] [char](1) NULL,
	[RegisterUpdation] [char](1) NULL,
	[BackDateProcess] [char](1) NULL,
	[ReProcess] [char](1) NULL,
	[OTCAL] [char](1) NULL,
	[auth_comp] [varchar](max) NULL,
	[Auth_dept] [varchar](max) NULL,
	[USERTYPE] [char](1) NULL,
	[paycode] [char](10) NULL,
	[CompAdd] [char](1) NULL,
	[CompModi] [char](1) NULL,
	[CompDel] [char](1) NULL,
	[DeptAdd] [char](1) NULL,
	[DeptModi] [char](1) NULL,
	[DeptDel] [char](1) NULL,
	[CatAdd] [char](1) NULL,
	[CatModi] [char](1) NULL,
	[CatDel] [char](1) NULL,
	[SecAdd] [char](1) NULL,
	[SecModi] [char](1) NULL,
	[SecDel] [char](1) NULL,
	[GrdAdd] [char](1) NULL,
	[GrdModi] [char](1) NULL,
	[GrdDel] [char](1) NULL,
	[SftAdd] [char](1) NULL,
	[SftModi] [char](1) NULL,
	[SftDel] [char](1) NULL,
	[EmpAdd] [char](1) NULL,
	[EmpModi] [char](1) NULL,
	[EmpDel] [char](1) NULL,
	[DataMaintenance] [char](1) NULL,
	[LoginType] [char](1) NULL,
	[IsValid] [char](1) NULL,
	[Approver_Visible] [char](1) NULL,
	[RosterView] [char](1) NULL,
	[trainer] [char](1) NULL,
	[Attrition] [char](1) NULL,
	[OTApproval] [char](1) NULL,
	[IsOA] [char](1) NULL,
	[LeaveApproval] [char](1) NULL,
	[IsAutoLogin] [char](1) NULL,
	[Group_EMP] [char](1) NULL,
	[Area] [char](1) NULL,
	[ReportingPosition] [char](1) NULL,
	[Location] [char](1) NULL,
	[Business_Unit] [char](1) NULL,
	[MonthlyLeaveCredit] [char](1) NULL,
	[position] [char](1) NULL,
	[business] [char](1) NULL,
	[addArea] [char](1) NULL,
	[addPosition] [char](1) NULL,
	[addBusiness] [char](1) NULL,
	[addLocation] [char](1) NULL,
	[ChangeFieldsHead] [char](1) NULL,
	[HeadidUpdation] [char](1) NULL,
	[Controller] [char](1) NULL,
	[CompanyCode] [char](10) NULL,
	[SSN] [char](20) NULL,
	[Active] [char](10) NULL,
	[ManualUpload] [char](1) NULL,
	[Auth_Device_Group] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVehicle]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVehicle](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[MC_NO] [char](2) NULL,
	[IN_OUT] [char](1) NULL,
	[ACCESS_ALLOWED] [char](1) NULL,
	[DOOR_TIME] [char](5) NULL,
	[ERROR_CODE] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVerification]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVerification](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[LYear] [bigint] NOT NULL,
	[LMonth] [bigint] NOT NULL,
	[isDeducted] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVisitor]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVisitor](
	[VisitorName] [char](30) NULL,
	[CardNo] [char](8) NULL,
	[Purpose] [char](30) NULL,
	[Address] [char](100) NULL,
	[PersonToMeet] [char](30) NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[Department] [char](25) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVoucherNo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVoucherNo](
	[VoucherNo] [char](10) NOT NULL,
	[CompanyCode] [varchar](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblyears]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblyears](
	[yr] [bigint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TerminalInfo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TerminalInfo](
	[ServerID] [int] NULL,
	[ServerCode] [nvarchar](255) NULL,
	[RegUsers] [int] NULL,
	[Capacity] [int] NULL,
	[Security_level] [int] NULL,
	[Device_Name] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[Comm] [nvarchar](255) NULL,
	[IP_address] [nvarchar](255) NULL,
	[Port] [nvarchar](255) NULL,
	[Modem_Port] [nvarchar](255) NULL,
	[Modem_Phone] [nvarchar](255) NULL,
	[COM_Port] [nvarchar](255) NULL,
	[TempSize] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeTransfer]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeTransfer](
	[TransferTime] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeZone]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeZone](
	[SNo] [int] NULL,
	[In1] [nvarchar](5) NULL,
	[In2] [nvarchar](5) NULL,
	[In3] [nvarchar](5) NULL,
	[In4] [nvarchar](5) NULL,
	[Out1] [nvarchar](5) NULL,
	[Out2] [nvarchar](5) NULL,
	[Out3] [nvarchar](5) NULL,
	[Out4] [nvarchar](5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeZoneMaster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeZoneMaster](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[TimeZoneID] [int] NULL,
	[TimeZoneValue] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeZoneMasterZK]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeZoneMasterZK](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[TimeZoneID] [int] NULL,
	[TimeZoneValue] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TmpDelCardList]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TmpDelCardList](
	[CardNo1] [nvarchar](10) NULL,
	[CardNo] [nvarchar](10) NULL,
	[Name] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAttendance]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAttendance](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[AttState] [varchar](50) NULL,
	[VerifyMode] [varchar](50) NULL,
	[WorkCode] [varchar](50) NULL,
	[AttDateTime] [datetime] NULL,
	[UpdateedOn] [datetime] NULL,
	[CTemp] [varchar](50) NULL,
	[FTemp] [varchar](50) NULL,
	[MaskStatus] [varchar](50) NULL,
	[IsAbnomal] [varchar](50) NULL,
	[LImage] [varbinary](max) NULL,
	[TImage] [varbinary](max) NULL,
	[io_workcode] [varchar](50) NULL,
	[verify_mode] [varchar](50) NULL,
	[io_mode] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
 ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userdetail]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userdetail](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](250) NULL,
	[UserID] [varchar](50) NULL,
	[Name] [varchar](150) NULL,
	[Pri] [int] NULL,
	[Password] [varchar](50) NULL,
	[Card] [varchar](500) NULL,
	[DeviceGroup] [varchar](50) NULL,
	[TimeZone] [varchar](250) NULL,
	[Verify] [varchar](50) NULL,
	[AccesstimeFrom] [varchar](50) NULL,
	[AccessTimeTo] [varchar](50) NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userface]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userface](
	[RowId] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[FID] [varchar](50) NULL,
	[Size] [varchar](50) NULL,
	[Valid] [varchar](50) NULL,
	[FaceTemplate] [varchar](3000) NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserFinger]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserFinger](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[FID] [int] NULL,
	[Size] [int] NULL,
	[Valid] [varchar](50) NULL,
	[FingerTemplate] [varchar](3000) NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserInfo]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInfo](
	[UserID] [nvarchar](255) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[Designation] [int] NULL,
	[Department] [int] NULL,
	[Verity_Type] [nvarchar](255) NULL,
	[Password] [nvarchar](255) NULL,
	[Pic] [nvarchar](255) NULL,
	[Fingers] [nvarchar](255) NULL,
	[Finger1] [image] NULL,
	[Finger2] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserPicture]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPicture](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[FileName] [varchar](50) NULL,
	[Size] [varchar](50) NULL,
	[Contents] [varchar](max) NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitorRawPunch]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitorRawPunch](
	[CARDNO] [char](10) NOT NULL,
	[OFFICEPUNCH] [datetime] NOT NULL,
	[MC_NO] [char](2) NULL,
	[IN_OUT] [char](1) NULL,
	[ACCESS_ALLOWED] [char](1) NULL,
	[DOOR_TIME] [char](5) NULL,
	[ERROR_CODE] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZKATTLOGStamp]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZKATTLOGStamp](
	[ATTLOGStamp] [varchar](50) NULL,
	[SN] [varchar](50) NULL,
	[OPERLOGStamp] [varchar](50) NULL,
	[USERINFOStamp] [varchar](50) NULL,
	[ATTPHOTOStamp] [varchar](50) NULL,
	[fingertmpStamp] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZKServiceErrorLog]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZKServiceErrorLog](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[ErrorMessage] [varchar](2000) NULL,
	[ErrorSource] [varchar](100) NULL,
	[ErrorTime] [datetime] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[BioDeviceGroup] ([GroupID], [GroupName]) VALUES (N'G01', N'General')
INSERT [dbo].[BioDeviceGroup] ([GroupID], [GroupName]) VALUES (N'G02', N'Test')
INSERT [dbo].[BioDeviceGroup] ([GroupID], [GroupName]) VALUES (N'G03', N'Test1')
INSERT [dbo].[BioDeviceGroupMapping] ([GroupID], [DeviceID]) VALUES (N'g01', N'0000000001')
SET IDENTITY_INSERT [dbo].[ClientDeviceGroup] ON 

INSERT [dbo].[ClientDeviceGroup] ([GroupID], [ClientID], [GroupName]) VALUES (1, 1, N'TW')
SET IDENTITY_INSERT [dbo].[ClientDeviceGroup] OFF
SET IDENTITY_INSERT [dbo].[DeviceCommands] ON 

INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (20334, N'A5KD194460034', N'CHECK', N'A5KD194460034', NULL, 1, 1, CAST(N'2019-12-23T11:09:33.450' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (20335, N'A5KD194460034', N'flaginfo', N'A5KD194460034', NULL, 0, 1, CAST(N'2019-12-23T11:09:33.470' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (30334, N'CDSL183461836', N'CHECK', N'CDSL183461836', NULL, 1, 1, CAST(N'2020-01-11T15:19:43.107' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (30335, N'CDSL183461836', N'flaginfo', N'CDSL183461836', NULL, 1, 1, CAST(N'2020-01-11T15:19:43.120' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (30336, N'A5KD194460034', N'ATTLOG', N'A5KD194460034', NULL, 0, 1, CAST(N'2020-05-16T13:01:42.483' AS DateTime), NULL, CAST(N'2020-05-16T00:00:00.000' AS DateTime), CAST(N'2020-05-16T00:00:00.000' AS DateTime))
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40336, N'BYRQ183760314', N'CHECK', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T18:06:02.783' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40337, N'BYRQ183760314', N'flaginfo', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T18:06:02.800' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40338, N'BYRQ183760314', N'CHECK', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T18:08:16.953' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40339, N'BYRQ183760314', N'flaginfo', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T18:08:16.970' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40342, N'BYRQ183760314', N'CHECK', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T19:41:02.140' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40343, N'BYRQ183760314', N'flaginfo', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T19:41:02.140' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40344, N'A5KD194460034', N'ATTLOG', N'A5KD194460034', NULL, 0, 1, CAST(N'2020-05-29T23:25:15.253' AS DateTime), NULL, CAST(N'2020-05-29T00:00:00.000' AS DateTime), CAST(N'2020-05-29T23:59:59.000' AS DateTime))
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40345, N'BYRQ183760314', N'ATTLOG', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T23:25:15.253' AS DateTime), NULL, CAST(N'2020-05-29T00:00:00.000' AS DateTime), CAST(N'2020-05-29T23:59:59.000' AS DateTime))
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40346, N'CDSL183461836', N'ATTLOG', N'CDSL183461836', NULL, 0, 1, CAST(N'2020-05-29T23:25:15.253' AS DateTime), NULL, CAST(N'2020-05-29T00:00:00.000' AS DateTime), CAST(N'2020-05-29T23:59:59.000' AS DateTime))
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40340, N'BYRQ183760314', N'CHECK', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T18:10:54.433' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[DeviceCommands] ([Dev_Cmd_ID], [SerialNumber], [CommandContent], [TransferToDevice], [UserID], [Executed], [IsOnline], [CreatedOn], [FID], [StartDateATTLOG], [EndDateATTLOG]) VALUES (40341, N'BYRQ183760314', N'flaginfo', N'BYRQ183760314', NULL, 1, 1, CAST(N'2020-05-29T18:10:54.433' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DeviceCommands] OFF
INSERT [dbo].[iDMSSetting] ([NoOfCommandsAtTime], [AutoSync]) VALUES (N'100', N'N')
INSERT [dbo].[InstallSystemInfo] ([MAC], [ComName], [ComAdd], [ComContact], [ComEmail], [UserKey], [License], [InstalledDate], [LastUpdated], [MaxDevice], [MaxUser]) VALUES (N'708BCD5866A8', N'TimeWatch Infocom Pvt. Ltd.', N'Delhi', N'011-41916615', N'ajitesh@timewatchindia.com', N'708BCD5866A8', N'15949-25642-3382', NULL, NULL, N'kIIpkYndvHs=', N'BW3NeDS6oxo=')
INSERT [dbo].[ParallelDB] ([IsParallel], [DBName], [DBType], [AuthMode], [ServerName], [UserName], [Pwd], [TblName], [PreCardNo], [PreIsPrifix], [PreLength], [PreText], [Paycode], [PayIsPrefix], [PayLength], [PayText], [PunchDate], [PunchDateFormat], [PunchTime], [PunchTimeFormat], [PunchDateTime], [PunchDateTimeFormat], [DeviceId], [PunchDirection], [InValue], [OutVaue], [IsManual], [YesValue], [NoValue], [Rfield1], [Rfield1Value], [Rfield2], [Rfield2Value], [Rfield3], [Rfield3Value]) VALUES (N'N', N'x', N'S', N'W', N'x', N'', N'', N'RawData', N'1020', N'N', N'', N'', N'TestPay', N'N', N'', N'', N'Yes', N'yyyy-MM-dd', N'xcv', N'HH:mm', N'DateTime', N'yyyy-MM-dd HH:mm', N'Dev', N'InOut', N'0', N'1', N'Yes', N'Y', N'N', N'Req1', N'Req2', N'Req2', N'Req2V', N'Req3', N'Req3V')
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'210', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'213', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'208', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'211', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'212', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'214', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tbl_fkcmd_trans_cmd_result] ([trans_id], [device_id], [cmd_result]) VALUES (N'209', N'0000000001', 0x810000007B22666163655F636F756E74223A342C226964636172645F636F756E74223A302C226D616E616765725F636F756E74223A302C2270617373776F72645F636F756E74223A302C22746F74616C5F6C6F675F636F756E74223A31322C22746F74616C5F757365725F636F756E74223A342C22757365725F636F756E74223A347D0A00)
INSERT [dbo].[tblbranch] ([BRANCHCODE], [BRANCHNAME], [COMPANYCODE]) VALUES (N'001', N'None                               ', N'HMC       ')
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-01-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-02-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-03-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-04-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-05-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-06-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-07-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-08-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-09-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-10-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-11-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-01-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-02-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-03-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-04-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-05-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-06-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-07-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-08-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-09-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-10-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-11-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2020-12-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2018-12-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-01-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-02-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-03-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-04-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-05-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-06-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-07-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-08-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-09-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-10-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-11-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-11-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-12-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2019-12-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-01-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-02-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-03-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-04-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-05-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-06-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-07-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
GO
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-08-31T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-09-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-01T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-02T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-03T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-04T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-05T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-06T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-07T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-08T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-09T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-10T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-11T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-12T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-13T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-14T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-15T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-16T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-17T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-18T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-19T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-20T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-21T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-22T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-23T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-24T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-25T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-26T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-27T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-28T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-29T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
INSERT [dbo].[tblCalander] ([MDate], [PROCess], [NRTCPROC], [AbsentMail], [AutoEL], [Autoleaveapproval]) VALUES (CAST(N'2016-10-30T00:00:00.000' AS DateTime), NULL, NULL, NULL, N'N', NULL)
SET IDENTITY_INSERT [dbo].[tblCompany] ON 

INSERT [dbo].[tblCompany] ([COMPANYCODE], [COMPANYNAME], [COMPANYADDRESS], [SHORTNAME], [PANNUM], [TANNUMBER], [TDSCIRCLE], [LCNO], [PFNO], [Id], [active], [DCode]) VALUES (N'C001', N'TimeWatch Infocom                                 ', N'Delhi                                                                                                                                                 ', N'TW        ', N'123456                   ', N'ajitesh@timewatchindia.com                        ', NULL, N'1                        ', N'            ', 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblCompany] OFF
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-24T10:21:21.677' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-24T10:23:09.607' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T10:14:53.290' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T10:15:25.160' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:23:06.500' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:19.327' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:22.450' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:32.303' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:46.610' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:48.273' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:49.170' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:53.167' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:54.480' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T17:26:55.460' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2020-01-07T11:17:09.463' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2020-02-06T17:48:03.953' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'CDSL183461836', CAST(N'2020-02-06T17:48:03.957' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2020-04-15T16:02:53.343' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'CDSL183461836', CAST(N'2020-04-15T16:02:53.353' AS DateTime), N'OffLine             ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2020-04-15T16:12:49.720' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'CDSL183461836', CAST(N'2020-04-15T16:12:49.723' AS DateTime), N'OffLine             ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2020-04-15T16:13:10.970' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'CDSL183461836', CAST(N'2020-04-15T16:13:10.973' AS DateTime), N'OffLine             ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-24T10:56:29.947' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-24T10:56:38.737' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:39.960' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:40.390' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:42.353' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:44.653' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:45.080' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:46.060' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:13:46.510' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblDeviceStatus] ([SerialNumber], [ConnectionTime], [Status]) VALUES (N'A5KD194460034', CAST(N'2019-12-27T18:14:16.857' AS DateTime), N'Communicating       ')
INSERT [dbo].[tblFTPSetup] ([EnableFTP], [FTPServer], [FTPUser], [FTPPassword]) VALUES (N'N', N'ftpserver', N'ftpuser', N'ftpassword')
SET IDENTITY_INSERT [dbo].[tblLog_ManualPunch] ON 

INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (20010, N'1001      ', CAST(N'2020-04-15T15:00:00.000' AS DateTime), N'ADMIN', CAST(N'2020-04-15T16:37:08.363' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30010, N'D045652   ', CAST(N'2020-05-30T08:00:00.000' AS DateTime), N'HMC', CAST(N'2020-05-30T15:10:44.690' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30011, N'D045649   ', CAST(N'2020-05-29T18:00:00.000' AS DateTime), N'HMC', CAST(N'2020-05-30T15:12:32.860' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30012, N'D045649   ', CAST(N'2020-05-30T08:00:00.000' AS DateTime), N'HMC', CAST(N'2020-05-30T15:26:00.977' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30013, N'D045649   ', CAST(N'2020-05-30T21:00:00.000' AS DateTime), N'HMC', CAST(N'2020-05-31T00:21:40.553' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30014, N'IS258     ', CAST(N'2020-05-29T08:00:00.000' AS DateTime), N'HMC', CAST(N'2020-05-31T18:40:47.643' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30015, N'IS258     ', CAST(N'2020-05-30T08:00:00.000' AS DateTime), N'HMC', CAST(N'2020-05-31T18:41:01.150' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30037, N'D045651   ', CAST(N'2020-06-03T04:45:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:10:40.340' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30038, N'D045650   ', CAST(N'2020-06-02T21:22:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:12:36.260' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30039, N'D045650   ', CAST(N'2020-06-03T05:20:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:12:47.377' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30040, N'D045653   ', CAST(N'2020-06-02T21:45:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:13:17.510' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30041, N'D045653   ', CAST(N'2020-06-03T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:13:31.647' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30042, N'IS258     ', CAST(N'2020-06-02T21:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:14:03.863' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30043, N'IS258     ', CAST(N'2020-06-03T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:14:18.153' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30044, N'D045654   ', CAST(N'2020-06-02T09:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:14:56.873' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30045, N'D045654   ', CAST(N'2020-06-02T18:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:15:03.897' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30046, N'D045654   ', CAST(N'2020-06-02T00:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:15:45.710' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30047, N'D045654   ', CAST(N'2020-06-02T09:20:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:16:25.483' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30048, N'D045654   ', CAST(N'2020-06-02T17:20:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:16:48.213' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30049, N'D045655   ', CAST(N'2020-06-02T09:46:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:17:14.553' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30050, N'D045656   ', CAST(N'2020-06-02T21:21:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:20:32.620' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30051, N'D045656   ', CAST(N'2020-06-03T05:20:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:21:06.683' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30052, N'D045657   ', CAST(N'2020-06-02T09:55:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:21:49.293' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30053, N'D045646   ', CAST(N'2020-06-03T09:50:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T12:06:40.977' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30054, N'D045646   ', CAST(N'2020-06-03T05:45:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T12:09:05.923' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30055, N'D045646   ', CAST(N'2020-06-03T17:45:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T12:09:20.280' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30056, N'D045649   ', CAST(N'2020-06-03T08:45:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T12:12:32.627' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30057, N'D045649   ', CAST(N'2020-06-03T18:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T12:15:10.423' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30058, N'D045655   ', CAST(N'2020-06-03T09:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T16:30:41.380' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30059, N'D045655   ', CAST(N'2020-06-03T10:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T16:30:58.787' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30060, N'D045655   ', CAST(N'2020-06-03T19:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T16:34:12.920' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30061, N'D045652   ', CAST(N'2020-06-03T09:55:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T17:21:20.720' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30062, N'D045652   ', CAST(N'2020-06-03T19:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T17:21:45.643' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30063, N'IS258     ', CAST(N'2020-06-03T00:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T18:11:40.280' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30064, N'IS258     ', CAST(N'2020-06-02T21:50:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T18:12:47.227' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30067, N'IS258     ', CAST(N'2020-06-03T06:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T20:20:05.543' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30068, N'D045646   ', CAST(N'2020-06-04T09:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:28:28.567' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30069, N'D045646   ', CAST(N'2020-06-04T10:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:28:43.437' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30070, N'D045646   ', CAST(N'2020-06-04T18:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:28:53.290' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30071, N'D045652   ', CAST(N'2020-06-04T09:25:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:30:16.870' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30072, N'D045652   ', CAST(N'2020-06-04T18:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:30:29.973' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30073, N'D045654   ', CAST(N'2020-06-04T10:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:31:48.567' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30074, N'D045654   ', CAST(N'2020-06-04T18:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:31:54.853' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30075, N'D045655   ', CAST(N'2020-06-04T10:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:32:11.357' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30076, N'D045655   ', CAST(N'2020-06-04T18:25:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:32:24.730' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30077, N'D045657   ', CAST(N'2020-06-04T10:20:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:32:52.240' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30078, N'D045657   ', CAST(N'2020-06-04T18:01:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:32:58.567' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30079, N'D045648   ', CAST(N'2020-06-03T22:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:53:42.303' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30080, N'D045648   ', CAST(N'2020-06-04T04:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:54:12.137' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30081, N'D045650   ', CAST(N'2020-06-03T21:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:54:37.453' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30082, N'D045650   ', CAST(N'2020-06-04T05:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:55:02.293' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30083, N'D045651   ', CAST(N'2020-06-03T21:55:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:55:34.610' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30084, N'D045651   ', CAST(N'2020-06-04T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:55:47.370' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30085, N'D045653   ', CAST(N'2020-06-03T21:05:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:56:03.047' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30086, N'D045653   ', CAST(N'2020-06-04T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:56:15.243' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30087, N'D045656   ', CAST(N'2020-06-03T22:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:56:32.187' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30088, N'D045656   ', CAST(N'2020-06-04T04:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:56:46.803' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30089, N'IS258     ', CAST(N'2020-06-03T22:05:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:57:30.883' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30090, N'IS258     ', CAST(N'2020-06-04T05:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-04T09:57:41.790' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30091, N'D045651   ', CAST(N'2020-06-07T22:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T08:26:50.527' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30092, N'D045651   ', CAST(N'2020-06-08T05:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T08:27:12.040' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30093, N'D045650   ', CAST(N'2020-06-08T22:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T08:31:39.990' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30094, N'D045650   ', CAST(N'2020-06-09T05:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T08:31:55.723' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30095, N'D045650   ', CAST(N'2020-06-07T22:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T08:34:10.993' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30096, N'D045650   ', CAST(N'2020-06-08T05:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T08:34:26.467' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30097, N'D045648   ', CAST(N'2020-06-07T22:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:24:50.223' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30098, N'D045648   ', CAST(N'2020-06-08T05:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:25:02.663' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30102, N'D045651   ', CAST(N'2020-06-06T21:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:50:25.100' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30103, N'D045651   ', CAST(N'2020-06-07T05:33:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:50:36.693' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30104, N'D045651   ', CAST(N'2020-06-04T10:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:53:39.953' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30105, N'D045651   ', CAST(N'2020-06-05T05:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:53:51.877' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30106, N'D045651   ', CAST(N'2020-06-04T22:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:55:22.810' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30016, N'IS258     ', CAST(N'2020-05-31T19:55:00.000' AS DateTime), N'HMC', CAST(N'2020-05-31T19:39:29.700' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30099, N'D045648   ', CAST(N'2020-06-04T21:50:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:35:41.003' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30017, N'IS258     ', CAST(N'2020-05-31T09:10:00.000' AS DateTime), N'HMC', CAST(N'2020-05-31T19:46:42.697' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30018, N'IS258     ', CAST(N'2020-05-31T08:25:00.000' AS DateTime), N'HMC', CAST(N'2020-05-31T19:52:00.090' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30019, N'IS258     ', CAST(N'2020-06-01T21:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T11:58:32.777' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30020, N'D045649   ', CAST(N'2020-06-01T09:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T12:10:50.663' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30021, N'D045649   ', CAST(N'2020-06-01T18:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T12:11:04.407' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30022, N'D045649   ', CAST(N'2020-06-01T18:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T13:48:50.690' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30023, N'D045649   ', CAST(N'2020-05-31T21:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T13:53:58.850' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30024, N'D045649   ', CAST(N'2020-06-01T05:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T13:54:28.467' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30025, N'D045651   ', CAST(N'2020-05-31T21:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T13:56:28.220' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30026, N'D045651   ', CAST(N'2020-06-01T05:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-01T13:56:52.640' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30027, N'D045654   ', CAST(N'2020-06-02T09:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T08:40:46.603' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30028, N'D045654   ', CAST(N'2020-06-01T08:45:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T08:41:19.497' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30029, N'D045654   ', CAST(N'2020-06-01T13:35:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T08:41:35.440' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30030, N'D045652   ', CAST(N'2020-06-02T09:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:03:42.413' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30031, N'D045652   ', CAST(N'2020-06-02T17:55:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:04:00.717' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30032, N'D045646   ', CAST(N'2020-06-02T09:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:04:49.220' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30033, N'D045646   ', CAST(N'2020-06-02T18:05:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:05:06.857' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30034, N'D045648   ', CAST(N'2020-06-02T21:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:05:44.023' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30035, N'D045648   ', CAST(N'2020-06-03T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:06:07.730' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30036, N'D045651   ', CAST(N'2020-06-02T21:50:00.000' AS DateTime), N'HMC', CAST(N'2020-06-02T09:08:36.520' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30115, N'IS258     ', CAST(N'2020-06-08T05:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T12:06:27.693' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30116, N'D045654   ', CAST(N'2020-06-08T10:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T12:22:39.317' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30117, N'D045654   ', CAST(N'2020-06-08T18:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T12:22:58.560' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30065, N'IS258     ', CAST(N'2020-06-03T06:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T18:13:14.593' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30066, N'IS258     ', CAST(N'2020-06-02T21:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-03T19:46:25.367' AS DateTime))
GO
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30100, N'D045648   ', CAST(N'2020-06-05T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:35:51.340' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30101, N'D045648   ', CAST(N'2020-06-04T22:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T09:36:32.227' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30107, N'D045652   ', CAST(N'2020-06-08T09:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:51:10.480' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30108, N'D045652   ', CAST(N'2020-06-08T18:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:51:21.610' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30109, N'D045646   ', CAST(N'2020-06-08T09:15:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:55:39.307' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30110, N'D045646   ', CAST(N'2020-06-08T18:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:55:55.363' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30111, N'D045651   ', CAST(N'2020-06-05T21:00:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:57:16.093' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30112, N'D045651   ', CAST(N'2020-06-06T05:10:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:58:24.587' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30113, N'IS258     ', CAST(N'2020-06-07T22:01:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:59:27.170' AS DateTime))
INSERT [dbo].[tblLog_ManualPunch] ([LogId], [Paycode], [PunchDate], [LoggedUser], [LoggedDate]) VALUES (30114, N'IS258     ', CAST(N'2020-06-08T05:30:00.000' AS DateTime), N'HMC', CAST(N'2020-06-08T11:59:45.630' AS DateTime))
SET IDENTITY_INSERT [dbo].[tblLog_ManualPunch] OFF
INSERT [dbo].[tblMailSetup] ([Server], [sender], [type], [username], [password], [pop3], [Port], [SendTo], [CC1], [CC2], [RunTime], [HRMail], [CCMail], [SenderName], [IsEnable], [enablessl]) VALUES (N'smtp.gmail.com', N'ninu.andrews@qa.g4s.com', NULL, N'ninu.andrews@qa.g4s.com', N'03091988vavS@', NULL, 587, NULL, NULL, NULL, NULL, NULL, NULL, N'Ninu', NULL, N'N')
SET IDENTITY_INSERT [dbo].[tblMapping] ON 

INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (1, N'CardNo                                                                                              ', N'CardNo              ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (2, N'EmpCode                                                                                             ', N'Payroll Code        ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (3, N'Name                                                                                                ', N'Name                ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (4, N'FName                                                                                               ', N'Guardian Name       ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (5, N'Company/School/University Name                                                                      ', N'Company             ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (6, N'Department                                                                                          ', N'Department          ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (7, N'Catagory                                                                                            ', N'Catagory            ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (8, N'Section                                                                                             ', N'Section             ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (9, N'Grade                                                                                               ', N'Grade               ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (10, N'Designation                                                                                         ', N'Designation         ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (11, N'Branch                                                                                              ', N'Branch              ')
INSERT [dbo].[tblMapping] ([id], [FieldName], [ShowAs]) VALUES (12, N'Employee                                                                                            ', N'Employee            ')
SET IDENTITY_INSERT [dbo].[tblMapping] OFF
SET IDENTITY_INSERT [dbo].[tblParallelDB] ON 

INSERT [dbo].[tblParallelDB] ([RowID], [IsEnableParallel], [EmpCode], [BiometricID], [PunchTime], [InOut], [DeviceID], [DeviceSerial], [UserName], [ConnectionMode], [Server], [DBUser], [DBPassword], [DBName], [TableName]) VALUES (1, N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'S', N'sa', N'sa', N'sss', N'SSSDB_TIPL', N'RawData')
SET IDENTITY_INSERT [dbo].[tblParallelDB] OFF
INSERT [dbo].[tblsetup] ([SETUPID], [PERMISLATEARR], [PERMISEARLYDEP], [DUPLICATECHECKMIN], [ISOVERSTAY], [S_END], [S_OUT], [ISOTOUTMINUSSHIFTENDTIME], [ISOTWRKGHRSMINUSSHIFTHRS], [ISOTEARLYCOMEPLUSLATEDEP], [ISOTALL], [ISOTEARLYCOMING], [OTEARLYDUR], [OTLATECOMINGDUR], [OTRESTRICTENDDUR], [OTEARLYDEPARTUREDUR], [DEDUCTWOOT], [DEDUCTHOLIDAYOT], [ISPRESENTONWOPRESENT], [ISPRESENTONHLDPRESENT], [MAXWRKDURATION], [TIME1], [CHECKLATE], [CHECKEARLY], [TIME], [HALF], [SHORT], [ISAUTOABSENT], [AUTOSHIFT_LOW], [AUTOSHIFT_UP], [ISAUTOSHIFT], [ISHALFDAY], [ISSHORT], [TWO], [WOINCLUDE], [IsOutWork], [NightShiftFourPunch], [LinesPerPage], [SkipAfterDepartment], [meals_rate], [INOUT], [SMART], [ISHELP], [ATT_ACC], [OTROUND], [PREWO], [ISAWA], [ISPREWO], [LeaveFinancialYear], [Online], [AutoDownload], [MIS], [OwMinus], [LDAP], [Morning_Time], [Evening_Time], [Email_Late], [Email_Early], [Email_MIS], [Email_Abs], [StartCoreTime], [EndCoreTime], [MaxFlexiHrs], [MaxFlexiHrsWeek], [ValidTimeEarly], [ValidTimeDept], [SMSPassword], [SMSUserID], [SMSMessage], [FromText], [ReminderDays], [LateVerification], [TimerDur], [BackDays], [CompanyCode], [ISAW], [ISWA]) VALUES (N'2          ', 10, 10, 5, N'N', CAST(N'2019-06-25T05:00:00.000' AS DateTime), CAST(N'2019-06-25T08:00:00.000' AS DateTime), N'N', N'Y', N'N', N'N', N'N', 0, 0, 0, NULL, 0, 0, N'N', N'N', 1440, 1020, 240, 240, 240, 300, 120, N'Y', 240, 240, N'Y', N'N', N'N', N'N', N'N', N'N', N'N', 58, N'Y', CAST(1.00 AS Numeric(7, 2)), N'N', N'N', N'N', N'N', N'N', 3, N'Y', N'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'C001', N'Y', N'Y')
INSERT [dbo].[tblShiftMaster] ([SHIFT], [STARTTIME], [ENDTIME], [LUNCHTIME], [LUNCHDURATION], [LUNCHENDTIME], [ORDERINF12], [OTSTARTAFTER], [OTDEDUCTHRS], [LUNCHDEDUCTION], [SHIFTPOSITION], [SHIFTDURATION], [OTDEDUCTAFTER], [CompanyCode]) VALUES (N'GEN', CAST(N'1900-01-01T05:00:00.000' AS DateTime), CAST(N'1900-01-01T13:00:00.000' AS DateTime), CAST(N'1900-01-01T10:00:00.000' AS DateTime), 30, CAST(N'1900-01-01T10:30:00.000' AS DateTime), NULL, 0, 0, 30, N'Day    ', 450, 0, N'C001')
INSERT [dbo].[TBLUSER] ([USER_R], [USERDESCRIPRION], [PASSWORD], [AutoProcess], [DataProcess], [Main], [V_Transaction], [Admin], [Payroll], [Reports], [Leave], [Company], [Department], [Section], [Grade], [Category], [Shift], [Employee], [Visitor], [Reason_Card], [Manual_Attendance], [OstoOt], [ShiftChange], [HoliDay], [LeaveMaster], [LeaveApplication], [LeaveAccural], [LeaveAccuralAuto], [TimeOfficeSetup], [UserPrevilege], [Verification], [InstallationSetup], [EmployeeSetup], [ArearEntry], [Advance_Loan], [Form16], [Form16Return], [payrollFormula], [PayrollSetup], [LoanAdjustment], [TimeOfficeReport], [VisitorReport], [PayrollReport], [RegisterCreation], [RegisterUpdation], [BackDateProcess], [ReProcess], [OTCAL], [auth_comp], [Auth_dept], [USERTYPE], [paycode], [CompAdd], [CompModi], [CompDel], [DeptAdd], [DeptModi], [DeptDel], [CatAdd], [CatModi], [CatDel], [SecAdd], [SecModi], [SecDel], [GrdAdd], [GrdModi], [GrdDel], [SftAdd], [SftModi], [SftDel], [EmpAdd], [EmpModi], [EmpDel], [DataMaintenance], [LoginType], [IsValid], [Approver_Visible], [RosterView], [trainer], [Attrition], [OTApproval], [IsOA], [LeaveApproval], [IsAutoLogin], [Group_EMP], [Area], [ReportingPosition], [Location], [Business_Unit], [MonthlyLeaveCredit], [position], [business], [addArea], [addPosition], [addBusiness], [addLocation], [ChangeFieldsHead], [HeadidUpdation], [Controller], [CompanyCode], [SSN], [Active], [ManualUpload], [Auth_Device_Group]) VALUES (N'ADMIN               ', N'ADMIN                                             ', N'ADMIN               ', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', NULL, N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'N', N'N', N'N', N'N', N'N', N'N', N'Y', NULL, N'N', N'Y', N'Y', N'Y', N'N', N'N', N'C001      ', N'001', N'A', N'C001      ', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'N', N'L', N'Y', N'N', N'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'C001      ', N'C001_C001           ', NULL, N'Y', NULL)
INSERT [dbo].[tblyears] ([yr]) VALUES (2019)
INSERT [dbo].[tblyears] ([yr]) VALUES (2020)
INSERT [dbo].[tblyears] ([yr]) VALUES (2021)
SET IDENTITY_INSERT [dbo].[TimeZoneMaster] ON 

INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (35, -12, N'Etc/GMT-12')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (36, -11, N'Etc/GMT-11')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (37, -10, N'Etc/GMT-10')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (38, -9, N'Etc/GMT-9')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (39, -8, N'Etc/GMT-8')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (40, -7, N'Etc/GMT-7')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (41, -6, N'Etc/GMT-6')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (42, -5, N'Etc/GMT-5')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (43, -270, N'Etc/GMT-4:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (44, -4, N'Etc/GMT-4')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (45, -210, N'Etc/GMT-3:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (46, -3, N'Etc/GMT-3')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (47, -2, N'Etc/GMT-2')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (48, -1, N'Etc/GMT-1')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (49, 14, N'Etc/GMT')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (50, 1, N'Etc/GMT+1')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (51, 2, N'Etc/GMT+2')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (52, 3, N'Etc/GMT+3')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (53, 210, N'Etc/GMT+3:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (54, 4, N'Etc/GMT+4')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (55, 270, N'Etc/GMT+4:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (56, 5, N'Etc/GMT+5')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (57, 330, N'Etc/GMT+5:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (58, 345, N'Etc/GMT+5:45')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (59, 6, N'Etc/GMT+6')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (60, 390, N'Etc/GMT+6:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (61, 7, N'Etc/GMT+7')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (62, 8, N'Etc/GMT+8')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (63, 9, N'Etc/GMT+9')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (64, 570, N'Etc/GMT+9:30')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (65, 10, N'Etc/GMT+10')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (66, 11, N'Etc/GMT+11')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (67, 12, N'Etc/GMT+12')
INSERT [dbo].[TimeZoneMaster] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (68, 13, N'Etc/GMT+13')
SET IDENTITY_INSERT [dbo].[TimeZoneMaster] OFF
SET IDENTITY_INSERT [dbo].[TimeZoneMasterZK] ON 

INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (35, -12, N'Etc/GMT-12')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (36, -11, N'Etc/GMT-11')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (37, -10, N'Etc/GMT-10')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (38, -9, N'Etc/GMT-9')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (39, -8, N'Etc/GMT-8')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (40, -7, N'Etc/GMT-7')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (41, -6, N'Etc/GMT-6')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (42, -5, N'Etc/GMT-5')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (43, -270, N'Etc/GMT-4:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (44, -4, N'Etc/GMT-4')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (45, -210, N'Etc/GMT-3:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (46, -3, N'Etc/GMT-3')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (47, -2, N'Etc/GMT-2')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (48, -1, N'Etc/GMT-1')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (49, 14, N'Etc/GMT')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (50, 1, N'Etc/GMT+1')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (51, 2, N'Etc/GMT+2')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (52, 3, N'Etc/GMT+3')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (53, 210, N'Etc/GMT+3:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (54, 4, N'Etc/GMT+4')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (55, 270, N'Etc/GMT+4:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (56, 5, N'Etc/GMT+5')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (57, 330, N'Etc/GMT+5:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (58, 345, N'Etc/GMT+5:45')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (59, 6, N'Etc/GMT+6')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (60, 390, N'Etc/GMT+6:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (61, 7, N'Etc/GMT+7')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (62, 8, N'Etc/GMT+8')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (63, 9, N'Etc/GMT+9')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (64, 570, N'Etc/GMT+9:30')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (65, 10, N'Etc/GMT+10')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (66, 11, N'Etc/GMT+11')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (67, 12, N'Etc/GMT+12')
INSERT [dbo].[TimeZoneMasterZK] ([RowID], [TimeZoneID], [TimeZoneValue]) VALUES (68, 13, N'Etc/GMT+13')
SET IDENTITY_INSERT [dbo].[TimeZoneMasterZK] OFF
SET IDENTITY_INSERT [dbo].[Userdetail] ON 

INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (11767, N'A5KD194460034', N'9999', N'', 0, N'', N'[0000000000]', N'1', N'0000000000000000', N'0', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21767, N'CDSL183461836', N'1', N'1', 0, N'', N'', N'1', N'0000000000000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21768, N'CDSL183461836', N'2', N'2', 0, N'', N'', N'1', N'0000000000000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21769, N'CDSL183461836', N'9004', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21770, N'CDSL183461836', N'1414', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21771, N'CDSL183461836', N'3', N'NEHA', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21772, N'CDSL183461836', N'9', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21773, N'CDSL183461836', N'1010', N'Ajitesh', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21774, N'CDSL183461836', N'4', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21775, N'CDSL183461836', N'5', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21776, N'CDSL183461836', N'6', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
INSERT [dbo].[Userdetail] ([RowID], [DeviceID], [UserID], [Name], [Pri], [Password], [Card], [DeviceGroup], [TimeZone], [Verify], [AccesstimeFrom], [AccessTimeTo], [UpdatedOn]) VALUES (21777, N'CDSL183461836', N'5858', N'', 0, N'', N'', N'1', N'0000000100000000', N'-1', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Userdetail] OFF
INSERT [dbo].[ZKATTLOGStamp] ([ATTLOGStamp], [SN], [OPERLOGStamp], [USERINFOStamp], [ATTPHOTOStamp], [fingertmpStamp]) VALUES (NULL, N'A5KD194460034', NULL, N'9999', NULL, NULL)
INSERT [dbo].[ZKATTLOGStamp] ([ATTLOGStamp], [SN], [OPERLOGStamp], [USERINFOStamp], [ATTPHOTOStamp], [fingertmpStamp]) VALUES (N'9999', N'CDSL183461836', NULL, N'9999', NULL, NULL)
INSERT [dbo].[ZKATTLOGStamp] ([ATTLOGStamp], [SN], [OPERLOGStamp], [USERINFOStamp], [ATTPHOTOStamp], [fingertmpStamp]) VALUES (N'9999', N'BYRQ183760314', NULL, N'9999', NULL, NULL)
SET ANSI_PADDING ON
GO
/****** Object:  Index [NonClusteredIndex-20181114-113648]    Script Date: 02/07/2020 11:09:44 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20181114-113648] ON [dbo].[DeviceCommands]
(
	[CommandContent] ASC,
	[TransferToDevice] ASC,
	[UserID] ASC,
	[Executed] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DeviceCommands] ADD  CONSTRAINT [DF_DeviceCommands_IsTransfer]  DEFAULT ((0)) FOR [TransferToDevice]
GO
ALTER TABLE [dbo].[DeviceCommands] ADD  CONSTRAINT [DF_DeviceCommands_Executed]  DEFAULT ((0)) FOR [Executed]
GO
ALTER TABLE [dbo].[DeviceCommands] ADD  CONSTRAINT [DF_DeviceCommands_IsOnline]  DEFAULT ((1)) FOR [IsOnline]
GO
ALTER TABLE [dbo].[DeviceCommands] ADD  CONSTRAINT [DF_DeviceCommands_UpdatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DeviceMaster] ADD  CONSTRAINT [DF_DeviceMaster_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Holiday] ADD  CONSTRAINT [DF__Holiday__BranchC__200DB40D]  DEFAULT ('B1') FOR [BranchCode]
GO
ALTER TABLE [dbo].[Holiday] ADD  CONSTRAINT [DF__Holiday__Divisio__5D3D1038]  DEFAULT ('B1') FOR [Divisioncode]
GO
ALTER TABLE [dbo].[OperationLog] ADD  CONSTRAINT [DF_OperationLog_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VBASI__66603565]  DEFAULT ((0)) FOR [VBASIC]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VBank__6754599E]  DEFAULT ('') FOR [VBankAcc]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VDA_R__68487DD7]  DEFAULT ((0)) FOR [VDA_RATE]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VHRA___693CA210]  DEFAULT ((0)) FOR [VHRA_RATE]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VCONV__6A30C649]  DEFAULT ((0)) FOR [VCONV_RATE]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VMED___6B24EA82]  DEFAULT ((0)) FOR [VMED_RATE]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VOT_R__6C190EBB]  DEFAULT ((0)) FOR [VOT_RATE]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_1__6D0D32F4]  DEFAULT ((0)) FOR [VI_1]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_2__6E01572D]  DEFAULT ((0)) FOR [VI_2]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_3__6EF57B66]  DEFAULT ((0)) FOR [VI_3]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_4__6FE99F9F]  DEFAULT ((0)) FOR [VI_4]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_5__70DDC3D8]  DEFAULT ((0)) FOR [VI_5]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_6__71D1E811]  DEFAULT ((0)) FOR [VI_6]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_7__72C60C4A]  DEFAULT ((0)) FOR [VI_7]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_8__73BA3083]  DEFAULT ((0)) FOR [VI_8]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VI_9__74AE54BC]  DEFAULT ((0)) FOR [VI_9]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VI_10__75A278F5]  DEFAULT ((0)) FOR [VI_10]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__PF_AL__76969D2E]  DEFAULT ('N') FOR [PF_ALLOWED]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__ESI_A__778AC167]  DEFAULT ('N') FOR [ESI_ALLOWED]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VPF_A__787EE5A0]  DEFAULT ('N') FOR [VPF_ALLOWED]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__BONUS__797309D9]  DEFAULT ('N') FOR [BONUS_ALLOWED]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__GRADU__7A672E12]  DEFAULT ('N') FOR [GRADUTY_ALLOWED]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__PROF___7B5B524B]  DEFAULT ('N') FOR [PROF_TAX_ALLOWED]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_1__7C4F7684]  DEFAULT ((0)) FOR [VD_1]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_2__7D439ABD]  DEFAULT ((0)) FOR [VD_2]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_3__7E37BEF6]  DEFAULT ((0)) FOR [VD_3]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_4__7F2BE32F]  DEFAULT ((0)) FOR [VD_4]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_5__00200768]  DEFAULT ((0)) FOR [VD_5]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_6__01142BA1]  DEFAULT ((0)) FOR [VD_6]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_7__02084FDA]  DEFAULT ((0)) FOR [VD_7]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_8__02FC7413]  DEFAULT ((0)) FOR [VD_8]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Master__VD_9__03F0984C]  DEFAULT ((0)) FOR [VD_9]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VD_10__04E4BC85]  DEFAULT ((0)) FOR [VD_10]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__Pay_Maste__VTDS___05D8E0BE]  DEFAULT ((0)) FOR [VTDS_RATE]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__pay_maste__VGros__5026DB83]  DEFAULT ((0)) FOR [VGross]
GO
ALTER TABLE [dbo].[Pay_Master] ADD  CONSTRAINT [DF__pay_maste__VIncr__520F23F5]  DEFAULT ((0)) FOR [VIncrement]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VMED_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VCONV_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_1_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_2_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_3_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_4_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_5_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_6_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_7_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_8_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_9_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ((0)) FOR [VI_10_AMT]
GO
ALTER TABLE [dbo].[PAY_REIMURSH] ADD  DEFAULT ('N') FOR [paid]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VBASI__1EA48E88]  DEFAULT ((0)) FOR [VBASIC]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VSALA__1F98B2C1]  DEFAULT ((0)) FOR [VSALARY]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VDA_R__208CD6FA]  DEFAULT ((0)) FOR [VDA_RATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VDA_A__2180FB33]  DEFAULT ((0)) FOR [VDA_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VMED___22751F6C]  DEFAULT ((0)) FOR [VMED_RATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VMED___236943A5]  DEFAULT ((0)) FOR [VMED_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VCONV__245D67DE]  DEFAULT ((0)) FOR [VCONV_RATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VCONV__25518C17]  DEFAULT ((0)) FOR [VCONV_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VHRA___2645B050]  DEFAULT ((0)) FOR [VHRA_RATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VHRA___2739D489]  DEFAULT ((0)) FOR [VHRA_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VOT__282DF8C2]  DEFAULT ((0)) FOR [VOT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VOT_R__29221CFB]  DEFAULT ((0)) FOR [VOT_RATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VOT_A__2A164134]  DEFAULT ((0)) FOR [VOT_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_1__2B0A656D]  DEFAULT ((0)) FOR [VI_1]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_1___2BFE89A6]  DEFAULT ((0)) FOR [VI_1_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_2__2CF2ADDF]  DEFAULT ((0)) FOR [VI_2]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_2___2DE6D218]  DEFAULT ((0)) FOR [VI_2_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_3__2EDAF651]  DEFAULT ((0)) FOR [VI_3]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_3___2FCF1A8A]  DEFAULT ((0)) FOR [VI_3_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_4__30C33EC3]  DEFAULT ((0)) FOR [VI_4]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_4___31B762FC]  DEFAULT ((0)) FOR [VI_4_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_5__32AB8735]  DEFAULT ((0)) FOR [VI_5]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_5___339FAB6E]  DEFAULT ((0)) FOR [VI_5_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_6__3493CFA7]  DEFAULT ((0)) FOR [VI_6]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_6___3587F3E0]  DEFAULT ((0)) FOR [VI_6_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_7__367C1819]  DEFAULT ((0)) FOR [VI_7]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_7___37703C52]  DEFAULT ((0)) FOR [VI_7_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_8__3864608B]  DEFAULT ((0)) FOR [VI_8]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_8___395884C4]  DEFAULT ((0)) FOR [VI_8_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VI_9__3A4CA8FD]  DEFAULT ((0)) FOR [VI_9]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_9___3B40CD36]  DEFAULT ((0)) FOR [VI_9_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_10__3C34F16F]  DEFAULT ((0)) FOR [VI_10]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VI_10__3D2915A8]  DEFAULT ((0)) FOR [VI_10_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__PF_AL__3E1D39E1]  DEFAULT ('N') FOR [PF_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__ESI_A__3F115E1A]  DEFAULT ('N') FOR [ESI_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VPF_A__40058253]  DEFAULT ('N') FOR [VPF_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__PROF___40F9A68C]  DEFAULT ('N') FOR [PROF_TAX_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__PROF___41EDCAC5]  DEFAULT ((0)) FOR [PROF_TAX_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VPF_A__42E1EEFE]  DEFAULT ((0)) FOR [VPF_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VVPF___43D61337]  DEFAULT ((0)) FOR [VVPF_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VEPF___44CA3770]  DEFAULT ((0)) FOR [VEPF_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VFPF___45BE5BA9]  DEFAULT ((0)) FOR [VFPF_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VESI___46B27FE2]  DEFAULT ((0)) FOR [VESI_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VTDS___47A6A41B]  DEFAULT ((0)) FOR [VTDS_RATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VTDS___489AC854]  DEFAULT ((0)) FOR [VTDS_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_1__498EEC8D]  DEFAULT ((0)) FOR [VD_1]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_1___4A8310C6]  DEFAULT ((0)) FOR [VD_1_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_2__4B7734FF]  DEFAULT ((0)) FOR [VD_2]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_2___4C6B5938]  DEFAULT ((0)) FOR [VD_2_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_3__4D5F7D71]  DEFAULT ((0)) FOR [VD_3]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_3___4E53A1AA]  DEFAULT ((0)) FOR [VD_3_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_4__4F47C5E3]  DEFAULT ((0)) FOR [VD_4]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_4___503BEA1C]  DEFAULT ((0)) FOR [VD_4_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_5__51300E55]  DEFAULT ((0)) FOR [VD_5]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_5___5224328E]  DEFAULT ((0)) FOR [VD_5_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_6__531856C7]  DEFAULT ((0)) FOR [VD_6]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_6___540C7B00]  DEFAULT ((0)) FOR [VD_6_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_7__55009F39]  DEFAULT ((0)) FOR [VD_7]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_7___55F4C372]  DEFAULT ((0)) FOR [VD_7_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_8__56E8E7AB]  DEFAULT ((0)) FOR [VD_8]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_8___57DD0BE4]  DEFAULT ((0)) FOR [VD_8_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VD_9__58D1301D]  DEFAULT ((0)) FOR [VD_9]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_9___59C55456]  DEFAULT ((0)) FOR [VD_9_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_10__5AB9788F]  DEFAULT ((0)) FOR [VD_10]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VD_10__5BAD9CC8]  DEFAULT ((0)) FOR [VD_10_AMT]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VFINE__5CA1C101]  DEFAULT ((0)) FOR [VFINE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VADVA__5D95E53A]  DEFAULT ((0)) FOR [VADVANCE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VPRE__5E8A0973]  DEFAULT ((0)) FOR [VPRE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VABS__5F7E2DAC]  DEFAULT ((0)) FOR [VABS]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VHLD__607251E5]  DEFAULT ((0)) FOR [VHLD]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VWO__6166761E]  DEFAULT ((0)) FOR [VWO]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VCL__625A9A57]  DEFAULT ((0)) FOR [VCL]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESULT__VSL__634EBE90]  DEFAULT ((0)) FOR [VSL]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VPL_E__6442E2C9]  DEFAULT ((0)) FOR [VPL_EL]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VOTHE__65370702]  DEFAULT ((0)) FOR [VOTHER_LV]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VLEAV__662B2B3B]  DEFAULT ((0)) FOR [VLEAVE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VTDAY__671F4F74]  DEFAULT ((0)) FOR [VTDAYS]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VTOTA__681373AD]  DEFAULT ((0)) FOR [VTOTAL_DAY]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VLATE__690797E6]  DEFAULT ((0)) FOR [VLATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VT_LA__69FBBC1F]  DEFAULT ((0)) FOR [VT_LATE]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VEARL__6AEFE058]  DEFAULT ((0)) FOR [VEARLY]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__VT_EA__6BE40491]  DEFAULT ((0)) FOR [VT_EARLY]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__AMTON__6CD828CA]  DEFAULT ((0)) FOR [AMTONPF]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__AMTON__6DCC4D03]  DEFAULT ((0)) FOR [AMTONVPF]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__AMTON__6EC0713C]  DEFAULT ((0)) FOR [AMTONESI]
GO
ALTER TABLE [dbo].[PAY_RESULT] ADD  CONSTRAINT [DF__PAY_RESUL__ESION__6FB49575]  DEFAULT ((0)) FOR [ESIONOT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VBASIC__72910220]  DEFAULT ((0)) FOR [VBASIC]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VSALAR__73852659]  DEFAULT ((0)) FOR [VSALARY]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VDA_RA__74794A92]  DEFAULT ((0)) FOR [VDA_RATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VDA_AM__756D6ECB]  DEFAULT ((0)) FOR [VDA_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VMED_R__76619304]  DEFAULT ((0)) FOR [VMED_RATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VMED_A__7755B73D]  DEFAULT ((0)) FOR [VMED_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VCONV___7849DB76]  DEFAULT ((0)) FOR [VCONV_RATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VCONV___793DFFAF]  DEFAULT ((0)) FOR [VCONV_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VHRA_R__7A3223E8]  DEFAULT ((0)) FOR [VHRA_RATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VHRA_A__7B264821]  DEFAULT ((0)) FOR [VHRA_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VOT__7C1A6C5A]  DEFAULT ((0)) FOR [VOT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VOT_RA__7D0E9093]  DEFAULT ((0)) FOR [VOT_RATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VOT_AM__7E02B4CC]  DEFAULT ((0)) FOR [VOT_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_1__7EF6D905]  DEFAULT ((0)) FOR [VI_1]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_1_A__7FEAFD3E]  DEFAULT ((0)) FOR [VI_1_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_2__00DF2177]  DEFAULT ((0)) FOR [VI_2]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_2_A__01D345B0]  DEFAULT ((0)) FOR [VI_2_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_3__02C769E9]  DEFAULT ((0)) FOR [VI_3]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_3_A__03BB8E22]  DEFAULT ((0)) FOR [VI_3_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_4__04AFB25B]  DEFAULT ((0)) FOR [VI_4]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_4_A__05A3D694]  DEFAULT ((0)) FOR [VI_4_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_5__0697FACD]  DEFAULT ((0)) FOR [VI_5]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_5_A__078C1F06]  DEFAULT ((0)) FOR [VI_5_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_6__0880433F]  DEFAULT ((0)) FOR [VI_6]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_6_A__09746778]  DEFAULT ((0)) FOR [VI_6_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_7__0A688BB1]  DEFAULT ((0)) FOR [VI_7]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_7_A__0B5CAFEA]  DEFAULT ((0)) FOR [VI_7_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_8__0C50D423]  DEFAULT ((0)) FOR [VI_8]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_8_A__0D44F85C]  DEFAULT ((0)) FOR [VI_8_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_9__0E391C95]  DEFAULT ((0)) FOR [VI_9]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_9_A__0F2D40CE]  DEFAULT ((0)) FOR [VI_9_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_10__10216507]  DEFAULT ((0)) FOR [VI_10]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VI_10___11158940]  DEFAULT ((0)) FOR [VI_10_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__PF_ALL__1209AD79]  DEFAULT ('N') FOR [PF_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__ESI_AL__12FDD1B2]  DEFAULT ('N') FOR [ESI_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VPF_AL__13F1F5EB]  DEFAULT ('N') FOR [VPF_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__PROF_T__14E61A24]  DEFAULT ('N') FOR [PROF_TAX_ALLOWED]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__PROF_T__15DA3E5D]  DEFAULT ((0)) FOR [PROF_TAX_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VPF_AM__16CE6296]  DEFAULT ((0)) FOR [VPF_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VVPF_A__17C286CF]  DEFAULT ((0)) FOR [VVPF_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VEPF_A__18B6AB08]  DEFAULT ((0)) FOR [VEPF_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VFPF_A__19AACF41]  DEFAULT ((0)) FOR [VFPF_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VESI_A__1A9EF37A]  DEFAULT ((0)) FOR [VESI_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VTDS_R__1B9317B3]  DEFAULT ((0)) FOR [VTDS_RATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VTDS_A__1C873BEC]  DEFAULT ((0)) FOR [VTDS_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_1__1D7B6025]  DEFAULT ((0)) FOR [VD_1]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_1_A__1E6F845E]  DEFAULT ((0)) FOR [VD_1_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_2__1F63A897]  DEFAULT ((0)) FOR [VD_2]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_2_A__2057CCD0]  DEFAULT ((0)) FOR [VD_2_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_3__214BF109]  DEFAULT ((0)) FOR [VD_3]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_3_A__22401542]  DEFAULT ((0)) FOR [VD_3_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_4__2334397B]  DEFAULT ((0)) FOR [VD_4]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_4_A__24285DB4]  DEFAULT ((0)) FOR [VD_4_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_5__251C81ED]  DEFAULT ((0)) FOR [VD_5]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_5_A__2610A626]  DEFAULT ((0)) FOR [VD_5_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_6__2704CA5F]  DEFAULT ((0)) FOR [VD_6]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_6_A__27F8EE98]  DEFAULT ((0)) FOR [VD_6_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_7__28ED12D1]  DEFAULT ((0)) FOR [VD_7]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_7_A__29E1370A]  DEFAULT ((0)) FOR [VD_7_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_8__2AD55B43]  DEFAULT ((0)) FOR [VD_8]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_8_A__2BC97F7C]  DEFAULT ((0)) FOR [VD_8_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_9__2CBDA3B5]  DEFAULT ((0)) FOR [VD_9]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_9_A__2DB1C7EE]  DEFAULT ((0)) FOR [VD_9_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_10__2EA5EC27]  DEFAULT ((0)) FOR [VD_10]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VD_10___2F9A1060]  DEFAULT ((0)) FOR [VD_10_AMT]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VFINE__308E3499]  DEFAULT ((0)) FOR [VFINE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VADVAN__318258D2]  DEFAULT ((0)) FOR [VADVANCE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VPRE__32767D0B]  DEFAULT ((0)) FOR [VPRE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VABS__336AA144]  DEFAULT ((0)) FOR [VABS]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VHLD__345EC57D]  DEFAULT ((0)) FOR [VHLD]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VWO__3552E9B6]  DEFAULT ((0)) FOR [VWO]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VCL__36470DEF]  DEFAULT ((0)) FOR [VCL]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VSL__373B3228]  DEFAULT ((0)) FOR [VSL]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VPL_EL__382F5661]  DEFAULT ((0)) FOR [VPL_EL]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VOTHER__39237A9A]  DEFAULT ((0)) FOR [VOTHER_LV]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VLEAVE__3A179ED3]  DEFAULT ((0)) FOR [VLEAVE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VTDAYS__3B0BC30C]  DEFAULT ((0)) FOR [VTDAYS]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VTOTAL__3BFFE745]  DEFAULT ((0)) FOR [VTOTAL_DAY]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VLATE__3CF40B7E]  DEFAULT ((0)) FOR [VLATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VT_LAT__3DE82FB7]  DEFAULT ((0)) FOR [VT_LATE]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VEARLY__3EDC53F0]  DEFAULT ((0)) FOR [VEARLY]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__VT_EAR__3FD07829]  DEFAULT ((0)) FOR [VT_EARLY]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__AMTONP__40C49C62]  DEFAULT ((0)) FOR [AMTONPF]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__AMTONV__41B8C09B]  DEFAULT ((0)) FOR [AMTONVPF]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__AMTONE__42ACE4D4]  DEFAULT ((0)) FOR [AMTONESI]
GO
ALTER TABLE [dbo].[PAY_TEMP] ADD  CONSTRAINT [DF__PAY_TEMP__ESIONO__43A1090D]  DEFAULT ((0)) FOR [ESIONOT]
GO
ALTER TABLE [dbo].[PhoneAPIServerConfig] ADD  CONSTRAINT [DF_PhoneAPIServerConfig_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PhoneAPIServerConfig] ADD  CONSTRAINT [DF_APIServerConfiguration_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PhoneAPIServerConfig] ADD  CONSTRAINT [DF_APIServerConfiguration_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PhoneAPIServerConfig] ADD  CONSTRAINT [DF_PhoneAPIServerConfig_UseTestMode]  DEFAULT ((0)) FOR [UseTestMode]
GO
ALTER TABLE [dbo].[SerialKeyValidator] ADD  CONSTRAINT [DF_SerialKeyValidator_isActive]  DEFAULT ((0)) FOR [isActive]
GO
ALTER TABLE [dbo].[TBLADVANCE] ADD  DEFAULT ((0)) FOR [ADV_AMT]
GO
ALTER TABLE [dbo].[TBLADVANCE] ADD  DEFAULT ((0)) FOR [INST_AMT]
GO
ALTER TABLE [dbo].[TBLADVANCE] ADD  DEFAULT ((0)) FOR [INSTNO]
GO
ALTER TABLE [dbo].[TBLADVANCE] ADD  DEFAULT ((0)) FOR [INTREST_RATE]
GO
ALTER TABLE [dbo].[TBLADVANCEDATA] ADD  DEFAULT ((0)) FOR [INST_AMT]
GO
ALTER TABLE [dbo].[TBLADVANCEDATA] ADD  DEFAULT ((0)) FOR [CASH_AMT]
GO
ALTER TABLE [dbo].[TBLADVANCEDATA] ADD  DEFAULT ((0)) FOR [BALANCE_AMT]
GO
ALTER TABLE [dbo].[TBLADVANCEDATA] ADD  DEFAULT ((0)) FOR [PAMT]
GO
ALTER TABLE [dbo].[TBLADVANCEDATA] ADD  DEFAULT ((0)) FOR [IAMT]
GO
ALTER TABLE [dbo].[TBLADVANCEDATA] ADD  DEFAULT ((0)) FOR [INSTNO]
GO
ALTER TABLE [dbo].[tblAtt_Mail] ADD  DEFAULT ('N') FOR [Sent]
GO
ALTER TABLE [dbo].[tblCalander] ADD  DEFAULT ('N') FOR [AutoEL]
GO
ALTER TABLE [dbo].[tblemployee] ADD  CONSTRAINT [DF__tblemploy__Templ__638EB5B2]  DEFAULT ('N') FOR [Template_Send]
GO
ALTER TABLE [dbo].[tblemployee] ADD  CONSTRAINT [DF__tblemploy__Grade__6576FE24]  DEFAULT ((15)) FOR [Grade]
GO
ALTER TABLE [dbo].[tblEmployeeShiftMaster] ADD  CONSTRAINT [DF__tblEmploye__TIME__07020F21]  DEFAULT ((0)) FOR [TIME]
GO
ALTER TABLE [dbo].[tblEmployeeShiftMaster] ADD  CONSTRAINT [DF__tblEmploy__SHORT__07F6335A]  DEFAULT ((0)) FOR [SHORT]
GO
ALTER TABLE [dbo].[tblEmployeeShiftMaster] ADD  CONSTRAINT [DF__tblEmploye__HALF__08EA5793]  DEFAULT ((0)) FOR [HALF]
GO
ALTER TABLE [dbo].[tblGrade] ADD  CONSTRAINT [DF__tblGrade__Grade__6482D9EB]  DEFAULT ((15)) FOR [Grade]
GO
ALTER TABLE [dbo].[tblMachine] ADD  CONSTRAINT [DF_tblMachine_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[tblMachine] ADD  CONSTRAINT [DF_tblMachine_DeviceMode]  DEFAULT ('BIO') FOR [DeviceMode]
GO
ALTER TABLE [dbo].[tblMachine] ADD  CONSTRAINT [DF_tblMachine_Timezone]  DEFAULT ((330)) FOR [Timezone]
GO
ALTER TABLE [dbo].[TblMailShoot] ADD  DEFAULT ('N') FOR [Bday]
GO
ALTER TABLE [dbo].[TblMailShoot] ADD  DEFAULT ('N') FOR [Anniversary]
GO
ALTER TABLE [dbo].[TblMailShoot] ADD  DEFAULT ('N') FOR [called]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [HOURSWORKED]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [EXCLUNCHHOURS]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [OTDURATION]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [OSDURATION]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [OTAMOUNT]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [EARLYARRIVAL]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [EARLYDEPARTURE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LATEARRIVAL]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LUNCHEARLYDEPARTURE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LUNCHLATEARRIVAL]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [TOTALLOSSHRS]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LEAVEVALUE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [PRESENTVALUE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [ABSENTVALUE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [HOLIDAY_VALUE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [WO_VALUE]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [OUTWORKDURATION]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LEAVEAMOUNT]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LEAVEAMOUNT1]
GO
ALTER TABLE [dbo].[tblTimeRegisterd] ADD  DEFAULT ((0)) FOR [LEAVEAMOUNT2]
GO
ALTER TABLE [dbo].[UserPicture] ADD  CONSTRAINT [DF_UserPicture_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[ZKServiceErrorLog] ADD  CONSTRAINT [DF_ZKServiceErrorLog_ErrorTime]  DEFAULT (getdate()) FOR [ErrorTime]
GO
/****** Object:  StoredProcedure [dbo].[AddLeaveRequest]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddLeaveRequest] 
	-- Add the parameters for the stored procedure here
	@PayCode varchar(50),
	@RequestId varchar(50),
	@RequestDate datetime,
	@LeaveFrom datetime,
	@LeaveTo datetime,
	@LeaveCode varchar(15),
	@HalfDay varchar(5),
	@LeaveDays float,
	@UserRemarks varchar(255),
	@SSN varchar(20),
	@Status bit output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ApplicationNo NUMERIC(18,0)
	DECLARE @LeaveApprovalStages AS INT
	DECLARE @HeadId AS VARCHAR(50)
	DECLARE @HeadId2 AS VARCHAR(50)
	DECLARE @CompanyCode AS VARCHAR(50)	
	DECLARE @DepartmentCode AS VARCHAR(50)
	DECLARE @IsHolidayInclude AS BIT=0
	DECLARE @IsOffInclude AS VARCHAR(1)
	DECLARE @HolidayCount AS VARCHAR(1)=0
	DECLARE @WeekOffCount AS VARCHAR(1)=0
	DECLARE @TempLeaveCount FLOAT=0
	DECLARE @LeaveField AS VARCHAR(50)
	DECLARE @LeaveDescription AS VARCHAR(50)



	-- Get the days counts between two days
	SET @TempLeaveCount  = DATEDIFF(DAY, @LeaveFrom, @LeaveTo) + 1
	--SET @TempLeaveCount = @LeaveDays

    -- Check If Holiday Is Included
	IF EXISTS(SELECT * FROM tblleavemaster WHERE LEAVECODE = @LeaveCode  and ISHOLIDAYINCLUDE ='N')
	BEGIN	    
		SELECT @HolidayCount=COUNT(*) FROM TBLTIMEREGISTER WHERE HOLIDAY_VALUE > 0 AND SSN=@SSN AND DATEOFFICE BETWEEN @LeaveFrom AND @LeaveTo
		SET @TempLeaveCount = @TempLeaveCount - @HolidayCount
	END

	
	-- Check if IsOff included
	IF EXISTS(SELECT * FROM tblleavemaster WHERE LEAVECODE = @LeaveCode  and ISOFFINCLUDE ='N')
	BEGIN	    
		SELECT @WeekOffCount=COUNT(*) FROM TBLTIMEREGISTER WHERE WO_VALUE > 0 AND SSN=@SSN AND DATEOFFICE BETWEEN @LeaveFrom AND @LeaveTo
		SET @TempLeaveCount = @TempLeaveCount - @WeekOffCount
	END


   -- GET CompanyCode and Department Code to get holidays
	SELECT  @CompanyCode=COMPANYCODE, @DepartmentCode=DepartmentCode from tblemployee Where SSN=@SSN
    
	--TODO: To be removed.  GET Description and Leave Field. It should be passed as parameters. 
	SELECT @LeaveField=LEAVEFIELD, @LeaveDescription=LEAVEDESCRIPTION FROM tblleavemaster WHERE LEAVECODE = @LeaveCode and CompanyCode = @CompanyCode

	-- This needs to be calcuated at last after calculating Holidays, Week End, Acrual
	IF @HalfDay <> 'N'
	BEGIN
		SET @TempLeaveCount = @TempLeaveCount /2		
	END
		
	SET @Status = 1
	Select @ApplicationNo= Max(application_no) from leave_request(nolock)

	select @LeaveApprovalStages = LeaveApprovalStages, 
	       @HeadId =headid, 
		   @HeadId2 = headid_2 from tblemployee(nolock) where PAYCODE=@PayCode and SSN =@SSN

    

	IF @LeaveApprovalStages = 2
	 BEGIN

	-- Insert statements for procedure here
	INSERT INTO leave_request (
	        [application_no],
	        [paycode]
           ,[request_id]
           ,[request_date]
           ,[leave_from]
           ,[leave_to]
           ,[leavecode]
           ,[halfday]
           ,[leavedays]
           ,[userremarks]
		   ,[ssn]
		   ,[Stage1_approval_id]
		   ,[Stage2_approval_id]
		   )
		   values
		   (
		    @ApplicationNo+1,
			@PayCode, 
			@RequestId, 
			@RequestDate, 
			@LeaveFrom, 
			@LeaveTo, 
			@LeaveCode, 
			@HalfDay, 
			@TempLeaveCount,
			@UserRemarks,
			@SSN, 
			@HeadId,
			@HeadId2
		   )	
		   END
		   ELSE	
		   BEGIN	   
		   	INSERT INTO leave_request (
	        [application_no],
	        [paycode]
           ,[request_id]
           ,[request_date]
           ,[leave_from]
           ,[leave_to]
           ,[leavecode]
           ,[halfday]
           ,[leavedays]
           ,[userremarks]
		   ,[ssn]
		   ,[Stage1_approval_id]
		   )
		   values
		   (
		    @ApplicationNo+1,
			@PayCode, 
			@RequestId, 
			@RequestDate, 
			@LeaveFrom, 
			@LeaveTo, 
			@LeaveCode, 
			@HalfDay, 
			@TempLeaveCount,
			@UserRemarks,
			@SSN,
			@HeadId
		   )
		   END
		   	
				  
		  SELECT @LeaveField=LEAVEFIELD, @LeaveDescription=LEAVEDESCRIPTION FROM tblleavemaster WHERE LEAVECODE = @LeaveCode and CompanyCode=@CompanyCode
		  IF EXISTS (SELECT * FROM tblleavemaster WHERE LEAVECODE = @LeaveCode)
		  BEGIN		  
		        IF(@LeaveField = 'L01')
				BEGIN
					UPDATE [tblleaveledger] SET L01 = L01+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L02')
				BEGIN 
					UPDATE [tblleaveledger] SET L02 = L02+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L03')
				BEGIN 
					UPDATE [tblleaveledger] SET L03 = L03+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L04')
				BEGIN 
					UPDATE [tblleaveledger] SET L04 = L04+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L05')
				BEGIN 
					UPDATE [tblleaveledger] SET L05 = L05+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L06')
				BEGIN 
					UPDATE [tblleaveledger] SET L06 = L06+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L07')
				BEGIN 
					UPDATE [tblleaveledger] SET L07 = L07+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L08')
				BEGIN 
					UPDATE [tblleaveledger] SET L08 = L08+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L09')
				BEGIN 
					UPDATE [tblleaveledger] SET L09 = L09+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L10')
				BEGIN 
					UPDATE [tblleaveledger] SET L10 = L10+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L11')
				BEGIN 
					UPDATE [tblleaveledger] SET L11 = L11+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L12')
				BEGIN 
					UPDATE [tblleaveledger] SET L12 = L12+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L13')
				BEGIN 
					UPDATE [tblleaveledger] SET L13 = L13+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L14')
				BEGIN 
					UPDATE [tblleaveledger] SET L14 = L14+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L15')
				BEGIN 
					UPDATE [tblleaveledger] SET L15 = L15+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L16')
				BEGIN 
					UPDATE [tblleaveledger] SET L16 = L16+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L17')
				BEGIN 
					UPDATE [tblleaveledger] SET L17 = L17+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L18')
				BEGIN 
					UPDATE [tblleaveledger] SET L18 = L18+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L19')
				BEGIN 
					UPDATE [tblleaveledger] SET L19 = L19+@TempLeaveCount Where SSN =@SSN
				END
				ELSE IF (@LeaveField = 'L20')
				BEGIN 
					UPDATE [tblleaveledger] SET L20 = L20+@TempLeaveCount Where SSN =@SSN
				END
		  END	 
		 
END




GO
/****** Object:  StoredProcedure [dbo].[checkpassword]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[checkpassword] @user varchar(1000), @password varchar(100),@companycode varchar(100), @newpassword varchar(100) as if exists(select * from tbluser where user_r=@user and password=@password and CompanyCode=@companycode) begin update tbluser set password=@newpassword where user_r=@user and password=@password and CompanyCode=@companycode end



GO
/****** Object:  StoredProcedure [dbo].[GetAttendanceStatus]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAttendanceStatus] 
	@PayCode as varchar(50) ,
	@CompanyCode as varchar(50),
	@SSN as varchar(50),
	@AttendanceDate as DateTime,
	@HeadCount as INT OUTPUT, 
	@PresentCount as FLOAT OUTPUT, 
	@AbsentCount as FLOAT OUTPUT, 
	@LeaveCount as FLOAT OUTPUT, 
	@HolidayCount as FLOAT OUTPUT, 
	@OffCount as FLOAT OUTPUT, 
	@LateCount as FLOAT OUTPUT
AS
BEGIN
	IF 1=0 
	BEGIN
		SET FMTONLY OFF
	END

		SET NOCOUNT ON;
	
	SELECT 
	    CONVERT(varchar(50),'') as PayCode,
	    CONVERT(integer,0) as HeadCount,        
        CONVERT(decimal(10,2),0) as PresentCount,
        CONVERT(decimal(10,2),0) as AbsentCount,
        CONVERT(decimal(10,2),0) as LeaveCount,
	    CONVERT(decimal(10,2),0) as HolidayCount,
        CONVERT(decimal(10,2),0) as OffCount,
		CONVERT(decimal(10,2),0) as LateCount


	IF @AttendanceDate IS NULL
	BEGIN
		SET @AttendanceDate =  DATEADD(D,DATEDIFF(D,0,GETDATE()),0)
	END

	DECLARE  @TempTimeRegister TABLE
	(
	   DateOFFICE DATETIME,
	   [STATUS] CHAR(6),
	   LEAVEAmount float,
	   Holiday_Value float,
	   WO_VALUE float,
	   LateArrival int,
	   PresentValue float,
	   AbsentValue float
	)

	DECLARE @UserType CHAR(1)

	SELECT @UserType=USERTYPE FROM TBLUSER WHERE USER_R=@PayCode and CompanyCode=@CompanyCode

	IF @UserType = 'A'
	BEGIN 
		INSERT INTO @TempTimeRegister SELECT DateOFFICE, [STATUS] , LEAVEAmount, Holiday_Value, WO_VALUE, LateArrival, PresentValue, AbsentValue
		FROM tbltimeregister  where DateOFFICE = @AttendanceDate 
	END

	IF @UserType = 'H'
	BEGIN
	  INSERT INTO @TempTimeRegister SELECT DateOFFICE, [STATUS] , LEAVEAmount, Holiday_Value, WO_VALUE, LateArrival, PresentValue, AbsentValue
	  FROM tbltimeregister where DateOFFICE = @AttendanceDate and SUBSTRING(SSN, 0, charindex('_', SSN))= @CompanyCode
	END
	
	DECLARE  @AttendanceStatus TABLE
	(
	   PayCode VARCHAR(20),
	   AttendanceDate DATE,
	   HeadCount INT,
	   PresentCount FLOAT,
	   AbsentCount FLOAT,
	   LeaveCount FLOAT,
	   HolidayCount FLOAT,
	   OffCount FLOAT,
	   LateCount FLOAT
	)
	

	---- Get Total count as count of employee
	--SET @HeadCount = 75

	---- Get the count of present as employee PRESENTVALUE Count
	--SET @PresentCount = 70

	
	---- Get the ABSENTVALUE Count
	--SET @AbsentCount = 5

	---- Get the LeaveCount by  COUNT(LEAVEAmount > 0)
	--SET @LeaveCount= 10

	---- Get the Holiday Count by COUNT(Holiday_Value > 0)
	--SET @HolidayCount = 3

	---- Get the Off count by COUNT(WO > 1)
	--SET @OffCount = 5

	---- Get the Late count by COUNT(LateArrival >1 )
	--SET @LateCount = 3


	SELECT  @HeadCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate  
	SELECT  @PresentCount = SUM(ISNULL(PresentValue,0)) from @TempTimeRegister where DateOFFICE = @AttendanceDate
	SELECT  @AbsentCount = SUM(ISNULL(AbsentValue,0)) from @TempTimeRegister where DateOFFICE = @AttendanceDate  
	SELECT  @LeaveCount = SUM(ISNULL(LEAVEAmount,0)) from @TempTimeRegister where DateOFFICE = @AttendanceDate 
	SELECT  @HolidayCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and Holiday_Value > 0
	SELECT  @OffCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and WO_VALUE > 0
	SELECT  @LateCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and LateArrival > 0

	INSERT INTO @AttendanceStatus ( PayCode, AttendanceDate,  HeadCount,PresentCount, AbsentCount, LeaveCount, HolidayCount, OffCount, LateCount)
	SELECT  @PayCode, @AttendanceDate,@HeadCount, @PresentCount, @AbsentCount, @LeaveCount, @HolidayCount, @OffCount, @LateCount

	SELECT * FROM @AttendanceStatus

	END



GO
/****** Object:  StoredProcedure [dbo].[GetAttendanceStatus_Old]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAttendanceStatus_Old] 
	@PayCode as varchar(50) ,
	@CompanyCode as varchar(50),
	@SSN as varchar(50),
	@AttendanceDate as DateTime,
	@HeadCount as INT OUTPUT, 
	@PresentCount as FLOAT OUTPUT, 
	@AbsentCount as FLOAT OUTPUT, 
	@LeaveCount as FLOAT OUTPUT, 
	@HolidayCount as FLOAT OUTPUT, 
	@OffCount as FLOAT OUTPUT, 
	@LateCount as FLOAT OUTPUT
AS
BEGIN
	IF 1=0 
	BEGIN
		SET FMTONLY OFF
	END

		SET NOCOUNT ON;
	
	SELECT 
	    CONVERT(varchar(50),'') as PayCode,
	    CONVERT(integer,0) as HeadCount,        
        CONVERT(decimal(10,2),0) as PresentCount,
        CONVERT(decimal(10,2),0) as AbsentCount,
        CONVERT(decimal(10,2),0) as LeaveCount,
	    CONVERT(decimal(10,2),0) as HolidayCount,
        CONVERT(decimal(10,2),0) as OffCount,
		CONVERT(decimal(10,2),0) as LateCount


	IF @AttendanceDate IS NULL
	BEGIN
		SET @AttendanceDate =  DATEADD(D,DATEDIFF(D,0,GETDATE()),0)
	END

	DECLARE  @TempTimeRegister TABLE
	(
	   DateOFFICE DATETIME,
	   [STATUS] CHAR(6),
	   LEAVEAmount float,
	   Holiday_Value float,
	   WO_VALUE float,
	   LateArrival int
	)

	DECLARE @UserType CHAR(1)

	SELECT @UserType=USERTYPE FROM TBLUSER WHERE USER_R=@PayCode and CompanyCode=@CompanyCode

	IF @UserType = 'A'
	BEGIN 
		INSERT INTO @TempTimeRegister SELECT DateOFFICE, [STATUS] , LEAVEAmount, Holiday_Value, WO_VALUE, LateArrival 
		FROM tbltimeregister  where DateOFFICE = @AttendanceDate 
	END

	IF @UserType = 'H'
	BEGIN
	  INSERT INTO @TempTimeRegister SELECT DateOFFICE, [STATUS] , LEAVEAmount, Holiday_Value, WO_VALUE, LateArrival 
	  FROM tbltimeregister where DateOFFICE = @AttendanceDate and SUBSTRING(SSN, 0, charindex('_', SSN))= @CompanyCode
	END
	
	DECLARE  @AttendanceStatus TABLE
	(
	   PayCode VARCHAR(20),
	   AttendanceDate DATE,
	   HeadCount INT,
	   PresentCount FLOAT,
	   AbsentCount FLOAT,
	   LeaveCount FLOAT,
	   HolidayCount FLOAT,
	   OffCount FLOAT,
	   LateCount FLOAT
	)
	

	---- Get Total count as count of employee
	--SET @HeadCount = 75

	---- Get the count of present as employee PRESENTVALUE Count
	--SET @PresentCount = 70

	
	---- Get the ABSENTVALUE Count
	--SET @AbsentCount = 5

	---- Get the LeaveCount by  COUNT(LEAVEAmount > 0)
	--SET @LeaveCount= 10

	---- Get the Holiday Count by COUNT(Holiday_Value > 0)
	--SET @HolidayCount = 3

	---- Get the Off count by COUNT(WO > 1)
	--SET @OffCount = 5

	---- Get the Late count by COUNT(LateArrival >1 )
	--SET @LateCount = 3


	SELECT  @HeadCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate  
	SELECT  @PresentCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and [STATUS] = 'P' 
	SELECT  @AbsentCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and [STATUS] = 'A' 
	SELECT  @LeaveCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and LEAVEAmount > 0
	SELECT  @HolidayCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and Holiday_Value > 0
	SELECT  @OffCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and WO_VALUE > 0
	SELECT  @LateCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and LateArrival > 0

	INSERT INTO @AttendanceStatus ( PayCode, AttendanceDate,  HeadCount,PresentCount, AbsentCount, LeaveCount, HolidayCount, OffCount, LateCount)
	SELECT  @PayCode, @AttendanceDate,@HeadCount, @PresentCount, @AbsentCount, @LeaveCount, @HolidayCount, @OffCount, @LateCount

	SELECT * FROM @AttendanceStatus
	
END



GO
/****** Object:  StoredProcedure [dbo].[GetAttendanceStatus_Test]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAttendanceStatus_Test] 
	@PayCode as varchar(50) ,
	@CompanyCode as varchar(50),
	@SSN as varchar(50),
	@AttendanceDate as DateTime,
	@HeadCount as INT OUTPUT, 
	@PresentCount as FLOAT OUTPUT, 
	@AbsentCount as FLOAT OUTPUT, 
	@LeaveCount as FLOAT OUTPUT, 
	@HolidayCount as FLOAT OUTPUT, 
	@OffCount as FLOAT OUTPUT, 
	@LateCount as FLOAT OUTPUT
AS
BEGIN
	IF 1=0 
	BEGIN
		SET FMTONLY OFF
	END

		SET NOCOUNT ON;
	
	SELECT 
	    CONVERT(varchar(50),'') as PayCode,
	    CONVERT(integer,0) as HeadCount,        
        CONVERT(decimal(10,2),0) as PresentCount,
        CONVERT(decimal(10,2),0) as AbsentCount,
        CONVERT(decimal(10,2),0) as LeaveCount,
	    CONVERT(decimal(10,2),0) as HolidayCount,
        CONVERT(decimal(10,2),0) as OffCount,
		CONVERT(decimal(10,2),0) as LateCount


	IF @AttendanceDate IS NULL
	BEGIN
		SET @AttendanceDate =  DATEADD(D,DATEDIFF(D,0,GETDATE()),0)
	END

	DECLARE  @TempTimeRegister TABLE
	(
	   DateOFFICE DATETIME,
	   [STATUS] CHAR(6),
	   LEAVEAmount float,
	   Holiday_Value float,
	   WO_VALUE float,
	   LateArrival int,
	   PresentValue float,
	   AbsentValue float
	)

	DECLARE @UserType CHAR(1)

	SELECT @UserType=USERTYPE FROM TBLUSER WHERE USER_R=@PayCode and CompanyCode=@CompanyCode

	IF @UserType = 'A'
	BEGIN 
		INSERT INTO @TempTimeRegister SELECT DateOFFICE, [STATUS] , LEAVEAmount, Holiday_Value, WO_VALUE, LateArrival, PresentValue, AbsentValue
		FROM tbltimeregister  where DateOFFICE = @AttendanceDate 
	END

	IF @UserType = 'H'
	BEGIN
	  INSERT INTO @TempTimeRegister SELECT DateOFFICE, [STATUS] , LEAVEAmount, Holiday_Value, WO_VALUE, LateArrival, PresentValue, AbsentValue
	  FROM tbltimeregister where DateOFFICE = @AttendanceDate and SUBSTRING(SSN, 0, charindex('_', SSN))= @CompanyCode
	END
	
	DECLARE  @AttendanceStatus TABLE
	(
	   PayCode VARCHAR(20),
	   AttendanceDate DATE,
	   HeadCount INT,
	   PresentCount FLOAT,
	   AbsentCount FLOAT,
	   LeaveCount FLOAT,
	   HolidayCount FLOAT,
	   OffCount FLOAT,
	   LateCount FLOAT
	)
	

	---- Get Total count as count of employee
	--SET @HeadCount = 75

	---- Get the count of present as employee PRESENTVALUE Count
	--SET @PresentCount = 70

	
	---- Get the ABSENTVALUE Count
	--SET @AbsentCount = 5

	---- Get the LeaveCount by  COUNT(LEAVEAmount > 0)
	--SET @LeaveCount= 10

	---- Get the Holiday Count by COUNT(Holiday_Value > 0)
	--SET @HolidayCount = 3

	---- Get the Off count by COUNT(WO > 1)
	--SET @OffCount = 5

	---- Get the Late count by COUNT(LateArrival >1 )
	--SET @LateCount = 3


	SELECT  @HeadCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate  
	SELECT  @PresentCount = SUM(ISNULL(PresentValue,0)) from @TempTimeRegister where DateOFFICE = @AttendanceDate
	SELECT  @AbsentCount = SUM(ISNULL(AbsentValue,0)) from @TempTimeRegister where DateOFFICE = @AttendanceDate  
	SELECT  @LeaveCount = SUM(ISNULL(LEAVEAmount,0)) from @TempTimeRegister where DateOFFICE = @AttendanceDate 
	SELECT  @HolidayCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and Holiday_Value > 0
	SELECT  @OffCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and WO_VALUE > 0
	SELECT  @LateCount = COUNT(*) from @TempTimeRegister where DateOFFICE = @AttendanceDate and LateArrival > 0

	INSERT INTO @AttendanceStatus ( PayCode, AttendanceDate,  HeadCount,PresentCount, AbsentCount, LeaveCount, HolidayCount, OffCount, LateCount)
	SELECT  @PayCode, @AttendanceDate,@HeadCount, @PresentCount, @AbsentCount, @LeaveCount, @HolidayCount, @OffCount, @LateCount

	SELECT * FROM @AttendanceStatus

	END



GO
/****** Object:  StoredProcedure [dbo].[GetForgottenPassword]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[GetForgottenPassword]
(
	@UserID as varchar(100),
	@EmailID as varchar(200),
	@DateOfBirth as DateTime,
	@CompanyCode as varchar(100)

)
AS
BEGIN
	select t.USER_R,t.PASSWORD,e.E_MAIL1 Email , e.EMPNAME
	from tbluser t inner join tblEmployee e 
	on t.paycode=e.PAYCODE 
	where USER_R=@UserID and e.DateOFBIRTH = @DateOfBirth and e.E_MAIL1 = @EmailID and e.COMPANYCODE = @CompanyCode
END





GO
/****** Object:  StoredProcedure [dbo].[GetLeaveRequestEmailDetails]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetLeaveRequestEmailDetails]
	@PayCode varchar(50),
	@SSN varchar(20)
AS
BEGIN
	
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT  EMP.E_MAIL1 AS EMPLOYEEEMAILID, 
			EMP.EMPNAME, 
			HEAD1.E_MAIL1 AS HEAD1EMAILID , 
			HEAD1.EMPNAME AS HEAD1NAME,
			HEAD1.PAYCODE AS HEAD1PAYCODE,
			HEAD1.SSN AS HEAD1SSN,
			HEAD2.E_MAIL1 AS HEAD2EMAILID , 
			HEAD2.EMPNAME AS HEAD2NAME,
			HEAD2.PAYCODE AS HEAD2PAYCODE,
			HEAD2.SSN AS HEAD2SSN
	FROM TBLEMPLOYEE EMP 
	LEFT JOIN TBLEMPLOYEE(NOLOCK) HEAD1 ON EMP.HEADID = HEAD1.SSN
	LEFT JOIN TBLEMPLOYEE(NOLOCK) HEAD2 ON EMP.HEADID_2 = HEAD2.PAYCODE
	WHERE EMP.PAYCODE = @PAYCODE AND EMP.SSN = @SSN
END




GO
/****** Object:  StoredProcedure [dbo].[ProcessAllRecords]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessAllRecords]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@CompanyCode VARCHAR(10),
	@PayCode VARCHAR(10),
	@SetupId Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- tblSetup variables
	DECLARE @TempGNightShiftFourPunch CHAR(1)

	DECLARE @TempCompanyCode VARCHAR(10)
	DECLARE @TempDepartmentCode VARCHAR(10)
	DECLARE @TempDayHour FLOAT
	DECLARE @TempSSN VARCHAR(100)
	DECLARE @TempPayCode VARCHAR(10)
	DECLARE @TempIsOutWork CHAR(1)
	DECLARE @TempIsPunchAll CHAR(1)
	DECLARE @TempRSShiftAttended CHAR(3)

	-- RsEMP temp varaibles
	DECLARE @TempRsEmpTwo CHAR(1)
	DECLARE @TempRsEmpInOnly CHAR(1)
	
	-- For MachineRowPunch
	DECLARE @TempReasonCode VARCHAR(100)
	DECLARE @TempFirstPunchReasonCode VARCHAR(100)
	DECLARE @TempLastPunchReasonCode VARCHAR(100)
	DECLARE @TempRsTOut DATETIME
	
	
	DECLARE @TempRsPunchOFficePunch DATETIME
	DECLARE @TempRSPunchPDay CHAR(1)
	DECLARE @TempMPFlg CHAR(1)
	DECLARE @TempExecuteMachineRawUpdate BIT

	-- RsOut Temp Variables for INSERT
	DECLARE @TempRsOutDateOffice DATETIME
	DECLARE @TempRsOutPayCode VARCHAR(10)
	DECLARE @TempRsOutIN1 DATETIME 
	DECLARE @TempRsOutRIN1 CHAR(3) 
	DECLARE @TempRsOutOUT1 DATETIME 
	DECLARE @TempRsOutROUT1 CHAR(3) 
	DECLARE @TempRsOutIN2 DATETIME 
	DECLARE @TempRsOutRIN2 CHAR(3) 
	DECLARE @TempRsOutOUT2 DATETIME 
	DECLARE @TempRsOutROUT2 CHAR(3) 
	DECLARE @TempRsOutIN3 DATETIME 
	DECLARE @TempRsOutRIN3 CHAR(3) 
	DECLARE @TempRsOutOUT3 DATETIME 
	DECLARE @TempRsOutROUT3 CHAR(3) 
	DECLARE @TempRsOutIN4 DATETIME 
	DECLARE @TempRsOutRIN4 CHAR(3) 
	DECLARE @TempRsOutOUT4 DATETIME 
	DECLARE @TempRsOutROUT4 CHAR(3) 
	DECLARE @TempRsOutIN5 DATETIME 
	DECLARE @TempRsOutRIN5 CHAR(3) 
	DECLARE @TempRsOutOUT5 DATETIME 
	DECLARE @TempRsOutROUT5 CHAR(3) 
	DECLARE @TempRsOutIN6 DATETIME 
	DECLARE @TempRsOutRIN6 CHAR(3) 
	DECLARE @TempRsOutOUT6 DATETIME 
	DECLARE @TempRsOutROUT6 CHAR(3) 
	DECLARE @TempRsOutIN7 DATETIME 
	DECLARE @TempRsOutRIN7 CHAR(3) 
	DECLARE @TempRsOutOUT7 DATETIME 
	DECLARE @TempRsOutROUT7 CHAR(3) 
	DECLARE @TempRsOutIN8 DATETIME 
	DECLARE @TempRsOutRIN8 CHAR(3) 
	DECLARE @TempRsOutOUT8 DATETIME 
	DECLARE @TempRsOutROUT8 CHAR(3) 
	DECLARE @TempRsOutIN9 DATETIME 
	DECLARE @TempRsOutRIN9 CHAR(3) 
	DECLARE @TempRsOutOUT9 DATETIME 
	DECLARE @TempRsOutROUT9 CHAR(3) 
	DECLARE @TempRsOutIN10 DATETIME 
	DECLARE @TempRsOutRIN10 CHAR(3) 
	DECLARE @TempRsOutOUT10 DATETIME 
	DECLARE @TempRsOutROUT10 CHAR(3) 
	DECLARE @TempRsOutOutWorkDuration int
	DECLARE @TempRsOutReasonOutWork CHAR(1)
	DECLARE @TempRsOutSSN VARCHAR(100)	
	DECLARE @TempRsOutRecordCount INT

	DECLARE @IsPreviousDaySet BIT

		
	DECLARE @TempMReasonCode VARCHAR(100)
	DECLARE @TempMFirstPunchReasonCode VARCHAR(100)
	DECLARE @TempMLastPunchReasonCode VARCHAR(100)
	DECLARE @TempTimeRegsiterCount INT
	DECLARE @TempProcDate DATETIME
	DECLARE @TempMIn1 DATETIME
	DECLARE @TempMIn2 DATETIME
	DECLARE @TempMOut1 DATETIME
	DECLARE @TempMOut2 DATETIME
	DECLARE @TempMShiftStartTime DATETIME
	DECLARE @TempMShiftEndTime DATETIME
	DECLARE @TempMLunchStartTime DATETIME
	DECLARE @TempMLunchEndTime DATETIME
	DECLARE @TempMDate1 DATETIME
	DECLARE @TempMDate2 DATETIME
	DECLARE @TempMIn1Manual CHAR(1)
	DECLARE @TempMIn2Manual CHAR(1)
	DECLARE @TempMOut2Manual CHAR(1)
	DECLARE @TempMOut1Manual CHAR(1)
	DECLARE @TempMReasonOutWork CHAR(1)
	DECLARE @TempMRsPunchCount INT
	DECLARE @TempMCtr INT

	DECLARE @TempMRsShiftAttended CHAR(3)
	DECLARE @HolidayCount INT
	DECLARE @IsHoliday BIT

	-- RSTime Temp Variables
	DECLARE @TempRsTimePaycode VARCHAR(10)
	DECLARE @TempRsTimeDateOffice DATETIME

	-- RSPunch Temp Varaibles
	DECLARE @TempRsPunchRowNumber INT
	DECLARE @TempRsPunchRowCount INT
	DECLARE @TempRsPunchCurrentRowIndex INT
	DECLARE @TempRsPunchCurrentRowIndex2 INT
	DECLARE @TempRsPunchTimeOut DATETIME
	DECLARE @TempRsPunchReasonCode CHAR(3)

	DECLARE @HDate	DATETIME
	DECLARE @HOLIDAY VARCHAR(20)
	DECLARE @ADJUSTMENTHOLIDAY DATETIME
	DECLARE @OT_FACTOR FLOAT
	DECLARE @HD_COMPANYCODE VARCHAR(10)
	DECLARE @HD_DEPARTMENTCODE	VARCHAR(10)
	DECLARE @BRANCHCODE VARCHAR(10)
	DECLARE @Divisioncode	VARCHAR(10)	


	-- TempTable Loop Varialbes 
	DECLARE @TempRsEmpRowNumber INT
	DECLARE @TempRsEmpRecordCount INT

	DECLARE @TempRsTimeRowNumber INT
	DECLARE @TempRsTimeRecordCount INT


	-- RsEmp Variables
	DECLARE @TempRSEmpAuthShift CHAR(50)

	-- tbl SETUP
	DECLARE @TempTblSetupSOut DATETIME
	DECLARE @TempTblSetupSEnd DATETIME
	DECLARE @TempMinutes INT

	-- RsTime Variable
	DECLARE @TempRsTimeShift CHAR(3)
	DECLARE @AutoShift CHAR(3)

	DECLARE @TempMRsTimeLeaveAmount FLOAT
	DECLARE @TempMRsTimeLeaveType1 CHAR(1)
	DECLARE @TempMRsTimeLeaveType2 CHAR(1)
	DECLARE @TempMRsTimeLeaveCode CHAR(3)
	DECLARE @TempMRsTimeLeaveType CHAR(1)
	DECLARE @TempMRsTimeFirstHalfLeaveCode CHAR(3)
	DECLARE @TempMRsTimeSecondHalfLeaveCode CHAR(3)
	DECLARE @TempMRsEmpIsOS CHAR(1)
	DECLARE @TempMRsEmpIsOT CHAR(1)
	DECLARE @TempMRsEmpInOnly CHAR(1)
	DECLARE @TempMRsEmpIsPunchAll CHAR(1)
	DECLARE @TempMRsEmpPermisLateArrival INT
	DECLARE @TempMRsEmpTime INT
	DECLARE @TempMRsEmpPermisEarlyDepart INT
	DECLARE @TempMRsEmpIsHalfDay CHAR(1)
	DECLARE @TempMRsEmpHalf INT
	DECLARE @TempMRsEmpIsShort CHAR(1)
	DECLARE @TempMRsEmpShort INT
	DECLARE @TempMRsEmpIsTimeLossAllowed CHAR(1)
	DECLARE @TempMRsEmpOTRate FLOAT
	DECLARE @TempRsEmpIsRoundTheClock CHAR(1)
	DECLARE @MIS CHAR (1)
	DECLARE @ISOTONOFF CHAR(1)
	DECLARE @ResignedAfter INT
	DECLARE @ENABLEAUTORESIGN CHAR(1)

	-- Variables added FOR EMPLOYEE GROUP
	-- 08/08/2019

	DECLARE @S_END DATETIME
	DECLARE @S_OUT DATETIME
	DECLARE @AUTOSHIFT_LOW INT
	DECLARE @AUTOSHIFT_UP INT
	DECLARE @ISPRESENTONWOPRESENT CHAR(1)
	DECLARE @ISPRESENTONHLDPRESENT CHAR(1)
	DECLARE @NightShiftFourPunch CHAR(1)
	DECLARE @ISAUTOABSENT CHAR(1)
	DECLARE @ISAWA CHAR(1)
	DECLARE @ISWA CHAR(1)
	DECLARE @ISAW CHAR(1)
	DECLARE @ISPREWO CHAR(1)
	DECLARE @ISOTOUTMINUSSHIFTENDTIME CHAR(1)
	DECLARE @ISOTWRKGHRSMINUSSHIFTHRS CHAR(1)
	DECLARE @ISOTEARLYCOMEPLUSLATEDEP CHAR(1)
	DECLARE @DEDUCTHOLIDAYOT INT
	DECLARE @DEDUCTWOOT INT
	DECLARE @ISOTEARLYCOMING INT
	DECLARE @OTMinus INT
	DECLARE @OTROUND INT
	DECLARE @OTEARLYDUR INT
	DECLARE @OTLATECOMINGDUR INT
	DECLARE @OTRESTRICTENDDUR INT
	DECLARE @DUPLICATECHECKMIN INT
    DECLARE @PREWO INT
	 
	-- Variables added during teting with Ajitesh
	-- 02/01/2018
	DECLARE @TempOutDate2DayHour DATETIME
	

	DECLARE  @Holidays TABLE
	(
		HDate	DATETIME,
		HOLIDAY VARCHAR(20),
		ADJUSTMENTHOLIDAY DATETIME,
		OT_FACTOR FLOAT,
		HD_COMPANYCODE VARCHAR(10),
		HD_DEPARTMENTCODE	VARCHAR(10),
		BRANCHCODE VARCHAR(10),
		Divisioncode	VARCHAR(10)	
	)


	DECLARE @TempRsEmp TABLE 
	(
	    RowNumber INT,
		PAYCODE  CHAR (10),
		CARDNO  CHAR (8),
		SHIFT  CHAR (3) ,
		SHIFTTYPE  CHAR (1) ,
		SHIFTPATTERN  CHAR (11) ,
		SHIFTREMAINDAYS  INT  ,
		LASTSHIFTPERFORMED  CHAR (3) ,
		INONLY  CHAR (1) ,
		ISPUNCHALL  CHAR (1) ,
		ISTIMELOSSALLOWED  CHAR (1) ,
		ALTERNATE_OFF_DAYS  CHAR (10) ,
		CDAYS  FLOAT  ,
		ISROUNDTHECLOCKWORK  CHAR (1) ,
		ISOT  CHAR (1) ,
		OTRATE  CHAR (6) ,
		FIRSTOFFDAY  CHAR (3) ,
		SECONDOFFTYPE  CHAR (1) ,
		HALFDAYSHIFT  CHAR (3) ,
		SECONDOFFDAY  CHAR (3) ,
		PERMISLATEARRIVAL  INT  ,
		PERMISEARLYDEPRT  INT  ,
		ISAUTOSHIFT  CHAR (1) ,
		ISOUTWORK  CHAR (1) ,
		MAXDAYMIN  FLOAT  ,
		ISOS  CHAR (1) ,
		AUTH_SHIFTS  CHAR (50) ,
		TIME  INT ,
		SHORT  INT,
		HALF  INT,
		ISHALFDAY  CHAR (1) ,
		ISSHORT  CHAR (1) ,
		TWO  CHAR (1) ,
		isReleaver  CHAR (1) ,
		isWorker  CHAR (1) ,
		isFlexi  CHAR (1) ,
		SSN  VARCHAR (100),
		MIS CHAR(1),
		IsCOF CHAR(1),
		HLFAfter INT,
		HLFBefore INT,
	    ResignedAfter INT,
		EnableAutoResign CHAR (1),
		S_END DATETIME,
		S_OUT DATETIME,
		AUTOSHIFT_LOW INT,
		AUTOSHIFT_UP INT,
		ISPRESENTONWOPRESENT CHAR(1),
		ISPRESENTONHLDPRESENT CHAR(1),
		NightShiftFourPunch CHAR(1),
		ISAUTOABSENT CHAR(1),
		ISAWA CHAR(1),
		ISWA CHAR(1),
		ISAW CHAR(1),
		ISPREWO CHAR(1),
		ISOTOUTMINUSSHIFTENDTIME CHAR(1),
		ISOTWRKGHRSMINUSSHIFTHRS CHAR(1),
		ISOTEARLYCOMEPLUSLATEDEP CHAR(1),
		DEDUCTHOLIDAYOT INT,
		DEDUCTWOOT INT,
		ISOTEARLYCOMING CHAR(1),
		OTMinus CHAR(1),
		OTROUND CHAR(1),
		OTEARLYDUR INT,
		OTLATECOMINGDUR INT,
		OTRESTRICTENDDUR INT,
		DUPLICATECHECKMIN INT,
		PREWO INT,
		CompanyCode VARCHAR(10),
		DepartmentCode VARCHAR(10)
	)

	DECLARE @TempRsTime TABLE 
	(	
	    RowNumber INT,
		PAYCODE CHAR(10) ,
		DateOFFICE DATETIME ,
		SHIFTSTARTTIME DATETIME ,
		SHIFTENDTIME DATETIME ,
		LUNCHSTARTTIME DATETIME ,
		LUNCHENDTIME DATETIME ,
		HOURSWORKED INT ,
		EXCLUNCHHOURS INT ,
		OTDURATION INT ,
		OSDURATION INT ,
		OTAMOUNT FLOAT ,
		EARLYARRIVAL INT ,
		EARLYDEPARTURE INT ,
		LATEARRIVAL INT ,
		LUNCHEARLYDEPARTURE INT ,
		LUNCHLATEARRIVAL INT ,
		TOTALLOSSHRS INT ,
		STATUS CHAR(6) ,
		LEAVETYPE1 CHAR(1) ,
		LEAVETYPE2 CHAR(1) ,
		FIRSTHALFLEAVECODE CHAR(3) ,
		SECONDHALFLEAVECODE CHAR(3) ,
		REASON VARCHAR(2000) ,
		SHIFT CHAR(3) ,
		SHIFTATTENDED CHAR(3) ,
		IN1 DATETIME ,
		IN2 DATETIME ,
		OUT1 DATETIME ,
		OUT2 DATETIME ,
		IN1MANNUAL CHAR(1) ,
		IN2MANNUAL CHAR(1) ,
		OUT1MANNUAL CHAR(1) ,
		OUT2MANNUAL CHAR(1) ,
		LEAVEVALUE FLOAT ,
		PRESENTVALUE FLOAT ,
		ABSENTVALUE FLOAT ,
		HOLIDAY_VALUE FLOAT ,
		WO_VALUE FLOAT ,
		OUTWORKDURATION INT ,
		LEAVETYPE CHAR(1) ,
		LEAVECODE CHAR(3) ,
		LEAVEAMOUNT FLOAT ,
		LEAVEAMOUNT1 FLOAT ,
		LEAVEAMOUNT2 FLOAT ,
		FLAG CHAR(4) ,
		LEAVEAPRDate DATETIME ,
		VOUCHER_NO CHAR(10) ,
		ReasonCode CHAR(3) ,
		ApprovedOT FLOAT ,
		WBR_Flag CHAR(1) ,
		Stage1OTApprovedBy CHAR(10) ,
		Stage2OTApprovedBy CHAR(10) ,
		Stage1ApprovalDate DATETIME ,
		Stage2ApprovalDate DATETIME ,
		AllowOT INT ,
		AbsentSMS CHAR(1) ,
		LateSMS CHAR(1) ,
		InSMS CHAR(1) ,
		OutSMS CHAR(1) ,
		SSN VARCHAR(100)	,
		COFUsed CHAR(1) 	
	)

	DECLARE @TempRsPunch TABLE
	(
		CARDNO char(10) ,
		OFFICEPUNCH datetime ,
		P_DAY char(1) ,
		ISMANUAL char(1) ,
		ReasonCode char(3) ,
		MC_NO char(3) ,
		INOUT char(1) ,
		PAYCODE char(10) ,
		Lcode char(4) ,
		SSN varchar(100) ,
		CompanyCode varchar(4),
		Remarks varchar(200),
		RowNumber INT
	)

	DECLARE @TempRsOutWork TABLE
	(
		PAYCODE char(10) ,
		DateOFFICE datetime ,
		IN1 datetime ,
		RIN1 char(3) ,
		OUT1 datetime ,
		ROUT1 char(3) ,
		IN2 datetime ,
		RIN2 char(3) ,
		OUT2 datetime ,
		ROUT2 char(3) ,
		IN3 datetime ,
		RIN3 char(3) ,
		OUT3 datetime ,
		ROUT3 char(3) ,
		IN4 datetime ,
		RIN4 char(3) ,
		OUT4 datetime ,
		ROUT4 char(3) ,
		IN5 datetime ,
		RIN5 char(3) ,
		OUT5 datetime ,
		ROUT5 char(3) ,
		IN6 datetime ,
		RIN6 char(3) ,
		OUT6 datetime ,
		ROUT6 char(3) ,
		IN7 datetime ,
		RIN7 char(3) ,
		OUT7 datetime ,
		ROUT7 char(3) ,
		IN8 datetime ,
		RIN8 char(3) ,
		OUT8 datetime ,
		ROUT8 char(3) ,
		IN9 datetime ,
		RIN9 char(3) ,
		OUT9 datetime ,
		ROUT9 char(3) ,
		IN10 datetime ,
		RIN10 char(3) ,
		OUT10 datetime ,
		ROUT10 char(3) ,
		OUTWORKDURATION int ,
		Reason_OutWork char(1) ,
		SSN varchar(100) 
	)

	SET @TempGNightShiftFourPunch =''
	SELECT @TempGNightShiftFourPunch = NightShiftFourPunch 
	FROM TBLEMPLOYEESHIFTMASTER (nolock)
	WHERE PAYCODE = @PayCode
	SET @TempGNightShiftFourPunch='N'
	IF @PayCode != ''
	BEGIN
	    INSERT INTO @TempRsEmp
		Select  ROW_NUMBER() OVER (ORDER BY Emp.PayCode) RowNumber, 
		    EmpShift.*,
			Emp.companycode,
			Emp.departmentcode 
			from tblemployeeshiftmaster(nolock) EmpShift
			INNER JOIN tblemployee(nolock)  Emp
			ON EmpShift.SSN = Emp.SSN 
			WHERE Emp.COMPANYCODE =@CompanyCode
			AND EmpShift.PAYCODE=@PayCode
			AND Emp.ACTIVE='Y'
	END
	ELSE
	BEGIN
		INSERT INTO @TempRsEmp
		Select 
		    ROW_NUMBER() OVER (ORDER BY Emp.PayCode) RowNumber, 
		    EmpShift.*,
			Emp.companycode,
			Emp.departmentcode 
			from tblemployeeshiftmaster(nolock) EmpShift
			INNER JOIN tblemployee(nolock)  Emp
			ON EmpShift.SSN = Emp.SSN 
			WHERE Emp.COMPANYCODE =@CompanyCode
			AND Emp.ACTIVE='Y'
	END


	SET @TempRsEmpRecordCount = 0
	SELECT @TempRsEmpRecordCount = COUNT(*) FROM @TempRsEmp 

	SET @TempRsEmpRowNumber = 1

	--print 'Processing Number of employee' & @TempRsEmpRowNumber
	IF @TempRsEmpRecordCount > 0 
	BEGIN
		While @TempRsEmpRowNumber <= @TempRsEmpRecordCount
		BEGIN
		        SET @TempSSN = ''
				SET @TempCompanyCode = ''
				SET @TempDepartmentCode = ''
				SET @TempDayHour =  0
				SET @TempPayCode = ''
				SET @TempIsOutWork = ''
				SET @TempIsPunchAll = ''
				SET @TempRsEmpTwo = ''
				SET @TempRsEmpInOnly = ''
				SET @TempRSEmpAuthShift = '' 
				SET @TempMRsEmpIsOS = ''
				SET @TempMRsEmpIsOT = ''
				SET @TempMRsEmpInOnly = ''
				SET @TempMRsEmpIsPunchAll = ''
				SET @TempMRsEmpPermisLateArrival = 0
				SET @TempMRsEmpTime = 0
				SET @TempMRsEmpPermisEarlyDepart = 0
				SET @TempMRsEmpIsHalfDay = ''
				SET @TempMRsEmpHalf = 0
				SET @TempMRsEmpIsShort = ''
				SET @TempMRsEmpShort = 0
				SET @TempMRsEmpIsTimeLossAllowed = ''
				SET @TempMRsEmpOTRate = 0.0
				SET @TempRsEmpIsRoundTheClock = ''
				SET @MIS=''
				SET @ISOTONOFF=''
				SET @ENABLEAUTORESIGN='N'
						
			SELECT 
				@TempSSN = SSN,
				@TempCompanyCode = CompanyCode,
				@TempDepartmentCode = DepartmentCode,
				@TempDayHour =  ISNULL(MaxDayMin,0),
				@TempPayCode = PAYCODE,
				@TempIsOutWork = IsOutWork,
				@TempIsPunchAll = ISPUNCHALL,
				@TempRsEmpTwo = TWO,
				@TempRsEmpInOnly = INONLY,
				@TempRSEmpAuthShift = AUTH_SHIFTS ,
				@TempMRsEmpIsOS = ISOS,
				@TempMRsEmpIsOT = ISOT,
				@TempMRsEmpInOnly = INONLY,
				@TempMRsEmpIsPunchAll = ISPUNCHALL,
				@TempMRsEmpPermisLateArrival = PERMISLATEARRIVAL,
				@TempMRsEmpTime = [Time],
				@TempMRsEmpPermisEarlyDepart = PERMISEARLYDEPRT,
				@TempMRsEmpIsHalfDay = ISHALFDAY,
				@TempMRsEmpHalf = HALF,
				@TempMRsEmpIsShort = ISSHORT,
				@TempMRsEmpShort = SHORT,
				@TempMRsEmpIsTimeLossAllowed = ISTIMELOSSALLOWED,
				@TempMRsEmpOTRate = OTRATE,
				@TempRsEmpIsRoundTheClock = ISROUNDTHECLOCKWORK	,
				@MIS=MIS,
				@ISOTONOFF=IsCOF,
				@S_END=S_END,
				@S_OUT=S_OUT,
				@AUTOSHIFT_LOW=AUTOSHIFT_LOW,
				@AUTOSHIFT_UP=AUTOSHIFT_UP,
				@ISPRESENTONWOPRESENT=ISPRESENTONWOPRESENT,
				@ISPRESENTONHLDPRESENT=ISPRESENTONHLDPRESENT,
				@NightShiftFourPunch=NightShiftFourPunch,
				@ISOTOUTMINUSSHIFTENDTIME=ISOTOUTMINUSSHIFTENDTIME,
				@ISOTWRKGHRSMINUSSHIFTHRS=ISOTWRKGHRSMINUSSHIFTHRS,
				@ISOTEARLYCOMEPLUSLATEDEP=ISOTEARLYCOMEPLUSLATEDEP
	     		FROM @TempRsEmp WHERE RowNumber = @TempRsEmpRowNumber


			DELETE FROM @Holidays
			-- Load Holidays
			-- TODO : Optimize . Load Holiday table once and filter based 
			-- on Department and company code
			INSERT INTO @Holidays 
				SELECT * FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @TempDepartmentCode 
			AND CompanyCode = @TempCompanyCode
			
			-- TODO : Remove it after verification as we are already updating in ProcessBackData
			UPDATE MACHINERAWPUNCH 
			SET P_DAY='N' 
			WHERE PAYCODE=@TempPayCode 
			AND OFFICEPUNCH>=CAST(CONVERT(varchar(10), (DATEADD(DAY,1,@FromDate)), 101) AS DATE)

			
			DELETE FROM tblOutWorkRegister 
			WHERE PAYCODE=@PayCode
			AND DateOFFICE BETWEEN @FromDate AND  @ToDate
			--Ali
			DECLARE @RowCount1 INTEGER
			--End Ali
			UPDATE tbltimeregister 
			SET in1=null,
				in2=null,
				out1=null,
				out2=null 
			WHERE PAYCODE=@PayCode
			AND DateOFFICE BETWEEN @FromDate AND  @ToDate

			--Ali
			SELECT @RowCount1 = @@ROWCOUNT
			--End Ali

			DELETE FROM @TempRsTime

			INSERT INTO @TempRsTime
			SELECT  ROW_NUMBER() OVER (ORDER BY SSN) RowNumber,  * FROM tbltimeregister(nolock)
			WHERE PAYCODE=@PayCode
			AND DateOFFICE BETWEEN @FromDate AND  @ToDate
			ORDER BY DateOFFICE

			SET @TempRsTimeRecordCount = 0
			SELECT @TempRsTimeRecordCount = COUNT(*) 
			FROM @TempRsTime

			SET @TempMReasonCode = ''
			SET @TempFirstPunchReasonCode = ''
			SET @TempLastPunchReasonCode = ''

			SET @TempRsTimeRowNumber = 1
			While @TempRsTimeRowNumber <= @TempRsTimeRecordCount
			BEGIN

				SET @TempMIn1 = NULL
				SET @TempMIn2 = NULL
				SET @TempMOut1 = NULL
				SET @TempMOut2 = NULL
				SET @TempMShiftStartTime = NULL
				SET @TempMShiftEndTime = NULL
				SET @TempMLunchStartTime = NULL
				SET @TempMLunchEndTime = NULL

				SET @TempMIn1Manual = 'N'
				SET @TempMIn2Manual = 'N'
				SET @TempMOut1Manual = 'N'
				SET @TempMOut2Manual = 'N'


				SET @TempProcDate=''
				SET @TempMDate1=''
				SET @TempRsTimeDateOffice = ''
				SET @TempRsTimePayCode = ''
				SET @TempRsShiftAttended = ''
				SET @TempRsTimeShift = ''
			
				SELECT 
					@TempProcDate=DateOFFICE,
					@TempMDate1=DateOFFICE,
					@TempRsTimeDateOffice = DateOFFICE,
					@TempRsTimePayCode = PayCode,
					@TempRsShiftAttended = SHIFTATTENDED,
					@TempRsTimeShift = [SHIFT]
				FROM @TempRsTime
				WHERE RowNumber = @TempRsTimeRowNumber

				SET @TempProcDate = CAST(CONVERT(varchar(10), @TempProcDate, 101) AS DATE)
				-- TODO :
				-- If it is NonRTC , Don't add the 1 day
				-- Get the value of NonRTC from EmployeeShift Master
				IF @TempRsEmpIsRoundTheClock = 'Y'
				BEGIN
					SET @TempMDate2 = DATEADD(DAY,1, @TempMDate1)

					SET @TempTblSetupSOut = ''
					SET @TempTblSetupSEnd = ''

					SELECT 
						@TempTblSetupSOut = S_OUT,
						@TempTblSetupSEnd = S_END
					FROM tblsetup (nolock)	  
					WHERE SETUPID = @SetupId
					
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
					SET @TempMDate2 = DATEADD(HH, @TempMinutes/60, @TempMDate2)
					SET @TempMDate2 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate2)
				END
				ELSE
				BEGIN	
				   	SELECT 
						@TempTblSetupSOut = S_OUT,
						@TempTblSetupSEnd = S_END
					FROM tblsetup	  (nolock)
					WHERE SETUPID = @SetupId
									    
					SET @TempMDate2 = DATEADD(DAY,1, @TempMDate1)
					SET @TempMDate2 = CAST(CAST(@TempMDate2 AS VARCHAR(12)) AS DATETIME)
					SET @TempMDate2 = DATEADD(MINUTE,-1,@TempMDate2)
				END

				DELETE FROM @TempRsPunch
				-- 
				IF @TempIsOutWork = 'Y' 
				BEGIN

				    SET @TempOutDate2DayHour = NULL
					SET @TempOutDate2DayHour = DATEADD(DAY,1,@TempRsTimeDateOffice)						
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
					SET @TempOutDate2DayHour = DATEADD(HH, @TempMinutes/60, @TempOutDate2DayHour)
					SET @TempOutDate2DayHour = DATEADD(MINUTE, @TempMinutes%60,@TempOutDate2DayHour)

				  -- TODO : @TempDate2 take the g_o_end into consideration
					INSERT INTO @TempRsPunch
					SELECT *, ROW_NUMBER() OVER (ORDER BY OfficePunch) RowNumber FROM MachineRawPunch (nolock)
					WHERE (OFFICEPUNCH BETWEEN CONVERT(DATETIME, CONVERT(varchar(11),@TempMDate1, 111 ) + ' 00:00:00', 111)
									   AND @TempOutDate2DayHour)
					AND p_day<>'Y' 
					AND PAYCODE = @PayCode
					Order By OfficePunch
				END
				ELSE
				BEGIN
				  
					INSERT INTO @TempRsPunch
					SELECT *, ROW_NUMBER() OVER (ORDER BY OfficePunch) RowNumber FROM MachineRawPunch (nolock)
					WHERE (OFFICEPUNCH BETWEEN CONVERT(DATETIME, CONVERT(varchar(11),@TempMDate1, 111 ) + ' 00:00:00', 111)
									   AND CONVERT(DATETIME, CONVERT(varchar(11),@TempMDate2, 111 ) + ' 23:59:59', 111) )
					AND p_day<>'Y' 
					AND PAYCODE = @PayCode
					Order By OfficePunch
				END
				
				IF @TempIsPunchAll = 'N'
				BEGIN
					GOTO AsgIt
				END
				SET @TempMRsPunchCount = 0
				SELECT @TempMRsPunchCount = COUNT(*) FROM @TempRsPunch

				IF @TempMRsPunchCount > 0 AND @TempIsOutWork = 'Y'
				BEGIN
					SET @TempMIn1 = ''
					SET @TempMDate2 = ''

					SET @TempMIn1 = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch)
					SET @TempMDate2 = DATEADD(MINUTE, @TempDayHour, @TempMIn1)
									
					SET @TempOutDate2DayHour = NULL
					SET @TempOutDate2DayHour = DATEADD(DAY,1,@TempRsTimeDateOffice)						
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
					SET @TempOutDate2DayHour = DATEADD(HH, @TempMinutes/60, @TempOutDate2DayHour)
					SET @TempOutDate2DayHour = DATEADD(MINUTE, @TempMinutes%60,@TempOutDate2DayHour)


					IF @TempMDate2 > @TempOutDate2DayHour
					BEGIN
					    SET @TempMinutes = 0
						SET @TempMDate2 = DATEADD(DAY,1,@TempRsTimeDateOffice)
						
						SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSOut), 0), @TempTblSetupSOut)
						SET @TempMDate2 = DATEADD(HH, @TempMinutes/60, @TempMDate2)
						SET @TempMDate2 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate2)
					END

					-- DELETE Old records from RsMachinPunch
					DELETE FROM @TempRsPunch

					INSERT INTO @TempRsPunch
					SELECT *, ROW_NUMBER() OVER (ORDER BY OfficePunch) RowNumber FROM MachineRawPunch (nolock)
					WHERE (OFFICEPUNCH BETWEEN @TempMIn1 AND @TempMDate2)
					AND p_day<>'Y' 
					AND PAYCODE = @PayCode
					Order By OFFICEPUNCH
				END

				SET @TempMRsPunchCount = 0
				SELECT @TempMRsPunchCount = COUNT(*) FROM @TempRsPunch

				SET @TempMIn1Manual = ''
				SET @TempReasonCode = ''
				SET @TempRsPunchOFficePunch = ''

				SET @TempMIn1Manual = (SELECT TOP 1 ISMANUAL FROM @TempRsPunch)
				SET @TempReasonCode = (SELECT TOP 1 ISNULL(ReasonCode,'') FROM @TempRsPunch)
				SET @TempRsPunchOFficePunch = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch)
				--SET @TempMOut2Manual = (SELECT TOP 1 ISMANUAL FROM @TempRsPunch Order By OFFICEPUNCH DESC)

				IF @TempMRsPunchCount > 2  AND @TempIsOutWork = 'Y'
				BEGIN
				    
					

					SET @TempMIn1 = @TempRsPunchOFficePunch
					SET @TempMDate1 = DATEADD(DAY,1,@TempRsTimeDateOffice)

					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSEnd), 0), @TempTblSetupSEnd)
					SET @TempMDate1 = DATEADD(HH, @TempMinutes/60, @TempMDate1)
					SET @TempMDate1 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate1)

					IF DATEDIFF(MINUTE,@TempRsPunchOFficePunch,@TempMDate1) < 0
					BEGIN
						SET @TempMIn1 = NULL
						GOTO AsgIt
					END

					IF LEN(@TempReasonCode) <> 0
					BEGIN
						SET @TempMReasonCode = @TempReasonCode
						SET @TempMFirstPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
						SET @TempMFirstPunchReasonCode = ''
					END

					IF DATEDIFF(DAY, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
					   UPDATE MachineRawPunch
					   SET P_Day='Y'
					   WHERE PAYCODE=@PayCode 
					   AND OFFICEPUNCH = @TempMIn1
					END

					SET @TempMOut2 = ''
					SET @TempMOut2Manual = ''
					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''

					-- Move to last record. Order by Desc
					SET @TempMOut2 = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch Order By OFFICEPUNCH DESC)
					SET @TempMOut2Manual = (SELECT TOP 1 ISMANUAL FROM @TempRsPunch Order By OFFICEPUNCH DESC)
					SET @TempReasonCode = (SELECT TOP 1 ISNULL(ReasonCode,'') FROM @TempRsPunch Order By OFFICEPUNCH DESC)
					SET @TempRsPunchOFficePunch = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch Order By OFFICEPUNCH DESC)

					IF LEN(@TempReasonCode) <> 0
					BEGIN	
						SET @TempMReasonCode = @TempReasonCode
						SET @TempLastPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
						SET @TempLastPunchReasonCode = ''
					END


					IF DATEDIFF(DAY, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
					   UPDATE MachineRawPunch
					   SET P_Day='Y'
					   WHERE SSN=@TempSSN 
					   AND OFFICEPUNCH = @TempMOut2
					END

					SET @TempRsTOut = ''
					SET @TempRsTOut = (SELECT TOP 1 OFFICEPUNCH FROM @TempRsPunch Order By OFFICEPUNCH DESC)

					INSERT INTO @TempRsOutWork
					SELECT * FROM tblOutWorkRegister (nolock)
					WHERE PAYCODE = @PayCode
					AND DateOFFICE = @TempMDate1

					SET @TempRsOutRecordCount = 0

					SELECT @TempRsOutRecordCount =COUNT(*) FROM @TempRsOutWork

					IF @TempRsOutRecordCount < 1
					BEGIN
						SET @TempRsOutPayCode = @TempRsTimePaycode
						INSERT INTO tblOutWorkRegister
							(
								PAYCODE,
								SSN,
								DateOFFICE
							)
						VALUES
							(
								@TempRsOutPayCode,
								@TempSSN,
								@TempRsTimeDateOffice
							)
					END

					SET @TempRsPunchRowCount = 0
					SET @TempRsPunchCurrentRowIndex = 2
					SET @TempRsOutOutWorkDuration = 0
					SET @TempMReasonOutWork = 'N'
					SET @TempMCtr = 1

					SELECT @TempRsPunchRowCount = Count(*) FROM @TempRsPunch
					
					SET @TempRsOutIN1=NULL
					SET @TempRsOutIN2=NULL
					SET @TempRsOutIN3=NULL
					SET @TempRsOutIN4=NULL
					SET @TempRsOutIN5=NULL
					SET @TempRsOutIN6=NULL
					SET @TempRsOutIN7=NULL
					SET @TempRsOutIN8=NULL
					SET @TempRsOutIN9=NULL
					SET @TempRsOutIN10=NULL

					SET @TempRsOutRIN1=NULL
					SET @TempRsOutRIN2=NULL
					SET @TempRsOutRIN3=NULL
					SET @TempRsOutRIN4=NULL
					SET @TempRsOutRIN5=NULL
					SET @TempRsOutRIN6=NULL
					SET @TempRsOutRIN7=NULL
					SET @TempRsOutRIN8=NULL
					SET @TempRsOutRIN9=NULL
					SET @TempRsOutRIN10=NULL

					SET @TempRsOutOUT1=NULL
					SET @TempRsOutOUT2=NULL
					SET @TempRsOutOUT3=NULL
					SET @TempRsOutOUT4=NULL
					SET @TempRsOutOUT5=NULL
					SET @TempRsOutOUT6=NULL
					SET @TempRsOutOUT7=NULL
					SET @TempRsOutOUT8=NULL
					SET @TempRsOutOUT9=NULL
					SET @TempRsOutOUT10=NULL

					SET @TempRsOutROUT1=NULL
					SET @TempRsOutROUT2=NULL
					SET @TempRsOutROUT3=NULL
					SET @TempRsOutROUT4=NULL
					SET @TempRsOutROUT5=NULL
					SET @TempRsOutROUT6=NULL
					SET @TempRsOutROUT7=NULL
					SET @TempRsOutROUT8=NULL
					SET @TempRsOutROUT9=NULL
					SET @TempRsOutROUT10=NULL

					SET @TempMReasonOutWork=''
					SET @TempRsOutOutWorkDuration=0
					-- STEP  : Find OUTTime from Mutliple Rows
					WHILE @TempRsPunchCurrentRowIndex <=  @TempRsPunchRowCount
					BEGIN
					    
						SET @TempRsPunchTimeOut = ''
						SET @TempRsPunchReasonCode = ''

						SELECT @TempRsPunchTimeOut = OFFICEPUNCH,
								@TempRsPunchReasonCode = RTRIM(LTRIM(ISNULL(ReasonCode, '')))
					    FROM @TempRsPunch Where RowNumber = @TempRsPunchCurrentRowIndex

						IF @TempRsTOut = @TempRsPunchTimeOut
						BEGIN
							BREAK
						END

						IF @TempMCtr = 1
						BEGIN 
							SET @TempRsOutIN1  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 2
						BEGIN 
							SET @TempRsOutIN2  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 3
						BEGIN 
							SET @TempRsOutIN3  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 4
						BEGIN 
							SET @TempRsOutIN4  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 5
						BEGIN 
							SET @TempRsOutIN5  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 6
						BEGIN 
							SET @TempRsOutIN6  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 7
						BEGIN 
							SET @TempRsOutIN7  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 8
						BEGIN 
							SET @TempRsOutIN8  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 9
						BEGIN 
							SET @TempRsOutIN9  =  @TempRsPunchTimeOut	
						END

						IF @TempMCtr = 10
						BEGIN 
							SET @TempRsOutIN10  =  @TempRsPunchTimeOut	
						END

						IF @TempRsPunchReasonCode <> ''
						BEGIN
							IF @TempMCtr = 1
							BEGIN 
								SET @TempRsOutRIN1 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 2
							BEGIN 
								SET @TempRsOutRIN2 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 3
							BEGIN 
								SET @TempRsOutRIN3 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 4
							BEGIN 
								SET @TempRsOutRIN4 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 5
							BEGIN 
								SET @TempRsOutRIN5 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 6
							BEGIN 
								SET @TempRsOutRIN6 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 7
							BEGIN 
								SET @TempRsOutRIN7 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 8
							BEGIN 
								SET @TempRsOutRIN8 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 9
							BEGIN 
								SET @TempRsOutRIN9 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 10
							BEGIN 
								SET @TempRsOutRIN10 = @TempRsPunchReasonCode
							END
							SET @TempMReasonCode = @TempRsPunchReasonCode

							IF(LEN(@TempRsPunchReasonCode) > 0)
							BEGIN
								SET @TempMReasonOutWork = 'Y'
							END
							ELSE
							BEGIN
								SET  @TempMReasonOutWork = 'N'
							END
						END	

						IF DATEDIFF(D,@TempRsTimeDateOffice, @TempRsPunchTimeOut) > 0
						BEGIN
							UPDATE MachineRawPunch SET P_DAY = 'Y'
							WHERE SSN = @TempSSN 
							AND OFFICEPUNCH = @TempRsPunchTimeOut							
						END

						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1

						IF @TempRsPunchCurrentRowIndex > @TempRsPunchRowCount
						BEGIN
							BREAK
						END

						IF @TempRsPunchCurrentRowIndex = @TempRsPunchRowCount
						BEGIN
							BREAK
						END

						SET @TempRsPunchTimeOut = ''
						SET @TempRsPunchReasonCode = ''

						SELECT @TempRsPunchTimeOut = OFFICEPUNCH,
							   @TempRsPunchReasonCode = RTRIM(LTRIM(ISNULL(ReasonCode, '')))
						FROM @TempRsPunch Where RowNumber = @TempRsPunchCurrentRowIndex
						
						IF @TempMCtr = 1
						BEGIN 
							SET @TempRsOutOUT1  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN1 , @TempRsOutOUT1)
						END

						IF @TempMCtr = 2
						BEGIN 
							SET @TempRsOutOUT2  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN2 , @TempRsOutOUT2)
						END

						IF @TempMCtr = 3
						BEGIN 
							SET @TempRsOutOUT3  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN3 , @TempRsOutOUT3)
						END

						IF @TempMCtr = 4
						BEGIN 
							SET @TempRsOutOUT4  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN4 , @TempRsOutOUT4)
						END

						IF @TempMCtr = 5
						BEGIN 
							SET @TempRsOutOUT5  =  @TempRsPunchTimeOut
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN5 , @TempRsOutOUT5)	
						END

						IF @TempMCtr = 6
						BEGIN 
							SET @TempRsOutOUT6  =  @TempRsPunchTimeOut
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN6, @TempRsOutOUT6)	
						END

						IF @TempMCtr = 7
						BEGIN 
							SET @TempRsOutOUT7  =  @TempRsPunchTimeOut
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN7 , @TempRsOutOUT7)	
						END

						IF @TempMCtr = 8
						BEGIN 
							SET @TempRsOutOUT8  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN8 , @TempRsOutOUT8)
						END

						IF @TempMCtr = 9
						BEGIN 
							SET @TempRsOutOUT9  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN9, @TempRsOutOUT9)
						END

						IF @TempMCtr = 10
						BEGIN 
							SET @TempRsOutOUT10  =  @TempRsPunchTimeOut	
							SET @TempRsOutOutWorkDuration =  @TempRsOutOutWorkDuration + DATEDIFF(N, @TempRsOutIN10, @TempRsOutOUT10)
						END


						IF @TempRsPunchReasonCode <> ''
						BEGIN
							IF @TempMCtr = 1
							BEGIN 
								SET @TempRsOutROUT1 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 2
							BEGIN 
								SET @TempRsOutROUT2 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 3
							BEGIN 
								SET @TempRsOutROUT3 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 4
							BEGIN 
								SET @TempRsOutROUT4 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 5
							BEGIN 
								SET @TempRsOutROUT5 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 6
							BEGIN 
								SET @TempRsOutROUT6 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 7
							BEGIN 
								SET @TempRsOutROUT7 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 8
							BEGIN 
								SET @TempRsOutROUT8 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 9
							BEGIN 
								SET @TempRsOutROUT9 = @TempRsPunchReasonCode
							END

							IF @TempMCtr = 10
							BEGIN 
								SET @TempRsOutROUT10 = @TempRsPunchReasonCode
							END
							SET @TempMReasonCode = @TempRsPunchReasonCode

							IF(LEN(@TempRsPunchReasonCode) > 0)
							BEGIN
								SET @TempMReasonOutWork = 'Y'
							END
							ELSE
							BEGIN
								SET  @TempMReasonOutWork = 'N'
							END
						END	

						
						IF DATEDIFF(D,@TempRsTimeDateOffice, @TempRsPunchTimeOut) > 0
						BEGIN
							UPDATE MachineRawPunch SET P_DAY = 'Y'
							WHERE PAYCODE = @PayCode 
							AND OFFICEPUNCH = @TempRsPunchTimeOut							
						END

						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
						SET @TempMCtr = @TempMCtr + 1

					END

					-- TODO
					-- INSERT this record first. Instead of UPDATE
					UPDATE tblOutWorkRegister
					SET 
						IN1 = @TempRsOutIN1,
						IN2 = @TempRsOutIN2,
						IN3 = @TempRsOutIN3,
						IN4 = @TempRsOutIN4,
						IN5 = @TempRsOutIN5,
						IN6 = @TempRsOutIN6,
						IN7 = @TempRsOutIN7,
						IN8 = @TempRsOutIN8,
						IN9 = @TempRsOutIN9,
						IN10 = @TempRsOutIN10,
						RIN1 = @TempRsOutRIN1,
						RIN2 = @TempRsOutRIN2,
						RIN3 = @TempRsOutRIN3,
						RIN4 = @TempRsOutRIN4,
						RIN5 = @TempRsOutRIN5,
						RIN6 = @TempRsOutRIN6,
						RIN7 = @TempRsOutRIN7,
						RIN8 = @TempRsOutRIN8,
						RIN9 = @TempRsOutRIN9,
						RIN10 = @TempRsOutRIN10,
						OUT1 = @TempRsOutOUT1,
						OUT2 = @TempRsOutOUT2,
						OUT3 = @TempRsOutOUT3,
						OUT4 = @TempRsOutOUT4,
						OUT5 = @TempRsOutOUT5,
						OUT6 = @TempRsOutOUT6,
						OUT7 = @TempRsOutOUT7,
						OUT8 = @TempRsOutOUT8,
						OUT9 = @TempRsOutOUT9,
						OUT10 = @TempRsOutOUT10,
						ROUT1 = @TempRsOutROUT1,
						ROUT2 = @TempRsOutROUT2,
						ROUT3 = @TempRsOutROUT3,
						ROUT4 = @TempRsOutROUT4,
						ROUT5 = @TempRsOutROUT5,
						ROUT6 = @TempRsOutROUT6,
						ROUT7 = @TempRsOutROUT7,
						ROUT8 = @TempRsOutROUT8,
						ROUT9 = @TempRsOutROUT9,
						ROUT10 = @TempRsOutROUT10,
						Reason_OutWork = @TempMReasonOutWork,
						OUTWORKDURATION = @TempRsOutOutWorkDuration,
						PAYCODE = @TempRsOutPayCode,
						DateOFFICE =  @TempRsTimeDateOffice
					WHERE PAYCODE = @PayCode
					AND DateOFFICE = @TempRsTimeDateOffice
					
					GOTO AsgIt
				END

				IF @TempIsOutWork = 'Y' and   @TempMRsPunchCount > 0					
				BEGIN
					SET @TempRsPunchCurrentRowIndex = 1
					SET @TempRsPunchCurrentRowIndex2 = 1
					SET @IsPreviousDaySet = 0
										
					WHILE @TempRsPunchCurrentRowIndex <= @TempMRsPunchCount
					BEGIN
					    SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''

						SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = P_DAY
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex2

						IF @TempRSPunchPDay = 'Y' AND DATEDIFF(D, @TempRsPunchOFficePunch, @TempMDate1) = 0
						BEGIN
							
							IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
							BEGIN
							    SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
								SET @IsPreviousDaySet = 1
								BREAK
							END
						END
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
						SET @TempRsPunchCurrentRowIndex2 = @TempRsPunchCurrentRowIndex2 + 1
					END

					--IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					--BEGIN
					--	GOTO AsgIt
					--END	
					

					IF @IsPreviousDaySet = 1
					BEGIN 
						GOTO AsgIt
					END
					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''

					SET @TempRsPunchCurrentRowIndex = 1
					SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					SET @TempMDate1 = DATEADD(D, 1, @TempRsTimeDateOffice)
					
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSEnd), 0), @TempTblSetupSEnd)
					SET @TempMDate1 = DATEADD(HH, @TempMinutes/60, @TempMDate1)
					SET @TempMDate1 = DATEADD(MINUTE, @TempMinutes%60,@TempMDate1)

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) < 0
					BEGIN
						SET @TempMIn1 = NULL
						GOTO AsgIt
					END

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
					BEGIN
						SET @TempMIn1 = @TempRsPunchOFficePunch

						IF LEN(RTRIM(LTRIM(@TempReasonCode))) > 0
						BEGIN
							SET @TempMReasonCode = @TempReasonCode
							SET @TempFirstPunchReasonCode = @TempReasonCode
						END
						ELSE
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempFirstPunchReasonCode = ''
						END

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
						BEGIN
							SET @TempMPFlg = 'Y'							
						END
						ELSE
						BEGIN
							SET @TempMPFlg = 'N'
						END

						IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END
						ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END

						IF @TempExecuteMachineRawUpdate = 1
						BEGIN
							UPDATE MachineRawPunch 
							Set P_Day=@TempMPFlg 
							Where PAYCODE=@PayCode
							AND OfficePunch = @TempMIn1
						END
					END
					ELSE
					BEGIN
						GOTO AsgIt
					END

					SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
					IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					BEGIN
						GOTO AsgIt
					END	

					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''

					SELECT 
						@TempReasonCode = ISNULL(ReasonCode,''),
						@TempRsPunchOFficePunch = OFFICEPUNCH,
						@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					SET @TempMOut1 = @TempRsPunchOFficePunch

					IF LEN(RTRIM(LTRIM(@TempReasonCode))) > 0
					BEGIN
						SET @TempMReasonCode = @TempReasonCode
						SET @TempFirstPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
					    IF LEN(LTRIM(RTRIM(@TempMReasonCode))) <= 0
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempFirstPunchReasonCode = ''
						END
					END

					IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
						SET @TempMPFlg = 'Y'							
					END
					ELSE
					BEGIN
						SET @TempMPFlg = 'N'
					END

					IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END
					ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END

					IF @TempExecuteMachineRawUpdate = 1
					BEGIN
						UPDATE MachineRawPunch 
						Set P_Day=@TempMPFlg 
						Where PAYCODE=@PayCode
						AND OfficePunch = @TempMOut1
					END
					GoTo AsgIt	
				 END				

				IF   @TempMRsPunchCount > 0
				BEGIN
					SET @TempRsPunchCurrentRowIndex = 1
					SET @TempRsPunchCurrentRowIndex2 = 1
					SET @IsPreviousDaySet = 0					
					WHILE @TempRsPunchCurrentRowIndex <= @TempMRsPunchCount
					BEGIN
					    
						SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''

						SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = P_DAY
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex2

						IF @TempRSPunchPDay = 'Y' AND DATEDIFF(D, @TempRsPunchOFficePunch, @TempMDate1) = 0
						BEGIN
							
							IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
							BEGIN
							    SET @IsPreviousDaySet = 1
							    SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
								BREAK
							END
						END
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
						SET @TempRsPunchCurrentRowIndex2 = @TempRsPunchCurrentRowIndex2 + 1
					END

					--IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					--BEGIN
					--	GOTO AsgIt
					--END
					
					IF @IsPreviousDaySet = 1
					BEGIN 
						GOTO AsgIt
					END
					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''	
					
					SET @TempRsPunchCurrentRowIndex = 1
					SELECT 
							@TempReasonCode = ISNULL(ReasonCode,''),
							@TempRsPunchOFficePunch = OFFICEPUNCH,
							@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					SET @TempMDate1 = DATEADD(D, 1, @TempRsTimeDateOffice)
					
					SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempTblSetupSEnd), 0), @TempTblSetupSEnd)
					SET @TempMDate1 = DATEADD(HH, @TempMinutes/60, @TempMDate1)
					SET @TempMDate1 = DATEADD(MINUTE, @TempMinutes%60, @TempMDate1)

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) < 0
					BEGIN
						SET @TempMIn1 = NULL
						GOTO AsgIt
					END

					IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
					BEGIN
						SET @TempMIn1 = @TempRsPunchOFficePunch

						IF LEN(LTRIM(RTRIM(@TempReasonCode))) > 0
						BEGIN
							SET @TempMReasonCode = @TempReasonCode
							SET @TempFirstPunchReasonCode = @TempReasonCode
						END
						ELSE
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempFirstPunchReasonCode = ''
						END

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
						BEGIN
							SET @TempMPFlg = 'Y'							
						END
						ELSE
						BEGIN
							SET @TempMPFlg = 'N'
						END

						IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END
						ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
						BEGIN
							SET @TempExecuteMachineRawUpdate = 1
						END

						IF @TempExecuteMachineRawUpdate = 1
						BEGIN
							UPDATE MachineRawPunch 
							Set P_Day=@TempMPFlg 
							Where PAYCODE=@PayCode
							AND OfficePunch = @TempMIn1
						END
					END
					ELSE
					BEGIN
						GOTO AsgIt
					END


					SET @TempMDate1 = DATEADD(N, @TempDayHour, @TempRsPunchOFficePunch)
					SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1
					IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
					BEGIN
						GOTO AsgIt
					END	

					SET @TempReasonCode = ''
					SET @TempRsPunchOFficePunch = ''
					SET @TempRSPunchPDay = ''

					SELECT 
						@TempReasonCode = ISNULL(ReasonCode,''),
						@TempRsPunchOFficePunch = OFFICEPUNCH,
						@TempRSPunchPDay = ISNULL(P_DAY,'')
					FROM @TempRsPunch 
					WHERE RowNumber = @TempRsPunchCurrentRowIndex

					IF DATEDIFF(N,@TempRsPunchOFficePunch,@TempMDate1) >=0 
					BEGIN
						SET @TempMOut1 = @TempRsPunchOFficePunch

						IF LEN(RTRIM(LTRIM(@TempReasonCode))) > 0
					BEGIN	
						SET @TempMReasonCode = @TempReasonCode
						SET @TempLastPunchReasonCode = @TempReasonCode
					END
					ELSE
					BEGIN
					    IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
						BEGIN
							SET @TempMReasonCode = ''
							SET @TempLastPunchReasonCode = ''
						END
					END

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
					BEGIN
						SET @TempMPFlg = 'Y'							
					END
					ELSE
					BEGIN
						SET @TempMPFlg = 'N'
					END

						IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END
					ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
					BEGIN
						SET @TempExecuteMachineRawUpdate = 1
					END

						IF @TempExecuteMachineRawUpdate = 1
					BEGIN
						UPDATE MachineRawPunch 
						Set P_Day=@TempMPFlg 
						Where PAYCODE=@PayCode
						AND OfficePunch = @TempMOut1
					END								
				
				ELSE
				BEGIN
					GOTO AsgIt
				END
					END
					ELSE
					BEGIN
						GOTO AsgIT
					END

					IF @TempGNightShiftFourPunch = 'Y'
					BEGIN
						GOTO AsgIt
					END

					IF @TempRsPunchCurrentRowIndex <= @TempMRsPunchCount AND @TempRsEmpTwo <> 'Y' AND @TempRsEmpInOnly = 'N'
					BEGIN
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1

						IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
						BEGIN
							GOTO AsgIt
						END

						SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''
						SELECT 
								@TempReasonCode = ISNULL(ReasonCode,''),
								@TempRsPunchOFficePunch = OFFICEPUNCH,
								@TempRSPunchPDay = ISNULL(P_DAY,'')
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex

						IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch)  > 0 AND @TempGNightShiftFourPunch = 'N'
						BEGIN
							GOTO AsgIt
						END

						IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
						BEGIN
							SET @TempMIn2 = @TempRsPunchOFficePunch

							IF LEN(LTRIM(RTRIM(@TempReasonCode))) > 0
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = @TempReasonCode								
								END
							END
							ELSE
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = ''
								END
							END

							IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
							BEGIN
								SET @TempMPFlg = 'Y'							
							END
							ELSE
							BEGIN
								SET @TempMPFlg = 'N'
							END

							IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END
							ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END

							IF @TempExecuteMachineRawUpdate = 1
							BEGIN
								UPDATE MachineRawPunch 
								Set P_Day=@TempMPFlg 
								Where SSN=@TempSSN
								AND OfficePunch = @TempMIn2
							END
						END
						ELSE
						BEGIN
							GOTO AsgIt
						END
					
						SET @TempRsPunchCurrentRowIndex = @TempRsPunchCurrentRowIndex + 1

						IF @TempRsPunchCurrentRowIndex > @TempMRsPunchCount
						BEGIN
							GOTO AsgIt
						END

						SET @TempReasonCode = ''
						SET @TempRsPunchOFficePunch = ''
						SET @TempRSPunchPDay = ''
						SELECT 
								@TempReasonCode = ISNULL(ReasonCode,''),
								@TempRsPunchOFficePunch = OFFICEPUNCH,
								@TempRSPunchPDay = ISNULL(P_DAY,'')
						FROM @TempRsPunch 
						WHERE RowNumber = @TempRsPunchCurrentRowIndex

						IF DATEDIFF(N, @TempRsPunchOFficePunch, @TempMDate1) >= 0
						BEGIN
							SET @TempMOut2 = @TempRsPunchOFficePunch

							IF LEN(LTRIM(RTRIM(@TempReasonCode))) > 0
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = @TempReasonCode
									SET @TempLastPunchReasonCode = @TempReasonCode	
								END
							END
							ELSE
							BEGIN
								IF LEN(RTRIM(LTRIM(@TempMReasonCode))) <= 0
								BEGIN
									SET @TempMReasonCode = ''
									SET @TempLastPunchReasonCode = ''	
								END
							END

							IF DATEDIFF(D, @TempRsTimeDateOffice, @TempRsPunchOFficePunch) > 0
							BEGIN
								SET @TempMPFlg = 'Y'							
							END
							ELSE
							BEGIN
								SET @TempMPFlg = 'N'
							END

							IF RTRIM(LTRIM(@TempRSPunchPDay)) = ''
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END
							ELSE IF @TempMPFlg <> RTRIM(LTRIM(@TempRSPunchPDay)) 
							BEGIN
								SET @TempExecuteMachineRawUpdate = 1
							END

							IF @TempExecuteMachineRawUpdate = 1
							BEGIN
								UPDATE MachineRawPunch 
								Set P_Day=@TempMPFlg 
								Where SSN=@TempSSN
								AND OfficePunch = @TempMOut2
							END
						END
				    END
				
				END
			AsgIt:
				

				IF @TempMOut2 IS NULL AND @TempMOut1 IS NOT NULL AND @TempMIn2 IS NULL 
				BEGIN
					SET @TempMOut2  = @TempMOut1
					SET @TempMOut1 = NULL
				END	

				SET @HolidayCount = 0

				SELECT @HolidayCount = COUNT(*) FROM Holiday(nolock) 
				WHERE DEPARTMENTCODE = @TempDepartmentCode 
				AND CompanyCode = @CompanyCode 
				AND  CAST(CONVERT(varchar(10), HDate, 101) AS DATE) = CAST(CONVERT(varchar(10), @TempRsTimeDateOffice, 101) AS DATE)  
		
				IF @HolidayCount > 0
				BEGIN 
					SET @IsHoliday = 1
				END
				ELSE
				BEGIN 
					SET @IsHoliday = 0
				END

				--SET @TempMRsShiftAttended = 
				EXEC [dbo].[ProcessDecideShift] @TempRsShiftAttended, @IsHoliday,@TempRSEmpAuthShift, @SetupId, @TempRsTimeDateOffice, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @CompanyCode, @TempRsTimeShift, @AutoShift
				EXEC [dbo].[ProcessPostLeaveThruReason] @TempMReasonCode, @TempMFirstPunchReasonCode, @TempLastPunchReasonCode, @TempPayCode, @TempRsTimeDateOffice
				
				SET @TempMRsTimeLeaveAmount = 0.0
				SET @TempMRsTimeLeaveType1 = ''
				SET @TempMRsTimeLeaveType2 = ''
				SET @TempMRsTimeLeaveCode = ''
				SET @TempMRsTimeLeaveType = ''
				SET @TempMRsTimeFirstHalfLeaveCode = ''
				SET @TempMRsTimeSecondHalfLeaveCode = ''
					 
				SELECT 
					@TempMRsTimeLeaveAmount  = LEAVEAMOUNT,
					@TempMRsTimeLeaveType1 = LEAVETYPE1,
					@TempMRsTimeLeaveType2 = LEAVETYPE2,
					@TempMRsTimeLeaveCode = LeaveCode,
					@TempMRsTimeLeaveType = LEAVETYPE,
					@TempMRsTimeFirstHalfLeaveCode = FIRSTHALFLEAVECODE,
					@TempMRsTimeSecondHalfLeaveCode = SECONDHALFLEAVECODE
				FROM tbltimeregister(nolock) 
				WHERE SSN= @TempSSN
				AND DateOFFICE = @TempRsTimeDateOffice

				--EXEC [dbo].[ProcessUPD2] @SetupId, @AutoShift, @CompanyCode, @TempDepartmentCode, @TempSSN, @TempRsTimeDateOffice, @TempMRsTimeLeaveAmount, @TempMRsTimeLeaveCode, @TempMRsTimeLeaveType, @TempMRsTimeLeaveType1, @TempMRsTimeLeaveType2,@TempMRsTimeFirstHalfLeaveCode, @TempMRsTimeSecondHalfLeaveCode, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @TempMRsEmpIsOS, @TempMRsEmpIsOT, @TempMRsEmpInOnly,@TempMRsEmpIsPunchAll,@TempMRsEmpPermisLateArrival,@TempMRsEmpTime,@TempMRsEmpPermisEarlyDepart,@TempMRsEmpIsHalfDay,@TempMRsEmpHalf,@TempMRsEmpIsShort,@TempMRsEmpShort,@TempMRsEmpIsTimeLossAllowed,@TempMRsEmpOTRate, @TempMIn1Manual, @TempMIn2Manual, @TempMOut1Manual, @TempMOut2Manual,@MIS,@ISOTONOFF
				EXEC [dbo].[ProcessUPD2] @TempSSN, @TempRsShiftAttended, @CompanyCode, @TempDepartmentCode, @TempSSN, @TempRsTimeDateOffice, @TempMRsTimeLeaveAmount, @TempMRsTimeLeaveCode, @TempMRsTimeLeaveType, @TempMRsTimeLeaveType1, @TempMRsTimeLeaveType2,@TempMRsTimeFirstHalfLeaveCode, @TempMRsTimeSecondHalfLeaveCode, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @TempMRsEmpIsOS, @TempMRsEmpIsOT, @TempMRsEmpInOnly,@TempMRsEmpIsPunchAll,@TempMRsEmpPermisLateArrival,@TempMRsEmpTime,@TempMRsEmpPermisEarlyDepart,@TempMRsEmpIsHalfDay,@TempMRsEmpHalf,@TempMRsEmpIsShort,@TempMRsEmpShort,@TempMRsEmpIsTimeLossAllowed,@TempMRsEmpOTRate, @TempMIn1Manual, @TempMIn2Manual, @TempMOut1Manual, @TempMOut2Manual,@MIS,@ISOTONOFF
				EXEC [dbo].[ProcessUPDReason] @TempPayCode, @TempRsTimeDateOffice, @TempSSN, @TempMReasonCode

				SET @TempRsTimeRowNumber = @TempRsTimeRowNumber + 1				
			END
			SET @TempRsEmpRowNumber = @TempRsEmpRowNumber + 1
		END
		
	END
	ELSE
	BEGIN
		PRINT 'Process All Records - No records to process'
	END
END




GO
/****** Object:  StoredProcedure [dbo].[ProcessAssignedHolidays]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessAssignedHolidays] 
@MinDate DATETIME,
@MaxDate DATETIME,
@CompanyCode VARCHAR(10),
@DepartmentCode VARCHAR(10),
@SetupId Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @HDate	DATETIME
	DECLARE @HOLIDAY VARCHAR(20)
	DECLARE @ADJUSTMENTHOLIDAY DATETIME
	DECLARE @OT_FACTOR FLOAT
	DECLARE @HD_COMPANYCODE VARCHAR(10)
	DECLARE @HD_DEPARTMENTCODE	VARCHAR(10)
	DECLARE @BRANCHCODE VARCHAR(10)
	DECLARE @Divisioncode	VARCHAR(10)	

	DECLARE @HolidayCount INT
	DECLARE @TempDepartmentCode VARCHAR(10)
	DECLARE @IsHoliday BIT
	DECLARE @TempRSEmpAuthShift CHAR(50)
	DECLARE @TempMIn1 DATETIME
	DECLARE @TempMIn2 DATETIME
	DECLARE @TempMOut1 DATETIME
	DECLARE @TempMOut2 DATETIME
	DECLARE @TempRsTimeShift CHAR(3)

	DECLARE @RsTimeUpdateSSN VARCHAR(100)


	DECLARE @RsTimeUpdateLeaveAmount FLOAT
	DECLARE @RsTimeUpdateLeaveCode CHAR(3)
	DECLARE @RsTimeUpdateLeaveType CHAR(1)
	DECLARE @RsTimeUpdateLeaveType1 CHAR(1)
	DECLARE @RsTimeUpdateLeaveType2 CHAR(1)
	DECLARE @RsTimeUpdateFirstHalfLeavecode CHAR(3)
	DECLARE @RsTimeUpdateSecondHalfLeavecode CHAR(3)
	DECLARE @RsTimeUpdateMIn1 DATETIME
	DECLARE @RsTimeUpdateMIn2 DATETIME
	DECLARE @RsTimeUpdateMOut1 DATETIME
	DECLARE @RsTimeUpdateMOut2 DATETIME
	DECLARE @RsEmpUpdateIsOS Char(1)
	DECLARE @RsEmpUpdateIsOT Char(1)
	DECLARE @RsEmpUpdateInOnly Char(1)
	DECLARE @RsEmpUpdateIsPunchAll Char(1)
	DECLARE @RsEmpUpdatePermisLateArrival INT
	DECLARE @RsEmpUpdateTime INT
	DECLARE @RsEmpUpdatePermisEarlyDepart INT
	DECLARE @RsEmpUpdateIsHalfDay CHAR(1)
	DECLARE @RsEmpUpdateHalf INT
	DECLARE @RsEmpUpdateIsShort CHAR(1) 
	DECLARE @RsEmpUpdateShort INT
	DECLARE @RsEmpUpdateIsTimeLossAllowed CHAR(1)
	DECLARE @RsEmpUpdateOTRate FLOAT
	DECLARE @RsTimeUpdateMIn1Manual CHAR(1)
	DECLARE @RsTimeUpdateMIn2Manual CHAR(1)
	DECLARE @RsTimeUpdateMOUT1Manual CHAR(1)
	DECLARE @RsTimeUpdateMOUT2Manual CHAR(1)

	DECLARE  @Holidays TABLE
	(
		HDate	DATETIME,
		HOLIDAY VARCHAR(20),
		ADJUSTMENTHOLIDAY DATETIME,
		OT_FACTOR FLOAT,
		HD_COMPANYCODE VARCHAR(10),
		HD_DEPARTMENTCODE	VARCHAR(10),
		BRANCHCODE VARCHAR(10),
		Divisioncode	VARCHAR(10)	
	)

	DECLARE @TempRsTime TABLE 
	(
	    RowNumber INT,
		PAYCODE CHAR(10) ,
		DateOFFICE DATETIME ,
		SHIFTSTARTTIME DATETIME ,
		SHIFTENDTIME DATETIME ,
		LUNCHSTARTTIME DATETIME ,
		LUNCHENDTIME DATETIME ,
		HOURSWORKED INT ,
		EXCLUNCHHOURS INT ,
		OTDURATION INT ,
		OSDURATION INT ,
		OTAMOUNT FLOAT ,
		EARLYARRIVAL INT ,
		EARLYDEPARTURE INT ,
		LATEARRIVAL INT ,
		LUNCHEARLYDEPARTURE INT ,
		LUNCHLATEARRIVAL INT ,
		TOTALLOSSHRS INT ,
		STATUS CHAR(6) ,
		LEAVETYPE1 CHAR(1) ,
		LEAVETYPE2 CHAR(1) ,
		FIRSTHALFLEAVECODE CHAR(3) ,
		SECONDHALFLEAVECODE CHAR(3) ,
		REASON VARCHAR(2000) ,
		SHIFT CHAR(3) ,
		SHIFTATTENDED CHAR(3) ,
		IN1 DATETIME ,
		IN2 DATETIME ,
		OUT1 DATETIME ,
		OUT2 DATETIME ,
		IN1MANNUAL CHAR(1) ,
		IN2MANNUAL CHAR(1) ,
		OUT1MANNUAL CHAR(1) ,
		OUT2MANNUAL CHAR(1) ,
		LEAVEVALUE FLOAT ,
		PRESENTVALUE FLOAT ,
		ABSENTVALUE FLOAT ,
		HOLIDAY_VALUE FLOAT ,
		WO_VALUE FLOAT ,
		OUTWORKDURATION INT ,
		LEAVETYPE CHAR(1) ,
		LEAVECODE CHAR(3) ,
		LEAVEAMOUNT FLOAT ,
		LEAVEAMOUNT1 FLOAT ,
		LEAVEAMOUNT2 FLOAT ,
		FLAG CHAR(4) ,
		LEAVEAPRDate DATETIME ,
		VOUCHER_NO CHAR(10) ,
		ReasonCode CHAR(3) ,
		ApprovedOT FLOAT ,
		WBR_Flag CHAR(1) ,
		Stage1OTApprovedBy CHAR(10) ,
		Stage2OTApprovedBy CHAR(10) ,
		Stage1ApprovalDate DATETIME ,
		Stage2ApprovalDate DATETIME ,
		AllowOT INT ,
		AbsentSMS CHAR(1) ,
		LateSMS CHAR(1) ,
		InSMS CHAR(1) ,
		OutSMS CHAR(1) ,
		SSN VARCHAR(100) 		
	)

	DECLARE @TempRsEmp TABLE 
	(
	    RowNumber INT,
		PAYCODE  CHAR (10),
		CARDNO  CHAR (8),
		SHIFT  CHAR (3) ,
		SHIFTTYPE  CHAR (1) ,
		SHIFTPATTERN  CHAR (11) ,
		SHIFTREMAINDAYS  INT  ,
		LASTSHIFTPERFORMED  CHAR (3) ,
		INONLY  CHAR (1) ,
		ISPUNCHALL  CHAR (1) ,
		ISTIMELOSSALLOWED  CHAR (1) ,
		ALTERNATE_OFF_DAYS  CHAR (10) ,
		CDAYS  FLOAT  ,
		ISROUNDTHECLOCKWORK  CHAR (1) ,
		ISOT  CHAR (1) ,
		OTRATE  CHAR (6) ,
		FIRSTOFFDAY  CHAR (3) ,
		SECONDOFFTYPE  CHAR (1) ,
		HALFDAYSHIFT  CHAR (3) ,
		SECONDOFFDAY  CHAR (3) ,
		PERMISLATEARRIVAL  INT  ,
		PERMISEARLYDEPRT  INT  ,
		ISAUTOSHIFT  CHAR (1) ,
		ISOUTWORK  CHAR (1) ,
		MAXDAYMIN  FLOAT  ,
		ISOS  CHAR (1) ,
		AUTH_SHIFTS  CHAR (50) ,
		TIME  INT ,
		SHORT  INT,
		HALF  INT,
		ISHALFDAY  CHAR (1) ,
		ISSHORT  CHAR (1) ,
		TWO  CHAR (1) ,
		isReleaver  CHAR (1) ,
		isWorker  CHAR (1) ,
		isFlexi  CHAR (1) ,
		SSN  VARCHAR (100)
	)


	DECLARE @TempRsTimeRecordCount INT
	DECLARE @TempHolidayCount INT
	DECLARE @TempRsTimeLoopIncrementor INT
	DECLARE @TempRsEmpRecordCount INT
	DECLARE @TempRsEmpIncrementor INT
	DECLARE @TempMShift CHAR(3)

	DECLARE @AutoShift VARCHAR(3)
	DECLARE @TempMUShiftStartTime DATETIME
	DECLARE @TempMUShiftEndTime DATETIME
	DECLARE @TempMULunchStartTime DATETIME
	DECLARE @TempMULunchEndTime DATETIME

	DECLARE @TempMUShift CHAR(3)

	-- RS Temp Variables

	DECLARE @TempRsPayCode VARCHAR(10)

	DECLARE @TempRsDateOffice DATETIME
	DECLARE @TempRsShiftAttended CHAR(3)

	DECLARE @TempRsEmpPayCode VARCHAR(10)
	
	INSERT INTO @Holidays 
			SELECT * FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @DepartmentCode 
			AND CompanyCode = @CompanyCode


   	SET @TempMUShiftStartTime  = NULL
	SET @TempMUShiftEndTime  = NULL
	SET @TempMULunchStartTime  = NULL
	SET @TempMULunchEndTime = NULL

	INSERT INTO @TempRsTime
			SELECT ROW_NUMBER() OVER (ORDER BY PayCode) RowNumber, * FROM tbltimeregister
			WHERE PAYCODE In (SELECT PayCode FROM tblemployee WHERE DepartmentCode = @DepartmentCode 
			AND CompanyCode = @CompanyCode )
			AND  ( CAST(CONVERT(varchar(10), DateOFFICE, 101) AS DATE) = CAST(CONVERT(varchar(10), @MinDate, 101) AS DATE)  
			OR CAST(CONVERT(varchar(10), DateOFFICE, 101) AS DATE) = CAST(CONVERT(varchar(10), @MaxDate, 101) AS DATE)   )
			ORDER BY DateOFFICE

    
	SELECT  @TempRsTimeRecordCount = COUNT(*) FROM @TempRsTime
	SET @TempRsTimeLoopIncrementor = 1
	WHILE @TempRsTimeRecordCount > 0 AND @TempRsTimeLoopIncrementor <= @TempRsTimeRecordCount
	BEGIN
	    
		SET @TempRsPayCode = ''
		SET @TempRsDateOffice = NULL
		SET @TempRsShiftAttended = ''
		SET @TempMIn1 = NULL
		SET @TempMIn2 = NULL
		SET @TempMOut1 = NULL
		SET @TempMOut2 = NULL
		SET @TempRsTimeShift = ''
		SET @RsTimeUpdateSSN = ''
		
		SELECT 
			@TempRsPayCode = PAYCODE,
			@TempRsDateOffice = DateOFFICE,
			@TempRsShiftAttended = SHIFTATTENDED,
			@TempMIn1 = IN1,
			@TempMIn2 = IN2,
			@TempMOut1 = OUT1,
			@TempMOut2 = OUT2,
			@TempRsTimeShift = [SHIFT],
			@RsTimeUpdateSSN = SSN
		FROM @TempRsTime 
		WHERE RowNumber = @TempRsTimeLoopIncrementor


	    INSERT INTO @TempRsEmp 
		SELECT  ROW_NUMBER() OVER (ORDER BY PayCode) RowNumber,
				* FROM TblEmployeeShiftMaster
		WHERE SSN = @RsTimeUpdateSSN

		SELECT @TempRsEmpRecordCount = COUNT(*) FROM @TempRsEmp
		SET @TempRsEmpIncrementor = 1

		

		IF @TempRsEmpRecordCount > 0
		BEGIN 	
		    SET @TempRsEmpPayCode = ''
			SET @RsEmpUpdateIsOS = ''
			SET @RsEmpUpdateIsOT = ''
			SET @RsEmpUpdateInOnly = ''
			SET @RsEmpUpdateIsPunchAll = ''
			SET @RsEmpUpdatePermisLateArrival = 0
			SET @RsEmpUpdateTime = 0
			SET @RsEmpUpdatePermisEarlyDepart = 0
			SET @RsEmpUpdateIsHalfDay = ''
			SET @RsEmpUpdateHalf = 0
			SET @RsEmpUpdateIsShort = ''
			SET @RsEmpUpdateShort = 0
			SET @RsEmpUpdateIsTimeLossAllowed = ''
			
					    
		    SELECT 
				@TempRsEmpPayCode = PAYCODE,
				@RsEmpUpdateIsOS = ISOS,
				@RsEmpUpdateIsOT = ISOT,
				@RsEmpUpdateInOnly = INONLY,
				@RsEmpUpdateIsPunchAll = ISPUNCHALL,
				@RsEmpUpdatePermisLateArrival = PERMISLATEARRIVAL,
				@RsEmpUpdateTime = [TIME],
				@RsEmpUpdatePermisEarlyDepart = PERMISEARLYDEPRT,
				@RsEmpUpdateIsHalfDay = ISHALFDAY,
				@RsEmpUpdateHalf = HALF,
				@RsEmpUpdateIsShort = ISSHORT,
				@RsEmpUpdateShort = SHORT,
				@RsEmpUpdateIsTimeLossAllowed = ISTIMELOSSALLOWED
				--@RsTimeUpdateOTRate = 
			FROM @TempRsEmp 
			WHERE RowNumber = @TempRsEmpIncrementor


			SET @HolidayCount = 0

			SELECT @HolidayCount = COUNT(*) FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @DepartmentCode 
			AND CompanyCode = @CompanyCode 
			AND  CAST(CONVERT(varchar(10), HDate, 101) AS DATE) = CAST(CONVERT(varchar(10), @TempRsDateOffice, 101) AS DATE)  
		
			IF @HolidayCount > 0
			BEGIN 
				SET @IsHoliday = 1
			END
			ELSE
			BEGIN 
				SET @IsHoliday = 0
			END
			IF CAST(CONVERT(varchar(10), @TempRsDateOffice, 101) AS DATE)  = CAST(CONVERT(varchar(10), @MinDate, 101) AS DATE) 
			BEGIN
				SET @TempMShift = @TempRsShiftAttended
			END
			ELSE
			BEGIN
				SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor + 1
				SELECT 
					@TempRsPayCode = PAYCODE,
					@TempRsDateOffice = DateOFFICE,
					@TempRsShiftAttended = SHIFTATTENDED,
					@TempMIn1 = IN1,
					@TempMIn2 = IN2,
					@TempMOut1 = OUT1,
					@TempMOut2 = OUT2,
					@TempRsTimeShift = [SHIFT]
				FROM @TempRsTime 
				WHERE RowNumber = @TempRsTimeLoopIncrementor

				IF @TempRsTimeLoopIncrementor = @TempRsTimeRecordCount 
				BEGIN
					SET @TempMShift = @TempRsShiftAttended
					SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor - 1
				END
				ELSE 
				BEGIN
					SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor - 1

					SET @TempRsPayCode = ''
					SET @TempRsDateOffice = NULL
					SET @TempRsShiftAttended = ''
					SET @TempMIn1 = NULL
					SET @TempMIn2 = NULL
					SET @TempMOut1 = NULL
					SET @TempMOut2 = NULL
					SET @TempRsTimeShift = ''

					SELECT 
						@TempRsPayCode = PAYCODE,
						@TempRsDateOffice = DateOFFICE,
						@TempRsShiftAttended = SHIFTATTENDED,
						@TempMUShiftStartTime = SHIFTSTARTTIME,
						@TempMUShiftEndTime = SHIFTENDTIME,
						@TempMULunchStartTime = LUNCHSTARTTIME,
						@TempMULunchEndTime = LUNCHENDTIME					
					FROM @TempRsTime 
					WHERE RowNumber = @TempRsTimeLoopIncrementor

					SET @TempMShift = @TempRsShiftAttended
				END


			END
						
			IF CAST(CONVERT(varchar(10), @TempRsDateOffice, 101) AS DATE)  != CAST(CONVERT(varchar(10), @MinDate, 101) AS DATE) 
			BEGIN
				SET @TempMUShift = @TempMShift
			END
			

			-- CALL AUTOSHIFT
			EXEC [dbo].[ProcessDecideShiftForHolidays] @RsTimeUpdateSSN, @TempMUShift, @IsHoliday,@TempRSEmpAuthShift, @SetupId, @TempRsDateOffice, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @CompanyCode, @TempRsTimeShift
			
			SET @RsTimeUpdateLeaveAmount = 0.0
			SET @RsTimeUpdateLeaveCode = ''
			SET @RsTimeUpdateLeaveType = ''
			SET @RsTimeUpdateLeaveType1 = ''
			SET @RsTimeUpdateLeaveType2 = ''
			SET @RsTimeUpdateFirstHalfLeavecode = ''
			SET @RsTimeUpdateSecondHalfLeavecode = ''
			SET @RsTimeUpdateMIn1 = NULL
			SET @RsTimeUpdateMIn2 = NULL
			SET @RsTimeUpdateMOut1 = NULL
			SET @RsTimeUpdateMOut2 = NULL	
			SET @RsTimeUpdateMIn1Manual = ''
			SET @RsTimeUpdateMIn2Manual = ''
			SET @RsTimeUpdateMOUT1Manual = ''
			SET @RsTimeUpdateMOUT2Manual = ''


			SELECT 
				@AutoShift = SHIFTATTENDED,
				@RsTimeUpdateLeaveAmount = LEAVEAMOUNT,
				@RsTimeUpdateLeaveCode = LEAVECODE,
				@RsTimeUpdateLeaveType = LEAVETYPE,
				@RsTimeUpdateLeaveType1 = LEAVETYPE1,
				@RsTimeUpdateLeaveType2 = LEAVETYPE2,
				@RsTimeUpdateFirstHalfLeavecode = FIRSTHALFLEAVECODE,
				@RsTimeUpdateSecondHalfLeavecode = SECONDHALFLEAVECODE,
				@RsTimeUpdateMIn1 = IN1,
				@RsTimeUpdateMIn2 = IN2,
				@RsTimeUpdateMOut1 = OUT1,
				@RsTimeUpdateMOut2 = OUT2,	
				@RsTimeUpdateMIn1Manual = IN1MANNUAL,
				@RsTimeUpdateMIn2Manual = IN2MANNUAL,
				@RsTimeUpdateMOUT1Manual = OUT1MANNUAL,
				@RsTimeUpdateMOUT2Manual = OUT2MANNUAL
			FROM tbltimeregister
			WHERE SSN = @RsTimeUpdateSSN
			AND DateOFFICE = @TempRsDateOffice
					
			-- CALL UPD
			EXEC [dbo].[ProcessUPD2] @SetupId, @AutoShift, @CompanyCode, @DepartmentCode, @RsTimeUpdateSSN, @TempRsDateOffice, @RsTimeUpdateLeaveAmount, @RsTimeUpdateLeaveCode, @RsTimeUpdateLeaveType, @RsTimeUpdateLeaveType1, @RsTimeUpdateLeaveType2,@RsTimeUpdateFirstHalfLeaveCode, @RsTimeUpdateSecondHalfLeaveCode, @RsTimeUpdateMIn1, @RsTimeUpdateMIn2, @RsTimeUpdateMOut1, @RsTimeUpdateMOut2, @RsEmpUpdateIsOS, @RsEmpUpdateIsOT, @RsEmpUpdateInOnly,@RsEmpUpdateIsPunchAll,@RsEmpUpdatePermisLateArrival,@RsEmpUpdateTime,@RsEmpUpdatePermisEarlyDepart,@RsEmpUpdateIsHalfDay,@RsEmpUpdateHalf,@RsEmpUpdateIsShort,@RsEmpUpdateShort,@RsEmpUpdateIsTimeLossAllowed,@RsEmpUpdateOTRate, @RsTimeUpdateMIn1Manual, @RsTimeUpdateMIn2Manual, @RsTimeUpdateMOUT1Manual, @RsTimeUpdateMOUT2Manual
			-- UPDATE RSTIME - Not required.

			SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor + 1
			IF @TempRsTimeLoopIncrementor <= @TempRsTimeRecordCount
			BEGIN
				SET @TempRsPayCode = ''
				SET @TempRsDateOffice = NULL
				SET @TempRsShiftAttended = ''
				SET @TempMIn1 = NULL
				SET @TempMIn2 = NULL
				SET @TempMOut1 = NULL
				SET @TempMOut2 = NULL
				SET @TempRsTimeShift = ''
				SET @RsTimeUpdateSSN = ''
				SET @RsTimeUpdateLeaveAmount = 0.0
				SET @RsTimeUpdateLeaveCode = ''
				SET @RsTimeUpdateLeaveType = ''
				SET @RsTimeUpdateLeaveType1 = ''
				SET @RsTimeUpdateLeaveType2 = ''
				SET @RsTimeUpdateFirstHalfLeavecode = ''
				SET @RsTimeUpdateSecondHalfLeavecode = ''
				SET @RsTimeUpdateMIn1 = NULL
				SET @RsTimeUpdateMIn2 = NULL
				SET @RsTimeUpdateMOut1 = NULL
				SET @RsTimeUpdateMOut2 = NULL	
				SET @RsTimeUpdateMIn1Manual = ''
				SET @RsTimeUpdateMIn2Manual = ''
				SET @RsTimeUpdateMOUT1Manual = ''
				SET @RsTimeUpdateMOUT2Manual = ''
				SELECT 
					@TempRsPayCode = PAYCODE,
					@TempRsDateOffice = DateOFFICE,
					@TempRsShiftAttended = SHIFTATTENDED,
					@TempMIn1 = IN1,
					@TempMIn2 = IN2,
					@TempMOut1 = OUT1,
					@TempMOut2 = OUT2,
					@TempRsTimeShift = [SHIFT],
					@RsTimeUpdateSSN = SSN,
					@AutoShift = SHIFTATTENDED,
					@RsTimeUpdateLeaveAmount = LEAVEAMOUNT,
					@RsTimeUpdateLeaveCode = LEAVECODE,
					@RsTimeUpdateLeaveType = LEAVETYPE,
					@RsTimeUpdateLeaveType1 = LEAVETYPE1,
					@RsTimeUpdateLeaveType2 = LEAVETYPE2,
					@RsTimeUpdateFirstHalfLeavecode = FIRSTHALFLEAVECODE,
					@RsTimeUpdateSecondHalfLeavecode = SECONDHALFLEAVECODE,
					@RsTimeUpdateMIn1 = IN1,
					@RsTimeUpdateMIn2 = IN2,
					@RsTimeUpdateMOut1 = OUT1,
					@RsTimeUpdateMOut2 = OUT2,	
					@RsTimeUpdateMIn1Manual = IN1MANNUAL,
					@RsTimeUpdateMIn2Manual = IN2MANNUAL,
					@RsTimeUpdateMOUT1Manual = OUT1MANNUAL,
					@RsTimeUpdateMOUT2Manual = OUT2MANNUAL
				FROM @TempRsTime 
				WHERE RowNumber = @TempRsTimeLoopIncrementor


				IF @TempRsPayCode = @TempRsEmpPayCode
				BEGIN
					IF @TempRsDateOffice != @MinDate
					BEGIN
						SET @TempMUShift = @TempMShift
					END
					-- CALL UPD
										
					SET @AutoShift  = @TempMUShift
					-- CALL UPD
					EXEC [dbo].[ProcessUPD2] @SetupId, @AutoShift, @CompanyCode, @TempDepartmentCode, @RsTimeUpdateSSN, @TempRsDateOffice, @RsTimeUpdateLeaveAmount, @RsTimeUpdateLeaveCode, @RsTimeUpdateLeaveType, @RsTimeUpdateLeaveType1, @RsTimeUpdateLeaveType2,@RsTimeUpdateFirstHalfLeaveCode, @RsTimeUpdateSecondHalfLeaveCode, @RsTimeUpdateMIn1, @RsTimeUpdateMIn2, @RsTimeUpdateMOut1, @RsTimeUpdateMOut2, @RsEmpUpdateIsOS, @RsEmpUpdateIsOT, @RsEmpUpdateInOnly,@RsEmpUpdateIsPunchAll,@RsEmpUpdatePermisLateArrival,@RsEmpUpdateTime,@RsEmpUpdatePermisEarlyDepart,@RsEmpUpdateIsHalfDay,@RsEmpUpdateHalf,@RsEmpUpdateIsShort,@RsEmpUpdateShort,@RsEmpUpdateIsTimeLossAllowed,@RsEmpUpdateOTRate, @RsTimeUpdateMIn1Manual, @RsTimeUpdateMIn2Manual, @RsTimeUpdateMOUT1Manual, @RsTimeUpdateMOUT2Manual
			

					-- UPDATE RsTime
					SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor + 1
				END
			END
		END
		ELSE
		BEGIN
		SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor + 1
			SELECT 
					@TempRsPayCode = PAYCODE,
					@TempRsDateOffice = DateOFFICE,
					@TempRsShiftAttended = SHIFTATTENDED,
					@TempMUShiftStartTime = SHIFTSTARTTIME,
					@TempMUShiftEndTime = SHIFTENDTIME,
					@TempMULunchStartTime = LUNCHSTARTTIME,
					@TempMULunchEndTime = LUNCHENDTIME	
			FROM @TempRsTime 
			WHERE RowNumber = @TempRsTimeLoopIncrementor
		END


	


		
	END
	-- UPDATE RSTime Batch

END






GO
/****** Object:  StoredProcedure [dbo].[ProcessAttendance]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessAttendance] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @TempCount INT
	DECLARE @TableID INT
	
	DECLARE @PayCode CHAR(10)	
	DECLARE @FromDate DATETIME 
	DECLARE @ToDate DATETIME 
	DECLARE @FunctionName VARCHAR(50) 
	DECLARE @CompanyCode VARCHAR(4) 
	DECLARE @DepartmentCode CHAR(10) 
	DECLARE @UsbFileName VARCHAR(200) 
	DECLARE @CaptureData CHAR(1)
	DECLARE @Called CHAR(1)  
	DECLARE @ID BIGINT  
	DECLARE @Divisioncode CHAR(3) 

	
	DECLARE @TempTblFunctionCall TABLE 
	(
		paycode char(10),
		FromDate datetime,
		ToDate datetime,
		FunctionName varchar(50),
		CompanyCode varchar(10),
		DepartmentCode varchar(10),
		UsbFileName varchar(200),
		CaptureData char(1),
		Called char(1),
		ID bigint,
		Divisioncode char(3) 
	)

	DECLARE @SetupId INT
	-- 
	DELETE from tblFunctionCall WHERE Called = 'Y'
	-- GET Record to process
	INSERT INTO @TempTblFunctionCall select * from tblFunctionCall(nolock) where (Called != 'Y' OR CALLED IS NULL )

	SELECT @TempCount = count(*) FROM @TempTblFunctionCall
	IF ( @TempCount > 0)
	BEGIN
		WHILE  EXISTS(SELECT * FROM @TempTblFunctionCall)
		BEGIN
		   -- GET First record to process
		   SELECT @TableID = (SELECT TOP 1 ID FROM @TempTblFunctionCall ORDER BY ID ASC)
	   
		   SELECT @PayCode = RTRIM(LTRIM(ISNULL(paycode,''))) , 
				  @FromDate = ISNULL(FromDate,''),
				  @ToDate = ISNULL(ToDate,''),
				  @FunctionName = UPPER(RTRIM(LTRIM(ISNULL(FunctionName,'')))),
				  @CompanyCode = RTRIM(LTRIM(ISNULL(CompanyCode,''))) ,
				  @DepartmentCode = RTRIM(LTRIM(ISNULL(DepartmentCode,''))) ,
				  @UsbFileName = RTRIM(LTRIM(ISNULL(UsbFileName,''))),
				  @CaptureData = RTRIM(LTRIM(ISNULL(CaptureData,''))),
				  @Called = Called,
				  @Divisioncode=RTRIM(LTRIM(ISNULL(Divisioncode,'')))
			from @TempTblFunctionCall where ID=@TableID


			SELECT TOP 1 @SetupId = SETUPID
				FROM tblsetup 
				
				ORDER BY SETUPID DESC

			IF @FunctionName = UPPER('CreateDutyRoster')
			BEGIN
				PRINT 'Inside CreateDutyRoster'
				EXEC ProcessCreateDutyRoster @PayCode, @FromDate,@CompanyCode
			END

			
			IF @FunctionName = UPPER('UpdateDutyRoster')
			BEGIN
				PRINT 'Inside UpdateDutyRoster'
				EXEC ProcessUpdateDutyRoster @PayCode, @FromDate,@CompanyCode
			END

			
			ELSE IF @FunctionName = UPPER('AssignHolidays')
			BEGIN
				PRINT 'Calling Assign Holidays'
				PRINT 'Parameters are :'
				PRINT '@FromDate : ' + CONVERT(VARCHAR, @FromDate, 120)
				PRINT '@ToDate : '  + CONVERT(VARCHAR, @ToDate, 120)
				PRINT '@CompanyCode :'+ @CompanyCode
				PRINT '@DepartmentCode : '+ @DepartmentCode


				EXEC ProcessAssignedHolidays @FromDate, @ToDate, @CompanyCode, @DepartmentCode,@SetupId
			END 
			ELSE IF @FunctionName = UPPER('DeleteHolidays')
			BEGIN
				PRINT 'Calling Delete Holidays'
				PRINT 'Parameters are :'
				PRINT '@FromDate : ' + CONVERT(VARCHAR, @FromDate, 120)
				PRINT '@ToDate : '  + CONVERT(VARCHAR, @ToDate, 120)
				PRINT '@CompanyCode :'+ @CompanyCode
				PRINT '@DepartmentCode : '+ @DepartmentCode


				EXEC ProcessDeleteHolidays @FromDate, @ToDate, @CompanyCode, @DepartmentCode,@SetupId
			END 
			ELSE IF  @FunctionName = UPPER('BackDateProcess') OR  @FunctionName = UPPER('Process_NonRTC_NEW')  OR  @FunctionName = UPPER('Process_RtcM_NEW')  OR  @FunctionName = UPPER('Process_NonRTCInout_NEW') OR @FunctionName = UPPER('Process_RTCInoutM_NEW') 
			BEGIN
				PRINT 'BackDateProcess'
				PRINT 'Parameters are :'
				PRINT '@FromDate : ' + CONVERT(VARCHAR, @FromDate, 120)
				PRINT '@ToDate : '  + CONVERT(VARCHAR, @ToDate, 120)
				PRINT '@CompanyCode :'+ @CompanyCode
				PRINT '@DepartmentCode : '+ @DepartmentCode
				PRINT '@PayCode :'+ @PayCode
				PRINT '@SetupId :'+ CONVERT (VARCHAR,@SetupId)

				EXEC ProcessBackDate @FromDate, @ToDate, @CompanyCode, @DepartmentCode, @PayCode, @SetupId
			END 

		   -- DELETE the record from temp table after processing
		   DELETE @TempTblFunctionCall  where ID = @TableID
		   DELETE FROM TblFunctionCall where ID = @TableID

		END
	END
	ELSE
	BEGIN
	 PRINT 'No record to process'
	END	
END





GO
/****** Object:  StoredProcedure [dbo].[ProcessBackDate]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery7.sql|7|0|C:\Users\Amajd\AppData\Local\Temp\~vsF422.sql

CREATE PROCEDURE [dbo].[ProcessBackDate]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@CompanyCode VARCHAR(4),
	@DepartmentCode CHAR(3),
	@PayCode VARCHAR(10),
	@SetupId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TempInOUT Char(1)	

    IF @PayCode != ''
	BEGIN
		UPDATE machinerawpunch 
		SET p_day='N' 
		WHERE companycode=@CompanyCode 
		AND officepunch >= @FromDate
		AND PAYCODE = @PayCode
	END
	ELSE
	BEGIN
		UPDATE machinerawpunch 
		SET p_day='N' 
		WHERE companycode=@CompanyCode and CAST(CONVERT(varchar(10), officepunch, 101) AS DATE)   >=  CAST(CONVERT(varchar(10), @FromDate, 101) AS DATE) 
		
	END
	
	SELECT @TempInOUT=INOUT FROM tblsetup (nolock)-- where CompanyCode=@CompanyCode
	
	SET @FromDate = CAST(CONVERT(varchar(10), @FromDate, 101) AS DATE)	
	IF @ToDate = ''
	BEGIN
		SET @ToDate = CONVERT(DATETIME, CONVERT(varchar(11),GETDATE(), 101 ) + ' 23:59:59', 101)
		--SET @ToDate =	CAST(CONVERT(varchar(10), GETDATE(), 101) AS DATE)		
	END
	ELSE
	BEGIN
		SET @ToDate = CONVERT(DATETIME, CONVERT(varchar(11),@ToDate, 101 ) + ' 23:59:59', 101)		
	END
	
	
	-- TODO - Use for loop, pass set up id
	EXEC ProcessAllRecords @FromDate,@ToDate, @CompanyCode, @PayCode, @SetupId

END






GO
/****** Object:  StoredProcedure [dbo].[ProcessCreateDutyRoster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessCreateDutyRoster] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@CompanyCode VARCHAR(10)
AS
BEGIN
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	PRINT 'INSIDE ProcessCreateDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT '@CompanyCode :' + @CompanyCode

	DECLARE @TempCount INT
	DECLARE @TableID INT
	DECLARE @TempSSN VARCHAR(100)
	DECLARE @MinDate DATETIME
	DECLARE @WOInclude VARCHAR(5)

	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @SSN VARCHAR(100)

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		SSN VARCHAR(100)
	)

	IF (@PayCode = '')
	BEGIN
		INSERT INTO @EmployeeShiftDetails
		SELECT EmpShift.PAYCODE, 
			   Emp.DateOFJOIN,
			   EmpShift.[SHIFT],
			   EmpShift.ALTERNATE_OFF_DAYS,
			   EmpShift.FIRSTOFFDAY,
			   EmpShift.SECONDOFFTYPE,
			   EmpShift.HALFDAYSHIFT,
			   EmpShift.SECONDOFFDAY,
			   Emp.SSN
		FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
		INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
		WHERE Emp.active = 'Y' 
		AND Emp.Companycode=@CompanyCode  Order By EmpShift.Paycode
    END
	ELSE
	BEGIN
		INSERT INTO @EmployeeShiftDetails
		SELECT EmpShift.PAYCODE, 
			   Emp.DateOFJOIN,
			   EmpShift.[SHIFT],
			   EmpShift.ALTERNATE_OFF_DAYS,
			   EmpShift.FIRSTOFFDAY,
			   EmpShift.SECONDOFFTYPE,
			   EmpShift.HALFDAYSHIFT,
			   EmpShift.SECONDOFFDAY,
			   Emp.SSN
		FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
		INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
		WHERE Emp.active = 'Y' 
		AND Emp.Companycode=@CompanyCode 
		AND EmpShift.PAYCODE = @PayCode Order By EmpShift.Paycode
	END

	SELECT @TempCount = count(*) FROM @EmployeeShiftDetails
	IF ( @TempCount > 0)
	BEGIN
		WHILE  EXISTS(SELECT * FROM @EmployeeShiftDetails)
		BEGIN
		   -- GET First record to process
		   SELECT @TempSSN = (SELECT TOP 1 SSN FROM @EmployeeShiftDetails ORDER BY SSN ASC)
		   SELECT @TempPayCode = (SELECT TOP 1 PayCode FROM @EmployeeShiftDetails ORDER BY SSN ASC)
	       SELECT @DateOfJoin = DateOfJoin FROM @EmployeeShiftDetails WHERE SSN = @TempSSN

		   IF @DateOfJoin > @FromDate
		   BEGIN
				SET @MinDate = @DateOfJoin  
		   END
		   ELSE
		   BEGIN
				SET @MinDate = @FromDate
		   END
		   
		   SELECT @WOInclude = WOInclude FROM tblsetup (nolock) where CompanyCode=@CompanyCode

		   EXEC ProcessDutyRosterForCreate @TempPayCode, @MinDate, @WOInclude, @TempSSN

		   -- DELETE the record from temp table after processing
		   DELETE @EmployeeShiftDetails  where SSN = @TempSSN

		END
	END
	ELSE
	BEGIN
	 PRINT 'No record to process'
	END	


	--SELECT * FROM @EmployeeShiftDetails
END






GO
/****** Object:  StoredProcedure [dbo].[ProcessDecideShift]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[ProcessDecideShift] 
@ShiftAttended VARCHAR(3),
@IsHoliday BIT,	
@EmpAutoShifts VARCHAR(11),
@SetupID INT,
@DateOfOffice DATETIME,
@MIn1 DATETIME,
@MIn2 DATETIME,
@MOut1 DATETIME,
@MOut2 DATETIME,
@CompanyCode VARCHAR(10),
@MTimeShift VARCHAR(3),
@AutoShift CHAR(3) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempAutoShift_Low INT
	DECLARE @TempAutoShift_Up INT
	DECLARE @TempMCtr INT
	DECLARE @TempMFlag BIT
	DECLARE @TempMShift VARCHAR(11)
	DECLARE @TempShift VARCHAR(3)
	DECLARE @TempDate DATE
	DECLARE @TempMShiftTime DATETIME
	DECLARE @TempMShiftStartTime DATETIME
	DECLARE @TempMShiftEndTime DATETIME
	DECLARE @TempMinutes INT

	DECLARE @TempShiftStartDateTime DATETIME
	DECLARE @TempMShiftStartDateTime DATETIME
	DECLARE @TempMShiftEndDateTime DATETIME
	
	DECLARE @TempInnerShift VARCHAR(3)

	SET @AutoShift = @ShiftAttended

	IF  @IsHoliday = 1 OR @ShiftAttended='OFF'
	BEGIN 
		--RETURN @AutoShift
			SELECT @AutoShift
			RETURN 0;
	END

	SET @EmpAutoShifts = ISNULL(@EmpAutoShifts,'')

	IF @EmpAutoShifts = '' AND LEN(LTRIM(RTRIM(@EmpAutoShifts))) = 0
	BEGIN 
		--RETURN @AutoShift
		SELECT @AutoShift
		RETURN 0;
	END

	SET @TempMShift = @EmpAutoShifts
	SELECT 
		@TempAutoShift_Low = AUTOSHIFT_LOW,
		@TempAutoshift_Up = AUTOSHIFT_UP
	FROM tblsetup 
	WHERE SETUPID = @SetupId 

	SET @TempMCtr = 1
	SET @TempMFlag = 0

	WHILE SUBSTRING(@TempMShift,@TempMCtr,3) != ''
	BEGIN
		SET @TempShift = SUBSTRING(@TempMShift,@TempMCtr,3)

		SET @TempInnerShift = ''
		SELECT 	
			@TempInnerShift = ISNULL([SHIFT],''),
			@TempMShiftStartTime = StartTime,
			@TempMShiftEndTime = ENDTIME			
		FROM [dbo].[tblShiftMaster]
		WHERE [SHIFT]=@TempShift
		--AND CompanyCode = @CompanyCode

		IF @TempInnerShift != ''
		BEGIN
			SET @TempDate = CAST(@DateOfOffice AS date)
			SET @TempShiftStartDateTime = CAST(@TempDate AS DATETIME)

			SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempMShiftStartTime), 0), @TempMShiftStartTime)
			SET @TempShiftStartDateTime = DATEADD(HH, @TempMinutes/60, @TempShiftStartDateTime)
		    SET @TempShiftStartDateTime = DATEADD(MINUTE, @TempMinutes%60,@TempShiftStartDateTime)


			SET @TempMShiftStartDateTime= DATEADD(N,0-@TempAutoShift_Low, @TempShiftStartDateTime)
			SET @TempMShiftEndDateTime = DATEADD(N, @TempAutoShift_Low, @TempShiftStartDateTime)

			IF ISDATE(@MIn1) = 1
			BEGIN 
				IF (DATEDIFF(N, @TempMShiftStartDateTime, @MIn1) >= 0) AND (DATEDIFF(N, @MIn1, @TempMShiftEndDateTime) >= 0)
				BEGIN 
					SET @AutoShift = @TempShift
					SET @TempMFlag = 1
						SELECT @AutoShift
						RETURN 0;
				END
			END
		END
		SET @TempMCtr = @TempMCtr + 4	 
	END

	IF ISDATE(@MIn1) = 0 AND ISDATE(@MOut1) = 0 AND ISDATE(@MIn2) = 0 AND ISDATE(@MOut2) = 0
	BEGIN 
		SET @AutoShift = @MTimeShift
	END

	IF @TempMFlag = 0 
	BEGIN 
		SET @AutoShift = @MTimeShift
	END	

	SELECT @AutoShift
	--RETURN @AutoShift    
	RETURN 0;
END






GO
/****** Object:  StoredProcedure [dbo].[ProcessDecideShiftForHolidays]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE  [dbo].[ProcessDecideShiftForHolidays] 
@TempSSN VARCHAR(100),
@ShiftAttended VARCHAR(3),
@IsHoliday BIT,	
@EmpAutoShifts VARCHAR(11),
@SetupID INT,
@DateOfOffice DATETIME,
@MIn1 DATETIME,
@MIn2 DATETIME,
@MOut1 DATETIME,
@MOut2 DATETIME,
@CompanyCode VARCHAR(10),
@MTimeShift VARCHAR(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempAutoShift_Low INT
	DECLARE @TempAutoShift_Up INT
	DECLARE @TempMCtr INT
	DECLARE @TempMFlag BIT
	DECLARE @TempMShift VARCHAR(11)
	DECLARE @TempShift VARCHAR(3)
	DECLARE @TempDate DATE
	DECLARE @TempMShiftTime DATETIME
	DECLARE @TempMShiftStartTime DATETIME
	DECLARE @TempMShiftEndTime DATETIME
	DECLARE @TempMinutes INT

	DECLARE @TempShiftStartDateTime DATETIME
	DECLARE @TempMShiftStartDateTime DATETIME
	DECLARE @TempMShiftEndDateTime DATETIME
	
	DECLARE @TempInnerShift VARCHAR(3)

	DECLARE @AutoShift VARCHAR(3) 
	DECLARE @TempMUShiftStartTime DATETIME 
	DECLARE @TempMUShiftEndTime DATETIME 
	DECLARE @TempMULunchStartTime DATETIME 
	DECLARE @TempMULunchEndTime DATETIME 

	SET @AutoShift = @ShiftAttended

	IF  @IsHoliday = 1 OR @MTimeShift='OFF'
	BEGIN 
		--RETURN @AutoShift 
		set @TempMUShiftStartTime  = NULL
		set @TempMUShiftEndTime  = NULL
		set @TempMULunchStartTime  = NULL
		set @TempMULunchEndTime = NULL

		--SELECT @AutoShift, @TempMUShiftStartTime, @TempMUShiftEndTime, @TempMULunchStartTime, @TempMULunchEndTime
		GOTO UpdateShift
	END
	SET @EmpAutoShifts = ISNULL(@EmpAutoShifts,'')

	IF @EmpAutoShifts = '' AND LEN(LTRIM(RTRIM(@EmpAutoShifts))) = 0
	BEGIN 
		--RETURN @AutoShift

		SET @AutoShift = @MTimeShift
		--SELECT @AutoShift, @TempMUShiftStartTime, @TempMUShiftEndTime, @TempMULunchStartTime, @TempMULunchEndTime
		GOTO UpdateShift
	END

	SET @TempMShift = @EmpAutoShifts
	SELECT 
		@TempAutoShift_Low = AUTOSHIFT_LOW,
		@TempAutoshift_Up = AUTOSHIFT_UP
	FROM tblsetup 
	WHERE SETUPID = @SetupId 

	SET @TempMCtr = 1
	SET @TempMFlag = 0

	WHILE SUBSTRING(@TempMShift,@TempMCtr,3) != ''
	BEGIN
		SET @TempShift = SUBSTRING(@TempMShift,@TempMCtr,3)

		SET @TempInnerShift = ''
		SELECT 	
			@TempInnerShift = ISNULL([SHIFT],''),
			@TempMShiftStartTime = StartTime,
			@TempMShiftEndTime = ENDTIME			
		FROM [dbo].[tblShiftMaster]
		WHERE [SHIFT]=@TempShift
		AND CompanyCode = @CompanyCode

		IF @TempInnerShift != ''
		BEGIN
			SET @TempDate = CAST(@DateOfOffice AS date)
			SET @TempShiftStartDateTime = CAST(@TempDate AS DATETIME)

			SET @TempMinutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempMShiftStartTime), 0), @TempMShiftStartTime)
			SET @TempShiftStartDateTime = DATEADD(HH, @TempMinutes/60, @TempShiftStartDateTime)
		    SET @TempShiftStartDateTime = DATEADD(MINUTE, @TempMinutes%60,@TempShiftStartDateTime)


			SET @TempMShiftStartDateTime= DATEADD(N,0-@TempAutoShift_Low, @TempShiftStartDateTime)
			SET @TempMShiftEndDateTime = DATEADD(N, @TempAutoShift_Low, @TempShiftStartDateTime)

			IF (DATEDIFF(N, @TempMShiftStartDateTime, @MIn1) >= 0) AND (DATEDIFF(N, @MIn1, @TempMShiftEndDateTime) >= 0)
			BEGIN 
				SET @AutoShift = @TempShift
				SET @TempMFlag = 1
				BREAK
			END

		END
		SET @TempMCtr = @TempMCtr + 4	 
	END

	IF ISDATE(@MIn1) = 0 AND ISDATE(@MOut1) = 0 AND ISDATE(@MIn2) = 0 AND ISDATE(@MOut2) = 0
	BEGIN 
		SET @AutoShift = @MTimeShift
		SET @TempMUShiftStartTime  = NULL
		SET @TempMUShiftEndTime  = NULL
		SET @TempMULunchStartTime  = NULL
		SET @TempMULunchEndTime = NULL
	END

	IF @TempMFlag = 0 
	BEGIN 
		SET @AutoShift = @MTimeShift
		SET @TempMUShiftStartTime  = NULL
		SET @TempMUShiftEndTime  = NULL
		SET @TempMULunchStartTime  = NULL
		SET @TempMULunchEndTime = NULL
	END	

	--SELECT @AutoShift, @TempMUShiftStartTime, @TempMUShiftEndTime, @TempMULunchStartTime, @TempMULunchEndTime
	--RETURN @AutoShift    
	
	UpdateShift:
	UPDATE tbltimeregister
	SET 
		SHIFTATTENDED = @AutoShift,
		SHIFTSTARTTIME = @TempMUShiftStartTime,
		SHIFTENDTIME = @TempMUShiftEndTime,
		LUNCHSTARTTIME = @TempMULunchStartTime,
		LUNCHENDTIME = @TempMULunchEndTime
	WHERE SSN = @TempSSN
	AND DateOFFICE = @DateOfOffice
END






GO
/****** Object:  StoredProcedure [dbo].[ProcessDeleteHolidays]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessDeleteHolidays]
	@MinDate DATETIME,
	@MaxDate DATETIME,
	@CompanyCode VARCHAR(10),
	@DepartmentCode VARCHAR(10),
	@SetupId Int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @HDate	DATETIME
	DECLARE @HOLIDAY VARCHAR(20)
	DECLARE @ADJUSTMENTHOLIDAY DATETIME
	DECLARE @OT_FACTOR FLOAT
	DECLARE @HD_COMPANYCODE VARCHAR(10)
	DECLARE @HD_DEPARTMENTCODE	VARCHAR(10)
	DECLARE @BRANCHCODE VARCHAR(10)
	DECLARE @Divisioncode	VARCHAR(10)	

	DECLARE @HolidayCount INT
	DECLARE @TempDepartmentCode VARCHAR(10)
	DECLARE @IsHoliday BIT
	DECLARE @TempRSEmpAuthShift CHAR(50)
	DECLARE @TempMIn1 DATETIME
	DECLARE @TempMIn2 DATETIME
	DECLARE @TempMOut1 DATETIME
	DECLARE @TempMOut2 DATETIME
	DECLARE @TempRsTimeShift CHAR(3)

	DECLARE @RsTimeUpdateSSN VARCHAR(100)


	DECLARE @RsTimeUpdateLeaveAmount FLOAT
	DECLARE @RsTimeUpdateLeaveCode CHAR(3)
	DECLARE @RsTimeUpdateLeaveType CHAR(1)
	DECLARE @RsTimeUpdateLeaveType1 CHAR(1)
	DECLARE @RsTimeUpdateLeaveType2 CHAR(1)
	DECLARE @RsTimeUpdateFirstHalfLeavecode CHAR(3)
	DECLARE @RsTimeUpdateSecondHalfLeavecode CHAR(3)
	DECLARE @RsTimeUpdateMIn1 DATETIME
	DECLARE @RsTimeUpdateMIn2 DATETIME
	DECLARE @RsTimeUpdateMOut1 DATETIME
	DECLARE @RsTimeUpdateMOut2 DATETIME
	DECLARE @RsEmpUpdateIsOS Char(1)
	DECLARE @RsEmpUpdateIsOT Char(1)
	DECLARE @RsEmpUpdateInOnly Char(1)
	DECLARE @RsEmpUpdateIsPunchAll Char(1)
	DECLARE @RsEmpUpdatePermisLateArrival INT
	DECLARE @RsEmpUpdateTime INT
	DECLARE @RsEmpUpdatePermisEarlyDepart INT
	DECLARE @RsEmpUpdateIsHalfDay CHAR(1)
	DECLARE @RsEmpUpdateHalf INT
	DECLARE @RsEmpUpdateIsShort CHAR(1) 
	DECLARE @RsEmpUpdateShort INT
	DECLARE @RsEmpUpdateIsTimeLossAllowed CHAR(1)
	DECLARE @RsEmpUpdateOTRate FLOAT
	DECLARE @RsTimeUpdateMIn1Manual CHAR(1)
	DECLARE @RsTimeUpdateMIn2Manual CHAR(1)
	DECLARE @RsTimeUpdateMOUT1Manual CHAR(1)
	DECLARE @RsTimeUpdateMOUT2Manual CHAR(1)

	DECLARE  @Holidays TABLE
	(
		HDate	DATETIME,
		HOLIDAY VARCHAR(20),
		ADJUSTMENTHOLIDAY DATETIME,
		OT_FACTOR FLOAT,
		HD_COMPANYCODE VARCHAR(10),
		HD_DEPARTMENTCODE	VARCHAR(10),
		BRANCHCODE VARCHAR(10),
		Divisioncode	VARCHAR(10)	
	)

	DECLARE @TempRsTime TABLE 
	(
	    RowNumber INT,
		PAYCODE CHAR(10) ,
		DateOFFICE DATETIME ,
		SHIFTSTARTTIME DATETIME ,
		SHIFTENDTIME DATETIME ,
		LUNCHSTARTTIME DATETIME ,
		LUNCHENDTIME DATETIME ,
		HOURSWORKED INT ,
		EXCLUNCHHOURS INT ,
		OTDURATION INT ,
		OSDURATION INT ,
		OTAMOUNT FLOAT ,
		EARLYARRIVAL INT ,
		EARLYDEPARTURE INT ,
		LATEARRIVAL INT ,
		LUNCHEARLYDEPARTURE INT ,
		LUNCHLATEARRIVAL INT ,
		TOTALLOSSHRS INT ,
		STATUS CHAR(6) ,
		LEAVETYPE1 CHAR(1) ,
		LEAVETYPE2 CHAR(1) ,
		FIRSTHALFLEAVECODE CHAR(3) ,
		SECONDHALFLEAVECODE CHAR(3) ,
		REASON VARCHAR(2000) ,
		SHIFT CHAR(3) ,
		SHIFTATTENDED CHAR(3) ,
		IN1 DATETIME ,
		IN2 DATETIME ,
		OUT1 DATETIME ,
		OUT2 DATETIME ,
		IN1MANNUAL CHAR(1) ,
		IN2MANNUAL CHAR(1) ,
		OUT1MANNUAL CHAR(1) ,
		OUT2MANNUAL CHAR(1) ,
		LEAVEVALUE FLOAT ,
		PRESENTVALUE FLOAT ,
		ABSENTVALUE FLOAT ,
		HOLIDAY_VALUE FLOAT ,
		WO_VALUE FLOAT ,
		OUTWORKDURATION INT ,
		LEAVETYPE CHAR(1) ,
		LEAVECODE CHAR(3) ,
		LEAVEAMOUNT FLOAT ,
		LEAVEAMOUNT1 FLOAT ,
		LEAVEAMOUNT2 FLOAT ,
		FLAG CHAR(4) ,
		LEAVEAPRDate DATETIME ,
		VOUCHER_NO CHAR(10) ,
		ReasonCode CHAR(3) ,
		ApprovedOT FLOAT ,
		WBR_Flag CHAR(1) ,
		Stage1OTApprovedBy CHAR(10) ,
		Stage2OTApprovedBy CHAR(10) ,
		Stage1ApprovalDate DATETIME ,
		Stage2ApprovalDate DATETIME ,
		AllowOT INT ,
		AbsentSMS CHAR(1) ,
		LateSMS CHAR(1) ,
		InSMS CHAR(1) ,
		OutSMS CHAR(1) ,
		SSN VARCHAR(100) 		
	)

	DECLARE @TempRsEmp TABLE 
	(
	    RowNumber INT,
		PAYCODE  CHAR (10),
		CARDNO  CHAR (8),
		SHIFT  CHAR (3) ,
		SHIFTTYPE  CHAR (1) ,
		SHIFTPATTERN  CHAR (11) ,
		SHIFTREMAINDAYS  INT  ,
		LASTSHIFTPERFORMED  CHAR (3) ,
		INONLY  CHAR (1) ,
		ISPUNCHALL  CHAR (1) ,
		ISTIMELOSSALLOWED  CHAR (1) ,
		ALTERNATE_OFF_DAYS  CHAR (10) ,
		CDAYS  FLOAT  ,
		ISROUNDTHECLOCKWORK  CHAR (1) ,
		ISOT  CHAR (1) ,
		OTRATE  CHAR (6) ,
		FIRSTOFFDAY  CHAR (3) ,
		SECONDOFFTYPE  CHAR (1) ,
		HALFDAYSHIFT  CHAR (3) ,
		SECONDOFFDAY  CHAR (3) ,
		PERMISLATEARRIVAL  INT  ,
		PERMISEARLYDEPRT  INT  ,
		ISAUTOSHIFT  CHAR (1) ,
		ISOUTWORK  CHAR (1) ,
		MAXDAYMIN  FLOAT  ,
		ISOS  CHAR (1) ,
		AUTH_SHIFTS  CHAR (50) ,
		TIME  INT ,
		SHORT  INT,
		HALF  INT,
		ISHALFDAY  CHAR (1) ,
		ISSHORT  CHAR (1) ,
		TWO  CHAR (1) ,
		isReleaver  CHAR (1) ,
		isWorker  CHAR (1) ,
		isFlexi  CHAR (1) ,
		SSN  VARCHAR (100)
	)


	DECLARE @TempRsTimeRecordCount INT
	DECLARE @TempHolidayCount INT
	DECLARE @TempRsTimeLoopIncrementor INT
	DECLARE @TempRsEmpRecordCount INT
	DECLARE @TempRsEmpIncrementor INT
	DECLARE @TempMShift CHAR(3)

	DECLARE @AutoShift VARCHAR(3)
	DECLARE @TempMUShiftStartTime DATETIME
	DECLARE @TempMUShiftEndTime DATETIME
	DECLARE @TempMULunchStartTime DATETIME
	DECLARE @TempMULunchEndTime DATETIME

	DECLARE @TempMUShift CHAR(3)

	-- RS Temp Variables

	DECLARE @TempRsPayCode VARCHAR(10)

	DECLARE @TempRsDateOffice DATETIME
	DECLARE @TempRsShiftAttended CHAR(3)

	DECLARE @TempRsEmpPayCode VARCHAR(10)
	
	INSERT INTO @Holidays 
			SELECT * FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @DepartmentCode 
			AND CompanyCode = @CompanyCode


   	SET @TempMUShiftStartTime  = NULL
	SET @TempMUShiftEndTime  = NULL
	SET @TempMULunchStartTime  = NULL
	SET @TempMULunchEndTime = NULL

	UPDATE tbltimeregister  set SHIFTATTENDED=[SHIFT] 
	WHERE PAYCODE in 
	      (
				SELECT PAYCODE FROM TBLEMPLOYEE 
				WHERE COMPANYCODE= @CompanyCode  AND DEPARTMENTCODE=@DepartmentCode AND DateOFFICE=@MinDate 
		  )	

	INSERT INTO @TempRsTime
				SELECT ROW_NUMBER() OVER (ORDER BY PayCode) RowNumber, * FROM tbltimeregister
				WHERE  DateOFFICE=@MinDate AND PAYCODE In (SELECT PayCode FROM tblemployee WHERE DepartmentCode = @DepartmentCode 
				AND CompanyCode = @CompanyCode )

    
	SELECT  @TempRsTimeRecordCount = COUNT(*) FROM @TempRsTime
	SET @TempRsTimeLoopIncrementor = 1
	WHILE @TempRsTimeRecordCount > 0 AND @TempRsTimeLoopIncrementor <= @TempRsTimeRecordCount
	BEGIN
	    
		SET @TempRsPayCode = ''
		SET @TempRsDateOffice = NULL
		SET @TempRsShiftAttended = ''
		SET @TempMIn1 = NULL
		SET @TempMIn2 = NULL
		SET @TempMOut1 = NULL
		SET @TempMOut2 = NULL
		SET @TempRsTimeShift = ''
		SET @RsTimeUpdateSSN = ''
		
		SELECT 
			@TempRsPayCode = PAYCODE,
			@TempRsDateOffice = DateOFFICE,
			@TempRsShiftAttended = SHIFTATTENDED,
			@TempMIn1 = IN1,
			@TempMIn2 = IN2,
			@TempMOut1 = OUT1,
			@TempMOut2 = OUT2,
			@TempRsTimeShift = [SHIFT],
			@RsTimeUpdateSSN = SSN
		FROM @TempRsTime 
		WHERE RowNumber = @TempRsTimeLoopIncrementor


	    INSERT INTO @TempRsEmp 
		SELECT  ROW_NUMBER() OVER (ORDER BY PayCode) RowNumber,
				* FROM TblEmployeeShiftMaster
		WHERE SSN = @RsTimeUpdateSSN

		SELECT @TempRsEmpRecordCount = COUNT(*) FROM @TempRsEmp
		SET @TempRsEmpIncrementor = 1

		

		IF @TempRsEmpRecordCount > 0
		BEGIN 	
		    SET @TempRsEmpPayCode = ''
			SET @RsEmpUpdateIsOS = ''
			SET @RsEmpUpdateIsOT = ''
			SET @RsEmpUpdateInOnly = ''
			SET @RsEmpUpdateIsPunchAll = ''
			SET @RsEmpUpdatePermisLateArrival = 0
			SET @RsEmpUpdateTime = 0
			SET @RsEmpUpdatePermisEarlyDepart = 0
			SET @RsEmpUpdateIsHalfDay = ''
			SET @RsEmpUpdateHalf = 0
			SET @RsEmpUpdateIsShort = ''
			SET @RsEmpUpdateShort = 0
			SET @RsEmpUpdateIsTimeLossAllowed = ''
			
					    
		    SELECT 
				@TempRsEmpPayCode = PAYCODE,
				@RsEmpUpdateIsOS = ISOS,
				@RsEmpUpdateIsOT = ISOT,
				@RsEmpUpdateInOnly = INONLY,
				@RsEmpUpdateIsPunchAll = ISPUNCHALL,
				@RsEmpUpdatePermisLateArrival = PERMISLATEARRIVAL,
				@RsEmpUpdateTime = [TIME],
				@RsEmpUpdatePermisEarlyDepart = PERMISEARLYDEPRT,
				@RsEmpUpdateIsHalfDay = ISHALFDAY,
				@RsEmpUpdateHalf = HALF,
				@RsEmpUpdateIsShort = ISSHORT,
				@RsEmpUpdateShort = SHORT,
				@RsEmpUpdateIsTimeLossAllowed = ISTIMELOSSALLOWED
				--@RsTimeUpdateOTRate = 
			FROM @TempRsEmp 
			WHERE RowNumber = @TempRsEmpIncrementor


			SET @HolidayCount = 0

			SELECT @HolidayCount = COUNT(*) FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @DepartmentCode 
			AND CompanyCode = @CompanyCode 
			AND  CAST(CONVERT(varchar(10), HDate, 101) AS DATE) = CAST(CONVERT(varchar(10), @TempRsDateOffice, 101) AS DATE)  
		
			IF @HolidayCount > 0
			BEGIN 
				SET @IsHoliday = 1
			END
			ELSE
			BEGIN 
				SET @IsHoliday = 0
			END
		
		
			SELECT 
				@TempRsPayCode = PAYCODE,
				@TempRsDateOffice = DateOFFICE,
				@TempRsShiftAttended = SHIFTATTENDED,
				@TempMIn1 = IN1,
				@TempMIn2 = IN2,
				@TempMOut1 = OUT1,
				@TempMOut2 = OUT2,
				@TempRsTimeShift = [SHIFT]
			FROM @TempRsTime 
			WHERE RowNumber = @TempRsTimeLoopIncrementor

			
		    SET @TempMShift = @TempRsShiftAttended
					

			-- CALL AUTOSHIFT
			EXEC [dbo].[ProcessDecideShiftForHolidays] @RsTimeUpdateSSN, @TempMUShift, @IsHoliday,@TempRSEmpAuthShift, @SetupId, @TempRsDateOffice, @TempMIn1, @TempMIn2, @TempMOut1, @TempMOut2, @CompanyCode, @TempRsTimeShift
			
			SET @RsTimeUpdateLeaveAmount = 0.0
			SET @RsTimeUpdateLeaveCode = ''
			SET @RsTimeUpdateLeaveType = ''
			SET @RsTimeUpdateLeaveType1 = ''
			SET @RsTimeUpdateLeaveType2 = ''
			SET @RsTimeUpdateFirstHalfLeavecode = ''
			SET @RsTimeUpdateSecondHalfLeavecode = ''
			SET @RsTimeUpdateMIn1 = NULL
			SET @RsTimeUpdateMIn2 = NULL
			SET @RsTimeUpdateMOut1 = NULL
			SET @RsTimeUpdateMOut2 = NULL	
			SET @RsTimeUpdateMIn1Manual = ''
			SET @RsTimeUpdateMIn2Manual = ''
			SET @RsTimeUpdateMOUT1Manual = ''
			SET @RsTimeUpdateMOUT2Manual = ''


			SELECT 
				@AutoShift = SHIFTATTENDED,
				@RsTimeUpdateLeaveAmount = LEAVEAMOUNT,
				@RsTimeUpdateLeaveCode = LEAVECODE,
				@RsTimeUpdateLeaveType = LEAVETYPE,
				@RsTimeUpdateLeaveType1 = LEAVETYPE1,
				@RsTimeUpdateLeaveType2 = LEAVETYPE2,
				@RsTimeUpdateFirstHalfLeavecode = FIRSTHALFLEAVECODE,
				@RsTimeUpdateSecondHalfLeavecode = SECONDHALFLEAVECODE,
				@RsTimeUpdateMIn1 = IN1,
				@RsTimeUpdateMIn2 = IN2,
				@RsTimeUpdateMOut1 = OUT1,
				@RsTimeUpdateMOut2 = OUT2,	
				@RsTimeUpdateMIn1Manual = IN1MANNUAL,
				@RsTimeUpdateMIn2Manual = IN2MANNUAL,
				@RsTimeUpdateMOUT1Manual = OUT1MANNUAL,
				@RsTimeUpdateMOUT2Manual = OUT2MANNUAL
			FROM tbltimeregister
			WHERE SSN = @RsTimeUpdateSSN
			AND DateOFFICE = @TempRsDateOffice
					
			-- CALL UPD
			EXEC [dbo].[ProcessUPD2] @SetupId, @AutoShift, @CompanyCode, @DepartmentCode, @RsTimeUpdateSSN, @TempRsDateOffice, @RsTimeUpdateLeaveAmount, @RsTimeUpdateLeaveCode, @RsTimeUpdateLeaveType, @RsTimeUpdateLeaveType1, @RsTimeUpdateLeaveType2,@RsTimeUpdateFirstHalfLeaveCode, @RsTimeUpdateSecondHalfLeaveCode, @RsTimeUpdateMIn1, @RsTimeUpdateMIn2, @RsTimeUpdateMOut1, @RsTimeUpdateMOut2, @RsEmpUpdateIsOS, @RsEmpUpdateIsOT, @RsEmpUpdateInOnly,@RsEmpUpdateIsPunchAll,@RsEmpUpdatePermisLateArrival,@RsEmpUpdateTime,@RsEmpUpdatePermisEarlyDepart,@RsEmpUpdateIsHalfDay,@RsEmpUpdateHalf,@RsEmpUpdateIsShort,@RsEmpUpdateShort,@RsEmpUpdateIsTimeLossAllowed,@RsEmpUpdateOTRate, @RsTimeUpdateMIn1Manual, @RsTimeUpdateMIn2Manual, @RsTimeUpdateMOUT1Manual, @RsTimeUpdateMOUT2Manual
			-- UPDATE RSTIME - Not required.

			SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor + 1
			
		END
		ELSE
		BEGIN
		SET @TempRsTimeLoopIncrementor = @TempRsTimeLoopIncrementor + 1
			SELECT 
					@TempRsPayCode = PAYCODE,
					@TempRsDateOffice = DateOFFICE,
					@TempRsShiftAttended = SHIFTATTENDED,
					@TempMUShiftStartTime = SHIFTSTARTTIME,
					@TempMUShiftEndTime = SHIFTENDTIME,
					@TempMULunchStartTime = LUNCHSTARTTIME,
					@TempMULunchEndTime = LUNCHENDTIME	
			FROM @TempRsTime 
			WHERE RowNumber = @TempRsTimeLoopIncrementor
		END		
	END
END




GO
/****** Object:  StoredProcedure [dbo].[ProcessDutyRosterForCreate]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessDutyRosterForCreate] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@WO_Include VARCHAR(5),
	@SSN VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SET NOCOUNT ON;
	PRINT  ''
	PRINT 'INSIDE ProcessDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT '@WO_Include  : ' + @WO_Include
	PRINT '@SSN :' + @SSN

	DECLARE @TempCreateDate DATETIME
	DECLARE @TempMCount INT
	DECLARE @TempShiftType CHAR(1)
	DECLARE @TempPos INT
	DECLARE @TempMDate DATETIME
	DECLARE @TempMWDay CHAR(5)

	DECLARE @TempMRdays  INT
	DECLARE @TempMLShift  VARCHAR(5)
	DECLARE @TempMPat  VARCHAR(11)
	DECLARE @TempMShift  VARCHAR(5)
	DECLARE @TempMCnt  INT
	DECLARE @TempMAbsentVal  INT
	DECLARE @TempMWoVal  INT
	DECLARE @TempMSTAT VARCHAR(6)
	DECLARE @TempMSatCtr  INT

	
	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @EmpSSN VARCHAR(100)
	DECLARE @ShiftType CHAR(1)
	DECLARE @ShiftRemainDays INT
	DECLARE @SHIFTPATTERN VARCHAR(11)
	DECLARE @CDays FLOAT
	

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		EmpSSN VARCHAR(100),
		ShiftType CHAR(1),
		ShiftRemainDays INT,
		SHIFTPATTERN VARCHAR(11),
		CDays FLOAT
	)	
	
	DECLARE @StringDateTime VARCHAR(50)
	IF NOT EXISTS(SELECT * FROM  tblcalander WHERE DatePart(yy,MDate) = DatePart(yy,@FromDate))
	BEGIN
	    SET  @StringDateTime = '1/1/'+ CAST(DatePart(yy,@FromDate) as varchar)
		PRINT @StringDateTime
		SET @TempCreateDate = CAST(@StringDateTime AS datetime)
		PRINT @TempCreateDate

		WHILE(DATEPART(yy, @TempCreateDate) = DatePart(yy,@FromDate))
		BEGIN
		  PRINT @TempCreateDate
		  INSERT INTO tblCalander (mDate) VALUES (@TempCreateDate)
		  SET @TempCreateDate = DateAdd(day, 1,@TempCreateDate)
		END
	END

	INSERT INTO @EmployeeShiftDetails
	SELECT 
		EmpShift.PAYCODE, 
		Emp.DateOFJOIN,
		EmpShift.[SHIFT],
		EmpShift.ALTERNATE_OFF_DAYS,
		EmpShift.FIRSTOFFDAY,
		EmpShift.SECONDOFFTYPE,
		EmpShift.HALFDAYSHIFT,
		EmpShift.SECONDOFFDAY,
		Emp.SSN,
		EmpShift.ShiftType,
		EmpShift.ShiftRemainDays,
		EmpShift.SHIFTPATTERN,
		EmpShift.CDays
	FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
	INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
	WHERE Emp.active = 'Y' AND Emp.SSN = @SSN

    SELECT 
		@TempPayCode = PayCode,
		@DateOfJoin = DateOfJoin,
		@Shift = [Shift],
		@AlternateOffDays = AlternateOffDays,
		@FirstOffDay = FirstOffDay,
		@SecondOffType = SecondOffType,
		@HalfDayShift = HalfDayShift,
		@SecondOffDay = SecondOffDay,
		@EmpSSN = EmpSSN,
		@ShiftType =  ShiftType,
		@ShiftRemainDays = ShiftRemainDays,
		@SHIFTPATTERN = SHIFTPATTERN,
		@CDays = CDays
	 FROM @EmployeeShiftDetails

	 SET @TempMSatCtr = 0
	 set @FromDate=@DateOfJoin
	-- SET TempMCount = 0 . This is not required but kept as it is used in vb code. 
	SET @TempMCount=0
	IF @TempMCount <= 0 
	BEGIN
		SET @TempMRdays = 7
		SET @TempMLShift = @Shift
		SET @TempShiftType = @ShiftType

		IF @TempShiftType = 'R'
		BEGIN
			SET  @TempMRdays = @ShiftRemainDays  
			SET  @TempMPat = @SHIFTPATTERN  
		END
		ELSE IF @TempShiftType = 'F'
		BEGIN
			IF len(@TempMLShift) = 1
			BEGIN 
				SET  @TempMPat = '  '+ @TempMLShift + ',' +'  '+  @TempMLShift + ',' + '  '+ @TempMLShift
			END
			ELSE IF len(@TempMLShift) = 2
			BEGIN
				SET @TempMPat = ' '+ @TempMLShift + ',' +' '+  @TempMLShift + ',' + ' '+ @TempMLShift
			END
			ELSE
			BEGIN
				SET @TempMPat = @TempMLShift + ',' + @TempMLShift + ',' +@TempMLShift
			END
		END
		ELSE
		BEGIN
			SET @TempMPat = 'IGN,IGN,IGN'
			SET @TempMLShift = 'IGN'
		END

		SET @TempPos = CHARINDEX(@TempMLShift,@TempMPat)

		IF @TempPos <= 0
		BEGIN
			SET @TempPos = 1
		ENd
		
		SET @TempMDate = @FromDate

		WHILE(DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
		BEGIN
			WHILE( @TempMRdays > 0 AND DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
			BEGIN
				IF(	DATEPART(DD, @TempMDate) = 1)
				BEGIN
					SET @TempMSatCtr = 0
				END

				SET @TempMWDay = UPPER(FORMAT(@TempMDate,'ddd'))
				IF @TempMWDay  = @FirstOffDay 
				BEGIN
					SET @TempMShift = 'OFF'
					IF @WO_Include != 'Y'
					BEGIN
						SET @TempMRdays = @TempMRdays + 1
					END
				END
				ELSE IF  @TempMWDay = @SecondOffDay
				BEGIN
					SET @TempMSatCtr= @TempMSatCtr + 1
					IF (CHARINDEX (CAST(@TempMSatCtr AS VARCHAR), @AlternateOffDays) > 0 )
					BEGIN
						IF @SecondOffType = 'H' 
						BEGIN
							SET @TempMShift  = @HalfDayShift
						END
						ELSE
						BEGIN
							SET @TempMShift  = 'OFF'
						END
				    
						IF @WO_Include != 'Y' 
						BEGIN
							SET @TempMRdays = @TempMRdays + 1
						END	                        
					END 
					ELSE
					BEGIN
						SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
					END 
				END
				ELSE
				BEGIN
					SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
				END      
			
			    IF @TempMShift = 'OFF' 
				BEGIN
					SET @TempMSTAT = 'WO'
					SET @TempMWoVal = 1
					SET @TempMAbsentVal = 0
				END
				ELSE
				BEGIN
					SET @TempMSTAT = 'A'
					SET @TempMWoVal = 0
					SET @TempMAbsentVal = 1				
				END

				INSERT INTO TBLTIMEREGISTER
					(
						Paycode,
						DateOffice,
						[Shift],
						ShiftAttended,
						[Status],
						WO_Value,
						AbsentValue,
						presentvalue,
						leavevalue,
						HOLIDAY_VALUE,
						HOURSWORKED,
						OtDuration,
						OTAmount,
						SSN
					)
				VALUES
					(
						@PayCode,
						@TempMDate,
						@TempMShift,
						@TempMShift,
						@TempMSTAT,
						@TempMWoVal,
						@TempMAbsentVal,
						0,
						0,
						0,
						0,
						0,
						0,
						@SSN
					)

					SET @TempMRdays = @TempMRdays - 1
					SET @TempMDate = DateAdd(day, 1,@TempMDate)				
			END

			SET @TempMRdays= @CDays
			IF @TempMRdays <= 0 
			BEGIN
				SET @TempMRdays = 7
			END

			SET @TempPos = @TempPos + 4

			IF (@TempPos > LEN(@TempMPat))
			BEGIN
				SET @TempPos = 1
			END

			IF SUBSTRING(@TempMPat, @TempPos, 3) = '   ' 
			BEGIN
				SET @TempPos = 1
			END                   
		END
	END  	
END





GO
/****** Object:  StoredProcedure [dbo].[ProcessDutyRosterForUpdate]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessDutyRosterForUpdate] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@WO_Include VARCHAR(5),
	@SSN VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SET NOCOUNT ON;
	PRINT  ''
	PRINT 'INSIDE ProcessDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT '@WO_Include  : ' + @WO_Include
	PRINT '@SSN :' + @SSN

	DECLARE @TempCreateDate DATETIME
	DECLARE @TempMCount INT
	DECLARE @TempShiftType CHAR(1)
	DECLARE @TempPos INT
	DECLARE @TempMDate DATETIME
	DECLARE @TempMWDay CHAR(5)

	DECLARE @TempMRdays  INT
	DECLARE @TempMLShift  VARCHAR(5)
	DECLARE @TempMPat  VARCHAR(11)
	DECLARE @TempMShift  VARCHAR(5)
	DECLARE @TempMCnt  INT
	DECLARE @TempMAbsentVal  INT
	DECLARE @TempMWoVal  INT
	DECLARE @TempMSTAT VARCHAR(6)
	DECLARE @TempMSatCtr  INT

	
	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @EmpSSN VARCHAR(100)
	DECLARE @ShiftType CHAR(1)
	DECLARE @ShiftRemainDays INT
	DECLARE @SHIFTPATTERN VARCHAR(11)
	DECLARE @CDays FLOAT
	

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		EmpSSN VARCHAR(100),
		ShiftType CHAR(1),
		ShiftRemainDays INT,
		SHIFTPATTERN VARCHAR(11),
		CDays FLOAT
	)	
	
	DECLARE @StringDateTime VARCHAR(50)
	IF NOT EXISTS(SELECT * FROM  tblcalander WHERE DatePart(yy,MDate) = DatePart(yy,@FromDate))
	BEGIN
	    SET  @StringDateTime = '1/1/'+ CAST(DatePart(yy,@FromDate) as varchar)
		PRINT @StringDateTime
		SET @TempCreateDate = CAST(@StringDateTime AS datetime)
		PRINT @TempCreateDate

		WHILE(DATEPART(yy, @TempCreateDate) = DatePart(yy,@FromDate))
		BEGIN
		  PRINT @TempCreateDate
		  INSERT INTO tblCalander (mDate) VALUES (@TempCreateDate)
		  SET @TempCreateDate = DateAdd(day, 1,@TempCreateDate)
		END
	END

	INSERT INTO @EmployeeShiftDetails
	SELECT 
		EmpShift.PAYCODE, 
		Emp.DateOFJOIN,
		EmpShift.[SHIFT],
		EmpShift.ALTERNATE_OFF_DAYS,
		EmpShift.FIRSTOFFDAY,
		EmpShift.SECONDOFFTYPE,
		EmpShift.HALFDAYSHIFT,
		EmpShift.SECONDOFFDAY,
		Emp.SSN,
		EmpShift.ShiftType,
		EmpShift.ShiftRemainDays,
		EmpShift.SHIFTPATTERN,
		EmpShift.CDays
	FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
	INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
	WHERE Emp.active = 'Y' AND Emp.SSN = @SSN

    SELECT 
		@TempPayCode = PayCode,
		@DateOfJoin = DateOfJoin,
		@Shift = [Shift],
		@AlternateOffDays = AlternateOffDays,
		@FirstOffDay = FirstOffDay,
		@SecondOffType = SecondOffType,
		@HalfDayShift = HalfDayShift,
		@SecondOffDay = SecondOffDay,
		@EmpSSN = EmpSSN,
		@ShiftType =  ShiftType,
		@ShiftRemainDays = ShiftRemainDays,
		@SHIFTPATTERN = SHIFTPATTERN,
		@CDays = CDays
	 FROM @EmployeeShiftDetails

	 SET @TempMSatCtr = 0
	-- SET TempMCount = 0 . This is not required but kept as it is used in vb code. 
	SET @TempMCount=0
	IF @TempMCount <= 0 
	BEGIN
		SET @TempMRdays = 7
		SET @TempMLShift = @Shift
		SET @TempShiftType = @ShiftType

		IF @TempShiftType = 'R'
		BEGIN
			SET  @TempMRdays = @ShiftRemainDays  
			SET  @TempMPat = @SHIFTPATTERN  
		END
		ELSE IF @TempShiftType = 'F'
		BEGIN
			IF len(@TempMLShift) = 1
			BEGIN 
				SET  @TempMPat = '  '+ @TempMLShift + ',' +'  '+  @TempMLShift + ',' + '  '+ @TempMLShift
			END
			ELSE IF len(@TempMLShift) = 2
			BEGIN
				SET @TempMPat = ' '+ @TempMLShift + ',' +' '+  @TempMLShift + ',' + ' '+ @TempMLShift
			END
			ELSE
			BEGIN
				SET @TempMPat = @TempMLShift + ',' + @TempMLShift + ',' +@TempMLShift
			END
		END
		ELSE
		BEGIN
			SET @TempMPat = 'IGN,IGN,IGN'
			SET @TempMLShift = 'IGN'
		END

		SET @TempPos = CHARINDEX(@TempMLShift,@TempMPat)

		IF @TempPos <= 0
		BEGIN
			SET @TempPos = 1
		ENd
		
		SET @TempMDate = @FromDate

		WHILE(DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
		BEGIN
			WHILE( @TempMRdays > 0 AND DATEPART(yy, @TempMDate) = DatePart(yy,@FromDate))
			BEGIN
				IF(	DATEPART(DD, @TempMDate) = 1)
				BEGIN
					SET @TempMSatCtr = 0
				END

				SET @TempMWDay = UPPER(FORMAT(@TempMDate,'ddd'))
				IF @TempMWDay  = @FirstOffDay 
				BEGIN
					SET @TempMShift = 'OFF'
					IF @WO_Include != 'Y'
					BEGIN
						SET @TempMRdays = @TempMRdays + 1
					END
				END
				ELSE IF  @TempMWDay = @SecondOffDay
				BEGIN
					SET @TempMSatCtr= @TempMSatCtr + 1
					IF (CHARINDEX (CAST(@TempMSatCtr AS VARCHAR), @AlternateOffDays) > 0 )
					BEGIN
						IF @SecondOffType = 'H' 
						BEGIN
							SET @TempMShift  = @HalfDayShift
						END
						ELSE
						BEGIN
							SET @TempMShift  = 'OFF'
						END
				    
						IF @WO_Include != 'Y' 
						BEGIN
							SET @TempMRdays = @TempMRdays + 1
						END	                        
					END 
					ELSE
					BEGIN
						SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
					END 
				END
				ELSE
				BEGIN
					SET @TempMShift = SUBSTRING(@TempMPat, @TempPos,3)
				END      
			
			    IF @TempMShift = 'OFF' 
				BEGIN
					SET @TempMSTAT = 'WO'
					SET @TempMWoVal = 1
					SET @TempMAbsentVal = 0
				END
				ELSE
				BEGIN
					SET @TempMSTAT = 'A'
					SET @TempMWoVal = 0
					SET @TempMAbsentVal = 1				
				END

				UPDATE TBLTIMEREGISTER
				SET 
					[Shift]= @TempMShift,
					ShiftAttended=@TempMShift,
					[Status]=@TempMSTAT,
					WO_Value=@TempMWoVal,
					AbsentValue=@TempMAbsentVal,
					LEAVEVALUE=0,
					HOLIDAY_VALUE=0,
					HOURSWORKED=0,
					OtDuration=0,
					OTAmount=0
				WHERE SSN = @SSN and   Dateoffice = @TempMDate

				SET @TempMRdays = @TempMRdays - 1
				SET @TempMDate = DateAdd(day, 1,@TempMDate)				
			END

			SET @TempMRdays= @CDays
			IF @TempMRdays <= 0 
			BEGIN
				SET @TempMRdays = 7
			END

			SET @TempPos = @TempPos + 4

			IF (@TempPos > LEN(@TempMPat))
			BEGIN
				SET @TempPos = 1
			END

			IF SUBSTRING(@TempMPat, @TempPos, 3) = '   ' 
			BEGIN
				SET @TempPos = 1
			END                   
		END
	END  	
END





GO
/****** Object:  StoredProcedure [dbo].[ProcessGetShiftForEmployee]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessGetShiftForEmployee]
    @Shiftattended VARCHAR(11),
	@DateOffice DateTime,
	@SetupId INT,
	@AuthShifts VARCHAR(15),
	@DepartmentCode VARCHAR(10),
	@CompanyCode VARCHAR(10),
	@ShiftName VARCHAR(15) OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @TempGAutoShiftLow INT
	DECLARE @TempGAutoShiftUp INT

	IF @Shiftattended = 'OFF'
	BEGIN
	  SET @ShiftName =  @Shiftattended
	  RETURN @ShiftName
	END


		IF EXISTS(		SELECT * FROM Holiday(nolock) 
			WHERE DEPARTMENTCODE = @DepartmentCode 
			AND @CompanyCode = @CompanyCode 
			AND HDate = @DateOffice )

		BEGIN
			SET @ShiftName =  @Shiftattended
			RETURN @ShiftName
		END

		SELECT 
			@TempGAutoShiftLow = AUTOSHIFT_LOW, 
			@TempGAutoShiftUp = AUTOSHIFT_UP
	    FROM tblsetup 
		WHERE  SETUPID = (
							  SELECT Max(Convert(int,Setupid)) 
							  FROM tblsetup 
							  WHERE CompanyCode = @CompanyCode
						  )


		
END






GO
/****** Object:  StoredProcedure [dbo].[ProcessPostLeaveThruReason]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessPostLeaveThruReason]
@ReasonCode VARCHAR(100),
@StrFirstPunchReasonCode VARCHAR(100),
@StrLastPunchReasonCode VARCHAR(100),
@PayCode  VARCHAR(10),
@DateOffice DATETIME

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempMLeaveCode CHAR(3)
	DECLARE @TempMLeaveAmount FLOAT
	DECLARE @TempMLeaveType CHAR(1)
	DECLARE @TempMLeaveField CHAR(3)
	DECLARE @TempReasonCount INT
	DECLARE @TempLeaveCount INT
	DECLARE @SQLQuery VARCHAR(250)


	DECLARE @TempReasonCode CHAR(3)
	DECLARE @TempLeaveCode CHAR(3)
	DECLARE @TempLeaveAmount FLOAT
	DECLARE @TempReasonMaster TABLE
	(
		ReasonCode CHAR(3),
		[Description] CHAR(45),
		LeaveCode CHAR(3),
		LeaveValue FLOAT,
		Late CHAR(1),
		Early CHAR(1),
		ExcLunchHours CHAR(1),
		HoursWorked CHAR(1)
	)

	DECLARE @TempLeaveMaster TABLE
	(
		LeaveField CHAR(3),
		LeaveCode CHAR(3),		
		LeaveType CHAr(1)
	)

	INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @ReasonCode

    SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

	IF @TempReasonCount > 0
	BEGIN 
		SELECT TOP 1
			@TempReasonCode = ReasonCode,
			@TempLeaveAmount = LeaveValue
		FROM @TempReasonMaster

		IF (LEN(RTRIM(LTRIM(@TempReasonCode)))) <= 3 AND (LEN(RTRIM(LTRIM(@TempReasonCode)))) > 0
		BEGIN 
			INSERT INTO @TempLeaveMaster
				SELECT LEAVEFIELD, LEAVECODE, LEAVETYPE 
				FROM tblleavemaster(NOLOCK) WHERE LEAVECODE = @ReasonCode

			SELECT  @TempLeaveCount =  COUNT(*) FROM @TempLeaveMaster

			IF @TempLeaveCount > 0
			BEGIN 
				SELECT TOP 1 
					@TempMLeaveCode = LeaveCode,
					@TempMLeaveAmount = @TempLeaveAmount,
					@TempMLeaveField = LeaveField				
				FROM @TempLeaveMaster
				
				-- TODO : Check with Virendra Sir

				SET @SQLQuery = 'Update tblLeaveLedger '+@TempMLeaveField + ' = ' + @TempMLeaveAmount  + ' WHERE PayCode ='+@PayCode 
				EXECUTE(@SQLQuery)	

				-- TODO : Check for RTC, NonRTC
				UPDATE tbltimeregister 
				SET [STATUS] = @TempMLeaveCode,
					LEAVETYPE = @TempMLeaveType,
					LEAVECODE = @TempMLeaveCode,
					LEAVEAMOUNT = @TempMLeaveAmount

				WHERE PAYCODE = @PayCode
				AND DateOFFICE = @DateOffice
		

			END
		END
	END

	IF @StrFirstPunchReasonCode != ''
	BEGIN
		DELETE FROM @TempReasonMaster
		INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @StrFirstPunchReasonCode

		SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

		IF @TempReasonCount = 1
		BEGIN 
			SELECT TOP 1
				@TempReasonCode = ReasonCode,
				@TempLeaveAmount = LeaveValue,
				@TempLeaveCode = ISNULL(LeaveCode,'')
			FROM @TempReasonMaster

			IF @TempLeaveCode != '' AND (LEN(LTRIM(RTRIM(@TempLeaveCode)))) > 0
			BEGIN
				UPDATE tbltimeregister 
				SET LATEARRIVAL = 0
				WHERE PAYCODE = @PayCode
				AND DateOFFICE = @DateOffice
			END 
		END
	END

	IF @StrLastPunchReasonCode != ''
	BEGIN
		DELETE FROM @TempReasonMaster
		INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @StrLastPunchReasonCode

		SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

		IF @TempReasonCount = 1
		BEGIN 
			SELECT TOP 1
				@TempReasonCode = ReasonCode,
				@TempLeaveAmount = LeaveValue,
				@TempLeaveCode = ISNULL(LeaveCode,'')
			FROM @TempReasonMaster

			IF @TempLeaveCode != '' AND (LEN(LTRIM(RTRIM(@TempLeaveCode)))) > 0
			BEGIN
				UPDATE tbltimeregister 
				SET EARLYDEPARTURE = 0
				WHERE PAYCODE = @PayCode
				AND DateOFFICE = @DateOffice
			END 
		END
	END
END







GO
/****** Object:  StoredProcedure [dbo].[ProcessRTCAll]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessRTCAll]
	@FromDate DATETIME,
	@CompanyCode VARCHAR(10),
	@PayCode VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

END






GO
/****** Object:  StoredProcedure [dbo].[ProcessUPD2]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessUPD2]
 @SetupId VARCHAR(50),
 @ShiftAttended VARCHAR(3),
 @CompanyCode VARCHAR(10),
 @DepartmentCode VARCHAR(10),
 
 @SSN VARCHAR(100),
 @DateOfOffice DATETIME,
 @LeaveAmount FLOAT,
 @LeaveCode CHAR(3),
 @LeaveType CHAR(1),
 @LeaveType1 CHAR(1),
 @LeaveType2 CHAR(1),
 @FirstHalfLeavecode CHAR(3),
 @SecondHalfLeavecode CHAR(3),
 @MIn1 DATETIME,
 @MIn2 DATETIME,
 @MOut1 DATETIME,
 @MOut2 DATETIME,
 @IsOS Char(1),
 @IsOT Char(1),
 @InOnly Char(1),
 @IsPunchAll Char(1),
 @PermisLateArrival INT,
 @Time INT,
 @PermisEarlyDepart INT,
 @IsHalfDay CHAR(1),
 @Half INT,
 @IsShort CHAR(1), 
 @Short INT,
 @IsTimeLossAllowed CHAR(1),
 @OTRate FLOAT,
 @MIn1Manual CHAR(1),
 @MIn2Manual CHAR(1),
 @MOUT1Manual CHAR(1),
 @MOUT2Manual CHAR(1),
 @MIS CHAR(1),
 @ISOTONOFF CHAR(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempMUStatus CHAR(6)
	DECLARE @TempMUHoursworked INT
	DECLARE @TempMUExcLunchHours INT
	DECLARE @TempMUOtDuration INT
	DECLARE @TempMUOSDuration INT
	DECLARE @TempMUOtAmount FLOAT
	DECLARE @TempMUEarlyArrival INT
	DECLARE @TempMUEarlyDeparture INT
	DECLARE @TempMULateArrival INT
	DECLARE @TempMULunchEarlyDeparture INT
	DECLARE @TempMULunchLateArrival INT
	DECLARE @TempMUTotalLossHRS INT
	DECLARE @TempMULeaveValue FLOAT
	DECLARE @TempMUPresentValue FLOAT
	DECLARE @TempMUAbsentValue FLOAT
	DECLARE @TempMUHolidayValue FLOAT
	DECLARE @TempMUWoValue FLOAT
	DECLARE @TempMUOutWorkDuration INT
	DECLARE @TempMUFlag CHAR(4)

	DECLARE @TempMLate INT
	DECLARE @TempMEarly INT
	DECLARE @TempMLate1 INT
	DECLARE @TempMEarly1 INT
	DECLARE @TempMShiftTime INT
	DECLARE @TempMAbsHours INT
	
	DECLARE @TempMOTInHours FLOAT
	DECLARE @TempMOTFactor FLOAT
	DECLARE @TempMLDed INT
	DECLARE @TempMLDur INT
	DECLARE @TempMLunchDur INT
	DECLARE @TempMOT INT
	DECLARE @TempMMinutes INT

	DECLARE @TempMOtStartAfter INT
	DECLARE @TempMOtDeductAfter INT
	DECLARE @TempMOtDeductHours INT

	-- Required for Processing
	DECLARE @TempMUShiftStartTime DATETIME
	DECLARE @TempMUShiftEndTime DATETIME

	DECLARE @TempShiftStartTime DATETIME
	DECLARE @TempShiftEndTime DATETIME
	DECLARE @TempShiftName VARCHAR(3)

	DECLARE @TempStartTime TIME
	DECLARE @TempEndTime TIME
	DECLARE @TempDate DATE
	DECLARE @TempShiftStartDateTime DATETIME
	DECLARE @TempShiftEndDateTime DATETIME
	DECLARE @Minutes INT

	DECLARE @TempLunchStartTime DATETIME
	DECLARE @TempLunchStartDateTime DATETIME
	DECLARE @TempLunchEndTime DATETIME
	DECLARE @TempLunchEndDateTime DATETIME
	DECLARE @TempLunchDeduction INT
	DECLARE @TempShiftPosition CHAR(7)

	-- TBLSetup value 
	DECLARE @TempGIsPresentOnWOPresent CHAR(1)
	DECLARE @TempGIsPresentOnHLDPresent CHAR(1)	              
	DECLARE @TempGWoOT INT   -- [DEDUCTWOOT]
	DECLARE @TempGHldOT INT  -- [DEDUCTHOLIDAYOT]
	DECLARE @TempGISOTOUTMINUSSHIFTENDTIME CHAR(1)
	DECLARE @TempGISOTWRKGHRSMINUSSHIFTHRS CHAR (1)
	DECLARE @TempGOTFormulae INT -- [ISOTOUTMINUSSHIFTENDTIME]
	DECLARE @TempGOTEarly INT  -- [OTEARLYDUR]
	DECLARE @TempGOTLate INT -- [OTLATECOMINGDUR]
	DECLARE @TempGOTEnd INT -- [OTRESTRICTENDDUR]
	DECLARE @TempGIsOTMinus CHAR(1) -- [OwMinus] TODO : Check with Virendra Sir
	DECLARE @TempGOTRound CHAr(1)  -- [OTROUND]
	
	DECLARE @IsHoliday BIT
	DECLARE @HolidayCount INT

	
	SET @TempMUStatus = 'A'
	SET @TempMUHoursworked = 0
	SET @TempMUExcLunchHours = 0
	SET @TempMUOtDuration = 0
	SET @TempMUOSDuration = 0
	SET @TempMUOtAmount = 0
	SET @TempMUEarlyArrival = 0
	SET @TempMUEarlyDeparture = 0
	SET @TempMULateArrival = 0
	SET @TempMULunchEarlyDeparture = 0
	SET @TempMULunchLateArrival = 0
	SET @TempMUTotalLossHRS = 0
	SET @TempMULeaveValue = 0
	SET @TempMUPresentValue = 0
	SET @TempMUAbsentValue = 0
	SET @TempMUHolidayValue = 0
	SET @TempMUWoValue = 0
	SET @TempMUOutWorkDuration = 0
	SET @TempMUFlag = ''


	SELECT 
		@TempGIsPresentOnWOPresent = ISPRESENTONWOPRESENT,
		@TempGIsPresentOnHLDPresent = ISPRESENTONHLDPRESENT,
		@TempGWoOT = DEDUCTWOOT,
		@TempGHldOT = DEDUCTHOLIDAYOT,
		@TempGISOTOUTMINUSSHIFTENDTIME = ISOTOUTMINUSSHIFTENDTIME,
		@TempGISOTWRKGHRSMINUSSHIFTHRS = ISOTWRKGHRSMINUSSHIFTHRS,
		@TempGOTLate = OTLATECOMINGDUR,
		@TempGOTEarly = OTEARLYDUR,
		@TempGOTEnd = OTRESTRICTENDDUR,
		@TempGOTRound = OTROUND,
		@TempGIsOTMinus = 'N'
    FROM TBLEMPLOYEESHIFTMASTER (nolock)
	WHERE SSN = @SetupId 

	IF @TempGISOTOUTMINUSSHIFTENDTIME = 'Y'
	BEGIN
		SET @TempGOTFormulae = 1
	END
	ELSE IF @TempGISOTWRKGHRSMINUSSHIFTHRS = 'Y'
	BEGIN
		SET @TempGOTFormulae = 2
	END
	ELSE
	BEGIN
		SET @TempGOTFormulae = 3
	END

	-- TODO : Check with Virendra Sir if it is needed. 
	IF @TempGOTFormulae != 3
	BEGIN
		SET @TempGOTLate = 0
		SET @TempGOTEarly = 0
		SET @TempGOTEnd = 0
	END

	SELECT @HolidayCount = COUNT(*) FROM Holiday(nolock) 
	WHERE DEPARTMENTCODE = @DepartmentCode 
	AND CompanyCode = @CompanyCode 
	AND  CAST(CONVERT(varchar(10), HDate, 101) AS DATE) = CAST(CONVERT(varchar(10), @DateOfOffice, 101) AS DATE)  
		
	IF @HolidayCount > 0
	BEGIN 
		SET @IsHoliday = 1
	END
	ELSE
	BEGIN 
		SET @IsHoliday = 0
	END

	-- SET Shift StartTime 
	IF @ShiftAttended != 'OFF' AND @ShiftAttended!= 'IGN'
	BEGIN
		SET @TempShiftName=''
		SELECT 
			@TempShiftName=[SHIFT],
			@TempShiftStartTime = StartTime,
			@TempShiftEndTime = ENDTIME,
			@TempLunchStartTime = LUNCHTIME,
			@TempLunchEndTime = LUNCHENDTIME,
			@TempLunchDeduction = LUNCHDEDUCTION,
			@TempShiftPosition = SHIFTPOSITION,
			@TempMOtStartAfter = OTSTARTAFTER,
			@TempMOtDeductAfter = OTDEDUCTAFTER,
			@TempMOtDeductHours = OTDEDUCTHRS
		FROM [dbo].[tblShiftMaster] (nolock)
		WHERE [SHIFT]=@ShiftAttended
		AND CompanyCode = @CompanyCode


		IF @TempShiftName = '' 
		BEGIN
		-- Log message that shift is not a valid shift for 
			RETURN 
		END

		SET @TempDate = CAST(@DateOfOffice AS date)

		SET @TempShiftStartDateTime = CAST(@TempDate AS DATETIME)
		SET @TempShiftEndDateTime = CAST(@TempDate AS DATETIME)

		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempShiftStartTime), 0), @TempShiftStartTime)
		SET @TempShiftStartDateTime = DATEADD(HH, @Minutes/60, @TempShiftStartDateTime)
		SET @TempShiftStartDateTime = DATEADD(MINUTE, @Minutes%60,@TempShiftStartDateTime)

		SET @TempShiftEndDateTime = CAST(@TempDate AS DATETIME)

		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempShiftEndTime), 0), @TempShiftEndTime)
		SET @TempShiftEndDateTime = DATEADD(HH, @Minutes/60, @TempShiftEndDateTime)
		SET @TempShiftEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempShiftEndDateTime)

		IF DATEDIFF(N, @TempShiftStartDateTime, @TempShiftEndDateTime) < 0
		BEGIN 
		    SET @TempDate = CAST (@TempShiftEndDateTime AS DATE)
			SET @TempDate = DATEADD(D, 1, @TempDate)
			
			SET @TempShiftEndDateTime = CAST(@TempDate AS DATETIME)
			SET @TempShiftEndDateTime = DATEADD(HH, @Minutes/60, @TempShiftEndDateTime)
			SET @TempShiftEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempShiftEndDateTime)
		END

		SET @TempDate = CAST(@DateOfOffice AS date)
		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempLunchStartTime), 0), @TempLunchStartTime) 

		SET @TempLunchStartDateTime = CAST(@TempDate AS DATETIME)
		SET @TempLunchStartDateTime = DATEADD(HH, @Minutes/60, @TempLunchStartDateTime)
		SET @TempLunchStartDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchStartDateTime)

		IF DATEDIFF(N, @TempShiftStartDateTime, @TempLunchStartDateTime) < 0
		BEGIN
			SET @TempDate = CAST (@TempLunchStartDateTime AS DATE)
			SET @TempDate = DATEADD(D, 1, @TempDate)
			
			SET @TempLunchStartDateTime = CAST(@TempDate AS DATETIME)
			SET @TempLunchStartDateTime = DATEADD(HH, @Minutes/60, @TempLunchStartDateTime)
			SET @TempLunchStartDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchStartDateTime)
		END


		SET @TempDate = CAST(@DateOfOffice AS date)
		SET @Minutes = DATEDIFF(MINUTE, DATEADD(DAY, DATEDIFF(DAY, 0, @TempLunchEndTime), 0), @TempLunchEndTime) 

		SET @TempLunchEndDateTime = CAST(@TempDate AS DATETIME)
		SET @TempLunchEndDateTime = DATEADD(HH, @Minutes/60, @TempLunchEndDateTime)
		SET @TempLunchEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchEndDateTime)

		IF DATEDIFF(N, @TempShiftStartDateTime, @TempLunchEndDateTime) < 0
		BEGIN
			SET @TempDate = CAST (@TempLunchEndDateTime AS DATE)
			SET @TempDate = DATEADD(D, 1, @TempDate)
			
			SET @TempLunchEndDateTime = CAST(@TempDate AS DATETIME)
			SET @TempLunchEndDateTime = DATEADD(HH, @Minutes/60, @TempLunchEndDateTime)
			SET @TempLunchEndDateTime = DATEADD(MINUTE, @Minutes%60,@TempLunchEndDateTime)
		END

		SET @TempMLDur = DATEDIFF(N, @TempLunchStartDateTime, @TempLunchEndDateTime)
		SET @TempMLunchDur = @TempMLDur

		SET @TempMLDed = @TempLunchDeduction
		SET @TempMShiftTime = DATEDIFF(N, @TempShiftStartDateTime, @TempShiftEndDateTime) - @TempMLDur
	END


	IF @LeaveAmount > 0
	BEGIN 
		IF @LeaveAmount = 0.25 
		BEGIN 
			SET @TempMUStatus = 'Q_'
		END
		ELSE IF @LeaveAmount = 0.5 
		BEGIN 
			SET @TempMUStatus = 'H_'
		END
		ELSE IF @LeaveAmount = 0.75 
		BEGIN 
			SET @TempMUStatus = 'T_'
		END
		ELSE
		BEGIN
			SET @TempMUStatus = ''
		END

		SET @TempMUStatus = LTRIM(RTRIM(@TempMUStatus)) + @LeaveCode

		IF ISDATE(@MIn1) = 1
		BEGIN
			IF @ShiftAttended != 'IGN' AND @ShiftAttended != 'OFF' AND @IsHoliday=0
			BEGIN
				SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
				IF @TempMEarly < 0
				BEGIN
					SET @TempMEarly = 0
				END
			END

			IF ISDATE(@MOut2) = 1
			BEGIN
				SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
				--TODO : Check with Virendra sir for condition
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					IF @TempMLDur < @TempMLDed 
					BEGIN
						SET @TempMLDur = @TempMLDed
					ENd
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END 
				ELSE
				BEGIN
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDed
				END
			END

			IF @LeaveType = 'P'
			BEGIN
				SET @TempMUPresentValue = 1
			END
			ELSE IF @LeaveType = 'L'
			BEGIN 
				SET @TempMULeaveValue = 0 + @LeaveAmount
				SET @TempMUPresentValue = 1 - @LeaveAmount
			END
			ELSE 
			BEGIN 
				SET @TempMUAbsentValue = 0 + @LeaveAmount
				SET @TempMUPresentValue = 1 - @LeaveAmount
			END
		END
		ELSE
		BEGIN
		    SET @LeaveType1 = ISNULL(@LeaveType1,'')
			SET @LeaveType2 = ISNULL(@LeaveType2,'')
			IF @LeaveType1 != '' AND @LeaveType2 != ''
			BEGIN
				SET @TempMUStatus = LTRIM(RTRIM(@FirstHalfLeavecode)) + LTRIM(RTRIM(@SecondHalfLeavecode))

				IF (@LeaveType1 = 'P' AND @LeaveType2 = 'A') OR (@LeaveType1 = 'A' AND @LeaveType2 = 'P')
				BEGIN
					SET @TempMUPresentValue = 0.5
					SET @TempMULeaveValue = 0
					SET @TempMUAbsentValue = 0.5
				END
				IF (@LeaveType1 = 'L' AND @LeaveType2 = 'P') OR (@LeaveType1 = 'P' AND @LeaveType2 = 'L')
				BEGIN
					SET @TempMUPresentValue = 0.5
					SET @TempMULeaveValue = 0.5
					SET @TempMUAbsentValue = 0
				END
				IF (@LeaveType1 = 'L' AND @LeaveType2 = 'A') OR (@LeaveType1 = 'A' AND @LeaveType2 = 'L')
				BEGIN
					SET @TempMUPresentValue = 0
					SET @TempMULeaveValue = 0.5
					SET @TempMUAbsentValue = 0.5
				END
				IF @LeaveType1 = 'L' AND @LeaveType2 = 'L'
				BEGIN
					SET @TempMUPresentValue = 0
					SET @TempMULeaveValue = 1
					SET @TempMUAbsentValue = 0					
				END
				IF @LeaveType1 = 'A' AND @LeaveType2 = 'A'
				BEGIN
					SET @TempMUPresentValue = 0
					SET @TempMULeaveValue = 0
					SET @TempMUAbsentValue = 1					
				END
				IF @LeaveType1 = 'P' AND @LeaveType2 = 'P'
				BEGIN
					SET @TempMUPresentValue = 1
					SET @TempMULeaveValue = 0
					SET @TempMUAbsentValue = 0					
				END
			END
			ELSE
			BEGIN
				IF @LeaveType = 'P'
				BEGIN
					SET @TempMUPresentValue = @LeaveAmount
				END
				ELSE IF @LeaveType = 'L'
				BEGIN
					SET @TempMULeaveValue = @LeaveAmount
				END
				ELSE IF @LeaveType = 'A' 
				BEGIN
					SET @TempMUAbsentValue = @LeaveAmount
				END

				IF @ShiftAttended = 'OFF'
				BEGIN 
					SET @TempMUWoValue = 1 - @LeaveAmount
				END
				ELSE IF @IsHoliday = 1
				BEGIN
					SET @TempMUHolidayValue = 1 - @LeaveAmount
				END
				ELSE IF SUBSTRING(@TempShiftPosition,1,1) = 'H'
				BEGIN
					SET @TempMUWoValue = 0.5
				END
				ELSE
				BEGIN
				    -- TODO : Check Bang Operator  mTime!LEAVETYPE <> "A" : Line NUmber 2188 in VB code in Module 1 
					IF @LeaveType != 'A' 
					BEGIN
						SET @TempMUAbsentValue = 1 - @LeaveAmount 
					END
				END
			END
		END
		GOTO Lastl
	END

	IF @ShiftAttended = 'OFF' AND @IsHoliday = 1
	BEGIN
		SET @TempMUHolidayValue = 1
		SET @TempMUStatus = 'WOH'

		IF ISDATE(@MIn1) = 1
		BEGIN
			SET @TempMUStatus = 'PWH'
			IF @TempGIsPresentOnWOPresent = 'Y' OR @TempGIsPresentOnHLDPresent = 'Y'
			BEGIN
				SET @TempMUPresentValue = 1
				SEt @TempMUHolidayValue = 0
			END

			IF ISDATE(@Mout2) = 1
			BEGIN
				SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END

				IF @IsOS = 'Y'
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked
				END

				IF @IsOT = 'Y' 
				BEGIN
					SET @TempMUOtDuration = @TempMUHoursworked
				END


			END			
		END
		IF @ISOTONOFF='C'
		BEGIN
			SET @TempMUOtDuration =0
		END
			
		GOTO Lastl
	END

	IF @ShiftAttended = 'OFF'
	BEGIN
		SET @TempMUWoValue = 1
		SET @TempMUStatus = 'WO'

		IF ISDATE(@MIn1) = 1
		BEGIN
			SET @TempMUStatus = 'POW'
			IF @TempGIsPresentOnWOPresent = 'Y' 
			BEGIN
				SET @TempMUWoValue = 0
				SET @TempMUPresentValue = 1
			END

			IF ISDATE(@MOut2) = 1
			BEGIN
				SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END

				IF @IsOS = 'Y' 
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked
				END

				IF @IsOT = 'Y'
				BEGIN
					SET @TempMUOtDuration = @TempMUHoursworked - @TempGWoOT
				END

			END
			IF @ISOTONOFF='C'
		BEGIN
			SET @TempMUOtDuration =0
		END
		END
		GOTO Lastl
	END

	IF @IsHoliday = 1
	BEGIN
		SET @TempMUHolidayValue = 1
		SET @TempMUStatus = 'HLD'

		IF ISDATE(@MIn1) = 1
		BEGIN
			SET @TempMUStatus = 'POH'
			IF @TempGIsPresentOnHLDPresent =  'Y'
			BEGIN
				SET @TempMUHolidayValue = 0
				SET @TempMUPresentValue = 1
			END
			IF ISDate(@MOut2) = 1
			BEGIN
				SET @TempMUHoursworked  = DATEDIFF(N, @MIn1, @MOut2)
				IF ISDATE(@MIn2) = 1
				BEGIN
					SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
					SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
				END

				IF @IsOS = 'Y' 
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked
				END

				IF @IsOT = 'Y'
				BEGIN
					SET @TempMUOtDuration = @TempMUHoursworked - @TempGHldOT
				END

			END
		END
		GOTO Lastl
	END
	--G4S 

	--IF ISDATE(@MIn1) = 1 AND @InOnly != 'N' 
	--BEGIN
	--    -- TODO : Check with Virendra sir. VB code.Line Number 2285, Module1.vb
	--	IF @InOnly != 'O' AND ISDATE(@Mout2) != 1
	--	BEGIN
	--	--	SET @MOut2 = @TempShiftEndDateTime
	--	END
	--END

	IF @IsPunchAll = 'N' AND ISDATE(@MIn1) != 1
	BEGIN
		SET @MIn1 = @TempShiftStartDateTime
		SET @MOut2 = @TempShiftEndDateTime
	END

	IF @IsPunchAll = 'N' AND ISDATE(@MIn1) = 1 AND ISDATE(@MOut2) != 1
	BEGIN
		IF DATEDIFF(N, @MIn1, @TempShiftEndDateTime) > 0
		BEGIN 
			SET @MOut2 = @TempShiftEndDateTime
		END
		ELSE
		BEGIN
			SET @MOut2 = @MIn1
			SET @MIn1 = @TempShiftStartDateTime
		END
	ENd

	IF (ISDATE(@MIn1) = 1 AND ISDATE(@MOut2) != 1) OR (ISDATE(@MIn1) != 1 AND ISDATE(@MOut2) = 1) 
	BEGIN
		IF @MIS='A'
		BEGIN
			SET @TempMUStatus = 'A'
			SET @TempMUPresentValue = 0
			SET @TempMUAbsentValue=1
		END
		ELSE IF  @MIS='H'
		BEGIN
			SET @TempMUStatus = 'HLF'
			SET @TempMUPresentValue =0.5
			SET @TempMUAbsentValue=0.5
		END
		ELSE
		BEGIN
			SET @TempMUStatus = 'MIS'
			SET @TempMUPresentValue =1
			SET @TempMUAbsentValue=0
		
		END


		IF SUBSTRING(@TempShiftPosition,1,1) = 'H'
		BEGIN
			IF @TempMULeaveValue < 0.5 
			BEGIN
				SET @TempMUPresentValue  = 0.5 - @TempMULeaveValue
			END
			SET @TempMUWoValue = 0.5
		END

		IF @ShiftAttended != 'AGN' 
		BEGIN
			IF ISDATE(@MIn1) = 1
			BEGIN
				SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
				IF @TempMEarly > 0
				BEGIN
					SET @TempMUEarlyArrival = @TempMEarly
				END
				ELSE IF ABS(@TempMEarly) > @PermisLateArrival
				BEGIN
					SET @TempMULateArrival = ABS(@TempMEarly)
				END
			END
		END
		GOTO Lastl
	END

	IF SUBSTRING(@TempShiftPosition,1,1) = 'H' AND @TempMULeaveValue = 0
	BEGIN
		SET @TempMUWoValue = 0.5
		IF ISDATE(@MIn1) = 1 
		BEGIN 
			SET @TempMUPresentValue = 0.5
		END
		ELSE
		BEGIN
			SET @TempMUAbsentValue = 0.5
		END
	END

	IF SUBSTRING(@TempShiftPosition,1,1) = 'H' AND @TempMULeaveValue > 0
	BEGIN
		SET @TempMUWoValue = 0.5
		IF ISDATE(@MIn1) = 1 AND  @TempMULeaveValue < 0.5
		BEGIN 
			SET @TempMUPresentValue = 0.5 - @TempMULeaveValue
		END
		ELSE
		BEGIN
			IF  @TempMULeaveValue < 0.5
			BEGIN
				SET @TempMUAbsentValue = 0.5 - @TempMULeaveValue
			END			
		END
	END

	IF ISDATE(@MOut2) = 1 AND ISDATE(@MIn1) =1 
	BEGIN
		SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
		IF ISDATE(@MIn2) = 1
		BEGIN
			SET @TempMLDur = DATEDIFF(N, @MOut1, @MIn2)
			IF @TempMLDur < @TempMLDed 
			BEGIN
				SET @TempMLDur = @TempMLDed
			END
			SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDur
		END
		ELSE
		BEGIN
			SET @TempMUHoursworked = @TempMUHoursworked - @TempMLDed
		END

		IF @ShiftAttended != 'IGN'
		BEGIN
			SET @TempMUHoursworked = DATEDIFF(N, @MIn1, @MOut2)
			IF @TempMUHoursworked < @Time
			BEGIN 
				SET @TempMUHoursworked = 0
				SET @TempMUAbsentValue = 1
				GOTO Lastl
			END

			IF ISDATE(@MIn2) = 1 AND ISDATE(@TempLunchStartDateTime) = 1
			BEGIN
				SET @TempMEarly1 = DATEDIFF (N, @MOut1, @TempLunchStartDateTime)
				IF @TempMEarly1 > 0 
				BEGIN 
					SET @TempMULunchEarlyDeparture  = @TempMEarly1
				END

				SET @TempMLate1 = DATEDIFF(N, @TempLunchEndDateTime, @MIn2)
				IF @TempMLate1 > 0
				BEGIN
					SET @TempMULunchLateArrival = @TempMLate1				
				END

				IF @TempMLDur > @TempMLunchDur
				BEGIN
					SET @TempMUExcLunchHours = @TempMLDur - @TempMLunchDur
				END
			END

			IF @IsOS = 'Y' 
			BEGIN
				IF @TempMUStatus = 'HLF'
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked - 240
				END
				ELSE
				BEGIN
					SET @TempMUOSDuration = @TempMUHoursworked - @TempMShiftTime
				END

				IF @TempMUOSDuration < 0 
				BEGIN
					SET @TempMUOSDuration = 0
				END
			END

			SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
			IF @TempMEarly > 0
			BEGIN
				SET @TempMUEarlyArrival = @TempMEarly
			END
			ELSE IF ABS(@TempMEarly) > @PermisLateArrival
			BEGIN
				SET @TempMULateArrival = ABS(@TempMEarly)
			END

			SET @TempMLate = DATEDIFF(N, @TempShiftEndDateTime, @MOut2)
			IF @TempMLate < 0 AND ABS(@TempMLate) > @PermisEarlyDepart
			BEGIN 
				SET @TempMUEarlyDeparture = ABS(@TempMLate)
			END

			SET @TempMUStatus = 'P'

			IF SUBSTRING(@TempShiftPosition,1,1) = 'H'
			BEGIN
				IF @TempMULeaveValue  < 0.5 
				BEGIN
					SET @TempMUPresentValue = 0.5 - @TempMULeaveValue
				END
				SET @TempMUAbsentValue = 0				
			END
			ELSE
			BEGIN
				SET @TempMUPresentValue = 1
			END

			SET @TempMAbsHours = DATEDIFF(N, @TempShiftStartDateTime, @TempShiftEndDateTime) - @TempMUHoursworked
			IF @TempMAbsHours > 0
			BEGIN
				IF @IsHalfDay = 'Y' AND @TempMUHoursworked < @Half AND @TempMUHoursworked >= @Time
				BEGIN 
					SET @TempMUStatus = 'HLF'
					SET @TempMUPresentValue = 0.5
					SET @TempMUAbsentValue = 0.5 
					SET @TempMULateArrival = 0
					SET @TempMUEarlyDeparture = 0
					SET @TempMUTotalLossHRS = 0
				END
				ELSE IF @IsHalfDay = 'Y' AND @IsShort = 'Y' AND @TempMUHoursworked <= @Short AND @TempMUHoursworked > @Half
				BEGIN
					SET @TempMUStatus = 'SRT'
					SET @TempMUPresentValue = 1
				END
			END

			IF @IsOT = 'Y'
			BEGIN
				IF @TempGOTFormulae = 1 
				BEGIN 
					SET @TempMOT = DATEDIFF(N, @TempShiftEndDateTime, @MOut2)
				END
				ELSE IF @TempGOTFormulae = 2
				BEGIN 
					SET @TempMOT = @TempMUHoursworked - @TempMShiftTime
				END
				ELSE IF  @TempGOTFormulae = 3
				BEGIN
					SET @TempMEarly = DATEDIFF(N, @MIn1, @TempShiftStartDateTime)
					IF @TempMEarly <= @TempGOTEarly 
					BEGIN
						SET @TempMEarly = 0
					END

					SET @TempMLate = DATEDIFF(N, @TempShiftEndDateTime , @MOut2)
					IF @TempMLate <= @TempGOTLate
					BEGIN 
						SET @TempMLate = 0
					END

					SET @TempMOT = @TempMEarly + @TempMLate - @TempMUExcLunchHours 
				END

				IF @TempMOT < @TempMOtStartAfter
				BEGIN 
					SET @TempMOT = 0
				END
				ELSE IF @TempMOtDeductHours = 0 AND @TempMOT > @TempMOtDeductAfter
				BEGIN 
					SET @TempMOT = @TempMOtDeductAfter
				END
				ELSE IF @TempMOT >= (@TempMOtDeductAfter + @TempMOtDeductHours)
				BEGIN 
					SET @TempMOT = @TempMOT - @TempMOtDeductHours 
				END

				IF @TempGIsOTMinus = 'N' AND @TempMOT < 0
				BEGIN 
					SET @TempMOT = 0
				END

				SET @TempMUOtDuration = @TempMOT
			END
			GOTO Lastl
		END
		ELSE
		BEGIN 
			SET @TempMUPresentValue = 1
			SET @TempMUStatus = 'P'
		END

		
	END

	IF @TempMULeaveValue = 0 AND @TempMUPresentValue = 0 AND @TempMUHolidayValue = 0 AND @TempMUWoValue = 0
		BEGIN 
			SET @TempMUAbsentValue = 1
		END
	Lastl:

	IF @TempMUStatus = 'HLF' AND @IsOT = 'Y'
	BEGIN 
		SET @TempMUOtDuration = @TempMUHoursworked - 240
	END

	IF @TempGOTRound = 'Y'
	BEGIN 
		IF @TempMUOtDuration >= 0
		BEGIN
			SET @TempMOTInHours = (@TempMUOtDuration/60) + ((@TempMUOtDuration % 60)* 0.01)
			SET @TempMOTFactor = @TempMOTInHours - (@TempMUOtDuration/60)
		END
		ELSE
		BEGIN
			SET @TempMOTInHours = -((ABS(@TempMUOtDuration)/60) + ((ABS(@TempMUOtDuration) % 60)*0.01))
			SET @TempMOTFactor = @TempMOTInHours - (-(ABS(@TempMUOtDuration)/60))
		END
		
		IF @TempMOTFactor <= 0.15 
		BEGIN
			SET @TempMOTInHours =  (@TempMUOtDuration/60) 
			SET @TempMMinutes = 0
		END
		ELSE IF @TempMOTFactor >= 0.15  AND @TempMOTFactor < 0.45
		BEGIN
			SET @TempMOTInHours =  (@TempMUOtDuration/60) + 0.3
			SET @TempMMinutes =  0.3 * 60
		END
		ELSE IF  @TempMOTFactor >= 0.45 
		BEGIN
			SET @TempMOTInHours =  (@TempMUOtDuration/60) + 1
			SET @TempMMinutes = 1 * 60
		END

		--TODO : Check actual value of MUOUTDuration being stored in database
		SET @TempMUOtDuration =  ((@TempMUOtDuration/60)* 60) + @TempMMinutes
	END

	IF @IsTimeLossAllowed = 'Y'
	BEGIN 
		SET @TempMUTotalLossHRS = @TempMULateArrival + @TempMUEarlyDeparture + @TempMULunchLateArrival + @TempMULunchEarlyDeparture
	END
	ELSE
	BEGIN		
		SET @TempMULateArrival = 0
		SET @TempMUEarlyDeparture = 0
		SET @TempMUTotalLossHRS = 0
		SET @TempMULunchLateArrival = 0
		SET @TempMULunchEarlyDeparture = 0
	END

	IF @OTRate = 0
	BEGIN 
		SET @TempMUOtAmount = 0
	END
	ELSE
	BEGIN
		SET @TempMUOtAmount = @TempMUOtDuration  * (@OTRate / 60)
	END
	IF @TempMULateArrival>60
	BEGIN
		SET @TempMUStatus = 'A'
		SET @TempMUPresentValue = 0
		SET @TempMUAbsentValue = 1 
	--	SET @TempMULateArrival = 0
	--	SET @TempMUEarlyDeparture = 0
	--	SET @TempMUTotalLossHRS = 0
	END
   
    UPDATE tbltimeregister
	SET 
		SHIFTSTARTTIME = @TempShiftStartDateTime,
		SHIFTENDTIME = @TempShiftEndDateTime,
		LUNCHSTARTTIME = @TempLunchStartDateTime,
		LUNCHENDTIME = @TempLunchEndDateTime,
		HOURSWORKED = @TempMUHoursworked,
		EXCLUNCHHOURS = @TempMUExcLunchHours,
		OTDURATION = @TempMUOtDuration,
		OSDURATION = @TempMUOSDuration,
		OTAMOUNT = @TempMUOtAmount,
		EARLYARRIVAL = @TempMUEarlyArrival,
		EARLYDEPARTURE = @TempMUEarlyDeparture,
		LATEARRIVAL = @TempMULateArrival,
		LUNCHEARLYDEPARTURE = @TempMULunchEarlyDeparture,
		LUNCHLATEARRIVAL = @TempMULunchLateArrival,
		TOTALLOSSHRS = @TempMUTotalLossHRS,
		[STATUS] = @TempMUStatus,
		SHIFTATTENDED = @ShiftAttended,
		IN1 = @MIn1,
		IN2 = @MIn2,
		OUT1 = @MOut1,
		OUT2 = @MOut2,
		IN1MANNUAL = @MIn1Manual,
		IN2MANNUAL = @MIn2Manual,
		OUT1MANNUAL = @MOUT1Manual,
		OUT2MANNUAL = @MOUT2Manual,
		LEAVEVALUE = @TempMULeaveValue,
		PRESENTVALUE = @TempMUPresentValue,
		ABSENTVALUE = @TempMUAbsentValue,
		HOLIDAY_VALUE = @TempMUHolidayValue,
		WO_VALUE = @TempMUWoValue,
		OUTWORKDURATION = @TempMUOutWorkDuration
		WHERE SSN = @SSN 
		AND DateOFFICE = @DateOfOffice
END	




GO
/****** Object:  StoredProcedure [dbo].[ProcessUpdateDutyRoster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessUpdateDutyRoster] 
	@PayCode CHAR(10),
	@FromDate DATETIME,
	@CompanyCode VARCHAR(10)
AS
BEGIN
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	PRINT 'INSIDE ProcessCreateDutyRoster => Parameters are - '
	PRINT '@PayCode   :' +@PayCode
	PRINT '@FromDate    :' + CAST(@FromDate AS VARCHAR(50))
	PRINT '@CompanyCode :' + @CompanyCode

	DECLARE @TempCount INT
	DECLARE @TableID INT
	DECLARE @TempSSN VARCHAR(100)
	DECLARE @MinDate DATETIME
	DECLARE @WOInclude VARCHAR(5)

	DECLARE @TempPayCode CHAR(10)
	DECLARE @DateOfJoin DateTime
	DECLARE @Shift VARCHAR(5)
	DECLARE @AlternateOffDays VARCHAR(10)
	DECLARE @FirstOffDay VARCHAR(5)
	DECLARE @SecondOffType CHAR(1)
	DECLARE @HalfDayShift VARCHAR(5)
	DECLARE @SecondOffDay VARCHAR(5)
	DECLARE @SSN VARCHAR(100)

	DECLARE @EmployeeShiftDetails Table 
	(
		PayCode CHAR(10),
		DateOfJoin DateTime,
		[Shift] VARCHAR(5),
		AlternateOffDays VARCHAR(10),
		FirstOffDay VARCHAR(5),
		SecondOffType CHAR(1),
		HalfDayShift VARCHAR(5),
		SecondOffDay VARCHAR(5),
		SSN VARCHAR(100)
	)

	IF (@PayCode = '')
	BEGIN
		INSERT INTO @EmployeeShiftDetails
		SELECT EmpShift.PAYCODE, 
			   Emp.DateOFJOIN,
			   EmpShift.[SHIFT],
			   EmpShift.ALTERNATE_OFF_DAYS,
			   EmpShift.FIRSTOFFDAY,
			   EmpShift.SECONDOFFTYPE,
			   EmpShift.HALFDAYSHIFT,
			   EmpShift.SECONDOFFDAY,
			   Emp.SSN
		FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
		INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
		WHERE Emp.active = 'Y' 
		AND Emp.Companycode=@CompanyCode  Order By EmpShift.Paycode
    END
	ELSE
	BEGIN
		INSERT INTO @EmployeeShiftDetails
		SELECT EmpShift.PAYCODE, 
			   Emp.DateOFJOIN,
			   EmpShift.[SHIFT],
			   EmpShift.ALTERNATE_OFF_DAYS,
			   EmpShift.FIRSTOFFDAY,
			   EmpShift.SECONDOFFTYPE,
			   EmpShift.HALFDAYSHIFT,
			   EmpShift.SECONDOFFDAY,
			   Emp.SSN
		FROM tblEmployeeShiftMaster(NOLOCK) EmpShift
		INNER JOIN tblEmployee(nolock) Emp ON Emp.SSN = EmpShift.SSN 
		WHERE Emp.active = 'Y' 
		AND Emp.Companycode=@CompanyCode 
		AND EmpShift.PAYCODE = @PayCode Order By EmpShift.Paycode
	END

	SELECT @TempCount = count(*) FROM @EmployeeShiftDetails
	IF ( @TempCount > 0)
	BEGIN
		WHILE  EXISTS(SELECT * FROM @EmployeeShiftDetails)
		BEGIN
		   -- GET First record to process
		   SELECT @TempSSN = (SELECT TOP 1 SSN FROM @EmployeeShiftDetails ORDER BY SSN ASC)
		   SELECT @TempPayCode = (SELECT TOP 1 PayCode FROM @EmployeeShiftDetails ORDER BY SSN ASC)
	       SELECT @DateOfJoin = DateOfJoin FROM @EmployeeShiftDetails WHERE SSN = @TempSSN

		   IF @DateOfJoin > @FromDate
		   BEGIN
				SET @MinDate = @DateOfJoin  
		   END
		   ELSE
		   BEGIN
				SET @MinDate = @FromDate
		   END
		   
		   SELECT @WOInclude = WOInclude FROM tblsetup (nolock) where CompanyCode=@CompanyCode

		   EXEC ProcessDutyRosterForUpdate @TempPayCode, @MinDate, @WOInclude, @TempSSN

		   -- DELETE the record from temp table after processing
		   DELETE @EmployeeShiftDetails  where SSN = @TempSSN

		END
	END
	ELSE
	BEGIN
	 PRINT 'No record to process'
	END	


	--SELECT * FROM @EmployeeShiftDetails
END






GO
/****** Object:  StoredProcedure [dbo].[ProcessUPDReason]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProcessUPDReason]
	@PayCode VARCHAR(10),
	@DateOffice DATETIME,
	@TempSSN VARCHAR(100),
	@ReasonCode VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempMHours INT
	DECLARE @TempReasonCount INT
	DECLARE @LateArrival INT
	DECLARE @EarlyDeparture INT
	DECLARE @ExcLunchHours INT
	DECLARE @HoursWorked INT


	

	DECLARE @TempReasonMaster TABLE
	(
		ReasonCode CHAR(3),
		[Description] CHAR(45),
		LeaveCode CHAR(3),
		LeaveValue FLOAT,
		Late CHAR(1),
		Early CHAR(1),
		ExcLunchHours CHAR(1),
		HoursWorked CHAR(1)
	)



	DECLARE @TempLate CHAR(1)
	DECLARE @TempEarly CHAR(1)
	DECLARE @TempExcLunchHours CHAR(1)
	DECLARE @TempHoursWorked CHAR(1)

	SET @TempMHours = 0

	SELECT @LateArrival=LATEARRIVAL,
		   @EarlyDeparture = EARLYDEPARTURE,
		   @ExcLunchHours = EXCLUNCHHOURS,
		   @HoursWorked = HOURSWORKED
    FROM tbltimeregister
	WHERE SSN = @TempSSN AND DateOFFICE = @DateOffice

	INSERT INTO @TempReasonMaster 
		SELECT * FROM tblReasonMaster
		WHERE ReasonCode = @ReasonCode


	SELECT @TempReasonCount = COUNT(*) FROM @TempReasonMaster

	IF @TempReasonCount > 0
	BEGIN 
		SELECT TOP 1
			@TempLate = Late,
			@TempEarly = Early,
			@TempExcLunchHours = ExcLunchHours
		FROM @TempReasonMaster

		IF @TempLate != 'N'
		BEGIN 
			SET @TempMHours = @LateArrival

			UPDATE tbltimeregister 
			SET LATEARRIVAL = 0
			WHERE PAYCODE = @PayCode
			AND DateOFFICE = @DateOffice
		END

		IF @TempEarly != 'N'
		BEGIN 
			SET @TempMHours = @TempMHours + @EarlyDeparture

			UPDATE tbltimeregister 
			SET EARLYDEPARTURE = 0
			WHERE PAYCODE = @PayCode
			AND DateOFFICE = @DateOffice
		END

		IF @ExcLunchHours != 'N'
		BEGIN 
			SET @TempMHours = @TempMHours + @ExcLunchHours

			UPDATE tbltimeregister 
			SET EXCLUNCHHOURS = 0
			WHERE PAYCODE = @PayCode
			AND DateOFFICE = @DateOffice
		END

		IF @TempHoursWorked != 'N'
		BEGIN 		
			UPDATE tbltimeregister 
			SET HOURSWORKED = @HoursWorked + @TempMHours 
			WHERE PAYCODE = @PayCode
			AND DateOFFICE = @DateOffice
		END
	END   
END






GO
/****** Object:  StoredProcedure [dbo].[sp_CheckLogin]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_CheckLogin]  
(  
 @Customerid varchar(20),  
 @UserID varchar(50),  
 @Password varchar(50),  
 @Result int=0 output   
)  
AS  
declare @LoginUser varchar(10)
begin  
if exists(select ShortName from tblCompany where Active='Y' and ShortName=@Customerid)  
 BEGIN  
   set @Customerid=(select CompanyCode from tblCompany where Active='Y' and ShortName=@Customerid)  
   print @Customerid  
	set @LoginUser=(select ltrim(rtrim(upper(user_R))) from tbluser where companycode=@Customerid and LoginType='L')  
	print ''''+@LoginUser+''''
	
	--select * from tbluser where tbluser.CompanyCode=''''+@Customerid+''''  and  tbluser.user_r=@Userid and tbluser.Password=@Password

      if ltrim(rtrim(upper(@UserID)))=@LoginUser
            if exists(select * from tbluser where tbluser.CompanyCode=@Customerid and  tbluser.user_r=@Userid and tbluser.Password=@Password)  
			   begin  
				set @Result=2      
				print @Result
				select *,userdescriprion as EmpName,tbluser.Grade as 'GV' from tbluser where tbluser.CompanyCode=@Customerid and  tbluser.user_r=@Userid and tbluser.Password=@Password      
			   end  
			  else  
			   begin  
				set @Result=1
				print @Result+''+'hi'      
			   end  
			ELSE  
			  if exists(select * from tblemployee join tbluser on tblemployee.paycode=tbluser.user_r and tbluser.CompanyCode=tblemployee.companycode where tblemployee.active='Y' and tbluser.CompanyCode=@Customerid and  tbluser.user_r=@Userid and tbluser.Password=@Password)   
			   begin  
				set @Result=2      
				print @Result
				select * ,tbluser.Grade as 'GV' from tblemployee join tbluser on tblemployee.paycode=tbluser.user_r and tbluser.CompanyCode=tblemployee.companycode where tblemployee.active='Y' and tbluser.CompanyCode=@Customerid and  tbluser.user_r=@Userid and tbluser.Password=@Password      
			   end  
			  else  
			   begin  
				set @Result=1
				print @Result
		end  
 end  
else  
 begin  
  set @Result=0    
	print @Result
 end  
end  



GO
/****** Object:  StoredProcedure [dbo].[SP_CreateDutyRoster]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[SP_CreateDutyRoster] --'C001','KCI/004','2016-01-01'
@ComCode varchar(10),
@Paycode varchar(10),
@StDt varchar(10)
as
begin	
	if exists(Select tblEmployeeShiftMaster.PayCode,Convert(varchar(10),tblemployee.dateofjoin,126) 'dateofjoin',tblEmployeeShiftMaster.SHIFT, tblEmployeeShiftMaster.ALTERNATE_OFF_DAYS, tblEmployeeShiftMaster.FIRSTOFFDAY, tblEmployeeShiftMaster.SECONDOFFTYPE, tblEmployeeShiftMaster.HALFDAYSHIFT, tblEmployeeShiftMaster.SECONDOFFDAY from TblEmployeeShiftMaster join tblEmployee on tblEmployee.Paycode = tblEmployeeShiftMaster.Paycode And tblEmployee.active = 'Y' and tblemployee.companycode=@ComCode and tblemployee.paycode=@paycode )	
		begin 
			print 'Exists'	
			declare @dtJoin varchar(10),@Shift varchar(3),@AltDays varchar(10),@FWoff varchar(10),@SWoff varchar(10),@SWoffType varchar(10),@HLFShift varchar(10)
			declare @mmin_date varchar(10)
			declare EmpDetails cursor
			static
			for
			Select Convert(varchar(10),tblemployee.dateofjoin,126) 'dateofjoin',tblEmployeeShiftMaster.SHIFT, tblEmployeeShiftMaster.ALTERNATE_OFF_DAYS, tblEmployeeShiftMaster.FIRSTOFFDAY,tblEmployeeShiftMaster.SECONDOFFDAY,tblEmployeeShiftMaster.SECONDOFFTYPE,tblEmployeeShiftMaster.HALFDAYSHIFT from TblEmployeeShiftMaster join tblEmployee on tblEmployee.Paycode = tblEmployeeShiftMaster.Paycode And tblEmployee.active = 'Y' and tblemployee.companycode=@ComCode and tblemployee.paycode=@paycode 
			--Select tblemployee.dateofjoin from TblEmployeeShiftMaster join tblEmployee on tblEmployee.Paycode = tblEmployeeShiftMaster.Paycode And tblEmployee.active = 'Y' and tblemployee.companycode=@ComCode and tblemployee.paycode=@paycode 
			open EmpDetails
			fetch next from EmpDetails into @dtJoin,@Shift,@AltDays,@FWoff,@SWoff,@SWoffType,@HLFShift
			while @@fetch_Status=0
			begin
			print @dtJoin +' '+@Shift+' '+@AltDays+' '+@FWoff+' '+@SWoff+' '+@SWoffType+' '+@HLFShift
			fetch next from EmpDetails into @dtJoin,@Shift,@AltDays,@FWoff,@SWoff,@SWoffType,@HLFShift
			end
			close EmpDetails
			deallocate EmpDetails

			print @dtJoin +' '+@Shift+' '+@AltDays+' '+@FWoff+' '+@SWoff+' '+@SWoffType+' '+@HLFShift
			if(cast(@dtJoin as datetime) > cast(@StDt as datetime))
				set @mmin_date=@dtJoin
			else
				set @mmin_date=@StDt
			--end
			print @mmin_date
		end
	else
		print 'Not Exists'
end





GO
/****** Object:  StoredProcedure [dbo].[SPInsert]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SPInsert] 
@ID int,
@Name nvarchar(100)

As
Begin
Insert into  Test (ID,Name)Values (@ID,@Name)

END

GO
/****** Object:  StoredProcedure [dbo].[upd]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[upd](@fcode char(10),@dateoffice1 datetime)
as
    declare mTime cursor scroll for 
    select * from tbltimeregister where paycode=@fcode and dateoffice=@dateoffice1
    declare
    @TIME 	numeric,
    @dateoffice datetime,
    @paycode char(10),
    @mEarly numeric,
    @mShiftTime numeric,
    @AbsHours   numeric,
    @isHld      char(1),
    @OTinHrs    numeric,
    @OTfactor   numeric,
    @mLDed      numeric,
    @mLDur      numeric,
    @mLunchDur  numeric,
    @mOt        numeric,
    @mOtStartAfter numeric,
    @mOtDeductAfter numeric,
    @mOtDeductHrs   numeric,
    @LeaveType1 char(1),
    @LeaveType2 char(1),
    @Status  char(6),
    @HOURSWORKED numeric,
    @EXCLUNCHHOURS numeric,
    @OTDURATION numeric,
    @OSDURATION numeric,
    @OTAMOUNT numeric(10,2),
    @EarlyArrival numeric,
    @EARLYDEPARTURE numeric,
    @LateArrival numeric,
    @LunchEarlyDeparture numeric,
    @LunchLateArrival numeric,
    @TOTALLOSSHRS numeric,
    @LEAVEVALUE numeric(10,2),
    @PRESENTVALUE numeric(10,2),
    @ABSENTVALUE numeric(10,2),
    @HOLIDAY_VALUE numeric(10,2),
    @WO_VALUE numeric(10,2),
    @OUTWORKDURATION numeric,
    @Flag  char(1),
    @mCnt  numeric,
    @shiftstarttime varchar(25),
    @shiftendtime datetime,
    @lunchstarttime datetime,
    @lunchendtime datetime,
    @reason varchar(100),
    @shift char(3),
    @shiftattended char(3),
    @in1 datetime,
    @in2 datetime,
    @out1 datetime,
    @out2 datetime,
    @in1mannual char(1),
    @in2mannual char(1),
    @out2mannual char(1),
    @out1mannual char(1),
    @leavetype   char(1),
    @leavecode char(3),
    @leaveamount numeric(10,2),
    @leaveaprdate datetime,
    @voucher_no char(10),
    @reasoncode char(3),
    @FirstHalfLeavecode char(3),
    @SecondHalfLeavecode char(3),
    @mSHiftpos char(1),
    @LEAVEAMOUNT1 numeric(10,2),
    @LEAVEAMOUNT2 numeric(10,2),
    @Wbr char(1),
    @AllowedOT numeric(10),
    @ApprovedOT numeric(10),
    @Stage1OTApprovedBy char(10),
    @Stage2OTApprovedBy char(10),
    @Stage1ApprovalDate Datetime, 
    @Stage2ApprovalDate Datetime
    
    Begin
    
    Set @Status = 'A'
    Set @HOURSWORKED = 0
    Set @EXCLUNCHHOURS = 0
    Set @OTDURATION = 0
    Set @OSDURATION = 0
    Set @OTAMOUNT = 0
    Set @EarlyArrival = 0
    Set @EARLYDEPARTURE = 0
    Set @LateArrival = 0
    Set @LunchEarlyDeparture = 0
    Set @LunchLateArrival = 0
    Set @TOTALLOSSHRS = 0
    Set @LEAVEVALUE = 0
    Set @PRESENTVALUE = 0
    Set @ABSENTVALUE = 0
    Set @HOLIDAY_VALUE = 0
    Set @WO_VALUE = 0
    Set @OUTWORKDURATION = 0
    Set @Flag = ''
    set @leaveamount = 0
    print 'leaveamount' + str(@LeaveAmount)
    print @fcode
    print @dateoffice1
OPEN mTIME
--fetch next from mTime  into @PAYCODE,@DateOFFICE,@SHIFTSTARTTIME,@SHIFTENDTIME,@LUNCHSTARTTIME,@LUNCHENDTIME,@HOURSWORKED,@EXCLUNCHHOURS,@OTDURATION,@OSDURATION,@OTAMOUNT,@EARLYARRIVAL,@EARLYDEPARTURE,@LATEARRIVAL,@LUNCHEARLYDEPARTURE,@LUNCHLATEARRIVAL,

--@TOTALLOSSHRS,@STATUS,@REASON,@SHIFT,@SHIFTATTENDED,@IN1,@IN2,@OUT1,@OUT2,@IN1MANNUAL,@IN2MANNUAL,@OUT1MANNUAL,@OUT2MANNUAL,@LEAVEVALUE,@PRESENTVALUE,@ABSENTVALUE,@HOLIDAY_VALUE,@WO_VALUE,@OUTWORKDURATION,@LEAVETYPE,@LEAVECODE,@LEAVEAMOUNT,@FLAG,@LEAVEA

--Date,@VOUCHER_NO,@ReasonCode
fetch next from mTime  into @PAYCODE,@DateOFFICE,@SHIFTSTARTTIME,@SHIFTENDTIME,@LUNCHSTARTTIME,@LUNCHENDTIME,@HOURSWORKED,@EXCLUNCHHOURS,@OTDURATION,@OSDURATION,@OTAMOUNT,@EARLYARRIVAL,@EARLYDEPARTURE,@LATEARRIVAL,@LUNCHEARLYDEPARTURE,@LUNCHLATEARRIVAL,@TOTALLOSSHRS,@STATUS,@LEAVETYPE1,@LEAVETYPE2,@FIRSTHALFLEAVECODE,@SECONDHALFLEAVECODE,@REASON,@SHIFT,@SHIFTATTENDED,@IN1,@IN2,@OUT1,@OUT2,@IN1MANNUAL,@IN2MANNUAL,@OUT1MANNUAL,@OUT2MANNUAL,@LEAVEVALUE,@PRESENTVALUE,@ABSENTVALUE,@HOLIDAY_VALUE,@WO_VALUE,@OUTWORKDURATION,@LEAVETYPE,@LEAVECODE,@LEAVEAMOUNT,@LEAVEAMOUNT1,@LEAVEAMOUNT2,@FLAG, @LEAVEAPRDate, @VOUCHER_NO, @ReasonCode, @Wbr, @AllowedOT, @ApprovedOT, @Stage1OTApprovedBy, @Stage2OTApprovedBy, @Stage1ApprovalDate, @Stage2ApprovalDate 
    Set @EarlyArrival = 0
    Set @EARLYDEPARTURE = @EARLYDEPARTURE
    Set @LateArrival = @LATEARRIVAL
    Set @LunchEarlyDeparture = 0
    Set @LunchLateArrival = 0
WHILE @@FETCH_STATUS = 0
begin
  --checking for holiday
    print @LEAVEAMOUNT
    print '1'

    --Select @mCnt=Count(*) From holiday Where hdate=@dateoffice  and companycode=(select companycode from tblemployee where paycode=@fcode)
    Select @mCnt=Count(*) From tblempholiday Where hdate=@dateoffice  and Paycode=@fcode
    If @mCnt <= 0
    begin
        set @isHld = 'F'
    end
    Else
    begin
       set @isHld = 'T'
    End
    If (IsNull(@ShiftAttended,'true')='true') or (Len(rtrim(ltrim(@ShiftAttended))) < 1)
    begin
        set @ShiftAttended = @Shift
    End
    
    print '2'
    If @ShiftAttended <> 'OFF' And  @ShiftAttended <> 'IGN' 
    begin
       select @shiftstarttime = convert(char(11),@dateoffice,113) + ' ' + convert(char(5),starttime,108) from tblshiftmaster where shift=@shiftattended 
       select @shiftendtime = convert(char(11),@dateoffice,113) + ' ' + convert(char(5),endtime,108) from tblshiftmaster where shift=@shiftattended        
       If DateDiff(n, @ShiftStartTime, @ShiftEndTime) < 0 
       begin
            set @ShiftEndTime = DateAdd(d, 1, @ShiftEndTime)
       End
       select @LUNCHSTARTTIME= convert(char(11),@dateoffice,113) + ' ' + convert(char(5),lunchtime,108) from tblshiftmaster where shift=@shiftattended 
       If DateDiff(n, @ShiftStartTime, @LunchStartTime) < 0 
       begin
            set @LUNCHSTARTTIME = DateAdd(d, 1, @LunchStartTime)
       End
       select @lunchendtime= convert(char(11),@dateoffice,113) + ' ' + convert(char(5),lunchendtime,108) from tblshiftmaster where shift=@shiftattended 
       If DateDiff(n, @ShiftStartTime, @LunchEndTime) < 0 
       begin
           set @lunchendtime = DateAdd(d, 1, @LunchEndTime)
       End
       set @mLDur = DateDiff(n, @LunchStartTime, @LunchEndTime)
       set @mLunchDur = @mLDur
       select @mLDed= lunchdeduction from tblshiftmaster where shift=@shiftattended 
       set @mShiftTime = DateDiff(n, @ShiftStartTime, @ShiftEndTime) - @mLDur
    End
    print '3'
    print @LeaveAmount
    print @leavecode
    If @LeaveAmount > 0 
    begin
	 print 'leave entered'
         print 'LeaveAmount=' + str(@LeaveAmount)
         if @LeaveAmount=0.25
         begin
              set @Status = 'Q_' + @LeaveCode
         end
         else if @LeaveAmount=0.5
         begin
              set @Status = 'H_' + @LeaveCode
         end
         else if @LeaveAmount=0.75
         begin

              set @Status = 'T_' + @LeaveCode
         end
         Else
         begin
              set @Status = @LeaveCode
         End 
	 print 'status=' + @Status
         --set @Status = @Status + @LeaveCode
         print 'status=' + @Status
         print 'leavecode=' + @LeaveCode
         If IsDate(@IN1)=1 
         begin
         print 'leave with in1'
            If @ShiftAttended <> 'IGN' And @ShiftAttended <> 'OFF' And @isHld='F'
            begin
               set @mEarly = DateDiff(n, @In1, @ShiftStartTime)
                If @mEarly > 0
                begin
                    set @EarlyArrival = @mEarly
                End
            End
            If IsDate(@Out2)=1
            begin
                set @HOURSWORKED = DateDiff(n, @In1, @Out2)
                If IsDate(@in2)=1 
                begin
                    set @mLDur = DateDiff(n, @out1, @in2)
                    If @mLDur < @mLDed 
                    begin
                        set @mLDur = @mLDed
                    End
                    set @HOURSWORKED = @HoursWorked - @mLDur
                End
            End
--updation
	    Select @time=time From tblemployeeshiftmaster Where paycode=@paycode
	    If @HOURSWORKED < @Time
	    begin
                set @HOURSWORKED = 0
                set @absentvalue = 1
	    end
            Else
	    begin
                set @presentvalue = 1
	    end
	   print 'presenthours ' + str(@presentvalue) 
--updation end

           If (IsNull(@LeaveType1,'N')='N' And IsNull(@LeaveType2,'N')='N') Or (IsNull(@LeaveType1,'N')='N' And Not IsNull(@LeaveType2,'N')='N') Or (Not IsNull(@LeaveType1,'N')='N' And IsNull(@LeaveType2,'N')='N')
	   begin
	            print 'updated if'
	            if @LeaveType='P'
        	    begin
                	 set @PRESENTVALUE = @PRESENTVALUE+ @LEAVEAMOUNT
			 set @ABSENTVALUE= @ABSENTVALUE-@LEAVEAMOUNT
        	    end            
		    else if @LeaveType='L'
        	    begin
                	 set @LEAVEVALUE = @LeaveAmount
		         set @PRESENTVALUE = @PRESENTVALUE - @LEAVEAMOUNT
			 SET @ABSENTVALUE=@ABSENTVALUE-@LEAVEAMOUNT
	            end
        	    Else
	            begin
        	          set @ABSENTVALUE = @ABSENTVALUE + @LeaveAmount
                	  set @PRESENTVALUE = @PRESENTVALUE - @LeaveAmount
            	   End
	           If @presentvalue < 0
		   BEGIN	    
		 	  SET @presentvalue = 0
	           END
            	   If @absentvalue < 0 
                   BEGIN
		 	  SET @absentvalue = 0
	           END
                   If @presentvalue > 1 
	           BEGIN
			 SET @presentvalue = 1
	           END
            	   If @absentvalue > 1
	           BEGIN
			SET @absentvalue = 1
	           END
	   end
	   else
	   begin
	   	print 'updated else'		
	        If (@LeaveType1= 'P' And @LeaveType2 = 'A') Or (@LeaveType1 = 'A' And @LeaveType2 = 'P') 
                begin
                    set @PRESENTVALUE = 0.5
                    set @LEAVEVALUE = 0
                    set @ABSENTVALUE = 0.5
                End
                If (@LeaveType1 = 'L' And @LeaveType2 = 'P') Or (@LeaveType1 = 'P' And @LeaveType2 = 'L')
                begin
		    PRINT 'IN LP'
                    set @PRESENTVALUE = 0.5
                    set @LEAVEVALUE = 0.5
                    set @ABSENTVALUE = 0
		    PRINT @LEAVEVALUE
                End
                If (@LeaveType1 = 'L' And @LeaveType2 = 'A') Or (@LeaveType1 = 'A' And @LeaveType2 = 'L')
                begin
                    set @PRESENTVALUE = 0
                    set @LEAVEVALUE = 0.5
                    set @ABSENTVALUE = 0.5
                End
                If (@LeaveType1 = 'L' And @LeaveType2 = 'L')
                begin
		    PRINT 'IN LL'
                    set @PRESENTVALUE = 0
                    set @LEAVEVALUE = 1
                    set @ABSENTVALUE = 0
                End
                If (@LeaveType1 = 'A' And @LeaveType2 = 'A') 
                begin
                    set @PRESENTVALUE = 0
                    set @LEAVEVALUE = 0
                    set @ABSENTVALUE = 1
                End
                If (@LeaveType1 = 'P' And @LeaveType2 = 'P') 
                begin
                    set @PRESENTVALUE = 1
                    set @LEAVEVALUE = 0
                    set @ABSENTVALUE = 0
                End
            end
        end
	Else
        begin
            print 'checking for presentvalue'
	    print IsNull(@LeaveType1,'true')
	    print IsNull(@LeaveType2,'true')
            If not(IsNull(@LeaveType1,'f')='f') And not(IsNull(@LeaveType2,'f')='f')
	    begin
		print 'checking for leave leave'
                set @Status = rtrim(@FirstHalfLeavecode) +  rtrim(@SecondHalfLeavecode)
                If (@LeaveType1= 'P' And @LeaveType2 = 'A') Or (@LeaveType1 = 'A' And @LeaveType2 = 'P') 
                begin
                    set @PRESENTVALUE = 0.5
                    set @LEAVEVALUE = 0
                    set @ABSENTVALUE = 0.5
                End
                If (@LeaveType1 = 'L' And @LeaveType2 = 'P') Or (@LeaveType1 = 'P' And @LeaveType2 = 'L')
                begin
		    PRINT 'IN LP'
                    set @PRESENTVALUE = 0.5
                    set @LEAVEVALUE = 0.5
                    set @ABSENTVALUE = 0
		    PRINT @LEAVEVALUE
                End
                If (@LeaveType1 = 'L' And @LeaveType2 = 'A') Or (@LeaveType1 = 'A' And @LeaveType2 = 'L')
                begin
                    set @PRESENTVALUE = 0
                    set @LEAVEVALUE = 0.5
                    set @ABSENTVALUE = 0.5
                End
                If (@LeaveType1 = 'L' And @LeaveType2 = 'L')
                begin
		    PRINT 'IN LL'
                    set @PRESENTVALUE = 0
                    set @LEAVEVALUE = 1
                    set @ABSENTVALUE = 0
                End
                If (@LeaveType1 = 'A' And @LeaveType2 = 'A') 
                begin
                    set @PRESENTVALUE = 0
                    set @LEAVEVALUE = 0
                    set @ABSENTVALUE = 1
                End
                If (@LeaveType1 = 'P' And @LeaveType2 = 'P') 
                begin
                    set @PRESENTVALUE = 1
                    set @LEAVEVALUE = 0
                    set @ABSENTVALUE = 0
                End
            end
            Else
            begin
		print 'checking for present type leave'
                print @leavetype
	        select @mSHiftpos= left(shiftposition,1) from tblshiftmaster where shift=@shiftattended 
                If rtrim(@LeaveType) = 'P' 
                begin
                print 'MANIPULATING LEAVEAMOUNT  FOR ' 
	        print @LeaveAmount
                    set @PRESENTVALUE = @LeaveAmount
                end
                Else If rtrim(@LeaveType) = 'L'
                begin
                    PRINT ' LEAVE VALUE= LEAVE AMOUNT'
		    set @LEAVEVALUE = @LeaveAmount
                end
                Else If rtrim(@LeaveType) = 'A' 
                begin
                   set @ABSENTVALUE = @LeaveAmount
                end
                If @ShiftAttended = 'OFF'
                begin
                   set @WO_VALUE = 1 - @LeaveAmount
                end
                Else If @isHld='T'
                begin
		PRINT 'PPPPPPP' + STR(@HOLIDAY_VALUE)
                   set @HOLIDAY_VALUE = 1 - @LeaveAmount
		PRINT 'TTTTT' + STR(@HOLIDAY_VALUE)
                end
                Else If @mSHiftpos = 'H'
                begin
                   set @WO_VALUE = 0.5
                end
                Else
                begin
                    If rtrim(@LeaveType) = 'P' Or rtrim(@LeaveType) = 'L'
                    begin
                      set  @ABSENTVALUE = 1 - @LeaveAmount
                    End
                End
                If @PRESENTVALUE = 0 And @LEAVEVALUE = 0 And @WO_VALUE = 0 And @HOLIDAY_VALUE = 0 
                begin
                    set @ABSENTVALUE = 1
                End
            End
        End
    end 
print   @ABSENTVALUE
PRINT @LEAVEVALUE
    update tbltimeregister set SHIFTSTARTTIME=@SHIFTSTARTTIME,SHIFTENDTIME=@SHIFTENDTIME,LUNCHSTARTTIME=@LUNCHSTARTTIME,LUNCHENDTIME=@LUNCHENDTIME,HOURSWORKED=@HOURSWORKED,EXCLUNCHHOURS=@EXCLUNCHHOURS,OTDURATION=@OTDURATION,OSDURATION=@OSDURATION,OTAMOUNT=@OTAMOUNT,EARLYARRIVAL=@EARLYARRIVAL,EARLYDEPARTURE=@EARLYDEPARTURE,LATEARRIVAL=@LATEARRIVAL,LUNCHEARLYDEPARTURE=@LUNCHEARLYDEPARTURE,LUNCHLATEARRIVAL=@LUNCHLATEARRIVAL,TOTALLOSSHRS=@TOTALLOSSHRS,STATUS=@STATUS,REASON=@REASON,SHIFT=@SHIFT,SHIFTATTENDED=@SHIFTATTENDED,IN1=@IN1,IN2=@IN2,OUT1=@OUT1,OUT2=@OUT2,IN1MANNUAL=@IN1MANNUAL,IN2MANNUAL=@IN2MANNUAL,OUT1MANNUAL=@OUT1MANNUAL,OUT2MANNUAL=@OUT2MANNUAL,LEAVEVALUE=@LEAVEVALUE,PRESENTVALUE=@PRESENTVALUE,ABSENTVALUE=@ABSENTVALUE,HOLIDAY_VALUE=@HOLIDAY_VALUE,WO_VALUE=@WO_VALUE,OUTWORKDURATION=@OUTWORKDURATION,LEAVETYPE=@LEAVETYPE,LEAVECODE=@LEAVECODE,LEAVEAMOUNT=@LEAVEAMOUNT,FLAG=@FLAG,LEAVEAPRDate=@LEAVEAPRDate,VOUCHER_NO=@VOUCHER_NO,ReasonCode=@ReasonCode WHERE PAYCODE=@PAYCODE AND DATEOFFICE=@DATEOFFICE
fetch next from mTime  into @PAYCODE,@DateOFFICE,@SHIFTSTARTTIME,@SHIFTENDTIME,@LUNCHSTARTTIME,@LUNCHENDTIME,@HOURSWORKED,@EXCLUNCHHOURS,@OTDURATION,@OSDURATION,@OTAMOUNT,@EARLYARRIVAL,@EARLYDEPARTURE,@LATEARRIVAL,@LUNCHEARLYDEPARTURE,@LUNCHLATEARRIVAL,@TOTALLOSSHRS,@STATUS,@LEAVETYPE1,@LEAVETYPE2,@FIRSTHALFLEAVECODE,@SECONDHALFLEAVECODE,@REASON,@SHIFT,@SHIFTATTENDED,@IN1,@IN2,@OUT1,@OUT2,@IN1MANNUAL,@IN2MANNUAL,@OUT1MANNUAL,@OUT2MANNUAL,@LEAVEVALUE,@PRESENTVALUE,@ABSENTVALUE,@HOLIDAY_VALUE,@WO_VALUE,@OUTWORKDURATION,@LEAVETYPE,@LEAVECODE,@LEAVEAMOUNT,@LEAVEAMOUNT1,@LEAVEAMOUNT2,@FLAG,@LEAVEAPRDate,@VOUCHER_NO,@ReasonCode, @Wbr,@AllowedOT,@ApprovedOT,@Stage1OTApprovedBy, @Stage2OTApprovedBy,@Stage1ApprovalDate,@Stage2ApprovalDate 
end
end

Deallocate mtime






GO
/****** Object:  StoredProcedure [dbo].[usp_check_reset_fk_cmd]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_check_reset_fk_cmd]
	-- Add the parameters for the stored procedure here
	@dev_id varchar(24),
	@trans_id varchar(16) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- trans_id를 무효한 값으로 설정한다.
    select @trans_id=''
    if @dev_id is null or len(@dev_id) = 0
		return -1
    
    -- Insert statements for procedure here
	-- 해당 기대에 대하여 '기대재기동'지령이 발행된것이 있는가 조사한다.
	SELECT @trans_id=trans_id FROM tbl_fkcmd_trans where device_id=@dev_id AND cmd_code='RESET_FK' AND status='WAIT'
	if @@ROWCOUNT = 0
		return -2 -- 없다면 복귀한다.
	
	begin transaction
	BEGIN TRY
		declare @trans_id_tmp as varchar(16)
		declare @csrTransId as cursor
		set @csrTransId = Cursor For
			 select trans_id
			 from tbl_fkcmd_trans
			 where device_id=@dev_id AND status='RUN'

		-- 기대의 지령수행상태가 'RUN'인것들을 조사하여 그에 해당한 레코드들을
		--  tbl_fkcmd_trans_cmd_param 표와 tbl_fkcmd_trans_cmd_result 표에서 지운다.
		Open @csrTransId
		Fetch Next From @csrTransId	Into @trans_id_tmp
		While(@@FETCH_STATUS = 0)
		begin
			DELETE FROM tbl_fkcmd_trans_cmd_param WHERE trans_id=@trans_id_tmp
			DELETE FROM tbl_fkcmd_trans_cmd_result WHERE trans_id=@trans_id_tmp
			Fetch Next From @csrTransId	Into @trans_id_tmp
		end
		close @csrTransId
		
		-- 기대의 지령수행상태가 'RUN'인것들을 'CANCELLED'로 바꾼다.
		UPDATE tbl_fkcmd_trans SET status='CANCELLED', update_time = GETDATE() WHERE device_id=@dev_id AND status='RUN'
		-- 기대에 대해 '재기동'지령이 발행된것들이 또 있으면 그것들의 상태를 'RESULT'로 바꾼다.
		UPDATE tbl_fkcmd_trans SET status='RESULT', update_time = GETDATE() WHERE device_id=@dev_id AND cmd_code='RESET_FK'
	END TRY
    BEGIN CATCH
		rollback transaction
		select @trans_id=''
		return -2
    END CATCH

	commit transaction
	return 0
END





GO
/****** Object:  StoredProcedure [dbo].[usp_receive_cmd]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_receive_cmd]
	-- Add the parameters for the stored procedure here
	@dev_id varchar(24),
	@trans_id varchar(16) output,
	@cmd_code varchar(32) output,
	@cmd_param_bin varbinary(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select @trans_id = ''
	-- 파라메터들을 검사한다.
	if @dev_id is null or len(@dev_id) = 0
		return -1
	
	begin transaction
	BEGIN TRY
		declare @trans_id_tmp as varchar(16)
		declare @csrTransId as cursor
		
		-- 먼저 tbl_fkcmd_trans 표에서 실행상태가 'RUN'인것들의 trans_id를 얻어낸다.
		set @csrTransId = Cursor For
			 select trans_id
			 from tbl_fkcmd_trans
			 where device_id=@dev_id AND status='RUN'
		
		-- tbl_fkcmd_trans_cmd_param, tbl_fkcmd_trans_cmd_result 표들에서 
		--  해당 trans_id에 해당한 레코드들을 삭제한다.
		Open @csrTransId
		Fetch Next From @csrTransId	Into @trans_id_tmp
		While(@@FETCH_STATUS = 0)
		begin
			DELETE FROM tbl_fkcmd_trans_cmd_param WHERE trans_id=@trans_id_tmp
			DELETE FROM tbl_fkcmd_trans_cmd_result WHERE trans_id=@trans_id_tmp
			Fetch Next From @csrTransId	Into @trans_id_tmp
		end
		close @csrTransId
	END TRY
    BEGIN CATCH
		rollback transaction
		select @trans_id=''
		return -2
    END CATCH
	
	-- tbl_fkcmd_trans 표에서 실행상태가 'RUN' 이던 트랜잭션들의 상태를 'CANCELLED'로 바꾼다.
	UPDATE tbl_fkcmd_trans SET status='CANCELLED', update_time = GETDATE() WHERE device_id=@dev_id AND status='RUN'
	if @@error <> 0
	begin
		rollback transaction
		return -2
	end
	commit transaction
	
		BEGIN TRY
		SELECT @trans_id=trans_id, @cmd_code=cmd_code FROM tbl_fkcmd_trans
		WHERE device_id=@dev_id AND status='WAIT' ORDER BY update_time DESC
		
		if @@ROWCOUNT = 0
		begin
			select @trans_id=''
			return -3
		end
		
		--  tbl_fkcmd_trans_cmd_param 표의 cmd_param 필드의 값을 출구파라메터 @cmd_param_bin에 설정한다.
		select @cmd_param_bin=cmd_param from tbl_fkcmd_trans_cmd_param
		where trans_id=@trans_id
		
		--  tbl_fkcmd_trans 표의 status 필드의 값을 'WAIT'로 바꾼다.
		UPDATE tbl_fkcmd_trans SET status='RUN', update_time = GETDATE() WHERE trans_id=@trans_id
	END TRY
    BEGIN CATCH
    	select @trans_id=''
		return -2
	END CATCH

	return 0
END -- proc: usp_receive_cmd



GO
/****** Object:  StoredProcedure [dbo].[usp_set_cmd_result]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_set_cmd_result]
	-- Add the parameters for the stored procedure here
	@dev_id varchar(24),
	@trans_id varchar(16),
	@return_code varchar(128),
	@cmd_result_bin varbinary(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- 파라메터들을 검사한다.
	if @dev_id is null or len(@dev_id) = 0
		return -1
	if @trans_id is null or len(@trans_id) = 0
		return -1
	
	begin transaction
	BEGIN TRY
		select trans_id from tbl_fkcmd_trans where trans_id = @trans_id and status='RUN'
		if @@ROWCOUNT != 1
		begin
			return -2
		end
		
		-- 먼저 tbl_fkcmd_trans_cmd_result 표에서 @trans_id에 해당한 레코드를 지우고 결과자료를 삽입한다.
		-- 만일 바이너리결과자료의 길이가 0이면 레코드를 삽입하지 않는다.
		delete from tbl_fkcmd_trans_cmd_result where trans_id=@trans_id
		if len(@cmd_result_bin) > 0
		begin
			insert into tbl_fkcmd_trans_cmd_result (trans_id, device_id, cmd_result) values(@trans_id, @dev_id, @cmd_result_bin)
		end
		
		-- tbl_fkcmd_trans 표에서 실행상태가 'RUN' 이던 트랜잭션들의 상태를 'RESULT'로 바꾼다.
		update tbl_fkcmd_trans set status='RESULT', return_code=@return_code, update_time = GETDATE() where trans_id=@trans_id and device_id=@dev_id and status='RUN'
	END TRY
    BEGIN CATCH
		rollback transaction
		return -3
    END CATCH
	
	if @@error <> 0
	begin
		rollback transaction
		return -3
	end
	commit transaction
	
	return 0
END -- proc: usp_set_cmd_result




GO
/****** Object:  StoredProcedure [dbo].[usp_update_device_conn_status]    Script Date: 02/07/2020 11:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_update_device_conn_status]
	-- Add the parameters for the stored procedure here
	@dev_id varchar(24),
	@dev_name varchar(24),
	@tm_last_update datetime,
	@fktm_last_update datetime,
	@dev_info varchar(256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @dev_registered int
	if len(@dev_id) < 1 
		return -1
	if len(@dev_name) < 1 
		return -1
	
	begin transaction
	
	SELECT @dev_registered = COUNT(device_id) from tbl_fkdevice_status WHERE device_id=@dev_id
	if  @dev_registered = 0
	begin
		INSERT INTO tbl_fkdevice_status( 
				device_id, 
				device_name, 
				connected, 
				last_update_time, 
				last_update_fk_time, 
				device_info)
			VALUES(
				@dev_id,
				@dev_name, 
				1,
				@tm_last_update,
				@fktm_last_update,
				@dev_info)
	end	
	else -- if @@ROWCOUNT = 0
	begin
		UPDATE tbl_fkdevice_status SET 
				device_id=@dev_id, 
				device_name=@dev_name, 
				connected=1,
				last_update_time=@tm_last_update,
				last_update_fk_time=@fktm_last_update,
				device_info=@dev_info
			WHERE 
				device_id=@dev_id
	end
	
	if @@error <> 0
	begin
		rollback transaction
		return -2
	end
	
	commit transaction
	return 0
END -- proc: usp_update_device_conn_status




GO
USE [master]
GO
ALTER DATABASE [iASWeb] SET  READ_WRITE 
GO
