﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginMain.aspx.cs" Inherits="LoginMain" %>

<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

    <link id="Link1" runat="server" rel="shortcut icon" href="~/images/logo.png" type="image/x-icon" />
    <title>iAS Web Application</title>
    <style type="text/css">
   .style1
    {
        width: 100%;
    }
        .style2
        {
            height: 23px;
        }
        .style8
        {
            height: 23px;
            width: 73px;
        }
        .style11
        {
            height: 23px;
            width: 157px;
        }
        .style12
        {
            width: 73px;
        }
        .style13
        {
            width: 157px;
        }
  
     body {
            background: url(./images/loginbg.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: 100% 100%;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='./images/loginbg.jpg',     sizingMethod='scale');
            -ms-filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='./images/loginbg', sizingMethod='scale');
        }
        .stretch {
            width:100%;
            height:100%;
        }
          </style>
      

</head>
<body>
    <form id="form1" runat="server">
    <br />
    <br />
    <br />
     <center>
    &nbsp;&nbsp;&nbsp;&nbsp;
         <table class="dxflInternalEditorTable_RedWine">
             <tr>
                 <td width="70%">
                     &nbsp;</td>
                 <td align="center">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br/>
                    <br/>
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Login" 
        Theme="SoftOrange" Width="316px">
        <PanelCollection>
<dx:PanelContent runat="server">
    <table class="style1">
        <tr>
            <td class="style12">
                &nbsp;</td>
            <td class="style13">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style12">
                <asp:Label ID="Label1" runat="server" Text="User Id   "></asp:Label>
            </td>
            <td align="center" class="style13">
                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" MaxLength="25" 
                    Theme="SoftOrange" Width="150px">
                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td align="left">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="ASPxTextBox1" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style12">
                &nbsp;</td>
            <td class="style13">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style12">
                <asp:Label ID="Label2" runat="server" Text="Password   "></asp:Label>
            </td>
            <td align="center" class="style13">
                <dx:ASPxTextBox ID="ASPxTextBox2" runat="server" MaxLength="25" 
                    Theme="SoftOrange" Width="150px" Password="True">
                </dx:ASPxTextBox>
            </td>
            <td align="left">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="ASPxTextBox2" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style12">
                &nbsp;</td>
            <td class="style13">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style12">
                &nbsp;</td>
            <td class="style13">
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Login" 
                    Theme="SoftOrange" ValidationGroup="1" OnClick="ASPxButton1_Click">
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style8">
            </td>
            <td class="style11">
            </td>
            <td class="style2">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td align="center">
                &nbsp;</td>
        </tr>
    </table>
            </dx:PanelContent>
</PanelCollection>
    </dx:ASPxRoundPanel>
    
                 </td>
             </tr>
         </table>
    </center>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
    <br />
    <br />
   
    </form>
</body>
</html>
