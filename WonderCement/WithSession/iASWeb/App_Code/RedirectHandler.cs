﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for RedirectHandler
/// </summary>
public class RedirectHandler
{
	public RedirectHandler()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext context)
    {
        //Your target URL here
        context.Response.Redirect("Login.aspx");
        context.Response.End();
    }
}