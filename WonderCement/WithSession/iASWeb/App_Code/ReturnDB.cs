using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


/// <summary>
/// Summary description for ReturnDB
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ReturnDB : System.Web.Services.WebService {

    SqlConnection con = null;
    SqlCommand cmd = null;
    SqlDataAdapter da = null;
    public ReturnDB () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public DataSet retrunPaycode()
    {
        using (con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con1"].ToString()))
        {
            cmd = new SqlCommand("Select paycode from tblemployee", con);
            da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
    }
}

