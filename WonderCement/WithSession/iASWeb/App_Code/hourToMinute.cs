using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for hourToMinute
/// </summary>
public class hourToMinute
{
    public int hour(string time)
    {      
        string[] values =time.Split(':');
        int h =Convert.ToInt32(values[0]);
         int m = Convert.ToInt32(values[1]);
        return h * 60 + m;
    }

    public string minutetohour(string min)
    {
        int latehours=Convert.ToInt32(Convert.ToInt32(min)/60);
        int latemin=Convert.ToInt32(Convert.ToInt32(min)%60);
        string totallate=latehours.ToString()+":"+latemin.ToString();
        return totallate;            
    }

    public string minutetohourNew(string min)
    {
        int latehours = Convert.ToInt32(Convert.ToInt32(min) / 60);
        int latemin = Convert.ToInt32(Convert.ToInt32(min) % 60);
        // string totallate = string.Format("{0:00}", Convert.ToInt32(latehours)) +":" + latemin.ToString();
        string totallate = string.Format("{0:00}", Convert.ToInt32(latehours)) + ":" + string.Format("{0:00}", Convert.ToInt32(latemin));
        return totallate;
    }
    public string minutetohour_New(string min)
    {
        TimeSpan sp = TimeSpan.FromMinutes(Convert.ToDouble(min));
        int latehours = (int)sp.TotalHours;
        double latemin = sp.Minutes;
        string totallate = latehours.ToString() + ":" + latemin.ToString("00");
        return totallate;
    }
    public string leaveAccural(string time)
    {
        string[] t = time.ToString().Split('.');
        string f = null;
        int length = Convert.ToInt32(t[0].Length);
        if (length == 1)
        {
            f = "00" + time.ToString() + "00";
        }
        else if (length == 2)
        {
            f = "0" + time.ToString() + "00";
        }
        else if (length == 3)
        {
            f = time.ToString() + "00";
        }
        return f;
    }
    
    //Separate alternate offdays by comma
    public string ReverseNumber(long number)
    {
        long digit = (long)number % 10;
        long rest = (long)number / 10;
        long o = digit;
        string a=null;
        while (rest > 0)
        {
            digit = rest % 10;
            rest /= 10;            
            a += ","+digit.ToString();
        }
        return a.Substring(1);
    }
    //Upper case first latter
    public string UppercaseFirst(string s)
    {
        char[] a = s.ToCharArray();
        a[0] = char.ToUpper(a[0]);
        return new string(a);
    }

    public hourToMinute()
	{       
		//
		// TODO: Add constructor logic here
		//
	}
}
