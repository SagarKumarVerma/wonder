using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Data.OleDb;
using System.Data.SqlClient;
 
/// <summary>
/// Summary description for Class_Connection
/// </summary>
/// 

public class Class_Connection
{
    OleDbConnection MyConn;
    OleDbCommand MyCmd;
    OleDbDataAdapter MyDa;
    DataSet MyDs;
    DataTable MyDT;
    OleDbDataReader MyRdr;

    SqlConnection MysqlConn;
    SqlCommand MysqlCmd;
    SqlDataAdapter MysqlDa;
    
    SqlDataReader MysqlRdr;
    //int result = 0;

    public Class_Connection()
    {        
    }
    public OleDbDataReader ExecuteReader(string str_Query)
    {
         
        MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
        if (MyConn.State == 0)
            MyConn.Open();
        MyCmd = new OleDbCommand(str_Query, MyConn);
        MyCmd.CommandTimeout = 18000;
        MyRdr = MyCmd.ExecuteReader(CommandBehavior.CloseConnection);
        return MyRdr;
    }
    public DataSet FillDataSet(string str_Query)
    {        
        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]))
        {
            MyCmd = new OleDbCommand();
            MyCmd.CommandTimeout = 18000;
            MyDa = new OleDbDataAdapter(str_Query, MyConn);
            MyDs = new DataSet();
            MyDa.Fill(MyDs);
            MyDa.Dispose();
            MyConn.Dispose();
        }
        return MyDs;
    }

    public DataTable FillDataTable(string StrQuery)
    {
         using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]))
        {
            MyCmd = new OleDbCommand();
            MyCmd.CommandTimeout = 18000;
            MyDa = new OleDbDataAdapter(StrQuery, MyConn);
            MyDT = new DataTable();
            MyDa.Fill(MyDT);
            MyDa.Dispose();
            MyConn.Dispose();
        }

         return MyDT;
    }
    public DataSet FillAccessDataSet(string str_Query)
    {
        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
        {
            MyCmd = new OleDbCommand();
            MyCmd.CommandTimeout = 18000;
            MyDa = new OleDbDataAdapter(str_Query, MyConn);
            MyDs = new DataSet();
            MyDa.Fill(MyDs, "tbl");
            MyDa.Dispose();
            MyConn.Dispose();
        }
        return MyDs;
    }
    public void ChkValidation()
    {
        if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            System.Web.HttpContext.Current.Response.Redirect("default.aspx");      
  
        if (System.Web.HttpContext.Current.Session["UserDs"] == null)
            System.Web.HttpContext.Current.Response.Redirect("default.aspx");

        DataSet ds;
        ds = (DataSet)System.Web.HttpContext.Current.Session["UserDs"];

        if (System.Web.HttpContext.Current.Session["LoggedUser"].ToString() != ds.Tables[0].Rows[0]["user_r"].ToString().Trim().ToUpper())
            System.Web.HttpContext.Current.Response.Redirect("default.aspx");
    }
    public int execute_NonQuery(string strsql)
    {
        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
        {
            MyConn.Open();
            MyCmd = new OleDbCommand(strsql, MyConn);            
            MyCmd.CommandTimeout = 18000;
            int result = MyCmd.ExecuteNonQuery();
            return result;        
        }    
    }
    public int execute_Scalar(string strsql)
    {
        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
        {
            MyConn.Open();// 
            MyCmd = new OleDbCommand(strsql, MyConn);            
            MyCmd.CommandTimeout = 18000;
            int result =(int)MyCmd.ExecuteScalar();
            return result;
        }
    }
    public string ServerDate()
    {
        string  ServerDate =DateTime.Now.ToString("yyyy-MM-dd HH:mm"); 
        return ServerDate; 
    }
    public OleDbDataReader Execute_Reader(String Qry)
    {
       MyRdr= ExecuteReader(Qry);
       return MyRdr;

        //using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
        //{
        //    if (MyConn.State == 0)
        //    {
        //        MyConn.Open();
        //    }
        //    MyCmd = new OleDbCommand(Qry, MyConn);
        //    MyRdr = MyCmd.ExecuteReader();
        //    return MyRdr;
        //}
    }


   /* public SqlDataReader ExecuteReadersql(string str_Query)
    {
        MysqlConn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString1"]);
        if (MysqlConn.State == 0)
            MysqlConn.Open();
        MysqlCmd = new SqlCommand(str_Query, MysqlConn);
        MysqlCmd.CommandTimeout = 18000;
        MysqlRdr = MysqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
        return MysqlRdr;
    }
    public DataSet FillDataSetsql(string str_Query)
    {
        MysqlConn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString1"]);
        {
            MysqlCmd = new SqlCommand();
            MysqlCmd.CommandTimeout = 18000;
            MysqlDa = new SqlDataAdapter(str_Query, MysqlConn);
            MyDs = new DataSet();
            MysqlDa.Fill(MyDs, "tbl");
            MysqlDa.Dispose();
            MysqlConn.Dispose();
        }
        return MyDs;
    }    
    
    public int execute_NonQuery(string strsql)
    {
        MysqlConn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString1"]);
        {
            MysqlConn.Open();
            MysqlCmd = new SqlCommand(strsql, MysqlConn);
            MysqlCmd.CommandTimeout = 18000;
            int result = MysqlCmd.ExecuteNonQuery();
            return result;
        }
    }
    public int execute_Scalarsql(string strsql)
    {
        MysqlConn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString1"]);
        {
            MysqlConn.Open();// 
            MysqlCmd = new SqlCommand(strsql, MysqlConn);
            MysqlCmd.CommandTimeout = 18000;
            int result = (int)MysqlCmd.ExecuteScalar();
            return result;
        }
    }    
    public SqlDataReader Execute_Readersql(String Qry)
    {
        MysqlRdr = ExecuteReadersql(Qry);
        return MysqlRdr;
    }*/
    public bool chkvalid(string str)
    {
        str = str.ToLower().ToString().Trim();
        //"xp_ ", "update ", "insert ", "select ", "drop ", "alter ", "create ", "rename ", "delete ", "replace " 
        /*str = str.Replace("'", "''"); // Most important one! This line alone can prevent most injection attacks
        str = str.Replace("--", "");
        str = str.Replace("[", "[[]");
        str= str.Replace("%", "[%]");*/

        /* if ((str.Contains("-- ")) || (str.Contains("or ")) || (str.Contains("xp_ ")) || (str.Contains("update ")) || (str.Contains("insert "))
             || (str.Contains("select ")) || (str.Contains("drop ")) || (str.Contains("alter ")) || (str.Contains("create "))
             || (str.Contains("rename ")) || (str.Contains("delete ")) || (str.Contains("replace ")))*/

        if (str.Contains(" delete "))
            return false;
        else if (str.Contains(" -- "))
            return false;
        else if (str.Contains(" xp_ "))
            return false;
        else if (str.Contains(" drop "))
            return false;

        if (str.Contains(" select "))
            return false;
        else if (str.Contains(" alter "))
            return false;
        else if (str.Contains(" rename "))
            return false;
        else if (str.Contains(" replace "))
            return false;

        else if (str.Contains(" create "))
            return false;
        else if (str.Contains(" insert "))
            return false;
        else if (str.Contains(" or "))
            return false;
        else if (str.Contains(" ' "))
            return false;
        else
            return true;
    }

    public DataSet FillDataSetWinPack(string str_Query)
    {
        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["WinpackCon"]))
        {
            MyCmd = new OleDbCommand();
            MyCmd.CommandTimeout = 18000;
            MyDa = new OleDbDataAdapter(str_Query, MyConn);
            MyDs = new DataSet();
            MyDa.Fill(MyDs);
            MyDa.Dispose();
            MyConn.Dispose();
        }
        return MyDs;
    }
    

    
}
