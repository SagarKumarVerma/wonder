﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DataFilter
/// </summary>
public class DataFilter
{
    Class_Connection Con = new Class_Connection();
    public static DataTable EmpNonAdmin = new DataTable("TBLEmployee");
    public static DataTable LocationNonAdmin = new DataTable("tbldepartment");
    public static DataTable GradeNonAdmin = new DataTable("tblGrade");
    public static DataTable CatNonAdmin = new DataTable("tblCategory");
    public static DataTable CompanyNonAdmin = new DataTable("tblCompany");
    public static DataTable DivNonAdmin = new DataTable("tblDivision");
    public static DataTable ShiftNonAdmin = new DataTable("tblShiftMaster");
    public static string[] EmpArr;
    public static string[] ComArr;
    string sSql = string.Empty;
    DataSet DsNew = new DataSet();
    //public static string LoginUserName = string.Empty;
    //public static string LoginUserType = string.Empty;
    //public static string LoginPayCode = string.Empty;
    //public static string LoginCompany = string.Empty;
    public static int ClientID = 0;
    public static int LoginClientID = 1;
    int Result = 0;

    #region globalveriables
   public static string AuthComp = "";
   public static string AuthDept = "";
    #endregion
    public DataFilter()
	{

        
		
	}

    public static void LoadCompany(string LoginUserName)
    {
        try
        {
            CompanyNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string Strsql = "Select companycode,companycode+'-'+companyname as 'compDetails' from tblCompany";
            string Strsql = "Select * from tblCompany ";
            if (LoginUserName.ToUpper() != "ADMIN")
            {
                Strsql += " where companycode in (" + DataFilter.AuthComp.Trim() + ") ";
            }
            else
            {
                //Strsql += " where companycode ='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(CompanyNonAdmin);
        }
        catch
        {

        }
    }
    public static void LoadDepartment(string LoginUserName, string LoginCompany)
    {
        try
        {
            LocationNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            string Strsql = "Select * from tblDepartment ";
            if (LoginUserName.ToUpper() != "ADMIN")
            {
                Strsql += " where DepartmentCode in (" + DataFilter.AuthDept.Trim() + ") and  CompanyCode='" + LoginCompany + "' ";
            }
            else
            {
                Strsql += " where  CompanyCode='" + LoginCompany + "' ";
            }
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(LocationNonAdmin);

        }
        catch
        {

        }
    }
    public static void LoadGgrade(String LoginCompany)
    {
        try
        {
            GradeNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string Strsql = "select *  from tblGrade";
            string Strsql = "select * from tblGrade where   CompanyCode='" + LoginCompany + "'";
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(GradeNonAdmin);

        }
        catch
        {

        }
    }
    public static void LoadCat(string LoginCompany)
    {
        try
        {
            CatNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string Strsql = "select * from tblcategory";
            string Strsql = "select CAT,CATAGORYNAME from tblCatagory where   CompanyCode='" + LoginCompany + "'";
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(CatNonAdmin);

        }
        catch
        {

        }
    }
    public static void LoadDivision(string LoginCompany)
    {
        try
        {
            DivNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string Strsql = "select * from tbldivision";
            string Strsql = "select * from tblDivision where   CompanyCode='" + LoginCompany + "'";
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(DivNonAdmin);

        }
        catch
        {

        }
    }

    public static void Loademployee(String LoginCompany)
    {
        try
        {
            EmpNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string Strsql = "select * from tblemployee where companycode in ("+AuthComp.Trim()+") and departmentcode in ("+AuthDept.Trim()+")";
            string Strsql = "select paycode,Empname from tblemployee where active='Y' and  CompanyCode='" + LoginCompany + "'";
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(EmpNonAdmin);

        }
        catch
        {

        }
    }

    public static void Loadshift(string LoginCompany)
    {
        try
        {
            ShiftNonAdmin.Clear();
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
            //string Strsql = "select * from tbldivision";
            string Strsql = "select * from tblshiftmaster where   CompanyCode='" + LoginCompany + "'";
            DataSet DsLoc = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(Strsql, Conn);
            SDA.Fill(ShiftNonAdmin);

        }
        catch
        {

        }
    }
    public static void GetClientID(string LoginUserName, string LoginCompany)
    {
        try
        { string sSql="";
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
             if (LoginUserName.Trim().ToUpper() != "ADMIN")
             {
                 sSql = "select Id from tblCompany Where COMPANYCODE='" + LoginCompany.Trim() + "'";
             }
             else
             {
                 sSql = "select Id from tblCompany Where COMPANYCODE='" + LoginCompany.Trim() + "'";
             }
             
            DataSet DsCID = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(sSql, Conn);
            SDA.Fill(DsCID);
            if(DsCID.Tables[0].Rows.Count>0)
            {
                ClientID = Convert.ToInt32(DsCID.Tables[0].Rows[0][0].ToString().Trim());
            }
            else
            {
                ClientID = 0;
            }

        }
        catch
        {

        }
    }

}