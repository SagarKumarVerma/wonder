using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public class Module_UPD
{
    string mStatus ="";
    double mPresentValue = 0;
    double mLeaveValue = 0;
    double mAbsentValue = 0;
    double mWO_Value = 0;
    double mHoliday_Value = 0;
    string strsql = "";
    Class_Connection con = new Class_Connection();
	public Module_UPD()
	{

	}
    //public void Upd(string Pay, DateTime doffice, double lvamount, double lvamount1, double lvamount2, string lvcode, string FirstHalfLeavecode, string SecondHalfLeavecode, string lvtype, string lvtype1, string lvtype2, string shiftattended, string IsHld, string mOldStatus,double PV,double AV,double LV)
    public void Upd(string Pay, DateTime doffice, double lvamount, double lvamount1, double lvamount2, string lvcode, string FirstHalfLeavecode, string SecondHalfLeavecode, string lvtype, string lvtype1, string lvtype2, string shiftattended, string IsHld, string mOldStatus)
    {
        mStatus = "";
        mPresentValue = 0;
        mLeaveValue = 0;
        mAbsentValue = 0;
        mWO_Value = 0;
        mHoliday_Value = 0;
        if (lvamount > 0 )
        {
          if( lvamount == 0.5)
          {
            mStatus = "H_";
          }
          else
          {
            mStatus = "";
          }
        }
        mStatus = mStatus.ToString().Trim() +""+ lvcode.ToString();
        if (!string.IsNullOrEmpty(mOldStatus))
        {
            if (lvtype.ToString().Trim() == "P")
            {
                mPresentValue = 1;
            }
            else if (lvtype.ToString().Trim() == "L")
            {
                mLeaveValue = 0 + lvamount;
                mPresentValue = 1 - lvamount;
            }
            else
            {
                mAbsentValue = 0 + lvamount;
                mPresentValue = 1 - lvamount;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(lvtype1) && !string.IsNullOrEmpty(lvtype2))
            {
                mStatus = FirstHalfLeavecode.ToString().Trim() + "" + SecondHalfLeavecode.ToString().Trim();
                if ((lvtype1.ToString().Trim() == "P" && lvtype2.ToString().Trim() == "A") || (lvtype1.ToString().Trim() == "A" && lvtype2.ToString().Trim() == "P"))
                {
                    mPresentValue = 0.5;
                    mLeaveValue = 0;
                    mAbsentValue = 0.5;
                }
                if ((lvtype1.ToString().Trim() == "L" && lvtype2.ToString().Trim() == "P") || (lvtype1.ToString().Trim() == "P" && lvtype2.ToString().Trim() == "L"))
                {
                    mPresentValue = 0.5;
                    mLeaveValue = 0.5;
                    mAbsentValue = 0;
                }
                if ((lvtype1.ToString().Trim() == "L" && lvtype2.ToString().Trim() == "A") || (lvtype1.ToString().Trim() == "A" && lvtype2.ToString().Trim() == "L"))
                {
                    mPresentValue = 0;
                    mLeaveValue = 0.5;
                    mAbsentValue = 0.5;
                }
                if ((lvtype1.ToString().Trim() == "L") && (lvtype2.ToString().Trim() == "L"))
                {
                    mPresentValue = 0;
                    mLeaveValue = 1;
                    mAbsentValue = 0;
                }
                if ((lvtype1.ToString().Trim() == "A") && (lvtype2.ToString().Trim() == "A"))
                {
                    mPresentValue = 0;
                    mLeaveValue = 0;
                    mAbsentValue = 1;
                }
                if ((lvtype1.ToString().Trim() == "P") && (lvtype2.ToString().Trim() == "P"))
                {
                    mPresentValue = 1;
                    mLeaveValue = 0;
                    mAbsentValue = 0;
                }
            }
            else
            {
                if (lvtype.ToString().Trim() == "P")
                {
                    mPresentValue = lvamount;
                }
                else if (lvtype.ToString().Trim() == "L")
                {
                    mLeaveValue = lvamount;
                }
                else if (lvtype.ToString().Trim() == "A")
                {
                    mAbsentValue = lvamount;
                }

                if (shiftattended.ToString().Trim().ToUpper() == "OFF")
                {
                    mWO_Value = 1 - lvamount;
                }
                else if (IsHld.ToString().Trim() == "Y")
                {
                    mHoliday_Value = 1 - lvamount;
                }
                else
                {
                    if (lvtype.ToString().Trim() != "A")
                    {
                        mAbsentValue = 1 - lvamount;
                    }
                }
            }
        }
         strsql = "Update tblTimeRegister Set status='"+mStatus.ToString()+"', presentvalue="+mPresentValue +",absentvalue="+mAbsentValue +",leavevalue="+mLeaveValue+", "+
            " holiday_value=" + mHoliday_Value + ",wo_value=" + mWO_Value + "  Where paycode='" + Pay.ToString().Trim() + "' and dateoffice='" + doffice.ToString("yyyy-MM-dd") + "'  ";
            con.execute_NonQuery(strsql);        
    }
}
