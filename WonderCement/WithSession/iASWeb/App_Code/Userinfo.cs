﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
///Userinfo 的摘要说明
/// </summary>
public class Userinfo
{
    public Userinfo()
    {
    }

    public string user_id = null;
    public string user_name = null;
    public string user_privilege = null;
    public string user_photo = null;
    public Enroll_Data[] enroll_data_array = null;

    public byte[] photoData = null;
    public byte[] faceData = null;
    public byte[][] fpData = new byte[10][];
    public string cardData = null;
    public string pwdData = null;

    private string checkNull(string str)
    {
        return str == null ? "NULL" : str;
    }

    private string checkByteData(byte[] data)
    {
        return data == null ? "NULL" : data.Length.ToString();
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("user_id = ").Append(checkNull(user_id))
            .Append(",user_name =").Append(checkNull(user_name))
            .Append(",user_privilege =").Append(checkNull(user_privilege))
            .Append(",user_photo =").Append(checkNull(user_photo))
            .Append(",enroll_data_array = [");
        foreach (Enroll_Data data in enroll_data_array)
        {
            sb.Append("\"").Append(data.enroll_data).Append(",").Append(data.backup_number).Append("\";");
        }
        sb.Append("],").Append("cardData = ")
            .Append(checkNull(cardData)).Append(",pwdData = ")
            .Append(checkNull(pwdData)).Append(",photoData = ")
            .Append(checkByteData(photoData)).Append(",faceData = ")
            .Append(checkByteData(faceData))
            .Append(",fpData = [");
        for (int i = 0; i < fpData.Length; i++)
        {
            sb.Append("\"").Append(checkByteData(fpData[i])).Append("\";");
        }
        sb.Append("]");
        return sb.ToString();
    }
}

public class Enroll_Data
{
    public Enroll_Data()
    {
    }

    public int backup_number;
    public string enroll_data;
}