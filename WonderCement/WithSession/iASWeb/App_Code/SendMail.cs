using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


/// <summary>
/// Summary description for SendMail
/// </summary>
public class SendMail
{

    
	public SendMail()
	{
		//
		// TODO: Add constructor logic here//
		//
	}
    public static bool SendEMail(string ToMail, string strSubject, string msg)
    {
        Class_Connection cn = new Class_Connection();
        //try
        //{
        //    MailMessage message ;
        //    if(FromMail.ToString().Trim()=="") 
        //      message = new MailMessage(ConfigurationSettings.AppSettings["FromMail"].ToString(), strTo, strSubject, msgBody);
        //    else 
        //      message = new MailMessage(FromMail.ToString().Trim(), strTo.ToString().Trim(), strSubject, msgBody);
            
        //    SmtpClient emailClient = new SmtpClient(ConfigurationSettings.AppSettings["SmtpServer"].ToString());
        //    System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SmtpUser"].ToString(), ConfigurationSettings.AppSettings["SmtpPass"].ToString());
        //    message.IsBodyHtml = true;
        //    emailClient.UseDefaultCredentials = false;
        //    emailClient.Credentials = SMTPUserInfo;
        //    emailClient.Send(message);
        //    return true;
        //}
        //catch(Exception ex)
        //{
        //    return false;
        //}


        string server = "";
        string username = "";
        string password = "";
        string strsql = "";
        string Strsql = "";
        string frommail = "";
        string ssl = "";
        int port = 0;
        strsql = "select server,sender,port,sendername,username,password,enablessl from tblmailsetup";
         DataSet ds = new DataSet();
         ds = cn.FillDataSet(strsql);
         if (ds.Tables[0].Rows.Count > 0)
         {

             server = ds.Tables[0].Rows[0]["server"].ToString().Trim();
             username = ds.Tables[0].Rows[0]["username"].ToString().Trim();
             password = ds.Tables[0].Rows[0]["password"].ToString().Trim();
             frommail = ds.Tables[0].Rows[0]["sender"].ToString().Trim();
             port = Convert.ToInt32(ds.Tables[0].Rows[0]["port"].ToString().Trim());
             ssl = ds.Tables[0].Rows[0]["enablessl"].ToString().Trim();
         }
         else
         {
             server = ConfigurationSettings.AppSettings["SmtpServer"].ToString();
             username = ConfigurationSettings.AppSettings["SmtpUser"].ToString();
             password = ConfigurationSettings.AppSettings["SmtpPass"].ToString();
             frommail = ConfigurationSettings.AppSettings["FromMail"].ToString();
             port = Convert.ToInt32(ConfigurationSettings.AppSettings["smtpport"].ToString());
         }





         MailMessage message = new MailMessage(frommail, ToMail, strSubject, msg.ToString());
         SmtpClient emailClient;
         if (port == 0)
         {
             emailClient = new SmtpClient(server.ToString());
         }
         else
         {
             emailClient = new SmtpClient(server.ToString(), port);
         }
         //SmtpClient emailClient = new SmtpClient(server.ToString(), port);
         System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(username.ToString().Trim(), password.ToString().Trim());
         emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
         emailClient.UseDefaultCredentials = false;
         message.IsBodyHtml = true;
         //Attachment at = new Attachment(filename.ToString());
         //message.Attachments.Add(at);
         emailClient.Credentials = SMTPUserInfo;
         if (ssl == "Y")
         {
             emailClient.EnableSsl = true;
         }
         else
         {
             emailClient.EnableSsl = false;
         }

         try
         {
             emailClient.Send(message);
             return true;
         }
         catch (Exception ex)
         {
             string sSql = "insert into ZKServiceErrorLog (ErrorMessage)values('" + ex.Message.ToString().Trim() + "')";
             cn.execute_NonQuery(sSql);
             return false;
         }


        //Test
        //SmtpClient Smtp_Server = new SmtpClient();
        //MailMessage e_mail = new MailMessage();
        //System.Net.Mail.Attachment attachment;
        //Smtp_Server.UseDefaultCredentials = false;

        // Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
        //Smtp_Server.Credentials = new System.Net.NetworkCredential(username, password);
        //Smtp_Server.Port = Convert.ToInt16(port); // 587
        //Smtp_Server.EnableSsl = true; // True
        //Smtp_Server.Host = server; // "smtp.gmail.com"
        //e_mail = new MailMessage();
        // e_mail.From = New MailAddress("nittslam@gmail.com")
        //e_mail.From = new MailAddress(frommail);
        //try
        //{

        //    e_mail.To.Add(ToMail.Trim());

        //    e_mail.IsBodyHtml = true;
        //    e_mail.IsBodyHtml = true;
        //    e_mail.Subject = "iAS Test Mail";
        //    e_mail.Body = "";
        //    Smtp_Server.Send(e_mail);
        //    return true;
        //}
        //catch (Exception)
        //{

        //    return false;
        //}

    }

    public static bool SendEMailSL(string ToMail, string strSubject, string msg, string filename)
    {
        string server = "";
        string username = "";
        string password = "";
        string strsql = "";
        string Strsql = "";
        string frommail = "";
        int port = 0;

        server = ConfigurationSettings.AppSettings["SmtpServer"].ToString();
        username = ConfigurationSettings.AppSettings["SmtpUser"].ToString();
        password = ConfigurationSettings.AppSettings["SmtpPass"].ToString();
        frommail = ConfigurationSettings.AppSettings["FromMail"].ToString();
        port = Convert.ToInt32(ConfigurationSettings.AppSettings["smtpport"].ToString());

        MailMessage message = new MailMessage(frommail, ToMail, strSubject, msg.ToString());
        SmtpClient emailClient = new SmtpClient(server.ToString(), port);
        System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(username.ToString(), password.ToString());
        emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        emailClient.UseDefaultCredentials = false;
        message.IsBodyHtml = true;
        if(!string.IsNullOrEmpty(filename.ToString()))
        {
            Attachment at = new Attachment(filename.ToString());
            message.Attachments.Add(at);
        }
        emailClient.Credentials = SMTPUserInfo;
        emailClient.EnableSsl = true;
        try
        {
            emailClient.Send(message);
            return true;
        }
        catch
        {
            return false;
        }

    }



}
