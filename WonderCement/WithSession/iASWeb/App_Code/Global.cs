﻿using System;
using System.Collections.Generic;
using System.Web;


namespace GlobalSettings
{
    public class Global
    {

        /*lblCardNo.Text = ds.Tables[0].Rows[0]["Showas"].ToString().Trim();
           lblEmpCode.Text = ds.Tables[0].Rows[1]["Showas"].ToString().Trim();
           lblName.Text = ds.Tables[0].Rows[2]["Showas"].ToString().Trim();
           lblFather.Text = ds.Tables[0].Rows[3]["Showas"].ToString().Trim();
           lblCompany.Text = ds.Tables[0].Rows[4]["Showas"].ToString().Trim();
           lblCom1.Text = ds.Tables[0].Rows[4]["Showas"].ToString().Trim();
           lblDepartment.Text = ds.Tables[0].Rows[5]["Showas"].ToString().Trim();
           lbldept1.Text = ds.Tables[0].Rows[5]["Showas"].ToString().Trim();
           lblCatagory.Text = ds.Tables[0].Rows[6]["Showas"].ToString().Trim();
           lblCat1.Text = ds.Tables[0].Rows[6]["Showas"].ToString().Trim();
           lblSection.Text = ds.Tables[0].Rows[7]["Showas"].ToString().Trim();
           lblSec1.Text = ds.Tables[0].Rows[7]["Showas"].ToString().Trim();
           lblGrade.Text = ds.Tables[0].Rows[8]["Showas"].ToString().Trim();
           lblGrade1.Text = ds.Tables[0].Rows[8]["Showas"].ToString().Trim();
           lblDesignation.Text = ds.Tables[0].Rows[9]["Showas"].ToString().Trim();
           lblBranch.Text = ds.Tables[0].Rows[10]["Showas"].ToString().Trim();
           lblMsg.Text = ds.Tables[0].Rows[11]["Showas"].ToString().Trim();*/


        private static string G_GuardianName = "";
        private static string G_Name = "";
        private static string G_Card = "";
        private static string G_Paycode = "";
        private static string G_Dept = "";
        private static string G_Designation = "";
        private static string G_Compnay = "";

        private static string G_Category = "";
        private static string G_Grade = "";
        private static string G_Branch = "";

        private static string G_Section = "";
        private static string G_Msg = "";
        

        public Global()
        {

        }
        public static class getEmpInfo
        {
            public static string _Paycode
            {
                get
                {
                    return G_Paycode;
                }
                set
                {
                    G_Paycode  = value;
                }
            }

            public static string _Designation
            {
                get
                {
                    return G_Designation;
                }
                set
                {
                    G_Designation = value;
                }
            }




            public static string _Dept
            {
                get
                {
                    return G_Dept;
                }
                set
                {
                    G_Dept = value;
                }
            }

            public static string _CardNo
            {
                get
                {
                    return G_Card;
                }
                set
                {
                    G_Card  = value;
                }
            }



            ///

            public static string _EmpName
            {
                get
                {
                    return G_Name;
                }
                set
                {
                    G_Name = value;
                }
            }

            public static string _Company
            {
                get
                {
                    return G_Compnay;
                }
                set
                {
                    G_Compnay = value;
                }
            }




            public static string _Category
            {
                get
                {
                    return G_Category;
                }
                set
                {
                    G_Category = value;
                }
            }

            public static string _Grade
            {
                get
                {
                    return G_Grade;
                }
                set
                {
                    G_Grade = value;
                }
            }
            ///////
            public static string _GuardianName 
            {
                get
                {
                    return G_GuardianName;
                }
                set
                {
                    G_GuardianName = value;
                }
            }

            public static string _Msg
            {
                get
                {
                    return G_Msg;
                }
                set
                {
                    G_Msg = value;
                }
            }




            public static string _Section
            {
                get
                {
                    return G_Section;
                }
                set
                {
                    G_Section = value;
                }
            }

            public static string _Branch
            {
                get
                {
                    return G_Branch;
                }
                set
                {
                    G_Branch = value;
                }
            }


  
        }
    }
}