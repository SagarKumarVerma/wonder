﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Threading;
using System.Data.OleDb;
using System.Configuration;
using System.Diagnostics;
using GlobalSettings;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;

using System.Text;
using System.IO;
using DevExpress.Web;
public partial class Attendance : System.Web.UI.Page
{
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    Class_Connection cn = new Class_Connection();
    ErrorClass ec = new ErrorClass();
    Setting_App Settings = new Setting_App();
    OleDbDataReader dr;
    public string txtFromDateCID = "";
    public string txtToDateCID = "";
    DateTime fd = System.DateTime.Today;
    string Pcode, StrSql;
    string Strsql = null;
    DataSet ds = null;
    string ReportView = "";
    string a = "Attendence Report";
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));

        ec.Write_Log(PageName, FunctionName, ErrorMsg);
    }
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
        }

        try
        {
            // Put user code to initialize the page here
            if (Session["PAYCODE"] == null || Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //txtFromDateCID = txtfromdate.ClientID;
            //txtToDateCID = txttodate.ClientID;

            if (Session["usertype"].ToString().Trim().ToUpper() == "U")
            {
                //TD1.Visible = false;
                //tdrad.Visible = false;
            }
            if (!IsPostBack)
            {
               // lblMsg.Text = "Attendance View";
                grdrequest.Visible = false;
               // txtfromdate.Text = "01/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString();
                txtfromdate.Value = Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-01"));
                txttodate.Value = Convert.ToDateTime(fd.ToString("yyyy-MM-dd"));

                LblEmpName.Text = Session["UserName"].ToString().Trim() + " ( " + Session["PAYCODE"].ToString() + " ) ";
                //if (Session["usertype"].ToString() != "U" || Session["Trainer"].ToString() == "Y")
                //{
                //    Fill_Combo();
                //}
                Fill_Combo();
                bindFields();
                if (Session["PAYCODE"].ToString().ToUpper() != "ADMIN")
                {
                    UpdateTimeRegister();
                }
                BindGrid();
                //if ((Settings.View_InActive.ToString() == "Y") && (Session["usertype"].ToString().Trim() == "A"))
                //{
                //    ChkInActive.Visible = true;
                //}
                //radSelectType.Items[0].Text = Global.getEmpInfo._EmpName;
                //radSelectType.Items[1].Text = Global.getEmpInfo._Paycode;



            }
        }
        catch (Exception er)
        {
            Error_Occured("Form Load", er.Message);
        }
    }
    protected void bindFields()
    {
        /*Strsql = "select * from tblMapping";
        ds = new DataSet();
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {                
            lblEmp.Text = ds.Tables[0].Rows[11]["Showas"].ToString().Trim();
        }*/
        //lblEmp.Text = Global.getEmpInfo._Msg.ToString();



    }
    private void Fill_Combo()
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        try
        {
            StrSql = "";
            if (Session["usertype"].ToString() == "C")  //HOD
            {
                StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where hod_code ='" + Session["PAYCODE"].ToString() + "'  and active='Y' order by empname";
            }
            else if (Session["usertype"].ToString() == "H")  //Reproting Manager 
            {
                if (ReportView.ToString().Trim() == "Y")
                {
                    StrSql = "";
                    StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and active='Y' order by empname";
                }
                else
                {
                    StrSql = "";
                    StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y'  ";
                    if (Session["Auth_Comp"] != null)
                    {
                        StrSql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        StrSql += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                    }
                    StrSql += " order by empname";
                }
            }
            else if (Session["usertype"].ToString() == "A")  //Admin
            {
                StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y'  ";
                if ((Session["usertype"].ToString() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
                {
                    StrSql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim()+ ")";
                }
                if ((Session["usertype"].ToString() == "A") && (Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
                {
                    StrSql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
                StrSql += " Order by EmpName ";
            }

            else if (Session["usertype"].ToString() == "U")  //Admin
            {
                StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' and paycode='" + Session["LoginUserName"].ToString().Trim() +"'  ";
               
            }
            if (StrSql.Trim() == "")
                return;
            DataSet DsEmpName = new DataSet();
            DsEmpName = cn.FillDataSet(StrSql);
            EmpCombo.DataSource = DsEmpName;
            EmpCombo.TextField = "EmpName";
            EmpCombo.ValueField = "Paycode";
            EmpCombo.DataBind();
            EmpCombo.SelectedIndex = -1;
            if (Session["usertype"].ToString() != "A")  //Admin
            {
                //EmpCombo.Items.Insert(0, new ListItem(Session["UserName"].ToString().Trim() + " - " + Session["PAYCODE"].ToString(), Session["PAYCODE"].ToString()));
            }
            //if (EmpCombo.Items.Count > 0)
            //    TD1.Visible = true;
            //else
            //    TD1.Visible = false;
        }
        catch (Exception er)
        {
            Error_Occured("Fill_Combo", er.Message);
        }
    }
    private void BindGrid()
    {
        //try
        //{
        string strsql, strsql1;
        //string strsql, strsql1;
        string msg = "";
          
        DateTime dateFrom = System.DateTime.MinValue;
        DateTime dateTo = System.DateTime.MinValue;
        try
        {
            dateFrom = Convert.ToDateTime(txtfromdate.Value);//DateTime.ParseExact(txtfromdate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dateTo = Convert.ToDateTime(txttodate.Value);
        }
        catch (Exception ex)
        {
            //msg = "<script lanaguage='javascript'>alert('Invalid date. ')</script>";
            //Page.RegisterStartupScript("msg", msg);
            //txtfromdate.Text = "01/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString();
            //txttodate.Text = fd.ToString("dd/MM/yyyy");
            return;
        }
        int i = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(dateFrom.Year.ToString("0000")));
        int j = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(dateTo.Year.ToString("0000")));
        //Response.Write(i.ToString() + "------" + j.ToString());

        if (i < 0)
        {
            //msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
            //Page.RegisterStartupScript("msg", msg);
            //txtfromdate.Focus();
            //return;
        }
        if (j < 0)
        {
            //msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
            //Page.RegisterStartupScript("msg", msg);
            //txttodate.Focus();
            return;
        }            // 
        if (dateTo > fd)
        {
            dateTo = fd;
        }

        if (dateTo < dateFrom)
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('To Date Must Be Greater Than From Date');", true);
            return;
        }
        
        if (!IsPostBack && Session["usertype"].ToString().ToUpper() == "A")
            return;

        if (Session["usertype"].ToString().Trim().ToUpper() == "U")
        {
            Pcode = Session["PAYCODE"].ToString();
            LblEmpName.Text = Session["username"].ToString().Trim();
        }
        else if (EmpCombo.SelectedIndex == -1)
        {
            msg = "<script lanaguage='javascript'>alert('Please Select Employee')</script>";
            Page.RegisterStartupScript("msg", msg);
            return;
        }
        else
        {
            try
            {
                if (EmpCombo.Items.Count > 0)
                {
                    Pcode = EmpCombo.SelectedItem.Value.ToString();
                    LblEmpName.Text = EmpCombo.SelectedItem.Text.Trim();
                }
                else
                {
                    Pcode = Session["PAYCODE"].ToString();
                    LblEmpName.Text = Session["username"].ToString().Trim();
                }
            }
            catch
            {
                
                
            }
        }

        DataSet dsPresent = new DataSet();
        DataSet dsLate = new DataSet();
        strsql = "";
        strsql = "select paycode, isnull(sum(ApprovedOT),0) ApprovedOT, sum(PRESENTVALUE) totpresent,sum(ABSENTVALUE) totabsent,sum(LEAVEVALUE) totleave,sum(WO_VALUE) wovalue,sum(HOLIDAY_VALUE) hovalue,sum(HOURSWORKED) hoursworked from tbltimeregister " +
          " where paycode='" + Pcode.ToString() + "' and dateoffice Between '" + dateFrom.ToString("MMM dd yyyy") + "' AND '" + dateTo.ToString("MMM dd yyyy") + "' group by paycode";
        dsPresent = cn.FillDataSet(strsql);
        if (dsPresent.Tables[0].Rows.Count > 0)
        {
            lblPresent.Text = "";
            lblApprovedLeave.Text = "";
            lblweeklyoff.Text = "";
            lbldaysabsent.Text = "";
            lblHoliday.Text = "";
            lblPresent.Text = dsPresent.Tables[0].Rows[0]["totpresent"].ToString();
            lblApprovedLeave.Text = dsPresent.Tables[0].Rows[0]["totleave"].ToString();
            lblweeklyoff.Text = dsPresent.Tables[0].Rows[0]["wovalue"].ToString();
            lbldaysabsent.Text = dsPresent.Tables[0].Rows[0]["totabsent"].ToString();
            lblHoliday.Text = dsPresent.Tables[0].Rows[0]["hovalue"].ToString();
        }
        else
        {
            lblPresent.Text = "";
            lblApprovedLeave.Text = "";
            lblweeklyoff.Text = "";
            lbldaysabsent.Text = "";
            lblHoliday.Text = "";
        }
        strsql1 = "";
        strsql1 = "select count(*) latearrival  from tbltimeregister where paycode='" + Pcode.ToString() + "' and status !='OD' and status!='H_OD'  and dateoffice Between '" + dateFrom.ToString("MMM dd yyyy") + "' AND '" + dateTo.ToString("MMM dd yyyy") + "' and LATEARRIVAL>0";
        //strsql1 = "select count(*) latearrival  from tbltimeregister where paycode='" + Pcode.ToString() + "' and status is not  and dateoffice Between '" + dateFrom.ToString("MMM dd yyyy") + "' AND '" + dateTo.ToString("MMM dd yyyy") + "' and LATEARRIVAL>0";                        
        dsLate = cn.FillDataSet(strsql1);
        if (dsLate.Tables[0].Rows.Count > 0)
        {
            lblLateArrivals.Text = dsLate.Tables[0].Rows[0]["latearrival"].ToString();

        }
        else
        {
            lblLateArrivals.Text = "";
        }

        //strsql = "select left(datename(dw,dateoffice),3) as Day, convert (char,dateoffice,103) DATE, SubString(Convert(Char(17),ShiftStartTime,13),13,5) 'ROSTER TIME',SubString(Convert(Char(17),in1,13),13,5) 'IN TIME', SubString(Convert(Char(17),tbltimeregister.out2,13),13,5) 'OUT TIME', LATE= CASE WHEN ISNULL(Latearrival,0)=0 THEN ' ' ELSE  RIGHT('00' + LTRIM(STR(Latearrival/60)),2) + '.' + RIGHT('00' + LTRIM(STR(Latearrival%60)),2) END, 'HOURS WORKED'=CASE WHEN ISNULL(HOURSWORKED,0)=0 THEN ' ' ELSE  RIGHT('00' + LTRIM(STR(HOURSWORKED/60)),2) + '.' + RIGHT('00' + LTRIM(STR(HOURSWORKED%60)),2) END,STATUS  from tbltimeregister where paycode='" + Pcode.ToString() + "' and dateoffice between '" + dateFrom.ToString("MMM dd yyyy") + "'  and '" + dateTo.ToString("MMM dd yyyy") + "' order by dateoffice";
        strsql = "select left(datename(dw,dateoffice),3) as Day, convert (char,dateoffice,103) DATE, SubString(Convert(Char(17),ShiftStartTime,13),13,5) 'ROSTER TIME',SubString(Convert(Char(17),in1,13),13,5) 'IN TIME', SubString(Convert(Char(17),tbltimeregister.out2,13),13,5) 'OUT TIME', LATE= CASE WHEN  STATUS='OD' OR STATUS='H_OD' OR ISNULL(Latearrival,0)=0 THEN ' ' ELSE  RIGHT('00' + LTRIM(STR(Latearrival/60)),2) + '.' + RIGHT('00' + LTRIM(STR(Latearrival%60)),2) END, 'HOURS WORKED'=CASE WHEN ISNULL(HOURSWORKED,0)=0 THEN RIGHT('00' + LTRIM(STR((datediff(minute,in1,out2))/60)),2) + '.' + RIGHT('00' + LTRIM(STR((datediff(minute,in1,out2))%60)),2) ELSE  RIGHT('00' + LTRIM(STR(HOURSWORKED/60)),2) + '.' + RIGHT('00' + LTRIM(STR(HOURSWORKED%60)),2) END,STATUS  from tbltimeregister where paycode='" + Pcode.ToString() + "' and dateoffice between '" + dateFrom.ToString("MMM dd yyyy") + "'  and '" + dateTo.ToString("MMM dd yyyy") + "' order by dateoffice";
        //Response.Write(strsql.ToString());
        grdrequest.Visible = true;
        ds = cn.FillDataSet(strsql);
        grdrequest.DataSource = ds;
        grdrequest.DataBind();
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {

    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        BindGrid();
        grdrequest.DataBind();
    }
    protected void btnView_Click(object sender, System.EventArgs e)
    {
        try
        {
            UpdateTimeRegister();
        }
        catch
        {
        }
        BindGrid();
    }
    protected void UpdateTimeRegister_Half()
    {
        DataSet dsDate = new DataSet();
        DateTime dateFrom = System.DateTime.MinValue;
        DateTime dateTo = System.DateTime.MinValue;
        string strsql = "";
        strsql = "select convert(varchar(10),dateadd(mm,-1,dateadd(mm, datediff(mm,0,getdate()),0)),103),convert(varchar(10),getdate(),103)";
        dsDate = cn.FillDataSet(strsql);
        if (dsDate.Tables[0].Rows.Count > 0)
        {
            try
            {
                dateFrom = DateTime.ParseExact(dsDate.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dateTo = DateTime.ParseExact(dsDate.Tables[0].Rows[0][1].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        else
            return;

        string str = "";
        string Upaycode = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        string StrSql = "";
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        double TimeLVAmount1 = 0;
        double TimeLVAmount2 = 0;
        double presntvalue = 0;
        double absentvalue = 0;
        double LeaveValue = 0;
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103),l.leavecode, " +
                       " l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.presentvalue,t.absentvalue,T.LEAVEVALUE from tbltimeregister t join leave_request l 	on t.paycode=l.paycode and t.dateoffice between l.leave_from and l.leave_to " +
                        " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+ltrim(rtrim(l.leavecode))+'%' and l.stage1_approved='Y' and l.stage2_approved='Y'  " +
                        " and dateoffice between '" + dateFrom.ToString("yyyy-MM-dd") + "' and '" + dateTo.ToString("yyyy-MM-dd") + "'  " +
                        " and l.halfday!='N' order by t.paycode";

            ds = cn.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Upaycode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[i][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    TimeLVAmount1 = Convert.ToDouble(ds.Tables[0].Rows[i][8].ToString());
                    TimeLVAmount2 = Convert.ToDouble(ds.Tables[0].Rows[i][9].ToString());
                    presntvalue = Convert.ToDouble(ds.Tables[0].Rows[i][10].ToString());
                    absentvalue = Convert.ToDouble(ds.Tables[0].Rows[i][11].ToString());
                    LeaveValue = Convert.ToDouble(ds.Tables[0].Rows[i][12].ToString());

                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = cn.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    str = "";

                    if (hday.ToString().Trim() == "F")
                    {
                        lvamount = 0.5;
                        lvamount1 = 0.5;
                        lvamount2 = 0;
                    }
                    else
                    {
                        lvamount = 0.5;
                        lvamount1 = 0;
                        lvamount2 = 0.5;
                    }
                    if (lvtype.ToString().Trim().ToUpper() == "L")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype1='L',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (LeaveValue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='L',leavetype2='L',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "P")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype1='P',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                               " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (presntvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='P',leavetype2='P',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    else if (lvtype.ToString().Trim().ToUpper() == "A")
                    {
                        if (hday.ToString().Trim() == "F")
                        {
                            if (TimeLVAmount1 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype1='A',Firsthalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount1=" + lvamount1 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            if (TimeLVAmount2 == 0)
                            {
                                if (absentvalue == 1)
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                                else
                                {
                                    str = "update tbltimeregister set leavetype='A',leavetype2='A',Secondhalfleavecode='" + lvcodeTm.ToString().Trim() + "' " +
                                        ", leaveamount2=" + lvamount2 + " " +
                                          " ,leaveamount=" + lvamount + ",voucher_no='" + vno.ToString() + "',LeaveAprdate='" + lvapr.ToString("yyyy-MM-dd") + "'," +
                                          " Reason='" + reason.ToString() + "',LeaveCode='" + lvcodeTm.ToString() + "' where paycode='" + Upaycode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(str))
                    {
                        cn.execute_NonQuery(str);
                        if (Connection.State == 0)
                            Connection.Open();

                        OleDbCommand upd = new OleDbCommand("upd", Connection);
                        upd.CommandType = CommandType.StoredProcedure;
                        OleDbParameter myParm1 = upd.Parameters.Add("@paycode", OleDbType.Char, 10);
                        myParm1.Value = Upaycode.ToString();
                        OleDbParameter myParm2 = upd.Parameters.Add("@dateoffice", OleDbType.Date, 10);
                        myParm2.Value = dtFrom.ToString("yyyy-MM-dd");
                        upd.ExecuteNonQuery();
                        Connection.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }


    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        if (Session["usertype"].ToString() == "A")
        {
            Response.Redirect("dashboard.aspx");
        }
        else
        {
            Response.Redirect("HomePage.aspx");
        }

    }
    protected void radSelectType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        try
        {
            //StrSql = "";
            //if (radSelectType.SelectedIndex == 0)
            //{
            //    if (Session["usertype"].ToString() == "C")  //HOD
            //    {
            //        StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where hod_code ='" + Session["PAYCODE"].ToString() + "'  and ";
            //    }
            //    if (Session["usertype"].ToString() == "H")  //Hod 
            //    {
            //        // StrSql = "select paycode,rtrim(Empname) + ' - ' + rtrim(paycode) as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and ";
            //        if (ReportView.ToString().Trim() == "Y")
            //        {
            //            StrSql = "";
            //            StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and active='Y' order by empname";
            //        }
            //        else
            //        {
            //            StrSql = "";
            //            StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y'  ";
            //            if (Session["Auth_Comp"] != null)
            //            {
            //                StrSql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
            //            }
            //            if (Session["Auth_Dept"] != null)
            //            {
            //                StrSql += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
            //            }
            //            StrSql += " order by empname";
            //        }

            //    }
            //    if (Session["usertype"].ToString() == "A")  //Admin
            //    {
            //        StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where  ";
            //        if (ChkInActive.Checked)
            //        {
            //            StrSql += " Active='N'  ";
            //        }
            //        else
            //        {
            //            StrSql += " Active='Y'  ";
            //        }
            //        if ((Session["usertype"].ToString() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            //        {
            //            StrSql += " and companycode in (" + Session["Auth_Comp"].ToString() + ") ";
            //        }
            //        if ((Session["usertype"].ToString() == "A") && (Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            //        {
            //            StrSql += " and Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
            //        }
            //        StrSql += " order by empname  ";
            //    }
            //}
            //else
            //{
            //    if (Session["usertype"].ToString() == "C")  //HOD
            //    {
            //        StrSql = "select paycode,rtrim(Paycode) + ' - ' + Empname as Empname  from tblemployee where hod_code ='" + Session["PAYCODE"].ToString() + "'  and ";
            //    }
            //    if (Session["usertype"].ToString() == "H")  //Hod 
            //    {
            //        // StrSql = "select paycode,  rtrim(paycode) + '-'+ rtrim(Empname) as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and ";
            //        if (ReportView.ToString().Trim() == "Y")
            //        {
            //            StrSql = "";
            //            StrSql = "select paycode,  rtrim(paycode) + '-'+ rtrim(Empname) as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and active='Y' order by paycode";
            //        }
            //        else
            //        {
            //            StrSql = "";
            //            StrSql = "Select paycode,  rtrim(paycode) + '-'+ rtrim(Empname) as Empname  from tblemployee where Active='Y'  ";
            //            if (Session["Auth_Comp"] != null)
            //            {
            //                StrSql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
            //            }
            //            if (Session["Auth_Dept"] != null)
            //            {
            //                StrSql += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
            //            }
            //            StrSql += " order by Paycode";
            //        }
            //    }
            //    if (Session["usertype"].ToString() == "A")  //Admin
            //    {
            //        StrSql = "Select paycode,rtrim(Paycode) + ' - ' + rtrim(EmpName) as Empname  from tblemployee where ";
            //        if (ChkInActive.Checked)
            //        {
            //            StrSql += " Active='N'  ";
            //        }
            //        else
            //        {
            //            StrSql += " Active='Y'  ";
            //        }
            //        if ((Session["usertype"].ToString() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            //        {
            //            StrSql += " and companycode in (" + Session["Auth_Comp"].ToString() + ") ";
            //        }
            //        if ((Session["usertype"].ToString() == "A") && (Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            //        {
            //            StrSql += " and Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
            //        }
            //        StrSql += " order by empname  ";
            //    }
            //}

            //if (StrSql.Trim() == "")
            //    return;
            //DataSet DsEmpName = new DataSet();
            //DsEmpName = cn.FillDataSet(StrSql);
            //EmpCombo.DataSource = DsEmpName;
            //EmpCombo.DataTextField = "EmpName";
            //EmpCombo.DataValueField = "Paycode";
            //EmpCombo.DataBind();
            //if (Session["usertype"].ToString() != "A")  //Admin
            //{
            //    if (radSelectType.SelectedIndex == 0)
            //    {
            //        EmpCombo.Items.Insert(0, new ListItem(Session["UserName"].ToString().Trim() + " - " + Session["PAYCODE"].ToString(), Session["PAYCODE"].ToString()));
            //    }
            //    else
            //    {
            //        EmpCombo.Items.Insert(0, new ListItem(Session["PAYCODE"].ToString().Trim() + " - " + Session["UserName"].ToString(), Session["PAYCODE"].ToString()));
            //    }
            //}
            //if (EmpCombo.Items.Count > 0)
            //    TD1.Visible = true;
            //else
            //    TD1.Visible = false;
        }
        catch (Exception er)
        {
            Error_Occured("Fill_Combo", er.Message);
        }
    }


    protected void UpdateTimeRegister()
    {
        string msg = "";
        if (txtfromdate.Text.ToString().Trim().Length < 10)
        {
            msg = "<script language='javascript'> alert('Enter a valid From Date.')</script>";
            Page.RegisterStartupScript("msg", msg);
            //txtfromdate.Value = "01/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString();
            txtfromdate.Value = Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-01"));
            txtfromdate.Focus();
            return;
        }
        if (txttodate.Text.ToString().Trim().Length < 10)
        {
            msg = "<script language='javascript'> alert('Enter A Valid To Date.')</script>";
            Page.RegisterStartupScript("msg", msg);
            txttodate.Value = Convert.ToDateTime(fd.ToString("yyyy-MM-dd"));
            txttodate.Focus();
            return;
        }
        DateTime dateFrom = System.DateTime.MinValue;
        DateTime dateTo = System.DateTime.MinValue;
        try
        {
            dateFrom = Convert.ToDateTime(txtfromdate.Value);
            dateTo = Convert.ToDateTime(txttodate.Value);
        }
        catch (Exception ex)
        {
            msg = "<script lanaguage='javascript'>alert('Invalid Date. ')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtfromdate.Value = Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-01"));
            txttodate.Value = fd.ToString("dd/MM/yyyy");
            return;
        }

        Module_UPD UPD = new Module_UPD();
        string str = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string Pcode = "";
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        string LvType1 = "";
        string LvType2 = "";
        string FirstHalfLvCode = "";
        string SecondHalfLvCode = "";
        string ShiftAttended = "";
        string Holiday = "";
        string OldStatus = "";
        DataSet dsD = new DataSet();
        int result = 0;
        string isOffInclude = "";
        string isHldInclude = "";
        string Status = "";

        if (Session["usertype"].ToString().Trim().ToUpper() == "U")
        {
            Pcode = Session["PAYCODE"].ToString();
        }
        else
        {
            if (EmpCombo.Items.Count > 0)
            {
                Pcode = EmpCombo.SelectedItem.Value.ToString();
            }
            else
            {
                Pcode = Session["PAYCODE"].ToString();
            }
        }
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103), " +
                    " l.leavecode,  l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.status,l.leavecode,t.presentvalue,t.absentvalue,T.LEAVEVALUE,t.ShiftAttended,t.in1,lm.isoffinclude,lm.ISHOLIDAYINCLUDE,t.Status  " +
                    " from tbltimeregister t join leave_request l 	on t.paycode=l.paycode join tblleavemaster lm on lm.leavecode=l.leavecode and t.dateoffice between l.leave_from and l.leave_to  " +
                    " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+rtrim(l.leavecode)+'%' and l.stage1_approved='Y' and l.stage2_approved='Y' " +
                    " and dateoffice between '" + dateFrom.ToString("yyyy-MM-dd") + "' and '" + dateTo.ToString("yyyy-MM-dd") + "' and t.paycode='" + Pcode.ToString().Trim() + "' order by t.paycode";
            ds = cn.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Pcode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    ShiftAttended = ds.Tables[0].Rows[i]["ShiftAttended"].ToString().Trim();
                    OldStatus = ds.Tables[0].Rows[i]["in1"].ToString().Trim();
                    isOffInclude = ds.Tables[0].Rows[i]["isoffinclude"].ToString().Trim();
                    isHldInclude = ds.Tables[0].Rows[i]["ISHOLIDAYINCLUDE"].ToString().Trim();
                    Status = ds.Tables[0].Rows[i]["Status"].ToString().Trim();
                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = cn.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    if (isOffInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && ShiftAttended.ToString().Trim().ToUpper() == "OFF")
                    {
                        continue;
                    }
                    if (isHldInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && Status.ToString().Trim().ToUpper() == "HLD")
                    {
                        continue;
                    }
                    try
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            lvamount = 1;
                            lvcodeTm = lvcode;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            lvamount = 0.5;
                            lvamount1 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        else
                        {
                            lvamount = 0.5;
                            lvamount2 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        StrSql = "";
                        StrSql = "select * from holiday where hdate='" + dtFrom.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + Pcode.ToString().Trim() + "') " +
                                "and departmentcode=(select departmentcode from tblemployee where paycode='" + Pcode.ToString().Trim() + "')";
                        dsD = cn.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            Holiday = "Y";
                        }
                        StrSql = "";
                        if (hday.ToString().Trim() == "N")
                        {
                            FirstHalfLvCode = null;
                            LvType1 = null;
                            SecondHalfLvCode = null;
                            LvType2 = null;
                            lvamount = 1;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "select isnull(SECONDHALFLEAVECODE,''),LeaveType2 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = cn.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = lvcode.ToString().Trim();
                                    LvType1 = lvtype.ToString().Trim();
                                    SecondHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType2 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "select isnull(FIRSTHALFLEAVECODE,''),LeaveType1 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = cn.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType1 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    SecondHalfLvCode = lvcode.ToString().Trim();
                                    LvType2 = lvtype.ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        if (hday.ToString().Trim() == "N")
                        {
                            StrSql = "Update tblTimeRegister Set LeaveCode='" + lvcodeTm.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "Update tblTimeRegister Set FIRSTHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType1='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount1=" + lvamount1 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "Update tblTimeRegister Set SECONDHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType2='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount2=" + lvamount2 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        result = cn.execute_NonQuery(StrSql);
                        if (result > 0)
                        {
                            UPD.Upd(Pcode, dtFrom, lvamount, lvamount1, lvamount2, lvcode, FirstHalfLvCode, SecondHalfLvCode, lvtype, LvType1, LvType2, ShiftAttended, Holiday, OldStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ChkInActive_CheckedChanged(object sender, EventArgs e)
    {
        //if (ChkInActive.Checked == true)
        //{
        //    Fill_Combo_InActive();
        //}
        //else
        //{
        //    Fill_Combo();
        //}
    }
    private void Fill_Combo_InActive()
    {
        try
        {
            //StrSql = "";
            //if (Session["usertype"].ToString() == "C")  //HOD
            //{
            //    StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where hod_code ='" + Session["PAYCODE"].ToString() + "'  and active='N' order by empname";
            //}
            //if (Session["usertype"].ToString() == "H")  //Reproting Manager 
            //{
            //    StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and active='N' order by empname";
            //}
            //if (Session["usertype"].ToString() == "A")  //Admin
            //{
            //    StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='N'  ";
            //    if ((Session["usertype"].ToString() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            //    {
            //        StrSql += " and companycode in (" + Session["Auth_Comp"].ToString() + ") ";
            //    }
            //    if ((Session["usertype"].ToString() == "A") && (Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
            //    {
            //        StrSql += " and Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
            //    }
            //    StrSql = " Order by EmpName ";
            //}


            //if (StrSql.Trim() == "")
            //    return;
            //DataSet DsEmpName = new DataSet();
            //DsEmpName = cn.FillDataSet(StrSql);
            //EmpCombo.DataSource = DsEmpName;
            //EmpCombo.DataTextField = "EmpName";
            //EmpCombo.DataValueField = "Paycode";
            //EmpCombo.DataBind();
            //if (Session["usertype"].ToString() != "A")  //Admin
            //{
            //    EmpCombo.Items.Insert(0, new ListItem(Session["UserName"].ToString().Trim() + " - " + Session["PAYCODE"].ToString(), Session["PAYCODE"].ToString()));
            //}
            //if (EmpCombo.Items.Count > 0)
            //    TD1.Visible = true;
            //else
            //    TD1.Visible = false;
        }
        catch (Exception er)
        {
            Error_Occured("Fill_Combo", er.Message);
        }
    }
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<a href='Default.aspx' class='link'> Back to Login Page </a> <br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void btExcel_Click(object sender, EventArgs e)
    {
        //Response.Clear();
        //Response.AddHeader("content-disposition", "attachment;filename=AttendenceReport.xls");
        //Response.Charset = string.Empty;
        //Response.ContentType = "application/vnd.ms-excel";
        //System.IO.StringWriter stringWrite1 = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter htmlWrite1 = new HtmlTextWriter(stringWrite1);
        //MainDiv.RenderControl(htmlWrite1);
        //Response.Write("<meta http-equiv=Content-Type content=\"text/html; charset=utf-8\">\n");
        //// add the style props to get the page orientation
        //Response.Write(AddExcelStyling());
        //Response.Write(stringWrite1.ToString());
        //Response.Write("</body>"); Response.Write("</html>");
        //Response.End();
    }
    private string AddExcelStyling()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office'\n" +
        "xmlns:x='urn:schemas-microsoft-com:office:excel'\n" +
        "xmlns='http://www.w3.org/TR/REC-html40'>\n" +
        "<head>\n");
        sb.Append("<center><tr><td> " + a + "</td></tr></center> ");
        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
        //sb.Append("<tr><td style='text-align: center'>"+msg+" " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
        sb.Append("<style>\n");
        sb.Append("@page");
        sb.Append("{margin:0.5in 0.5in 0.5in 0.5in;\n");
        sb.Append("mso-header-margin:.5in;\n");
        sb.Append("mso-footer-margin:.1in;\n");
        sb.Append("mso-page-Fit To:yes;\n");
        sb.Append("mso-page-Scaling:Fit2Page;\n");
        sb.Append("{mso-footer-data:'&L&D   &T  &RPage &P of &n ';");
        sb.Append("mso-page-FitToPagesTall:True;\n");
        sb.Append("mso-paper-source:0;\n");
        sb.Append("mso-page-Scaling:Fit2Page;");
        // If we want to export as Portrait
        sb.Append("mso-page-orientation:Portrait;}\n");
        // If we want to export as Landscape
        //sb.Append("mso-page-orientation: Landscape;}\n");  
        sb.Append("</style>\n");
        sb.Append("<!--[if gte mso 9]><xml>\n");
        sb.Append("<x:ExcelWorkbook>\n");
        sb.Append("<x:ExcelWorksheets>\n");
        sb.Append("<x:ExcelWorksheet>\n");
        sb.Append("<x:Name>Sheet1</x:Name>\n");
        sb.Append("<x:WorksheetOptions>\n");
        sb.Append("<x:Print>\n");
        sb.Append("<x:ValidPrinterInfo/>\n");
        sb.Append("<x:PaperSizeIndex>9</x:PaperSizeIndex>\n");
        // Paper Size Index 9 – A4, 5 -Legal
        sb.Append("<x:HorizontalResolution>20</x:HorizontalResolution\n");
        sb.Append("<x:VerticalResolution>20</x:VerticalResolution\n");
        sb.Append("<x:Scale>100</x:Scale>");  //Scaling        

        sb.Append("<x:FitWidth>1</x:FitWidth>");
        sb.Append("<x:FitHeight>700</x:FitHeight>");
        sb.Append("</x:Print>\n");
        sb.Append("<x:Selected/>\n");
        sb.Append("<x:DoNotDisplayGridlines/>\n");
        sb.Append("<x:ProtectContents>False</x:ProtectContents>\n");
        sb.Append("<x:ProtectObjects>False</x:ProtectObjects>\n");
        sb.Append("<x:ProtectScenarSoftOrange>False</x:ProtectScenarSoftOrange>\n");
        sb.Append("</x:WorksheetOptions>\n");
        sb.Append("</x:ExcelWorksheet>\n");
        sb.Append("</x:ExcelWorksheets>\n");
        sb.Append("<x:WindowHeight>12780</x:WindowHeight>\n");
        sb.Append("<x:WindowWidth>19035</x:WindowWidth>\n");
        sb.Append("<x:WindowTopX>0</x:WindowTopX>\n");
        sb.Append("<x:WindowTopY>15</x:WindowTopY>\n");
        sb.Append("<x:ProtectStructure>False</x:ProtectStructure>\n");
        sb.Append("<x:ProtectWindows>False</x:ProtectWindows>\n");
        sb.Append("</x:ExcelWorkbook>\n");
        sb.Append("<x:ExcelName>\n");
        sb.Append("<x:Name>Print_Titles</x:Name>\n");
        sb.Append("<x:SheetIndex>1</x:SheetIndex>\n");
        sb.Append("<x:Formula>=Sheet1!$1:$5</x:Formula>\n");
        sb.Append("</x:ExcelName>\n");
        sb.Append("</xml><![endif]-->\n");
        sb.Append("</head>\n");
        sb.Append("<body>\n");
        return sb.ToString();
    }
    protected void grdrequest_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        grdrequest.PageIndex = pageIndex;
        this.BindGrid();
    }
    protected void EmpCombo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sSql = "";
        DataSet DsEmp = new DataSet();
        
        
        try
        {
            if(EmpCombo.SelectedIndex==-1 || EmpCombo.SelectedItem.Text.ToString().Trim().ToUpper()=="NONE")
            {
                LblEmpName.Text = "";
                lblLocation.Text = "";
                lblPresent.Text = "";
                lbldaysabsent.Text = "";
                lblHoliday.Text = "";
                lblLateArrivals.Text = "";
                lblweeklyoff.Text = "";
                lblApprovedLeave.Text = "";
                EmpCombo.Focus();
                return;
            }
            sSql = "select EMPNAME,tblDepartment.DepartmentName from tblemployee inner join tblDepartment on tblDepartment.DepartmentCode=tblemployee.DepartmentCode " +
                    "where tblemployee.PAYCODE='" + EmpCombo.SelectedItem.Value.ToString().Trim() + "' and tblemployee.COMPANYCODE='"+Session["LoginCompany"].ToString().Trim()+"' "+
                    " and tblDepartment.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            DsEmp = cn.FillDataSet(sSql);
            if(DsEmp.Tables[0].Rows.Count>0)
            {
                LblEmpName.Text = DsEmp.Tables[0].Rows[0]["EMPNAME"].ToString().Trim();
                lblLocation.Text = DsEmp.Tables[0].Rows[0]["DepartmentName"].ToString().Trim();
            }
            


        }
        catch
        {
            LblEmpName.Text = "";
            lblLocation.Text = "";
            lblPresent.Text = "";
            lbldaysabsent.Text = "";
            lblHoliday.Text = "";
            lblLateArrivals.Text = "";
            lblweeklyoff.Text = "";
            lblApprovedLeave.Text = "";
            EmpCombo.Focus();
            return;
        }
    }
    protected void txtfromdate_CalendarDayCellPrepared(object sender, CalendarDayCellPreparedEventArgs e)
    {
        
        //if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Red;
        //}
        //else
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Black;
        //}
    }
    protected void txttodate_CalendarDayCellPrepared(object sender, CalendarDayCellPreparedEventArgs e)
    {

        //if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Red;
        //}
        //else
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Black;
        //}
    }
}