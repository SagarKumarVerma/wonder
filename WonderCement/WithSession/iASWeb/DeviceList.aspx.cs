﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using System.Collections;
using System.Net;
using Newtonsoft.Json;
using BioCloud;
public partial class DeviceList : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    public string G_Model = null;
    const int GET_DEVICE_STATUS = 1;
    string sSql = string.Empty;
    DataSet ds = null;
    SqlConnection msqlConn;
    ErrorClass ec = new ErrorClass();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);

    const int GET_USER_ID_LIST = 0;
    const int GET_USER_INFO = 1;
    const int SET_USER_INFO = 2;
    const int DEL_USER_INFO = 3;
    const int ALL_DEL = 4;
    const int GET_ALL_USER_INFO = 5;
    const int SET_ALL_USER_INFO = 6;
    const int SET_USER_NAME = 7;
    const int SET_USER_PRIVILEGE = 8;


    const int STATE_WAIT = 0;
    const int STATE_VIEW = 1;
    const int STATE_EDIT = 2;

    const int LIST_OF_DEVICE = 0;
    const int LIST_OF_PC = 1;
    const int LIST_OF_DEVICE_FOR_BATCH = 2;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                ASPxBtnClearData.Visible = true;
                ASPxBtnClearLog.Visible = true;
            }
            else
            {
                ASPxBtnClearData.Visible = true;
                ASPxBtnClearLog.Visible = true;
            }
           // BulkDelete();
            SetTimeZone();
            GetDevieListAll();
            msqlConn = FKWebTools.GetDBPool();
        }
    }
    public void GetCompany()
    {
        try
        {

        }
        catch (Exception Exc)
        {
            Error_Occured("GetCompany", Exc.Message);

        }

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        GetDevieListAll();
        ASPxGridView1.DataBind();
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    public void GetDeviceList()
    {
        try
        {
            DataSet DSDev = new DataSet();
            ds = new DataSet();
            string str = "";
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                str = "select * from tbl_fkdevice_status";
            }
            else
            {
                str = "select * from tbl_fkdevice_status where groupid in ('" + Session["Group"] + "')";
            }
            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DSDev;
                ASPxGridView1.DataBind();
                Session["DataSet"] = DSDev;
            }

        }

        catch (Exception Exc)
        {
            Error_Occured("GetDeviceList", Exc.Message);

        }

    }

    public void UpdateDeviceTable()
    {
        DataSet DsDeviceAdded = new DataSet();

        try
        {
            sSql = "select * from tbl_fkdevice_status where IsInserted=0 or IsInserted is null";
            DsDeviceAdded = cn.FillDataSet(sSql);
            if (DsDeviceAdded.Tables[0].Rows.Count > 0)
            {
                for (int A = 0; A < DsDeviceAdded.Tables[0].Rows.Count; A++)
                {
                    int DevID = GetMaxID();
                    sSql = "insert into tblMachine(id_no,ClientID,SerialNumber,DeviceMode,CompanyCode,mac_Address,UpdatedOn,DeviceName) values (" + DevID + ",1,'" + DsDeviceAdded.Tables[0].Rows[A]["device_id"].ToString().Trim() + "', " +
                    " 'BIO','C001','" + DsDeviceAdded.Tables[0].Rows[A]["device_id"].ToString().Trim() + "','" + DsDeviceAdded.Tables[0].Rows[A]["last_update_time"].ToString().Trim() + "','" + DsDeviceAdded.Tables[0].Rows[A]["device_name"].ToString().Trim() + "') ";
                    cn.execute_NonQuery(sSql);
                }
            }


        }
        catch (Exception Exc)
        {
            Error_Occured("UpdateDeviceTable", Exc.Message);

        }
    }

    public int GetMaxID()
    {
        int MaxID = 0;
        try
        {
            sSql = "select ID_NO=isnull(max(ID_NO),0) from TblMACHINE";
            DataSet MaxDev = new DataSet();
            MaxDev = cn.FillDataSet(sSql);
            MaxID = Convert.ToInt32(MaxDev.Tables[0].Rows[0][0].ToString().Trim()) + 1;

        }
        catch (Exception Exc)
        {
            Error_Occured("GetMaxID", Exc.Message);

        }

        return MaxID;
    }

    public void GetDevieListAll()
    {
        try
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("device_id");
            DT.Columns.Add("device_name");
            DT.Columns.Add("connected");
            DT.Columns.Add("IPAddrss");
            DT.Columns.Add("last_update_time");
            DT.Columns.Add("user_count");
            DT.Columns.Add("fp_count");
            DT.Columns.Add("face_count");
            DT.Columns.Add("password_count");
            DT.Columns.Add("idcard_count");
            DT.Columns.Add("manager_count");
            DT.Columns.Add("IsMasterDevice");
            DT.Columns.Add("GroupID");
            DT.Columns.Add("DeviceMode");
            DT.Columns.Add("IsAuthorized");
            DT.Columns.Add("TimeZone");
            DT.Columns.Add("CompanyCode");
            DT.Columns.Add("MachineType");

            string sSqlBio = "";
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                sSqlBio = "select * from tbl_fkdevice_status";
            }
            else
            {
                sSqlBio = "select * from tbl_fkdevice_status where CompanyCode in ("+ Session["Auth_Comp"].ToString().Trim() + ")";
            }
            DataSet DsBio = new DataSet();
            DsBio = cn.FillDataSet(sSqlBio);
            if (DsBio.Tables[0].Rows.Count > 0)
            {
                for (int B = 0; B < DsBio.Tables[0].Rows.Count; B++)
                {
                    DT.Rows.Add(DsBio.Tables[0].Rows[B]["device_id"].ToString().Trim(), DsBio.Tables[0].Rows[B]["device_name"].ToString().Trim(), DsBio.Tables[0].Rows[B]["connected"].ToString().Trim()
                                , DsBio.Tables[0].Rows[B]["IPAddrss"].ToString().Trim(), DsBio.Tables[0].Rows[B]["last_update_time"].ToString().Trim(), DsBio.Tables[0].Rows[B]["user_count"].ToString().Trim()
                                , DsBio.Tables[0].Rows[B]["fp_count"].ToString().Trim(), DsBio.Tables[0].Rows[B]["face_count"].ToString().Trim(), DsBio.Tables[0].Rows[B]["password_count"].ToString().Trim()
                                , DsBio.Tables[0].Rows[B]["idcard_count"].ToString().Trim(), DsBio.Tables[0].Rows[B]["manager_count"].ToString().Trim(), DsBio.Tables[0].Rows[B]["IsMasterDevice"].ToString().Trim()
                                , DsBio.Tables[0].Rows[B]["GroupID"].ToString().Trim(), "BIO", DsBio.Tables[0].Rows[B]["IsAuthorized"].ToString().Trim(), DsBio.Tables[0].Rows[B]["TimeZone"].ToString().Trim(), DsBio.Tables[0].Rows[B]["CompanyCode"].ToString().Trim(),"");
                }
            }
            string sSqlZK;
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                 sSqlZK = " select * from tblMachine where DeviceMode In ('ZKT','HTSeries')";
            }
            else
            {
                //Session["Auth_Comp"]
              //  sSqlZK = " select * from tblMachine where DeviceMode In ('ZKT','HTSeries') and CompanyCode='"+ Session["LoginCompany"].ToString().Trim()  + "'";
                sSqlZK = " select * from tblMachine where DeviceMode In ('ZKT','HTSeries') and CompanyCode   like '%"+ Session["LoginCompany"].ToString().Trim() + "%'  ";
               // sSqlZK = " select * from tblMachine where DeviceMode In ('ZKT','HTSeries') and CompanyCode in (" + Session["Auth_Comp"].ToString().Trim() + ")";
            }
            DataSet DsZK = new DataSet();
            DsZK = cn.FillDataSet(sSqlZK);
            if (DsZK.Tables[0].Rows.Count > 0)
            {
                for (int D = 0; D < DsZK.Tables[0].Rows.Count; D++)
                {
                    DT.Rows.Add(DsZK.Tables[0].Rows[D]["SerialNumber"].ToString().Trim(), DsZK.Tables[0].Rows[D]["DeviceName"].ToString().Trim(), "",
                                DsZK.Tables[0].Rows[D]["LOCATION"].ToString().Trim(), DsZK.Tables[0].Rows[D]["updatedon"].ToString().Trim(), DsZK.Tables[0].Rows[D]["UserCount"].ToString().Trim(),
                                DsZK.Tables[0].Rows[D]["FPCount"].ToString().Trim(), DsZK.Tables[0].Rows[D]["FaceCount"].ToString().Trim(), ""
                                , "", "", DsZK.Tables[0].Rows[D]["IsMasterDevice"].ToString().Trim()
                                , DsZK.Tables[0].Rows[D]["GroupID"].ToString().Trim(), DsZK.Tables[0].Rows[D]["DeviceMode"].ToString().Trim(), DsZK.Tables[0].Rows[D]["IsAuthorized"].ToString().Trim(), DsZK.Tables[0].Rows[D]["TimeZone"].ToString().Trim(), DsZK.Tables[0].Rows[D]["CompanyCode"].ToString().Trim(), DsZK.Tables[0].Rows[D]["MachineType"].ToString().Trim());
                }

            }

            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.KeyFieldName = "device_id";
                ASPxGridView1.DataBind();
            }
        }
        catch (Exception Exc)
        {
            Error_Occured("GetDevices", Exc.Message);

        }
    }

    protected void ASPxGridView1_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "Status")
        {

            DateTime dtDev = System.DateTime.MinValue;
            string IsAuth = "";
            dtDev = Convert.ToDateTime(e.GetValue("last_update_time"));
            IsAuth = e.GetValue("IsAuthorized").ToString().Trim();
            TimeSpan TS = System.DateTime.Now - dtDev;
            long Diff = Convert.ToInt32(TS.TotalMinutes);
            if (IsAuth == "N" || (string.IsNullOrEmpty(IsAuth)))
            {
                e.Cell.ForeColor = System.Drawing.Color.Purple;
                e.Cell.Text = "Not Authorized";
            }
            else if (Diff <= 1)
            {
                e.Cell.ForeColor = System.Drawing.Color.Green;
                e.Cell.Text = "Online";

            }

            else
            {
                e.Cell.ForeColor = System.Drawing.Color.Red;
                e.Cell.Text = "Offline";
            }
        }




    }

    protected void ASPxGridView1_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
    {



    }

    protected void ASPxBtnReboot_Click(object sender, EventArgs e)
    {


        try
        {

            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }

            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                    if (G_Model == "BIO")
                    {
                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "RESET_FK", mDevId, null);
                    }
                    else
                    {
                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn) values('" + mDevId.Trim() + "','flagreboot','" + mDevId.Trim() + "',0,1,getdate())";
                        cn.execute_NonQuery(sSql);
                    }

                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);

        }
        catch (Exception Exc)
        {
            Error_Occured("Reboot", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        }
    }

    protected void ASPxCallbackPanel1_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        //  GetDeviceList();
    }

    protected void ASPxCallbackPanel1_Init(object sender, EventArgs e)
    {
        // GetDeviceList();
    }

    public void BulkDelete()
    {
        try
        {

            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            JObject vResultJson;

            string mDeviceIdTemp;
            sSql = "Select * from tbl_DeviceCommands where UpdateFlag='N'";
            DataSet DsThird = new DataSet();
            DsThird = cn.FillDataSet(sSql);
            if (DsThird.Tables[0].Rows.Count > 0)
            {
                for (int T = 0; T < DsThird.Tables[0].Rows.Count; T++)
                {
                    try
                    {
                        string mUserId = DsThird.Tables[0].Rows[T]["UserID"].ToString().Trim();
                        mDeviceIdTemp = DsThird.Tables[0].Rows[T]["DeviceID"].ToString().Trim();
                        vResultJson = new JObject();
                        vResultJson.Add("user_id", mUserId);
                        string sFinal = vResultJson.ToString(Formatting.None);
                        byte[] strParam = new byte[0];
                        cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "DELETE_USER", mDeviceIdTemp, strParam);
                        sSql = "update tbl_DeviceCommands set UpdateFlag='Y',ExecutionTime=getdate() where RowID=" + DsThird.Tables[0].Rows[T]["RowID"].ToString().Trim() + "  ";
                        cn.execute_NonQuery(sSql);
                    }
                    catch
                    {

                    }
                }
            }
        }
        catch (Exception Exc)
        {
            Error_Occured("BulkDelete", Exc.Message);

        }
    }

    public void SetTimeZone()
    {

        string sSql = "";
        DataSet DsTemp = new DataSet();

        try
        {
            sSql = "Select device_id from tbl_fkdevice_status where connected=1 and TimeZone is Null";
            DsTemp = new DataSet();
            DsTemp = cn.FillDataSet(sSql);
            if (DsTemp.Tables[0].Rows.Count > 0)
            {
                for (int D = 0; D < DsTemp.Tables[0].Rows.Count; D++)
                {
                    try
                    {
                        sSql = "Update tbl_fkdevice_status set TimeZone=330 where device_id='" + DsTemp.Tables[0].Rows[D][0].ToString().Trim() + "' ";
                        cn.execute_NonQuery(sSql);
                        DateTime now = DateTime.Now;
                        string sNowTxt = FKWebTools.GetFKTimeString14(now);
                        JObject vResultJson = new JObject();
                        FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                        vResultJson.Add("time", sNowTxt);
                        string sFinal = vResultJson.ToString();
                        byte[] strParam = new byte[0];
                        cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "SET_TIME", DsTemp.Tables[0].Rows[D][0].ToString().Trim(), strParam);
                    }
                    catch (Exception Ex)
                    {
                        Error_Occured("SetTimeZone-Loop", Ex.Message);
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("SetTimeZone", Ex.Message);
        }
    }


    protected void ASPxBtnDoor_Click(object sender, EventArgs e)
    {
        try
        {

            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });

            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }


            for (int i = 0; i < keys.Count; i++)
            {

                string mDevId = Convert.ToString(keys[i]);
                G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                if (G_Model == "BIO")
                {


                    DateTime now = DateTime.Now;
                    string sNowTxt = FKWebTools.GetFKTimeString14(now);
                    JObject vResultJson = new JObject();
                    FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                    vResultJson.Add("time", sNowTxt);
                    string sFinal = vResultJson.ToString();
                    byte[] strParam = new byte[0];
                    cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                    msqlConn = FKWebTools.GetDBPool();
                    mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "SET_TIME", mDevId, strParam);
                }



            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);

        }
        catch (Exception Exc)
        {
            Error_Occured("SetTime", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        }
    }

    protected void ASPxBtnClearLog_Click(object sender, EventArgs e)
    {
        try
        {
            JObject vResultJson = new JObject();
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            string sBeginDate = "01/12/2019";
            string sEndDate = "";
            DateTime dtBegin, dtEnd;

            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });

            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }
            try
            {
                dtBegin = Convert.ToDateTime(sBeginDate);
                sBeginDate = FKWebTools.GetFKTimeString14(dtBegin);
                vResultJson.Add("begin_time", sBeginDate);

            }
            catch
            {

                sBeginDate = "";
            }
            if (sEndDate.Length > 0)
            {
                try
                {
                    dtEnd = Convert.ToDateTime(sEndDate);
                    sEndDate = FKWebTools.GetFKTimeString14(dtEnd);
                    vResultJson.Add("end_time", sEndDate);

                }
                catch
                {

                    sEndDate = "";
                }
            }

            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                    if (G_Model == "BIO")
                    {

                        string sFinal = vResultJson.ToString(Formatting.None);
                        byte[] strParam = new byte[0];
                        cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "GET_LOG_DATA", mDevId, strParam);
                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "CLEAR_LOG_DATA", mDevId, null);
                    }
                    else
                    {

                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevId.Trim() + "','flagclearLog','" + mDevId.Trim() + "',0,1,getdate(),'8')";
                        cn.execute_NonQuery(sSql);
                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevId.Trim() + "','flaginfo','" + mDevId.Trim() + "',0,1,getdate(),'7')";
                        cn.execute_NonQuery(sSql);
                    }

                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);

        }
        catch (Exception Exc)
        {
            Error_Occured("Clear Log", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        }
    }

    protected void ASPxBtnClearData_Click(object sender, EventArgs e)
    {
        try
        {

            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });

            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }

            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                    if (G_Model == "BIO")
                    {

                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "CLEAR_ENROLL_DATA", mDevId, null);
                    }
                    else
                    {

                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevId.Trim() + "','flagclearData','" + mDevId.Trim() + "',0,1,getdate(),'5')";
                        cn.execute_NonQuery(sSql);
                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevId.Trim() + "','flaginfo','" + mDevId.Trim() + "',0,1,getdate(),'7')";
                        cn.execute_NonQuery(sSql);
                    }

                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);

        }
        catch (Exception Exc)
        {
            Error_Occured("Clear Data", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        }
        //catch (Exception ex)
        //{
        //    ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        //}
    }

    protected void ASPxBtnSummary_Click(object sender, EventArgs e)
    {

        try
        {

            //sSql = "delete from tbl_fkcmd_trans";
            //cn.execute_NonQuery(sSql);
            //sSql = "delete from tbl_fkcmd_trans_cmd_param";
            //cn.execute_NonQuery(sSql);
            //sSql = "delete from tbl_fkcmd_trans_cmd_result";
            //cn.execute_NonQuery(sSql);
            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });

            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            } if (keys.Count > 1)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Only One Device')</script>");
                return;
            }

            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                    if (G_Model == "BIO")
                    {

                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "GET_DEVICE_STATUS", mDevId, null);
                        Session["operation"] = GET_DEVICE_STATUS;
                        System.Threading.Thread.Sleep(10000);
                        // Enables(false);
                        UpdateSummary();
                        //  DeviceStatus(mDevId);
                    }
                    else if (G_Model == "ZK")
                    {

                        //sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn) values('" + mDevId.Trim() + "','CHECK','" + mDevId.Trim() + "',0,1,getdate())";
                        //cn.execute_NonQuery(sSql);
                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn) values('" + mDevId.Trim() + "','flaginfo','" + mDevId.Trim() + "',0,1,getdate())";
                        cn.execute_NonQuery(sSql);
                    }
                    else if (G_Model == "HTSeries")
                    {

                        //sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn) values('" + mDevId.Trim() + "','CHECK','" + mDevId.Trim() + "',0,1,getdate())";
                        //cn.execute_NonQuery(sSql);
                        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn) values('" + mDevId.Trim() + "','flaginfo','" + mDevId.Trim() + "',0,1,getdate())";
                        cn.execute_NonQuery(sSql);
                    }

                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);

        }
        catch (Exception Exc)
        {
            Error_Occured("Summary", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        }

    }

    private void Enables(bool flag)
    {

        TimerT.Enabled = !flag;
    }

    protected void UpdateSummary()
    {
        string sTransId = mTransIdTxt.Text;
        if (sTransId.Length == 0)
        {
            return;
        }
        msqlConn = FKWebTools.GetDBPool();
        if (msqlConn.State != ConnectionState.Open)
        {
            msqlConn.Open();
        }
        bool ISUpdated = false;
        while (ISUpdated != true)
        {
            string sSql = "select status,device_id from tbl_fkcmd_trans where trans_id='" + sTransId + "'";
            DataSet DsSumm = new DataSet();
            DsSumm = cn.FillDataSet(sSql);
            if (DsSumm.Tables[0].Rows.Count > 0)
            {
                if (DsSumm.Tables[0].Rows[0][0].ToString().Trim() == "RESULT" || DsSumm.Tables[0].Rows[0][0].ToString().Trim() == "CANCELLED")
                {
                    if ((int)Session["operation"] == GET_DEVICE_STATUS)
                    {
                        string mDev = DsSumm.Tables[0].Rows[0][1].ToString().Trim();

                        try
                        {
                            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                            sTransId = mTransIdTxt.Text;
                            string DeviD = mDev;
                            if (sTransId.Length == 0)
                                return;
                            if (msqlConn.State != ConnectionState.Open)
                            {
                                msqlConn.Open();
                            }

                            sSql = "select trans_id from tbl_fkcmd_trans where trans_id='" + sTransId + "' AND status='RESULT'";
                            SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
                            SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                            sTransId = "";
                            if (sqlReader.HasRows)
                            {
                                if (sqlReader.Read())
                                {
                                    sTransId = sqlReader.GetString(0);
                                }
                            }
                            sqlReader.Close();

                            if (msqlConn.State != ConnectionState.Closed)
                            {
                                msqlConn.Close();
                            }
                            if (sTransId.Length == 0)
                                return;

                            if (msqlConn.State != ConnectionState.Open)
                            {
                                msqlConn.Open();
                            }
                            sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sTransId + "'";
                            sqlCmd = new SqlCommand(sSql, msqlConn);
                            SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
                            sqlParamCmdParamBin.Direction = ParameterDirection.Output;
                            sqlParamCmdParamBin.Size = -1;
                            sqlCmd.Parameters.Add(sqlParamCmdParamBin);

                            sqlCmd.ExecuteNonQuery();

                            byte[] bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
                            byte[] bytResultBin = new byte[0];
                            string sResultText;


                            cmdTrans.GetStringAndBinaryFromBSCommBuffer(bytCmdResult, out sResultText, out bytResultBin);
                            if (msqlConn.State != ConnectionState.Closed)
                            {
                                msqlConn.Close();
                            }


                            try
                            {
                                String TotUser = "";
                                String TotManager = "";
                                String TotFP = "";
                                String TotFace = "";
                                String TotPass = "";
                                String Totcard = "";
                                String TotLog = "";
                                String AllUser = "";
                                JObject jobjTest = JObject.Parse(sResultText);

                                try
                                {
                                    AllUser = jobjTest["total_user_count"].ToString();
                                }
                                catch
                                {
                                    AllUser = "0";
                                }
                                try
                                {
                                    TotUser = jobjTest["user_count"].ToString();
                                }
                                catch
                                {
                                    TotUser = "0";
                                }
                                try
                                {
                                    TotManager = jobjTest["manager_count"].ToString();
                                }
                                catch
                                {
                                    TotManager = "0";
                                }

                                try
                                {
                                    TotFP = jobjTest["fp_count"].ToString();
                                }
                                catch
                                {
                                    TotFP = "0";
                                }
                                try
                                {
                                    TotFace = jobjTest["face_count"].ToString();
                                }
                                catch
                                {
                                    TotFace = "0";
                                }
                                try
                                {
                                    TotPass = jobjTest["password_count"].ToString();
                                }
                                catch
                                {
                                    TotPass = "0";
                                }
                                try
                                {
                                    Totcard = jobjTest["idcard_count"].ToString();
                                }
                                catch
                                {
                                    Totcard = "0";
                                }
                                try
                                {
                                    TotLog = jobjTest["total_log_count"].ToString();
                                }
                                catch
                                {
                                    TotLog = "0";
                                }
                                sSql = " update tbl_fkdevice_status set total_user_count=" + AllUser + ",user_count=" + TotUser + ",manager_count=" + TotManager + ",fp_count=" + TotFP + ", " +
                                             " face_count=" + TotFace + ",password_count=" + TotPass + ",idcard_count=" + Totcard + ",total_log_count=" + TotLog + " where device_id='" + DeviD + "'";

                                cn.execute_NonQuery(sSql);
                                ISUpdated = true;
                            }
                            catch (Exception Exc)
                            {
                                Error_Occured("DeviceStatus", Exc.Message);
                            }
                        }
                        catch (Exception Exc)
                        {
                            Error_Occured("DeviceStatusFun", Exc.Message);
                        }
                        return;
                    }
                }
            }
        }

    }

    protected void Timer_Watch(object sender, EventArgs e)
    {
        string sTransId = mTransIdTxt.Text;
        if (sTransId.Length == 0)
        {
            return;
        }
        msqlConn = FKWebTools.GetDBPool();
        if (msqlConn.State != ConnectionState.Open)
        {
            msqlConn.Open();
        }
        string sSql = "select status,device_id from tbl_fkcmd_trans where trans_id='" + sTransId + "'";
        SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
        SqlDataReader sqlReader = sqlCmd.ExecuteReader();
        try
        {
            if (sqlReader.HasRows)
            {
                if (sqlReader.Read())
                {

                    if (sqlReader.GetString(0) == "RESULT" || sqlReader.GetString(0) == "CANCELLED")
                    {

                        Enables(true);


                        if ((int)Session["operation"] == GET_DEVICE_STATUS)
                        {
                            string mDev = sqlReader.GetString(1).ToString().Trim();
                            sqlReader.Close();
                            DeviceStatus(mDev);
                            return;
                        }
                    }
                    else
                    {

                    }

                }
                sqlReader.Close();
            }
            if (msqlConn.State != ConnectionState.Closed)
            {
                msqlConn.Close();
            }
        }
        catch (Exception ex)
        {
            Error_Occured("Timer", ex.Message);
            sqlReader.Close();
        }
    }
    private void DeviceStatus(string mDevId)
    {
        try
        {
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            string sTransId = mTransIdTxt.Text;
            string DeviD = mDevId;
            if (sTransId.Length == 0)
                return;
            if (msqlConn.State != ConnectionState.Open)
            {
                msqlConn.Open();
            }

            string sSql = "select trans_id from tbl_fkcmd_trans where trans_id='" + sTransId + "' AND status='RESULT'";
            SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
            SqlDataReader sqlReader = sqlCmd.ExecuteReader();

            sTransId = "";
            if (sqlReader.HasRows)
            {
                if (sqlReader.Read())
                {
                    sTransId = sqlReader.GetString(0);
                }
            }
            sqlReader.Close();

            if (msqlConn.State != ConnectionState.Closed)
            {
                msqlConn.Close();
            }
            if (sTransId.Length == 0)
                return;

            if (msqlConn.State != ConnectionState.Open)
            {
                msqlConn.Open();
            }
            sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sTransId + "'";
            sqlCmd = new SqlCommand(sSql, msqlConn);
            SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
            sqlParamCmdParamBin.Direction = ParameterDirection.Output;
            sqlParamCmdParamBin.Size = -1;
            sqlCmd.Parameters.Add(sqlParamCmdParamBin);

            sqlCmd.ExecuteNonQuery();

            byte[] bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
            byte[] bytResultBin = new byte[0];
            string sResultText;


            cmdTrans.GetStringAndBinaryFromBSCommBuffer(bytCmdResult, out sResultText, out bytResultBin);
            if (msqlConn.State != ConnectionState.Closed)
            {
                msqlConn.Close();
            }


            try
            {
                String TotUser = "";
                String TotManager = "";
                String TotFP = "";
                String TotFace = "";
                String TotPass = "";
                String Totcard = "";
                String TotLog = "";
                String AllUser = "";
                JObject jobjTest = JObject.Parse(sResultText);

                try
                {
                    AllUser = jobjTest["total_user_count"].ToString();
                }
                catch
                {
                    AllUser = "0";
                }
                try
                {
                    TotUser = jobjTest["user_count"].ToString();
                }
                catch
                {
                    TotUser = "0";
                }
                try
                {
                    TotManager = jobjTest["manager_count"].ToString();
                }
                catch
                {
                    TotManager = "0";
                }

                try
                {
                    TotFP = jobjTest["fp_count"].ToString();
                }
                catch
                {
                    TotFP = "0";
                }
                try
                {
                    TotFace = jobjTest["face_count"].ToString();
                }
                catch
                {
                    TotFace = "0";
                }
                try
                {
                    TotPass = jobjTest["password_count"].ToString();
                }
                catch
                {
                    TotPass = "0";
                }
                try
                {
                    Totcard = jobjTest["idcard_count"].ToString();
                }
                catch
                {
                    Totcard = "0";
                }
                try
                {
                    TotLog = jobjTest["total_log_count"].ToString();
                }
                catch
                {
                    TotLog = "0";
                }
                sSql = " update tbl_fkdevice_status set total_user_count=" + AllUser + ",user_count=" + TotUser + ",manager_count=" + TotManager + ",fp_count=" + TotFP + ", " +
                             " face_count=" + TotFace + ",password_count=" + TotPass + ",idcard_count=" + Totcard + ",total_log_count=" + TotLog + " where device_id='" + DeviD + "'";

                cn.execute_NonQuery(sSql);

            }
            catch (Exception Exc)
            {
                Error_Occured("DeviceStatus", Exc.Message);
            }
        }
        catch (Exception Exc)
        {
            Error_Occured("DeviceStatusFun", Exc.Message);
        }
    }

    private void UpdateDeviceInfo(string sResultText, string DeviD)
    {
        try
        {
            String TotUser;
            String TotManager;
            String TotFP;
            String TotFace;
            String TotPass;
            String Totcard;
            String TotLog;
            String AllUser;

            JObject jobjTest = JObject.Parse(sResultText);

            AllUser = jobjTest["total_user_count"].ToString();
            TotUser = jobjTest["user_count"].ToString();
            TotManager = jobjTest["manager_count"].ToString();
            try
            {
                TotFP = jobjTest["fp_count"].ToString();
            }
            catch
            {
                TotFP = "0";
            }
            try
            {
                TotFace = jobjTest["face_count"].ToString();
            }
            catch
            {
                TotFace = "0";
            }

            TotPass = jobjTest["password_count"].ToString();
            Totcard = jobjTest["idcard_count"].ToString();
            TotLog = jobjTest["total_log_count"].ToString();
            string sSql = " update tbl_fkdevice_status set total_user_count=" + AllUser + ",user_count=" + TotUser + ",manager_count=" + TotManager + ",fp_count=" + TotFP + ", " +
                         " face_count=" + TotFace + ",password_count=" + TotPass + ",idcard_count=" + Totcard + ",total_log_count=" + TotLog + " where device_id='" + DeviD + "'";
            cn.execute_NonQuery(sSql);

        }
        catch (Exception Exc)
        {
            Error_Occured("Update Device Info", Exc.Message);
        }
    }
    public static bool ValidateIPv4(string ipString)
    {
        if (ipString.Count(c => c == '.') != 3) return false;
        IPAddress address;
        return IPAddress.TryParse(ipString, out address);
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {

            var c = ((ASPxGridView)sender).Columns["GroupID"] as GridViewDataColumn;
            var lc = ((ASPxGridView)sender).FindEditRowCellTemplateControl(c, "ASPxGridLookup1") as ASPxGridLookup;
            var cc = ((ASPxGridView)sender).Columns["CompanyCode"] as GridViewDataColumn;
            var lcc = ((ASPxGridView)sender).FindEditRowCellTemplateControl(cc, "GridLookupcomp") as ASPxGridLookup;
            string IsMaster = "N";
            string IsAuth = "N";
            string k = e.Keys["device_id"].ToString();
            string IPAdd = e.NewValues["IPAddrss"].ToString();
            string DevMode = e.NewValues["DeviceMode"].ToString();
            string CCode =""; 
            Session["DeviceType"] = DevMode;
            string Purpose = "A";
            int TZ = 330;
            try
            {
                IsMaster = e.NewValues["IsMasterDevice"].ToString().Trim();
            }
            catch
            {
                IsMaster = "N";
            }
            try
            {
                Purpose = e.NewValues["MachineType"].ToString().Trim();
            }
            catch
            {
                Purpose = "A";
            }

            try
            {
                IsAuth = e.NewValues["IsAuthorized"].ToString().Trim();
            }
            catch
            {
                IsAuth = "N";
            }
            string DevName = "";

            try
            {
                DevName = e.NewValues["device_name"].ToString().Trim();
            }
            catch
            {
                DevName = "";
            }

            try
            {
                TZ = Convert.ToInt32(e.NewValues["TimeZone"].ToString().Trim());
            }
            catch
            {
                TZ = 330;
            }
            try
            {
                CCode = e.NewValues["CompanyCode"].ToString();
            }
            catch
            {
                CCode = "";
            }

            string Group = lc.Text.ToString().Trim();
            CCode= lcc.Text.ToString().Trim();

            if (DevMode == "BIO")
            {
                string sSql = " update tbl_fkdevice_status  set   CompanyCode='"+ CCode.Trim() +"',TimeZone=" + TZ + ",  ismasterdevice='" + IsMaster.ToString().Trim() + "',IsAuthorized='" + IsAuth.Trim() + "', device_name='" + DevName.Trim() + "',IPAddrss='" + IPAdd.Trim() + "',GroupID='" + Group.Trim() + "' where device_id='" + k.ToString().Trim() + "'";
                cn.execute_NonQuery(sSql);
            }
            else
            {
                string sSql = " update tblmachine set MachineType='"+ Purpose.Trim() +"',   CompanyCode='" + CCode.Trim() + "' , ClientID= " + DataFilter.LoginClientID + ",  IsAuthorized='" + IsAuth.Trim() + "',ismasterdevice='" + IsMaster.ToString().Trim() + "', devicename='" + DevName.Trim() + "',Location='" + IPAdd.Trim() + "',GroupID='" + Group.Trim() + "' where Serialnumber='" + k.ToString().Trim() + "'";
                cn.execute_NonQuery(sSql);
            }
            if (DevMode == "BIO")
            {
                try
                {
                    JObject vResultJson = new JObject();
                    FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                    vResultJson.Add("fk_name", DevName);
                    string sFinal = vResultJson.ToString(Formatting.None);
                    byte[] strParam = new byte[0];
                    cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                    mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "SET_FK_NAME", k, strParam);


                }
                catch (Exception Exc)
                {
                    Error_Occured("DeviceNameChange Line --702", Exc.Message);
                }

                try
                {
                    DateTime now = DateTime.Now.AddHours(1);
                    string sNowTxt = FKWebTools.GetFKTimeString14(now);
                    JObject vResultJson = new JObject();
                    FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                    vResultJson.Add("time", sNowTxt);
                    string sFinal = vResultJson.ToString(Formatting.None);
                    byte[] strParam = new byte[0];
                    cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                    mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "SET_TIME", k, strParam);

                }
                catch (Exception Exc)
                {
                    Error_Occured("SetTime Line --742", Exc.Message);
                }

            }

            ASPxGridView1.CancelEdit();
            e.Cancel = true;
            GetDevieListAll();
        }
        catch (Exception Exc)
        {
            Error_Occured("ASPxGridView1_RowUpdating", Exc.Message);
        }
    }
    protected void Lookup_InitComp(object sender, EventArgs e)
    {
        try
        {
           
            ASPxGridLookup lc = (ASPxGridLookup)sender;
            int editingRowVisibleIndex = ASPxGridView1.EditingRowVisibleIndex;
            if (editingRowVisibleIndex >= 0)
            {
                string rowValue = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "CompanyCode").ToString().Trim();
                string[] rowValueItems = rowValue.Split(',');
                List<string> rowValueItemsAsList = new List<string>();
                rowValueItemsAsList.AddRange(rowValueItems);
                for (int i = 0; i <= rowValueItems.Count() - 1; i++)
                {
                    lc.GridView.Selection.SetSelectionByKey(rowValueItems[i].ToString().Trim(), true);
                }


            }
            if (Session["LoginCompany"].ToString().Trim().ToUpper() == "ADMIN")
            {
                lc.ReadOnly = true;
            }
            else
            {
                lc.ReadOnly = false;
            }
        }
        catch
        {

        }
       
    }

    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {


            DataSet ds = (DataSet)Session["DataSet"];
            ASPxGridView gridView = (ASPxGridView)sender;
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                    if (G_Model == "BIO")
                    {
                        string sSql = " delete from tbl_fkdevice_status where device_id='" + mDevId.ToString().Trim() + "'";
                        cn.execute_NonQuery(sSql);
                    }
                    else
                    {
                        string sSql = " delete from tblmachine where serialnumber='" + mDevId.ToString().Trim() + "'";
                        cn.execute_NonQuery(sSql);
                    }
                }
            }




            gridView.CancelEdit();
            e.Cancel = true;
            GetDevieListAll();


        }
        catch (Exception Exc)
        {
            Error_Occured("ASPxGridView1_RowDeleting", Exc.Message);
        }
    }



    protected void Lookup_Init(object sender, EventArgs e)
    {
        ASPxGridLookup lc = (ASPxGridLookup)sender;
        int editingRowVisibleIndex = ASPxGridView1.EditingRowVisibleIndex;
        if (editingRowVisibleIndex >= 0)
        {
            string rowValue = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "GroupID").ToString().Trim();
            string[] rowValueItems = rowValue.Split(',');
            List<string> rowValueItemsAsList = new List<string>();
            rowValueItemsAsList.AddRange(rowValueItems);
            for (int i = 0; i <= rowValueItems.Count() - 1; i++)
            {
                lc.GridView.Selection.SetSelectionByKey(rowValueItems[i].ToString().Trim(), true);
            }


        }
    }


    protected void ASPxGridView1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
       
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.EditFormCaption.ToString() == "Company")
            {
                e.Editor.ReadOnly = true;
            }
            if (e.Column.Caption == "Company")
            {
                if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
                {
                    e.Editor.ReadOnly = false;
                }
                else
                {
                    e.Editor.ReadOnly = true;
                    e.Column.Caption = "";
                    e.Editor.Caption = "";
                    e.Column.FieldName = "";
                }
            }
            if (e.Column.FieldName == "device_id" || e.Column.FieldName == "connected" || e.Column.FieldName == "last_update_time" || e.Column.FieldName == "user_count" || e.Column.FieldName == "fp_count" || e.Column.FieldName == "face_count" || e.Column.FieldName == "password_count" || e.Column.FieldName == "idcard_count" || e.Column.FieldName == "manager_count")
            {
                e.Editor.ReadOnly = true;

            }
            if (e.Column.FieldName == "device_name" || e.Column.FieldName == "IPAddrss")
            {
                e.Editor.ReadOnly = false;
            }


            if (e.Column.FieldName == "CompanyCode")
            {
                if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
                {
                    e.Editor.ReadOnly = false;
                }
                else
                {
                    e.Editor.ReadOnly = true;
                }
            }

        }
       
    }

    protected void ASPxGridView1_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string IPAdd = Convert.ToString(e.NewValues["IPAddrss"]);
        bool ValidIP = ValidateIPv4(IPAdd);
        string k = e.Keys["device_id"].ToString();
        if (ValidIP == false)
        {
            AddError(e.Errors, ASPxGridView1.Columns[0], "Invali IP");

            e.RowError = "*Invalid IP";
        }

        sSql = "Select * from tbl_fkdevice_status where IPAddrss='" + IPAdd + "' and device_id !='" + k.ToString().Trim() + "' ";
        DataSet DsIP = new DataSet();
        DsIP = cn.FillDataSet(sSql);
        if (DsIP.Tables[0].Rows.Count > 0)
        {
            AddError(e.Errors, ASPxGridView1.Columns[0], "Duplicate IP");

            e.RowError = "*Duplicate IP";
        }


    }

    protected void btOK_Click(object sender, EventArgs e)
    {

        try
        {
            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            string sBeginDate = FromDate.Value.ToString();
            string sEndDate = ToDate.Value.ToString();
            DateTime dtBegin, dtEnd;
            JObject vResultJson = new JObject();
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            DateTime AttstartDate = System.DateTime.Today;
            DateTime AttEndDate = System.DateTime.Today;
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }
            //for (int i = 0; i < keys.Count; i++)
            //{
            //    string mDevId = Convert.ToString(keys[i]);

            //}

            try
            {
                dtBegin = Convert.ToDateTime(sBeginDate);
                AttstartDate = Convert.ToDateTime(sBeginDate);
                sBeginDate = FKWebTools.GetFKTimeString14(dtBegin);
                vResultJson.Add("begin_time", sBeginDate);

            }
            catch
            {

                sBeginDate = "";
            }
            if (sEndDate.Length > 0)
            {
                try
                {
                    dtEnd = Convert.ToDateTime(sEndDate);
                    AttEndDate = Convert.ToDateTime(sEndDate);
                    sEndDate = FKWebTools.GetFKTimeString14(dtEnd);
                    vResultJson.Add("end_time", sEndDate);

                }
                catch
                {

                    sEndDate = "";
                }
            }
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                    if (G_Model == "BIO")
                    {

                        string sFinal = vResultJson.ToString(Formatting.None);
                        byte[] strParam = new byte[0];
                        cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);
                        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "GET_LOG_DATA", mDevId, strParam);
                    }
                    else
                    {
                        sSql = "INSERT INTO DeviceCommands (SerialNumber,CommandContent, TransferToDevice, Executed,IsOnline, CreatedOn, StartDateATTLOG, EndDateATTLOG,Priority) VALUES('" + mDevId.Trim() + "','ATTLOG','" + mDevId.Trim() + "',0,1,GETDATE(), '" + AttstartDate.ToString("yyyy-MM-dd") + "','" + AttEndDate.ToString("yyyy-MM-dd") + "','2')";
                        cn.execute_NonQuery(sSql);

                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);
        }
        catch (Exception Exc)
        {
            Error_Occured("DownloadLog", Exc.Message);
        }


    }

    protected void btCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("DeviceList.aspx");
    }

    protected void pcLogin_Load(object sender, EventArgs e)
    {
        try
        {
            if(!IsPostBack)
            {
                 FromDate.Value = System.DateTime.Now;
                 ToDate.Value = System.DateTime.Now;
            }
           

        }
        catch
        {

        }
    }

    protected void ASPxSync_Click(object sender, EventArgs e)
    {
        try
        {

            string sSql = "";
            DataSet DsUser = new DataSet();
            List<object> keys = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });

            JObject vResultJson = new JObject();
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            string sUserId, sUserName, sPreviledge;
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }

            //for (int i = 0; i < keys.Count; i++)
            //{

            //    string mDevId = Convert.ToString(keys[i]);
                
            //    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
            //    if (G_Model == "BIO")
            //    {
            //        sSql = "delete from tbl_fkcmd_trans_cmd_result_user_id_list where device_id=''";
            //        cn.execute_NonQuery(sSql);
            //        Session["operation"] = GET_USER_ID_LIST;
            //        msqlConn = FKWebTools.GetDBPool();
            //        mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "GET_USER_ID_LIST", mDevId, null);
            //    }
            //    else
            //    {
            //        sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevId.Trim() + "','CHECK','" + mDevId.Trim() + "',0,1,getdate(),'3')";
            //        cn.execute_NonQuery(sSql);
            //    }
            //}



            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {

                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = ASPxGridView1.GetRowValues(i, "device_id").ToString().Trim();
                    G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();

                        G_Model = ASPxGridView1.GetRowValues(i, "DeviceMode").ToString().Trim();
                        if (G_Model == "BIO")
                        {
                            sSql = "delete from tbl_fkcmd_trans_cmd_result_user_id_list where device_id=''";
                            cn.execute_NonQuery(sSql);
                            Session["operation"] = GET_USER_ID_LIST;
                            msqlConn = FKWebTools.GetDBPool();
                            mTransIdTxt.Text = FKWebTools.MakeCmd(msqlConn, "GET_USER_ID_LIST", mDevId, null);
                        }
                        else
                        {
                            sSql = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevId.Trim() + "','CHECK','" + mDevId.Trim() + "',0,1,getdate(),'3')";
                            cn.execute_NonQuery(sSql);
                        }
                    }
                }
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='DeviceList.aspx'", true);



        }
        catch (Exception Ex)
        {
            Error_Occured("GetUserList", Ex.Message);
        }
    }

    private Userinfo getUserInfo(string user_id)
    {
        Userinfo userinfo = new Userinfo();
        string sSql = "SELECT @user_name=[user_name],@privilige=[privilige],@card=[card],@Password=[Password],@Face=[Face],@photo=[photo],@Fp_0=[Fp_0],@Fp_1=[Fp_1],"
            + "@Fp_2=[Fp_2],@Fp_3=[Fp_3],@Fp_4=[Fp_4],@Fp_5=[Fp_5],@Fp_6=[Fp_6],@Fp_7=[Fp_7],@Fp_8=[Fp_8],@Fp_9=[Fp_9] FROM tbl_realtime_userinfo where user_id = '" + user_id + "'";

        //sSql = "";
        //sSql = "SELECT @user_name=[empname],@privilige=[privilige],@card=[card],@Password=[Password],@Face=[Face],@photo=[photo],@Fp_0=[Fp_0],@Fp_1=[Fp_1],"
        //    + "@Fp_2=[Fp_2],@Fp_3=[Fp_3],@Fp_4=[Fp_4],@Fp_5=[Fp_5],@Fp_6=[Fp_6],@Fp_7=[Fp_7],@Fp_8=[Fp_8],@Fp_9=[Fp_9] FROM tbl_realtime_userinfo,TblEmployee  where CAST(tblemployee.presentcardno as int)=tbl_realtime_userinfo.user_id and user_id = '" + user_id + "'";



        //sSql = "Select TE.EmpName empname,TRT.privilige,TRT.card,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige,TRT.privilige " +
        //       " FROM tbl_realtime_userinfo,TblEmployee  where CAST(tblemployee.presentcardno as int)=tbl_realtime_userinfo.user_id and user_id = '" + user_id + "'  ";


        SqlCommand sqlCmd = new SqlCommand(sSql, con);

        SqlParameter sql_user_name = new SqlParameter("@user_name", SqlDbType.VarChar);
        sql_user_name.Direction = ParameterDirection.Output;
        sql_user_name.Size = -1;
        sqlCmd.Parameters.Add(sql_user_name);

        SqlParameter sql_privilige = new SqlParameter("@privilige", SqlDbType.VarChar);
        sql_privilige.Direction = ParameterDirection.Output;
        sql_privilige.Size = -1;
        sqlCmd.Parameters.Add(sql_privilige);

        SqlParameter sql_card = new SqlParameter("@card", SqlDbType.VarChar);
        sql_card.Direction = ParameterDirection.Output;
        sql_card.Size = -1;
        sqlCmd.Parameters.Add(sql_card);

        SqlParameter sql_Password = new SqlParameter("@Password", SqlDbType.VarChar);
        sql_Password.Direction = ParameterDirection.Output;
        sql_Password.Size = -1;
        sqlCmd.Parameters.Add(sql_Password);

        SqlParameter sql_Face = new SqlParameter("@Face", SqlDbType.VarBinary);
        sql_Face.Direction = ParameterDirection.Output;
        sql_Face.Size = -1;
        sqlCmd.Parameters.Add(sql_Face);

        SqlParameter sql_photo = new SqlParameter("@photo", SqlDbType.VarBinary);
        sql_photo.Direction = ParameterDirection.Output;
        sql_photo.Size = -1;
        sqlCmd.Parameters.Add(sql_photo);

        SqlParameter sql_Fp_0 = new SqlParameter("@Fp_0", SqlDbType.VarBinary);
        sql_Fp_0.Direction = ParameterDirection.Output;
        sql_Fp_0.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_0);

        SqlParameter sql_Fp_1 = new SqlParameter("@Fp_1", SqlDbType.VarBinary);
        sql_Fp_1.Direction = ParameterDirection.Output;
        sql_Fp_1.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_1);

        SqlParameter sql_Fp_2 = new SqlParameter("@Fp_2", SqlDbType.VarBinary);
        sql_Fp_2.Direction = ParameterDirection.Output;
        sql_Fp_2.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_2);

        SqlParameter sql_Fp_3 = new SqlParameter("@Fp_3", SqlDbType.VarBinary);
        sql_Fp_3.Direction = ParameterDirection.Output;
        sql_Fp_3.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_3);

        SqlParameter sql_Fp_4 = new SqlParameter("@Fp_4", SqlDbType.VarBinary);
        sql_Fp_4.Direction = ParameterDirection.Output;
        sql_Fp_4.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_4);

        SqlParameter sql_Fp_5 = new SqlParameter("@Fp_5", SqlDbType.VarBinary);
        sql_Fp_5.Direction = ParameterDirection.Output;
        sql_Fp_5.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_5);

        SqlParameter sql_Fp_6 = new SqlParameter("@Fp_6", SqlDbType.VarBinary);
        sql_Fp_6.Direction = ParameterDirection.Output;
        sql_Fp_6.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_6);

        SqlParameter sql_Fp_7 = new SqlParameter("@Fp_7", SqlDbType.VarBinary);
        sql_Fp_7.Direction = ParameterDirection.Output;
        sql_Fp_7.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_7);

        SqlParameter sql_Fp_8 = new SqlParameter("@Fp_8", SqlDbType.VarBinary);
        sql_Fp_8.Direction = ParameterDirection.Output;
        sql_Fp_8.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_8);

        SqlParameter sql_Fp_9 = new SqlParameter("@Fp_9", SqlDbType.VarBinary);
        sql_Fp_9.Direction = ParameterDirection.Output;
        sql_Fp_9.Size = -1;
        sqlCmd.Parameters.Add(sql_Fp_9);

        FKWebTools.GetDBPool();
        if (con.State != ConnectionState.Open)
        {
            con.Open();
        }
        sqlCmd.ExecuteNonQuery();

        if (con.State != ConnectionState.Closed)
        {
            con.Close();
        }
        userinfo.user_id = user_id;
        if (sql_user_name.Value.ToString().Trim().Length > 14)
        {
            sql_user_name.Value = sql_user_name.Value.ToString().Trim().Substring(0, 13);
        }

        userinfo.user_name = FormatSqlStringData(sql_user_name.Value.ToString().Trim());
        userinfo.user_privilege = FormatSqlStringData(sql_privilige.Value);
        userinfo.pwdData = FormatSqlStringData(sql_Password.Value);
        userinfo.cardData = FormatSqlStringData(sql_card.Value);
        userinfo.photoData = FormatSqlbyteData(sql_photo.Value);
        userinfo.faceData = FormatSqlbyteData(sql_Face.Value);
        userinfo.fpData[0] = FormatSqlbyteData(sql_Fp_0.Value);
        userinfo.fpData[1] = FormatSqlbyteData(sql_Fp_1.Value);
        userinfo.fpData[2] = FormatSqlbyteData(sql_Fp_2.Value);
        userinfo.fpData[3] = FormatSqlbyteData(sql_Fp_3.Value);
        userinfo.fpData[4] = FormatSqlbyteData(sql_Fp_4.Value);
        userinfo.fpData[5] = FormatSqlbyteData(sql_Fp_5.Value);
        userinfo.fpData[6] = FormatSqlbyteData(sql_Fp_6.Value);
        userinfo.fpData[7] = FormatSqlbyteData(sql_Fp_7.Value);
        userinfo.fpData[8] = FormatSqlbyteData(sql_Fp_8.Value);
        userinfo.fpData[9] = FormatSqlbyteData(sql_Fp_9.Value);
        return userinfo;
    }
    private string FormatSqlStringData(object value)
    {
        if (value == null) return string.Empty;
        return value == DBNull.Value ? string.Empty : value.ToString();
    }

    private byte[] FormatSqlbyteData(object value)
    {
        if (value == null) return null;
        return value == DBNull.Value ? null : (byte[])value;
    }

    protected void ASPxGridView1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView1.DataBind();
    }

    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        try
        {
            if (ASPxComboBox2.SelectedItem.Text.ToString().Trim() == "Excel")
            {
                ASPxGridViewExporter1.WriteXlsxToResponse();
            }
            else if (ASPxComboBox2.SelectedItem.Text.ToString().Trim() == "PDF")
            {
                ASPxGridViewExporter1.WritePdfToResponse();
            }
            else if (ASPxComboBox2.SelectedItem.Text.ToString().Trim() == "CSV")
            {
                ASPxGridViewExporter1.WritePdfToResponse();
            }
        }
        catch (Exception er)
        {
            Error_Occured("ExportData", er.Message);
        }
    }

    protected void TimerT_Tick(object sender, EventArgs e)
    {
        string sTransId = mTransIdTxt.Text;
        if (sTransId.Length == 0)
        {
            return;
        }
        string sSql = "select status,device_id from tbl_fkcmd_trans where trans_id='" + sTransId + "'";
        SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
        SqlDataReader sqlReader = sqlCmd.ExecuteReader();
        try
        {
            if (sqlReader.HasRows)
            {
                if (sqlReader.Read())
                {

                    if (sqlReader.GetString(0) == "RESULT" || sqlReader.GetString(0) == "CANCELLED")
                    {


                        TimerT.Enabled = false;

                        if ((int)Session["operation"] == GET_DEVICE_STATUS)
                        {
                            sqlReader.Close();
                            DeviceStatus(sqlReader.GetString(1).ToString().Trim());
                            return;
                        }
                    }
                    else
                    {

                    }

                }
                sqlReader.Close();
            }

        }
        catch (Exception ex)
        {
            Error_Occured("Timer", ex.Message);
            sqlReader.Close();
        }
    }




    protected void ASPxGridView1_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
    {
        try
        {
            if (Session["username"].ToString().Trim().ToUpper() != "ADMIN")
            {

                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
                {
                    e.Enabled = false;
                }
                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
                {
                    e.Enabled = false;
                }

            }
        }
        catch
        {

        }
    }
}