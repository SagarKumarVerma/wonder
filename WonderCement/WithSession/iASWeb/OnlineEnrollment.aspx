﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="OnlineEnrollment.aspx.cs" Inherits="OnlineEnrollment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
       <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True">
        </asp:ScriptManager>
        <div id="Frame" style="align-content: center; position: absolute; top: 150px; left: 550px; width: 500px; height: 150px;">


            <table border="1" align="center" width="450px" style="border-color: #000000; color: #000000;">

                <tr>
                    <td colspan="2" align="center" style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Online User Registration" Theme="SoftOrange" Font-Bold="True" Font-Size="Large"></dx:ASPxLabel>
                        <br />
                        <dx:ASPxTextBox ID="mTransIdTxt" runat="server" Width="170px" Visible="false"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="width: auto; padding: 5px">
                        <br />
                    </td>

                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="lblDevType" runat="server" Text="Device Type*:" Visible="true"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxRadioButton ID="radZK" Text="ZK" GroupName="A" Checked="true" AutoPostBack="true" runat="server" OnCheckedChanged="radZK_CheckedChanged"  Theme="SoftOrange"></dx:ASPxRadioButton>

                        <dx:ASPxRadioButton ID="radHK" Text="HT Series" GroupName="A" AutoPostBack="true" runat="server" OnCheckedChanged="radZK_CheckedChanged"  Theme="SoftOrange"></dx:ASPxRadioButton>
                    </td>

                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Device Name*:"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <%--<dx:ASPxTextBox ID="txtDeviD" runat="server" Width="170px">
                    <ValidationSettings SetFocusOnError="True">
                        <RequiredField IsRequired="True"/>
                        
                    </ValidationSettings>
                </dx:ASPxTextBox>--%>
                        <dx:ASPxComboBox ID="GrdDevice" runat="server" ValueType="System.String" Theme="SoftOrange" NullText="NONE"></dx:ASPxComboBox>
                    </td>

                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Employee ID*:"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxTextBox ID="txtuserID" runat="server" Width="170px">
                            <ValidationSettings SetFocusOnError="True">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Employee Name:"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxTextBox ID="UserName" runat="server" Width="170px">
                            <ValidationSettings SetFocusOnError="True">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Card:"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxTextBox ID="CardNum" runat="server" Width="170px"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Password:"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxTextBox ID="Password" runat="server" Width="170px"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Mode:"></dx:ASPxLabel>
                    </td>
                    <td style="width: auto; padding: 5px">
                        <dx:ASPxRadioButton ID="radFace" Text="Face" GroupName="B" Checked="true" AutoPostBack="true" runat="server" Theme="SoftOrange"></dx:ASPxRadioButton>

                        <dx:ASPxRadioButton ID="radFP" Text="Finger" GroupName="B" AutoPostBack="true" runat="server" Theme="SoftOrange"></dx:ASPxRadioButton>
                    </td>
                </tr>

                <tr>

                    <td style="width: auto; padding: 5px" align="center">&nbsp;</td>
                    <td style="width: auto; padding: 5px" align="right">
                        <dx:ASPxButton ID="btnUploadOffline" runat="server" Text="Enroll" Theme="SoftOrange" Width="50px" AutoPostBack="true" OnClick="btnUploadOffline_Click">
                        </dx:ASPxButton>

                        <dx:ASPxButton ID="btnExit" runat="server" Text="Exit" Theme="SoftOrange" Width="50px" ValidationGroup="A">
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr>
                    <td style="width: auto; padding: 15px" colspan="2">
                        <dx:ASPxLabel ID="lblStatus" runat="server" Text="" ForeColor="Red" Font-Size="Small"></dx:ASPxLabel>

                    </td>

                </tr>
            </table>

        </div>

    </div>
</asp:Content>

