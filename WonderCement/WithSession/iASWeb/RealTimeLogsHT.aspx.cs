﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using System.Text;
using DevExpress.Web;
public partial class RealTimeLogsHT : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    DateTime FromDate = System.DateTime.MinValue;
    DateTime ToDate = System.DateTime.MinValue;


    ErrorClass ec = new ErrorClass();

    SqlConnection msqlConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            GetLogs();
            msqlConn = FKWebTools.GetDBPool();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetLogs();
        ASPxGridView1.DataBind();
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
        }
    }

    public void GetLogs()
    {
        try
        {
           
                msqlConn = FKWebTools.GetDBPool();
                if (msqlConn.State != ConnectionState.Open)
                {
                    msqlConn.Open();
                }
                DataSet dsLog = new DataSet();
                string strSelectCmd;
                 if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
                 {
                     strSelectCmd = "SELECT  top 20 * FROM UserAttendance where AttDateTime >= '" + System.DateTime.Today.ToString("yyyy-MM-dd") + "'  order by AttDateTime desc";
                 }
                 else
                 {
                     strSelectCmd = "SELECT  top 20 * FROM UserAttendance where AttDateTime >= '" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and DeviceID In (select SerialNumber from tblMachine where CompanyCode like '%" + Session["LoginCompany"].ToString().Trim() + "%')   order by AttDateTime desc";
                 }
                 
                SqlDataAdapter da = new SqlDataAdapter(strSelectCmd, msqlConn);
                da.Fill(dsLog, "UserAttendance");
                DataView dvLog = dsLog.Tables["UserAttendance"].DefaultView;
                // dvLog.Sort = ViewState["SortExpression"].ToString();
                ASPxGridView1.DataSource = dvLog;
                ASPxGridView1.DataBind();
           
        }

        catch (Exception er)
        {
            Error_Occured("GetGroup", er.Message);
        }

    }



    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        try
        {


            if (ASPxComboBox2.SelectedItem.Text.ToString().Trim() == "Excel")
            {
                ASPxGridViewExporter1.WriteXlsxToResponse();
            }
            else if (ASPxComboBox2.SelectedItem.Text.ToString().Trim() == "PDF")
            {
                ASPxGridViewExporter1.WritePdfToResponse();
            }
            else if (ASPxComboBox2.SelectedItem.Text.ToString().Trim() == "CSV")
            {
                ASPxGridViewExporter1.WritePdfToResponse();
            }
        }
        catch (Exception er)
        {
            Error_Occured("ExportData", er.Message);
        }



    }



    protected void ASPxGridView1_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView1.PageIndex = pageIndex;
        this.GetLogs();
    }

    protected void ASPxGridView1_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "Temprature In Celsius")
        {

            double Temp = 0;
            try
            {
                Temp = Convert.ToDouble(e.GetValue("io_mode").ToString().Trim());
            }
            catch
            {

            }

            if (Temp > 37)
            {
                e.Cell.ForeColor = System.Drawing.Color.Red;

            }

            else
            {
                e.Cell.ForeColor = System.Drawing.Color.Green;

            }
        }
    }
}