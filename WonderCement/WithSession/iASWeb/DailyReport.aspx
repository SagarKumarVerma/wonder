﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="DailyReport.aspx.cs" Inherits="DailyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <style type="text/css">
        .style1 {
            width: 16%;           
        }

         .auto-style_Tr_Height {
            height: 28px;
        }
         </style>
    <table style="width: 100%">
        <tr>
            <td>
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100%"><PanelCollection>
<dx:PanelContent runat="server">
    <table style="width: 100%">
        <tr>
            <td colspan="6" align="center">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Daily Report"></dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="From Date">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxDateEdit ID="txtDateFrom" runat="server" AutoPostBack="True" Date="2020-05-10" OnTextChanged="txtDateFrom_TextChanged" Theme="SoftOrange" Width="120px">
                </dx:ASPxDateEdit>
            </td>
            <td align="center">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="To Date">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxDateEdit ID="txtToDate" runat="server" AutoPostBack="True" Date="2020-05-10" Theme="SoftOrange" Width="120px">
                </dx:ASPxDateEdit>
            </td>
            <td align="center">
                <dx:ASPxButton ID="cmdGenerate" runat="server" Text="Generate">
                </dx:ASPxButton>
            </td>
            <td><dx:ASPxRadioButtonList ID="RadExport" runat="server" EnableTheming="True" Height="19px" RepeatDirection="Horizontal" Style="top: 0px" Theme="SoftOrange">
                <Items>
                    <dx:ListEditItem Text="Preview" Value="0"  />
                    <dx:ListEditItem Text="Excel" Selected="true" Value="1"  />
                    <dx:ListEditItem Text="PDF" Value="0"  />
                </Items>
            </dx:ASPxRadioButtonList></td>
        </tr>
    </table>
                    </dx:PanelContent>
</PanelCollection>
                </dx:ASPxPanel>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                       <td class="style1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Select Company">
                            </dx:ASPxLabel>
                        </td>
                        <td class="style1">
                            <dx:ASPxGridLookup runat="server" KeyFieldName="COMPANYCODE"  SelectionMode="Multiple" 
                                ID="lookupComp" TextFormatString="{0}" MultiTextSeparator=",">
                                <GridViewProperties>
                                    <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True">
                                    </SettingsBehavior>
                                    <Settings ShowFilterRow="True" GroupFormat="{0}-{1}">
                                    </Settings>
                                </GridViewProperties>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                    <dx:GridViewDataTextColumn FieldName="COMPANYCODE" ShowInCustomizationForm="True" Caption="Code" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="COMPANYNAME" ShowInCustomizationForm="True" Caption="Name" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridLookup>
                        </td>
                        <td  class="style1" align="center"> <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Select Location"></dx:ASPxLabel>
                        </td>
                        <td  class="style1">
                            <dx:ASPxGridLookup runat="server" ID="lookupDept" KeyFieldName="DepartmentCode" SelectionMode="Multiple"
                                TextFormatString="{0}" MultiTextSeparator="," AllowUserInput="False">
                                <GridViewProperties>
                                    <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                    <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                </GridViewProperties>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                    <dx:GridViewDataTextColumn Caption="Code" FieldName="DepartmentCode" ShowInCustomizationForm="True" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Name" FieldName="DepartmentName" ShowInCustomizationForm="True" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridLookup></td>
                        <td class="style1">&nbsp;</td>
                       <td class="style1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                      <td class="style1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                      <td class="style1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                      <td class="style1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                        <td class="style1">&nbsp;</td>
                       <td class="style1">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

