﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="DeviceGroupMapping.aspx.cs" Inherits="DeviceGroupMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="Frame" style="align-content: center; position: absolute; top: 150px; left: 550px; width: 500px; height: 150px;">
        <table align="center" style="border-style: solid; border-width: thin; padding: 5px" width="500px">
            <tr>
                <td colspan="2">
                    <%--<dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Bold="true" Font-Size="Medium" Text="Device Group Mapping">
                </dx:ASPxLabel>--%>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px; width: 20%">
                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Group" Theme="SoftOrange">
                    </dx:ASPxLabel>
                </td>
                <td style="padding: 5px; width: 20%">
                    <dx:ASPxComboBox ID="ddlGroup" runat="server" ValueType="System.String" Width="150px" Theme="SoftOrange" ValueField="GroupID" AutoPostBack="True" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                        <ValidationSettings>
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>

            <tr>
                <td style="padding: 5px; width: 20%" colspan="2" align="center">
                    <dx:ASPxGridView ID="grdDevice" runat="server" Width="100%" Theme="SoftOrange" KeyFieldName="SerialNumber" AutoGenerateColumns="False">
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                        <Columns>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="Serial Number" FieldName="SerialNumber" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Device Name" FieldName="DeviceName" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>

            </tr>
            <tr>
                <td style="padding: 5px; width: 20%" colspan="2" align="center">
                    <dx:ASPxButton ID="btnUpdate" runat="server" Text="Map Group" OnClick="btnUpdate_Click" Theme="SoftOrange"></dx:ASPxButton>
                </td>
            </tr>

        </table>
    </div>
</asp:Content>

