﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="DeviceList.aspx.cs" Inherits="DeviceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script type="text/javascript">
        function ShowLoginWindow() {
            pcLogin.Show();
        }
        function ShowCreateAccountWindow() {
            pcCreateAccount.Show();
            tbUsername.Focus();
        }
    </script>
    <div>
        <asp:ScriptManager runat="server" ID="scrpt1">
        </asp:ScriptManager>
        <table width="100%">
            <tr>
                <td>
                    <dx:ASPxTextBox ID="mTransIdTxt" runat="server" Width="170px" Visible="false">
                    </dx:ASPxTextBox>
                </td>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td style="width:100px;padding:5px; height: 51px;">
                                <dx:ASPxButton ID="ASPxBtnReboot" runat="server" Text="Reboot" 
                         Theme="SoftOrange" Width="100px" Wrap="True" OnClick="ASPxBtnReboot_Click">
                                    <Image IconID="actions_reset2_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td style="width:100px;padding:5px; height: 51px;">
                                <dx:ASPxButton ID="ASPxBtnDoor" runat="server" Text="Sync Time" 
                        Theme="SoftOrange" Width="100px" OnClick="ASPxBtnDoor_Click">
                                    <Image IconID="numberformats_time2_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td style="width:100px;padding:5px; height: 51px;">
                                <dx:ASPxButton ID="ASPxBtnClearLog" runat="server" Text="Download Logs" 
                    Theme="SoftOrange" Width="100px" AutoPostBack="False"  >
                                    <Image IconID="navigation_next_16x16">
                                    </Image>
                                    <ClientSideEvents Click="function(s, e) { ShowLoginWindow(); }" />
                                </dx:ASPxButton>
                            </td>
                            <td style="width:100px;padding:5px; height: 51px;">
                                <dx:ASPxButton ID="ASPxBtnClearData" runat="server" Text="Clear Data" 
                    Theme="SoftOrange" Width="100px" OnClick="ASPxBtnClearData_Click">
                                    <Image IconID="actions_trash_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td style="width:100px;padding:5px; height: 51px;">
                                <dx:ASPxButton ID="ASPxBtnSummary" runat="server" Text="Get Summary" 
                    Theme="SoftOrange" Width="120px" OnClick="ASPxBtnSummary_Click">
                                    <Image IconID="businessobjects_boreport2_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                            <td style="width:100px;padding:5px; height: 51px;">
                                <dx:ASPxButton ID="ASPxSync" runat="server" Visible="true" Text="Get User Datails" 
                    Theme="SoftOrange" Width="100px" OnClick="ASPxSync_Click" >
                                    <Image IconID="actions_convert_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <%--<dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" runat="server" Theme="SoftOrange" Width="100%" OnCallback="ASPxCallbackPanel1_Callback" OnInit="ASPxCallbackPanel1_Init">
                        <PanelCollection>
                            <dx:PanelContent runat="server">--%>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT [TimeZoneID], [TimeZoneValue] FROM [TimeZoneMaster]"></asp:SqlDataSource>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        EnableTheming="True" KeyFieldName="device_id" 
        Theme="SoftOrange" Width="100%" Font-Size="Small" OnCustomColumnDisplayText="ASPxGridView1_CustomColumnDisplayText" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared" OnRowUpdating="ASPxGridView1_RowUpdating" OnRowDeleting="ASPxGridView1_RowDeleting" OnCellEditorInitialize="ASPxGridView1_CellEditorInitialize" OnRowValidating="ASPxGridView1_RowValidating" OnCustomCallback="ASPxGridView1_CustomCallback" OnCommandButtonInitialize="ASPxGridView1_CommandButtonInitialize" >
                        <Settings HorizontalScrollBarMode="Visible" 
            VerticalScrollableHeight="400" />
                        <SettingsResizing ColumnResizeMode="Control" />
                        <SettingsPager PageSize="50">
                        </SettingsPager>
                        <SettingsEditing Mode="PopupEditForm">
                        </SettingsEditing>
                        <Settings HorizontalScrollBarMode="Visible" 
                                                        VerticalScrollableHeight="400" VerticalScrollBarMode="Auto" 
                                                        VerticalScrollBarStyle="VirtualSmooth" ShowFilterRow="True" />
                        <SettingsBehavior ConfirmDelete="True" AllowFocusedRow="True" />
                        <SettingsResizing ColumnResizeMode="Control" />
                        <SettingsCommandButton>
                            <NewButton ButtonType="Image" RenderMode="Image">
                                <Image IconID="actions_add_16x16">
                                </Image>
                            </NewButton>
                            <UpdateButton ButtonType="Image" RenderMode="Image">
                                <Image IconID="save_save_16x16office2013">
                                </Image>
                            </UpdateButton>
                            <CancelButton ButtonType="Image" RenderMode="Image">
                                <Image IconID="actions_cancel_16x16office2013">
                                </Image>
                            </CancelButton>
                            <EditButton ButtonType="Image" RenderMode="Image" >
                                <Image IconID="actions_edit_16x16devav">
                                </Image>
                            </EditButton>
                            <DeleteButton ButtonType="Image" RenderMode="Image">
                                <Image IconID="actions_trash_16x16">
                                </Image>
                            </DeleteButton>
                        </SettingsCommandButton>
                        <SettingsDataSecurity AllowInsert="False" />
                        <SettingsPopup>
                            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                        </SettingsPopup>
                        <SettingsText CommandUpdate="Save" PopupEditFormCaption="Device Management" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowInCustomizationForm="True" VisibleIndex="0" Width="70px" Caption=" " 
                SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" ShowEditButton="True" ShowClearFilterButton="True">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="Cloud Id" FieldName="device_id" 
                ShowInCustomizationForm="True" VisibleIndex="1" Width="200px" ReadOnly="True">
                                <CellStyle ForeColor="Blue">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Device Name" FieldName="device_name" 
                ShowInCustomizationForm="True" Width="200px" VisibleIndex="2">
                                <PropertiesTextEdit>
                                    <ValidationSettings SetFocusOnError="True">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Status" FieldName="connected" 
                ShowInCustomizationForm="True" VisibleIndex="3">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataCheckColumn FieldName="IsAuthorized"  Caption="Is Authorized"    VisibleIndex="14">
                                <PropertiesCheckEdit DisplayTextChecked="Y" DisplayTextGrayed="N" DisplayTextUnchecked="N" DisplayTextUndefined="N" ValueChecked="Y" ValueGrayed="N" ValueType="System.String" ValueUnchecked="N">
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewDataTextColumn Caption="Device IP" FieldName="IPAddrss"
                ShowInCustomizationForm="True"  VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Last Connected" Width="200px" FieldName="last_update_time" 
                ShowInCustomizationForm="True" VisibleIndex="5">
                                <Settings AllowEllipsisInText="True" />
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="User" FieldName="user_count" 
                ShowInCustomizationForm="True" VisibleIndex="6" UnboundType="String" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Finger" ShowInCustomizationForm="True" 
                VisibleIndex="7" FieldName="fp_count" UnboundType="String" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Face" 
                FieldName="face_count" ShowInCustomizationForm="True" VisibleIndex="8" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Password" FieldName="password_count" 
                ShowInCustomizationForm="True" VisibleIndex="9" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Card" FieldName="idcard_count" 
                ShowInCustomizationForm="True" VisibleIndex="10" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Admin" FieldName="manager_count" 
                ShowInCustomizationForm="True" VisibleIndex="11" ReadOnly="True">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <%-- <dx:GridViewDataTextColumn Caption="Log" 
                ShowInCustomizationForm="True"  VisibleIndex="12">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="Pending Commands" 
                ShowInCustomizationForm="True"  VisibleIndex="13">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>--%>
                            <dx:GridViewDataTextColumn Caption="Device Mode" FieldName="DeviceMode" 
                ShowInCustomizationForm="True"  VisibleIndex="12" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Group" FieldName="GroupID"
                ShowInCustomizationForm="True"  VisibleIndex="13" >
                                <EditItemTemplate>
                                    <dx:ASPxGridLookup ID="ASPxGridLookup1" runat="server" OnInit="Lookup_Init"
                        AutoGenerateColumns="False" DataSourceID="SqlDataSourceGrp" EnableTheming="True" 
                        KeyFieldName="GroupID" MultiTextSeparator="," SelectionMode="Multiple" 
                        TextFormatString="{0}" Theme="SoftOrange" Width="230" >
                                        <GridViewProperties>
                                            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                        </GridViewProperties>
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="GroupID" ReadOnly="True" 
                                VisibleIndex="1">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GroupName" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridLookup>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataCheckColumn FieldName="IsMasterDevice"  Caption="Is Master"    VisibleIndex="15">
                                <PropertiesCheckEdit DisplayTextChecked="Y" DisplayTextGrayed="N" DisplayTextUnchecked="N" DisplayTextUndefined="N" ValueChecked="Y" ValueGrayed="N" ValueType="System.String" ValueUnchecked="N">
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewDataComboBoxColumn Caption="TimeZone" FieldName="TimeZone" ShowInCustomizationForm="True" VisibleIndex="16">
                                <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="TimeZoneValue" ValueField="TimeZoneID">
                                </PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>

                          <%--   <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyCode" ShowInCustomizationForm="True" VisibleIndex="17">
                                <PropertiesComboBox DataSourceID="SqlDataSourceCmp" TextField="COMPANYNAME" ValueField="COMPANYCODE" >
                                </PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>--%>

                            
                                    <dx:GridViewDataTextColumn Caption="Company" FieldName="CompanyCode"
                ShowInCustomizationForm="True"  VisibleIndex="14" >
                                <EditItemTemplate >
                                    <dx:ASPxGridLookup ID="GridLookupcomp" runat="server" OnInit="Lookup_InitComp"
                        AutoGenerateColumns="False" DataSourceID="SqlDataSourceCmp" EnableTheming="True" 
                        KeyFieldName="COMPANYCODE" MultiTextSeparator="," SelectionMode="Multiple" 
                        TextFormatString="{0}" Theme="SoftOrange" Width="280"  >
                                        <GridViewProperties>
                                            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                        </GridViewProperties>
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" SelectAllCheckboxMode="AllPages" VisibleIndex="0" Caption=" ">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="COMPANYCODE" Caption="Code" ReadOnly="True" 
                                VisibleIndex="1">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="COMPANYNAME" Caption= "Name" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridLookup>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataComboBoxColumn Caption="Purpose" FieldName="MachineType" ShowInCustomizationForm="True" VisibleIndex="6" Width="120px">
                 <PropertiesComboBox>
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="Attendance" Value="A" />
                                        <dx:ListEditItem Text="Verification " Value="V" />
                                        
                                    </Items>
                                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
                        </Columns>
                    </dx:ASPxGridView>
                          <%--  </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>--%>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <table>
                        <tr>
                            <td style="width: auto; padding: 5px">
                                <dx:ASPxComboBox ID="ASPxComboBox2" runat="server" AutoPostBack="True" Theme="SoftOrange" SelectedIndex="0" DropDownStyle="DropDownList">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="Excel" Value="0" ImageUrl="~/Image/Excel.png" />
                                        <dx:ListEditItem Text="PDF" Value="1" ImageUrl="~/Image/pdf.png" />
                                        <dx:ListEditItem Text="CSV" Value="2" ImageUrl="~/Image/csv.png" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td style="padding: 5px" class="auto-style1">
                                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Export"
                                Theme="SoftOrange" OnClick="ASPxButton2_Click" >
                                    <Image IconID="export_export_16x16">
                                    </Image>
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <br />
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"
                    FileName="DeviceList" GridViewID="ASPxGridView1">
                    </dx:ASPxGridViewExporter>
                    <br /></td>
            </tr>
        </table>
    </div>
    <asp:timer ID="TimerT" runat="server" Interval="10"  
                    Enabled="False" OnTick="Timer_Watch">
    </asp:timer>
    <asp:SqlDataSource ID="SqlDataSourceGrp" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT * FROM [BioDeviceGroup]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceCmp" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT * FROM [tblCompany]"></asp:SqlDataSource>
    <div>
        <dx:ASPxPopupControl ID="pcLogin" runat="server" Width="420" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Middle" ClientInstanceName="pcLogin" Theme="SoftOrange"
        HeaderText="Download Logs" AllowDragging="True" PopupAnimationType="Fade"  EnableViewState="False" AutoUpdatePosition="true" OnLoad="pcLogin_Load">
            <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup'); tbLogin.Focus(); }" />
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK">
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                                <dx:ASPxFormLayout runat="server" ID="ASPxFormLayout2" Width="100%" Height="100%">
                                    <Items>
                                        <dx:LayoutItem Caption="From Date">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <%-- <asp:UpdatePanel runat="server" ID="upd">
                                                        <ContentTemplate>--%>
                                                    <dx:ASPxDateEdit ID="FromDate" runat="server" Theme="SoftOrange" Date="2020-01-01">
                                                    </dx:ASPxDateEdit>
                                                    <dx:ASPxLabel runat="server" ID="lblMsg" Visible="false" Theme="SoftOrange" ForeColor="Red" Font-Size="Small">
                                                    </dx:ASPxLabel>
                                                    <%--  </ContentTemplate>
                                                    </asp:UpdatePanel>--%>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem Caption="To Date:">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit ID="ToDate" runat="server" Theme="SoftOrange" Date="01/06/2020 14:29:23">
                                                    </dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem ShowCaption="False" Paddings-PaddingTop="19">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxButton ID="btOK" runat="server" CausesValidation="true" ValidationGroup="entryGroup" Text="OK" Width="80px" AutoPostBack="False" Style="float: left; margin-right: 8px" Theme="SoftOrange" OnClick="btOK_Click">
                                                        <ClientSideEvents Click="function(s, e) { if(ASPxClientEdit.ValidateGroup('entryGroup')) pcLogin.Hide(); }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btCancel" runat="server" Text="Cancel" Width="80px" AutoPostBack="False" Style="float: left; margin-right: 8px" Theme="SoftOrange" OnClick="btCancel_Click">
                                                        <ClientSideEvents Click="function(s, e) { pcLogin.Hide(); }" />
                                                    </dx:ASPxButton>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                            <Paddings PaddingTop="19px">
                                            </Paddings>
                                        </dx:LayoutItem>
                                    </Items>
                                </dx:ASPxFormLayout>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </div>
</asp:Content>

