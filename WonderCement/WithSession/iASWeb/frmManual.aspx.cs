﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using GlobalSettings;
using DevExpress.Web;
using System.Data.SqlClient;

public partial class frmManual : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class1 cCount = new Class1();
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;
    string ReportView = "";
    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;
    string sSql=string.Empty;
    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;
    DateTime PunchDate;
    ErrorClass ec = new ErrorClass();
    string msg;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            TxtFromDate.Text =System.DateTime.Today.ToString("dd/MM/yyyy");  //"01/" + System.DateTime.Now.Month.ToString("00") + "/" + System.DateTime.Now.Year.ToString("0000");
           
            if (Request.QueryString["ID"] != null)
            {
                string Grade = Request.QueryString["ID"].ToString();

            }
            bindPaycode();

        }
    }
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FillGrid();
        grdAtt.DataBind();
        grdPunch.DataBind();
        
    }
    protected void bindPaycode()
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where Active='Y' ";
        if (Session["usertype"].ToString() == "A")
        {
            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + DataFilter.AuthDept.Trim() + ") ";
            }
            Strsql += "  Order by EmpName ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql += " and headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and Departmentcode in (" + DataFilter.AuthDept.Trim() + ") ";
                }
            }
            Strsql += "  Order by EmpName ";
        }
        else
            return;


        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPayCode.DataSource = ds;
            ddlPayCode.TextField = "EmpName1";
            ddlPayCode.ValueField = "paycode";
            ddlPayCode.DataBind();
            ddlPayCode.SelectedIndex = -1;
        }
        else
        {
            ddlPayCode.Items.Add("NONE");
            ddlPayCode.SelectedIndex = 0;


        }
    }
    protected void ddlPayCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if(ddlPayCode.SelectedItem.Text.ToString().Trim().ToUpper()=="NONE")
            {
                lblName.Text = "";
                lblLocation.Text = "";
                lblRTC.Text = "";
                grdAtt.DataSource = null;
                grdAtt.DataBind();
                grdPunch.DataSource = null;
                grdPunch.DataBind();
                return;

            }
            
            
            sSql =" SELECT EMPNAME,tblDepartment.DEPARTMENTNAME,case when tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK='Y' then 'YES' " +
                  " when tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK='N' then 'NO' else 'NA' end 'ISRTC' FROM TBLEMPLOYEE inner join tblDepartment on  " +
                  " tblDepartment.DEPARTMENTCODE=TBLEMPLOYEE.DepartmentCode inner join tblEmployeeShiftMaster on tblEmployeeShiftMaster.SSN=TBLEMPLOYEE.SSN "+
                  " and TBLEMPLOYEE.paycode='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and TBLEMPLOYEE.CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
            DataSet dsEmp = new DataSet();
            dsEmp = Con.FillDataSet(sSql);
            if (dsEmp.Tables[0].Rows.Count>0)
            {
                lblName.Text = dsEmp.Tables[0].Rows[0]["Empname"].ToString().Trim();
                lblLocation.Text = dsEmp.Tables[0].Rows[0]["DEPARTMENTNAME"].ToString().Trim();
                lblRTC.Text = dsEmp.Tables[0].Rows[0]["ISRTC"].ToString().Trim();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please select Any Record');", true);
                ddlPayCode.Focus();
                return;
            }
            FillGrid();
        }
        catch (Exception Ex)
        {
            Error_Occured("PayCode Change", Ex.Message);
        }
    }
    private void FillGrid()
    {
        //DateTime dateFrom;
        try
        {
           
            try
            {
                PunchDate = DateTime.ParseExact(TxtFromDate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {

                //Msg = "<script language='javascript'> alert('Enter A Valid Punch Date')</script>";
                //Page.RegisterStartupScript("msg", Msg);
                //TxtFromDate.Focus();
                //return;
            }
            //int i = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(PunchDate.Year.ToString("0000")));
            //if (i < 0)
            //{
            //    msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
            //    Page.RegisterStartupScript("msg", msg);
            //    TxtFromDate.Focus();
            //    return;
            //}
            Strsql = "select officepunch 'PDate' ,SubString(Convert(Char(17),officepunch,13),13,5) 'punch' , Convert(Char(12), officepunch, 103) 'OfficePunch',P_Day,IsManual from MachineRawPunch where paycode='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' And convert(char(10), OfficePunch,120) >='" + PunchDate.ToString("yyyy-MM-dd") + "' order by officepunch";
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdPunch.DataSource = ds.Tables[0];
                grdPunch.DataBind();
            }
            else
            {
                grdPunch.DataSource = null;
                grdPunch.DataBind();

            }
            //if (lblRTC.Text.ToString().Trim() == "NO")
            //{


            //    Strsql = " Select Convert(Char(12), DateOffice, 103) DateOffice, Status, ShiftAttended, LateArrival,case when hoursworked >0 " +
            //            " then CONVERT(varchar(5), DATEADD(minute, DATEDIFF(minute, in1, out2), 0), 114)  else '0' end as 'Hours',  Convert(Char(12), In1, 103) 'In Date', " +
            //            " Case Len(RTrim(LTrim(DateName(Hour, In1)))) When 1 Then '0' + RTrim(LTrim(DateName(Hour, In1))) Else RTrim(LTrim(DateName(Hour, In1))) End + ':' +  " +
            //            " Case Len(RTrim(LTrim(DateName(Minute, In1)))) When 1 Then '0' + RTrim(LTrim(DateName(Minute, In1))) Else RTrim(LTrim(DateName(Minute, In1))) End 'In Time'," +
            //            " Convert(Char(12), Out2, 103) 'Out Date',  Case Len(RTrim(LTrim(DateName(Hour, Out2)))) When 1 Then '0' + RTrim(LTrim(DateName(Hour, Out2))) Else " +
            //            " RTrim(LTrim(DateName(Hour, Out2))) End + ':' +   Case Len(RTrim(LTrim(DateName(Minute, Out2)))) When 1 Then '0' + RTrim(LTrim(DateName(Minute, Out2))) " +
            //            " Else RTrim(LTrim(DateName(Minute, Out2))) End 'Out Time',  Convert(Char(12), Out1, 103) 'Lunch Out Date',  Case Len(RTrim(LTrim(DateName(Hour, Out1)))) When  " +
            //            " 1 Then '0' + RTrim(LTrim(DateName(Hour, Out1))) Else RTrim(LTrim(DateName(Hour, Out1))) End + ':' +    Case Len(RTrim(LTrim(DateName(Minute, Out1)))) " +
            //            " When 1 Then '0' + RTrim(LTrim(DateName(Minute, Out1))) Else RTrim(LTrim(DateName(Minute, Out1))) End 'Lunch Out Time',  Convert(Char(12), In2, 103) " +
            //            " 'Lunch In Date',   Case Len(RTrim(LTrim(DateName(Hour, In2)))) When 1 Then '0' + RTrim(LTrim(DateName(Hour, In2))) Else RTrim(LTrim(DateName(Hour, In2))) " +
            //            " End  + ':' +   Case Len(RTrim(LTrim(DateName(Minute, In2)))) When 1 Then '0' + RTrim(LTrim(DateName(Minute, In2))) Else RTrim(LTrim(DateName(Minute, In2)))" +
            //            " End 'Lunch In Time ' " +
            //            "  from tblTimeRegister Where Paycode = '" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and convert(char(10), DateOffice,120) ='" + PunchDate.ToString("yyyy-MM-dd") + "' order by DateOffice";
            //}
            //else
            //{


                Strsql = " Select Convert(Char(12), DateOffice, 103) DateOffice, Status, ShiftAttended, LateArrival,case when hoursworked >0 " +
                        " then CONVERT(varchar(5), DATEADD(minute, DATEDIFF(minute, in1, out2), 0), 114)  else '0' end as 'Hours',  Convert(Char(12), In1, 103) 'In Date', " +
                        " Case Len(RTrim(LTrim(DateName(Hour, In1)))) When 1 Then '0' + RTrim(LTrim(DateName(Hour, In1))) Else RTrim(LTrim(DateName(Hour, In1))) End + ':' +  " +
                        " Case Len(RTrim(LTrim(DateName(Minute, In1)))) When 1 Then '0' + RTrim(LTrim(DateName(Minute, In1))) Else RTrim(LTrim(DateName(Minute, In1))) End 'In Time'," +
                        " Convert(Char(12), Out2, 103) 'Out Date',  Case Len(RTrim(LTrim(DateName(Hour, Out2)))) When 1 Then '0' + RTrim(LTrim(DateName(Hour, Out2))) Else " +
                        " RTrim(LTrim(DateName(Hour, Out2))) End + ':' +   Case Len(RTrim(LTrim(DateName(Minute, Out2)))) When 1 Then '0' + RTrim(LTrim(DateName(Minute, Out2))) " +
                        " Else RTrim(LTrim(DateName(Minute, Out2))) End 'Out Time',  Convert(Char(12), Out1, 103) 'Lunch Out Date',  Case Len(RTrim(LTrim(DateName(Hour, Out1)))) When  " +
                        " 1 Then '0' + RTrim(LTrim(DateName(Hour, Out1))) Else RTrim(LTrim(DateName(Hour, Out1))) End + ':' +    Case Len(RTrim(LTrim(DateName(Minute, Out1)))) " +
                        " When 1 Then '0' + RTrim(LTrim(DateName(Minute, Out1))) Else RTrim(LTrim(DateName(Minute, Out1))) End 'Lunch Out Time',  Convert(Char(12), In2, 103) " +
                        " 'Lunch In Date',   Case Len(RTrim(LTrim(DateName(Hour, In2)))) When 1 Then '0' + RTrim(LTrim(DateName(Hour, In2))) Else RTrim(LTrim(DateName(Hour, In2))) " +
                        " End  + ':' +   Case Len(RTrim(LTrim(DateName(Minute, In2)))) When 1 Then '0' + RTrim(LTrim(DateName(Minute, In2))) Else RTrim(LTrim(DateName(Minute, In2)))" +
                        " End 'Lunch In Time ' " +
                         "  from tblTimeRegister Where Paycode = '" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "'  and convert(char(10), DateOffice,120) between '" + PunchDate.AddDays(-1).ToString("yyyy-MM-dd") + "' and getdate()  order by CAST(dateoffice as date)";




            //}
            DataSet DsTTR = new DataSet();
            DsTTR = Con.FillDataSet(Strsql);
            if (DsTTR.Tables[0].Rows.Count > 0)
            {
                grdAtt.DataSource = DsTTR;
                grdAtt.DataBind();
            }
            else
            {
                grdAtt.DataSource = null;
                grdAtt.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("FillGrid", Ex.Message);
        }


    }
    protected void TxtFromDate_DateChanged(object sender, EventArgs e)
    {
        try
        {

            try
            {
                PunchDate = DateTime.ParseExact(TxtFromDate.Value.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                
            }
            catch
            {
                return;
            }
            
            string Tmp;
            string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "", SSN;
            Pcode = ddlPayCode.SelectedItem.Value.ToString().Trim();
            Strsql = "Select presentCardno,companycode,departmentcode,ssn from tblemployee where Active='Y' And paycode ='" + Pcode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            dr = Con.ExecuteReader(Strsql);
            if (dr.Read())
                CardNo = dr[0].ToString().Trim();
                CompanyCode = dr[1].ToString().Trim();
                DepartmentCode = dr[2].ToString().Trim();
                SSN = dr[3].ToString().Trim();
                dr.Close();

                if (CardNo.Trim() == "")
                    return;

            if (lblRTC.Text.ToUpper() == "YES")
            {
                try
                {
                    using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                    {

                        MyCmd = new OleDbCommand("ProcessAllRecords", MyConn);
                        MyCmd.CommandTimeout = 1800;
                        MyCmd.CommandType = CommandType.StoredProcedure;
                        MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PunchDate.AddDays(-1).ToString("yyyy-MM-dd");
                        MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                        MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                       // MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                        MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = Pcode;
                        MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = 2;
                        MyConn.Open();
                        result = MyCmd.ExecuteNonQuery();
                        MyConn.Close();

                    }
                }
                catch (Exception Ex)
                {
                    Error_Occured("datechange", Ex.Message);
                }
            }
            FillGrid();
        }
        catch (Exception Ex)
        {
            Error_Occured("datechange", Ex.Message);
        }
        
        
       
    }
    protected void btnPost_Click(object sender, EventArgs e)
    {
        try
        {
            string Tmp;
            string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "", SSN;
            lblName.Text = "";
            string[] tme = new string[2];
            int hour = 23;
            int min = 59;
            string SetupID = "";
            int DupMin = 5;
            DataSet Rs = new DataSet();
            string sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
            }

            if (ddlPayCode.SelectedItem.Text.Trim() == "-Select-" || ddlPayCode.SelectedItem.Text.Trim()=="")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Select PayCode');", true);
                ddlPayCode.Focus();
                return;
            }
            if (TxtFromDate.Text.ToString().Trim().Length < 10)
            {
                Msg = "<script language='javascript'> alert('Enter A Valid Punch Date...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                TxtFromDate.Focus();
                return;
            }
            try
            {
               // PunchDate = DateTime.ParseExact(TxtFromDate.Text.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                PunchDate = Convert.ToDateTime(TxtFromDate.Value.ToString().Trim());
                Tmp = PunchDate.ToString("yyyy-MM-dd") + " " + TxtPunchTime.Text.Trim();
            }
            catch
            {
                //Msg = "<script language='javascript'> alert('Invalid Punch Date...')</script>";
                //Page.RegisterStartupScript("msg", Msg);
                //TxtFromDate.Focus();
                return;
            }

            int i = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(PunchDate.Year.ToString("0000")));
            string msg = "";
            if (i < 0)
            {
                //msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
                //Page.RegisterStartupScript("msg", msg);
                //TxtFromDate.Focus();
                //return;
            }
            if (TxtPunchTime.Text.ToString().Trim() == "")
            {
                Msg = "<script language='javascript'> alert('Enter Punch Time...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                TxtPunchTime.Focus();
                return;
            }
            Pcode = ddlPayCode.SelectedItem.Value.ToString().Trim();

            //Strsql = "Select presentCardno,companycode,departmentcode,ssn from tblemployee where Active='Y' And paycode ='" + Pcode + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";

            Strsql = " Select presentCardno,tblemployee.companycode,departmentcode,tblemployee.ssn,tblEmployeeGroupPolicy.DUPLICATECHECKMIN from tblemployee " +
                     " inner join tblEmployeeGroupPolicy on tblEmployeeGroupPolicy.GroupID = tblemployee.GroupID " +
                     " where Active = 'Y' And tblemployee.paycode = '" + Pcode + "' and tblemployee.CompanyCode = '" + Session["LoginCompany"].ToString().Trim() + "'";
            dr = Con.ExecuteReader(Strsql);
            if (dr.Read())
            CardNo = dr[0].ToString().Trim();
            CompanyCode = dr[1].ToString().Trim();
            DepartmentCode = dr[2].ToString().Trim();
            SSN = dr[3].ToString().Trim();
            try
            {
                DupMin = Convert.ToInt32(dr[4].ToString().Trim());
            }
            catch
            {
                DupMin = 5;
            }


            dr.Close();
            if (CardNo.Trim() == "")
                return;

            //Checking for the entered punch is already available or not
            Strsql = "Select * from MachineRawPunch where paycode = '" + Pcode + "' And OfficePunch = '" + Tmp + "'  and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            dr = Con.Execute_Reader(Strsql);
            if (dr.Read())
            {
                dr.Close();
                Msg = "<script language='javascript'> alert('This Punch is already available...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                return;
            }
            dr.Close();

            Strsql = "Insert into MachineRawPunch  (CARDNO,PAYCODE,ISMANUAL,P_Day,OFFICEPUNCH,CompanyCode,SSN)  VALUES('" + CardNo + "','" + Pcode + "','Y','N','" + Tmp + "','" + CompanyCode + "','" + SSN + "') ";
            result = Con.execute_NonQuery(Strsql);

            if (result > 0)
            {
                Strsql = "Insert into tblLog_ManualPunch (Paycode,PunchDate,LoggedUser,LoggedDate)  VALUES('" + Pcode + "','" + Tmp + "','" + Session["PAYCODE"].ToString().Trim() + "',getdate() )";
                result = Con.execute_NonQuery(Strsql);

                try
                {
                    sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Manual Punch Posted For Code: " + Pcode + "','Punch Date: " + PunchDate.ToString("dd/MM/yyyy") + "','Punch Time: " + Tmp + "','" + Session["LoginCompany"].ToString().Trim() + "')";
                    Con.execute_NonQuery(sSql);

                }
                catch
                {

                }
                try
                {
                    RemoveDuplicatePunch(Pcode, Convert.ToDateTime(Tmp), DupMin);
                }
                catch
                {

                }
                
                if(lblRTC.Text.ToUpper()=="YES")
                {
                    try
                    {
                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                        {

                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                            MyCmd.CommandTimeout = 1800;
                            MyCmd.CommandType = CommandType.StoredProcedure;
                            MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PunchDate.AddDays(-1).ToString("yyyy-MM-dd");
                            MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                            MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                            MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                            MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = Pcode;
                            MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                            MyConn.Open();
                            result = MyCmd.ExecuteNonQuery();
                            MyConn.Close();

                        }
                    }
                    catch 
                    {
                        
                        
                    }
                }
                else
                {
                    try
                    {
                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                        {

                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                            MyCmd.CommandTimeout = 1800;
                            MyCmd.CommandType = CommandType.StoredProcedure;
                            MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PunchDate.ToString("yyyy-MM-dd");
                            MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = PunchDate.ToString("yyyy-MM-dd");
                            MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                            MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                            MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = Pcode;
                            MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                            MyConn.Open();
                            result = MyCmd.ExecuteNonQuery();
                            MyConn.Close();

                        }
                    }
                    catch     
                    {
                        
                        
                    }
                }
             

                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Manual Punch Posted Successfully');", true);

                result = 0;
                TxtPunchTime.Text = "";
            }



            FillGrid();
        }
        catch (Exception Ex)
        {
            Error_Occured("PostPunch", Ex.Message);
        }
    }
    protected void grdPunch_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string Tmp;
            string PCode;
            string CardNo = "", CompanyCode = "", DepartmentCode = "", SetupID = "";
            DataSet Rs = new DataSet();
            string sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
            }
            Tmp = e.Keys["PDate"].ToString().Trim();

            PunchDate = Convert.ToDateTime(Tmp);
            Strsql = "Delete from machinerawpunch where paycode ='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' And Officepunch= '" + PunchDate.ToString("yyyy-MM-dd HH:mm") + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            Con.execute_NonQuery(Strsql);

            try
            {
                sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Punch Deleted For Code: " + ddlPayCode.SelectedItem.Value.ToString().Trim() + "','Punch Date: " + PunchDate.ToString("dd/MM/yyyy") + "','Punch Time: " + Tmp + "','" + Session["LoginCompany"].ToString().Trim() + "')";
                Con.execute_NonQuery(sSql);

            }
            catch
            {

            }
            Strsql = "Select presentCardno,companycode,departmentcode from tblemployee where Active='Y' And paycode ='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            dr = Con.ExecuteReader(Strsql);
            if (dr.Read())
                CardNo = dr[0].ToString().Trim();
            CompanyCode = dr[1].ToString().Trim();
            DepartmentCode = dr[2].ToString().Trim();
            PCode = ddlPayCode.SelectedItem.Value.ToString().Trim();
            dr.Close();
            if (lblRTC.Text.ToUpper() == "YES")
            {
                using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                {

                    MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                    MyCmd.CommandTimeout = 1800;
                    MyCmd.CommandType = CommandType.StoredProcedure;
                    MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PunchDate.AddDays(-1).ToString("yyyy-MM-dd");
                    MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                    MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                    MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                    MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = PCode;
                    MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                    MyConn.Open();
                    result = MyCmd.ExecuteNonQuery();
                    MyConn.Close();

                }
            }
            else
            {
                using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                {

                    MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                    MyCmd.CommandTimeout = 1800;
                    MyCmd.CommandType = CommandType.StoredProcedure;
                    MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = DateTime.ParseExact(TxtFromDate.Text.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = DateTime.ParseExact(TxtFromDate.Text.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                    MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                    MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = PCode;
                    MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                    MyConn.Open();
                    result = MyCmd.ExecuteNonQuery();
                    MyConn.Close();

                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Manual Punch Deleted Successfully');", true);
            grdPunch.CancelEdit();
            e.Cancel = true;
            FillGrid();
        }
        catch (Exception Ex)
        {
            Error_Occured("Delete Punch", Ex.Message);
            grdPunch.CancelEdit();
            e.Cancel = true;
            FillGrid();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void grdAtt_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        grdAtt.PageIndex = pageIndex;
        this.FillGrid();
    }
    protected void grdPunch_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        grdPunch.PageIndex = pageIndex;
        this.FillGrid();
    }
    protected void TxtFromDate_CalendarDayCellPrepared(object sender, CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }


    private void RemoveDuplicatePunch(string PayCode, DateTime DupDate, int DuplicateMin)
    {
        try
        {
           
            DateTime DupDt = DupDate;
            DataSet Rs = new DataSet();
            string mEmpcd = PayCode;
            DateTime mDate = DateTime.MinValue;
            int S_Dupli = 10;
            S_Dupli = Convert.ToInt32(DuplicateMin);
            DupDt = Convert.ToDateTime(DupDate);
            sSql = "Select * from Machinerawpunch where officepunch between '" + DupDt.ToString("yyyy-MM-dd") + " 00:00:00' and '" + DupDt.ToString("yyyy-MM-dd") + " 23:59:59' and paycode='" + mEmpcd.ToString() + "' Order By Cardno,officepunch";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                for (int id = 0; id < Rs.Tables[0].Rows.Count; id++)
                {
                    mEmpcd = Rs.Tables[0].Rows[id]["Cardno"].ToString().Trim();
                    mDate = Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]);
                    id = id + 1;
                    if (id == Rs.Tables[0].Rows.Count)
                    {
                        break;
                    }
                    while (mEmpcd == Rs.Tables[0].Rows[id]["Cardno"].ToString().Trim())
                    {
                        if (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, mDate, Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"])) <= S_Dupli)
                        {
                            
                            sSql = "Delete MachineRawPunch Where Cardno='" + mEmpcd + "'And OfficePunch = '" + Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]).ToString("yyyy-MM-dd HH:mm") + "'";
                            Con.execute_NonQuery(sSql);
                            
                        }
                        else
                        {
                            mDate = Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]);
                        }
                        id = id + 1;
                        if (id == Rs.Tables[0].Rows.Count)
                        {
                            break;
                        }
                    }
                }
            }
            else
            {

            }
            return;
        }
        catch (Exception Ex)
        {

            
        }
    }
    internal static class SimulateIsDate
    {
        internal static bool IsDate(object expression)
        {
            if (expression == null)
                return false;

            System.DateTime testDate;
            return System.DateTime.TryParse(expression.ToString(), out testDate);
        }
    }
    internal static class SimulateVal
    {

        internal static double Val(string expression)
        {
            if (expression == null)
                return 0;

            //try the entire string, then progressively smaller
            //substrings to simulate the behavior of VB's 'Val',
            //which ignores trailing characters after a recognizable value:
            for (int size = expression.Length; size > 0; size--)
            {
                double testDouble;
                if (double.TryParse(expression.Substring(0, size), out testDouble))
                    return testDouble;
            }

            //no value is recognized, so return 0:
            return 0;
        }
        internal static double Val(object expression)
        {
            if (expression == null)
                return 0;

            double testDouble;
            if (double.TryParse(expression.ToString(), out testDouble))
                return testDouble;

            //VB's 'Val' function returns -1 for 'true':
            bool testBool;
            if (bool.TryParse(expression.ToString(), out testBool))
                return testBool ? -1 : 0;

            //VB's 'Val' function returns the day of the month for dates:
            System.DateTime testDate;
            if (System.DateTime.TryParse(expression.ToString(), out testDate))
                return testDate.Day;

            //no value is recognized, so return 0:
            return 0;
        }
        internal static int Val(char expression)
        {
            int testInt;
            if (int.TryParse(expression.ToString(), out testInt))
                return testInt;
            else
                return 0;
        }
    }
    //----------------------------------------------------------------------------------------
    //	Copyright © 2009 - 2010 Tangible Software Solutions Inc.
    //	This class can be used by anyone provided that the copyright notice remains intact.
    //
    //	This class simulates the behavior of the classic VB 'DateDiff' function.
    //----------------------------------------------------------------------------------------
    internal static class SimulateDateDiff
    {
        internal enum DateInterval
        {
            Day,
            DayOfYear,
            Hour,
            Minute,
            Month,
            Quarter,
            Second,
            Weekday,
            WeekOfYear,
            Year
        }


        internal static long DateDiff(DateInterval intervalType, System.DateTime dateOne, System.DateTime dateTwo)
        {
            switch (intervalType)
            {
                case DateInterval.Day:
                case DateInterval.DayOfYear:
                    System.TimeSpan spanForDays = dateTwo - dateOne;
                    return (long)spanForDays.TotalDays;
                case DateInterval.Hour:
                    System.TimeSpan spanForHours = dateTwo - dateOne;
                    return (long)spanForHours.TotalHours;
                case DateInterval.Minute:
                    System.TimeSpan spanForMinutes = dateTwo - dateOne;
                    return (long)spanForMinutes.TotalMinutes;
                case DateInterval.Month:
                    return ((dateTwo.Year - dateOne.Year) * 12) + (dateTwo.Month - dateOne.Month);
                case DateInterval.Quarter:
                    long dateOneQuarter = (long)System.Math.Ceiling(dateOne.Month / 3.0);
                    long dateTwoQuarter = (long)System.Math.Ceiling(dateTwo.Month / 3.0);
                    return (4 * (dateTwo.Year - dateOne.Year)) + dateTwoQuarter - dateOneQuarter;
                case DateInterval.Second:
                    System.TimeSpan spanForSeconds = dateTwo - dateOne;
                    return (long)spanForSeconds.TotalSeconds;
                case DateInterval.Weekday:
                    System.TimeSpan spanForWeekdays = dateTwo - dateOne;
                    return (long)(spanForWeekdays.TotalDays / 7.0);
                case DateInterval.WeekOfYear:
                    System.DateTime dateOneModified = dateOne;
                    System.DateTime dateTwoModified = dateTwo;
                    while (dateTwoModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    {
                        dateTwoModified = dateTwoModified.AddDays(-1);
                    }
                    while (dateOneModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    {
                        dateOneModified = dateOneModified.AddDays(-1);
                    }
                    System.TimeSpan spanForWeekOfYear = dateTwoModified - dateOneModified;
                    return (long)(spanForWeekOfYear.TotalDays / 7.0);
                case DateInterval.Year:
                    return dateTwo.Year - dateOne.Year;
                default:
                    return 0;
            }
        }
    }
}