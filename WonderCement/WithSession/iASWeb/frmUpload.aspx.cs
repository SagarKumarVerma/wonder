﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;
using BioCloud;
using System.Data.Odbc;

public partial class frmUpload : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;
    ErrorClass ec = new ErrorClass();
    SqlConnection msqlConn;
    string fileName = "";
    string  UName, PhotoPath, Face;
   
    
    DataSet Ds;
    string StrSql, Qry, change, Tmp;
    Class1 cls = new Class1();

    OdbcCommand ocmd;
    OdbcDataAdapter oDa;


    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;


    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    string SSN = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;

    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;

    string btnGoBack = "";
    string resMsg = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatus.Text = "";
        if (!IsPostBack)
        {
            lblStatus.Text = "";
            msqlConn = FKWebTools.GetDBPool();


        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    
    public static string[] ExcelSheetNames_First(String excelFile)
    {
        DataTable dt;
        string[] res = new string[0];
        //string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=HTML Import;";
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";
        using (OleDbConnection objConn = new OleDbConnection(connString))
        {
            objConn.Open();
            dt =
            objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dt == null)
            {
                return null;
            }
            res = new string[dt.Rows.Count];
            for (int i = 0; i < res.Length; i++)
            {
                string name = dt.Rows[i]["TABLE_NAME"].ToString();
                if (name[0] == '\'')
                {
                    //numeric sheetnames get single quotes around
                    //remove them here
                    if (Regex.IsMatch(name, @"^'\d\w+\$'$"))
                    {
                        name = name.Substring(1, name.Length - 2);
                    }
                }
                res[i] = name;
            }
            return res;
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Text = "";
            string str_FileLocation = "";
            string xConnStr = "";
            str_FileLocation = Server.MapPath("UploadTemp");
            str_FileLocation = str_FileLocation + "\\" + fileName;
            int date = 0;
            date = Convert.ToInt32(System.DateTime.Now.Day);

           
            DataSet dsResult = new DataSet();
            string Cardno, istimelossallowed, isot, Permissablelate, permisearly, isautoshift, isoutwork, maxdaymin, isos, time, shortday, half, ishalfday, isshort;

            int l = 0;
            DataSet dscon = new DataSet();
            DataSet dsDept = new DataSet();
            Cardno = "";

            string AuthShifts = "";
            string Shift = "";
            string EmployeeGroup = "", EmployeeGroup1 = "";
            string SecondOffDay = "", weekdays = "";
            string ssql = "", Active = "", ComCode = "", Deptcode = "", CatID = "", Email = "", DivCode = "", Grade = "", GradeCode = "", LAStages = "", ShiftType = "", ISRoundClock = "", Inonly = "", IsPunchAll = "", Two = "", FirstOffDay = "", SecondOffType = "", UserType = "";
            string SSN = "";
            DateTime dob = System.DateTime.MinValue, doj = System.DateTime.MinValue;
            DataSet dsRecord = new DataSet();
            string Married = "";
            string Qualification = "";
            string Exp = "";

            string card, paycode, Name, FNAme, Company, Branch, Section, Department, catagory, Designation1, Sex, headid1, Email1;


            string BranchID = "";
            DataSet dspaycode = new DataSet();
            string[] d = new string[3];
            string[] dj = new string[3];
            string[] a = new string[3];


            Permissablelate = "";
            permisearly = "";
            maxdaymin = "";
            time = "";
            half = "";
            shortday = "";

            StrSql = "select PermisLateArr,PermisEarlyDep,PREWO,Time1,MaxWrkDuration,Time,Half,Short from tblsetup " +
                        "where setupid in (select max(convert(int,setupid)) from tblsetup)";
            ds = cn.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Permissablelate = ds.Tables[0].Rows[0]["PermisLateArr"].ToString().Trim();
                permisearly = ds.Tables[0].Rows[0]["PermisEarlyDep"].ToString().Trim();
                maxdaymin = ds.Tables[0].Rows[0]["MaxWrkDuration"].ToString().Trim();
                time = ds.Tables[0].Rows[0]["Time"].ToString().Trim();
                half = ds.Tables[0].Rows[0]["Half"].ToString().Trim();
                shortday = ds.Tables[0].Rows[0]["Short"].ToString().Trim();
            }

            Strsql = "select top 1 case len(ltrim(rtrim(shift))) when 1 then shift+'  ' when 2 then shift+' ' else shift end shift from tblshiftmaster WHERE CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            ds = cn.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Shift = ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                Shift = "GEN";
            }

            Strsql = "select top 1 GroupID from tblEmployeeGroupPolicy WHERE CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
            DataSet dsG = cn.FillDataSet(Strsql);
            if (dsG.Tables[0].Rows.Count > 0)
            {
                EmployeeGroup = dsG.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                change = "<script language='JavaScript'>alert('Employee Group Not Created')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;

            }
           
            if (File.Exists(str_FileLocation))
            {
                File.Delete(str_FileLocation);
            }
            ASPxUploadControl1.SaveAs(str_FileLocation);
            string[] f = ExcelSheetNames_First(str_FileLocation);

            if (fileName.ToString().Trim() == "")
            {
                lblStatus.Text = "Please Select File";
                return;
            }
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(f[0].ToString().Trim()))
                    {

                        lblStatus.Text = "Excel File Is Not In Valid Format...";
                        return;

                    }

                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str_FileLocation + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text""";

                    using (OleDbConnection connection = new OleDbConnection(xConnStr))
                    {
                        OleDbCommand command = new OleDbCommand("Select * FROM [" + f[0] + "] ", connection);
                        connection.Open();
                        using (OleDbDataReader dr1 = command.ExecuteReader())
                        {
                            while (dr1.Read())
                            {
                                Section = dr1[0].ToString().Trim();
                                if (Convert.ToInt32(Section.ToString().Length) > 45)
                                {
                                    Section = Section.ToString().Substring(0, 45);
                                }
                                Company = dr1[1].ToString().Trim();
                                if (Convert.ToInt32(Company.ToString().Length) > 50)
                                {
                                    Company = Company.Substring(0, 50);
                                }
                                paycode = dr1[2].ToString().Trim();
                                Name = dr1[3].ToString().Trim();
                                if (Name.Length > 25)
                                {
                                    Name = Name.Substring(0, 25);
                                }
                                Email = dr1[4].ToString().Trim();
                                headid1 = dr1[5].ToString().Trim();
                                if (headid1.ToString().Trim() != "")
                                {
                                    if (Convert.ToInt32(headid1.ToString().Length) > 10)
                                    {
                                        headid1 = headid1.Substring(0, 10);
                                    }
                                }
                                card = dr1[6].ToString().Trim();
                                try
                                {
                                    Cardno = string.Format("{0:00000000}", Convert.ToInt32(card));
                                }
                                catch
                                {
                                    continue;
                                }
                                catagory = dr1[7].ToString().Trim();
                                if (Convert.ToInt32(catagory.ToString().Length) > 45)
                                {
                                    catagory = catagory.ToString().Substring(0, 35);
                                }

                                Grade = dr1[8].ToString().Trim();
                                if (Convert.ToInt32(Grade.ToString().Length) > 45)
                                {
                                    Grade = Grade.ToString().Substring(0, 45);
                                }

                                Department = dr1[9].ToString().Trim();
                                if (Convert.ToInt32(Department.ToString().Length) > 45)
                                {
                                    Department = Department.ToString().Substring(0, 45);
                                }

                                if (!dr1.IsDBNull(10))
                                {
                                    dob = Convert.ToDateTime(dr1[10]);
                                }

                                if (!dr1.IsDBNull(13))
                                {
                                    doj = Convert.ToDateTime(dr1[13]);
                                }

                                Designation1 = dr1[11].ToString().Trim();
                                if (Designation1.Length > 25)
                                {
                                    Designation1 = Designation1.ToString().Substring(0, 25);
                                }
                                FNAme = dr1[12].ToString().Trim();
                                if (FNAme.Length > 30)
                                {
                                    FNAme = FNAme.ToString().Substring(0, 30);
                                }


                                Sex = dr1[14].ToString().Trim();
                                if (Sex.ToString().Length > 1)
                                {
                                    Sex = Sex.ToString().Substring(0, 1);
                                }
                                try
                                {
                                    EmployeeGroup1 = dr1[15].ToString().Trim();
                                    if (EmployeeGroup1.Trim() == string.Empty)
                                    {
                                        EmployeeGroup1 = EmployeeGroup;
                                    }
                                }
                                catch 
                                {
                                    
                                   EmployeeGroup1 = EmployeeGroup;
                                }
                               

                                Branch = "None";
                                Married = "N";
                                Qualification = "";
                                Exp = "";
                                if (Convert.ToInt32(Qualification.ToString().Length) > 20)
                                {
                                    Qualification = Qualification.Substring(0, 20);
                                }

                                Active = "Y";
                               
                                ssql = "select Companycode from tblcompany where Companycode='" + Company.ToString().Trim() + "'";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    
                                    ComCode = string.Format("{0:000}", dsResult.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    
                                    change = "<script language='JavaScript'>alert('Company Code Not Valid')</script>";
                                    Page.RegisterStartupScript("frmchange", change);
                                    return;

                                }

                                ssql = "select departmentcode from tbldepartment where departmentname='" + Department.ToString().Trim() + "'  and CompanyCode='" + Company.ToString().Trim() + "'";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    //Deptcode = dsResult.Tables[0].Rows[0][0].ToString().Trim();
                                    Deptcode = string.Format("{0:000}", dsResult.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    ssql = "select isnull(max(cast(substring(departmentcode,1,len(departmentcode))as int)),0)+1 'Code' from tbldepartment where DepartmentCode NOT LIKE '%[^0-9]%' and CompanyCode='" + Company.Trim() + "'";
                                    dsDept = cn.FillDataSet(ssql);
                                    if (dsDept.Tables[0].Rows.Count > 0)
                                    {
                                        Deptcode = dsDept.Tables[0].Rows[0][0].ToString().Trim();
                                        Deptcode = string.Format("{0:000}", dsDept.Tables[0].Rows[0][0]);
                                        ssql = "insert into tbldepartment(Departmentcode,DEPARTMENTNAME,CompanyCode)values('" + Deptcode.ToString() + "','" + Department.ToString().Trim() + "','" + Company.Trim() + "') ";
                                        cn.execute_NonQuery(ssql);
                                    }
                                }

                                ssql = "select cat from tblcatagory where catagoryname='" + catagory.ToString().Trim() + "' and CompanyCode='" + Company.ToString().Trim() + "' ";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    //CatID = dsResult.Tables[0].Rows[0][0].ToString().Trim();
                                    CatID = string.Format("{0:000}", dsResult.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    ssql = "select isnull(max(cast(substring(cat,1,len(cat))as int)),0)+1 'Code' from tblcatagory  where CAT NOT LIKE '%[^0-9]%' and CompanyCode='" + Company.Trim() + "' ";
                                    dsDept = cn.FillDataSet(ssql);
                                    if (dsDept.Tables[0].Rows.Count > 0)
                                    {
                                        CatID = dsDept.Tables[0].Rows[0][0].ToString().Trim();
                                        CatID = string.Format("{0:000}", dsDept.Tables[0].Rows[0][0]);
                                        ssql = "insert into tblcatagory(cat,catagoryNAME,CompanyCode)values('" + CatID.ToString() + "','" + catagory.ToString().Trim() + "','" + Company.Trim() + "') ";
                                        cn.execute_NonQuery(ssql);
                                    }
                                }

                                ssql = "select branchCode from tblbranch where branchname='" + Branch.ToString().Trim() + "' and CompanyCode='" + Company.ToString().Trim() + "'";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    //BranchID = dsResult.Tables[0].Rows[0][0].ToString().Trim();
                                    BranchID = string.Format("{0:000}", dsResult.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    ssql = "select isnull(max(cast(substring(branchcode,2,len(branchcode))as int)),0)+1 'Code' from tblbranch  where branchcode NOT LIKE '%[^0-9]%' and CompanyCode='" + Company.Trim() + "'";
                                    dsDept = cn.FillDataSet(ssql);
                                    if (dsDept.Tables[0].Rows.Count > 0)
                                    {
                                        BranchID = dsDept.Tables[0].Rows[0][0].ToString().Trim();
                                        BranchID = string.Format("{0:000}", dsDept.Tables[0].Rows[0][0]);
                                        ssql = "insert into tblbranch(branchcode,branchNAME,CompanyCode)values('" + BranchID.ToString() + "','" + Branch.ToString().Trim() + "','" + Company.Trim() + "') ";
                                        cn.execute_NonQuery(ssql);
                                    }
                                }


                                ssql = "select DivisionCode from tbldivision where divisionname='" + Section.ToString().Trim() + "' and CompanyCode='" + Company.ToString().Trim() + "'";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    //DivCode = dsResult.Tables[0].Rows[0][0].ToString().Trim();
                                    DivCode = string.Format("{0:000}", dsResult.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    ssql = "select isnull(max(cast(substring(divisioncode,1,len(divisioncode))as int)),0)+1 'Code' from tblDivision where divisioncode NOT LIKE '%[^0-9]%' and CompanyCode='" + Company.Trim() + "'";
                                    dsDept = cn.FillDataSet(ssql);
                                    if (dsDept.Tables[0].Rows.Count > 0)
                                    {
                                        DivCode = dsDept.Tables[0].Rows[0][0].ToString().Trim();
                                        DivCode = string.Format("{0:000}", dsDept.Tables[0].Rows[0][0]);
                                        ssql = "insert into tblDivision(divisioncode,divisionNAME,CompanyCode)values('" + DivCode.ToString() + "','" + Section.ToString().Trim() + "','" + Company.Trim() + "') ";
                                        cn.execute_NonQuery(ssql);
                                    }
                                }
                                ssql = "select GradeCode from tblGrade where Gradename='" + Grade.ToString().Trim() + "' and CompanyCode='" + Company.ToString().Trim() + "'";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    //GradeCode = dsResult.Tables[0].Rows[0][0].ToString().Trim();
                                    GradeCode = string.Format("{0:000}", dsResult.Tables[0].Rows[0][0]);
                                }
                                else
                                {
                                    ssql = "select isnull(max(cast(substring(Gradecode,1,len(Gradecode))as int)),0)+1 'Code' from tblGrade where Gradecode NOT LIKE '%[^0-9]%' and CompanyCode='" + Company.Trim() + "'";
                                    dsDept = cn.FillDataSet(ssql);
                                    if (dsDept.Tables[0].Rows.Count > 0)
                                    {
                                        GradeCode = dsDept.Tables[0].Rows[0][0].ToString().Trim();
                                        GradeCode = string.Format("{0:000}", dsDept.Tables[0].Rows[0][0]);
                                        ssql = "insert into tblGrade(Gradecode,GradeNAME,CompanyCode)values('" + GradeCode.ToString() + "','" + Grade.ToString().Trim() + "','" + Company.Trim() + "') ";
                                        cn.execute_NonQuery(ssql);
                                    }
                                }

                                LAStages = "1";
                                ShiftType = "F";
                                ISRoundClock = "N";
                                Inonly = "N";
                                IsPunchAll = "Y";
                                Two = "N";
                                FirstOffDay = "SUN";
                                UserType = "U";
                                istimelossallowed = "Y";
                                isot = "N";
                                isautoshift = "Y";
                                isoutwork = "Y";
                                isos = "Y";
                                ishalfday = "Y";
                                isshort = "N";
                                HALFDAYSHIFT = " ";
                                SecondOffDay = "SAT";
                                weekdays = "12345";
                                SecondOffType = "F";
                                AuthShifts = "";
                                string FourPunch_Night = "N";
                                string OTOutMinusShiftEndTime = "N";
                                string OTWrkgHrsMinusShiftHrs = "Y";
                                string OTEarlyComePlusLateDep = "N";
                                string OTRound = "N";
                                string OT_EARLYCOMING = "N";
                                string PresentOn_HLDPresent = "N";
                                string PresentOn_WOPresent = "N";
                                string AbsentOn_WOAbsent = "N";
                                string awa = "N";
                                string AW = "N";
                                string WA = "N";

                                string Wo_Absent = "N";
                                string EndTime_In = "05:00";
                                string EndTime_RTCEmp = "05:00";

                                int AUTOSHIFT_LOW = 240;
                                int AUTOSHIFT_UP = 240;

                                int DEDUCTHOLIDAYOT = 0;
                                int DEDUCTWOOT = 0;
                                string ISOTEARLYCOMING = "N";
                                string OTMinus = "N";
                                int OTEARLYDUR = 0;
                                int OTLATECOMINGDUR = 0;
                                int OTRESTRICTENDDUR = 0;
                                int DUPLICATECHECKMIN = 0;
                                int PREWO = 0;
                                string EnableAutoResign = "N";
                                try
                                {
                                    ssql = "select * from tblEmployeeGroupPolicy WHERE CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and GroupID='" + EmployeeGroup1.Trim() + "' ";
                                    DataSet DsEmpGrp = new DataSet();
                                    DsEmpGrp = cn.FillDataSet(ssql);
                                    if (DsEmpGrp.Tables[0].Rows.Count>0)
                                    {
                                        Shift = DsEmpGrp.Tables[0].Rows[0]["SHIFT"].ToString().Trim();
                                        ShiftType = DsEmpGrp.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim();
                                        ISRoundClock = DsEmpGrp.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim();
                                        Inonly = DsEmpGrp.Tables[0].Rows[0]["INONLY"].ToString().Trim();
                                        IsPunchAll = DsEmpGrp.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
                                        Two = DsEmpGrp.Tables[0].Rows[0]["TWO"].ToString().Trim();
                                        FirstOffDay = DsEmpGrp.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString().Trim();
                                        UserType = "U";
                                        istimelossallowed = DsEmpGrp.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
                                        isot = DsEmpGrp.Tables[0].Rows[0]["ISOT"].ToString().Trim();
                                        isautoshift = DsEmpGrp.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();
                                        isoutwork = DsEmpGrp.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();
                                        isos = DsEmpGrp.Tables[0].Rows[0]["ISOS"].ToString().Trim();
                                        ishalfday = DsEmpGrp.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
                                        isshort = DsEmpGrp.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
                                        HALFDAYSHIFT = DsEmpGrp.Tables[0].Rows[0]["HALFDAYSHIFT"].ToString().Trim();
                                        SecondOffDay = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFDAY"].ToString().Trim();
                                        weekdays = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();
                                        SecondOffType = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString().Trim();
                                        AuthShifts = DsEmpGrp.Tables[0].Rows[0]["AUTH_SHIFTS"].ToString().Trim();
                                        FourPunch_Night = DsEmpGrp.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
                                        OTOutMinusShiftEndTime = DsEmpGrp.Tables[0].Rows[0]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
                                        OTWrkgHrsMinusShiftHrs = DsEmpGrp.Tables[0].Rows[0]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
                                        OTEarlyComePlusLateDep = "N";
                                        OTRound = DsEmpGrp.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
                                        OT_EARLYCOMING = "N";
                                        PresentOn_HLDPresent = "N";
                                        PresentOn_WOPresent = "N";
                                        AbsentOn_WOAbsent = "N";
                                        awa = "N";
                                        AW = "N";
                                        WA = "N";
                                        Wo_Absent = "N";
                                        EndTime_In = "05:00";
                                        EndTime_RTCEmp = "05:00";
                                        AUTOSHIFT_LOW = 240;
                                        AUTOSHIFT_UP = 240;
                                        DEDUCTHOLIDAYOT = 0;
                                        DEDUCTWOOT = 0;
                                        ISOTEARLYCOMING = "N";
                                        OTMinus = "N";
                                        OTEARLYDUR = 0;
                                        OTLATECOMINGDUR = 0;
                                        OTRESTRICTENDDUR = 0;
                                        DUPLICATECHECKMIN = 0;
                                        PREWO = 0;
                                        EnableAutoResign = "N";



                                    }
                                }
                                catch
                                {

                                }



                                SSN = ComCode.ToString().Trim() + "_" + paycode.ToString().Trim();
                                ssql = "select * from tblemployee where paycode='" + paycode.ToString().Trim() + "' and CompanyCode='"+ComCode.Trim()+"'";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    goto UpdateRecord;
                                   // continue;
                                }
                                ssql = "select * from tblemployee where presentcardno='" + Cardno.ToString().Trim() + "' and SSN='" + SSN.ToString().Trim() + "' ";
                                dsResult = cn.FillDataSet(ssql);
                                if (dsResult.Tables[0].Rows.Count > 0)
                                {
                                    goto UpdateRecord;
                                   // continue;
                                }



                                ssql = "insert into tblemployee(Active,Paycode,presentcardno,empname,companycode,departmentcode,designation,cat, " +
                                    "E_Mail1,divisioncode,gradecode,leaveapprovalstages,headid,Sex,branchcode,GUARDIANNAME,ISMARRIED,QUALIFICATION,EXPERIENCE,SSN,DateOfJoin,DateofBirth,GroupID) values ('" + Active.ToString() + "','" + paycode.ToString() + "','" + Cardno.ToString() + "', " +
                                    " '" + Name.ToString() + "','" + ComCode.ToString() + "','" + Deptcode.ToString() + "','" + Designation1.ToString() + "'," +
                                    " '" + CatID.ToString() + "','" + Email.ToString() + "','" + DivCode.ToString() + "'," +
                                    " '" + GradeCode.ToString() + "','" + LAStages.ToString() + "','" + headid1.ToString() + "','" + Sex.ToString() + "','" + BranchID.ToString().Trim() + "','" + FNAme.ToString() + "','" + Married.ToString() + "','" + Qualification.ToString() + "','" + Exp.ToString() + "','" + SSN + "','" + doj.ToString("yyyy-MM-dd") + "','" + dob.ToString("yyyy-MM-dd") + "','" + EmployeeGroup1.ToString().Trim() + "') ";
                                //Response.Write(ssql.ToString());
                                cn.execute_NonQuery(ssql);

                                ssql = "insert into tblemployeeshiftmaster(Paycode,cardno,shift,shifttype,isroundtheclockwork,inonly,ispunchall,two,firstoffday,secondoffday, secondofftype,istimelossallowed , isot ,PERMISLATEARRIVAL, PERMISEARLYDEPRT, isautoshift, isoutwork, maxdaymin,isos,time,short,half,ishalfday,isshort,ALTERNATE_OFF_DAYS,shiftRemainDays,CDays,otrate,auth_shifts,HALFDAYSHIFT,SSN,MIS,IsCOF,HLFAfter,HLFBefore,ResignedAfter,EnableAutoResign,[S_END],[S_OUT] " +
                                       ",[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME]" +
                                       ",[ISOTWRKGHRSMINUSSHIFTHRS],[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],[DUPLICATECHECKMIN]" +
                                       ",[PREWO]) values ('" + paycode.ToString() + "','" + Cardno.ToString() + "', " +
                                       " '" + Shift.ToString() + "','" + ShiftType.ToString() + "','" + ISRoundClock.ToString() + "','" + Inonly.ToString() + "', " +
                                       " '" + IsPunchAll.ToString() + "','" + Two.ToString() + "','" + FirstOffDay.ToString() + "','" + SecondOffDay.ToString() + "'," +
                                       " '" + SecondOffType.ToString() + "','" + istimelossallowed.ToString() + "','" + isot.ToString() + "'," + Permissablelate.ToString() + "," + permisearly.ToString() + ", " +
                                       " '" + isautoshift.ToString() + "','" + isoutwork.ToString() + "'," + maxdaymin.ToString() + ",'" + isos.ToString() + "'," + time.ToString() + "," + shortday.ToString() + "," + half.ToString() + ", " +
                                       " '" + ishalfday.ToString() + "','" + isshort.ToString() + "','" + weekdays.ToString() + "',0,0,0,'" + AuthShifts.ToString() + "','" + HALFDAYSHIFT.ToString() + "','" + SSN + "','P','O','0','0','0','" + EnableAutoResign + "',";
                                ssql += " '" + EndTime_In.ToString().Trim() + "','" + EndTime_RTCEmp + "', '" + AUTOSHIFT_LOW + "','" + AUTOSHIFT_UP + "','" + PresentOn_WOPresent + "','" + PresentOn_HLDPresent + "','" + FourPunch_Night + "','" + AbsentOn_WOAbsent + "','" + awa + "',";
                                ssql += " '" + WA.ToString().Trim() + "','" + AW.ToString().Trim() + "','" + Wo_Absent.ToString().Trim() + "','" + OTOutMinusShiftEndTime.ToString().Trim() + "',";
                                ssql += " '" + OTWrkgHrsMinusShiftHrs.ToString().Trim() + "','" + OTEarlyComePlusLateDep.ToString().Trim() + "','" + DEDUCTHOLIDAYOT.ToString().Trim() + "','" + DEDUCTWOOT.ToString().Trim() + "',";
                                ssql += " '" + ISOTEARLYCOMING.ToString().Trim() + "','" + OTMinus.ToString().Trim() + "','" + OTRound.ToString().Trim() + "','" + OTEARLYDUR.ToString().Trim() + "','" + OTLATECOMINGDUR.ToString().Trim() + "',";
                                ssql += " '" + OTRESTRICTENDDUR.ToString().Trim() + "','" + DUPLICATECHECKMIN.ToString().Trim() + "','" + PREWO.ToString().Trim() + "')";

                                cn.execute_NonQuery(ssql);

                                ssql = "insert into tbluser(user_r,password,USERDESCRIPRION,paycode,USERTYPE,SSN,CompanyCode )" +
                                    " values ('" + paycode.ToString() + "','" + paycode.ToString() + "', " +
                                    " '" + Name.ToString() + "','" + paycode.ToString() + "','" + UserType.ToString() + "','" + SSN + "','" + Company + "')";
                                cn.execute_NonQuery(ssql);

                                ssql = "insert into tblleaveledger(Paycode,lyear,SSN)" +
                                    " values ('" + paycode.ToString() + "'," + System.DateTime.Now.Year + ",'" + SSN + "')";
                                cn.execute_NonQuery(ssql);

                                //                    Response.Write(paycode.ToString());
                                if (doj.Year < System.DateTime.Now.Year)
                                {
                                    DateTime nowDate = DateTime.Now;
                                    DateTime firstDayCurrentYear = new DateTime(nowDate.Year, 1, 1);
                                    doj = firstDayCurrentYear;
                                }

                                try
                                {


                                    using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                    {
                                        MyCmd = new OleDbCommand("ProcessDutyRosterForCreate", MyConn);
                                        MyCmd.CommandTimeout = 1800;
                                        MyCmd.CommandType = CommandType.StoredProcedure;
                                        MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = paycode.ToString().Trim();
                                        MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = doj.ToString("yyyy-MM-dd");
                                        MyCmd.Parameters.Add("@WO_Include", SqlDbType.VarChar).Value = 'N';
                                        MyCmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = SSN;
                                        MyConn.Open();
                                        result = MyCmd.ExecuteNonQuery();
                                        MyConn.Close();

                                    }


                                    Strsql = "";

                                }
                                catch
                                {
                                    Strsql = "";
                                    continue;
                                }

                            UpdateRecord:
                                try
                                {
                                    Strsql = "update tblEmployee set SSN='" + SSN.Trim() + "',EmpName='" + Name.Trim().ToUpper() + "',  " +
                                            "GUARDIANNAME='" + FNAme.Trim().ToUpper() + "',DateOFJOIN='" + doj.ToString("yyyy-MM-dd") + "',PRESENTCARDNO='" + Cardno.ToString().Trim() + "', " +
                                            "COMPANYCODE='" + ComCode.Trim() + "',DEPARTMENTCODE='" + Deptcode.Trim() + "',CAT='" + CatID.Trim() + "', " +
                                            "SEX='" + Sex.ToString().Trim() + "',ISMARRIED='" + Married.Trim() + "' " +
                                            ",DESIGNATION='" + Designation1.ToString().Trim() + "',E_MAIL1='" + Email.Trim() + "',  DivisionCode='" + DivCode.Trim() + "', " +
                                            " GradeCode='" + GradeCode.Trim() + "',headid='" + headid1.Trim() + "' " +
                                            " Where paycode='" + paycode+ "' and  COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' ";

                                             result = cn.execute_NonQuery(Strsql);
                                }
                                catch
                                {
                                    continue;
                                }
                                
                               
                            }
                            dr1.Close();
                        }
                    }
                    lblStatus.Text = "Data Uploaded  Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Upload Successfully');document.location='Home.aspx'", true);
                }
                catch (Exception Ex)
                {
                    lblStatus.Text = Ex.Message;
                    Error_Occured("Upload Function", Ex.Message);
                }
            }



        }
        catch (Exception Ex)
        {
            lblStatus.Text = Ex.Message;
            Error_Occured("ASPxButton1_Click", Ex.Message);
        }
    }

    

   
    public byte[] StreamToByteArray(Stream input)
    {
        byte[] total_stream = new byte[0];
        byte[] stream_array = new byte[0];
        byte[] buffer = new byte[1024];

        int read = 0;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            stream_array = new byte[total_stream.Length + read];
            total_stream.CopyTo(stream_array, 0);
            Array.Copy(buffer, 0, stream_array, total_stream.Length, read);
            total_stream = stream_array;
        }

        return total_stream;
    }
    protected void ASPxUploadControl1_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
    {
        System.Threading.Thread.Sleep(2000);

        ASPxUploadControl uploadControl = sender as ASPxUploadControl;

        if (uploadControl.UploadedFiles != null && uploadControl.UploadedFiles.Length > 0)
        {
            for (int i = 0; i < uploadControl.UploadedFiles.Length; i++)
            {
                UploadedFile file = uploadControl.UploadedFiles[i];
                if (file.FileName != "")
                {
                    fileName = file.FileName.ToString().Trim();//string.Format("{0}{1}", MapPath("~/UploadTemp/"), file.FileName);

                }
            }
        }

    }


}