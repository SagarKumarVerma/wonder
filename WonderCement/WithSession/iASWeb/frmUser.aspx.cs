﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
public partial class frmUser : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    int result = 0;
    Class_Connection cn = new Class_Connection();
    string Temp;
    string strSql;
    int m_iRowIdx;
    public string KeyValue = "";// 
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    string Value = null;
    OleDbDataReader dr;
    DataSet Ds;
    DataSet ds;
    string str = null;
    string selection = "";
    ErrorClass ec = new ErrorClass();

    string com = "";
    DateTime dt;
    DataSet ds1;
    string find = "";
    string Strsql = null;
    string resMsg = "";
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<a href='Default.aspx' class='link'> Back to Login Page </a> <br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdUser.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {
            BindData();
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                strSql = "select u.user_r 'UserID' ,u.userdescriprion 'Discription' , paycode,SSN,u.UserType,CompanyCode,LoginType,auth_comp,Auth_dept,Main,DataProcess,Leave,Reports from tbluser u  Order by u.user_r";
            }
            else
            {
                strSql = "select u.user_r 'UserID' ,u.userdescriprion 'Discription' , paycode,SSN,u.UserType,CompanyCode,LoginType,auth_comp,Auth_dept,Main,DataProcess,Leave,Reports from tbluser u  where u.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' Order by u.user_r";
            }

            ds = new DataSet();
            ds = cn.FillDataSet(strSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdUser.DataSource = ds.Tables[0];
                GrdUser.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("BindData", Ex.Message);
        }
    }

}