﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="UserList.aspx.cs" Inherits="UserList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <table style="width: 100%">
        <tr>
            <td>
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared">
                    <Settings ShowFilterRow="True" />
                    <SettingsPager>
                        <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" AllItemText="All" ShowAllItem="true" />
                    </SettingsPager>
                      <SettingsSearchPanel Visible="True" />
                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                    <SettingsText Title="Real Time Logs" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Face" 
                ShowInCustomizationForm="True" VisibleIndex="5">
                <Settings AllowEllipsisInText="True" />
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Finger" 
                ShowInCustomizationForm="True" VisibleIndex="6">
                <Settings AllowEllipsisInText="True" />
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Card" 
                ShowInCustomizationForm="True" VisibleIndex="7">
                <Settings AllowEllipsisInText="True" />
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Pin" 
                ShowInCustomizationForm="True" VisibleIndex="8">
                <Settings AllowEllipsisInText="True" />
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            
                        <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="4" FieldName="UserName">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Enroll Id" VisibleIndex="3" FieldName="UserID">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Type" VisibleIndex="2" FieldName="DeviceType" >
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Devie ID" VisibleIndex="0" FieldName="DeviceID">
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Devie Name" VisibleIndex="1" FieldName="DevName">
                        </dx:GridViewDataTextColumn>
                       
                    </Columns>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>
</asp:Content>

