﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GroupEdit.aspx.cs" Inherits="GroupEdit" %>

<%@ Register assembly="DevExpress.Web.Bootstrap.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.Bootstrap" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style type="text/css">
        .auto-styleGen {
            width: 40%;
           align-items:center
        }
        /*.auto-style2 {
            height: 18px;
        }
        .auto-style_Tr_Height {
            height: 28px;
        }
         .auto-style_Td1_Width {
            width: 40%;
        }
          .auto-style_Td2_Width {
            width: 50%;
        }*/
          .auto-style3 {
              width: 40%;
              align-items: center;
              height: 22px;
          }
          .auto-style4 {
              height: 22px;
          }
          .auto-style5 {
              width: 100%;
          }
          .auto-style6 {
              height: 18px;
          }
          .auto-style7 {
              height: 17px;
          }
          .auto-style8 {
              height: 35px;
          }
          .auto-style9 {
              height: 30px;
          }
          .auto-style10 {
              height: 23px;
          }
        </style>
    <script type="text/javascript">  
        function LoadGrpScript() {
            //alert(window.parent.document.getElementById('MainPane_Content_MainContent_HdnRowId').value);
            if (window.parent.document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value == "") {
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').disabled = false;
                RadSinglePunch.SetEnabled(false);
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').focus();
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').value = "";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisLateArrival_I').value = "00:10";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisEalryDep').value = "00:00";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkInDay_I').value = "23:59";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsRTC_S_D').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsTimeLoss_S_D').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsHalfDay_S_D').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsSortDay_S_D').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPresentMarkDur_I').value = "04:00";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkForHalf_I').value = "06:00";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkForSort_I').value = "00:00";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelPReq_RadPunch_ValueInput').value = "Four Punches";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_IsOT_S_D').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_ASPxTextOTRate_I').value = "00:00";
               // document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_IsOS_S_D').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ddlShiftType_I').value = "F";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlFirstWeeklyOff_I').value = "SUN";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOff_I').value = "NON";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOff_I').value = "NON";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOff_I').value = "NON";
                
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextDupliateMin').value = "5";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsForurPunchInNight_S_D').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextBoxEndTimeForIn').value = "05:00";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextBoxEndTimeForOut_I').value = "05:00";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextEarlyMinAutoShift_I').value = "60";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextLateMinAutoShift_I').value = "60";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsPreOnWOP_S_D').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsPreOnHLDP_S_D').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsAWAA').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsAW').value = "5";
               // document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextDupliateMin').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsWA').value = "N";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel8_ASPxTextNoOfDayForWO_I').value = "3";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsAutoABS').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsWOInDutyRoster').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsAbsOnNoDay').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsMISAsA').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsMISAsH').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsOWMinus').value = "N";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsCOffAlloed').value = "N";


                document.getElementById('ASPxPageControl1_ASPxRoundPanelSing_RadSinglePunch_RB0_I_D').value = "O";
                document.getElementById('ASPxPageControl1_ASPxRoundPanelSing_RadSinglePunch_RB1_I_D').value="";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ddlShiftPat_B-1').value = "";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ASPxTextShiftRemainDay_I').value = "0";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ASPxTextShiftChangeDay_I').value = "0";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOffType_I').value = "";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlHalfDayShift_I').value = "";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_0').value = "1";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_1').value= "2";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_2').value = "3";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_3').value = "4";
                //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_4').value = "5";
                document.getElementById('ASPxPageControl1_ASPxRoundPanel8_ASPxTextNoOfDayForWO').value = "3";
              //  document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsAutoABS_S_D').value = "N";
                IsRTC.SetValue("N");
                IsTimeLoss.SetValue("N");
                IsHalfDay.SetValue("N");
                IsSortDay.SetValue("N");
                IsOT.SetValue("N");
                IsOS.SetValue("N");
                IsRunAutoShift.SetValue("N");
                IsForurPunchInNight.SetValue("N");
                IsPreOnWOP.SetValue("N");
                IsPreOnHLDP.SetValue("N");
                IsAWAA.SetValue("N");
                IsAW.SetValue("N");
                IsWA.SetValue("N");
                IsAutoABS.SetValue("N");
                IsWOInDutyRoster.SetValue("N");
                IsAbsOnNoDay.SetValue("N");
                IsMISAsA.SetValue("N");
                IsMISAsH.SetValue("N");
                IsOWMinus.SetValue("N");
                IsCOffAlloed.SetValue("N");
                ChkOffDay1.SetValue("N");
                ChkOffDay2.SetValue("N");
                ChkOffDay3.SetValue("N");
                ChkOffDay4.SetValue("N");
                ChkOffDay5.SetValue("N");
                
                
            }
            else {
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').disabled = true;
                document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisLateArrival_I').focus();
                PageMethods.LoadGrp(window.parent.document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value, GrpDataFunc);
            }
        }
        function GrpDataFunc(GrpData) {
            var temp = JSON.parse(GrpData);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').value = temp.GroupId;
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisLateArrival_I').value = MinutestoHourFormat(temp.PERMISLATEARRIVAL);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisEalryDep').value = MinutestoHourFormat(temp.PERMISEARLYDEPRT);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkInDay_I').value = MinutestoHourFormat(temp.MAXDAYMIN);
            //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsRTC_S_D').value= temp.ISROUNDTHECLOCKWORK;
            IsRTC.SetValue(temp.ISROUNDTHECLOCKWORK);
            //document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsTimeLoss_S_D').value = temp.ISTIMELOSSALLOWED;
            IsTimeLoss.SetValue(temp.ISTIMELOSSALLOWED);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsHalfDay_S_D').value = temp.ISHALFDAY;
            IsHalfDay.SetValue(temp.ISHALFDAY);
            //alert(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsHalfDay_S_D').value);
            // document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsSortDay_S_D').value = temp.ISSHORT;
            IsSortDay.SetValue(temp.ISSHORT);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPresentMarkDur_I').value = MinutestoHourFormat(temp.TIME);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkForHalf_I').value = MinutestoHourFormat(temp.HALF);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkForSort_I').value = MinutestoHourFormat(temp.SHORT);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelPReq_RadPunch_ValueInput').value = temp.PunchRequiredInDay;
            // document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_IsOT_S_D').value = temp.ISOT;
            IsOT.SetValue(temp.ISOT);
            document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_ASPxTextOTRate_I').value = temp.OTRATE;
            // document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_IsOS_S_D').value = temp.ISOS;
            IsOS.SetValue(temp.IsOS);
            document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ddlShiftType_I').value = temp.SHIFTTYPE;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlFirstWeeklyOff_I').value = temp.FIRSTOFFDAY;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOff_I').value = temp.SECONDOFFDAY;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextDupliateMin').value = temp.DUPLICATECHECKMIN;
          //  document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsForurPunchInNight_S_D').value = temp.NightShiftFourPunch;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextBoxEndTimeForIn').value = temp.S_END.substring(11, 16);
            document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextBoxEndTimeForOut_I').value = temp.S_OUT.substring(11, 16);
            document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextEarlyMinAutoShift_I').value = temp.AUTOSHIFT_LOW;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextLateMinAutoShift_I').value = temp.AUTOSHIFT_UP;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsPreOnWOP_S_D').value = temp.ISPRESENTONWOPRESENT;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsPreOnHLDP_S_D').value = temp.ISPRESENTONHLDPRESENT;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsAWAA').value = temp.ISAWA;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsAW').value = temp.ISAW;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel7_IsWA').value = temp.ISWA;
            IsRunAutoShift.SetValue(temp.IsRunAutoShift);
            IsForurPunchInNight.SetValue(temp.NightShiftFourPunch);
            IsPreOnWOP.SetValue(temp.ISPRESENTONWOPRESENT);
            IsPreOnHLDP.SetValue(temp.ISPRESENTONHLDPRESENT);
            IsAWAA.SetValue(temp.ISAWA);
            IsAW.SetValue(temp.ISAW);
            IsWA.SetValue(temp.ISWA);
            IsAutoABS.SetValue(temp.ISAUTOABSENT);
            IsWOInDutyRoster.SetValue(temp.WOINCLUDE);
            IsAbsOnNoDay.SetValue(temp.IsAbsOnNoDay);
            IsMISAsA.SetValue(temp.MIS);
            IsMISAsH.SetValue(temp.IsMISAsH);
            IsOWMinus.SetValue(temp.OwMinus);
            IsCOffAlloed.SetValue(temp.ISCOMPOFF);




            document.getElementById('ASPxPageControl1_ASPxRoundPanel8_ASPxTextNoOfDayForWO_I').value = temp.PREWO;
          //  document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsAutoABS').value = temp.ISAUTOABSENT;
           // document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsWOInDutyRoster').value = temp.WOINCLUDE;
          //  document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsAbsOnNoDay').value = "N"; //temp.ISROUNDTHECLOCKWORK;
          //  document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsMISAsA').value = temp.MIS;
           // document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsMISAsH').value = "N";//temp.ISROUNDTHECLOCKWORK;
          //  document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsOWMinus').value = temp.OwMinus;
         //   document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsCOffAlloed').value = temp.ISCOMPOFF;

            document.getElementById('ASPxPageControl1_ASPxRoundPanelSing_RadSinglePunch_RB0_I_D').value = "O";
            document.getElementById('ASPxPageControl1_ASPxRoundPanelSing_RadSinglePunch_RB1_I_D').value = "";
            document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ddlShiftPat_B-1').value = "";
            document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ASPxTextShiftRemainDay_I').value = temp.SHIFTPATTERN;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ASPxTextShiftChangeDay_I').value = temp.SHIFTREMAINDAYS;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOffType_I').value = temp.SECONDOFFTYPE;
            document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlHalfDayShift_I').value = temp.HALFDAYSHIFT;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_0').value = "1";
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_1').value = "2";
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_2').value = "3";
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_3').value = "4";
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel6_chkSecondOffDays_4').value = "5";
            document.getElementById('ASPxPageControl1_ASPxRoundPanel8_ASPxTextNoOfDayForWO').value = "3";
            // document.getElementById('ASPxPageControl1_ASPxRoundPanel8_IsAutoABS_S_D').value = temp.ISAUTOABSENT;
            //alert(temp.ALTERNATE_OFF_DAYS);
            ChkOffDay1.SetValue("N");
            ChkOffDay2.SetValue("N");
            ChkOffDay3.SetValue("N");
            ChkOffDay4.SetValue("N");
            ChkOffDay5.SetValue("N");
            if (temp.ALTERNATE_OFF_DAYS != "")
            {
                var altday = temp.ALTERNATE_OFF_DAYS;
                //const altday = Object.assign([], temp.ALTERNATE_OFF_DAYS)
                //alert(temp.ALTERNATE_OFF_DAYS);
                //alert(altday + " " + altday.length);
                for (var i = 0; i < altday.length; i++)
                {
                    //alert(altday[i]);
                    if (altday[i] == '1')
                        ChkOffDay1.SetValue("Y");
                    //else
                        //ChkOffDay1.SetValue("N");

                    if (altday[i] == '2')
                        ChkOffDay2.SetValue("Y");
                    //else
                        //ChkOffDay2.SetValue("N");

                    if (altday[i] == '3')
                        ChkOffDay3.SetValue("Y");
                    //else
                        //ChkOffDay3.SetValue("N");

                    if (altday[i] == '4')
                        ChkOffDay4.SetValue("Y");
                    //else
                        //ChkOffDay4.SetValue("N");

                    if (altday[i] == '5')
                        ChkOffDay5.SetValue("Y");
                    //else
                        //ChkOffDay5.SetValue("N");
                }
            }
   }
        function OnSaveClick() {
            //alert(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsTimeLoss_S_D').value);
            window.parent.document.getElementById('MainPane_Content_MainContent_HiddenFieldGrpId').value = "";
            var GroupId=document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').value ;
            var PERMISLATEARRIVAL = HourFormatToMinutes(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisLateArrival_I').value);
            var PERMISEARLYDEPRT = HourFormatToMinutes(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPermisEalryDep').value);
            var MAXDAYMIN = HourFormatToMinutes(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkInDay_I').value);
            var ISROUNDTHECLOCKWORK = IsRTC.GetValue();
            var ISTIMELOSSALLOWED = IsTimeLoss.GetValue();
            var ISHALFDAY = document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsHalfDay_S_D').value;
            var ISHALFDAY =IsHalfDay.GetValue();
            var ISSHORT = IsSortDay.GetValue();
            var TIME = HourFormatToMinutes(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextPresentMarkDur_I').value);
            var HALF=HourFormatToMinutes(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkForHalf_I').value) ;
            var SHORT = HourFormatToMinutes(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextMaxWorkForSort_I').value);
            var PunchRequiredInDay = document.getElementById('ASPxPageControl1_ASPxRoundPanelPReq_RadPunch_ValueInput').value;
            var ISOT = IsOT.GetValue();
            var OTRATE = document.getElementById('ASPxPageControl1_ASPxRoundPanelGOT_ASPxTextOTRate_I').value;
            var ISOS = IsOS.GetValue();
            var SHIFTTYPE = document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ddlShiftType_I').value;
            var FIRSTOFFDAY = document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlFirstWeeklyOff_I').value;
            var SECONDOFFDAY = document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOff_I').value;
            var DUPLICATECHECKMIN = document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextDupliateMin').value;

            var S_END = "1900-01-01 " + document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextBoxEndTimeForIn').value + ":00";
            var S_OUT = "1900-01-01 " + document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextBoxEndTimeForOut_I').value + ":00";

            var AUTOSHIFT_LOW = document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextEarlyMinAutoShift_I').value;
            var AUTOSHIFT_UP = document.getElementById('ASPxPageControl1_ASPxRoundPanel7_ASPxTextLateMinAutoShift_I').value;
            var IsRunAutoShift = IsRunAutoShift.GetValue();
            var NightShiftFourPunch = IsForurPunchInNight.GetValue();
            var ISPRESENTONWOPRESENT = IsPreOnWOP.GetValue();
            var ISPRESENTONHLDPRESENT = IsPreOnHLDP.GetValue();
            var ISAWA = IsAWAA.GetValue();
            var ISAW = IsAW.GetValue();
            var ISWA = IsWA.GetValue();
            var ISAUTOABSENT = IsAutoABS.GetValue();
            var WOINCLUDE = IsWOInDutyRoster.GetValue();
            var IsAbsOnNoDay = IsAbsOnNoDay.GetValue();
            var MIS = IsMISAsA.GetValue();
            var IsMISAsH = IsMISAsH.GetValue();
            var OwMinus = IsOWMinus.GetValue();
            var ISCOMPOFF = IsCOffAlloed.GetValue();

            var PREWO = document.getElementById('ASPxPageControl1_ASPxRoundPanel8_ASPxTextNoOfDayForWO_I').value;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanelSing_RadSinglePunch_RB0_I_D').value = "O";
            //document.getElementById('ASPxPageControl1_ASPxRoundPanelSing_RadSinglePunch_RB1_I_D').value = "";
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ddlShiftPat_B-1').value = "";
            var SHIFTPATTERN = document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ASPxTextShiftRemainDay_I').value;
            var SHIFTREMAINDAYS = document.getElementById('ASPxPageControl1_ASPxRoundPanel5_ASPxTextShiftChangeDay_I').value;
            var SECONDOFFTYPE = document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlSecondWeeklyOffType_I').value;
            var HALFDAYSHIFT = document.getElementById('ASPxPageControl1_ASPxRoundPanel6_ddlHalfDayShift_I').value;
            //document.getElementById('ASPxPageControl1_ASPxRoundPanel8_ASPxTextNoOfDayForWO').value = "3";
            
            var ALTERNATE_OFF_DAYS = "";
            if (ChkOffDay1.GetValue() == "Y")
                ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS + "1";

             if (ChkOffDay2.GetValue() == "Y")
                ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS + "2";

             if (ChkOffDay3.GetValue() == "Y")
                ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS + "3";

             if (ChkOffDay4.GetValue() == "Y")
                ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS + "4";

             if (ChkOffDay5.GetValue() == "Y")
                ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS + "5";

            var IsUpdate = document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_ASPxTextGrpID_I').disabled;
            PageMethods.SaveGrp(IsUpdate,GroupId,PERMISLATEARRIVAL,PERMISEARLYDEPRT,MAXDAYMIN,ISROUNDTHECLOCKWORK,ISTIMELOSSALLOWE,ISHALFDAY,ISHALFDAY,ISSHORT,TIME,HALF,SHORT,PunchRequiredInDay,ISOT,OTRATE,IsOS,SHIFTTYPE,FIRSTOFFDAY,SECONDOFFDAY,DUPLICATECHECKMIN,S_END,S_OUT,AUTOSHIFT_LOW,AUTOSHIFT_UP,IsRunAutoShift,NightShiftFourPunch,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,ISAWA,ISAW,ISWA,ISAUTOABSENT,WOINCLUDE,IsAbsOnNoDay,MIS,IsMISAsH,OwMinus,ISCOMPOFF,PREWO,SHIFTPATTERN,SHIFTREMAINDAYS,SECONDOFFTYPE,HALFDAYSHIFT,ALTERNATE_OFF_DAYS, HandleReturn);
        }
        function HandleReturn(returnValue) {
             if (returnValue == "true")
             {
                 window.parent.pcLogin.Hide();
                 //MainPane_Content_MainContent_grdShift
                 window.parent.ASPxCallbackPanel2.PerformCallback();
                 //window.location = "frmShift.aspx";
             }
             else {
                 //document.getElementById("lblError.ClientID").text = returnValue;
                 lblError.SetText(returnValue);                
             }
         }
        function HourFormatToMinutes(HrForm) {
            var min;
            var list = HrForm.split(":");
            min = parseInt(list[0]) * 60 + parseInt(list[1]);
            return min;
        }
        function MinutestoHourFormat(Min) {
            var HFormat;
            var hours = Math.floor(Min / 60);
            var minutes = Min % 60;
            if (hours < 10)
                hours = "0" + hours;
            if (minutes < 10)
                minutes = "0" + minutes;
            HFormat = hours + ":" + minutes;
            return HFormat;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <table width="100%" height="100%">
<tr>

    <td colspan="3">

        
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" Width="100%" ActiveTabIndex="1">
            <TabPages>
                <dx:TabPage Text="General">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table class="dx-justification">
                                <tr>
                                    <td width="50%" valign="top">
                                        <dx:ASPxRoundPanel ID="ASPxRoundPanelGen" runat="server" ShowHeader="true"  Width="100%" Theme="SoftOrange" HeaderText="General" >
         
                                            <PanelCollection>
                                                <dx:PanelContent runat="server">
                                                    <table class="auto-style5">
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Group Id" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextGrpID" runat="server" MaxLength="3" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Group Name" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextGrpName" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Permissible Late Arrival" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextPermisLateArrival" runat="server" MaxLength="5" Width="170px">
                                                                    <MaskSettings Mask="HH:mm" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Permissible Early Departure" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextPermisEalryDep" runat="server" Width="170px">
                                                                    <MaskSettings Mask="HH:mm" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Maxmimum Working Hours In A Day" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextMaxWorkInDay" runat="server" Width="170px">
                                                                    <MaskSettings Mask="HH:mm" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Is Rounnd The Clock Work" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="IsRTC" runat="server" CheckState="Unchecked" Theme="SoftOrange"  ValueType="System.String" ValueChecked="Y" ValueUnchecked="N" ClientInstanceName="IsRTC">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style3">
                                                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Consider Time Loss" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td class="auto-style4">
                                                                <dx:ASPxCheckBox ID="IsTimeLoss" runat="server" CheckState="Unchecked" Theme="SoftOrange" ValueType="System.String" ValueChecked="Y" ValueUnchecked="N" ClientInstanceName="IsTimeLoss">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Half Day Marking" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="IsHalfDay" runat="server" CheckState="Unchecked" Theme="SoftOrange"  ValueType="System.String" ValueChecked="Y" ValueUnchecked="N" ClientInstanceName="IsHalfDay">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Short Leave Marking" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="IsSortDay" runat="server" CheckState="Unchecked"  ValueType="System.String" ValueChecked="Y" ValueUnchecked="N" ClientInstanceName="IsSortDay"   Theme="SoftOrange">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Present Marking Duratiion" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextPresentMarkDur" runat="server" Width="170px">
                                                                    <MaskSettings Mask="HH:mm" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Maximum Working For HalfDay" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextMaxWorkForHalf" runat="server" Width="170px">
                                                                    <MaskSettings Mask="HH:mm" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-styleGen">
                                                                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Maximum Working For Sort Day" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="ASPxTextMaxWorkForSort" runat="server" Width="170px">
                                                                    <MaskSettings Mask="HH:mm" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </td>
                                    <td width="50%"  valign="top">
                                        <table width="100%" class="auto-style5">
                                            <tr>
                                                <td>
                                                     <dx:ASPxRoundPanel ID="ASPxRoundPanelPReq" runat="server" ShowHeader="true" Width="100%" Height="20%" Theme="SoftOrange" HeaderText="Punches Required In A Day" ClientInstanceName="ASPxRoundPanelPReq">
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                <dx:ASPxRadioButtonList ID="RadPunch" runat="server" ClientInstanceName="RadPunch" Width="100%">
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    //alert(RadPunch.GetSelectedIndex());
                                                                            if(RadPunch.GetSelectedIndex()==1)
                                                                            { 
                                                                                RadSinglePunch.SetEnabled(true);}
                                                                            else{
                                                                                RadSinglePunch.SetEnabled(false);
                                                                            }
                                                                        }" />
                                                                    <Items>
                                                                        <dx:ListEditItem Text="No Punch" Value="No Punch" />
                                                                        <dx:ListEditItem Text="Single Punch Only" Value="Single Punch" />
                                                                        <dx:ListEditItem Text="Two Punches" Value="Two Punches" />
                                                                        <dx:ListEditItem Text="Four Punches" Value="Four Punches" Selected="true" />
                                                                        <dx:ListEditItem Text="Multiple Punches" Value="Multiple Punches" />
                                                                    </Items>
                                                                </dx:ASPxRadioButtonList>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   <dx:ASPxRoundPanel ID="ASPxRoundPanelSing" runat="server" ShowHeader="true" Width="100%" Height="20%" Theme="SoftOrange" HeaderText="Single Punch" ClientInstanceName="ASPxRoundPanelSing">
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                 <dx:ASPxRadioButtonList ID="RadSinglePunch" runat="server" Width="100%" ClientInstanceName="RadSinglePunch">
                                                                    <Items>
                                                                        <dx:ListEditItem Text="Fixed Out Time" Value="F" Selected="true"  />
                                                                        <dx:ListEditItem Text="OverWrite" Value="O" />
                                                                        
                                                                    </Items>
                                                                </dx:ASPxRadioButtonList>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                     <dx:ASPxRoundPanel ID="ASPxRoundPanelGOT" runat="server" ShowHeader="true" Width="100%"  Height="20%" Theme="SoftOrange" HeaderText="Over Time" ClientInstanceName="ASPxRoundPanelGOT">
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                <table class="auto-style5">
                                                                    <tr>
                                                                        <td class="auto-style10">
                                                                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Over Time Applicable" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td class="auto-style10">
                                                                            <dx:ASPxCheckBox ID="IsOT" runat="server" CheckState="Unchecked" ValueType="System.String" ValueChecked="Y" ValueUnchecked="N" ClientInstanceName="IsOT"  Theme="SoftOrange">
                                                                            </dx:ASPxCheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Over Time Rate Per Hour" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="ASPxTextOTRate" runat="server" Width="170px">
                                                                                <MaskSettings Mask="&lt;0..99999g&gt;.&lt;00..99&gt;" />
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                         <td>
                                                    <dx:ASPxCheckBox ID="IsOS" runat="server" CheckState="Unchecked" Text="Over Stay Applicable" Theme="SoftOrange" ValueType="System.String" ValueChecked="Y" ValueUnchecked="N" ClientInstanceName="IsOS">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                                                    </tr>
                                                                </table>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   <%-- <dx:ASPxCheckBox ID="chkOverStay" runat="server" CheckState="Unchecked" Text="Over Stay Applicable" Theme="SoftOrange">
                                                    </dx:ASPxCheckBox>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Shift">
                    <ContentCollection>
                        <dx:ContentControl runat="server">

                            <table width="100%">
                                <tr>
                                    <td width="50%" valign="top">
                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" Theme="SoftOrange" Width="100%" HeaderText="Shift Details">
                                            <PanelCollection>
                                                <dx:PanelContent runat="server">

                                                    <table class="dxflInternalEditorTable_SoftOrange">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="Shift Type*">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td colspan="2">
                                                                <dx:ASPxComboBox ID="ddlShiftType" runat="server" ValueType="System.String" EnableTheming="True" Theme="SoftOrange">
                                                                    <Items>
                                                                        <dx:ListEditItem Text="Fixed" Value="F" Selected="true" />
                                                                        <dx:ListEditItem Text="Rorational" Value="R" />
                                                                        <dx:ListEditItem Text="Ignore" Value="I" />
                                                                        <dx:ListEditItem Text="Flexi" Value="FLX" />
                                                                        <dx:ListEditItem Text="Multi Shift" Value="M" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="SoftOrange Shift">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td colspan="2">
                                                                <dx:ASPxComboBox ID="ddlShift" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange">
                                                                    
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style6">
                                                                <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="Shift For Rotation">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td class="auto-style6">
                                                                <dx:ASPxComboBox ID="ddlShiftPat" runat="server" EnableTheming="True" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>
                                                                <dx:ASPxButton ID="ASPxButtonAddShift" runat="server" Text="Add">
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td class="auto-style6">&nbsp;</td>
                                                            <td class="auto-style6"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td colspan="2">
                                                                <dx:ASPxTextBox ID="ASPxTextShiftPat" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td colspan="2"> <dx:ASPxComboBox ID="ddlRemoveShiftPat" runat="server" EnableTheming="True" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>
                                                                <dx:ASPxButton ID="ASPxButtonRemoveShift" runat="server" Text="Remove">
                                                                </dx:ASPxButton></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td colspan="2">&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="Run Auto Shift">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td colspan="2">
                                                                <dx:ASPxCheckBox ID="IsRunAutoShift" runat="server" CheckState="Unchecked" Theme="SoftOrange" ClientInstanceName="IsRunAutoShift" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="Add Shift">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td colspan="2">
                                                                <dx:ASPxComboBox ID="ASPxComboBox5" runat="server" EnableTheming="True" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="Shift Remaining Days">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td colspan="2">
                                                                <dx:ASPxTextBox ID="ASPxTextShiftRemainDay" runat="server" MaxLength="2" Width="170px">
                                                                    <MaskSettings Mask="&lt;0..7&gt;" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style7">
                                                                <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="Shift Change After How Many Days">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td colspan="2" class="auto-style7">
                                                                <dx:ASPxTextBox ID="ASPxTextShiftChangeDay" runat="server" MaxLength="2" Width="170px">
                                                                    <MaskSettings Mask="&lt;0..31&gt;" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td class="auto-style7"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td colspan="2">&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>

                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </td>
                                   <td width="50%" valine="top">
                                       <dx:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" Theme="SoftOrange" Width="100%" HeaderText="Week Off Details">
                                            <PanelCollection>
                                                <dx:PanelContent runat="server">
                                                    <table class="dxflInternalEditorTable_SoftOrange">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="First Week Off Day">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlFirstWeeklyOff" runat="server" EnableTheming="True" Theme="SoftOrange">
                                                                    <ClientSideEvents ValueChanged="function(s, e) {
                                                                            EnableShiftControl()	
                                                                            }" />
                                                                    <Items>
                                                                        <dx:ListEditItem Text="None" Value="NON" />
                                                                        <dx:ListEditItem Selected="True" Text="Sunday" Value="SUN" />
                                                                        <dx:ListEditItem Text="Monday" Value="MON" />
                                                                        <dx:ListEditItem Text="Tuesday" Value="TUE" />
                                                                        <dx:ListEditItem Text="Wednesday" Value="WED" />
                                                                        <dx:ListEditItem Text="Thursday" Value="THU" />
                                                                        <dx:ListEditItem Text="Friday" Value="FRI" />
                                                                        <dx:ListEditItem Text="Saturday" Value="SAT" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel23" runat="server" Text="Second Week Off Day">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlSecondWeeklyOff" runat="server" EnableTheming="True"  Theme="SoftOrange">
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	EnableShiftControl()
}" />
                                                                    <Items>
                                                                        <dx:ListEditItem  Selected="True" Text="None" Value="NON" />
                                                                        <dx:ListEditItem  Text="Sunday" Value="SUN" />
                                                                        <dx:ListEditItem Text="Monday" Value="MON" />
                                                                        <dx:ListEditItem Text="Tuesday" Value="TUE" />
                                                                        <dx:ListEditItem Text="Wednesday" Value="WED" />
                                                                        <dx:ListEditItem Text="Thursday" Value="THU" />
                                                                        <dx:ListEditItem Text="Friday" Value="FRI" />
                                                                        <dx:ListEditItem Text="Saturday" Value="SAT" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel24" runat="server" Text="Second Week Off Type">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlSecondWeeklyOffType" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange">
                                                                    <Items>
                                                                       <dx:ListEditItem  Selected="True" Text="Full" Value="F" />
                                                                        <dx:ListEditItem  Text="Half Day" Value="H" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text="Half Day Shifts">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlHalfDayShift" runat="server" EnableTheming="True" Theme="SoftOrange">
                                                                   
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <dx:ASPxLabel ID="ASPxLabel26" runat="server" Text="Second Week Off Day">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td > 
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="ChkOffDay1" runat="server" CheckState="Unchecked" ClientInstanceName="ChkOffDay1" Text="1" Theme="SoftOrange" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="chkOffDay2" runat="server" CheckState="Unchecked" ClientInstanceName="ChkOffDay2" Text="2" Theme="SoftOrange" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="ChkOffDay3" runat="server" CheckState="Unchecked" ClientInstanceName="ChkOffDay3" Text="3" Theme="SoftOrange" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="ChkOffDay4" runat="server" CheckState="Unchecked" ClientInstanceName="ChkOffDay4" Text="4" Theme="SoftOrange" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="ChkOffDay5" runat="server" CheckState="Unchecked" ClientInstanceName="ChkOffDay5" Text="5" Theme="SoftOrange" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxRoundPanel>
                                   </td>
                                </tr>
                            </table>

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Time Office Policy">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="50%">
                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel7" runat="server" Width="100%" Theme="SoftOrange" HeaderText=""><PanelCollection>
<dx:PanelContent runat="server">
    <table width="100%">
        <tr>
            <td class="auto-style6">
                <dx:ASPxLabel ID="ASPxLabel27" runat="server" Text="Duplicate Check Minute">
                </dx:ASPxLabel>
            </td>
            <td class="auto-style6">
                <dx:ASPxTextBox ID="ASPxTextDupliateMin" runat="server" Width="170px">
                    <MaskSettings Mask="&lt;0..120&gt;" ShowHints="True" />
                </dx:ASPxTextBox>
            </td>
            <td class="auto-style6"></td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel28" runat="server" Text="4 Punches In Night Shift">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsForurPunchInNight" runat="server" CheckState="Unchecked" ClientInstanceName="IsForurPunchInNight" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel29" runat="server" Text="End Time For In Punch">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextBoxEndTimeForIn" runat="server" Width="170px">
                    <MaskSettings Mask="HH:mm" />
                </dx:ASPxTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel30" runat="server" Text="End Time For Out Punch(Next Date) For RTC Employee With Multiple Punch" Wrap="True">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextBoxEndTimeForOut" runat="server" Width="170px">
                    <MaskSettings Mask="HH:mm" />
                </dx:ASPxTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel31" runat="server" Text="Permis Early Min Auto Shift">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextEarlyMinAutoShift" runat="server" Width="170px">
                    <MaskSettings Mask="&lt;0..480&gt;" ShowHints="True" />
                </dx:ASPxTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel32" runat="server" Text="Permis Late Min Auto Shift">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextLateMinAutoShift" runat="server" Width="170px">
                    <MaskSettings Mask="&lt;0..480&gt;" ShowHints="True" />
                </dx:ASPxTextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style6">
                <dx:ASPxLabel ID="ASPxLabel33" runat="server" Text="Is Present Of Week Off Present">
                </dx:ASPxLabel>
            </td>
            <td class="auto-style6">
                <dx:ASPxCheckBox ID="IsPreOnWOP" runat="server" CheckState="Unchecked" ClientInstanceName="IsPreOnWOP" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td class="auto-style6"></td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel34" runat="server" Text="Is Present On Holiday">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsPreOnHLDP" runat="server" CheckState="Unchecked" ClientInstanceName="IsPreOnHLDP" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel35" runat="server" Text="Mark A W/H A As AAA">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsAWAA" runat="server" CheckState="Unchecked" ClientInstanceName="IsAWAA" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel36" runat="server" Text="Mark A W/H As AA">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsAW" runat="server" CheckState="Unchecked" ClientInstanceName="IsAW" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel37" runat="server" Text="Mark W/H As AA">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsWA" runat="server" CheckState="Unchecked" ClientInstanceName="IsWA" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
                                            </dx:PanelContent>
</PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </td>
                                    <td width="50%">
                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel8" runat="server" Width="100%" Theme="SoftOrange" HeaderText=""><PanelCollection>
<dx:PanelContent runat="server">
    <table width="100%">
        <tr>
            <td class="auto-style8">
                <dx:ASPxLabel ID="ASPxLabel38" runat="server" Text="No Of Present For Week Off">
                </dx:ASPxLabel>
            </td>
            <td class="auto-style8">
                <dx:ASPxTextBox ID="ASPxTextNoOfDayForWO" runat="server" Width="170px">
                    <MaskSettings Mask="&lt;0..6&gt;"  ShowHints="True" />
                </dx:ASPxTextBox>
            </td>
            <td class="auto-style8"></td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel39" runat="server" Text="Is Auto Absent Allowed">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsAutoABS" runat="server" CheckState="Unchecked" ClientInstanceName="IsAutoABS" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel40" runat="server" Text="Is WeekOff Include In Duty Roster">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsWOInDutyRoster" runat="server" CheckState="Unchecked" ClientInstanceName="IsWOInDutyRoster" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel41" runat="server" Text="Mark WO As A When No Of Present &lt; No of  Present For WO">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsAbsOnNoDay" runat="server" CheckState="Unchecked" ClientInstanceName="IsAbsOnNoDay" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel42" runat="server" Text="Mark MIS As Absent">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsMISAsA" runat="server" CheckState="Unchecked" ClientInstanceName="IsMISAsA" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel43" runat="server" Text="Mark MIS As Half Day">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsMISAsH" runat="server" CheckState="Unchecked" ClientInstanceName="IsMISAsH" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel44" runat="server" Text="Out Work Minus From Working Hours">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsOWMinus" runat="server" CheckState="Unchecked" ClientInstanceName="IsOWMinus" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel45" runat="server" Text="Is Comp Off Allowed For POH/POW">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxCheckBox ID="IsCOffAlloed" runat="server" CheckState="Unchecked" ClientInstanceName="IsCOffAlloed" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                </dx:ASPxCheckBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</dx:PanelContent>
</PanelCollection>
                                        </dx:ASPxRoundPanel>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                <dx:TabPage Text="Over Time Settings">
                    <ContentCollection>
                        <dx:ContentControl runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="50%" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <dx:ASPxRoundPanel ID="ASPxRoundPanel9" runat="server" Theme="SoftOrange" Width="100%" HeaderText="OT Option">
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                <dx:ASPxRadioButtonList ID="RadOTOption" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange">
                                                                    <Items>
                                                                        <dx:ListEditItem Selected="True" Text="OT=OutTime-ShiftEndTime" Value="1" />
                                                                        <dx:ListEditItem Text="OT=WorkingHours-ShiftHours" Value="2" />
                                                                        <dx:ListEditItem Text="OT=EarlyComing+LateDeparture" Value="3" />
                                                                    </Items>
                                                                </dx:ASPxRadioButtonList>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxRoundPanel ID="ASPxRoundPanel10" runat="server" Theme="SoftOrange" Width="100%" HeaderText="OT Parameter Option">
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                <dx:ASPxCheckBox ID="IsOTOnEarly" runat="server" CheckState="Unchecked" Text="OT Allowd In Case Of Ealry Arrival" ClientInstanceName="IsOTOnEarly" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                                <br />
                                                                <dx:ASPxCheckBox ID="IsOTinMinus" runat="server" CheckState="Unchecked" Text="OT In (-) Calculation" ClientInstanceName="IsOTinMinus" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                                <br />
                                                                <dx:ASPxCheckBox ID="IsOTRound" runat="server" CheckState="Unchecked" Text="Round Over Time" ClientInstanceName="IsOTRound" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                </dx:ASPxCheckBox>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxRoundPanel ID="ASPxRoundPanel11" runat="server" Theme="SoftOrange" Width="100%" HeaderText="OT Deduction">
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="auto-style9">
                                                                            <dx:ASPxLabel ID="ASPxLabel46" runat="server" Text="Deduct OT On Holiday">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td class="auto-style9">
                                                                            <dx:ASPxTextBox ID="ASPxTextOTDOnHLD" runat="server" onkeypress= "fncInputNumericValuesOnly('ASPxTextOTDedOnWO')" Width="170px">
                                                                                
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxLabel ID="ASPxLabel47" runat="server" Text="Deduct OT On WeekOff">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="ASPxTextOTDedOnWO"  onkeypress= "fncInputNumericValuesOnly('ASPxTextOTDedOnWO')"  runat="server" Width="170px">
                                                                                
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%" >
                                         <table width="100%">
                                            <tr>
                                                <td>
                                                    <dx:ASPxRoundPanel ID="ASPxRoundPanel12" runat="server" Theme="SoftOrange" Width="100%" HeaderText="OT Duration" >
                                                        <PanelCollection>
                                                            <dx:PanelContent runat="server">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="auto-style9">
                                                                            <dx:ASPxLabel ID="ASPxLabel48" runat="server" Text="Deduct OT On Holiday">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td class="auto-style9">
                                                                            <dx:ASPxTextBox ID="ASPxTextDedOTOnHLDsssss" runat="server"  onkeypress= "fncInputNumericValuesOnly('ASPxTextDedOTOnHLD')" Width="170px">
                                                                                
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxLabel ID="ASPxLabel49" runat="server" Text="Deduct OT On WeekOff">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="ASPxTextDedOTOnOFf" runat="server" onkeypress= "fncInputNumericValuesOnly('ASPxTextDedOTOnOFf')" Width="170px">
                                                                               
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxLabel ID="ASPxLabel50" runat="server" Text="OT Restict End Duration">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="ASPxTextBox23" runat="server" onkeypress= "fncInputNumericValuesOnly('ASPxTextBox23')" MaxLength="3 "  Width="170px">
                                                                              
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>

            <ClientSideEvents Init="function(s, e) {
	LoadGrpScript();
}" />

        </dx:ASPxPageControl>
        </td>
        </tr>
           <tr>
               <td>
                    <dx:ASPxLabel ID="lblError" runat="server" ClientInstanceName="lblError" ForeColor="Red" Text="   ">
                    </dx:ASPxLabel>
               </td>
               <td width="20%" align="center">

                   <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Cancel" Visible="False" >
                   </dx:ASPxButton>

               </td>
              <td width="20%" align="center">

                   <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Save" AutoPostBack="False">
                       <ClientSideEvents Click="function(s, e) {
//alert(document.getElementById('ASPxPageControl1_ASPxRoundPanelGen_IsTimeLoss_S_D').value);
                           //alert(IsRTC.GetValue());
OnSaveClick();
}" />
                   </dx:ASPxButton>

               </td>
           </tr>
     </table>
    </div>
    </form>

<%-- <dx:ASPxCheckBox ID="chkOverStay" runat="server" CheckState="Unchecked" Text="Over Stay Applicable" Theme="SoftOrange">
                                                    </dx:ASPxCheckBox>--%>
</body>
</html>
