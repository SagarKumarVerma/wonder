﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmEmployee.aspx.cs" Inherits="frmEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>
        <table style="width:100%; border:solid;">
            <tr>
                <td align="center"></td>
            </tr>
               <tr>
                <td>
                    <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click">
                        <Image IconID="people_ms_16x16devav">
                        </Image>
                    </dx:ASPxButton>
                     <dx:ASPxButton ID="btlDel" runat="server" Text="Delete" OnClick="btlDel_Click">
                         <Image IconID="actions_cancel_16x16office2013">
                         </Image>
                    </dx:ASPxButton>
                     <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" >
                         <Image IconID="actions_download_16x16">
                         </Image>
                    </dx:ASPxButton>
                </td>
                
            </tr>
            <tr>
                <td>
                    <dx:ASPxGridView ID="GrdEmp" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="PAYCODE" ClientInstanceName="GrdEmp" OnPageIndexChanged="GrdEmp_PageIndexChanged"  >
                        <Settings ShowFilterRow="True" ShowTitlePanel="True"/>
                        <ClientSideEvents CustomButtonClick="function(s, e) {

var obj=GrdEmp.GetRowKey(e.visibleIndex)

	window.location = &quot;EmployeeEdit.aspx?Value=&quot; + obj;
}"  />
                        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                        <SettingsPager>
                            <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                        </SettingsPager>
                        <SettingsEditing Mode="PopupEditForm">
                        </SettingsEditing>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior ConfirmDelete="True" />
                        <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                        <SettingsPopup>
                            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                        </SettingsPopup>
                        <SettingsSearchPanel Visible="True" />
                        <SettingsText PopupEditFormCaption="Employee" Title="Employee" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowNewButton="true" ShowEditButton="true" VisibleIndex="1" ButtonRenderMode="Image" ShowDeleteButton="True">
                                <CustomButtons >
                                    <dx:GridViewCommandColumnCustomButton ID="Edit"  >
                                        <Image ToolTip="Edit" IconID="actions_edit_16x16devav"/>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                               
                            </dx:GridViewCommandColumn>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0" Caption=" ">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="ACTIVE" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PAYCODE" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="EMPNAME" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GUARDIANNAME" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="DateOFBIRTH" VisibleIndex="6">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="DateOFJOIN" VisibleIndex="7">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="PRESENTCARDNO" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="COMPANYNAME" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DivisionName" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CATAGORYNAME" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DEPARTMENTNAME" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GradeName" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SEX" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ISMARRIED" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DESIGNATION" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
            </tr>
         
        </table>
    </div>
  
   
</asp:Content>

