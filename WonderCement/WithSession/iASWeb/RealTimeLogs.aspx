﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="RealTimeLogs.aspx.cs" Inherits="RealTimeLogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <script type="text/javascript">
    window.setInterval(function () {
        ASPxCallbackPanelGrd.PerformCallback();
        //ASPxGridView1.reload();
    }, 5000);
 </script>
    <table style="width: 100%">
        <tr>
            <td>
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%">
                    <Settings ShowFilterRow="True" />
                    <SettingsPager>
                        <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                    </SettingsPager>
                      <SettingsSearchPanel Visible="True" />
                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                    <SettingsText Title="Real Time Logs" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Verify Mode" VisibleIndex="4" FieldName="verify_mode">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Punch Time" VisibleIndex="3" FieldName="io_time">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="2" FieldName="EMPNAME">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Biometric ID" VisibleIndex="1" FieldName="user_id" >
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Devie ID" VisibleIndex="0" FieldName="device_id">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="In/ Out" VisibleIndex="5" FieldName="io_mode">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataBinaryImageColumn Caption="Image" VisibleIndex="6" Width="100px">
                            <PropertiesBinaryImage ImageHeight="100px" ImageWidth="100px">
                                <EditingSettings Enabled="True">
                                </EditingSettings>
                            </PropertiesBinaryImage>
                        </dx:GridViewDataBinaryImageColumn>
                    </Columns>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>

</asp:Content>

