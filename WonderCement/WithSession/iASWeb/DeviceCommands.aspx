﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="DeviceCommands.aspx.cs" Inherits="DeviceCommands" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <div style="border-style: solid; border-width: thin; border-color: inherit;">
    <table class="dxeBinImgCPnlSys">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
           <td>
               <table>
                   <tr>
                       <td style="width:auto;padding:5px">
                             <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" AutoPostBack="True" EnableTheming="True" Theme="SoftOrange"  OnSelectedIndexChanged="ASPxComboBox1_SelectedIndexChanged">
                   <Items>
                       <dx:ListEditItem Text="All" Value="A" Selected="true" />
                       <dx:ListEditItem Text="Success" Value="S" />
                       <dx:ListEditItem Text="Pending" Value="P" />
                   </Items>
               </dx:ASPxComboBox>
                       </td>
                       <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Clear Command" OnClick="ASPxButton1_Click" Theme="SoftOrange">
                    <Image IconID="actions_trash_16x16">
                    </Image>
                </dx:ASPxButton>
                       </td>
                   </tr>
               </table>
             

           </td>
            <td align="left">
                </td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
        </tr>
         <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" runat="server" Theme="Aqua" Width="100%"  >
                    <PanelCollection>
<dx:PanelContent runat="server">
   <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        EnableTheming="True" KeyFieldName="DeviceID" 
        Theme="SoftOrange" Width="100%" Font-Size="Small" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared"    >
        <Settings HorizontalScrollBarMode="Visible" 
            VerticalScrollableHeight="400" ShowFilterRow="True" />
          <SettingsResizing ColumnResizeMode="Control" />
        <SettingsPager PageSize="50">
        </SettingsPager>
        <SettingsEditing Mode="PopupEditForm" editformcolumncount="1">
            <BatchEditSettings EditMode="Row" />
        </SettingsEditing>
         <Settings HorizontalScrollBarMode="Visible" 
                                                        VerticalScrollableHeight="400" VerticalScrollBarMode="Auto" 
                                                        VerticalScrollBarStyle="VirtualSmooth" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsResizing ColumnResizeMode="Control" />
       
         <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
        <SettingsPopup>
            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
        </SettingsPopup>
        <SettingsText CommandUpdate="Save" PopupEditFormCaption="Device Commands" ConfirmDelete="Are You Sure Want To Delete Data?" />
        <Columns>
            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0" Width="8%" Caption=" " ShowClearFilterButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Transaction ID" FieldName="TransactionID" 
                ShowInCustomizationForm="True" VisibleIndex="1" Width="150px"  >
               
                <CellStyle ForeColor="Blue">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Device ID" FieldName="DeviceID" 
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Device Name" FieldName="DeviceName" 
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Command Code" FieldName="CommandCode" 
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Discription"  
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Return Code" FieldName="Result" 
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Result" FieldName="Status" 
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Command Time" FieldName="UpdateTime" 
                ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
              
            </dx:GridViewDataTextColumn>
           
           
        </Columns>
    </dx:ASPxGridView>
                        </dx:PanelContent>
</PanelCollection>

                </dx:ASPxCallbackPanel>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>

