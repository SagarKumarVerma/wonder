﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Globalization;
using System.Data.OleDb;
using GlobalSettings;
public partial class frmMain : System.Web.UI.Page
{
    Setting_App Sap = new Setting_App();
    string strsql = "";
    DataSet dsEmp = new DataSet();
    DataSet dsStages = new DataSet();
    DataSet dsLeave = new DataSet();
    DateTime Dt = System.DateTime.MinValue;
    Class_Connection cn = new Class_Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = "Home Page";
        if (!IsPostBack)
        {
            Session["G_Reader"] = "None";
            VadlidCheck();
           
        }

       
    }
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        DataSet dsRecords = new DataSet();
        string pcode = "";
        if (Session["LoginPayCode"].ToString().Trim().ToUpper() != "ADMIN")
        {

          //  string ssn = Session["Custcomp"] + "_" + Session["PAYCODE"].ToString();

            DateTime doffice = System.DateTime.MinValue;
            strsql = "select convert (char,dateoffice,126) DATE,SubString(Convert(Char(17),in1,13),13,5) 'INTIME', SubString(Convert(Char(17),tbltimeregister.out2,13),13,5) 'OUTTIME',STATUS  from tbltimeregister where PayCode='" + Session["LoginPayCode."].ToString().Trim().ToUpper() + "'  order by dateoffice";
            dsRecords = cn.FillDataSet(strsql);
            if (dsRecords.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsRecords.Tables[0].Rows.Count; i++)
                {
                    doffice = DateTime.ParseExact(dsRecords.Tables[0].Rows[i]["DATE"].ToString().Substring(0, 10), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (e.Day.Date == new DateTime(doffice.Year, doffice.Month, doffice.Day))
                    {
                        Literal Literal1 = new Literal();
                        Literal Literal2 = new Literal();
                        Literal Literal3 = new Literal();

                        string status = dsRecords.Tables[0].Rows[i]["Status"].ToString();
                        if (status.Trim() == "A")
                        {

                            Literal1.Text = "<br /><font size=2px color=Red >" + dsRecords.Tables[0].Rows[i]["Status"].ToString() + "</font>";
                        }

                        else
                        {
                            Literal1.Text = "<br /><font size=2px color=Green  >" + dsRecords.Tables[0].Rows[i]["Status"].ToString() + "</font>";
                        }

                        string in1 = dsRecords.Tables[0].Rows[i]["INTIME"].ToString();
                        if (in1 != "")
                        {
                            Literal2.Text = "<br /><font size=2px color=Voilet >" + dsRecords.Tables[0].Rows[i]["INTIME"].ToString() + "</font>";
                        }
                        else
                        {
                            Literal2.Text = "<br /><font size=2px color=Voilet > --:--</font>";
                        }

                        string out2 = dsRecords.Tables[0].Rows[i]["OUTTIME"].ToString();
                        if (out2 != "")
                        {
                            Literal3.Text = "<br /><font size=2px color=Voilet >" + dsRecords.Tables[0].Rows[i]["OUTTIME"].ToString() + "</font>";
                        }
                        else
                        {
                            Literal3.Text = "<br /><font size=2px color=Voilet > --:--</font>";
                        }
                        e.Cell.Controls.AddAt(1, Literal1);
                        e.Cell.Controls.AddAt(1, Literal3);
                        e.Cell.Controls.AddAt(1, Literal2);




                    }
                    if (e.Day.Date > new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day))
                    {
                        break;
                    }
                }

            }
            else
            {
                Calendar1.Visible = false;

            }

        }
    }
    protected void Calendar1_DayRender1(object sender, DayRenderEventArgs e)
    {
        DataSet dsRecords = new DataSet();
        string pcode = "";
        if (Session["LoginPayCode."].ToString().Trim().ToUpper() != "ADMIN")
        {

            DateTime doffice = System.DateTime.MinValue;
            strsql = "select convert (char,dateoffice,126) DATE,STATUS  from tbltimeregister where paycode='" + Session["LoginPayCode."].ToString().Trim().ToUpper() + "' order by dateoffice";
            dsRecords = cn.FillDataSet(strsql);
            if (dsRecords.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsRecords.Tables[0].Rows.Count; i++)
                {
                    doffice = DateTime.ParseExact(dsRecords.Tables[0].Rows[i]["DATE"].ToString().Substring(0, 10), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (e.Day.Date == new DateTime(doffice.Year, doffice.Month, doffice.Day))
                    {
                        Literal Literal1 = new Literal();
                        Literal1.Text = "<br /><font size=2px color=red >" + dsRecords.Tables[0].Rows[i]["Status"].ToString() + "</font>";
                        e.Cell.Controls.AddAt(1, Literal1);
                    }
                    if (e.Day.Date > new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day))
                    {
                        break;
                    }
                }

            }
            else
            {
                Calendar1.Visible = false;

            }

        }
    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        DateTime datetime = Calendar1.SelectedDate;

        string leave = Convert.ToString(datetime.ToString("dd/MM/yyyy"));
        Session["LeaveDate"] = leave;
        Session["Today"] = leave;
        Response.Redirect("frmLeaveApplication.aspx");
    }
    protected void Calendar1_DayRender2(object sender, DayRenderEventArgs e)
    {

        DataSet dsRecords = new DataSet();
        string pcode = "";
        if (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN")
        {

            string ssn = Session["Custcomp"] + "_" + Session["PAYCODE"].ToString();

            DateTime doffice = System.DateTime.MinValue;
            strsql = "select convert (char,dateoffice,126) DATE,SubString(Convert(Char(17),in1,13),13,5) 'INTIME', SubString(Convert(Char(17),tbltimeregister.out2,13),13,5) 'OUTTIME',STATUS  from tbltimeregister where ssn='" + ssn.ToString().Trim() + "'  order by dateoffice";
            dsRecords = cn.FillDataSet(strsql);
            if (dsRecords.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsRecords.Tables[0].Rows.Count; i++)
                {
                    doffice = DateTime.ParseExact(dsRecords.Tables[0].Rows[i]["DATE"].ToString().Substring(0, 10), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (e.Day.Date == new DateTime(doffice.Year, doffice.Month, doffice.Day))
                    {
                        Literal Literal1 = new Literal();
                        Literal Literal2 = new Literal();
                        Literal Literal3 = new Literal();

                        string status = dsRecords.Tables[0].Rows[i]["Status"].ToString();
                        if (status.Trim() == "A")
                        {

                            Literal1.Text = "<br /><font size=2px color=Red >" + dsRecords.Tables[0].Rows[i]["Status"].ToString() + "</font>";
                        }

                        else
                        {
                            Literal1.Text = "<br /><font size=2px color=Green  >" + dsRecords.Tables[0].Rows[i]["Status"].ToString() + "</font>";
                        }

                        string in1 = dsRecords.Tables[0].Rows[i]["INTIME"].ToString();
                        if (in1 != "")
                        {
                            Literal2.Text = "<br /><font size=2px color=Voilet >" + dsRecords.Tables[0].Rows[i]["INTIME"].ToString() + "</font>";
                        }
                        else
                        {
                            Literal2.Text = "<br /><font size=2px color=Voilet > --:--</font>";
                        }

                        string out2 = dsRecords.Tables[0].Rows[i]["OUTTIME"].ToString();
                        if (out2 != "")
                        {
                            Literal3.Text = "<br /><font size=2px color=Voilet >" + dsRecords.Tables[0].Rows[i]["OUTTIME"].ToString() + "</font>";
                        }
                        else
                        {
                            Literal3.Text = "<br /><font size=2px color=Voilet > --:--</font>";
                        }
                        e.Cell.Controls.AddAt(1, Literal1);
                        e.Cell.Controls.AddAt(1, Literal3);
                        e.Cell.Controls.AddAt(1, Literal2);




                    }
                    if (e.Day.Date > new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day))
                    {
                        break;
                    }
                }

            }
            else
            {
                Calendar1.Visible = false;

            }

        }
    }
    protected void Calendar1_SelectionChanged1(object sender, EventArgs e)
    {
        DateTime datetime = Calendar1.SelectedDate;

        string leave = Convert.ToString(datetime.ToString("dd/MM/yyyy"));
        Session["LeaveDate"] = leave;
        Response.Redirect("frm_LeaveApplication.aspx");
    }
    protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {

    }
  
    protected void VadlidCheck()
    {
        if (Sap.isDemo.ToString().Trim().ToUpper() == "Y")
        {
            DateTime currentdate = System.DateTime.Now;
            string cDate = Sap.ExpireDate.ToString();
            DateTime dt = DateTime.ParseExact(cDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (dt < currentdate)
            {
                strsql = "update tbluser set IsValid='N' where user_r='Admin'";
                cn.execute_NonQuery(strsql);
            }
        }
        else
        {
            strsql = "update tbluser set IsValid='Y' where user_r='Admin'";
            cn.execute_NonQuery(strsql);
        }
    }



 
}