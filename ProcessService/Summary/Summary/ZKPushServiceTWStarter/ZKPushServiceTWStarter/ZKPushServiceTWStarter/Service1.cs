﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Timers;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using FKWeb;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace ZKPushServiceTWStarter
{
    public partial class Service1 : ServiceBase
    {
        #region Fields

        // SqlConnection con = new SqlConnection("Integrated security=true;Initial Catalog=testclient;Data source=CORTAS");
        // SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=AppDB;Data source=137.59.201.60;User ID=sa;Password=sss");
        //<add name="myConnectionString" connectionString="Data Source=AIJAZ-PC;Initial Catalog=TimeWatch;Persist Security Info=True;User ID=sa;Password=smart@123;Connect Timeout=9999" providerName="System.Data.SqlClient" /> 
        //SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=Timewatch;Data source=CORTAS;User ID=timewatch;Password=timewatch");
        //SqlConnection con1 = new SqlConnection("Integrated security=false;Initial Catalog=eSSLSmartOffice;Data source=CORTAS;User ID=SA;Password=!ndI@@o1*");
        string Catalog = "", DataSource = "", DBUserID = "", DBPassword = "";
       
        //SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=testclient;Data source=192.168.1.2;User ID=sa;Password=sss");
        string sSql = "";
        SqlCommand cmd;
        SqlConnection con;
        SqlConnection msqlConn;

        OleDbConnection MyConn = new OleDbConnection();
        OleDbCommand MyCmd = new OleDbCommand();
       
        DataSet DsCmd = new DataSet();
        DateTime CommandTime = System.DateTime.MinValue;
        DateTime CurrentDate = System.DateTime.Today;
        bool IsSent = false;
        string SqlServer = string.Empty;
        string DB = string.Empty;
        string SqlUser = string.Empty;
        string SqlPass = string.Empty;
        string FTPAddress = string.Empty;
        string FTPUser = string.Empty;
        string FTPPass = string.Empty;
        int TimeToCall = 60;
        string LocalPath = string.Empty;
        TcpListener tcp;
        private Thread listenThread;
       
        int k = 0;
        string port = "";
        string sqlchkdevice = "";

        int Dev_Cmd_ID = 0;
        int LoopCount = 0;
        //Initialize the timer
        System.Timers.Timer timer = new System.Timers.Timer();
        #endregion
        const int GET_DEVICE_STATUS = 1;
         int TempStatus = 0;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            if (args.Length > 0)
            {
                Catalog = args[0];
                DataSource = args[1];
                DBUserID = args[2];
                DBPassword = args[3];
               // TimeToCall = args[4];

            }
            else
            {
                Catalog = "SSSDB";
                DataSource = "192.168.1.152";
                DBUserID = "sa";
                DBPassword = "sss";
              //  TimeToCall = "00:05";

            }
            //string L = Catalog + ", " + DataSource + ", " + DBUserID + ", " + DBPassword;

            SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
            //add this line to text file during start of service
            TraceService("Service Started Without Any Error", "Line 120");
          


            //handle Elapsed event
            //if (k <= 10)
            //{
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

                //This statement is used to set interval to 1 minute (= 60,000 milliseconds)

                timer.Interval = 6000000;

                //enabling the timer
                timer.Enabled = true;
            //}

        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
           // GetDataToUpload();
            CheckFPTTime();
           
        }


        private void CheckFPTTime()
        {
           try
            {
               
                DateTime DTInterval = DateTime.Now;
                if (DTInterval <= DateTime.Now)
                {
                    DTInterval = DTInterval.AddMinutes(Convert.ToDouble(TimeToCall));
                    
                    GetSummary();

                }

            }
            catch (Exception Ex)
            {
                    TraceService(Ex.Message, "CheckFPTTime");
               
            }
           
        }
        public void FTP_Report()
        {
            //string ConnectionString = "Data Source=" + SqlServer.Trim() + ";Initial Catalog=" + DB.Trim() + ";User Id=" + SqlUser.Trim() + ";Password=" + SqlPass.Trim() + ";";
            //SqlConnection con = new SqlConnection(ConnectionString);

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            int RowCount = 1;
            try
            {
                SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");


                sSql = "select paycode, convert (char,dateoffice,103) DATE, SubString(Convert(Char(17),in1,13),13,5) 'IN TIME', SubString(Convert(Char(17),tbltimeregister.out2,13),13,5) 'OUT TIME' from tbltimeregister  where dateoffice='" + CurrentDate.AddDays(-1).ToString("yyyy-MM-dd") + "' and in1 is not null";
                SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                DataSet dsRec = new DataSet();
                SDA.Fill(dsRec);
                if (dsRec.Tables[0].Rows.Count > 0)
                {
                    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                    object misValue = System.Reflection.Missing.Value;
                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    xlWorkSheet.Cells[1, 1] = "PayCode";
                    xlWorkSheet.Cells[1, 2] = "Date";
                    xlWorkSheet.Cells[1, 3] = "In";
                    xlWorkSheet.Cells[1, 4] = "Out";
                    for (int K = 0; K < dsRec.Tables[0].Rows.Count; K++)
                    {
                        RowCount = RowCount + 1;
                        xlWorkSheet.Cells[RowCount, 1] = dsRec.Tables[0].Rows[K]["PayCode"].ToString().Trim();
                        xlWorkSheet.Cells[RowCount, 2] = dsRec.Tables[0].Rows[K]["DATE"].ToString().Trim();
                        xlWorkSheet.Cells[RowCount, 3] = dsRec.Tables[0].Rows[K]["IN TIME"].ToString().Trim();
                        xlWorkSheet.Cells[RowCount, 4] = dsRec.Tables[0].Rows[K]["OUT TIME"].ToString().Trim();

                    }
                    
                   // string FilePath ="E:\\Websites\\Clients-Projects\\Dhanraj_FTP\\FTPtransfer\\FTPtransfer\\bin\\Debug\\";
                    string FilePath = "C:\\iAS\\FTP\\";
                    string FileName = "FTP_GGN_" + System.DateTime.Now.AddDays(-1).ToString("ddMMyyyy") + ".xls";
                    xlApp.DisplayAlerts = false;
                    xlApp.Columns.AutoFit();
                    xlWorkBook.SaveCopyAs(FilePath + FileName);
                   // xlWorkBook.SaveAs(FilePath + FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();

                   // Upload(FileName);
                    Upload(FilePath + FileName);
                    if (IsSent == true)
                    {

                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }
                        sSql = "update tblCSVCalled set isCalled='Y' where Convert(varchar(10),Dt,127)='" + CurrentDate.AddDays(-1).ToString("yyyy-MM-dd") + "'";
                        SqlCommand Cmd = new SqlCommand(sSql, con);
                        Cmd.CommandType = CommandType.Text;
                        Cmd.ExecuteNonQuery();
                        if (con.State != ConnectionState.Closed)
                        {
                            con.Close();
                        }
                    }
                    TraceService("Successfully Transfered For Date: "+System.DateTime.Today+"", "FTP Report");
                }


            }
            catch (Exception ex)
            {
                xlApp.Quit();
                TraceService(ex.ToString(),"FTP Report");
                if (con.State != ConnectionState.Closed)
                {
                    con.Close();
                }

            }
          



        }

        private void Upload(string filename)
        {

            string ftpServerIP = "ftp.peopleworks.ind.in"; //FTPAddress.Trim();
            string ftpUserID = "hwl"; //FTPUser;
            string ftpPassword = "hwl@270418"; //FTPPass;

            FileInfo fileInf = new FileInfo(filename);
            string uri = "ftp://" + ftpServerIP + "/" + fileInf.Name;
            FtpWebRequest reqFTP;

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServerIP + "/" + fileInf.Name));

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

            // By default KeepAlive is true, where the control connection is not closed
            // after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fileInf.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file to be uploaded
            FileStream fs = fileInf.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();
                IsSent = true;
            }
            catch (Exception ex)
            {

                IsSent = false;
                TraceService(ex.Message.ToString(),"FTP Upload Function");
            }
        }
        public void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                if (service.Status != ServiceControllerStatus.Stopped)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }
            }
            catch (Exception ex)
            {
                TraceService(ex.Message, "Line 200 ");
            }
        }


        public void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                if (service.Status != ServiceControllerStatus.Running)
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
            }
            catch (Exception ex)
            {
                TraceService(ex.Message, "Line 218  ");
            }
        }


        protected override void OnStop()
        {

        }
        private void TraceService(string errmessage, string source)
        {
            //FileNameLog = "ZKPUSHSERVICE_LOG-" + DateTime.Now + ".txt";
            //FileStream fs = new FileStream(@"D:\ZKLOGS\" + FileNameLog, FileMode.OpenOrCreate, FileAccess.Write);
            FileStream fs = new FileStream(@"C:\BioService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(errmessage + source + "::" + DateTime.Now);
            sw.Flush();
            sw.Close();

            

        }

        private void GetDataToUpload()
        {
            try
            {
                SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
                bool isUpdated = false;
               
                sSql = "Select * from Employee_SmartToBio where update_flag=0";
                SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                DataSet DsCheck = new DataSet();
                SDA.Fill(DsCheck);
                if (DsCheck.Tables[0].Rows.Count>0)
                {
                    for (int CR=0;CR<DsCheck.Tables[0].Rows.Count;CR++)
                    {
                        try
                        {
                            isUpdated = SetUserInfoName(DsCheck.Tables[0].Rows[CR]["User_Id"].ToString().Trim(), DsCheck.Tables[0].Rows[CR]["Device_ID"].ToString().Trim(), DsCheck.Tables[0].Rows[CR]["Name"].ToString().Trim());
                            TraceService("Uploading User Record " + DsCheck.Tables[0].Rows[CR]["User_Id"].ToString().Trim() + "In Device  " + DsCheck.Tables[0].Rows[CR]["Device_ID"].ToString().Trim() + "Rerurn  " + isUpdated, "GetDataToUpload");
                            if (isUpdated == true)
                            {
                                if (con.State != ConnectionState.Open)
                                {
                                    con.Open();
                                }

                                sSql = "update Employee_SmartToBio set Update_flag=1 where RecordID=" + DsCheck.Tables[0].Rows[CR]["RecordID"].ToString().Trim() + " ";
                                SqlCommand Cmd = new SqlCommand(sSql, con);
                                Cmd.CommandType = CommandType.Text;
                                Cmd.ExecuteNonQuery();
                                if (con.State != ConnectionState.Closed)
                                {
                                    con.Close();
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            TraceService(Ex.Message.ToString(), "GetDataToUpload-Loop");
                            continue;
                        }
                    }
                }


            }
            catch(Exception Ex)
            {
                TraceService(Ex.Message.ToString(), "GetDataToUpload");
            }
        }

        protected void GetSummary()
        {
            try
            {
                 SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
                bool isUpdated = false;

                

                sSql = "Select * from tbl_fkdevice_status where connected=1";
                SqlDataAdapter SDA = new SqlDataAdapter(sSql, con);
                DataSet DsCheck = new DataSet();
                SDA.Fill(DsCheck);
                if (DsCheck.Tables[0].Rows.Count > 0)
                {
                    for(int Dev=0; Dev<DsCheck.Tables[0].Rows.Count; Dev++)
                    {
                        TempStatus = 0;
                        string mDevId = DsCheck.Tables[0].Rows[Dev]["device_id"].ToString().Trim();
                       //msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmd(con, "GET_DEVICE_STATUS", mDevId, null);
                        TempStatus = GET_DEVICE_STATUS;
                        System.Threading.Thread.Sleep(10000);
                        TraceService("Updating Status For " + mDevId.Trim(), "GetSummary");
                        UpdateSummary(mDevId, TempStatus);


                    }
                   
                }
            }
            catch (Exception Ex)
            {
                TraceService(Ex.Message.ToString(), "GetDataToUpload");
            }
        }

        protected void UpdateSummary(string DevId, int TempStatus)
        {
            string sTransId = mTransIdTxt.Text;
            if (sTransId.Length == 0)
            {
                return;
            }
            TraceService("Enter Updating Status For " + DevId.Trim(), "UpdateSummary");
            //msqlConn = FKWebTools.GetDBPool();
            msqlConn = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
            if (msqlConn.State != ConnectionState.Open)
            {
                msqlConn.Open();
            }
            bool ISUpdated = false;
            int LoopCount = 0;
            while (ISUpdated != true  )
            {
                SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
                string sSql = "select status,device_id from tbl_fkcmd_trans where trans_id='" + sTransId + "'";
                DataSet DsSumm = new DataSet();
                SqlDataAdapter SD = new SqlDataAdapter(sSql, con);
                SD.Fill(DsSumm);
               
                if (DsSumm.Tables[0].Rows.Count > 0)
                {
                    if (DsSumm.Tables[0].Rows[0][0].ToString().Trim() == "RESULT" || DsSumm.Tables[0].Rows[0][0].ToString().Trim() == "CANCELLED")
                    {
                        if (TempStatus == GET_DEVICE_STATUS)
                        {
                            string mDev = DsSumm.Tables[0].Rows[0][1].ToString().Trim();

                            try
                            {
                                FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                                sTransId = mTransIdTxt.Text;
                                string DeviD = mDev;
                                if (sTransId.Length == 0)
                                    return;
                                if (msqlConn.State != ConnectionState.Open)
                                {
                                    msqlConn.Open();
                                }

                                sSql = "select trans_id from tbl_fkcmd_trans where trans_id='" + sTransId + "' AND status='RESULT'";
                                SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
                                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                                sTransId = "";
                                if (sqlReader.HasRows)
                                {
                                    if (sqlReader.Read())
                                    {
                                        sTransId = sqlReader[0].ToString().Trim();
                                    }
                                }
                                sqlReader.Close();

                                if (msqlConn.State != ConnectionState.Closed)
                                {
                                    msqlConn.Close();
                                }
                                if (sTransId.Length == 0)
                                    return;

                                if (msqlConn.State != ConnectionState.Open)
                                {
                                    msqlConn.Open();
                                }
                                sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sTransId + "'";
                                sqlCmd = new SqlCommand(sSql, msqlConn);
                                SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
                                sqlParamCmdParamBin.Direction = ParameterDirection.Output;
                                sqlParamCmdParamBin.Size = -1;
                                sqlCmd.Parameters.Add(sqlParamCmdParamBin);

                                sqlCmd.ExecuteNonQuery();

                                byte[] bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
                                byte[] bytResultBin = new byte[0];
                                string sResultText;


                                cmdTrans.GetStringAndBinaryFromBSCommBuffer(bytCmdResult, out sResultText, out bytResultBin);
                                if (msqlConn.State != ConnectionState.Closed)
                                {
                                    msqlConn.Close();
                                }


                                try
                                {
                                    String TotUser = "";
                                    String TotManager = "";
                                    String TotFP = "";
                                    String TotFace = "";
                                    String TotPass = "";
                                    String Totcard = "";
                                    String TotLog = "";
                                    String AllUser = "";
                                    JObject jobjTest = JObject.Parse(sResultText);

                                    try
                                    {
                                        AllUser = jobjTest["total_user_count"].ToString();
                                    }
                                    catch
                                    {
                                        AllUser = "0";
                                    }
                                    try
                                    {
                                        TotUser = jobjTest["user_count"].ToString();
                                    }
                                    catch
                                    {
                                        TotUser = "0";
                                    }
                                    try
                                    {
                                        TotManager = jobjTest["manager_count"].ToString();
                                    }
                                    catch
                                    {
                                        TotManager = "0";
                                    }

                                    try
                                    {
                                        TotFP = jobjTest["fp_count"].ToString();
                                    }
                                    catch
                                    {
                                        TotFP = "0";
                                    }
                                    try
                                    {
                                        TotFace = jobjTest["face_count"].ToString();
                                    }
                                    catch
                                    {
                                        TotFace = "0";
                                    }
                                    try
                                    {
                                        TotPass = jobjTest["password_count"].ToString();
                                    }
                                    catch
                                    {
                                        TotPass = "0";
                                    }
                                    try
                                    {
                                        Totcard = jobjTest["idcard_count"].ToString();
                                    }
                                    catch
                                    {
                                        Totcard = "0";
                                    }
                                    try
                                    {
                                        TotLog = jobjTest["total_log_count"].ToString();
                                    }
                                    catch
                                    {
                                        TotLog = "0";
                                    }
                                    if (con.State != ConnectionState.Open)
                                    {
                                        con.Open();
                                    }
                                    sSql = " update tbl_fkdevice_status set total_user_count=" + AllUser + ",user_count=" + TotUser + ",manager_count=" + TotManager + ",fp_count=" + TotFP + ", " +
                                                 " face_count=" + TotFace + ",password_count=" + TotPass + ",idcard_count=" + Totcard + ",total_log_count=" + TotLog + " where device_id='" + DevId + "'";

                                    SqlCommand Cmd = new SqlCommand(sSql, con);
                                    Cmd.CommandType = CommandType.Text;
                                    Cmd.ExecuteNonQuery();
                                    if (con.State != ConnectionState.Closed)
                                    {
                                        con.Close();
                                    }
                                    ISUpdated = true;
                                }
                                catch (Exception Exc)
                                {
                                    TraceService("UpateSummary", Exc.Message);
                                }
                            }
                            catch (Exception Exc)
                            {
                                TraceService("UpateSummary", Exc.Message);
                            }
                            return;
                        }
                    }
                }
                LoopCount = LoopCount + 1;
            }

        }

        protected bool SetUserInfo(string sUserId, string mDevId, string mName)
        {
            SqlConnection Con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");

            try
            {
                JObject vResultJson = new JObject();
                JArray vEnrollDataArrayJson = new JArray();
                FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                string sUserPwd = "";
                string sUserCard = "";
                int index = 1;
                string sUserName = mName.Trim();
                string sUserPriv = "USER";
                vResultJson.Add("user_id", sUserId);
                vResultJson.Add("user_name", sUserName);
                vResultJson.Add("user_privilege", sUserPriv);

                byte[] binData = new byte[0];
                byte[] strParam = new byte[0];
                if (FKWebTools.mBIN[FKWebTools.BACKUP_USER_PHOTO] != null && FKWebTools.mBIN[FKWebTools.BACKUP_USER_PHOTO].Length > 0)
                {
                    try
                    {
                        vResultJson.Add("user_photo", FKWebTools.GetBinIndexString(index++));
                        FKWebTools.AppendBinaryData(ref binData, FKWebTools.mBIN[FKWebTools.BACKUP_USER_PHOTO]);
                    }
                    catch (Exception Ex)
                    {
                        TraceService(Ex.Message.ToString(), "SetUserInfoFunction-Line720");

                    }
                }

                for (int nIndex = 0; nIndex < FKWebTools.BACKUP_MAX; nIndex++)
                {
                    if (nIndex == FKWebTools.BACKUP_PSW && sUserPwd.Length > 0)
                    {
                        FKWebTools.mBIN[nIndex] = new byte[sUserPwd.Length];
                        byte[] bytText = System.Text.Encoding.UTF8.GetBytes(sUserPwd);
                        bytText.CopyTo(FKWebTools.mBIN[nIndex], 0);
                    }
                    if (nIndex == FKWebTools.BACKUP_CARD && sUserCard.Length > 0)
                    {
                        FKWebTools.mBIN[nIndex] = new byte[sUserCard.Length];
                        byte[] bytText = System.Text.Encoding.UTF8.GetBytes(sUserCard);
                        bytText.CopyTo(FKWebTools.mBIN[nIndex], 0);
                    }
                    if (FKWebTools.mBIN[nIndex] != null && FKWebTools.mBIN[nIndex].Length > 0)
                    {
                        JObject vEnrollDataJson = new JObject();
                        vEnrollDataJson.Add("backup_number", nIndex);
                        vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                        vEnrollDataArrayJson.Add(vEnrollDataJson);
                        FKWebTools.AppendBinaryData(ref binData, FKWebTools.mBIN[nIndex]);
                    }
                }

                vResultJson.Add("enroll_data_array", vEnrollDataArrayJson);
                string sFinal = vResultJson.ToString(Formatting.None);
                while (sFinal.Contains("\r\n "))
                {
                    sFinal.Replace("\r\n ", "\r\n");
                }
                sFinal.Replace("\r\n", "");

                cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);

                FKWebTools.ConcateByteArray(ref strParam, binData);
                TraceService("Uploading UserRecord", "Entering Command With Con--" + Con.ConnectionString.Trim());
                mTransIdTxt.Text = FKWebTools.MakeCmd(Con, "SET_USER_INFO", mDevId, strParam);
                return true;
            }
            catch (Exception Ex)
            {
                TraceService(Ex.Message.ToString(), "SetUserInfoFunction");
                return false;
            }
        }
        protected  bool  SetUserInfoName(string sUserId, string mDevId,string mName)
        {
             SqlConnection Con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
           
            try
            {
                JObject vResultJson = new JObject();
                JArray vEnrollDataArrayJson = new JArray();
                FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
                byte[] mByteParam = new byte[0];

                string sUserName = mName.Trim();
                vResultJson.Add("user_id", sUserId);
                vResultJson.Add("user_name", sUserName);

                string sFinal = vResultJson.ToString(Formatting.None);
                while (sFinal.Contains("\r\n "))
                {
                    sFinal.Replace("\r\n ", "\r\n");
                }
                sFinal.Replace("\r\n", "");

                cmdTrans.CreateBSCommBufferFromString(sFinal, out mByteParam);
                TraceService("Entering Name Command", "SetUserInfoName");
                mTransIdTxt.Text = FKWebTools.MakeCmd(Con, "SET_USER_NAME", mDevId, mByteParam);
                return true;
            }
            catch (Exception Ex)
            {
                TraceService(Ex.Message.ToString(), "SetUserInfoName");
                return false;
            }
        }


    }
}
