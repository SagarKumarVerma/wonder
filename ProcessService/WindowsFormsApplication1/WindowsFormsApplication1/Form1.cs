﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          string  Catalog = "AppDB";
          string DataSource = "137.59.201.60";
          string DBUserID = "sa";
          string DBPassword = "sss";
          string port = "7777";
            SqlConnection con = new SqlConnection("Integrated security=false;Initial Catalog=" + Catalog + ";Data source=" + DataSource + ";User ID=" + DBUserID + ";Password=" + DBPassword + "");
            DataSet dschkdevice = new DataSet();
            con.Open();
          string  sqlchkdevice = "select CONVERT(VARCHAR(12), DATEDIFF(minute, updatetime, GETDATE()), 114) as UpdateTimeDiff from ServiceUpdateTime";
            SqlDataAdapter adphkdevice = new SqlDataAdapter(sqlchkdevice, con);
            adphkdevice.Fill(dschkdevice, "TableName");
            con.Close();
            if (dschkdevice.Tables[0].Rows.Count > 0)
            {
                int diff = Convert.ToInt32(dschkdevice.Tables[0].Rows[0]["UpdateTimeDiff"].ToString());
                if (diff > 2)
                {
                    StopService("ZKPUSHSERVICETW", 25000);

                    System.Threading.Thread.Sleep(120000);

                    StartService("ZKPUSHSERVICETW", 35000);
                }
            }
        }
        public void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                if (service.Status != ServiceControllerStatus.Stopped)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }
            }
            catch (Exception ex)
            {
                TraceService(ex.Message, "Line 200 ");
            }
        }


        public void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                if (service.Status != ServiceControllerStatus.Running)
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
            }
            catch (Exception ex)
            {
                TraceService(ex.Message, "Line 218  ");
            }
        }


       
        private void TraceService(string errmessage, string source)
        {
            //string FileNameLog = "";
            //FileNameLog = "ZKPUSHSERVICE_LOG-" + DateTime.Now + ".txt";
            //set up a filestream
            //FileStream fs = new FileStream(@"D:\ZKLOGS\" + FileNameLog, FileMode.OpenOrCreate, FileAccess.Write);
            FileStream fs = new FileStream(@"E:\ZKPUSHSERVICE_LOGS.txt", FileMode.OpenOrCreate, FileAccess.Write);

            //set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);

            //find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);

            //add the text
            sw.WriteLine(errmessage + source + "::" +  DateTime.Now);
            //add the text to the underlying filestream

            sw.Flush();
            //close the writer
            sw.Close();



        }
    }
}
